USE somanager;

-- Rappel des id des roles
-- (1, 'etudiant'),
-- (2, 'admin'),
-- (3, 'prof'),
-- (4, 'profOption'),
-- (5, 'profResponsable'),
-- (6, 'assistant'),
-- (9, 'entrepriseExt'),
-- (10, 'serviceCom');

--  Rappel des id des options
-- (1, 'LD'),
-- (2, 'SE'),
-- (3, 'OC'),
-- (4, 'BIO'),
-- (5, 'NRJ'),
-- (6, 'CC'),
-- (7, 'IIT'),
-- (8, 'DSMT'),
-- (9, 'BD'),
-- (20, 'toutes options');
--
-- INSERTION DES SUJETS
--
INSERT INTO sujet (idSujet, titre, description, nbrMinEleves, nbrMaxEleves, contratPro, confidentialite, etat, liens, interets, noteInteretTechno, noteInteretSujet, idForm) VALUES
 (1, 'Prothèse d’articulation contrôlée', 'En vue du récent développement d’une nouvelle prothèse d’articulation, de nombreuses idées d’améliorations ont vu le jour.Entre autres, la possibilité de la contrôler via une application. Il faudrait alors étudier les composants à ajouter dans cette prothèse afin de donner un bilan de faisabilité pour le CHU d’Angers.', 0, 99, NULL, NULL, 'depose', 'http://www.nouvo.ch/2014/08/ma-prothèse-est-connectée', NULL, NULL, NULL, NULL),
 (2, 'ECG', 'De nombreuses interférences existent lorsque l’on veut l’ECG d’une personne portant un pacemaker. Ces interférences ont notamment lieu à cause du bruit parasite émis par le pacemaker.L’objectif est de mettre en place une solution (ou un début de solution) afin d’améliorer les ECG qui pourraient alors gérer et éliminer ces bruits.', 0, 99, NULL, NULL, 'depose', 'http://pacingdefibrillation.com/fr/comment/143', NULL, NULL, NULL, NULL),
 (3, 'Éolienne, panneaux solaires et roues hydrauliques pour tout un village', ' Certains villages situés dans les Pyrénées parviennent difficilement à avoir un apport en électricité constant. En effet, leur localisation rend difficile l’approvisionnement par les grandes entreprises françaises et l’hiver, ils se retrouvent (trop) régulièrement sans électricité. Il faudrait penser à une solution utilisant des ressources renouvelables qui pourraient charger des générateurs de secours, utilisés en cas d’incidents.', 0, 99, NULL, NULL, 'valide', 'http://sigesmpy.brgm.fr/spip.php?article36', NULL, NULL, NULL, NULL),
 (4, 'Puce de localisation basse consommation', 'Pouvoir localiser ses animaux devient de plus en plus populaire mais les puces doivent être activées par un signal pour transmettre leur position. L’idée est de penser une nouvelle puce qui émettrait en continue pour prévenir l’utilisateur lorsque son animal sort d’un périmètre précis (jardin, …) ou s’éloigne à plus d’une certaine distance de lui. Cependant, cette puce devra consommer au minimum d’énergie. ', 0, 99, NULL, NULL, 'depose', 'https://www.petpointer.ch/fr/', NULL, NULL, NULL, NULL),
 (5, 'Langage GO et intelligence artificielle', 'Face à l’affluence de demandes de coachs sportifs portatifs, la société xxx développe actuellement une nouvelle application intégrant de l’intelligence artificielle. Le coach virtuel proposé sera alors personnalisable en continue en fonction d’un algorithme particulier. De nouvelles technologies sont déployées pour mener à bien ce projet et entre autres, le Go lié à l’intelligence artificielle.', 0, 99, NULL, NULL, 'depose', 'http://www.body-op.com', NULL, NULL, NULL, NULL),
 (6, 'PROSE et réalité augmentée', ' ', 0, 99, NULL, NULL, 'attribue', 'http://annecyelectronique.fr/realite-virtuelle-immersive/', NULL, NULL, NULL, NULL),
 (7, 'Traverser l’argile', 'Lors de fouilles concernant de vieux bâtiments historiques, les chercheurs ont du mal à savoir ce qui se situe à l’intérieur des constructions. En effet, elles sont entièrement closes par de l’argile et leurs émetteurs d’ondes ne permettent pas encore de percevoir ce qu’il y a à l’intérieur sans détruire la structure en argile. Il faut alors travailler sur des filtres et de la modulation pour obtenir de nouvelles ondes.', 0, 99, NULL, NULL, 'depose', 'http://www.normalesup.org/~clanglois/Sciences_Terre/Argiles/Argiles0.html', NULL, NULL, NULL, NULL),
 (8, 'Amélioration de l’architecture réseau de l’ESEO', ' De nombreux problèmes de connexion et de charge ont lieu sur le réseau de l’ESEO, notamment pour la connexion sur le portail qui bloque au-delà d’un certain nombre de connexions. Il faut repenser l’architecture réseau mise en place il y a quelques années par l’ESEO en prenant en compte les contraintes énoncées par l’équipe de gestion informatique de l’école.', 0, 99, NULL, NULL, 'depose', 'http://CHU.com', NULL, NULL, NULL, NULL),
 (10, 'Projet Alpha', 'Le projet alpha est un projet qui a été soumis à but de test', 2, 5, 0, 1, 'depose', '', 'Gestion de projet', NULL, NULL, 1),
 (11, 'Projet beta', 'La description du projet BETA', 1, 10, 1, 1, 'depose', 'unlien.fr', '', NULL, NULL, 2),
 (12, 'TEST SUBJECT', 'TEST DESC', 1, 2, 0, 0, 'depose', '', '', NULL, NULL, 3),
 (13, 'SUJET2', 'DESVC2', 3, 4, 0, 0, 'depose', '', '', NULL, NULL, 4),
 (14, 'SUJET', 'DESCRIPTION', 1, 5, 0, 0, 'depose', 'lien', '', NULL, NULL, 5),
 (15, 'Un nouveau sujet', 'Sujet de test', 2, 4, 1, 0, 'depose', '', '', NULL, NULL, 6);

--
-- INSERTION DES SUJETS
--
INSERT INTO optionsujets (idOption, idSujet) VALUES
 (1, 1);

--
-- INSERTION DES ANNEES SCOLAIRES
--
INSERT INTO AnneeScolaire (anneeDebut, anneeFin, dateDebutProjet, dateMiAvancement, dateDepotPoster, dateSoutenanceFinale, idOption)
VALUES (2017, 2018, '2017-09-01 01:00:00', '2017-12-12 01:00:00', '2018-03-11 01:00:00', '2018-05-01 01:00:00', 1);
INSERT INTO AnneeScolaire (anneeDebut, anneeFin, dateDebutProjet, dateMiAvancement, dateDepotPoster, dateSoutenanceFinale, idOption)
VALUES (2017, 2018, '2017-09-01 01:00:00', '2018-01-01 01:00:00', '2018-06-01 01:00:00', '2018-06-25 01:00:00', 2);
INSERT INTO AnneeScolaire (anneeDebut, anneeFin, dateDebutProjet, dateMiAvancement, dateDepotPoster, dateSoutenanceFinale, idOption)
VALUES (2017, 2018, '2017-09-01 01:00:00', '2018-01-01 01:00:00', '2018-06-01 01:00:00', '2018-06-25 01:00:00', 3);
INSERT INTO AnneeScolaire (anneeDebut, anneeFin, dateDebutProjet, dateMiAvancement, dateDepotPoster, dateSoutenanceFinale, idOption)
VALUES (2017, 2018, '2017-09-01 01:00:00', '2018-01-01 01:00:00', '2018-06-01 01:00:00', '2018-06-25 01:00:00', 4);
INSERT INTO AnneeScolaire (anneeDebut, anneeFin, dateDebutProjet, dateMiAvancement, dateDepotPoster, dateSoutenanceFinale, idOption)
VALUES (2017, 2018, '2017-09-01 01:00:00', '2018-01-01 01:00:00', '2018-06-01 01:00:00', '2018-06-25 01:00:00', 5);
INSERT INTO AnneeScolaire (anneeDebut, anneeFin, dateDebutProjet, dateMiAvancement, dateDepotPoster, dateSoutenanceFinale, idOption)
VALUES (2017, 2018, '2017-09-01 01:00:00', '2018-01-01 01:00:00', '2018-06-01 01:00:00', '2018-06-25 01:00:00', 6);
INSERT INTO AnneeScolaire (anneeDebut, anneeFin, dateDebutProjet, dateMiAvancement, dateDepotPoster, dateSoutenanceFinale, idOption)
VALUES (2017, 2018, '2017-09-01 01:00:00', '2018-01-01 01:00:00', '2018-06-01 01:00:00', '2018-06-25 01:00:00', 7);
INSERT INTO AnneeScolaire (anneeDebut, anneeFin, dateDebutProjet, dateMiAvancement, dateDepotPoster, dateSoutenanceFinale, idOption)
VALUES (2017, 2018, '2017-09-01 01:00:00', '2018-01-01 01:00:00', '2018-06-01 01:00:00', '2018-06-25 01:00:00', 8);


--
-- INSERTION DE L'UTILISATEUR 'TEST'
--
INSERT INTO Utilisateur (idUtilisateur, nom, prenom, identifiant, hash, email, valide)
VALUES (2, 'test', 'test', 'test', '$2a$10$IWaV3IPZ4h2IvTFIU4xn7eJKg4lOqzgXMKLFbi0s2vuEGin5ojsLa', 'test@test.fr', 'oui');

--
-- INSERTION DES ROLES UTILISATEUR
--
INSERT INTO RoleUtilisateur (idUtilisateur, idRole, idOption) VALUES (2, 1, 20);
INSERT INTO RoleUtilisateur (idUtilisateur, idRole, idOption) VALUES (2, 2, 20);
INSERT INTO RoleUtilisateur (idUtilisateur, idRole, idOption) VALUES (2, 3, 20);
INSERT INTO RoleUtilisateur (idUtilisateur, idRole, idOption) VALUES (2, 4, 20);
INSERT INTO RoleUtilisateur (idUtilisateur, idRole, idOption) VALUES (2, 5, 20);
INSERT INTO RoleUtilisateur (idUtilisateur, idRole, idOption) VALUES (2, 6, 20);
INSERT INTO RoleUtilisateur (idUtilisateur, idRole, idOption) VALUES (2, 9, 20);
INSERT INTO RoleUtilisateur (idUtilisateur, idRole, idOption) VALUES (2, 10, 20);

--
-- INSERTION DES AUTRES UTILISATEURS ET DE LEURS ROLES
--
INSERT INTO Utilisateur (idUtilisateur, nom, prenom, identifiant, hash, email, valide)
VALUES (3, 'prof', 'prof', 'prof', '$2a$10$IWaV3IPZ4h2IvTFIU4xn7eJKg4lOqzgXMKLFbi0s2vuEGin5ojsLa', 'prof@reseau.eseo.fr', 'oui');
INSERT INTO RoleUtilisateur (idUtilisateur, idRole, idOption) VALUES (3, 3, 20);

INSERT INTO Utilisateur (nom, prenom, identifiant, hash, email, valide)
VALUES ('profOption', 'profOption', 'profOption', '$2a$10$IWaV3IPZ4h2IvTFIU4xn7eJKg4lOqzgXMKLFbi0s2vuEGin5ojsLa', 'profoption@reseau.eseo.fr', 'oui');
INSERT INTO RoleUtilisateur (idUtilisateur, idRole, idOption) VALUES (4, 3, 20);
INSERT INTO RoleUtilisateur (idUtilisateur, idRole, idOption) VALUES (4, 4, 20);

INSERT INTO Utilisateur (nom, prenom, identifiant, hash, email, valide)
VALUES ('profRespOption', 'profRespOption', 'profRespOption', '$2a$10$IWaV3IPZ4h2IvTFIU4xn7eJKg4lOqzgXMKLFbi0s2vuEGin5ojsLa', 'profrespoption@reseau.eseo.fr', 'oui');
INSERT INTO RoleUtilisateur (idUtilisateur, idRole, idOption) VALUES (5, 5, 1);

INSERT INTO Utilisateur (nom, prenom, identifiant, hash, email, valide)
VALUES ('etudiant', 'etudiant', 'etudiant', '$2a$10$ctTt2vdEBHsAXYhJf/tzqO6ZjLI0TuwO.a1Qu2cLbkPPL9fmyQB6K', 'etudiant@reseau.eseo.fr', 'oui');
INSERT INTO RoleUtilisateur (idUtilisateur, idRole, idOption) VALUES (6, 1, 1);
INSERT INTO Etudiant(idEtudiant, annee) VALUES(6, 2017);

INSERT INTO Utilisateur (nom, prenom, identifiant, hash, email, valide)
VALUES ('assistant', 'assistant', 'assistant', '$2a$10$IWaV3IPZ4h2IvTFIU4xn7eJKg4lOqzgXMKLFbi0s2vuEGin5ojsLa', 'assistant@reseau.eseo.fr', 'oui');
INSERT INTO RoleUtilisateur (idUtilisateur, idRole, idOption) VALUES (7, 6, 20);

INSERT INTO Utilisateur (nom, prenom, identifiant, hash, email, valide)
VALUES ('entreprise', 'entreprise', 'entreprise', '$2a$10$IWaV3IPZ4h2IvTFIU4xn7eJKg4lOqzgXMKLFbi0s2vuEGin5ojsLa', 'entreprise@reseau.eseo.fr', 'oui');

INSERT INTO Utilisateur (nom, prenom, identifiant, hash, email, valide)
VALUES ('ALBERS', 'Patrick', 'Prof1', '$2a$10$IWaV3IPZ4h2IvTFIU4xn7eJKg4lOqzgXMKLFbi0s2vuEGin5ojsLa', 'Prof1@reseau.eseo.fr', 'oui');
INSERT INTO RoleUtilisateur (idUtilisateur, idRole, idOption) VALUES (9, 3, 1);

INSERT INTO Utilisateur (nom, prenom, identifiant, hash, email, valide)
VALUES ('BEAUDOUX', 'Olivier', 'Prof2', '$2a$10$IWaV3IPZ4h2IvTFIU4xn7eJKg4lOqzgXMKLFbi0s2vuEGin5ojsLa', 'Prof2@reseau.eseo.fr', 'oui');
INSERT INTO RoleUtilisateur (idUtilisateur, idRole, idOption) VALUES (10, 3, 1);

INSERT INTO Utilisateur (nom, prenom, identifiant, hash, email, valide)
VALUES ('CAMP', 'Olivier', 'Prof3', '$2a$10$IWaV3IPZ4h2IvTFIU4xn7eJKg4lOqzgXMKLFbi0s2vuEGin5ojsLa', 'Prof3@reseau.eseo.fr', 'oui');
INSERT INTO RoleUtilisateur (idUtilisateur, idRole, idOption) VALUES (11, 3, 1);

INSERT INTO Utilisateur (nom, prenom, identifiant, hash, email, valide)
VALUES ('CHHEL', 'Fabien', 'Prof8', '$2a$10$IWaV3IPZ4h2IvTFIU4xn7eJKg4lOqzgXMKLFbi0s2vuEGin5ojsLa', 'Prof8@reseau.eseo.fr', 'oui');
INSERT INTO RoleUtilisateur (idUtilisateur, idRole, idOption) VALUES (12, 3, 1);

INSERT INTO Utilisateur (nom, prenom, identifiant, hash, email, valide)
VALUES ('CLAVREUL', 'Mickael', 'Prof9', '$2a$10$IWaV3IPZ4h2IvTFIU4xn7eJKg4lOqzgXMKLFbi0s2vuEGin5ojsLa', 'Prof9@reseau.eseo.fr', 'oui');
INSERT INTO RoleUtilisateur (idUtilisateur, idRole, idOption) VALUES (13, 3, 1);

INSERT INTO Utilisateur (nom, prenom, identifiant, hash, email, valide)
VALUES ('GUTOWSKI', 'Nicolas', 'Prof4', '$2a$10$IWaV3IPZ4h2IvTFIU4xn7eJKg4lOqzgXMKLFbi0s2vuEGin5ojsLa', 'Prof4@reseau.eseo.fr', 'oui');
INSERT INTO RoleUtilisateur (idUtilisateur, idRole, idOption) VALUES (14, 3, 1);

INSERT INTO Utilisateur (nom, prenom, identifiant, hash, email, valide)
VALUES ('HAMMOUDI', 'Slimane', 'Prof5', '$2a$10$IWaV3IPZ4h2IvTFIU4xn7eJKg4lOqzgXMKLFbi0s2vuEGin5ojsLa', 'Prof5@reseau.eseo.fr', 'oui');
INSERT INTO RoleUtilisateur (idUtilisateur, idRole, idOption) VALUES (15, 3, 1);

INSERT INTO Utilisateur (nom, prenom, identifiant, hash, email, valide)
VALUES ('ROUSSEAU', 'Sophie', 'Prof6', '$2a$10$IWaV3IPZ4h2IvTFIU4xn7eJKg4lOqzgXMKLFbi0s2vuEGin5ojsLa', 'Prof6@reseau.eseo.fr', 'oui');
INSERT INTO RoleUtilisateur (idUtilisateur, idRole, idOption) VALUES (16, 3, 1);

INSERT INTO Utilisateur (nom, prenom, identifiant, hash, email, valide)
VALUES ('SCHANG', 'Daniel', 'Prof7', '$2a$10$IWaV3IPZ4h2IvTFIU4xn7eJKg4lOqzgXMKLFbi0s2vuEGin5ojsLa', 'Prof7@reseau.eseo.fr', 'oui');
INSERT INTO RoleUtilisateur (idUtilisateur, idRole, idOption) VALUES (17, 3, 1);

INSERT INTO Utilisateur (nom, prenom, identifiant, hash, email, valide)
VALUES ('Etu1', 'Etu1', 'Etu1', '$2a$10$IWaV3IPZ4h2IvTFIU4xn7eJKg4lOqzgXMKLFbi0s2vuEGin5ojsLa', 'Etu1@reseau.eseo.fr', 'oui');
INSERT INTO RoleUtilisateur (idUtilisateur, idRole, idOption) VALUES (18, 1, 1);

INSERT INTO Utilisateur (nom, prenom, identifiant, hash, email, valide)
VALUES ('Etu2', 'Etu2', 'Etu2', '$2a$10$IWaV3IPZ4h2IvTFIU4xn7eJKg4lOqzgXMKLFbi0s2vuEGin5ojsLa', 'Etu2@reseau.eseo.fr', 'oui');
INSERT INTO RoleUtilisateur (idUtilisateur, idRole, idOption) VALUES (19, 1, 1);

INSERT INTO Utilisateur (nom, prenom, identifiant, hash, email, valide)
VALUES ('Etu3', 'Etu3', 'Etu3', '$2a$10$IWaV3IPZ4h2IvTFIU4xn7eJKg4lOqzgXMKLFbi0s2vuEGin5ojsLa', 'Etu3@reseau.eseo.fr', 'oui');
INSERT INTO RoleUtilisateur (idUtilisateur, idRole, idOption) VALUES (20, 1, 1);

INSERT INTO Utilisateur (nom, prenom, identifiant, hash, email, valide)
VALUES ('Etu4', 'Etu4', 'Etu4', '$2a$10$IWaV3IPZ4h2IvTFIU4xn7eJKg4lOqzgXMKLFbi0s2vuEGin5ojsLa', 'Etu4@reseau.eseo.fr', 'oui');
INSERT INTO RoleUtilisateur (idUtilisateur, idRole, idOption) VALUES (21, 1, 1);

INSERT INTO Utilisateur (nom, prenom, identifiant, hash, email, valide)
VALUES ('Etu5', 'Etu5', 'Etu5', '$2a$10$IWaV3IPZ4h2IvTFIU4xn7eJKg4lOqzgXMKLFbi0s2vuEGin5ojsLa', 'Etu5@reseau.eseo.fr', 'oui');
INSERT INTO RoleUtilisateur (idUtilisateur, idRole, idOption) VALUES (22, 1, 1);

INSERT INTO Utilisateur (nom, prenom, identifiant, hash, email, valide)
VALUES ('Etu6', 'Etu6', 'Etu6', '$2a$10$IWaV3IPZ4h2IvTFIU4xn7eJKg4lOqzgXMKLFbi0s2vuEGin5ojsLa', 'Etu6@reseau.eseo.fr', 'oui');
INSERT INTO RoleUtilisateur (idUtilisateur, idRole, idOption) VALUES (23, 1, 1);

INSERT INTO `utilisateur` (`idUtilisateur`, `nom`, `prenom`, `identifiant`, `hash`, `email`, `valide`) 
VALUES (24, 'PRODUCT', 'Owner', 'productOwner', '$2a$10$IWaV3IPZ4h2IvTFIU4xn7eJKg4lOqzgXMKLFbi0s2vuEGin5ojsLa', 'product.owner@reseau.eseo.fr', 'oui');
INSERT INTO RoleUtilisateur (idUtilisateur, idRole, idOption) VALUES (24, 12, 1);

ALTER TABLE utilisateur AUTO_INCREMENT=100;

INSERT INTO utilisateur(idUtilisateur, nom, prenom, identifiant, hash, email, valide)
VALUES (100,'Vergote','Anne-Claire','Vergote','$2a$10$IWaV3IPZ4h2IvTFIU4xn7eJKg4lOqzgXMKLFbi0s2vuEGin5ojsLa','ac.ver@gmail.com','oui');
INSERT INTO `RoleUtilisateur` (`idUtilisateur`, `idRole`, `idOption`) VALUES ('100', '1', '1');

INSERT INTO utilisateur(idUtilisateur, nom, prenom, identifiant, hash, email, valide)
VALUES (101,'Pichavant','Quentin','Pichavant','$2a$10$IWaV3IPZ4h2IvTFIU4xn7eJKg4lOqzgXMKLFbi0s2vuEGin5ojsLa','pichavent@gmail.com','oui');
INSERT INTO `RoleUtilisateur` (`idUtilisateur`, `idRole`, `idOption`) VALUES ('101', '1', '1');

INSERT INTO utilisateur(idUtilisateur, nom, prenom, identifiant, hash, email, valide)
VALUES (102,'Legacque','Tristan','Legaque','$2a$10$IWaV3IPZ4h2IvTFIU4xn7eJKg4lOqzgXMKLFbi0s2vuEGin5ojsLa','tristan@gmail.com','oui');
INSERT INTO `RoleUtilisateur` (`idUtilisateur`, `idRole`, `idOption`) VALUES ('102', '1', '1')

INSERT INTO utilisateur(idUtilisateur, nom, prenom, identifiant, hash, email, valide)
VALUES (103,'Scuiller','Gaetan','Scuiller','$2a$10$IWaV3IPZ4h2IvTFIU4xn7eJKg4lOqzgXMKLFbi0s2vuEGin5ojsLa','quentin@gmail.com','oui');
INSERT INTO `RoleUtilisateur` (`idUtilisateur`, `idRole`, `idOption`) VALUES ('103', '1', '1');

INSERT INTO utilisateur(idUtilisateur, nom, prenom, identifiant, hash, email, valide)
VALUES (104,'olivier','camp','campo','$2a$10$IWaV3IPZ4h2IvTFIU4xn7eJKg4lOqzgXMKLFbi0s2vuEGin5ojsLa','olivier.camp@gmail.com','oui');

INSERT INTO utilisateur(idUtilisateur, nom, prenom, identifiant, hash, email, valide)
VALUES (105,'Mamalet','emilien','emilienma','$2a$10$IWaV3IPZ4h2IvTFIU4xn7eJKg4lOqzgXMKLFbi0s2vuEGin5ojsLa','emilien.mamalet@gmail.com','oui');
INSERT INTO `RoleUtilisateur` (`idUtilisateur`, `idRole`, `idOption`) VALUES ('105', '1', '1');

INSERT INTO utilisateur(idUtilisateur, nom, prenom, identifiant, hash, email, valide)
VALUES (106,'Maymard','Damien','maymard','$2a$10$IWaV3IPZ4h2IvTFIU4xn7eJKg4lOqzgXMKLFbi0s2vuEGin5ojsLa','damien.maymard@reseau.eseo.fr','oui');
INSERT INTO RoleUtilisateur (idUtilisateur, idRole, idOption) VALUES (106, 1, 1);

INSERT INTO utilisateur(idUtilisateur, nom, prenom, identifiant, hash, email, valide)
VALUES (107,'Ortiz Betancur','Juan Carlo','ortiz','$2a$10$IWaV3IPZ4h2IvTFIU4xn7eJKg4lOqzgXMKLFbi0s2vuEGin5ojsLa','juancarlo.ortizbetancur@reseau.eseo.fr','oui');
INSERT INTO RoleUtilisateur (idUtilisateur, idRole, idOption) VALUES (107, 1, 1);

INSERT INTO utilisateur(idUtilisateur, nom, prenom, identifiant, hash, email, valide)
VALUES (108,'Jarneau','Dimitri','jarneau','$2a$10$IWaV3IPZ4h2IvTFIU4xn7eJKg4lOqzgXMKLFbi0s2vuEGin5ojsLa','dimitri.jarneau@reseau.eseo.fr','oui');
INSERT INTO RoleUtilisateur (idUtilisateur, idRole, idOption) VALUES (108, 1, 1);

INSERT INTO utilisateur(idUtilisateur, nom, prenom, identifiant, hash, email, valide)
VALUES (109,'WOODWARD','Richard','woodward','$2a$10$IWaV3IPZ4h2IvTFIU4xn7eJKg4lOqzgXMKLFbi0s2vuEGin5ojsLa','WOODWARD.richard@gmail.com','oui');
INSERT INTO `RoleUtilisateur` (`idUtilisateur`, `idRole`, `idOption`) VALUES ('109', '13', '1');

--
-- INSERTION DES EQUIPES
--
INSERT INTO equipe (idEquipe, annee, taille, idSujet, valide) VALUES ('', '2017', '2', '1', 'oui');

--
-- INSERTION DES PROFESSEURS SUJETS
--
INSERT INTO porteursujet (idSujet, idUtilisateur, annee) VALUES
 (1, 2, NULL);

INSERT INTO ProfesseurSujet(idProfesseur, idSujet, fonction, valide)
VALUES (2, 4, 'référent', 'oui');

INSERT INTO ProfesseurSujet(idProfesseur, idSujet, fonction, valide)
VALUES (3, 6, 'référent', 'oui');

INSERT INTO ProfesseurSujet(idProfesseur, idSujet, fonction, valide)
VALUES (4, 10, 'référent', 'non');

INSERT INTO professeursujet (idProfesseur, idSujet, fonction, valide) VALUES
 (17, 1, 'co-encadrant', 'oui');


--
-- INSERTION DES POSTERS
--
INSERT INTO poster (idPoster, chemin, datePoster, idSujet, valide, idEquipe) VALUES
 (1, 'aucun', '2018-04-18 00:00:00', 1, 'oui', '2017_001');




-- INSERTION PROF REFERENT
INSERT INTO `professeur`(`idProfesseur`) VALUES (103);


-- ANNEE DEJA INSEREE POUR OPTION LD AVEC ID=1

-- INSERTION SPRINT
INSERT INTO `pgl_sprint`(`idSprint`, `numero`, `dateDebut`, `dateFin`, `nbrHeures`, `coefficient`, `refAnnee`) VALUES
 (01, 1, '2018-02-01', '2018-04-01', 24, 1, 1),
 (02, 2, '2018-04-02', '2018-05-01', 30, 2, 1),
 (03, 3, '2018-05-02', '2018-06-01', 40, 3, 1);


-- INSERTION PROJET EQUIPE BETA
INSERT INTO `pgl_projet`(`idProjet`, `titre`, `description`, `idProfReferent`) VALUES (1,'Base de donnees relationnelles','projet ld',103);

-- INSERTION PROJET EQUIPE ALPHA
INSERT INTO `pgl_projet`(`idProjet`, `titre`, `description`, `idProfReferent`) VALUES (2,'la qualité logiciel','projet de test',103);

-- INSERTION EQUIPE BETA
INSERT INTO `pgl_equipe`(`idEquipe`, `nom`, `refProjet`, `refAnnee`) VALUES (1,'Alpha',1,1);

-- INSERTION EQUIPE ALPHA
INSERT INTO `pgl_equipe`(`idEquipe`, `nom`, `refProjet`, `refAnnee`) VALUES (2,'Beta',2,1);

-- INSERTION ETUDIANT EQUIPE BETA
INSERT INTO `pgl_etudiant`(`idEtudiant`, `refAnnee`) VALUES (100,1);
INSERT INTO `pgl_etudiant`(`idEtudiant`, `refAnnee`) VALUES (101,1);
INSERT INTO `pgl_etudiant`(`idEtudiant`, `refAnnee`) VALUES (105,1);

-- INSERTION ETUDIANT EQUIPE ALPHA
INSERT INTO `pgl_etudiant`(`idEtudiant`, `refAnnee`) VALUES (102,1);
INSERT INTO `pgl_etudiant`(`idEtudiant`, `refAnnee`) VALUES (103,1);

-- INSERTION DANS LES EQUIPES
INSERT INTO pgl_etudiantequipe(idEtudiant, idEquipe, idSprint) VALUES
  (100, 1, 1),
  (101, 1, 1),
  (102, 2, 1),
  (103, 2, 1);

-- INSERTION MATIERE Power Point pour le sprint 1
INSERT INTO `pgl_matiere`(`idMatiere`, `libelle`, `coefficient`, `refSprint`) VALUES (7, 'power point',1, 1);

-- INSERTION MATIERE Presentation pour le sprint 1
INSERT INTO `pgl_matiere`(`idMatiere`, `libelle`, `coefficient`, `refSprint`) VALUES (8, 'presentation',1, 1);

-- INSERTION MATIERE Note technique pour le sprint 1
INSERT INTO `pgl_matiere`(`idMatiere`, `libelle`, `coefficient`, `refSprint`) VALUES (9, 'note technique',3, 1);



-- INSERTION BONUS MALUS

INSERT INTO `pgl_bonusmalus`(`idBonusMalus`, `valeur`, `validation`,  `justification`, `refSprint`, `refEtudiant`, `refEvaluateur`) VALUES (1, 0, null, 'attente', 1, 100, 100);

INSERT INTO `pgl_bonusmalus`(`idBonusMalus`, `valeur`, `validation`,  `justification`, `refSprint`, `refEtudiant`, `refEvaluateur`) VALUES (2, 0, null, 'refuser', 1, 101, 100);

INSERT INTO `pgl_bonusmalus`(`idBonusMalus`, `valeur`, `validation`,  `justification`, `refSprint`, `refEtudiant`, `refEvaluateur`) VALUES (3, 0, null, 'attente', 1, 102, 102);

INSERT INTO `pgl_bonusmalus`(`idBonusMalus`, `valeur`, `validation`,  `justification`, `refSprint`, `refEtudiant`, `refEvaluateur`) VALUES (4, 0, null, 'accepter', 1, 103, 102);

INSERT INTO `pgl_bonusmalus`(`idBonusMalus`, `valeur`, `validation`,  `justification`, `refSprint`, `refEtudiant`, `refEvaluateur`) VALUES (5, 0, null, 'refuser', 1, 105, 100);



-- ROLEEQUIPE

INSERT INTO `pgl_roleequipe`(`refEquipe`, `refUtilisateur`, `refRole`) VALUES (1,100,11);
INSERT INTO `pgl_roleequipe`(`refEquipe`, `refUtilisateur`, `refRole`) VALUES (1,103,11);

--
-- Contenu de la table `pgl_soutenance`
--

INSERT INTO `pgl_soutenance` (`idSoutenance`, `date`, `libelle`) VALUES
(1, '2018-02-09 08:00:00', 'SprintReview'),
(2, '2018-04-18 08:08:00', 'SprintReview'),
(3, '2018-05-05 15:00:00', 'SprintReview');

--
-- Contenu de la table `pgl_compositionjury`
--

INSERT INTO `pgl_compositionjury` (`refSoutenance`, `refProfesseur`) VALUES
(1, 9),
(1, 10),
(1, 11),
(1, 12),
(2, 13),
(2, 14),
(2, 15),
(2, 16),
(3, 17);


INSERT INTO `RoleUtilisateur`(`idUtilisateur`, `idRole`, `idOption`) VALUES (2,11,20);
INSERT INTO `RoleUtilisateur`(`idUtilisateur`, `idRole`, `idOption`) VALUES (2,12,20);
INSERT INTO `RoleUtilisateur`(`idUtilisateur`, `idRole`, `idOption`) VALUES (2,13,20);
INSERT INTO `RoleUtilisateur`(`idUtilisateur`, `idRole`, `idOption`) VALUES (104,13,20);
INSERT INTO `RoleUtilisateur`(`idUtilisateur`, `idRole`, `idOption`) VALUES (104,12,20);

INSERT INTO `pgl_etudiantequipe`(`idEtudiant`, `idEquipe`, `idSprint`) VALUES (105,1,1);

-- Insertion de notes
INSERT INTO pgl_note(note, refMatiere, refEtudiant, refEvaluateur) VALUES
 (12, 1, 100, 105),
 (14, 2, 100, 105),
 (1, 4, 100, 105),
 (1, 3, 101, 104),
 (18, 1, 102, 103),
 (13.2, 3, 102, 103);
 
 -- Insertion Equipes PFE
 INSERT INTO `equipe` (`annee`, `taille`, `idSujet`, `valide`) VALUES
(2017, 2, 2, 'oui'),
(2017, 2, 3, 'oui'),
(2017, 2, 4, 'oui'),
(2017, 2, 5, 'oui'),
(2017, 2, 6, 'oui'),
(2017, 2, 7, 'oui'),
(2017, 2, 8, 'oui'),
(2017, 2, 10, 'oui'),
(2017, 2, 11, 'oui');


-- Insertion Poster
INSERT INTO `poster` (`chemin`, `datePoster`, `idSujet`, `valide`, `idEquipe`) VALUES
('/home/etudiant/Posters/2018/1_architecte_logiciels_et_does.pdf', '2018-05-18 16:47:56', 2, 'oui', '2017_002'),
('/home/etudiant/Posters/2018/1_architecte_logicls_et_does.pdf', '2018-05-15 16:47:56', 3, 'oui', '2017_003'),
('/home/etudiant/Posters/2018/1_architecte_logicls_et_does.pdf', '2018-05-15 16:47:56', 4, 'oui', '2017_004'),
('/home/etudiant/Posters/2018/1_architecte_logicls_et_does.pdf', '2018-05-15 16:47:56', 5, 'oui', '2017_005'),
('/home/etudiant/Posters/2018/1_architecte_logicls_et_does.pdf', '2018-05-15 16:47:56', 6, 'oui', '2017_006'),
('/home/etudiant/Posters/2018/1_architecte_logicls_et_does.pdf', '2018-05-15 16:47:56', 7, 'oui', '2017_007'),
('/home/etudiant/Posters/2018/1_architecte_logicls_et_does.pdf', '2018-05-15 16:47:56', 8, 'oui', '2017_008'),
('/home/etudiant/Posters/2018/1_architecte_logicls_et_does.pdf', '2018-05-15 16:47:56', 10, 'oui', '2017_009');