USE somanager;

SET default_storage_engine=INNODB;

/*Structure de la table Pgl Sprint*/
CREATE TABLE IF NOT EXISTS pgl_sprint (
  idSprint INTEGER AUTO_INCREMENT,
  numero TINYINT NOT NULL,
  dateDebut DATE NOT NULL,
  dateFin DATE NOT NULL,
  nbrHeures INTEGER DEFAULT NULL,
  coefficient FLOAT DEFAULT 1.0,
  refAnnee INTEGER NOT NULL,
  notesPubliees BOOLEAN DEFAULT FALSE,

  PRIMARY KEY(idSprint),
  FOREIGN KEY (refAnnee) REFERENCES AnneeScolaire(idAnneeScolaire) ON DELETE CASCADE
);

/*Structure de la table Matière*/
CREATE TABLE IF NOT EXISTS pgl_matiere (
  idMatiere INTEGER AUTO_INCREMENT,
  libelle VARCHAR(40) NOT NULL,
  coefficient FLOAT NOT NULL DEFAULT 1.0,
  refSprint INTEGER NOT NULL,
  PRIMARY KEY (idMatiere),
  FOREIGN KEY (refSprint) REFERENCES pgl_sprint(idSprint) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS pgl_projet(
  idProjet int(5) NOT NULL AUTO_INCREMENT,
  titre varchar(30) DEFAULT NULL,
  description varchar(80) DEFAULT NULL,
  idProfReferent int(5),
  PRIMARY KEY (idProjet),
  FOREIGN KEY (idProfReferent) REFERENCES Professeur(idProfesseur)
);

CREATE TABLE IF NOT EXISTS pgl_soutenance(
  idSoutenance int(5) NOT NULL AUTO_INCREMENT,
  date datetime DEFAULT NULL,
  libelle varchar(30) DEFAULT NULL,
  PRIMARY KEY (idSoutenance)
);

CREATE TABLE IF NOT EXISTS pgl_compositionjury(
  refSoutenance int(5) NOT NULL,
  refProfesseur int(5) NOT NULL,
  PRIMARY KEY (refSoutenance,refProfesseur),
  FOREIGN KEY (refSoutenance) REFERENCES pgl_soutenance(idSoutenance) ON DELETE CASCADE,
  FOREIGN KEY (refProfesseur) REFERENCES Professeur(idProfesseur) ON DELETE CASCADE
);

/*Structure de la table Equipe*/
CREATE TABLE IF NOT EXISTS pgl_equipe(
  idEquipe INTEGER AUTO_INCREMENT,
  nom VARCHAR(10) NOT NULL,
  refProjet INTEGER DEFAULT NULL,
  refAnnee INTEGER NOT NULL,
  PRIMARY KEY(idEquipe),
  FOREIGN KEY(refProjet) REFERENCES pgl_projet(idProjet),
  FOREIGN KEY(refAnnee) REFERENCES anneescolaire(idAnneeScolaire) ON DELETE CASCADE
);

/*Structure de la table Etudiant*/
CREATE TABLE IF NOT EXISTS pgl_etudiant (
  idEtudiant INTEGER,
  refAnnee INTEGER DEFAULT NULL,
  PRIMARY KEY(idEtudiant),
  FOREIGN KEY(refAnnee) REFERENCES anneescolaire(idAnneeScolaire) ON DELETE CASCADE,
  FOREIGN KEY(idEtudiant) REFERENCES utilisateur(idUtilisateur) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS pgl_roleequipe(
  refEquipe INTEGER NOT NULL,
  refUtilisateur INTEGER NOT NULL,
  refRole INTEGER NOT NULL,

  PRIMARY KEY (refEquipe, refUtilisateur, refRole),
  FOREIGN KEY (refEquipe) REFERENCES pgl_equipe(idEquipe) ON DELETE CASCADE,
  FOREIGN KEY (refUtilisateur) REFERENCES utilisateur(idUtilisateur) ON DELETE CASCADE,
  FOREIGN KEY (refRole) REFERENCES role(idRole) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS pgl_evalue(
  refSoutenance int(5) NOT NULL,
  refEquipe int(5) NOT NULL,
  PRIMARY KEY (refSoutenance,refEquipe),
  FOREIGN KEY (refSoutenance) REFERENCES pgl_soutenance(idSoutenance) ON DELETE CASCADE,
  FOREIGN KEY (refEquipe) REFERENCES pgl_equipe(idEquipe) ON DELETE CASCADE
);

/*Structure de la table noteEquipe*/
CREATE TABLE IF NOT EXISTS pgl_note (
  idNote INTEGER AUTO_INCREMENT,
  note FLOAT NOT NULL,
  refMatiere INTEGER NOT NULL,
  refEtudiant INTEGER NOT NULL,
  refEvaluateur INTEGER NOT NULL,
  PRIMARY KEY (idNote),
  FOREIGN KEY (refMatiere) REFERENCES pgl_matiere(idMatiere) ON DELETE CASCADE,
  FOREIGN KEY (refEvaluateur) REFERENCES utilisateur(idUtilisateur) ON DELETE CASCADE,
  FOREIGN KEY (refEtudiant) REFERENCES pgl_etudiant(idEtudiant),
  CONSTRAINT UC_NoteMatiereEvaluateur UNIQUE (refMatiere,refEtudiant,refEvaluateur)
);

/*Structure de la table BonusMalus*/
CREATE TABLE IF NOT EXISTS pgl_bonusmalus(
  idBonusMalus INTEGER AUTO_INCREMENT,
  valeur FLOAT NOT NULL,
  validation ENUM('attente', 'refuser', 'accepter') DEFAULT 'attente',
  justification VARCHAR(250) NOT NULL,
  refSprint INTEGER NOT NULL,
  refEtudiant INTEGER NOT NULL,
  refEvaluateur INTEGER NOT NULL,
  PRIMARY KEY (idBonusMalus),
  FOREIGN KEY(refSprint) REFERENCES pgl_sprint(idSprint) ON DELETE CASCADE,
  FOREIGN KEY(refEtudiant) REFERENCES pgl_etudiant(idEtudiant) ON DELETE CASCADE,
  FOREIGN KEY(refEvaluateur) REFERENCES utilisateur(idUtilisateur)
);

--
-- Structure de la table EtudiantEquipe
--
CREATE TABLE IF NOT EXISTS pgl_etudiantequipe(
  idEtudiant INTEGER NOT NULL,
  idEquipe INTEGER NOT NULL,
  idSprint INTEGER NOT NULL,
  PRIMARY KEY (idEtudiant, idSprint),
  FOREIGN KEY(idEtudiant) REFERENCES pgl_etudiant(idEtudiant) ON DELETE CASCADE,
  FOREIGN KEY(idEquipe) REFERENCES pgl_equipe(idEquipe) ON DELETE CASCADE,
  FOREIGN KEY(idSprint) REFERENCES pgl_sprint(idSprint) ON DELETE CASCADE
);

--
-- Structure de la table `demandejury`
--

CREATE TABLE IF NOT EXISTS `demandejury` (
  `idDemande` int(5) NOT NULL AUTO_INCREMENT,
  `sujetsConcernes` text NOT NULL,
  `commentaire` text,
  `nomOption` enum('LD','SE','OC','BIO','NRJ','CC','IIT','DSMT','BD') NOT NULL,
  PRIMARY KEY (`idDemande`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Structure de la table `professeurdemande`
--

CREATE TABLE IF NOT EXISTS `professeurdemande` (
  `idProfesseur` int(5) NOT NULL,
  `idDemande` int(5) NOT NULL,
  PRIMARY KEY (`idProfesseur`,`idDemande`),
  KEY `ProfesseurDemande_ibfk_2` (`idDemande`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contraintes pour la table `professeurdemande`
--
ALTER TABLE `professeurdemande`
  ADD CONSTRAINT `ProfesseurDemande_ibfk_1` FOREIGN KEY (`idProfesseur`) REFERENCES `professeur` (`idProfesseur`) ON DELETE CASCADE,
  ADD CONSTRAINT `ProfesseurDemande_ibfk_2` FOREIGN KEY (`idDemande`) REFERENCES `demandejury` (`idDemande`) ON DELETE CASCADE;


--
--
-- Trigger pour inserer les matieres par défaut dans les sprints
--
DELIMITER //
CREATE TRIGGER `MatieresDefaut` AFTER INSERT ON pgl_sprint
  FOR EACH ROW BEGIN
  INSERT INTO pgl_matiere(libelle, coefficient, refSprint) VALUES('Soutenance matin', 1, NEW.idSprint);
  INSERT INTO pgl_matiere(libelle, coefficient, refSprint) VALUES('Soutenance après-midi', 1, NEW.idSprint);
END//
DELIMITER ;


--
-- Vue pour les notes moyennes
-- Affiche la moyenne pour chaque matière d'un etudiant, par sprint
--
CREATE OR REPLACE VIEW pgl_moyennesEtudiantsMatieres AS
  SELECT n.refEtudiant, u.nom, u.prenom, m.libelle, s.idSprint, a.anneeDebut, AVG(n.note) note, n.refMatiere, n.refEvaluateur, NULL idNote
  FROM pgl_note n
    JOIN pgl_etudiant e ON n.refEtudiant = e.idEtudiant
    JOIN pgl_matiere m ON m.idMatiere = n.refMatiere
    JOIN utilisateur u ON u.idUtilisateur = e.idEtudiant
    JOIN pgl_sprint s ON s.idSprint = m.refSprint
    JOIN anneescolaire a ON a.idAnneeScolaire = s.refAnnee
  GROUP BY s.idSprint, m.libelle, e.idEtudiant
  ORDER BY n.refMatiere ASC;


--
-- Vue pour les notes moyennes par sprint
-- Affiche la moyenne obtenue par sprint par etudiant
-- Le bonusMalus affiché est DEJA appliqué à la note
--

CREATE OR REPLACE VIEW pgl_moyennesEtudiantsSprint AS
  SELECT pmEM.refEtudiant, s.idSprint, s.numero, CONCAT('Sprint ', s.numero) libelle, s.coefficient, nom, prenom, pmEM.anneeDebut, NULL idNote,
                                                 GREATEST(0, LEAST(20,
                                                                   ((SUM(pmEM.note * m.coefficient) / SUM(m.coefficient) +
                                                                     IFNULL(SUM(IFNULL(bm.valeur, 0)) * COUNT(DISTINCT bm.idBonusMalus) / COUNT(bm.idBonusMalus), 0)
                                                                   ))
                                                 )) note,
                                                 IFNULL((SUM(IFNULL(bm.valeur,0)) * COUNT(DISTINCT bm.idBonusMalus) / COUNT(bm.idBonusMalus)), 0) bonusMalus

  FROM pgl_moyennesEtudiantsMatieres pmEM
    JOIN pgl_matiere m ON m.idMatiere = pmEM.refMatiere
    JOIN pgl_sprint s ON s.idSprint = m.refSprint
    LEFT JOIN pgl_bonusmalus bm ON bm.refEtudiant = pmEM.refEtudiant AND bm.refSprint = s.idSprint
  GROUP BY pmEM.refEtudiant, pmEM.idSprint;

--
-- Vue pour les notes
-- Affiche toutes les notes obtenues par un étudiant
--
CREATE OR REPLACE VIEW pgl_notesEtudiants AS
  SELECT a.idAnneeScolaire, s.idSprint, ee.idEquipe, u.idUtilisateur, u.nom, u.prenom, n.idNote, n.note, m.idMatiere, m.libelle, m.coefficient, eval.idUtilisateur idEvaluateur, s.notesPubliees
  FROM utilisateur u
    JOIN pgl_etudiant e ON u.idUtilisateur = e.idEtudiant
    JOIN anneescolaire a ON a.idAnneeScolaire = e.refAnnee
    JOIN pgl_sprint s ON s.refAnnee = a.idAnneeScolaire
    JOIN pgl_matiere m ON m.refSprint = s.idSprint
    JOIN pgl_etudiantequipe ee ON ee.idEtudiant = e.idEtudiant
    LEFT JOIN pgl_note n ON n.refMatiere = m.idMatiere AND n.refEtudiant = e.idEtudiant
    LEFT JOIN utilisateur eval ON eval.idUtilisateur = n.refEvaluateur
  ORDER BY s.numero, u.idUtilisateur ASC;

--
-- Récupère la liste des évaluateurs qui ont ajoutés une note par matière
--
CREATE OR REPLACE VIEW pgl_matieresEtudiantEvaluees AS
  SELECT a.idAnneeScolaire, s.idSprint, ee.idEquipe, u.idUtilisateur, u.nom, u.prenom, m.idMatiere, m.libelle, s.notesPubliees,m.coefficient, GROUP_CONCAT(eval.idUtilisateur) evaluateurs
  FROM utilisateur u
    JOIN pgl_etudiant e ON u.idUtilisateur = e.idEtudiant
    JOIN anneescolaire a ON a.idAnneeScolaire = e.refAnnee
    JOIN pgl_sprint s ON s.refAnnee = a.idAnneeScolaire
    JOIN pgl_matiere m ON m.refSprint = s.idSprint
    JOIN pgl_etudiantequipe ee ON ee.idEtudiant = e.idEtudiant
    LEFT JOIN pgl_note n ON n.refEtudiant = e.idEtudiant AND n.refMatiere = m.idMatiere
    LEFT JOIN utilisateur eval ON eval.idUtilisateur = n.refEvaluateur
  GROUP BY u.idUtilisateur, m.idMatiere
  ORDER BY s.numero, u.idUtilisateur ASC;

--
-- Procédure pour récupérer les notes notées et à noter pour un évaluateur donné
--
-- DELIMITER $$
-- 
-- CREATE PROCEDURE recupererNotesANoter (IN id_utilisateur INT, IN id_evaluateur INT, IN id_annee INT, IN id_sprint INT, IN id_equipe INT, IN id_matiere INT)
--   BEGIN
--     SELECT idAnneeScolaire, idSprint, idEquipe, idUtilisateur, nom, prenom,	NULL idNote, NULL note, idMatiere, libelle, coefficient, notesPubliees
--     FROM pgl_matieresetudiantevaluees
--     WHERE (idUtilisateur = id_utilisateur OR id_utilisateur IS NULL)
--           AND (idAnneeScolaire = id_annee OR id_annee IS NULL)
--           AND (idSprint = id_sprint OR id_sprint IS NULL)
--           AND (idEquipe = id_equipe OR id_equipe IS NULL)
--           AND (idMatiere = id_matiere OR id_matiere IS NULL)
--           AND (FIND_IN_SET(id_evaluateur, evaluateurs) = 0 OR FIND_IN_SET(id_evaluateur, evaluateurs) IS NULL)
--     UNION
--     SELECT idAnneeScolaire, idSprint, idEquipe, idUtilisateur, nom, prenom,	idNote, note, idMatiere, libelle, coefficient, notesPubliees
--     FROM pgl_notesetudiants
--     WHERE idEvaluateur = id_evaluateur
--           AND (idAnneeScolaire = id_annee OR id_annee IS NULL)
--           AND (idSprint = id_sprint OR id_sprint IS NULL)
--           AND (idEquipe = id_equipe OR id_equipe IS NULL)
--           AND (idMatiere = id_matiere OR id_matiere IS NULL)
--           AND (idUtilisateur = id_utilisateur OR id_utilisateur IS NULL)
--     ORDER BY idUtilisateur ASC;
--   END $$
-- DELIMITER ;
