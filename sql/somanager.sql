-- phpMyAdmin SQL Dump
-- version 4.2.12deb2+deb8u1
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Mar 27 Juin 2017 à 14:47
-- Version du serveur :  5.5.47-0+deb8u1
-- Version de PHP :  5.6.17-0+deb8u1
DROP DATABASE IF EXISTS somanager;

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `somanager`
--
CREATE DATABASE IF NOT EXISTS somanager;
USE somanager;

-- --------------------------------------------------------

--
-- Structure de la table `AnneeScolaire`
--

CREATE TABLE IF NOT EXISTS `AnneeScolaire` (
  `idAnneeScolaire` int(5) NOT NULL,
  `anneeDebut` year(4) NOT NULL,
  `anneeFin` year(4) NOT NULL,
  `dateDebutProjet` datetime DEFAULT NULL,
  `dateMiAvancement` datetime DEFAULT NULL,
  `dateDepotPoster` datetime DEFAULT NULL,
  `dateSoutenanceFinale` datetime DEFAULT NULL,
  `idOption` int(5) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;


--
-- Déclencheurs `AnneeScolaire`
--
DELIMITER //
CREATE TRIGGER `tg_verification_annee_debut_egale_annee_courante` BEFORE INSERT ON `AnneeScolaire`
  FOR EACH ROW begin

  if (new.anneeDebut> YEAR(NOW())) then
    SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Annee de début de lannée scolaire est supérieure à lannée courante';
  end if;

end
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Structure de la table `Commentaire`
--

CREATE TABLE IF NOT EXISTS `Commentaire` (
  `idCommentaire` int(5) NOT NULL,
  `contenu` varchar(500) DEFAULT NULL,
  `idSujet` int(5) DEFAULT NULL,
  `idUtilisateur` int(5) DEFAULT NULL,
  `idObservation` int(5) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;


-- --------------------------------------------------------

--
-- Structure de la table `Equipe`
--

CREATE TABLE IF NOT EXISTS `Equipe` (
  `idEquipe` varchar(10) NOT NULL DEFAULT '',
  `annee` int(4) DEFAULT NULL,
  `taille` int(3) DEFAULT NULL,
  `idSujet` int(5) DEFAULT NULL,
  `valide` enum('oui','non') NOT NULL DEFAULT 'non'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


--
-- Déclencheurs `Equipe`
--
DELIMITER //
CREATE TRIGGER `ModificationEtatSujet` AFTER DELETE ON `Equipe`
  FOR EACH ROW begin

  -- Quand une équipe est supprimée on remet l'état du sujet à laquelle elle était associé a publié

  UPDATE Sujet SET Sujet.etat="publie" WHERE Sujet.idSujet=old.idSujet;


end
//
DELIMITER ;
DELIMITER //
CREATE TRIGGER `tg_cle_primaire_equipe` BEFORE INSERT ON `Equipe`
  FOR EACH ROW begin

  declare anneeCourante year;
  declare nbre int;
  declare idCourant varchar(10);
  declare debut varchar(10);

  -- On récupère l'année courante :
  SET @anneeCourante := (SELECT anneeDebut FROM AnneeScolaire ORDER BY anneeDebut DESC LIMIT 1);

  SET @nbre := ((SELECT COUNT(*) FROM Equipe WHERE idEquipe LIKE Concat( @anneeCourante, '%')));

  IF (@nbre = 0) THEN
    -- Si aucune équipe n'a été créée, on crée la première équipe :
    SET @debut = Concat(@anneeCourante,'_001');
    SET new.idEquipe= @debut;
  ELSE

    -- On sélectionne le dernier ID de la table
    SET @idCourant := (SELECT idEquipe FROM Equipe WHERE idEquipe LIKE Concat( @anneeCourante, '%') ORDER BY idEquipe DESC LIMIT 1);

    -- On récupère le nombre derrière cet ID et on rajoute 1 car on crée une nouvelle entrée
    SET @nbre := (convert(SUBSTR(@idCourant FROM 6), SIGNED)+1);


    SET @debut=
    CASE
    -- Si le nombre d'équipe est compris entre 0 et 10, on ne rajoute que 2 zeros avant le nombre (ex : yyyy_002)
    WHEN @nbre<10 THEN Concat(@anneeCourante,'_00')

    -- Si le nombre d'équipe est compris entre 10 et 99, on ne rajoute que 1 zeros avant le nombre (ex : yyyy_022)
    WHEN @nbre>9 THEN Concat(@anneeCourante,'_0')

    END;
    SET new.idEquipe=Concat(@debut,@nbre);

  END IF;
end
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Structure de la table `Etudiant`
--

CREATE TABLE IF NOT EXISTS `Etudiant` (
  `idEtudiant` int(5) NOT NULL,
  `annee` int(5) DEFAULT NULL,
  `contratPro` enum('oui','non') NOT NULL DEFAULT 'non',
  `noteIntermediaire` float DEFAULT NULL,
  `noteProjet` float DEFAULT NULL,
  `noteSoutenance` float DEFAULT NULL,
  `notePoster` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


--
-- Déclencheurs `Etudiant`
--
DELIMITER //
CREATE TRIGGER `ReglageNotes` BEFORE INSERT ON `Etudiant`
  FOR EACH ROW begin

  SET new.noteIntermediaire=-1;
  SET new.noteProjet=-1;
  SET new.noteSoutenance=-1;
  SET new.notePoster=-1;

end
//
DELIMITER ;
DELIMITER //
CREATE TRIGGER `SuppressionEtudiantEquipe` BEFORE DELETE ON `Etudiant`
  FOR EACH ROW -- Trigger permettant de supprimer l'étudiant qui va être supprimé de l'équipe dont il fait partie, si il fait partie d'une.
  begin

    declare etudiantPresent int;

    -- On teste si l'étudiant est présent dans une équipe
    SET @etudiantPresent:=(SELECT COUNT(*) FROM EtudiantEquipe WHERE EtudiantEquipe.idEtudiant = old.idEtudiant);

    -- Si oui, on le supprime de l'équipe
    if (@etudiantPresent=1) then
      DELETE FROM EtudiantEquipe WHERE idEtudiant=old.idEtudiant;
    end if;
  end
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Structure de la table `EtudiantEquipe`
--

CREATE TABLE IF NOT EXISTS `EtudiantEquipe` (
  `idEtudiant` int(5) NOT NULL,
  `idEquipe` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déclencheurs `EtudiantEquipe`
--
DELIMITER //
CREATE TRIGGER `DecrementTailleEquipe` AFTER DELETE ON `EtudiantEquipe`
  FOR EACH ROW -- Trigger mettant à jour automatiquement la taille de l'équipe

  begin

    declare tailleEquipe int;

    -- On récupère la taille de l'équipe concernée
    SET @tailleEquipe :=(SELECT COUNT(EtudiantEquipe.idEtudiant) FROM EtudiantEquipe WHERE EtudiantEquipe.idEquipe=old.idEquipe);

    -- On met à jour
    UPDATE Equipe SET taille=@tailleEquipe WHERE idEquipe=old.idEquipe;

    DELETE FROM Equipe Where Equipe.taille=0;
  end
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Structure de la table `JuryPoster`
--

CREATE TABLE IF NOT EXISTS `JuryPoster` (
  `idJuryPoster` varchar(10) NOT NULL DEFAULT '',
  `idProf1` int(5) DEFAULT NULL,
  `idProf2` int(5) DEFAULT NULL,
  `idProf3` int(5) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `idEquipe` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déclencheurs `JuryPoster`
--
DELIMITER //
CREATE TRIGGER `tg_cle_primaire_jury_poster` BEFORE INSERT ON `JuryPoster`
  FOR EACH ROW begin

  declare anneeCourante year;
  declare nbre int;
  declare idCourant varchar(10);
  declare debut varchar(10);

  -- On récupère l'année courante :
  SET @anneeCourante := (SELECT anneeDebut FROM AnneeScolaire ORDER BY anneeDebut DESC LIMIT 1);

  SET @nbre := ((SELECT COUNT(*) FROM JuryPoster WHERE idJuryPoster LIKE Concat( @anneeCourante, '%')));

  IF (@nbre = 0) THEN
    -- Si aucune équipe n'a été créée, on crée la première équipe :
    SET @debut = Concat(@anneeCourante,'_001');
    SET new.idJuryPoster= @debut;
  ELSE

    -- On sélectionne le dernier ID de la table
    SET @idCourant := (SELECT idJuryPoster FROM JuryPoster WHERE idJuryPoster LIKE Concat( @anneeCourante, '%') ORDER BY idJuryPoster DESC LIMIT 1);

    -- On récupère le nombre derrière cet ID et on rajoute 1 car on crée une nouvelle entrée
    SET @nbre := (convert(SUBSTR(@idCourant FROM 6), SIGNED)+1);


    SET @debut=
    CASE
    -- Si le nombre d'équipe est compris entre 0 et 10, on ne rajoute que 2 zeros avant le nombre (ex : yyyy_002)
    WHEN @nbre<10 THEN Concat(@anneeCourante,'_00')

    -- Si le nombre d'équipe est compris entre 10 et 99, on ne rajoute que 1 zeros avant le nombre (ex : yyyy_022)
    WHEN @nbre>9 THEN Concat(@anneeCourante,'_0')

    END;
    SET new.idJuryPoster=Concat(@debut,@nbre);

  END IF;
end
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Structure de la table `JurySoutenance`
--

CREATE TABLE IF NOT EXISTS `JurySoutenance` (
  `idJurySoutenance` varchar(10) NOT NULL DEFAULT '',
  `idProf1` int(5) DEFAULT NULL,
  `idProf2` int(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déclencheurs `JurySoutenance`
--
DELIMITER //
CREATE TRIGGER `tg_cle_primaire_jury_soutenance` BEFORE INSERT ON `JurySoutenance`
  FOR EACH ROW begin

  declare anneeCourante year;
  declare nbre int;
  declare idCourant varchar(10);
  declare debut varchar(10);

  -- On récupère l'année courante :
  SET @anneeCourante := (SELECT anneeDebut FROM AnneeScolaire ORDER BY anneeDebut DESC LIMIT 1);

  SET @nbre := ((SELECT COUNT(*) FROM JurySoutenance WHERE idJurySoutenance LIKE Concat( @anneeCourante, '%')));

  IF (@nbre = 0) THEN
    -- Si aucune équipe n'a été créée, on crée la première équipe :
    SET @debut = Concat(@anneeCourante,'_001');
    SET new.idJurySoutenance= @debut;
  ELSE

    -- On sélectionne le dernier ID de la table
    SET @idCourant := (SELECT idJurySoutenance FROM JurySoutenance WHERE idJurySoutenance LIKE Concat( @anneeCourante, '%') ORDER BY idJurySoutenance DESC LIMIT 1);

    -- On récupère le nombre derrière cet ID et on rajoute 1 car on crée une nouvelle entrée
    SET @nbre := (convert(SUBSTR(@idCourant FROM 6), SIGNED)+1);


    SET @debut=
    CASE
    -- Si le nombre d'équipe est compris entre 0 et 10, on ne rajoute que 2 zeros avant le nombre (ex : yyyy_002)
    WHEN @nbre<10 THEN Concat(@anneeCourante,'_00')

    -- Si le nombre d'équipe est compris entre 10 et 99, on ne rajoute que 1 zeros avant le nombre (ex : yyyy_022)
    WHEN @nbre>9 THEN Concat(@anneeCourante,'_0')

    END;
    SET new.idJurySoutenance=Concat(@debut,@nbre);

  END IF;
end
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Structure de la table `NoteInteretSujet`
--

CREATE TABLE IF NOT EXISTS `NoteInteretSujet` (
  `idProfesseur` int(5) NOT NULL,
  `idSujet` int(5) NOT NULL,
  `note` int(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


-- --------------------------------------------------------

--
-- Structure de la table `NoteInteretTechno`
--

CREATE TABLE IF NOT EXISTS `NoteInteretTechno` (
  `idProfesseur` int(5) NOT NULL,
  `idSujet` int(5) NOT NULL,
  `note` int(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


-- --------------------------------------------------------

--
-- Structure de la table `NotePoster`
--

CREATE TABLE IF NOT EXISTS `NotePoster` (
  `idProfesseur` int(5) NOT NULL,
  `idEtudiant` int(5) NOT NULL,
  `idPoster` int(5) NOT NULL,
  `note` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déclencheurs `NotePoster`
--
DELIMITER //
CREATE TRIGGER `ExportNotePosterVersEtudiant` AFTER INSERT ON `NotePoster`
  FOR EACH ROW -- Trigger permettant d'exporter la note finale de l'étuidant quand tous les jurys ont mis leurs notes

  begin

    declare noteFinale float;

    if ((SELECT COUNT(*) FROM NotePoster WHERE idEtudiant=new.idEtudiant)=3) then

      SET @noteFinale:=(SELECT ROUND(AVG(note),2) FROM `NotePoster` WHERE idEtudiant=new.idEtudiant);

      UPDATE `Etudiant` SET `notePoster`= @noteFinale WHERE idEtudiant=new.IdEtudiant;

    end if;

  end
//
DELIMITER ;
DELIMITER //
CREATE TRIGGER `MiseAJourNotePosterVersEtudiant` AFTER UPDATE ON `NotePoster`
  FOR EACH ROW begin

  -- Trigger permettant de mettre à jour la note finale de l'étudiant si le prof concerné par le jury Soutenance, change sa note

  declare noteFinale float;

  if ((SELECT COUNT(*) FROM NotePoster WHERE idEtudiant=new.idEtudiant)=3 AND new.note!=old.note) then

    SET @noteFinale:=(SELECT ROUND(AVG(note),2) FROM `NotePoster` WHERE idEtudiant=new.idEtudiant);

    UPDATE `Etudiant` SET `notePoster`= @noteFinale WHERE idEtudiant=new.IdEtudiant;

  end if;

end
//
DELIMITER ;
DELIMITER //
CREATE TRIGGER `VerificationApresMiseAJourNotePoster` BEFORE UPDATE ON `NotePoster`
  FOR EACH ROW -- Trigger permettant de vérifier, lors d'une mise à jour de l'entrée, que les infos sont concordantes
  begin

    -- On vérifie d'abord que le professeur qui note le poster est dans le juryPoster
    if ((SELECT COUNT(*) FROM JuryPoster, Equipe,Sujet,Poster WHERE new.idProfesseur in(idProf1,idProf2,idProf3) and JuryPoster.idEquipe=Equipe.idEquipe and Equipe.idSujet = Sujet.idSujet and Poster.idSujet=Sujet.idSujet and Poster.idPoster=new.idPoster)!=1) then

      SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Le professeur spécifié ne fait pas partie dans la soutenance spécifiée';
    end if;

    -- On vérifie que l'étudiant spécifié fait partie de l'équipe que le prof juge
    if ((SELECT COUNT(DISTINCT Poster.idPoster) FROM EtudiantEquipe,Poster,Equipe WHERE EtudiantEquipe.idEtudiant=new.idEtudiant and EtudiantEquipe.idEquipe=Poster.idEquipe and Poster.idPoster=new.idPoster
        )!=1) then
      SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'L étudiant spécifié ne fait pas partie de l équipe du poster spécifiée';
    end if;

    -- On vérifie que la note est comprise entre 0 et 20
    if (new.note>20 OR new.note<0) then
      SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'La note n est pas comprise entre 0 et 20';
    end if;


  end
//
DELIMITER ;
DELIMITER //
CREATE TRIGGER `VerificationAvantInsertionNotePoster` BEFORE INSERT ON `NotePoster`
  FOR EACH ROW begin

  -- On vérifie d'abord que le professeur qui note le poster est dans le juryPoster
  if ((SELECT COUNT(*) FROM JuryPoster, Equipe,Sujet,Poster WHERE new.idProfesseur in(idProf1,idProf2,idProf3) and JuryPoster.idEquipe=Equipe.idEquipe and Equipe.idSujet = Sujet.idSujet and Poster.idSujet=Sujet.idSujet and Poster.idPoster=new.idPoster)!=1) then

    SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Le professeur spécifié ne fait pas partie dans la soutenance spécifiée';
  end if;

  -- On vérifie que l'étudiant spécifié fait partie de l'équipe que le prof juge
  if ((SELECT COUNT(DISTINCT Poster.idPoster) FROM EtudiantEquipe,Poster,Equipe WHERE EtudiantEquipe.idEtudiant=new.idEtudiant and EtudiantEquipe.idEquipe=Poster.idEquipe and Poster.idPoster=new.idPoster
      )!=1) then
    SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'L étudiant spécifié ne fait pas partie de l équipe du poster spécifiée';
  end if;

  -- On vérifie que la note est comprise entre 0 et 20
  if (new.note>20 OR new.note<0) then
    SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'La note n est pas comprise entre 0 et 20';
  end if;


end
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Structure de la table `NoteSoutenance`
--

CREATE TABLE IF NOT EXISTS `NoteSoutenance` (
  `idProfesseur` int(5) NOT NULL,
  `idEtudiant` int(5) NOT NULL,
  `idSoutenance` int(5) NOT NULL,
  `note` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déclencheurs `NoteSoutenance`
--
DELIMITER //
CREATE TRIGGER `ExportNoteSoutenanceVersEtudiant` AFTER INSERT ON `NoteSoutenance`
  FOR EACH ROW begin

  declare noteFinale float;

  if ((SELECT COUNT(*) FROM NoteSoutenance WHERE idEtudiant=new.idEtudiant)=2) then

    SET @noteFinale:=(SELECT ROUND(AVG(note),2) FROM `NoteSoutenance` WHERE idEtudiant=new.idEtudiant);

    UPDATE `Etudiant` SET `noteSoutenance`= @noteFinale WHERE idEtudiant=new.IdEtudiant;

  end if;

end
//
DELIMITER ;
DELIMITER //
CREATE TRIGGER `MiseAJourNoteSoutenanceVersEtudiant` AFTER UPDATE ON `NoteSoutenance`
  FOR EACH ROW begin

  -- Trigger permettant de mettre à jour la note finale de l'étudiant si le prof concerné par le jury Soutenance, change sa note

  declare noteFinale float;

  if ((SELECT COUNT(*) FROM NoteSoutenance WHERE idEtudiant=new.idEtudiant)=2 AND new.note!=old.note) then

    SET @noteFinale:=(SELECT ROUND(AVG(note),2) FROM `NoteSoutenance` WHERE idEtudiant=new.idEtudiant);

    UPDATE `Etudiant` SET `noteSoutenance`= @noteFinale WHERE idEtudiant=new.IdEtudiant;

  end if;

end
//
DELIMITER ;
DELIMITER //
CREATE TRIGGER `VerificationApresMiseAJourNoteSoutenance` BEFORE UPDATE ON `NoteSoutenance`
  FOR EACH ROW begin

  -- On vérifie d'abord que le professeur qui note la soutenance est dans le jurySoutenance

  if ((SELECT COUNT(*) FROM JurySoutenance,Soutenance WHERE new.idProfesseur in(idProf1,idProf2) and JurySoutenance.idJurySoutenance=Soutenance.idJurySoutenance and Soutenance.idSoutenance=new.idSoutenance)!=1) then
    SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Le professeur spécifié ne fait pas partie dans la soutenance spécifiée';
  end if;

  -- On vérifie que l'étudiant spécifié fait partie de l'équipe que le prof juge
  if ((SELECT COUNT(*) FROM EtudiantEquipe ee,Soutenance s WHERE ee.idEtudiant=new.idEtudiant AND ee.idEquipe=s.idEquipe AND s.idSoutenance=new.idSoutenance)!=1) then
    SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'L étudiant spécifié ne fait pas partie dans la soutenance spécifiée ou ne fait pas partie de l équipe de la soutenance spécifiée';
  end if;

  -- On vérifie que la note est comprise entre 0 et 20
  if (new.note>20 OR new.note<0) then
    SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'La note n est pas comprise entre 0 et 20';
  end if;

end
//
DELIMITER ;
DELIMITER //
CREATE TRIGGER `VerificationAvantInsertionNoteSoutenance` BEFORE INSERT ON `NoteSoutenance`
  FOR EACH ROW begin

  -- On vérifie d'abord que le professeur qui note la soutenance est dans le jurySoutenance

  if ((SELECT COUNT(*) FROM JurySoutenance,Soutenance WHERE new.idProfesseur in(idProf1,idProf2) and JurySoutenance.idJurySoutenance=Soutenance.idJurySoutenance and Soutenance.idSoutenance=new.idSoutenance)!=1) then
    SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Le professeur spécifié ne fait pas partie dans la soutenance spécifiée';
  end if;

  -- On vérifie que l'étudiant spécifié fait partie de l'équipe que le prof juge
  if ((SELECT COUNT(*) FROM EtudiantEquipe ee,Soutenance s WHERE ee.idEtudiant=new.idEtudiant AND ee.idEquipe=s.idEquipe AND s.idSoutenance=new.idSoutenance)!=1) then
    SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'L étudiant spécifié ne fait pas partie dans la soutenance spécifiée ou ne fait pas partie de l équipe de la soutenance spécifiée';
  end if;

  -- On vérifie que la note est comprise entre 0 et 20
  if (new.note>20 OR new.note<0) then
    SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'La note n est pas comprise entre 0 et 20';
  end if;

end
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Structure de la table `Notification`
--

CREATE TABLE IF NOT EXISTS `Notification` (
  `idNotification` int(5) NOT NULL,
  `commentaire` varchar(100) DEFAULT NULL,
  `lien` varchar(100) DEFAULT NULL,
  `vue` smallint(2) DEFAULT NULL,
  `idUtilisateur` int(5) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;


-- --------------------------------------------------------

--
-- Structure de la table `OptionESEO`
--

CREATE TABLE IF NOT EXISTS `OptionESEO` (
  `idOption` int(5) NOT NULL,
  `nomOption` varchar(5) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `OptionESEO`
--

INSERT INTO `OptionESEO` (`idOption`, `nomOption`) VALUES
  (1, 'LD'),
  (2, 'SE'),
  (3, 'OC'),
  (4, 'BIO'),
  (5, 'NRJ'),
  (6, 'CC'),
  (7, 'IIT'),
  (8, 'DSMT'),
  (9, 'BD'),
  (20, '');

-- --------------------------------------------------------

--
-- Structure de la table `OptionSujets`
--

CREATE TABLE IF NOT EXISTS `OptionSujets` (
  `idOption` int(5) NOT NULL,
  `idSujet` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `PartageRole`
--

CREATE TABLE IF NOT EXISTS `PartageRole` (
  `idPartageRole` int(5) NOT NULL,
  `idUtilisateur1` int(5) NOT NULL,
  `idUtilisateur2` int(5) NOT NULL,
  `idRole` int(5) NOT NULL,
  `idOption` int(5) NOT NULL,
  `dateDebut` datetime NOT NULL,
  `dateFin` datetime NOT NULL,
  `actif` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;


--
-- Déclencheurs `PartageRole`
--
DELIMITER //
CREATE TRIGGER `AjoutRoleTemporaire` BEFORE INSERT ON `PartageRole`
  FOR EACH ROW -- Trigger permettant d'ajouter dans la table PartageRole, une entrée
  -- On vérifie ici si les données rentrées sont corrects

  begin

    -- On vérifie que les dates sont corrects
    if (new.dateDebut>=new.dateFin) then
      SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Date de fin inférieur a date de début';
    end if;

    -- On vérifie que l'utilisateur bénéficiaire est bien un Professeur
    if ((SELECT COUNT(*) FROM Professeur WHERE idProfesseur=new.idUtilisateur2)=0) then
      SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Utilisateur2 nest pas un professeur';
    end if;

    -- Si la date de début est inférieur à la date du moment on active le partage
    if (new.dateDebut<=NOW() and new.actif=0) then
      INSERT INTO `RoleUtilisateur`(`idUtilisateur`, `idRole`, `idOption`) VALUES (new.idUtilisateur2,new.idRole,new.idOption);
      SET new.actif=1;
    end if;

  end
//
DELIMITER ;
DELIMITER //
CREATE TRIGGER `AjoutRoleTemporaire_2` BEFORE UPDATE ON `PartageRole`
  FOR EACH ROW -- Trigger permettant d'ajouter dans la table RoleUtilisateur, le role revenant à l'utilisateur quand l'attribut actif de PartageRole passe de 0 à 1. Ce passage est réalisé par un evenement qui update automatiquement cet attribut.

  begin
    if (old.actif=0 and new.actif=1) then
      INSERT INTO `RoleUtilisateur`(`idUtilisateur`, `idRole`, `idOption`) VALUES (new.idUtilisateur2,new.idRole,new.idOption);
    end if;
  end
//
DELIMITER ;
DELIMITER //
CREATE TRIGGER `SupressionRoleTemporaire` AFTER DELETE ON `PartageRole`
  FOR EACH ROW -- Apres la suppression de l'entrée de PartageRole, on supprime aussi l'entrée dans RoleUtilisateur

  DELETE FROM `RoleUtilisateur` WHERE idRole=old.idRole and idOption=old.idOption and idUtilisateur=old.idUtilisateur2
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Structure de la table `PorteurSujet`
--

CREATE TABLE IF NOT EXISTS `PorteurSujet` (
  `idSujet` int(5) NOT NULL,
  `idUtilisateur` int(5) NOT NULL,
  `annee` year(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `Poster`
--

CREATE TABLE IF NOT EXISTS `Poster` (
  `idPoster` int(5) NOT NULL,
  `chemin` varchar(100) DEFAULT NULL,
  `datePoster` datetime DEFAULT NULL,
  `idSujet` int(5) DEFAULT NULL,
  `valide` enum('oui','non') NOT NULL DEFAULT 'non',
  `idEquipe` varchar(10) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;


-- --------------------------------------------------------

--
-- Structure de la table `Professeur`
--

CREATE TABLE IF NOT EXISTS `Professeur` (
  `idProfesseur` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


-- --------------------------------------------------------

--
-- Structure de la table `ProfesseurSujet`
--

CREATE TABLE IF NOT EXISTS `ProfesseurSujet` (
  `idProfesseur` int(5) NOT NULL,
  `idSujet` int(5) NOT NULL,
  `fonction` enum('co-encadrant','interessé','consultant','référent') NOT NULL DEFAULT 'co-encadrant',
  `valide` enum('oui','non') NOT NULL DEFAULT 'non'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


--
-- Déclencheurs `ProfesseurSujet`
--
DELIMITER //
CREATE TRIGGER `ValidationAutomatique` BEFORE INSERT ON `ProfesseurSujet`
  FOR EACH ROW begin


  if (new.fonction IN ('co-encadrant','interessé','consultant')) then
    set new.valide='oui';
  end if;

end
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Structure de la table `Role`
--

CREATE TABLE IF NOT EXISTS `Role` (
  `idRole` int(5) NOT NULL,
  `nomRole` varchar(15) DEFAULT NULL,
  type enum('applicatif','equipe') DEFAULT 'applicatif'
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `Role`
--

INSERT INTO `Role` (`idRole`, `nomRole`, `type`) VALUES
  (1, 'etudiant', 'applicatif'),
  (2, 'admin','applicatif'),
  (3, 'prof','applicatif'),
  (4, 'profOption','applicatif'),
  (5, 'profResponsable','applicatif'),
  (6, 'assistant','applicatif'),
  (9, 'entrepriseExt','applicatif'),
  (10, 'serviceCom','applicatif'),
  (11, 'scrumMaster', 'equipe'),
  (12, 'productOwner', 'equipe'),
  (13, 'profReferent', 'equipe');
-- --------------------------------------------------------

--
-- Structure de la table `RoleUtilisateur`
--

CREATE TABLE IF NOT EXISTS `RoleUtilisateur` (
  `idUtilisateur` int(5) NOT NULL,
  `idRole` int(5) NOT NULL,
  `idOption` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO RoleUtilisateur (idUtilisateur, idRole, idOption) VALUES (1, 2, 20);


--
-- Déclencheurs `RoleUtilisateur`
--
DELIMITER //
CREATE TRIGGER `AjoutProfesseur` AFTER INSERT ON `RoleUtilisateur`
  FOR EACH ROW -- Trigger permettant d'automatiser la création des objets Professeur.

  begin

    DECLARE idRoleProf int;
    DECLARE idRoleProfOption int;
    DECLARE idRoleProfResp int;
    DECLARE verificationExistance int;

    -- On vérifie que l'objet professeur n'existe pas déja
    SET @verificationExistance:=(SELECT COUNT(*) FROM Professeur WHERE idProfesseur=NEW.idUtilisateur);

    -- On récupère l'idRole du role prof
    SET @idRoleProf :=(SELECT idRole FROM Role WHERE Role.nomRole="prof");

    -- On récupère l'idRole du role profOption
    SET @idRoleProfOption :=(SELECT idRole FROM Role WHERE Role.nomRole="profOption");

    -- On récupère l'idRole du role profResponsable
    SET @idRoleProfResp :=(SELECT idRole FROM Role WHERE Role.nomRole="profResponsable");

    -- Si l'objet Professeur n'exsite pas déja, on le crée
    if (@verificationExistance=0) then
      if (NEW.idRole=@idRoleProf OR NEW.idRole=@idRoleProfOption OR NEW.idRole=@idRoleProfResp) then
        INSERT INTO `Professeur`(`idProfesseur`) VALUES (new.idUtilisateur);
      end if;
    end if;
  end
//
DELIMITER ;
DELIMITER //
CREATE TRIGGER `SuppressionEtudiantProfesseur` AFTER DELETE ON `RoleUtilisateur`
  FOR EACH ROW -- Trigger permettant d'automatiser la suppression des objets Professeur et étudiant lors d'une suppression de role.

  begin
    DECLARE idRoleEtudiant int;
    DECLARE idRoleProf int;
    DECLARE idRoleProfOption int;
    DECLARE idRoleProfResp int;
    DECLARE nombreRoleProf int;

    SET @idRoleEtudiant :=(SELECT idRole FROM Role WHERE Role.nomRole="etudiant");

    SET @idRoleProf :=(SELECT idRole FROM Role WHERE Role.nomRole="prof");

    SET @idRoleProfOption :=(SELECT idRole FROM Role WHERE Role.nomRole="profOption");

    SET @idRoleProfResp :=(SELECT idRole FROM Role WHERE Role.nomRole="profResponsable");

    SET @nombreRoleProf :=(SELECT COUNT(*) FROM RoleUtilisateur WHERE idRole in (@idRoleProf,@idRoleProfOption,@idRoleProfResp) and idUtilisateur=OLD.idUtilisateur);


    if (@idRoleEtudiant=OLD.idRole) then
      DELETE FROM Etudiant Where Etudiant.idEtudiant=OLD.idUtilisateur;
    end if;

    if (@nombreRoleProf=0) then
      if (OLD.idRole=@idRoleProf OR OLD.idRole=@idRoleProfOption OR OLD.idRole=@idRoleProfResp) then
        DELETE FROM Professeur Where Professeur.idProfesseur=OLD.idUtilisateur;
      end if;
    end if;

  end
//
DELIMITER ;
DELIMITER //
CREATE TRIGGER `VerificationDroitSupressionRole` BEFORE DELETE ON `RoleUtilisateur`
  FOR EACH ROW -- On vérifie que la suppression du role n'est pas associé à un partage en cours. Si oui on invite l'utilisateur à faire cette suppression directement via l'interface de PartageRole

  begin

    if ((SELECT COUNT(*) FROM PartageRole WHERE old.idUtilisateur=PartageRole.idUtilisateur2 and old.idRole=PartageRole.idRole and old.idOption=PartageRole.idOption)=1) then
      SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Impossible de supprimer un role en cours de partage, veuillez vous referer à la table PartageRole directement';
    end if;

  end
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Structure de la table `Soutenance`
--

CREATE TABLE IF NOT EXISTS `Soutenance` (
  `idSoutenance` int(5) NOT NULL,
  `dateSoutenance` datetime DEFAULT NULL,
  `idJurySoutenance` varchar(10) DEFAULT NULL,
  `idEquipe` varchar(10) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;


-- --------------------------------------------------------

--
-- Structure de la table `Sujet`
--

CREATE TABLE IF NOT EXISTS `Sujet` (
  `idSujet` int(5) NOT NULL,
  `titre` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `nbrMinEleves` int(4) NOT NULL,
  `nbrMaxEleves` int(4) NOT NULL,
  `contratPro` tinyint(1) DEFAULT NULL,
  `confidentialite` tinyint(1) DEFAULT NULL,
  `etat` enum('depose','valide','attribue','refuse','publie') NOT NULL,
  `liens` text,
  `interets` text CHARACTER SET latin2,
  `noteInteretTechno` float DEFAULT NULL,
  `noteInteretSujet` float DEFAULT NULL,
  `idForm` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;


-- --------------------------------------------------------

--
-- Structure de la table `Utilisateur`
--

CREATE TABLE IF NOT EXISTS `Utilisateur` (
  `idUtilisateur` int(5) NOT NULL,
  `nom` varchar(20) DEFAULT NULL,
  `prenom` varchar(20) DEFAULT NULL,
  `identifiant` varchar(15) NOT NULL,
  `hash` varchar(100) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `valide` enum('oui','non') NOT NULL DEFAULT 'non'
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `Utilisateur`
--

INSERT INTO `Utilisateur` (`idUtilisateur`, `nom`, `prenom`, `identifiant`, `hash`, `email`, `valide`) VALUES (1, 'ESEO', 'admin', 'admin', '$2a$10$dJIeyRW2gEOZBns4zcvCbOPp1Y6yVm6vMkdMDowN8YqqkP6RhgQvq', 'admin@eseo.fr', 'oui');

--

--
-- Index pour la table `AnneeScolaire`
--
ALTER TABLE `AnneeScolaire`
  ADD PRIMARY KEY (`idAnneeScolaire`), ADD UNIQUE KEY `idOption` (`idOption`);

--
-- Index pour la table `Commentaire`
--
ALTER TABLE `Commentaire`
  ADD PRIMARY KEY (`idCommentaire`), ADD KEY `idObservation` (`idObservation`), ADD KEY `Commentaire_ibfk_1` (`idSujet`), ADD KEY `Commentaire_ibfk_2` (`idUtilisateur`);

--
-- Index pour la table `Equipe`
--
ALTER TABLE `Equipe`
  ADD PRIMARY KEY (`idEquipe`), ADD UNIQUE KEY `idEquipe` (`idEquipe`), ADD UNIQUE KEY `idSujet` (`idSujet`), ADD KEY `Equipe_ibfk_1` (`idSujet`);

--
-- Index pour la table `Etudiant`
--
ALTER TABLE `Etudiant`
  ADD PRIMARY KEY (`idEtudiant`), ADD UNIQUE KEY `idEtudiant` (`idEtudiant`);

--
-- Index pour la table `EtudiantEquipe`
--
ALTER TABLE `EtudiantEquipe`
  ADD PRIMARY KEY (`idEtudiant`,`idEquipe`), ADD UNIQUE KEY `idEtudiant` (`idEtudiant`), ADD KEY `EtudiantEquipe_ibfk_2` (`idEquipe`);

--
-- Index pour la table `JuryPoster`
--
ALTER TABLE `JuryPoster`
  ADD PRIMARY KEY (`idJuryPoster`), ADD UNIQUE KEY `idJuryPoster` (`idJuryPoster`), ADD UNIQUE KEY `idEquipe` (`idEquipe`), ADD KEY `JuryPoster_ibfk_3` (`idProf3`), ADD KEY `JuryPoster_ibfk_1` (`idProf1`), ADD KEY `JuryPoster_ibfk_2` (`idProf2`);

--
-- Index pour la table `JurySoutenance`
--
ALTER TABLE `JurySoutenance`
  ADD PRIMARY KEY (`idJurySoutenance`), ADD UNIQUE KEY `identifiant` (`idJurySoutenance`), ADD KEY `JurySoutenance_ibfk_2` (`idProf2`), ADD KEY `JurySoutenance_ibfk_1` (`idProf1`);

--
-- Index pour la table `NoteInteretSujet`
--
ALTER TABLE `NoteInteretSujet`
  ADD PRIMARY KEY (`idProfesseur`,`idSujet`), ADD KEY `NoteInteretSujet_ibfk_2` (`idSujet`);

--
-- Index pour la table `NoteInteretTechno`
--
ALTER TABLE `NoteInteretTechno`
  ADD PRIMARY KEY (`idProfesseur`,`idSujet`), ADD KEY `NoteInteretTechno_ibfk_2` (`idSujet`);

--
-- Index pour la table `NotePoster`
--
ALTER TABLE `NotePoster`
  ADD PRIMARY KEY (`idProfesseur`,`idEtudiant`,`idPoster`), ADD KEY `idPoster` (`idPoster`), ADD KEY `idEtudiant` (`idEtudiant`);

--
-- Index pour la table `NoteSoutenance`
--
ALTER TABLE `NoteSoutenance`
  ADD PRIMARY KEY (`idProfesseur`,`idEtudiant`,`idSoutenance`), ADD KEY `idSoutenance` (`idSoutenance`), ADD KEY `idEtudiant` (`idEtudiant`);

--
-- Index pour la table `Notification`
--
ALTER TABLE `Notification`
  ADD PRIMARY KEY (`idNotification`), ADD KEY `Notification_ibfk_1` (`idUtilisateur`);

--
-- Index pour la table `OptionESEO`
--
ALTER TABLE `OptionESEO`
  ADD PRIMARY KEY (`idOption`);

--
-- Index pour la table `OptionSujets`
--
ALTER TABLE `OptionSujets`
  ADD PRIMARY KEY (`idOption`,`idSujet`), ADD KEY `OptionSujets_ibfk_2` (`idSujet`);

--
-- Index pour la table `PartageRole`
--
ALTER TABLE `PartageRole`
  ADD PRIMARY KEY (`idPartageRole`,`idUtilisateur1`,`idUtilisateur2`,`idRole`,`idOption`), ADD UNIQUE KEY `UC_Person` (`idUtilisateur1`,`idUtilisateur2`,`idRole`,`idOption`), ADD KEY `PartageRole_ibfk_2` (`idUtilisateur2`), ADD KEY `PartageRole_ibfk_3` (`idRole`), ADD KEY `PartageRole_ibfk_4` (`idOption`);

--
-- Index pour la table `PorteurSujet`
--
ALTER TABLE `PorteurSujet`
  ADD PRIMARY KEY (`idSujet`,`idUtilisateur`), ADD UNIQUE KEY `idSujet` (`idSujet`), ADD KEY `PorteurSujet_ibfk_2` (`idUtilisateur`);

--
-- Index pour la table `Poster`
--
ALTER TABLE `Poster`
  ADD PRIMARY KEY (`idPoster`), ADD UNIQUE KEY `idSujet` (`idSujet`), ADD KEY `Poster_ibfk_3` (`idEquipe`);

--
-- Index pour la table `Professeur`
--
ALTER TABLE `Professeur`
  ADD PRIMARY KEY (`idProfesseur`);

--
-- Index pour la table `ProfesseurSujet`
--
ALTER TABLE `ProfesseurSujet`
  ADD PRIMARY KEY (`idProfesseur`,`idSujet`,`fonction`), ADD UNIQUE KEY `idSujet` (`idSujet`,`idProfesseur`);

--
-- Index pour la table `Role`
--
ALTER TABLE `Role`
  ADD PRIMARY KEY (`idRole`);

--
-- Index pour la table `RoleUtilisateur`
--
ALTER TABLE `RoleUtilisateur`
  ADD PRIMARY KEY (`idUtilisateur`,`idRole`,`idOption`), ADD KEY `RoleUtilisateur_ibfk_2` (`idRole`), ADD KEY `RoleUtilisateur_ibfk_3` (`idOption`);

--
-- Index pour la table `Soutenance`
--
ALTER TABLE `Soutenance`
  ADD PRIMARY KEY (`idSoutenance`), ADD UNIQUE KEY `idEquipe` (`idEquipe`), ADD KEY `Soutenance_ibfk_1` (`idJurySoutenance`);

--
-- Index pour la table `Sujet`
--
ALTER TABLE `Sujet`
  ADD PRIMARY KEY (`idSujet`);

--
-- Index pour la table `Utilisateur`
--
ALTER TABLE `Utilisateur`
  ADD PRIMARY KEY (`idUtilisateur`), ADD UNIQUE KEY `identifiant` (`identifiant`), ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `AnneeScolaire`
--
ALTER TABLE `AnneeScolaire`
  MODIFY `idAnneeScolaire` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT pour la table `Commentaire`
--
ALTER TABLE `Commentaire`
  MODIFY `idCommentaire` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT pour la table `Notification`
--
ALTER TABLE `Notification`
  MODIFY `idNotification` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT pour la table `OptionESEO`
--
ALTER TABLE `OptionESEO`
  MODIFY `idOption` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT pour la table `PartageRole`
--
ALTER TABLE `PartageRole`
  MODIFY `idPartageRole` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT pour la table `Poster`
--
ALTER TABLE `Poster`
  MODIFY `idPoster` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT pour la table `Role`
--
ALTER TABLE `Role`
  MODIFY `idRole` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT pour la table `Soutenance`
--
ALTER TABLE `Soutenance`
  MODIFY `idSoutenance` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT pour la table `Sujet`
--
ALTER TABLE `Sujet`
  MODIFY `idSujet` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT pour la table `Utilisateur`
--
ALTER TABLE `Utilisateur`
  MODIFY `idUtilisateur` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `AnneeScolaire`
--
ALTER TABLE `AnneeScolaire`
  ADD CONSTRAINT `AnneeScolaire_ibfk_1` FOREIGN KEY (`idOption`) REFERENCES `OptionESEO` (`idOption`) ON DELETE CASCADE;

--
-- Contraintes pour la table `Commentaire`
--
ALTER TABLE `Commentaire`
  ADD CONSTRAINT `Commentaire_ibfk_1` FOREIGN KEY (`idSujet`) REFERENCES `Sujet` (`idSujet`) ON DELETE CASCADE,
  ADD CONSTRAINT `Commentaire_ibfk_2` FOREIGN KEY (`idUtilisateur`) REFERENCES `Utilisateur` (`idUtilisateur`) ON DELETE CASCADE;

--
-- Contraintes pour la table `Equipe`
--
ALTER TABLE `Equipe`
  ADD CONSTRAINT `Equipe_ibfk_1` FOREIGN KEY (`idSujet`) REFERENCES `Sujet` (`idSujet`) ON DELETE CASCADE;

--
-- Contraintes pour la table `Etudiant`
--
ALTER TABLE `Etudiant`
  ADD CONSTRAINT `Etudiant_ibfk_1` FOREIGN KEY (`idEtudiant`) REFERENCES `Utilisateur` (`idUtilisateur`) ON DELETE CASCADE;

--
-- Contraintes pour la table `EtudiantEquipe`
--
ALTER TABLE `EtudiantEquipe`
  ADD CONSTRAINT `EtudiantEquipe_ibfk_1` FOREIGN KEY (`idEtudiant`) REFERENCES `Etudiant` (`idEtudiant`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `EtudiantEquipe_ibfk_2` FOREIGN KEY (`idEquipe`) REFERENCES `Equipe` (`idEquipe`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `JuryPoster`
--
ALTER TABLE `JuryPoster`
  ADD CONSTRAINT `JuryPoster_ibfk_1` FOREIGN KEY (`idProf1`) REFERENCES `Professeur` (`idProfesseur`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `JuryPoster_ibfk_2` FOREIGN KEY (`idProf2`) REFERENCES `Professeur` (`idProfesseur`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `JuryPoster_ibfk_3` FOREIGN KEY (`idProf3`) REFERENCES `Professeur` (`idProfesseur`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `JuryPoster_ibfk_4` FOREIGN KEY (`idEquipe`) REFERENCES `Equipe` (`idEquipe`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `JurySoutenance`
--
ALTER TABLE `JurySoutenance`
  ADD CONSTRAINT `JurySoutenance_ibfk_1` FOREIGN KEY (`idProf1`) REFERENCES `Professeur` (`idProfesseur`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `JurySoutenance_ibfk_2` FOREIGN KEY (`idProf2`) REFERENCES `Professeur` (`idProfesseur`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `NoteInteretSujet`
--
ALTER TABLE `NoteInteretSujet`
  ADD CONSTRAINT `NoteInteretSujet_ibfk_1` FOREIGN KEY (`idProfesseur`) REFERENCES `Professeur` (`idProfesseur`) ON DELETE CASCADE,
  ADD CONSTRAINT `NoteInteretSujet_ibfk_2` FOREIGN KEY (`idSujet`) REFERENCES `Sujet` (`idSujet`) ON DELETE CASCADE;

--
-- Contraintes pour la table `NoteInteretTechno`
--
ALTER TABLE `NoteInteretTechno`
  ADD CONSTRAINT `NoteInteretTechno_ibfk_1` FOREIGN KEY (`idProfesseur`) REFERENCES `Professeur` (`idProfesseur`) ON DELETE CASCADE,
  ADD CONSTRAINT `NoteInteretTechno_ibfk_2` FOREIGN KEY (`idSujet`) REFERENCES `Sujet` (`idSujet`) ON DELETE CASCADE;

--
-- Contraintes pour la table `NotePoster`
--
ALTER TABLE `NotePoster`
  ADD CONSTRAINT `NotePoster_ibfk_1` FOREIGN KEY (`idProfesseur`) REFERENCES `Professeur` (`idProfesseur`) ON DELETE CASCADE,
  ADD CONSTRAINT `NotePoster_ibfk_2` FOREIGN KEY (`idPoster`) REFERENCES `Poster` (`idPoster`) ON DELETE CASCADE,
  ADD CONSTRAINT `NotePoster_ibfk_3` FOREIGN KEY (`idEtudiant`) REFERENCES `Etudiant` (`idEtudiant`) ON DELETE CASCADE;

--
-- Contraintes pour la table `NoteSoutenance`
--
ALTER TABLE `NoteSoutenance`
  ADD CONSTRAINT `NoteSoutenance_ibfk_1` FOREIGN KEY (`idProfesseur`) REFERENCES `Professeur` (`idProfesseur`) ON DELETE CASCADE,
  ADD CONSTRAINT `NoteSoutenance_ibfk_2` FOREIGN KEY (`idSoutenance`) REFERENCES `Soutenance` (`idSoutenance`) ON DELETE CASCADE,
  ADD CONSTRAINT `NoteSoutenance_ibfk_3` FOREIGN KEY (`idEtudiant`) REFERENCES `Etudiant` (`idEtudiant`) ON DELETE CASCADE;

--
-- Contraintes pour la table `Notification`
--
ALTER TABLE `Notification`
  ADD CONSTRAINT `Notification_ibfk_1` FOREIGN KEY (`idUtilisateur`) REFERENCES `Utilisateur` (`idUtilisateur`) ON DELETE CASCADE;

--
-- Contraintes pour la table `OptionSujets`
--
ALTER TABLE `OptionSujets`
  ADD CONSTRAINT `OptionSujets_ibfk_1` FOREIGN KEY (`idOption`) REFERENCES `OptionESEO` (`idOption`) ON DELETE CASCADE,
  ADD CONSTRAINT `OptionSujets_ibfk_2` FOREIGN KEY (`idSujet`) REFERENCES `Sujet` (`idSujet`) ON DELETE CASCADE;

--
-- Contraintes pour la table `PartageRole`
--
ALTER TABLE `PartageRole`
  ADD CONSTRAINT `PartageRole_ibfk_1` FOREIGN KEY (`idUtilisateur1`) REFERENCES `Utilisateur` (`idUtilisateur`) ON DELETE CASCADE,
  ADD CONSTRAINT `PartageRole_ibfk_2` FOREIGN KEY (`idUtilisateur2`) REFERENCES `Utilisateur` (`idUtilisateur`) ON DELETE CASCADE,
  ADD CONSTRAINT `PartageRole_ibfk_3` FOREIGN KEY (`idRole`) REFERENCES `Role` (`idRole`) ON DELETE CASCADE,
  ADD CONSTRAINT `PartageRole_ibfk_4` FOREIGN KEY (`idOption`) REFERENCES `OptionESEO` (`idOption`) ON DELETE CASCADE;

--
-- Contraintes pour la table `PorteurSujet`
--
ALTER TABLE `PorteurSujet`
  ADD CONSTRAINT `PorteurSujet_ibfk_1` FOREIGN KEY (`idSujet`) REFERENCES `Sujet` (`idSujet`) ON DELETE CASCADE,
  ADD CONSTRAINT `PorteurSujet_ibfk_2` FOREIGN KEY (`idUtilisateur`) REFERENCES `Utilisateur` (`idUtilisateur`) ON DELETE CASCADE;

--
-- Contraintes pour la table `Poster`
--
ALTER TABLE `Poster`
  ADD CONSTRAINT `Poster_ibfk_2` FOREIGN KEY (`idSujet`) REFERENCES `Sujet` (`idSujet`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Poster_ibfk_3` FOREIGN KEY (`idEquipe`) REFERENCES `Equipe` (`idEquipe`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Poster_ibfk_4` FOREIGN KEY (`idEquipe`) REFERENCES `Equipe` (`idEquipe`);

--
-- Contraintes pour la table `Professeur`
--
ALTER TABLE `Professeur`
  ADD CONSTRAINT `Professeur_ibfk_1` FOREIGN KEY (`idProfesseur`) REFERENCES `Utilisateur` (`idUtilisateur`) ON DELETE CASCADE;

--
-- Contraintes pour la table `ProfesseurSujet`
--
ALTER TABLE `ProfesseurSujet`
  ADD CONSTRAINT `ProfesseurSujet_ibfk_1` FOREIGN KEY (`idProfesseur`) REFERENCES `Professeur` (`idProfesseur`) ON DELETE CASCADE,
  ADD CONSTRAINT `ProfesseurSujet_ibfk_2` FOREIGN KEY (`idSujet`) REFERENCES `Sujet` (`idSujet`) ON DELETE CASCADE;

--
-- Contraintes pour la table `RoleUtilisateur`
--
ALTER TABLE `RoleUtilisateur`
  ADD CONSTRAINT `RoleUtilisateur_ibfk_1` FOREIGN KEY (`idUtilisateur`) REFERENCES `Utilisateur` (`idUtilisateur`) ON DELETE CASCADE,
  ADD CONSTRAINT `RoleUtilisateur_ibfk_2` FOREIGN KEY (`idRole`) REFERENCES `Role` (`idRole`) ON DELETE CASCADE,
  ADD CONSTRAINT `RoleUtilisateur_ibfk_3` FOREIGN KEY (`idOption`) REFERENCES `OptionESEO` (`idOption`) ON DELETE CASCADE;

--
-- Contraintes pour la table `Soutenance`
--
ALTER TABLE `Soutenance`
  ADD CONSTRAINT `Soutenance_ibfk_1` FOREIGN KEY (`idJurySoutenance`) REFERENCES `JurySoutenance` (`idJurySoutenance`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Soutenance_ibfk_2` FOREIGN KEY (`idEquipe`) REFERENCES `Equipe` (`idEquipe`) ON DELETE CASCADE ON UPDATE CASCADE;

DELIMITER $$
--
-- Événements
--
CREATE DEFINER=`root`@`localhost` EVENT `VerificationPartageRole` ON SCHEDULE EVERY 3 MINUTE
  STARTS '2017-05-01 00:00:00' ENDS '2037-05-01 00:00:00'
  ON COMPLETION PRESERVE ENABLE
DO
  BEGIN
    UPDATE PartageRole SET actif=1 WHERE dateDebut<=NOW() AND PartageRole.actif=0;
    DELETE FROM PartageRole WHERE dateFin<=NOW();
  END$$

-- Activation des "evenement" sur PhpMyAdmin
SET GLOBAL event_scheduler="ON"$$

DELIMITER ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
USE somanager;
--
-- Creation des tables pour le projet de reprise de somanager
--
DROP TABLE IF EXISTS Parametres;

--
-- Creation d'une table 'parametres'
--
CREATE TABLE Parametres (
  nomParametre  VARCHAR(32) UNIQUE NOT NULL,
  valeur        TEXT,

  PRIMARY KEY (nomParametre)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


--
-- Creation d'une table 'modeleExcel'
--
CREATE TABLE ModeleExcel (
  nomFichier  VARCHAR(32) UNIQUE NOT NULL,
  ligneHeader INTEGER DEFAULT NULL,
  colId       INTEGER DEFAULT NULL,
  colNom      INTEGER DEFAULT NULL,
  colNote INTEGER DEFAULT NULL,
  colCom  INTEGER DEFAULT NULL,
  colMoyenne  INTEGER DEFAULT NULL,
  colGrade    INTEGER DEFAULT NULL,

  PRIMARY KEY(nomFichier)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Structure de la table Reunion*/
CREATE TABLE IF NOT EXISTS Reunion(
  idReunion int PRIMARY KEY AUTO_INCREMENT,
  dateReunion DATETIME,
  compteRendu varchar(200),
  titre varchar(30),
  description varchar(80),
  lieu varchar(30),
  refUtilisateur int,
  FOREIGN KEY(refUtilisateur) REFERENCES Utilisateur(idUtilisateur)
)ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

/*Structure de la table Invite_Reunion*/
CREATE TABLE IF NOT EXISTS Invite_Reunion(
  participe boolean,
  refReunion int,
  refUtilisateur int,
  FOREIGN KEY(refReunion) REFERENCES Reunion(idReunion),
  FOREIGN KEY(refUtilisateur) REFERENCES Utilisateur(idUtilisateur),
  PRIMARY KEY(refReunion, refUtilisateur)
);
USE somanager;

SET default_storage_engine=INNODB;

/*Structure de la table Pgl Sprint*/
CREATE TABLE IF NOT EXISTS pgl_sprint (
  idSprint INTEGER AUTO_INCREMENT,
  numero TINYINT NOT NULL,
  dateDebut DATE NOT NULL,
  dateFin DATE NOT NULL,
  nbrHeures INTEGER DEFAULT NULL,
  coefficient FLOAT DEFAULT 1.0,
  refAnnee INTEGER NOT NULL,
  notesPubliees BOOLEAN DEFAULT FALSE,

  PRIMARY KEY(idSprint),
  FOREIGN KEY (refAnnee) REFERENCES AnneeScolaire(idAnneeScolaire) ON DELETE CASCADE
);

/*Structure de la table Matière*/
CREATE TABLE IF NOT EXISTS pgl_matiere (
  idMatiere INTEGER AUTO_INCREMENT,
  libelle VARCHAR(40) NOT NULL,
  coefficient FLOAT NOT NULL DEFAULT 1.0,
  refSprint INTEGER NOT NULL,
  PRIMARY KEY (idMatiere),
  FOREIGN KEY (refSprint) REFERENCES pgl_sprint(idSprint) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS pgl_projet(
  idProjet int(5) NOT NULL AUTO_INCREMENT,
  titre varchar(30) DEFAULT NULL,
  description varchar(80) DEFAULT NULL,
  idProfReferent int(5),
  PRIMARY KEY (idProjet),
  FOREIGN KEY (idProfReferent) REFERENCES Professeur(idProfesseur)
);

CREATE TABLE IF NOT EXISTS pgl_soutenance(
  idSoutenance int(5) NOT NULL AUTO_INCREMENT,
  date datetime DEFAULT NULL,
  libelle varchar(30) DEFAULT NULL,
  PRIMARY KEY (idSoutenance)
);

CREATE TABLE IF NOT EXISTS pgl_compositionjury(
  refSoutenance int(5) NOT NULL,
  refProfesseur int(5) NOT NULL,
  PRIMARY KEY (refSoutenance,refProfesseur),
  FOREIGN KEY (refSoutenance) REFERENCES pgl_soutenance(idSoutenance) ON DELETE CASCADE,
  FOREIGN KEY (refProfesseur) REFERENCES Professeur(idProfesseur) ON DELETE CASCADE
);

/*Structure de la table Equipe*/
CREATE TABLE IF NOT EXISTS pgl_equipe(
  idEquipe INTEGER AUTO_INCREMENT,
  nom VARCHAR(10) NOT NULL,
  refProjet INTEGER DEFAULT NULL,
  refAnnee INTEGER NOT NULL,
  PRIMARY KEY(idEquipe),
  FOREIGN KEY(refProjet) REFERENCES pgl_projet(idProjet),
  FOREIGN KEY(refAnnee) REFERENCES anneescolaire(idAnneeScolaire) ON DELETE CASCADE
);

/*Structure de la table Etudiant*/
CREATE TABLE IF NOT EXISTS pgl_etudiant (
  idEtudiant INTEGER,
  refAnnee INTEGER DEFAULT NULL,
  PRIMARY KEY(idEtudiant),
  FOREIGN KEY(refAnnee) REFERENCES anneescolaire(idAnneeScolaire) ON DELETE CASCADE,
  FOREIGN KEY(idEtudiant) REFERENCES utilisateur(idUtilisateur) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS pgl_roleequipe(
  refEquipe INTEGER NOT NULL,
  refUtilisateur INTEGER NOT NULL,
  refRole INTEGER NOT NULL,

  PRIMARY KEY (refEquipe, refUtilisateur, refRole),
  FOREIGN KEY (refEquipe) REFERENCES pgl_equipe(idEquipe) ON DELETE CASCADE,
  FOREIGN KEY (refUtilisateur) REFERENCES utilisateur(idUtilisateur) ON DELETE CASCADE,
  FOREIGN KEY (refRole) REFERENCES role(idRole) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS pgl_evalue(
  refSoutenance int(5) NOT NULL,
  refEquipe int(5) NOT NULL,
  PRIMARY KEY (refSoutenance,refEquipe),
  FOREIGN KEY (refSoutenance) REFERENCES pgl_soutenance(idSoutenance) ON DELETE CASCADE,
  FOREIGN KEY (refEquipe) REFERENCES pgl_equipe(idEquipe) ON DELETE CASCADE
);

/*Structure de la table noteEquipe*/
CREATE TABLE IF NOT EXISTS pgl_note (
  idNote INTEGER AUTO_INCREMENT,
  note FLOAT NOT NULL,
  refMatiere INTEGER NOT NULL,
  refEtudiant INTEGER NOT NULL,
  refEvaluateur INTEGER NOT NULL,
  PRIMARY KEY (idNote),
  FOREIGN KEY (refMatiere) REFERENCES pgl_matiere(idMatiere) ON DELETE CASCADE,
  FOREIGN KEY (refEvaluateur) REFERENCES utilisateur(idUtilisateur) ON DELETE CASCADE,
  FOREIGN KEY (refEtudiant) REFERENCES pgl_etudiant(idEtudiant),
  CONSTRAINT UC_NoteMatiereEvaluateur UNIQUE (refMatiere,refEtudiant,refEvaluateur)
);

/*Structure de la table BonusMalus*/
CREATE TABLE IF NOT EXISTS pgl_bonusmalus(
  idBonusMalus INTEGER AUTO_INCREMENT,
  valeur FLOAT NOT NULL,
  validation ENUM('attente', 'refuser', 'accepter') DEFAULT 'attente',
  justification VARCHAR(250) NOT NULL,
  refSprint INTEGER NOT NULL,
  refEtudiant INTEGER NOT NULL,
  refEvaluateur INTEGER NOT NULL,
  PRIMARY KEY (idBonusMalus),
  FOREIGN KEY(refSprint) REFERENCES pgl_sprint(idSprint) ON DELETE CASCADE,
  FOREIGN KEY(refEtudiant) REFERENCES pgl_etudiant(idEtudiant) ON DELETE CASCADE,
  FOREIGN KEY(refEvaluateur) REFERENCES utilisateur(idUtilisateur)
);

--
-- Structure de la table EtudiantEquipe
--
CREATE TABLE IF NOT EXISTS pgl_etudiantequipe(
  idEtudiant INTEGER NOT NULL,
  idEquipe INTEGER NOT NULL,
  idSprint INTEGER NOT NULL,
  PRIMARY KEY (idEtudiant, idSprint),
  FOREIGN KEY(idEtudiant) REFERENCES pgl_etudiant(idEtudiant) ON DELETE CASCADE,
  FOREIGN KEY(idEquipe) REFERENCES pgl_equipe(idEquipe) ON DELETE CASCADE,
  FOREIGN KEY(idSprint) REFERENCES pgl_sprint(idSprint) ON DELETE CASCADE
);

--
-- Structure de la table `demandejury`
--

CREATE TABLE IF NOT EXISTS `demandejury` (
  `idDemande` int(5) NOT NULL AUTO_INCREMENT,
  `sujetsConcernes` text NOT NULL,
  `commentaire` text,
  `nomOption` enum('LD','SE','OC','BIO','NRJ','CC','IIT','DSMT','BD') NOT NULL,
  PRIMARY KEY (`idDemande`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Structure de la table `professeurdemande`
--

CREATE TABLE IF NOT EXISTS `professeurdemande` (
  `idProfesseur` int(5) NOT NULL,
  `idDemande` int(5) NOT NULL,
  PRIMARY KEY (`idProfesseur`,`idDemande`),
  KEY `ProfesseurDemande_ibfk_2` (`idDemande`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contraintes pour la table `professeurdemande`
--
ALTER TABLE `professeurdemande`
  ADD CONSTRAINT `ProfesseurDemande_ibfk_1` FOREIGN KEY (`idProfesseur`) REFERENCES `professeur` (`idProfesseur`) ON DELETE CASCADE,
  ADD CONSTRAINT `ProfesseurDemande_ibfk_2` FOREIGN KEY (`idDemande`) REFERENCES `demandejury` (`idDemande`) ON DELETE CASCADE;


--
--
-- Trigger pour inserer les matieres par défaut dans les sprints
--
DELIMITER //
CREATE TRIGGER `MatieresDefaut` AFTER INSERT ON pgl_sprint
  FOR EACH ROW BEGIN
  INSERT INTO pgl_matiere(libelle, coefficient, refSprint) VALUES('Soutenance matin', 1, NEW.idSprint);
  INSERT INTO pgl_matiere(libelle, coefficient, refSprint) VALUES('Soutenance après-midi', 1, NEW.idSprint);
END//
DELIMITER ;


--
-- Vue pour les notes moyennes
-- Affiche la moyenne pour chaque matière d'un etudiant, par sprint
--
CREATE OR REPLACE VIEW pgl_moyennesEtudiantsMatieres AS
  SELECT n.refEtudiant, u.nom, u.prenom, m.libelle, s.idSprint, a.anneeDebut, AVG(n.note) note, n.refMatiere, n.refEvaluateur, NULL idNote
  FROM pgl_note n
    JOIN pgl_etudiant e ON n.refEtudiant = e.idEtudiant
    JOIN pgl_matiere m ON m.idMatiere = n.refMatiere
    JOIN utilisateur u ON u.idUtilisateur = e.idEtudiant
    JOIN pgl_sprint s ON s.idSprint = m.refSprint
    JOIN anneescolaire a ON a.idAnneeScolaire = s.refAnnee
  GROUP BY s.idSprint, m.libelle, e.idEtudiant
  ORDER BY n.refMatiere ASC;


--
-- Vue pour les notes moyennes par sprint
-- Affiche la moyenne obtenue par sprint par etudiant
-- Le bonusMalus affiché est DEJA appliqué à la note
--

CREATE OR REPLACE VIEW pgl_moyennesEtudiantsSprint AS
  SELECT pmEM.refEtudiant, s.idSprint, s.numero, CONCAT('Sprint ', s.numero) libelle, s.coefficient, nom, prenom, pmEM.anneeDebut, NULL idNote,
                                                 GREATEST(0, LEAST(20,
                                                                   ((SUM(pmEM.note * m.coefficient) / SUM(m.coefficient) +
                                                                     IFNULL(SUM(IFNULL(bm.valeur, 0)) * COUNT(DISTINCT bm.idBonusMalus) / COUNT(bm.idBonusMalus), 0)
                                                                   ))
                                                 )) note,
                                                 IFNULL((SUM(IFNULL(bm.valeur,0)) * COUNT(DISTINCT bm.idBonusMalus) / COUNT(bm.idBonusMalus)), 0) bonusMalus

  FROM pgl_moyennesEtudiantsMatieres pmEM
    JOIN pgl_matiere m ON m.idMatiere = pmEM.refMatiere
    JOIN pgl_sprint s ON s.idSprint = m.refSprint
    LEFT JOIN pgl_bonusmalus bm ON bm.refEtudiant = pmEM.refEtudiant AND bm.refSprint = s.idSprint
  GROUP BY pmEM.refEtudiant, pmEM.idSprint;

--
-- Vue pour les notes
-- Affiche toutes les notes obtenues par un étudiant
--
CREATE OR REPLACE VIEW pgl_notesEtudiants AS
  SELECT a.idAnneeScolaire, s.idSprint, ee.idEquipe, u.idUtilisateur, u.nom, u.prenom, n.idNote, n.note, m.idMatiere, m.libelle, m.coefficient, eval.idUtilisateur idEvaluateur, s.notesPubliees
  FROM utilisateur u
    JOIN pgl_etudiant e ON u.idUtilisateur = e.idEtudiant
    JOIN anneescolaire a ON a.idAnneeScolaire = e.refAnnee
    JOIN pgl_sprint s ON s.refAnnee = a.idAnneeScolaire
    JOIN pgl_matiere m ON m.refSprint = s.idSprint
    JOIN pgl_etudiantequipe ee ON ee.idEtudiant = e.idEtudiant
    LEFT JOIN pgl_note n ON n.refMatiere = m.idMatiere AND n.refEtudiant = e.idEtudiant
    LEFT JOIN utilisateur eval ON eval.idUtilisateur = n.refEvaluateur
  ORDER BY s.numero, u.idUtilisateur ASC;

--
-- Récupère la liste des évaluateurs qui ont ajoutés une note par matière
--
CREATE OR REPLACE VIEW pgl_matieresEtudiantEvaluees AS
  SELECT a.idAnneeScolaire, s.idSprint, ee.idEquipe, u.idUtilisateur, u.nom, u.prenom, m.idMatiere, m.libelle, s.notesPubliees,m.coefficient, GROUP_CONCAT(eval.idUtilisateur) evaluateurs
  FROM utilisateur u
    JOIN pgl_etudiant e ON u.idUtilisateur = e.idEtudiant
    JOIN anneescolaire a ON a.idAnneeScolaire = e.refAnnee
    JOIN pgl_sprint s ON s.refAnnee = a.idAnneeScolaire
    JOIN pgl_matiere m ON m.refSprint = s.idSprint
    JOIN pgl_etudiantequipe ee ON ee.idEtudiant = e.idEtudiant
    LEFT JOIN pgl_note n ON n.refEtudiant = e.idEtudiant AND n.refMatiere = m.idMatiere
    LEFT JOIN utilisateur eval ON eval.idUtilisateur = n.refEvaluateur
  GROUP BY u.idUtilisateur, m.idMatiere
  ORDER BY s.numero, u.idUtilisateur ASC;

USE somanager;

-- Rappel des id des roles
-- (1, 'etudiant'),
-- (2, 'admin'),
-- (3, 'prof'),
-- (4, 'profOption'),
-- (5, 'profResponsable'),
-- (6, 'assistant'),
-- (9, 'entrepriseExt'),
-- (10, 'serviceCom');

--  Rappel des id des options
-- (1, 'LD'),
-- (2, 'SE'),
-- (3, 'OC'),
-- (4, 'BIO'),
-- (5, 'NRJ'),
-- (6, 'CC'),
-- (7, 'IIT'),
-- (8, 'DSMT'),
-- (9, 'BD'),
-- (20, 'toutes options');
--
-- INSERTION DES SUJETS
--
INSERT INTO sujet (idSujet, titre, description, nbrMinEleves, nbrMaxEleves, contratPro, confidentialite, etat, liens, interets, noteInteretTechno, noteInteretSujet, idForm) VALUES
  (1, 'Prothèse d’articulation contrôlée', 'En vue du récent développement d’une nouvelle prothèse d’articulation, de nombreuses idées d’améliorations ont vu le jour.Entre autres, la possibilité de la contrôler via une application. Il faudrait alors étudier les composants à ajouter dans cette prothèse afin de donner un bilan de faisabilité pour le CHU d’Angers.', 0, 99, NULL, NULL, 'depose', 'http://www.nouvo.ch/2014/08/ma-prothèse-est-connectée', NULL, NULL, NULL, NULL),
  (2, 'ECG', 'De nombreuses interférences existent lorsque l’on veut l’ECG d’une personne portant un pacemaker. Ces interférences ont notamment lieu à cause du bruit parasite émis par le pacemaker.L’objectif est de mettre en place une solution (ou un début de solution) afin d’améliorer les ECG qui pourraient alors gérer et éliminer ces bruits.', 0, 99, NULL, NULL, 'depose', 'http://pacingdefibrillation.com/fr/comment/143', NULL, NULL, NULL, NULL),
  (3, 'Éolienne, panneaux solaires et roues hydrauliques pour tout un village', ' Certains villages situés dans les Pyrénées parviennent difficilement à avoir un apport en électricité constant. En effet, leur localisation rend difficile l’approvisionnement par les grandes entreprises françaises et l’hiver, ils se retrouvent (trop) régulièrement sans électricité. Il faudrait penser à une solution utilisant des ressources renouvelables qui pourraient charger des générateurs de secours, utilisés en cas d’incidents.', 0, 99, NULL, NULL, 'valide', 'http://sigesmpy.brgm.fr/spip.php?article36', NULL, NULL, NULL, NULL),
  (4, 'Puce de localisation basse consommation', 'Pouvoir localiser ses animaux devient de plus en plus populaire mais les puces doivent être activées par un signal pour transmettre leur position. L’idée est de penser une nouvelle puce qui émettrait en continue pour prévenir l’utilisateur lorsque son animal sort d’un périmètre précis (jardin, …) ou s’éloigne à plus d’une certaine distance de lui. Cependant, cette puce devra consommer au minimum d’énergie. ', 0, 99, NULL, NULL, 'depose', 'https://www.petpointer.ch/fr/', NULL, NULL, NULL, NULL),
  (5, 'Langage GO et intelligence artificielle', 'Face à l’affluence de demandes de coachs sportifs portatifs, la société xxx développe actuellement une nouvelle application intégrant de l’intelligence artificielle. Le coach virtuel proposé sera alors personnalisable en continue en fonction d’un algorithme particulier. De nouvelles technologies sont déployées pour mener à bien ce projet et entre autres, le Go lié à l’intelligence artificielle.', 0, 99, NULL, NULL, 'depose', 'http://www.body-op.com', NULL, NULL, NULL, NULL),
  (6, 'PROSE et réalité augmentée', ' ', 0, 99, NULL, NULL, 'attribue', 'http://annecyelectronique.fr/realite-virtuelle-immersive/', NULL, NULL, NULL, NULL),
  (7, 'Traverser l’argile', 'Lors de fouilles concernant de vieux bâtiments historiques, les chercheurs ont du mal à savoir ce qui se situe à l’intérieur des constructions. En effet, elles sont entièrement closes par de l’argile et leurs émetteurs d’ondes ne permettent pas encore de percevoir ce qu’il y a à l’intérieur sans détruire la structure en argile. Il faut alors travailler sur des filtres et de la modulation pour obtenir de nouvelles ondes.', 0, 99, NULL, NULL, 'depose', 'http://www.normalesup.org/~clanglois/Sciences_Terre/Argiles/Argiles0.html', NULL, NULL, NULL, NULL),
  (8, 'Amélioration de l’architecture réseau de l’ESEO', ' De nombreux problèmes de connexion et de charge ont lieu sur le réseau de l’ESEO, notamment pour la connexion sur le portail qui bloque au-delà d’un certain nombre de connexions. Il faut repenser l’architecture réseau mise en place il y a quelques années par l’ESEO en prenant en compte les contraintes énoncées par l’équipe de gestion informatique de l’école.', 0, 99, NULL, NULL, 'depose', 'http://CHU.com', NULL, NULL, NULL, NULL),
  (10, 'Projet Alpha', 'Le projet alpha est un projet qui a été soumis à but de test', 2, 5, 0, 1, 'depose', '', 'Gestion de projet', NULL, NULL, 1),
  (11, 'Projet beta', 'La description du projet BETA', 1, 10, 1, 1, 'depose', 'unlien.fr', '', NULL, NULL, 2),
  (12, 'TEST SUBJECT', 'TEST DESC', 1, 2, 0, 0, 'depose', '', '', NULL, NULL, 3),
  (13, 'SUJET2', 'DESVC2', 3, 4, 0, 0, 'depose', '', '', NULL, NULL, 4),
  (14, 'SUJET', 'DESCRIPTION', 1, 5, 0, 0, 'depose', 'lien', '', NULL, NULL, 5),
  (15, 'Un nouveau sujet', 'Sujet de test', 2, 4, 1, 0, 'depose', '', '', NULL, NULL, 6);

--
-- INSERTION DES SUJETS
--
INSERT INTO optionsujets (idOption, idSujet) VALUES
  (1, 1);

--
-- INSERTION DES ANNEES SCOLAIRES
--
INSERT INTO AnneeScolaire (anneeDebut, anneeFin, dateDebutProjet, dateMiAvancement, dateDepotPoster, dateSoutenanceFinale, idOption)
VALUES (2017, 2018, '2017-09-01 01:00:00', '2018-01-01 01:00:00', '2018-06-01 01:00:00', '2018-06-25 01:00:00', 1);
INSERT INTO AnneeScolaire (anneeDebut, anneeFin, dateDebutProjet, dateMiAvancement, dateDepotPoster, dateSoutenanceFinale, idOption)
VALUES (2017, 2018, '2017-09-01 01:00:00', '2018-01-01 01:00:00', '2018-06-01 01:00:00', '2018-06-25 01:00:00', 2);
INSERT INTO AnneeScolaire (anneeDebut, anneeFin, dateDebutProjet, dateMiAvancement, dateDepotPoster, dateSoutenanceFinale, idOption)
VALUES (2017, 2018, '2017-09-01 01:00:00', '2018-01-01 01:00:00', '2018-06-01 01:00:00', '2018-06-25 01:00:00', 3);
INSERT INTO AnneeScolaire (anneeDebut, anneeFin, dateDebutProjet, dateMiAvancement, dateDepotPoster, dateSoutenanceFinale, idOption)
VALUES (2017, 2018, '2017-09-01 01:00:00', '2018-01-01 01:00:00', '2018-06-01 01:00:00', '2018-06-25 01:00:00', 4);
INSERT INTO AnneeScolaire (anneeDebut, anneeFin, dateDebutProjet, dateMiAvancement, dateDepotPoster, dateSoutenanceFinale, idOption)
VALUES (2017, 2018, '2017-09-01 01:00:00', '2018-01-01 01:00:00', '2018-06-01 01:00:00', '2018-06-25 01:00:00', 5);
INSERT INTO AnneeScolaire (anneeDebut, anneeFin, dateDebutProjet, dateMiAvancement, dateDepotPoster, dateSoutenanceFinale, idOption)
VALUES (2017, 2018, '2017-09-01 01:00:00', '2018-01-01 01:00:00', '2018-06-01 01:00:00', '2018-06-25 01:00:00', 6);
INSERT INTO AnneeScolaire (anneeDebut, anneeFin, dateDebutProjet, dateMiAvancement, dateDepotPoster, dateSoutenanceFinale, idOption)
VALUES (2017, 2018, '2017-09-01 01:00:00', '2018-01-01 01:00:00', '2018-06-01 01:00:00', '2018-06-25 01:00:00', 7);
INSERT INTO AnneeScolaire (anneeDebut, anneeFin, dateDebutProjet, dateMiAvancement, dateDepotPoster, dateSoutenanceFinale, idOption)
VALUES (2017, 2018, '2017-09-01 01:00:00', '2018-01-01 01:00:00', '2018-06-01 01:00:00', '2018-06-25 01:00:00', 8);

--
-- INSERTION DE L'UTILISATEUR 'TEST'
--
INSERT INTO Utilisateur (idUtilisateur, nom, prenom, identifiant, hash, email, valide)
VALUES (2, 'test', 'test', 'test', '$2a$10$IWaV3IPZ4h2IvTFIU4xn7eJKg4lOqzgXMKLFbi0s2vuEGin5ojsLa', 'test@test.fr', 'oui');

--
-- INSERTION DES ROLES UTILISATEUR
--
INSERT INTO RoleUtilisateur (idUtilisateur, idRole, idOption) VALUES (2, 1, 20);
INSERT INTO RoleUtilisateur (idUtilisateur, idRole, idOption) VALUES (2, 2, 20);
INSERT INTO RoleUtilisateur (idUtilisateur, idRole, idOption) VALUES (2, 3, 20);
INSERT INTO RoleUtilisateur (idUtilisateur, idRole, idOption) VALUES (2, 4, 20);
INSERT INTO RoleUtilisateur (idUtilisateur, idRole, idOption) VALUES (2, 5, 20);
INSERT INTO RoleUtilisateur (idUtilisateur, idRole, idOption) VALUES (2, 6, 20);
INSERT INTO RoleUtilisateur (idUtilisateur, idRole, idOption) VALUES (2, 9, 20);
INSERT INTO RoleUtilisateur (idUtilisateur, idRole, idOption) VALUES (2, 10, 20);

--
-- INSERTION DES AUTRES UTILISATEURS ET DE LEURS ROLES
--
INSERT INTO Utilisateur (idUtilisateur, nom, prenom, identifiant, hash, email, valide)
VALUES (3, 'prof', 'prof', 'prof', '$2a$10$IWaV3IPZ4h2IvTFIU4xn7eJKg4lOqzgXMKLFbi0s2vuEGin5ojsLa', 'prof@reseau.eseo.fr', 'oui');
INSERT INTO RoleUtilisateur (idUtilisateur, idRole, idOption) VALUES (3, 3, 20);

INSERT INTO Utilisateur (nom, prenom, identifiant, hash, email, valide)
VALUES ('profOption', 'profOption', 'profOption', '$2a$10$IWaV3IPZ4h2IvTFIU4xn7eJKg4lOqzgXMKLFbi0s2vuEGin5ojsLa', 'profoption@reseau.eseo.fr', 'oui');
INSERT INTO RoleUtilisateur (idUtilisateur, idRole, idOption) VALUES (4, 3, 20);
INSERT INTO RoleUtilisateur (idUtilisateur, idRole, idOption) VALUES (4, 4, 20);

INSERT INTO Utilisateur (nom, prenom, identifiant, hash, email, valide)
VALUES ('profRespOption', 'profRespOption', 'profRespOption', '$2a$10$IWaV3IPZ4h2IvTFIU4xn7eJKg4lOqzgXMKLFbi0s2vuEGin5ojsLa', 'profrespoption@reseau.eseo.fr', 'oui');
INSERT INTO RoleUtilisateur (idUtilisateur, idRole, idOption) VALUES (5, 5, 1);

INSERT INTO Utilisateur (nom, prenom, identifiant, hash, email, valide)
VALUES ('etudiant', 'etudiant', 'etudiant', '$2a$10$ctTt2vdEBHsAXYhJf/tzqO6ZjLI0TuwO.a1Qu2cLbkPPL9fmyQB6K', 'etudiant@reseau.eseo.fr', 'oui');
INSERT INTO RoleUtilisateur (idUtilisateur, idRole, idOption) VALUES (6, 1, 1);
INSERT INTO Etudiant(idEtudiant, annee) VALUES(6, 2017);

INSERT INTO Utilisateur (nom, prenom, identifiant, hash, email, valide)
VALUES ('assistant', 'assistant', 'assistant', '$2a$10$IWaV3IPZ4h2IvTFIU4xn7eJKg4lOqzgXMKLFbi0s2vuEGin5ojsLa', 'assistant@reseau.eseo.fr', 'oui');
INSERT INTO RoleUtilisateur (idUtilisateur, idRole, idOption) VALUES (7, 6, 20);

INSERT INTO Utilisateur (nom, prenom, identifiant, hash, email, valide)
VALUES ('entreprise', 'entreprise', 'entreprise', '$2a$10$IWaV3IPZ4h2IvTFIU4xn7eJKg4lOqzgXMKLFbi0s2vuEGin5ojsLa', 'entreprise@reseau.eseo.fr', 'oui');

INSERT INTO Utilisateur (nom, prenom, identifiant, hash, email, valide)
VALUES ('ALBERS', 'Patrick', 'Prof1', '$2a$10$IWaV3IPZ4h2IvTFIU4xn7eJKg4lOqzgXMKLFbi0s2vuEGin5ojsLa', 'Prof1@reseau.eseo.fr', 'oui');
INSERT INTO RoleUtilisateur (idUtilisateur, idRole, idOption) VALUES (9, 3, 1);

INSERT INTO Utilisateur (nom, prenom, identifiant, hash, email, valide)
VALUES ('BEAUDOUX', 'Olivier', 'Prof2', '$2a$10$IWaV3IPZ4h2IvTFIU4xn7eJKg4lOqzgXMKLFbi0s2vuEGin5ojsLa', 'Prof2@reseau.eseo.fr', 'oui');
INSERT INTO RoleUtilisateur (idUtilisateur, idRole, idOption) VALUES (10, 3, 1);

INSERT INTO Utilisateur (nom, prenom, identifiant, hash, email, valide)
VALUES ('CAMP', 'Olivier', 'Prof3', '$2a$10$IWaV3IPZ4h2IvTFIU4xn7eJKg4lOqzgXMKLFbi0s2vuEGin5ojsLa', 'Prof3@reseau.eseo.fr', 'oui');
INSERT INTO RoleUtilisateur (idUtilisateur, idRole, idOption) VALUES (11, 3, 1);

INSERT INTO Utilisateur (nom, prenom, identifiant, hash, email, valide)
VALUES ('CHHEL', 'Fabien', 'Prof8', '$2a$10$IWaV3IPZ4h2IvTFIU4xn7eJKg4lOqzgXMKLFbi0s2vuEGin5ojsLa', 'Prof8@reseau.eseo.fr', 'oui');
INSERT INTO RoleUtilisateur (idUtilisateur, idRole, idOption) VALUES (12, 3, 1);

INSERT INTO Utilisateur (nom, prenom, identifiant, hash, email, valide)
VALUES ('CLAVREUL', 'Mickael', 'Prof9', '$2a$10$IWaV3IPZ4h2IvTFIU4xn7eJKg4lOqzgXMKLFbi0s2vuEGin5ojsLa', 'Prof9@reseau.eseo.fr', 'oui');
INSERT INTO RoleUtilisateur (idUtilisateur, idRole, idOption) VALUES (13, 3, 1);

INSERT INTO Utilisateur (nom, prenom, identifiant, hash, email, valide)
VALUES ('GUTOWSKI', 'Nicolas', 'Prof4', '$2a$10$IWaV3IPZ4h2IvTFIU4xn7eJKg4lOqzgXMKLFbi0s2vuEGin5ojsLa', 'Prof4@reseau.eseo.fr', 'oui');
INSERT INTO RoleUtilisateur (idUtilisateur, idRole, idOption) VALUES (14, 3, 1);

INSERT INTO Utilisateur (nom, prenom, identifiant, hash, email, valide)
VALUES ('HAMMOUDI', 'Slimane', 'Prof5', '$2a$10$IWaV3IPZ4h2IvTFIU4xn7eJKg4lOqzgXMKLFbi0s2vuEGin5ojsLa', 'Prof5@reseau.eseo.fr', 'oui');
INSERT INTO RoleUtilisateur (idUtilisateur, idRole, idOption) VALUES (15, 3, 1);

INSERT INTO Utilisateur (nom, prenom, identifiant, hash, email, valide)
VALUES ('ROUSSEAU', 'Sophie', 'Prof6', '$2a$10$IWaV3IPZ4h2IvTFIU4xn7eJKg4lOqzgXMKLFbi0s2vuEGin5ojsLa', 'Prof6@reseau.eseo.fr', 'oui');
INSERT INTO RoleUtilisateur (idUtilisateur, idRole, idOption) VALUES (16, 3, 1);

INSERT INTO Utilisateur (nom, prenom, identifiant, hash, email, valide)
VALUES ('SCHANG', 'Daniel', 'Prof7', '$2a$10$IWaV3IPZ4h2IvTFIU4xn7eJKg4lOqzgXMKLFbi0s2vuEGin5ojsLa', 'Prof7@reseau.eseo.fr', 'oui');
INSERT INTO RoleUtilisateur (idUtilisateur, idRole, idOption) VALUES (17, 3, 1);

INSERT INTO Utilisateur (nom, prenom, identifiant, hash, email, valide)
VALUES ('Etu1', 'Etu1', 'Etu1', '$2a$10$IWaV3IPZ4h2IvTFIU4xn7eJKg4lOqzgXMKLFbi0s2vuEGin5ojsLa', 'Etu1@reseau.eseo.fr', 'oui');
INSERT INTO RoleUtilisateur (idUtilisateur, idRole, idOption) VALUES (18, 1, 1);
INSERT INTO Etudiant(idEtudiant, annee) VALUES(18, 2017);

INSERT INTO Utilisateur (nom, prenom, identifiant, hash, email, valide)
VALUES ('Etu2', 'Etu2', 'Etu2', '$2a$10$IWaV3IPZ4h2IvTFIU4xn7eJKg4lOqzgXMKLFbi0s2vuEGin5ojsLa', 'Etu2@reseau.eseo.fr', 'oui');
INSERT INTO RoleUtilisateur (idUtilisateur, idRole, idOption) VALUES (19, 1, 2);
INSERT INTO Etudiant(idEtudiant, annee) VALUES(19, 2017);

INSERT INTO Utilisateur (nom, prenom, identifiant, hash, email, valide)
VALUES ('Etu3', 'Etu3', 'Etu3', '$2a$10$IWaV3IPZ4h2IvTFIU4xn7eJKg4lOqzgXMKLFbi0s2vuEGin5ojsLa', 'Etu3@reseau.eseo.fr', 'oui');
INSERT INTO RoleUtilisateur (idUtilisateur, idRole, idOption) VALUES (20, 1, 3);
INSERT INTO Etudiant(idEtudiant, annee) VALUES(20, 2017);

INSERT INTO Utilisateur (nom, prenom, identifiant, hash, email, valide)
VALUES ('Etu4', 'Etu4', 'Etu4', '$2a$10$IWaV3IPZ4h2IvTFIU4xn7eJKg4lOqzgXMKLFbi0s2vuEGin5ojsLa', 'Etu4@reseau.eseo.fr', 'oui');
INSERT INTO RoleUtilisateur (idUtilisateur, idRole, idOption) VALUES (21, 1, 4);
INSERT INTO Etudiant(idEtudiant, annee) VALUES(21, 2017);

INSERT INTO Utilisateur (nom, prenom, identifiant, hash, email, valide)
VALUES ('Etu5', 'Etu5', 'Etu5', '$2a$10$IWaV3IPZ4h2IvTFIU4xn7eJKg4lOqzgXMKLFbi0s2vuEGin5ojsLa', 'Etu5@reseau.eseo.fr', 'oui');
INSERT INTO RoleUtilisateur (idUtilisateur, idRole, idOption) VALUES (22, 1, 5);
INSERT INTO Etudiant(idEtudiant, annee) VALUES(22, 2017);

INSERT INTO Utilisateur (nom, prenom, identifiant, hash, email, valide)
VALUES ('Etu6', 'Etu6', 'Etu6', '$2a$10$IWaV3IPZ4h2IvTFIU4xn7eJKg4lOqzgXMKLFbi0s2vuEGin5ojsLa', 'Etu6@reseau.eseo.fr', 'oui');
INSERT INTO RoleUtilisateur (idUtilisateur, idRole, idOption) VALUES (23, 1, 6);
INSERT INTO Etudiant(idEtudiant, annee) VALUES(23, 2017);

INSERT INTO Utilisateur (nom, prenom, identifiant, hash, email, valide)
VALUES ('Etu7', 'Etu7', 'Etu7', '$2a$10$IWaV3IPZ4h2IvTFIU4xn7eJKg4lOqzgXMKLFbi0s2vuEGin5ojsLa', 'Etu7@reseau.eseo.fr', 'oui');
INSERT INTO RoleUtilisateur (idUtilisateur, idRole, idOption) VALUES (24, 1, 7);
INSERT INTO Etudiant(idEtudiant, annee) VALUES(24, 2017);

INSERT INTO Utilisateur (nom, prenom, identifiant, hash, email, valide)
VALUES ('Etu8', 'Etu8', 'Etu8', '$2a$10$IWaV3IPZ4h2IvTFIU4xn7eJKg4lOqzgXMKLFbi0s2vuEGin5ojsLa', 'Etu8@reseau.eseo.fr', 'oui');
INSERT INTO RoleUtilisateur (idUtilisateur, idRole, idOption) VALUES (25, 1, 8);
INSERT INTO Etudiant(idEtudiant, annee) VALUES(25, 2017);

INSERT INTO `utilisateur` (`idUtilisateur`, `nom`, `prenom`, `identifiant`, `hash`, `email`, `valide`)
VALUES (30, 'PRODUCT', 'Owner', 'productOwner', '$2a$10$IWaV3IPZ4h2IvTFIU4xn7eJKg4lOqzgXMKLFbi0s2vuEGin5ojsLa', 'product.owner@reseau.eseo.fr', 'oui');
INSERT INTO RoleUtilisateur (idUtilisateur, idRole, idOption) VALUES (30, 12, 1);

ALTER TABLE utilisateur AUTO_INCREMENT=100;

INSERT INTO utilisateur(idUtilisateur, nom, prenom, identifiant, hash, email, valide)
VALUES (100,'Vergote','Anne-Claire','vergote','$2a$10$IWaV3IPZ4h2IvTFIU4xn7eJKg4lOqzgXMKLFbi0s2vuEGin5ojsLa','anneclaire.vergote@reseau.eseo.fr','oui');
INSERT INTO RoleUtilisateur (idUtilisateur, idRole, idOption) VALUES (100, 1, 1);

INSERT INTO utilisateur(idUtilisateur, nom, prenom, identifiant, hash, email, valide)
VALUES (101,'Pichavant','Quentin','pichavant','$2a$10$IWaV3IPZ4h2IvTFIU4xn7eJKg4lOqzgXMKLFbi0s2vuEGin5ojsLa','quentin.pichavant@reseau.eseo.fr','oui');
INSERT INTO RoleUtilisateur (idUtilisateur, idRole, idOption) VALUES (101, 1, 1);

INSERT INTO utilisateur(idUtilisateur, nom, prenom, identifiant, hash, email, valide)
VALUES (102,'Le Gacque','Tristan','legacque','$2a$10$IWaV3IPZ4h2IvTFIU4xn7eJKg4lOqzgXMKLFbi0s2vuEGin5ojsLa','tristan.legacque@reseau.eseo.fr','oui');
INSERT INTO RoleUtilisateur (idUtilisateur, idRole, idOption) VALUES (102, 1, 1);

INSERT INTO utilisateur(idUtilisateur, nom, prenom, identifiant, hash, email, valide)
VALUES (103,'Scuiller','Gaetan','scuiller','$2a$10$IWaV3IPZ4h2IvTFIU4xn7eJKg4lOqzgXMKLFbi0s2vuEGin5ojsLa','gaetan.scuiller@reseau.eseo.fr','oui');
INSERT INTO RoleUtilisateur (idUtilisateur, idRole, idOption) VALUES (103, 1, 1);

INSERT INTO utilisateur(idUtilisateur, nom, prenom, identifiant, hash, email, valide)
VALUES (104,'Camp','Olivier','camp','$2a$10$IWaV3IPZ4h2IvTFIU4xn7eJKg4lOqzgXMKLFbi0s2vuEGin5ojsLa','olivier.camp@eseo.fr','oui');

INSERT INTO utilisateur(idUtilisateur, nom, prenom, identifiant, hash, email, valide)
VALUES (105,'Mamalet','Emilien','mamalet','$2a$10$IWaV3IPZ4h2IvTFIU4xn7eJKg4lOqzgXMKLFbi0s2vuEGin5ojsLa','emilien.mamalet@reseau.eseo.fr','oui');
INSERT INTO RoleUtilisateur (idUtilisateur, idRole, idOption) VALUES (105, 1, 1);

INSERT INTO utilisateur(idUtilisateur, nom, prenom, identifiant, hash, email, valide)
VALUES (106,'Maymard','Damien','maymard','$2a$10$IWaV3IPZ4h2IvTFIU4xn7eJKg4lOqzgXMKLFbi0s2vuEGin5ojsLa','damien.maymard@reseau.eseo.fr','oui');
INSERT INTO RoleUtilisateur (idUtilisateur, idRole, idOption) VALUES (106, 1, 1);

INSERT INTO utilisateur(idUtilisateur, nom, prenom, identifiant, hash, email, valide)
VALUES (107,'Ortiz Betancur','Juan Carlo','ortiz','$2a$10$IWaV3IPZ4h2IvTFIU4xn7eJKg4lOqzgXMKLFbi0s2vuEGin5ojsLa','juancarlo.ortizbetancur@reseau.eseo.fr','oui');
INSERT INTO RoleUtilisateur (idUtilisateur, idRole, idOption) VALUES (107, 1, 1);

INSERT INTO utilisateur(idUtilisateur, nom, prenom, identifiant, hash, email, valide)
VALUES (108,'Jarneau','Dimitri','jarneau','$2a$10$IWaV3IPZ4h2IvTFIU4xn7eJKg4lOqzgXMKLFbi0s2vuEGin5ojsLa','dimitri.jarneau@reseau.eseo.fr','oui');
INSERT INTO RoleUtilisateur (idUtilisateur, idRole, idOption) VALUES (108, 1, 1);

INSERT INTO utilisateur(idUtilisateur, nom, prenom, identifiant, hash, email, valide)
VALUES (109,'WOODWARD','Richard','woodward','$2a$10$IWaV3IPZ4h2IvTFIU4xn7eJKg4lOqzgXMKLFbi0s2vuEGin5ojsLa','WOODWARD.richard@gmail.com','oui');
INSERT INTO `RoleUtilisateur` (`idUtilisateur`, `idRole`, `idOption`) VALUES ('109', '13', '1');

--
-- INSERTION DES EQUIPES
--
INSERT INTO equipe (idEquipe, annee, taille, idSujet, valide) VALUES ('', '2017', '2', '1', 'oui');

--
-- INSERTION DES PROFESSEURS SUJETS
--
INSERT INTO porteursujet (idSujet, idUtilisateur, annee) VALUES
  (1, 2, NULL);

INSERT INTO ProfesseurSujet(idProfesseur, idSujet, fonction, valide)
VALUES (2, 4, 'référent', 'oui');

INSERT INTO ProfesseurSujet(idProfesseur, idSujet, fonction, valide)
VALUES (3, 6, 'référent', 'oui');

INSERT INTO ProfesseurSujet(idProfesseur, idSujet, fonction, valide)
VALUES (4, 10, 'référent', 'non');

INSERT INTO professeursujet (idProfesseur, idSujet, fonction, valide) VALUES
  (17, 1, 'co-encadrant', 'oui');


--
-- INSERTION DES POSTERS
--
INSERT INTO poster (idPoster, chemin, datePoster, idSujet, valide, idEquipe) VALUES
  (1, 'aucun', '2018-04-18 00:00:00', 1, 'oui', '2017_001');




-- INSERTION PROF REFERENT
INSERT INTO `professeur`(`idProfesseur`) VALUES (103);


-- ANNEE DEJA INSEREE POUR OPTION LD AVEC ID=1

-- INSERTION SPRINT
INSERT INTO `pgl_sprint`(`idSprint`, `numero`, `dateDebut`, `dateFin`, `nbrHeures`, `coefficient`, `refAnnee`) VALUES
  (01, 1, '2018-02-01', '2018-04-01', 24, 1, 1),
  (02, 2, '2018-04-02', '2018-05-01', 30, 2, 1),
  (03, 3, '2018-05-02', '2018-06-01', 40, 3, 1);


-- INSERTION PROJET EQUIPE BETA
INSERT INTO `pgl_projet`(`idProjet`, `titre`, `description`, `idProfReferent`) VALUES (1,'Base de donnees relationnelles','projet ld',103);

-- INSERTION PROJET EQUIPE ALPHA
INSERT INTO `pgl_projet`(`idProjet`, `titre`, `description`, `idProfReferent`) VALUES (2,'la qualité logiciel','projet de test',103);

-- INSERTION EQUIPE BETA
INSERT INTO `pgl_equipe`(`idEquipe`, `nom`, `refProjet`, `refAnnee`) VALUES (1,'Alpha',1,1);

-- INSERTION EQUIPE ALPHA
INSERT INTO `pgl_equipe`(`idEquipe`, `nom`, `refProjet`, `refAnnee`) VALUES (2,'Beta',2,1);

-- INSERTION ETUDIANT PGL
INSERT INTO `pgl_etudiant`(`idEtudiant`, `refAnnee`) VALUES (106,1);
INSERT INTO `pgl_etudiant`(`idEtudiant`, `refAnnee`) VALUES (107,1);
INSERT INTO `pgl_etudiant`(`idEtudiant`, `refAnnee`) VALUES (108,1);

-- INSERTION ETUDIANT EQUIPE BETA
INSERT INTO `pgl_etudiant`(`idEtudiant`, `refAnnee`) VALUES (100,1);
INSERT INTO `pgl_etudiant`(`idEtudiant`, `refAnnee`) VALUES (101,1);
INSERT INTO `pgl_etudiant`(`idEtudiant`, `refAnnee`) VALUES (105,1);

-- INSERTION ETUDIANT EQUIPE ALPHA
INSERT INTO `pgl_etudiant`(`idEtudiant`, `refAnnee`) VALUES (102,1);
INSERT INTO `pgl_etudiant`(`idEtudiant`, `refAnnee`) VALUES (103,1);

-- INSERTION DANS LES EQUIPES
INSERT INTO pgl_etudiantequipe(idEtudiant, idEquipe, idSprint) VALUES
  (100, 1, 1),
  (101, 1, 1),
  (102, 2, 1),
  (103, 2, 1);

-- INSERTION MATIERE Power Point pour le sprint 1
INSERT INTO `pgl_matiere`(`idMatiere`, `libelle`, `coefficient`, `refSprint`) VALUES (7, 'power point',1, 1);

-- INSERTION MATIERE Presentation pour le sprint 1
INSERT INTO `pgl_matiere`(`idMatiere`, `libelle`, `coefficient`, `refSprint`) VALUES (8, 'presentation',1, 1);

-- INSERTION MATIERE Note technique pour le sprint 1
INSERT INTO `pgl_matiere`(`idMatiere`, `libelle`, `coefficient`, `refSprint`) VALUES (9, 'note technique',3, 1);



-- INSERTION BONUS MALUS

INSERT INTO `pgl_bonusmalus`(`idBonusMalus`, `valeur`, `validation`,  `justification`, `refSprint`, `refEtudiant`, `refEvaluateur`) VALUES (1, 0, null, 'attente', 1, 100, 100);

INSERT INTO `pgl_bonusmalus`(`idBonusMalus`, `valeur`, `validation`,  `justification`, `refSprint`, `refEtudiant`, `refEvaluateur`) VALUES (2, 0, null, 'refuser', 1, 101, 100);

INSERT INTO `pgl_bonusmalus`(`idBonusMalus`, `valeur`, `validation`,  `justification`, `refSprint`, `refEtudiant`, `refEvaluateur`) VALUES (3, 0, null, 'attente', 1, 102, 102);

INSERT INTO `pgl_bonusmalus`(`idBonusMalus`, `valeur`, `validation`,  `justification`, `refSprint`, `refEtudiant`, `refEvaluateur`) VALUES (4, 0, null, 'accepter', 1, 103, 102);

INSERT INTO `pgl_bonusmalus`(`idBonusMalus`, `valeur`, `validation`,  `justification`, `refSprint`, `refEtudiant`, `refEvaluateur`) VALUES (5, 0, null, 'refuser', 1, 105, 100);



-- ROLEEQUIPE

INSERT INTO `pgl_roleequipe`(`refEquipe`, `refUtilisateur`, `refRole`) VALUES (1,100,11);
INSERT INTO `pgl_roleequipe`(`refEquipe`, `refUtilisateur`, `refRole`) VALUES (1,103,11);

--
-- Contenu de la table `pgl_soutenance`
--

INSERT INTO `pgl_soutenance` (`idSoutenance`, `date`, `libelle`) VALUES
  (1, '2018-02-09 08:00:00', 'SprintReview'),
  (2, '2018-04-18 08:08:00', 'SprintReview'),
  (3, '2018-05-05 15:00:00', 'SprintReview');

--
-- Contenu de la table `pgl_compositionjury`
--

INSERT INTO `pgl_compositionjury` (`refSoutenance`, `refProfesseur`) VALUES
  (1, 9),
  (1, 10),
  (1, 11),
  (1, 12),
  (2, 13),
  (2, 14),
  (2, 15),
  (2, 16),
  (3, 17);


INSERT INTO `RoleUtilisateur`(`idUtilisateur`, `idRole`, `idOption`) VALUES (2,11,20);
INSERT INTO `RoleUtilisateur`(`idUtilisateur`, `idRole`, `idOption`) VALUES (2,12,20);
INSERT INTO `RoleUtilisateur`(`idUtilisateur`, `idRole`, `idOption`) VALUES (2,13,20);
INSERT INTO `RoleUtilisateur`(`idUtilisateur`, `idRole`, `idOption`) VALUES (104,13,20);
INSERT INTO `RoleUtilisateur`(`idUtilisateur`, `idRole`, `idOption`) VALUES (104,12,20);

INSERT INTO `pgl_etudiantequipe`(`idEtudiant`, `idEquipe`, `idSprint`) VALUES (105,1,1);

-- Insertion de notes
INSERT INTO pgl_note(note, refMatiere, refEtudiant, refEvaluateur) VALUES
  (12, 1, 100, 105),
  (14, 2, 100, 105),
  (1, 4, 100, 105),
  (1, 3, 101, 104),
  (18, 1, 102, 103),
  (13.2, 3, 102, 103);

-- Insertion Equipes PFE
INSERT INTO `equipe` (`annee`, `taille`, `idSujet`, `valide`) VALUES
  (2017, 2, 2, 'oui'),
  (2017, 2, 3, 'oui'),
  (2017, 2, 4, 'oui'),
  (2017, 2, 5, 'oui'),
  (2017, 2, 6, 'oui'),
  (2017, 2, 7, 'oui'),
  (2017, 2, 8, 'oui'),
  (2017, 2, 10, 'oui'),
  (2017, 2, 11, 'oui');


-- Insertion Poster
INSERT INTO `poster` (`chemin`, `datePoster`, `idSujet`, `valide`, `idEquipe`) VALUES
  ('/home/etudiant/Posters/2018/1_architecte_logiciels_et_does.pdf', '2018-05-18 16:47:56', 2, 'oui', '2017_002'),
  ('/home/etudiant/Posters/2018/1_architecte_logicls_et_does.pdf', '2018-05-15 16:47:56', 3, 'oui', '2017_003'),
  ('/home/etudiant/Posters/2018/1_architecte_logicls_et_does.pdf', '2018-05-15 16:47:56', 4, 'oui', '2017_004'),
  ('/home/etudiant/Posters/2018/1_architecte_logicls_et_does.pdf', '2018-05-15 16:47:56', 5, 'oui', '2017_005'),
  ('/home/etudiant/Posters/2018/1_architecte_logicls_et_does.pdf', '2018-05-15 16:47:56', 6, 'oui', '2017_006'),
  ('/home/etudiant/Posters/2018/1_architecte_logicls_et_does.pdf', '2018-05-15 16:47:56', 7, 'oui', '2017_007'),
  ('/home/etudiant/Posters/2018/1_architecte_logicls_et_does.pdf', '2018-05-15 16:47:56', 8, 'oui', '2017_008'),
  ('/home/etudiant/Posters/2018/1_architecte_logicls_et_does.pdf', '2018-05-15 16:47:56', 10, 'oui', '2017_009');

-- Roles
INSERT INTO pgl_roleequipe(refEquipe, refUtilisateur, refRole) VALUES (1, 109, 13)