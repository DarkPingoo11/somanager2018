SELECT idUtilisateur, identifiant, nom, prenom, hash
FROM utilisateur
WHERE identifiant = '<username>';

-- LIPRJ
SELECT s.idSujet projectId, s.titre title,
       IF(s.confidentialite = 1, IF(ps.idProfesseur IS NULL, 'Confidentiel', s.description), s.description) descrip,
       IF(p.idPoster IS NULL, false, true) poster,
       us.prenom supervisor_forename, us.nom supervisor_surname,
       s.confidentialite condif
FROM sujet s
  LEFT JOIN utilisateur u ON u.identifiant = '<username>'
  LEFT JOIN poster p ON s.idSujet = p.idSujet
  LEFT JOIN professeursujet ps ON ps.idSujet = s.idSujet AND ps.idProfesseur = u.idUtilisateur
  LEFT JOIN professeursujet pss ON pss.idSujet = s.idSujet
  LEFT JOIN utilisateur us ON us.idUtilisateur = pss.idProfesseur;

SELECT u.idUtilisateur, u.nom, u.prenom
FROM utilisateur u
  JOIN etudiantequipe ee ON ee.idEtudiant = u.idUtilisateur
  JOIN equipe e ON e.idEquipe = ee.idEquipe
  JOIN sujet s ON s.idSujet = e.idSujet
WHERE s.idSujet = '<id_sujet>';


-- MYPRJ
SELECT s.idSujet projectId, s.titre title,
       s.description descrip,
       IF(p.idPoster IS NULL, false, true) poster,
       u.prenom supervisor_forename, u.nom supervisor_surname,
       s.confidentialite condif
FROM sujet s
  JOIN utilisateur u ON u.identifiant = '<username>'
  LEFT JOIN poster p ON s.idSujet = p.idSujet
  JOIN professeursujet ps ON ps.idSujet = s.idSujet AND ps.idProfesseur = u.idUtilisateur;

SELECT u.idUtilisateur, u.nom, u.prenom
FROM utilisateur u
  JOIN etudiantequipe ee ON ee.idEtudiant = u.idUtilisateur
  JOIN equipe e ON e.idEquipe = ee.idEquipe
  JOIN sujet s ON s.idSujet = e.idSujet
WHERE s.idSujet = '<id_sujet>';


-- lIJUR
SELECT j.idJuryPoster idJury, j.date date
FROM juryposter j
UNION
SELECT js.idJurySoutenance, s.dateSoutenance date
FROM jurysoutenance js
  JOIN soutenance s on js.idJurySoutenance = s.idJurySoutenance;

SELECT s.idSujet projectId, s.titre title,
       s.confidentialite condif,
       IF(p.idPoster IS NULL, false, true) poster,
       u.prenom supervisor_forename, u.nom supervisor_surname
FROM sujet s
  LEFT JOIN poster p ON s.idSujet = p.idSujet
  JOIN professeursujet ps ON ps.idSujet = s.idSujet
  JOIN utilisateur u ON u.idUtilisateur = ps.idProfesseur
  JOIN equipe e ON e.idSujet = s.idSujet
  LEFT JOIN soutenance sou ON sou.idEquipe = e.idEquipe
  LEFT JOIN jurysoutenance j ON sou.idJurySoutenance = j.idJurySoutenance
  LEFT JOIN juryposter j2 on e.idEquipe = j2.idEquipe
WHERE j2.idJuryPoster = '<idJury>' OR j.idJurySoutenance = '<idJury>';



-- MYJUR
SELECT j.idJuryPoster idJury, j.date date
FROM juryposter j
  JOIN professeur p on j.idProf1 = p.idProfesseur OR j.idProf2 = p.idProfesseur OR j.idProf3 = p.idProfesseur
  JOIN utilisateur u on p.idProfesseur = u.idUtilisateur
WHERE u.identifiant = '<username>'
UNION
SELECT js.idJurySoutenance, s.dateSoutenance date
FROM jurysoutenance js
  JOIN soutenance s on js.idJurySoutenance = s.idJurySoutenance
  JOIN professeur p2 on js.idProf1 = p2.idProfesseur OR js.idProf2 = p2.idProfesseur
  JOIN utilisateur u2 on p2.idProfesseur = u2.idUtilisateur
WHERE u2.identifiant = '<username>';

SELECT s.idSujet projectId, s.titre title,
       s.confidentialite condif,
       IF(p.idPoster IS NULL, false, true) poster,
       u.prenom supervisor_forename, u.nom supervisor_surname
FROM sujet s
  LEFT JOIN poster p ON s.idSujet = p.idSujet
  JOIN professeursujet ps ON ps.idSujet = s.idSujet
  JOIN utilisateur u ON u.idUtilisateur = ps.idProfesseur
  JOIN equipe e ON e.idSujet = s.idSujet
  LEFT JOIN soutenance sou ON sou.idEquipe = e.idEquipe
  LEFT JOIN jurysoutenance j ON sou.idJurySoutenance = j.idJurySoutenance
  LEFT JOIN juryposter j2 on e.idEquipe = j2.idEquipe
WHERE j2.idJuryPoster = '<idJury>' OR j.idJurySoutenance = '<idJury>';

SELECT u.idUtilisateur, u.nom surname, u.prenom forename
FROM utilisateur u
  JOIN etudiant e on u.idUtilisateur = e.idEtudiant
  JOIN etudiantequipe e2 on e.idEtudiant = e2.idEtudiant
  JOIN equipe e3 on e2.idEquipe = e3.idEquipe
  JOIN sujet s on e3.idSujet = s.idSujet
WHERE s.idSujet = '<projectId>';

-- JYINF
SELECT s.idSujet projectId, s.titre title,
       s.confidentialite condif,
       IF(p.idPoster IS NULL, false, true) poster,
       u.prenom supervisor_forename, u.nom supervisor_surname
FROM sujet s
  LEFT JOIN poster p ON s.idSujet = p.idSujet
  JOIN professeursujet ps ON ps.idSujet = s.idSujet
  JOIN utilisateur u ON u.idUtilisateur = ps.idProfesseur
  JOIN equipe e ON e.idSujet = s.idSujet
  LEFT JOIN soutenance sou ON sou.idEquipe = e.idEquipe
  LEFT JOIN jurysoutenance j ON sou.idJurySoutenance = j.idJurySoutenance
  LEFT JOIN juryposter j2 on e.idEquipe = j2.idEquipe
WHERE (j2.idProf1 = u.idUtilisateur OR j2.idProf2 = u.idUtilisateur OR j2.idProf3 = u.idUtilisateur OR j.idProf1 = u.idUtilisateur OR j.idProf2 = u.idUtilisateur)
      AND u.identifiant = '<username>' AND (j2.idJuryPoster = '<idJury>' OR j.idJurySoutenance = '<idJury>');

SELECT u.idUtilisateur, u.nom surname, u.prenom forename
FROM utilisateur u
  JOIN etudiant e on u.idUtilisateur = e.idEtudiant
  JOIN etudiantequipe e2 on e.idEtudiant = e2.idEtudiant
  JOIN equipe e3 on e2.idEquipe = e3.idEquipe
  JOIN sujet s on e3.idSujet = s.idSujet
WHERE s.idSujet = '<projectId>';

-- POSTR
SELECT s.confidentialite confidentiality, p.chemin filepathPDF
FROM sujet s
  JOIN poster p on s.idSujet = p.idSujet
  JOIN equipe e ON e.idSujet = s.idSujet
  JOIN utilisateur u ON u.identifiant = '<username>'
  LEFT JOIN soutenance sou ON sou.idEquipe = e.idEquipe
  LEFT JOIN jurysoutenance j ON sou.idJurySoutenance = j.idJurySoutenance
  LEFT JOIN juryposter j2 on e.idEquipe = j2.idEquipe
  LEFT JOIN professeursujet p2 on s.idSujet = p2.idSujet
WHERE (((j2.idProf1 = u.idUtilisateur OR j2.idProf2 = u.idUtilisateur OR j2.idProf3 = u.idUtilisateur OR j.idProf1 = u.idUtilisateur OR j.idProf2 = u.idUtilisateur) AND confidentialite = 1)
       OR (p2.idProfesseur = u.idUtilisateur AND confidentialite = 1)
       OR (confidentialite = 0 OR confidentialite IS NULL))  AND s.idSujet = '<projectId>';

-- NOTES
SELECT u.idUtilisateur userId, u.nom surname, u.prenom forename, ((AVG(notePoster) + AVG(noteSoutenance))/2) avgNote
FROM sujet s
  JOIN equipe e on s.idSujet = e.idSujet
  JOIN etudiantequipe e2 on e.idEquipe = e2.idEquipe
  JOIN etudiant e3 on e2.idEtudiant = e3.idEtudiant
  JOIN utilisateur u on e3.idEtudiant = u.idUtilisateur
  LEFT JOIN noteposter np on e3.idEtudiant = np.idEtudiant
  LEFT JOIN notesoutenance ns on e3.idEtudiant = ns.idEtudiant
WHERE s.idSujet = '<projectId>'
GROUP BY u.idUtilisateur;

SELECT u.idUtilisateur userId, u.nom surname, u.prenom forename, ((ns.note + np.note) /2) mynote
FROM sujet s
  JOIN equipe e on s.idSujet = e.idSujet
  JOIN etudiantequipe e2 on e.idEquipe = e2.idEquipe
  JOIN etudiant e3 on e2.idEtudiant = e3.idEtudiant
  JOIN utilisateur u on e3.idEtudiant = u.idUtilisateur
  JOIN utilisateur u2 on u2.identifiant = '<username>'
  JOIN professeur p ON p.idProfesseur = u2.idUtilisateur
  LEFT JOIN noteposter np on e3.idEtudiant = np.idEtudiant AND np.idProfesseur = p.idProfesseur
  LEFT JOIN notesoutenance ns on e3.idEtudiant = ns.idEtudiant AND ns.idProfesseur = p.idProfesseur
WHERE s.idSujet = '<projectId>';


-- NEWNT
SELECT j.idJuryPoster idJury, u.idUtilisateur idProf, u2.idUtilisateur studentId, p.idPoster posterId
FROM juryposter j
  JOIN utilisateur u ON u.identifiant = '<username>'
  JOIN utilisateur u2 ON u2.identifiant = '<student>'
  JOIN equipe e on j.idEquipe = e.idEquipe
  JOIN sujet s on e.idSujet = s.idSujet
  JOIN poster p on s.idSujet = p.idSujet
  JOIN etudiantequipe e2 on e.idEquipe = e2.idEquipe AND e2.idEtudiant = u2.idUtilisateur
  WHERE j.idProf1 = u.idUtilisateur OR j.idProf2 = u.idUtilisateur OR j.idProf3 = u.idUtilisateur;

INSERT INTO noteposter(idProfesseur, idEtudiant, idPoster, note)
    VALUES('<idProf>', '<studentId>', '<posterId>', '<note>')
ON DUPLICATE KEY UPDATE note = '<note>';


-- PORTE
SELECT s.idSujet idProject, s.titre title, s.description, p.chemin filepathPDF
FROM sujet s
  JOIN poster p on s.idSujet = p.idSujet
  JOIN utilisateur u ON u.identifiant = '<username>'
  JOIN roleutilisateur r on u.idUtilisateur = r.idUtilisateur
WHERE (s.confidentialite = 0 OR s.confidentialite IS NULL) AND r.idRole = 10
