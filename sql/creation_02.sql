USE somanager;
--
-- Creation des tables pour le projet de reprise de somanager
--
DROP TABLE IF EXISTS Parametres;

--
-- Creation d'une table 'parametres'
--
CREATE TABLE Parametres (
  nomParametre  VARCHAR(32) UNIQUE NOT NULL,
  valeur        TEXT,

  PRIMARY KEY (nomParametre)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


--
-- Creation d'une table 'modeleExcel'
--
CREATE TABLE ModeleExcel (
  nomFichier  VARCHAR(32) UNIQUE NOT NULL,
  ligneHeader INTEGER DEFAULT NULL,
  colId       INTEGER DEFAULT NULL,
  colNom      INTEGER DEFAULT NULL,
  colNote INTEGER DEFAULT NULL,
  colCom  INTEGER DEFAULT NULL,
  colMoyenne  INTEGER DEFAULT NULL,
  colGrade    INTEGER DEFAULT NULL,

  PRIMARY KEY(nomFichier)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Structure de la table Reunion*/
CREATE TABLE IF NOT EXISTS Reunion(
  idReunion int PRIMARY KEY AUTO_INCREMENT,
  dateReunion DATETIME,
  compteRendu varchar(200),
  titre varchar(30),
  description varchar(80),
  lieu varchar(30),
  refUtilisateur int,
  FOREIGN KEY(refUtilisateur) REFERENCES Utilisateur(idUtilisateur)
)ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

/*Structure de la table Invite_Reunion*/
CREATE TABLE IF NOT EXISTS Invite_Reunion(
  participe boolean,
  refReunion int,
  refUtilisateur int,
  FOREIGN KEY(refReunion) REFERENCES Reunion(idReunion),
  FOREIGN KEY(refUtilisateur) REFERENCES Utilisateur(idUtilisateur),
  PRIMARY KEY(refReunion, refUtilisateur)
);
