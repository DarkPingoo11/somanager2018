-- phpMyAdmin SQL Dump
-- version 4.2.12deb2+deb8u1
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Mar 27 Juin 2017 à 14:47
-- Version du serveur :  5.5.47-0+deb8u1
-- Version de PHP :  5.6.17-0+deb8u1
DROP DATABASE IF EXISTS somanagertest;

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `somanagertest`
--
CREATE DATABASE IF NOT EXISTS somanagertest;
USE somanagertest;

-- --------------------------------------------------------

--
-- Structure de la table `AnneeScolaire`
--

CREATE TABLE IF NOT EXISTS `AnneeScolaire` (
  `idAnneeScolaire` int(5) NOT NULL,
  `anneeDebut` year(4) NOT NULL,
  `anneeFin` year(4) NOT NULL,
  `dateDebutProjet` datetime DEFAULT NULL,
  `dateMiAvancement` datetime DEFAULT NULL,
  `dateDepotPoster` datetime DEFAULT NULL,
  `dateSoutenanceFinale` datetime DEFAULT NULL,
  `idOption` int(5) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;


--
-- Déclencheurs `AnneeScolaire`
--
DELIMITER //
CREATE TRIGGER `tg_verification_annee_debut_egale_annee_courante` BEFORE INSERT ON `AnneeScolaire`
  FOR EACH ROW begin

  if (new.anneeDebut> YEAR(NOW())) then
    SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Annee de début de lannée scolaire est supérieure à lannée courante';
  end if;

end
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Structure de la table `Commentaire`
--

CREATE TABLE IF NOT EXISTS `Commentaire` (
  `idCommentaire` int(5) NOT NULL,
  `contenu` varchar(500) DEFAULT NULL,
  `idSujet` int(5) DEFAULT NULL,
  `idUtilisateur` int(5) DEFAULT NULL,
  `idObservation` int(5) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;


-- --------------------------------------------------------

--
-- Structure de la table `Equipe`
--

CREATE TABLE IF NOT EXISTS `Equipe` (
  `idEquipe` varchar(10) NOT NULL DEFAULT '',
  `annee` int(4) DEFAULT NULL,
  `taille` int(3) DEFAULT NULL,
  `idSujet` int(5) DEFAULT NULL,
  `valide` enum('oui','non') NOT NULL DEFAULT 'non'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


--
-- Déclencheurs `Equipe`
--
DELIMITER //
CREATE TRIGGER `ModificationEtatSujet` AFTER DELETE ON `Equipe`
  FOR EACH ROW begin

  -- Quand une équipe est supprimée on remet l'état du sujet à laquelle elle était associé a publié

  UPDATE Sujet SET Sujet.etat="publie" WHERE Sujet.idSujet=old.idSujet;


end
//
DELIMITER ;
DELIMITER //
CREATE TRIGGER `tg_cle_primaire_equipe` BEFORE INSERT ON `Equipe`
  FOR EACH ROW begin

  declare anneeCourante year;
  declare nbre int;
  declare idCourant varchar(10);
  declare debut varchar(10);

  -- On récupère l'année courante :
  SET @anneeCourante := (SELECT anneeDebut FROM AnneeScolaire ORDER BY anneeDebut DESC LIMIT 1);

  SET @nbre := ((SELECT COUNT(*) FROM Equipe WHERE idEquipe LIKE Concat( @anneeCourante, '%')));

  IF (@nbre = 0) THEN
    -- Si aucune équipe n'a été créée, on crée la première équipe :
    SET @debut = Concat(@anneeCourante,'_001');
    SET new.idEquipe= @debut;
  ELSE

    -- On sélectionne le dernier ID de la table
    SET @idCourant := (SELECT idEquipe FROM Equipe WHERE idEquipe LIKE Concat( @anneeCourante, '%') ORDER BY idEquipe DESC LIMIT 1);

    -- On récupère le nombre derrière cet ID et on rajoute 1 car on crée une nouvelle entrée
    SET @nbre := (convert(SUBSTR(@idCourant FROM 6), SIGNED)+1);


    SET @debut=
    CASE
    -- Si le nombre d'équipe est compris entre 0 et 10, on ne rajoute que 2 zeros avant le nombre (ex : yyyy_002)
    WHEN @nbre<10 THEN Concat(@anneeCourante,'_00')

    -- Si le nombre d'équipe est compris entre 10 et 99, on ne rajoute que 1 zeros avant le nombre (ex : yyyy_022)
    WHEN @nbre>9 THEN Concat(@anneeCourante,'_0')

    END;
    SET new.idEquipe=Concat(@debut,@nbre);

  END IF;
end
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Structure de la table `Etudiant`
--

CREATE TABLE IF NOT EXISTS `Etudiant` (
  `idEtudiant` int(5) NOT NULL,
  `annee` int(5) DEFAULT NULL,
  `contratPro` enum('oui','non') NOT NULL DEFAULT 'non',
  `noteIntermediaire` float DEFAULT NULL,
  `noteProjet` float DEFAULT NULL,
  `noteSoutenance` float DEFAULT NULL,
  `notePoster` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


--
-- Déclencheurs `Etudiant`
--
DELIMITER //
CREATE TRIGGER `ReglageNotes` BEFORE INSERT ON `Etudiant`
  FOR EACH ROW begin

  SET new.noteIntermediaire=-1;
  SET new.noteProjet=-1;
  SET new.noteSoutenance=-1;
  SET new.notePoster=-1;

end
//
DELIMITER ;
DELIMITER //
CREATE TRIGGER `SuppressionEtudiantEquipe` BEFORE DELETE ON `Etudiant`
  FOR EACH ROW -- Trigger permettant de supprimer l'étudiant qui va être supprimé de l'équipe dont il fait partie, si il fait partie d'une.
  begin

    declare etudiantPresent int;

    -- On teste si l'étudiant est présent dans une équipe
    SET @etudiantPresent:=(SELECT COUNT(*) FROM EtudiantEquipe WHERE EtudiantEquipe.idEtudiant = old.idEtudiant);

    -- Si oui, on le supprime de l'équipe
    if (@etudiantPresent=1) then
      DELETE FROM EtudiantEquipe WHERE idEtudiant=old.idEtudiant;
    end if;
  end
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Structure de la table `EtudiantEquipe`
--

CREATE TABLE IF NOT EXISTS `EtudiantEquipe` (
  `idEtudiant` int(5) NOT NULL,
  `idEquipe` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déclencheurs `EtudiantEquipe`
--
DELIMITER //
CREATE TRIGGER `DecrementTailleEquipe` AFTER DELETE ON `EtudiantEquipe`
  FOR EACH ROW -- Trigger mettant à jour automatiquement la taille de l'équipe

  begin

    declare tailleEquipe int;

    -- On récupère la taille de l'équipe concernée
    SET @tailleEquipe :=(SELECT COUNT(EtudiantEquipe.idEtudiant) FROM EtudiantEquipe WHERE EtudiantEquipe.idEquipe=old.idEquipe);

    -- On met à jour
    UPDATE Equipe SET taille=@tailleEquipe WHERE idEquipe=old.idEquipe;

    DELETE FROM Equipe Where Equipe.taille=0;
  end
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Structure de la table `JuryPoster`
--

CREATE TABLE IF NOT EXISTS `JuryPoster` (
  `idJuryPoster` varchar(10) NOT NULL DEFAULT '',
  `idProf1` int(5) DEFAULT NULL,
  `idProf2` int(5) DEFAULT NULL,
  `idProf3` int(5) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `idEquipe` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déclencheurs `JuryPoster`
--
DELIMITER //
CREATE TRIGGER `tg_cle_primaire_jury_poster` BEFORE INSERT ON `JuryPoster`
  FOR EACH ROW begin

  declare anneeCourante year;
  declare nbre int;
  declare idCourant varchar(10);
  declare debut varchar(10);

  -- On récupère l'année courante :
  SET @anneeCourante := (SELECT anneeDebut FROM AnneeScolaire ORDER BY anneeDebut DESC LIMIT 1);

  SET @nbre := ((SELECT COUNT(*) FROM JuryPoster WHERE idJuryPoster LIKE Concat( @anneeCourante, '%')));

  IF (@nbre = 0) THEN
    -- Si aucune équipe n'a été créée, on crée la première équipe :
    SET @debut = Concat(@anneeCourante,'_001');
    SET new.idJuryPoster= @debut;
  ELSE

    -- On sélectionne le dernier ID de la table
    SET @idCourant := (SELECT idJuryPoster FROM JuryPoster WHERE idJuryPoster LIKE Concat( @anneeCourante, '%') ORDER BY idJuryPoster DESC LIMIT 1);

    -- On récupère le nombre derrière cet ID et on rajoute 1 car on crée une nouvelle entrée
    SET @nbre := (convert(SUBSTR(@idCourant FROM 6), SIGNED)+1);


    SET @debut=
    CASE
    -- Si le nombre d'équipe est compris entre 0 et 10, on ne rajoute que 2 zeros avant le nombre (ex : yyyy_002)
    WHEN @nbre<10 THEN Concat(@anneeCourante,'_00')

    -- Si le nombre d'équipe est compris entre 10 et 99, on ne rajoute que 1 zeros avant le nombre (ex : yyyy_022)
    WHEN @nbre>9 THEN Concat(@anneeCourante,'_0')

    END;
    SET new.idJuryPoster=Concat(@debut,@nbre);

  END IF;
end
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Structure de la table `JurySoutenance`
--

CREATE TABLE IF NOT EXISTS `JurySoutenance` (
  `idJurySoutenance` varchar(10) NOT NULL DEFAULT '',
  `idProf1` int(5) DEFAULT NULL,
  `idProf2` int(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déclencheurs `JurySoutenance`
--
DELIMITER //
CREATE TRIGGER `tg_cle_primaire_jury_soutenance` BEFORE INSERT ON `JurySoutenance`
  FOR EACH ROW begin

  declare anneeCourante year;
  declare nbre int;
  declare idCourant varchar(10);
  declare debut varchar(10);

  -- On récupère l'année courante :
  SET @anneeCourante := (SELECT anneeDebut FROM AnneeScolaire ORDER BY anneeDebut DESC LIMIT 1);

  SET @nbre := ((SELECT COUNT(*) FROM JurySoutenance WHERE idJurySoutenance LIKE Concat( @anneeCourante, '%')));

  IF (@nbre = 0) THEN
    -- Si aucune équipe n'a été créée, on crée la première équipe :
    SET @debut = Concat(@anneeCourante,'_001');
    SET new.idJurySoutenance= @debut;
  ELSE

    -- On sélectionne le dernier ID de la table
    SET @idCourant := (SELECT idJurySoutenance FROM JurySoutenance WHERE idJurySoutenance LIKE Concat( @anneeCourante, '%') ORDER BY idJurySoutenance DESC LIMIT 1);

    -- On récupère le nombre derrière cet ID et on rajoute 1 car on crée une nouvelle entrée
    SET @nbre := (convert(SUBSTR(@idCourant FROM 6), SIGNED)+1);


    SET @debut=
    CASE
    -- Si le nombre d'équipe est compris entre 0 et 10, on ne rajoute que 2 zeros avant le nombre (ex : yyyy_002)
    WHEN @nbre<10 THEN Concat(@anneeCourante,'_00')

    -- Si le nombre d'équipe est compris entre 10 et 99, on ne rajoute que 1 zeros avant le nombre (ex : yyyy_022)
    WHEN @nbre>9 THEN Concat(@anneeCourante,'_0')

    END;
    SET new.idJurySoutenance=Concat(@debut,@nbre);

  END IF;
end
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Structure de la table `NoteInteretSujet`
--

CREATE TABLE IF NOT EXISTS `NoteInteretSujet` (
  `idProfesseur` int(5) NOT NULL,
  `idSujet` int(5) NOT NULL,
  `note` int(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


-- --------------------------------------------------------

--
-- Structure de la table `NoteInteretTechno`
--

CREATE TABLE IF NOT EXISTS `NoteInteretTechno` (
  `idProfesseur` int(5) NOT NULL,
  `idSujet` int(5) NOT NULL,
  `note` int(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


-- --------------------------------------------------------

--
-- Structure de la table `NotePoster`
--

CREATE TABLE IF NOT EXISTS `NotePoster` (
  `idProfesseur` int(5) NOT NULL,
  `idEtudiant` int(5) NOT NULL,
  `idPoster` int(5) NOT NULL,
  `note` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déclencheurs `NotePoster`
--
DELIMITER //
CREATE TRIGGER `ExportNotePosterVersEtudiant` AFTER INSERT ON `NotePoster`
  FOR EACH ROW -- Trigger permettant d'exporter la note finale de l'étuidant quand tous les jurys ont mis leurs notes

  begin

    declare noteFinale float;

    if ((SELECT COUNT(*) FROM NotePoster WHERE idEtudiant=new.idEtudiant)=3) then

      SET @noteFinale:=(SELECT ROUND(AVG(note),2) FROM `NotePoster` WHERE idEtudiant=new.idEtudiant);

      UPDATE `Etudiant` SET `notePoster`= @noteFinale WHERE idEtudiant=new.IdEtudiant;

    end if;

  end
//
DELIMITER ;
DELIMITER //
CREATE TRIGGER `MiseAJourNotePosterVersEtudiant` AFTER UPDATE ON `NotePoster`
  FOR EACH ROW begin

  -- Trigger permettant de mettre à jour la note finale de l'étudiant si le prof concerné par le jury Soutenance, change sa note

  declare noteFinale float;

  if ((SELECT COUNT(*) FROM NotePoster WHERE idEtudiant=new.idEtudiant)=3 AND new.note!=old.note) then

    SET @noteFinale:=(SELECT ROUND(AVG(note),2) FROM `NotePoster` WHERE idEtudiant=new.idEtudiant);

    UPDATE `Etudiant` SET `notePoster`= @noteFinale WHERE idEtudiant=new.IdEtudiant;

  end if;

end
//
DELIMITER ;
DELIMITER //
CREATE TRIGGER `VerificationApresMiseAJourNotePoster` BEFORE UPDATE ON `NotePoster`
  FOR EACH ROW -- Trigger permettant de vérifier, lors d'une mise à jour de l'entrée, que les infos sont concordantes
  begin

    -- On vérifie d'abord que le professeur qui note le poster est dans le juryPoster
    if ((SELECT COUNT(*) FROM JuryPoster, Equipe,Sujet,Poster WHERE new.idProfesseur in(idProf1,idProf2,idProf3) and JuryPoster.idEquipe=Equipe.idEquipe and Equipe.idSujet = Sujet.idSujet and Poster.idSujet=Sujet.idSujet and Poster.idPoster=new.idPoster)!=1) then

      SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Le professeur spécifié ne fait pas partie dans la soutenance spécifiée';
    end if;

    -- On vérifie que l'étudiant spécifié fait partie de l'équipe que le prof juge
    if ((SELECT COUNT(DISTINCT Poster.idPoster) FROM EtudiantEquipe,Poster,Equipe WHERE EtudiantEquipe.idEtudiant=new.idEtudiant and EtudiantEquipe.idEquipe=Poster.idEquipe and Poster.idPoster=new.idPoster
        )!=1) then
      SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'L étudiant spécifié ne fait pas partie de l équipe du poster spécifiée';
    end if;

    -- On vérifie que la note est comprise entre 0 et 20
    if (new.note>20 OR new.note<0) then
      SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'La note n est pas comprise entre 0 et 20';
    end if;


  end
//
DELIMITER ;
DELIMITER //
CREATE TRIGGER `VerificationAvantInsertionNotePoster` BEFORE INSERT ON `NotePoster`
  FOR EACH ROW begin

  -- On vérifie d'abord que le professeur qui note le poster est dans le juryPoster
  if ((SELECT COUNT(*) FROM JuryPoster, Equipe,Sujet,Poster WHERE new.idProfesseur in(idProf1,idProf2,idProf3) and JuryPoster.idEquipe=Equipe.idEquipe and Equipe.idSujet = Sujet.idSujet and Poster.idSujet=Sujet.idSujet and Poster.idPoster=new.idPoster)!=1) then

    SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Le professeur spécifié ne fait pas partie dans la soutenance spécifiée';
  end if;

  -- On vérifie que l'étudiant spécifié fait partie de l'équipe que le prof juge
  if ((SELECT COUNT(DISTINCT Poster.idPoster) FROM EtudiantEquipe,Poster,Equipe WHERE EtudiantEquipe.idEtudiant=new.idEtudiant and EtudiantEquipe.idEquipe=Poster.idEquipe and Poster.idPoster=new.idPoster
      )!=1) then
    SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'L étudiant spécifié ne fait pas partie de l équipe du poster spécifiée';
  end if;

  -- On vérifie que la note est comprise entre 0 et 20
  if (new.note>20 OR new.note<0) then
    SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'La note n est pas comprise entre 0 et 20';
  end if;


end
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Structure de la table `NoteSoutenance`
--

CREATE TABLE IF NOT EXISTS `NoteSoutenance` (
  `idProfesseur` int(5) NOT NULL,
  `idEtudiant` int(5) NOT NULL,
  `idSoutenance` int(5) NOT NULL,
  `note` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déclencheurs `NoteSoutenance`
--
DELIMITER //
CREATE TRIGGER `ExportNoteSoutenanceVersEtudiant` AFTER INSERT ON `NoteSoutenance`
  FOR EACH ROW begin

  declare noteFinale float;

  if ((SELECT COUNT(*) FROM NoteSoutenance WHERE idEtudiant=new.idEtudiant)=2) then

    SET @noteFinale:=(SELECT ROUND(AVG(note),2) FROM `NoteSoutenance` WHERE idEtudiant=new.idEtudiant);

    UPDATE `Etudiant` SET `noteSoutenance`= @noteFinale WHERE idEtudiant=new.IdEtudiant;

  end if;

end
//
DELIMITER ;
DELIMITER //
CREATE TRIGGER `MiseAJourNoteSoutenanceVersEtudiant` AFTER UPDATE ON `NoteSoutenance`
  FOR EACH ROW begin

  -- Trigger permettant de mettre à jour la note finale de l'étudiant si le prof concerné par le jury Soutenance, change sa note

  declare noteFinale float;

  if ((SELECT COUNT(*) FROM NoteSoutenance WHERE idEtudiant=new.idEtudiant)=2 AND new.note!=old.note) then

    SET @noteFinale:=(SELECT ROUND(AVG(note),2) FROM `NoteSoutenance` WHERE idEtudiant=new.idEtudiant);

    UPDATE `Etudiant` SET `noteSoutenance`= @noteFinale WHERE idEtudiant=new.IdEtudiant;

  end if;

end
//
DELIMITER ;
DELIMITER //
CREATE TRIGGER `VerificationApresMiseAJourNoteSoutenance` BEFORE UPDATE ON `NoteSoutenance`
  FOR EACH ROW begin

  -- On vérifie d'abord que le professeur qui note la soutenance est dans le jurySoutenance

  if ((SELECT COUNT(*) FROM JurySoutenance,Soutenance WHERE new.idProfesseur in(idProf1,idProf2) and JurySoutenance.idJurySoutenance=Soutenance.idJurySoutenance and Soutenance.idSoutenance=new.idSoutenance)!=1) then
    SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Le professeur spécifié ne fait pas partie dans la soutenance spécifiée';
  end if;

  -- On vérifie que l'étudiant spécifié fait partie de l'équipe que le prof juge
  if ((SELECT COUNT(*) FROM EtudiantEquipe ee,Soutenance s WHERE ee.idEtudiant=new.idEtudiant AND ee.idEquipe=s.idEquipe AND s.idSoutenance=new.idSoutenance)!=1) then
    SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'L étudiant spécifié ne fait pas partie dans la soutenance spécifiée ou ne fait pas partie de l équipe de la soutenance spécifiée';
  end if;

  -- On vérifie que la note est comprise entre 0 et 20
  if (new.note>20 OR new.note<0) then
    SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'La note n est pas comprise entre 0 et 20';
  end if;

end
//
DELIMITER ;
DELIMITER //
CREATE TRIGGER `VerificationAvantInsertionNoteSoutenance` BEFORE INSERT ON `NoteSoutenance`
  FOR EACH ROW begin

  -- On vérifie d'abord que le professeur qui note la soutenance est dans le jurySoutenance

  if ((SELECT COUNT(*) FROM JurySoutenance,Soutenance WHERE new.idProfesseur in(idProf1,idProf2) and JurySoutenance.idJurySoutenance=Soutenance.idJurySoutenance and Soutenance.idSoutenance=new.idSoutenance)!=1) then
    SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Le professeur spécifié ne fait pas partie dans la soutenance spécifiée';
  end if;

  -- On vérifie que l'étudiant spécifié fait partie de l'équipe que le prof juge
  if ((SELECT COUNT(*) FROM EtudiantEquipe ee,Soutenance s WHERE ee.idEtudiant=new.idEtudiant AND ee.idEquipe=s.idEquipe AND s.idSoutenance=new.idSoutenance)!=1) then
    SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'L étudiant spécifié ne fait pas partie dans la soutenance spécifiée ou ne fait pas partie de l équipe de la soutenance spécifiée';
  end if;

  -- On vérifie que la note est comprise entre 0 et 20
  if (new.note>20 OR new.note<0) then
    SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'La note n est pas comprise entre 0 et 20';
  end if;

end
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Structure de la table `Notification`
--

CREATE TABLE IF NOT EXISTS `Notification` (
  `idNotification` int(5) NOT NULL,
  `commentaire` varchar(100) DEFAULT NULL,
  `lien` varchar(100) DEFAULT NULL,
  `vue` smallint(2) DEFAULT NULL,
  `idUtilisateur` int(5) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;


-- --------------------------------------------------------

--
-- Structure de la table `OptionESEO`
--

CREATE TABLE IF NOT EXISTS `OptionESEO` (
  `idOption` int(5) NOT NULL,
  `nomOption` varchar(5) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `OptionESEO`
--

INSERT INTO `OptionESEO` (`idOption`, `nomOption`) VALUES
  (1, 'LD'),
  (2, 'SE'),
  (3, 'OC'),
  (4, 'BIO'),
  (5, 'NRJ'),
  (6, 'CC'),
  (7, 'IIT'),
  (8, 'DSMT'),
  (9, 'BD'),
  (20, '');

-- --------------------------------------------------------

--
-- Structure de la table `OptionSujets`
--

CREATE TABLE IF NOT EXISTS `OptionSujets` (
  `idOption` int(5) NOT NULL,
  `idSujet` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `PartageRole`
--

CREATE TABLE IF NOT EXISTS `PartageRole` (
  `idPartageRole` int(5) NOT NULL,
  `idUtilisateur1` int(5) NOT NULL,
  `idUtilisateur2` int(5) NOT NULL,
  `idRole` int(5) NOT NULL,
  `idOption` int(5) NOT NULL,
  `dateDebut` datetime NOT NULL,
  `dateFin` datetime NOT NULL,
  `actif` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;


--
-- Déclencheurs `PartageRole`
--
DELIMITER //
CREATE TRIGGER `AjoutRoleTemporaire` BEFORE INSERT ON `PartageRole`
  FOR EACH ROW -- Trigger permettant d'ajouter dans la table PartageRole, une entrée
  -- On vérifie ici si les données rentrées sont corrects

  begin

    -- On vérifie que les dates sont corrects
    if (new.dateDebut>=new.dateFin) then
      SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Date de fin inférieur a date de début';
    end if;

    -- On vérifie que l'utilisateur bénéficiaire est bien un Professeur
    if ((SELECT COUNT(*) FROM Professeur WHERE idProfesseur=new.idUtilisateur2)=0) then
      SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Utilisateur2 nest pas un professeur';
    end if;

    -- Si la date de début est inférieur à la date du moment on active le partage
    if (new.dateDebut<=NOW() and new.actif=0) then
      INSERT INTO `RoleUtilisateur`(`idUtilisateur`, `idRole`, `idOption`) VALUES (new.idUtilisateur2,new.idRole,new.idOption);
      SET new.actif=1;
    end if;

  end
//
DELIMITER ;
DELIMITER //
CREATE TRIGGER `AjoutRoleTemporaire_2` BEFORE UPDATE ON `PartageRole`
  FOR EACH ROW -- Trigger permettant d'ajouter dans la table RoleUtilisateur, le role revenant à l'utilisateur quand l'attribut actif de PartageRole passe de 0 à 1. Ce passage est réalisé par un evenement qui update automatiquement cet attribut.

  begin
    if (old.actif=0 and new.actif=1) then
      INSERT INTO `RoleUtilisateur`(`idUtilisateur`, `idRole`, `idOption`) VALUES (new.idUtilisateur2,new.idRole,new.idOption);
    end if;
  end
//
DELIMITER ;
DELIMITER //
CREATE TRIGGER `SupressionRoleTemporaire` AFTER DELETE ON `PartageRole`
  FOR EACH ROW -- Apres la suppression de l'entrée de PartageRole, on supprime aussi l'entrée dans RoleUtilisateur

  DELETE FROM `RoleUtilisateur` WHERE idRole=old.idRole and idOption=old.idOption and idUtilisateur=old.idUtilisateur2
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Structure de la table `PorteurSujet`
--

CREATE TABLE IF NOT EXISTS `PorteurSujet` (
  `idSujet` int(5) NOT NULL,
  `idUtilisateur` int(5) NOT NULL,
  `annee` year(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `Poster`
--

CREATE TABLE IF NOT EXISTS `Poster` (
  `idPoster` int(5) NOT NULL,
  `chemin` varchar(100) DEFAULT NULL,
  `datePoster` datetime DEFAULT NULL,
  `idSujet` int(5) DEFAULT NULL,
  `valide` enum('oui','non') NOT NULL DEFAULT 'non',
  `idEquipe` varchar(10) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;


-- --------------------------------------------------------

--
-- Structure de la table `Professeur`
--

CREATE TABLE IF NOT EXISTS `Professeur` (
  `idProfesseur` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


-- --------------------------------------------------------

--
-- Structure de la table `ProfesseurSujet`
--

CREATE TABLE IF NOT EXISTS `ProfesseurSujet` (
  `idProfesseur` int(5) NOT NULL,
  `idSujet` int(5) NOT NULL,
  `fonction` enum('co-encadrant','interessé','consultant','référent') NOT NULL DEFAULT 'co-encadrant',
  `valide` enum('oui','non') NOT NULL DEFAULT 'non'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


--
-- Déclencheurs `ProfesseurSujet`
--
DELIMITER //
CREATE TRIGGER `ValidationAutomatique` BEFORE INSERT ON `ProfesseurSujet`
  FOR EACH ROW begin


  if (new.fonction IN ('co-encadrant','interessé','consultant')) then
    set new.valide='oui';
  end if;

end
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Structure de la table `Role`
--

CREATE TABLE IF NOT EXISTS `Role` (
  `idRole` int(5) NOT NULL,
  `nomRole` varchar(15) DEFAULT NULL,
  type enum('applicatif','equipe') DEFAULT 'applicatif'
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `Role`
--

INSERT INTO `Role` (`idRole`, `nomRole`, `type`) VALUES
  (1, 'etudiant', 'applicatif'),
  (2, 'admin','applicatif'),
  (3, 'prof','applicatif'),
  (4, 'profOption','applicatif'),
  (5, 'profResponsable','applicatif'),
  (6, 'assistant','applicatif'),
  (9, 'entrepriseExt','applicatif'),
  (10, 'serviceCom','applicatif'),
  (11, 'scrumMaster', 'equipe'),
  (12, 'productOwner', 'equipe'),
  (13, 'profReferent', 'equipe');
-- --------------------------------------------------------

--
-- Structure de la table `RoleUtilisateur`
--

CREATE TABLE IF NOT EXISTS `RoleUtilisateur` (
  `idUtilisateur` int(5) NOT NULL,
  `idRole` int(5) NOT NULL,
  `idOption` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO RoleUtilisateur (idUtilisateur, idRole, idOption) VALUES (1, 2, 20);


--
-- Déclencheurs `RoleUtilisateur`
--
DELIMITER //
CREATE TRIGGER `AjoutProfesseur` AFTER INSERT ON `RoleUtilisateur`
  FOR EACH ROW -- Trigger permettant d'automatiser la création des objets Professeur.

  begin

    DECLARE idRoleProf int;
    DECLARE idRoleProfOption int;
    DECLARE idRoleProfResp int;
    DECLARE verificationExistance int;

    -- On vérifie que l'objet professeur n'existe pas déja
    SET @verificationExistance:=(SELECT COUNT(*) FROM Professeur WHERE idProfesseur=NEW.idUtilisateur);

    -- On récupère l'idRole du role prof
    SET @idRoleProf :=(SELECT idRole FROM Role WHERE Role.nomRole="prof");

    -- On récupère l'idRole du role profOption
    SET @idRoleProfOption :=(SELECT idRole FROM Role WHERE Role.nomRole="profOption");

    -- On récupère l'idRole du role profResponsable
    SET @idRoleProfResp :=(SELECT idRole FROM Role WHERE Role.nomRole="profResponsable");

    -- Si l'objet Professeur n'exsite pas déja, on le crée
    if (@verificationExistance=0) then
      if (NEW.idRole=@idRoleProf OR NEW.idRole=@idRoleProfOption OR NEW.idRole=@idRoleProfResp) then
        INSERT INTO `Professeur`(`idProfesseur`) VALUES (new.idUtilisateur);
      end if;
    end if;
  end
//
DELIMITER ;
DELIMITER //
CREATE TRIGGER `SuppressionEtudiantProfesseur` AFTER DELETE ON `RoleUtilisateur`
  FOR EACH ROW -- Trigger permettant d'automatiser la suppression des objets Professeur et étudiant lors d'une suppression de role.

  begin
    DECLARE idRoleEtudiant int;
    DECLARE idRoleProf int;
    DECLARE idRoleProfOption int;
    DECLARE idRoleProfResp int;
    DECLARE nombreRoleProf int;

    SET @idRoleEtudiant :=(SELECT idRole FROM Role WHERE Role.nomRole="etudiant");

    SET @idRoleProf :=(SELECT idRole FROM Role WHERE Role.nomRole="prof");

    SET @idRoleProfOption :=(SELECT idRole FROM Role WHERE Role.nomRole="profOption");

    SET @idRoleProfResp :=(SELECT idRole FROM Role WHERE Role.nomRole="profResponsable");

    SET @nombreRoleProf :=(SELECT COUNT(*) FROM RoleUtilisateur WHERE idRole in (@idRoleProf,@idRoleProfOption,@idRoleProfResp) and idUtilisateur=OLD.idUtilisateur);


    if (@idRoleEtudiant=OLD.idRole) then
      DELETE FROM Etudiant Where Etudiant.idEtudiant=OLD.idUtilisateur;
    end if;

    if (@nombreRoleProf=0) then
      if (OLD.idRole=@idRoleProf OR OLD.idRole=@idRoleProfOption OR OLD.idRole=@idRoleProfResp) then
        DELETE FROM Professeur Where Professeur.idProfesseur=OLD.idUtilisateur;
      end if;
    end if;

  end
//
DELIMITER ;
DELIMITER //
CREATE TRIGGER `VerificationDroitSupressionRole` BEFORE DELETE ON `RoleUtilisateur`
  FOR EACH ROW -- On vérifie que la suppression du role n'est pas associé à un partage en cours. Si oui on invite l'utilisateur à faire cette suppression directement via l'interface de PartageRole

  begin

    if ((SELECT COUNT(*) FROM PartageRole WHERE old.idUtilisateur=PartageRole.idUtilisateur2 and old.idRole=PartageRole.idRole and old.idOption=PartageRole.idOption)=1) then
      SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Impossible de supprimer un role en cours de partage, veuillez vous referer à la table PartageRole directement';
    end if;

  end
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Structure de la table `Soutenance`
--

CREATE TABLE IF NOT EXISTS `Soutenance` (
  `idSoutenance` int(5) NOT NULL,
  `dateSoutenance` datetime DEFAULT NULL,
  `idJurySoutenance` varchar(10) DEFAULT NULL,
  `idEquipe` varchar(10) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;


-- --------------------------------------------------------

--
-- Structure de la table `Sujet`
--

CREATE TABLE IF NOT EXISTS `Sujet` (
  `idSujet` int(5) NOT NULL,
  `titre` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `nbrMinEleves` int(4) NOT NULL,
  `nbrMaxEleves` int(4) NOT NULL,
  `contratPro` tinyint(1) DEFAULT NULL,
  `confidentialite` tinyint(1) DEFAULT NULL,
  `etat` enum('depose','valide','attribue','refuse','publie') NOT NULL,
  `liens` text,
  `interets` text CHARACTER SET latin2,
  `noteInteretTechno` float DEFAULT NULL,
  `noteInteretSujet` float DEFAULT NULL,
  `idForm` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;


-- --------------------------------------------------------

--
-- Structure de la table `Utilisateur`
--

CREATE TABLE IF NOT EXISTS `Utilisateur` (
  `idUtilisateur` int(5) NOT NULL,
  `nom` varchar(20) DEFAULT NULL,
  `prenom` varchar(20) DEFAULT NULL,
  `identifiant` varchar(15) NOT NULL,
  `hash` varchar(100) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `valide` enum('oui','non') NOT NULL DEFAULT 'non'
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `Utilisateur`
--

INSERT INTO `Utilisateur` (`idUtilisateur`, `nom`, `prenom`, `identifiant`, `hash`, `email`, `valide`) VALUES (1, 'ESEO', 'admin', 'admin', '$2a$10$dJIeyRW2gEOZBns4zcvCbOPp1Y6yVm6vMkdMDowN8YqqkP6RhgQvq', 'admin@eseo.fr', 'oui');

--

--
-- Index pour la table `AnneeScolaire`
--
ALTER TABLE `AnneeScolaire`
  ADD PRIMARY KEY (`idAnneeScolaire`), ADD UNIQUE KEY `idOption` (`idOption`);

--
-- Index pour la table `Commentaire`
--
ALTER TABLE `Commentaire`
  ADD PRIMARY KEY (`idCommentaire`), ADD KEY `idObservation` (`idObservation`), ADD KEY `Commentaire_ibfk_1` (`idSujet`), ADD KEY `Commentaire_ibfk_2` (`idUtilisateur`);

--
-- Index pour la table `Equipe`
--
ALTER TABLE `Equipe`
  ADD PRIMARY KEY (`idEquipe`), ADD UNIQUE KEY `idEquipe` (`idEquipe`), ADD UNIQUE KEY `idSujet` (`idSujet`), ADD KEY `Equipe_ibfk_1` (`idSujet`);

--
-- Index pour la table `Etudiant`
--
ALTER TABLE `Etudiant`
  ADD PRIMARY KEY (`idEtudiant`), ADD UNIQUE KEY `idEtudiant` (`idEtudiant`);

--
-- Index pour la table `EtudiantEquipe`
--
ALTER TABLE `EtudiantEquipe`
  ADD PRIMARY KEY (`idEtudiant`,`idEquipe`), ADD UNIQUE KEY `idEtudiant` (`idEtudiant`), ADD KEY `EtudiantEquipe_ibfk_2` (`idEquipe`);

--
-- Index pour la table `JuryPoster`
--
ALTER TABLE `JuryPoster`
  ADD PRIMARY KEY (`idJuryPoster`), ADD UNIQUE KEY `idJuryPoster` (`idJuryPoster`), ADD UNIQUE KEY `idEquipe` (`idEquipe`), ADD KEY `JuryPoster_ibfk_3` (`idProf3`), ADD KEY `JuryPoster_ibfk_1` (`idProf1`), ADD KEY `JuryPoster_ibfk_2` (`idProf2`);

--
-- Index pour la table `JurySoutenance`
--
ALTER TABLE `JurySoutenance`
  ADD PRIMARY KEY (`idJurySoutenance`), ADD UNIQUE KEY `identifiant` (`idJurySoutenance`), ADD KEY `JurySoutenance_ibfk_2` (`idProf2`), ADD KEY `JurySoutenance_ibfk_1` (`idProf1`);

--
-- Index pour la table `NoteInteretSujet`
--
ALTER TABLE `NoteInteretSujet`
  ADD PRIMARY KEY (`idProfesseur`,`idSujet`), ADD KEY `NoteInteretSujet_ibfk_2` (`idSujet`);

--
-- Index pour la table `NoteInteretTechno`
--
ALTER TABLE `NoteInteretTechno`
  ADD PRIMARY KEY (`idProfesseur`,`idSujet`), ADD KEY `NoteInteretTechno_ibfk_2` (`idSujet`);

--
-- Index pour la table `NotePoster`
--
ALTER TABLE `NotePoster`
  ADD PRIMARY KEY (`idProfesseur`,`idEtudiant`,`idPoster`), ADD KEY `idPoster` (`idPoster`), ADD KEY `idEtudiant` (`idEtudiant`);

--
-- Index pour la table `NoteSoutenance`
--
ALTER TABLE `NoteSoutenance`
  ADD PRIMARY KEY (`idProfesseur`,`idEtudiant`,`idSoutenance`), ADD KEY `idSoutenance` (`idSoutenance`), ADD KEY `idEtudiant` (`idEtudiant`);

--
-- Index pour la table `Notification`
--
ALTER TABLE `Notification`
  ADD PRIMARY KEY (`idNotification`), ADD KEY `Notification_ibfk_1` (`idUtilisateur`);

--
-- Index pour la table `OptionESEO`
--
ALTER TABLE `OptionESEO`
  ADD PRIMARY KEY (`idOption`);

--
-- Index pour la table `OptionSujets`
--
ALTER TABLE `OptionSujets`
  ADD PRIMARY KEY (`idOption`,`idSujet`), ADD KEY `OptionSujets_ibfk_2` (`idSujet`);

--
-- Index pour la table `PartageRole`
--
ALTER TABLE `PartageRole`
  ADD PRIMARY KEY (`idPartageRole`,`idUtilisateur1`,`idUtilisateur2`,`idRole`,`idOption`), ADD UNIQUE KEY `UC_Person` (`idUtilisateur1`,`idUtilisateur2`,`idRole`,`idOption`), ADD KEY `PartageRole_ibfk_2` (`idUtilisateur2`), ADD KEY `PartageRole_ibfk_3` (`idRole`), ADD KEY `PartageRole_ibfk_4` (`idOption`);

--
-- Index pour la table `PorteurSujet`
--
ALTER TABLE `PorteurSujet`
  ADD PRIMARY KEY (`idSujet`,`idUtilisateur`), ADD UNIQUE KEY `idSujet` (`idSujet`), ADD KEY `PorteurSujet_ibfk_2` (`idUtilisateur`);

--
-- Index pour la table `Poster`
--
ALTER TABLE `Poster`
  ADD PRIMARY KEY (`idPoster`), ADD UNIQUE KEY `idSujet` (`idSujet`), ADD KEY `Poster_ibfk_3` (`idEquipe`);

--
-- Index pour la table `Professeur`
--
ALTER TABLE `Professeur`
  ADD PRIMARY KEY (`idProfesseur`);

--
-- Index pour la table `ProfesseurSujet`
--
ALTER TABLE `ProfesseurSujet`
  ADD PRIMARY KEY (`idProfesseur`,`idSujet`,`fonction`), ADD UNIQUE KEY `idSujet` (`idSujet`,`idProfesseur`);

--
-- Index pour la table `Role`
--
ALTER TABLE `Role`
  ADD PRIMARY KEY (`idRole`);

--
-- Index pour la table `RoleUtilisateur`
--
ALTER TABLE `RoleUtilisateur`
  ADD PRIMARY KEY (`idUtilisateur`,`idRole`,`idOption`), ADD KEY `RoleUtilisateur_ibfk_2` (`idRole`), ADD KEY `RoleUtilisateur_ibfk_3` (`idOption`);

--
-- Index pour la table `Soutenance`
--
ALTER TABLE `Soutenance`
  ADD PRIMARY KEY (`idSoutenance`), ADD UNIQUE KEY `idEquipe` (`idEquipe`), ADD KEY `Soutenance_ibfk_1` (`idJurySoutenance`);

--
-- Index pour la table `Sujet`
--
ALTER TABLE `Sujet`
  ADD PRIMARY KEY (`idSujet`);

--
-- Index pour la table `Utilisateur`
--
ALTER TABLE `Utilisateur`
  ADD PRIMARY KEY (`idUtilisateur`), ADD UNIQUE KEY `identifiant` (`identifiant`), ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `AnneeScolaire`
--
ALTER TABLE `AnneeScolaire`
  MODIFY `idAnneeScolaire` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT pour la table `Commentaire`
--
ALTER TABLE `Commentaire`
  MODIFY `idCommentaire` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT pour la table `Notification`
--
ALTER TABLE `Notification`
  MODIFY `idNotification` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT pour la table `OptionESEO`
--
ALTER TABLE `OptionESEO`
  MODIFY `idOption` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT pour la table `PartageRole`
--
ALTER TABLE `PartageRole`
  MODIFY `idPartageRole` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT pour la table `Poster`
--
ALTER TABLE `Poster`
  MODIFY `idPoster` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT pour la table `Role`
--
ALTER TABLE `Role`
  MODIFY `idRole` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT pour la table `Soutenance`
--
ALTER TABLE `Soutenance`
  MODIFY `idSoutenance` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT pour la table `Sujet`
--
ALTER TABLE `Sujet`
  MODIFY `idSujet` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT pour la table `Utilisateur`
--
ALTER TABLE `Utilisateur`
  MODIFY `idUtilisateur` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `AnneeScolaire`
--
ALTER TABLE `AnneeScolaire`
  ADD CONSTRAINT `AnneeScolaire_ibfk_1` FOREIGN KEY (`idOption`) REFERENCES `OptionESEO` (`idOption`) ON DELETE CASCADE;

--
-- Contraintes pour la table `Commentaire`
--
ALTER TABLE `Commentaire`
  ADD CONSTRAINT `Commentaire_ibfk_1` FOREIGN KEY (`idSujet`) REFERENCES `Sujet` (`idSujet`) ON DELETE CASCADE,
  ADD CONSTRAINT `Commentaire_ibfk_2` FOREIGN KEY (`idUtilisateur`) REFERENCES `Utilisateur` (`idUtilisateur`) ON DELETE CASCADE;

--
-- Contraintes pour la table `Equipe`
--
ALTER TABLE `Equipe`
  ADD CONSTRAINT `Equipe_ibfk_1` FOREIGN KEY (`idSujet`) REFERENCES `Sujet` (`idSujet`) ON DELETE CASCADE;

--
-- Contraintes pour la table `Etudiant`
--
ALTER TABLE `Etudiant`
  ADD CONSTRAINT `Etudiant_ibfk_1` FOREIGN KEY (`idEtudiant`) REFERENCES `Utilisateur` (`idUtilisateur`) ON DELETE CASCADE;

--
-- Contraintes pour la table `EtudiantEquipe`
--
ALTER TABLE `EtudiantEquipe`
  ADD CONSTRAINT `EtudiantEquipe_ibfk_1` FOREIGN KEY (`idEtudiant`) REFERENCES `Etudiant` (`idEtudiant`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `EtudiantEquipe_ibfk_2` FOREIGN KEY (`idEquipe`) REFERENCES `Equipe` (`idEquipe`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `JuryPoster`
--
ALTER TABLE `JuryPoster`
  ADD CONSTRAINT `JuryPoster_ibfk_1` FOREIGN KEY (`idProf1`) REFERENCES `Professeur` (`idProfesseur`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `JuryPoster_ibfk_2` FOREIGN KEY (`idProf2`) REFERENCES `Professeur` (`idProfesseur`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `JuryPoster_ibfk_3` FOREIGN KEY (`idProf3`) REFERENCES `Professeur` (`idProfesseur`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `JuryPoster_ibfk_4` FOREIGN KEY (`idEquipe`) REFERENCES `Equipe` (`idEquipe`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `JurySoutenance`
--
ALTER TABLE `JurySoutenance`
  ADD CONSTRAINT `JurySoutenance_ibfk_1` FOREIGN KEY (`idProf1`) REFERENCES `Professeur` (`idProfesseur`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `JurySoutenance_ibfk_2` FOREIGN KEY (`idProf2`) REFERENCES `Professeur` (`idProfesseur`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `NoteInteretSujet`
--
ALTER TABLE `NoteInteretSujet`
  ADD CONSTRAINT `NoteInteretSujet_ibfk_1` FOREIGN KEY (`idProfesseur`) REFERENCES `Professeur` (`idProfesseur`) ON DELETE CASCADE,
  ADD CONSTRAINT `NoteInteretSujet_ibfk_2` FOREIGN KEY (`idSujet`) REFERENCES `Sujet` (`idSujet`) ON DELETE CASCADE;

--
-- Contraintes pour la table `NoteInteretTechno`
--
ALTER TABLE `NoteInteretTechno`
  ADD CONSTRAINT `NoteInteretTechno_ibfk_1` FOREIGN KEY (`idProfesseur`) REFERENCES `Professeur` (`idProfesseur`) ON DELETE CASCADE,
  ADD CONSTRAINT `NoteInteretTechno_ibfk_2` FOREIGN KEY (`idSujet`) REFERENCES `Sujet` (`idSujet`) ON DELETE CASCADE;

--
-- Contraintes pour la table `NotePoster`
--
ALTER TABLE `NotePoster`
  ADD CONSTRAINT `NotePoster_ibfk_1` FOREIGN KEY (`idProfesseur`) REFERENCES `Professeur` (`idProfesseur`) ON DELETE CASCADE,
  ADD CONSTRAINT `NotePoster_ibfk_2` FOREIGN KEY (`idPoster`) REFERENCES `Poster` (`idPoster`) ON DELETE CASCADE,
  ADD CONSTRAINT `NotePoster_ibfk_3` FOREIGN KEY (`idEtudiant`) REFERENCES `Etudiant` (`idEtudiant`) ON DELETE CASCADE;

--
-- Contraintes pour la table `NoteSoutenance`
--
ALTER TABLE `NoteSoutenance`
  ADD CONSTRAINT `NoteSoutenance_ibfk_1` FOREIGN KEY (`idProfesseur`) REFERENCES `Professeur` (`idProfesseur`) ON DELETE CASCADE,
  ADD CONSTRAINT `NoteSoutenance_ibfk_2` FOREIGN KEY (`idSoutenance`) REFERENCES `Soutenance` (`idSoutenance`) ON DELETE CASCADE,
  ADD CONSTRAINT `NoteSoutenance_ibfk_3` FOREIGN KEY (`idEtudiant`) REFERENCES `Etudiant` (`idEtudiant`) ON DELETE CASCADE;

--
-- Contraintes pour la table `Notification`
--
ALTER TABLE `Notification`
  ADD CONSTRAINT `Notification_ibfk_1` FOREIGN KEY (`idUtilisateur`) REFERENCES `Utilisateur` (`idUtilisateur`) ON DELETE CASCADE;

--
-- Contraintes pour la table `OptionSujets`
--
ALTER TABLE `OptionSujets`
  ADD CONSTRAINT `OptionSujets_ibfk_1` FOREIGN KEY (`idOption`) REFERENCES `OptionESEO` (`idOption`) ON DELETE CASCADE,
  ADD CONSTRAINT `OptionSujets_ibfk_2` FOREIGN KEY (`idSujet`) REFERENCES `Sujet` (`idSujet`) ON DELETE CASCADE;

--
-- Contraintes pour la table `PartageRole`
--
ALTER TABLE `PartageRole`
  ADD CONSTRAINT `PartageRole_ibfk_1` FOREIGN KEY (`idUtilisateur1`) REFERENCES `Utilisateur` (`idUtilisateur`) ON DELETE CASCADE,
  ADD CONSTRAINT `PartageRole_ibfk_2` FOREIGN KEY (`idUtilisateur2`) REFERENCES `Utilisateur` (`idUtilisateur`) ON DELETE CASCADE,
  ADD CONSTRAINT `PartageRole_ibfk_3` FOREIGN KEY (`idRole`) REFERENCES `Role` (`idRole`) ON DELETE CASCADE,
  ADD CONSTRAINT `PartageRole_ibfk_4` FOREIGN KEY (`idOption`) REFERENCES `OptionESEO` (`idOption`) ON DELETE CASCADE;

--
-- Contraintes pour la table `PorteurSujet`
--
ALTER TABLE `PorteurSujet`
  ADD CONSTRAINT `PorteurSujet_ibfk_1` FOREIGN KEY (`idSujet`) REFERENCES `Sujet` (`idSujet`) ON DELETE CASCADE,
  ADD CONSTRAINT `PorteurSujet_ibfk_2` FOREIGN KEY (`idUtilisateur`) REFERENCES `Utilisateur` (`idUtilisateur`) ON DELETE CASCADE;

--
-- Contraintes pour la table `Poster`
--
ALTER TABLE `Poster`
  ADD CONSTRAINT `Poster_ibfk_2` FOREIGN KEY (`idSujet`) REFERENCES `Sujet` (`idSujet`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Poster_ibfk_3` FOREIGN KEY (`idEquipe`) REFERENCES `Equipe` (`idEquipe`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Poster_ibfk_4` FOREIGN KEY (`idEquipe`) REFERENCES `Equipe` (`idEquipe`);

--
-- Contraintes pour la table `Professeur`
--
ALTER TABLE `Professeur`
  ADD CONSTRAINT `Professeur_ibfk_1` FOREIGN KEY (`idProfesseur`) REFERENCES `Utilisateur` (`idUtilisateur`) ON DELETE CASCADE;

--
-- Contraintes pour la table `ProfesseurSujet`
--
ALTER TABLE `ProfesseurSujet`
  ADD CONSTRAINT `ProfesseurSujet_ibfk_1` FOREIGN KEY (`idProfesseur`) REFERENCES `Professeur` (`idProfesseur`) ON DELETE CASCADE,
  ADD CONSTRAINT `ProfesseurSujet_ibfk_2` FOREIGN KEY (`idSujet`) REFERENCES `Sujet` (`idSujet`) ON DELETE CASCADE;

--
-- Contraintes pour la table `RoleUtilisateur`
--
ALTER TABLE `RoleUtilisateur`
  ADD CONSTRAINT `RoleUtilisateur_ibfk_1` FOREIGN KEY (`idUtilisateur`) REFERENCES `Utilisateur` (`idUtilisateur`) ON DELETE CASCADE,
  ADD CONSTRAINT `RoleUtilisateur_ibfk_2` FOREIGN KEY (`idRole`) REFERENCES `Role` (`idRole`) ON DELETE CASCADE,
  ADD CONSTRAINT `RoleUtilisateur_ibfk_3` FOREIGN KEY (`idOption`) REFERENCES `OptionESEO` (`idOption`) ON DELETE CASCADE;

--
-- Contraintes pour la table `Soutenance`
--
ALTER TABLE `Soutenance`
  ADD CONSTRAINT `Soutenance_ibfk_1` FOREIGN KEY (`idJurySoutenance`) REFERENCES `JurySoutenance` (`idJurySoutenance`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Soutenance_ibfk_2` FOREIGN KEY (`idEquipe`) REFERENCES `Equipe` (`idEquipe`) ON DELETE CASCADE ON UPDATE CASCADE;

DELIMITER $$
--
-- Événements
--
CREATE DEFINER=`root`@`localhost` EVENT `VerificationPartageRole` ON SCHEDULE EVERY 3 MINUTE
  STARTS '2017-05-01 00:00:00' ENDS '2037-05-01 00:00:00'
  ON COMPLETION PRESERVE ENABLE
DO
  BEGIN
    UPDATE PartageRole SET actif=1 WHERE dateDebut<=NOW() AND PartageRole.actif=0;
    DELETE FROM PartageRole WHERE dateFin<=NOW();
  END$$

-- Activation des "evenement" sur PhpMyAdmin
SET GLOBAL event_scheduler="ON"$$

DELIMITER ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
USE somanagertest;
--
-- Creation des tables pour le projet de reprise de somanagertest
--
DROP TABLE IF EXISTS Parametres;

--
-- Creation d'une table 'parametres'
--
CREATE TABLE Parametres (
  nomParametre  VARCHAR(32) UNIQUE NOT NULL,
  valeur        TEXT,

  PRIMARY KEY (nomParametre)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


--
-- Creation d'une table 'modeleExcel'
--
CREATE TABLE ModeleExcel (
  nomFichier  VARCHAR(32) UNIQUE NOT NULL,
  ligneHeader INTEGER DEFAULT NULL,
  colId       INTEGER DEFAULT NULL,
  colNom      INTEGER DEFAULT NULL,
  colNote INTEGER DEFAULT NULL,
  colCom  INTEGER DEFAULT NULL,
  colMoyenne  INTEGER DEFAULT NULL,
  colGrade    INTEGER DEFAULT NULL,

  PRIMARY KEY(nomFichier)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Structure de la table Reunion*/
CREATE TABLE IF NOT EXISTS Reunion(
  idReunion int PRIMARY KEY AUTO_INCREMENT,
  dateReunion DATETIME,
  compteRendu varchar(200),
  titre varchar(30),
  description varchar(80),
  lieu varchar(30),
  refUtilisateur int,
  FOREIGN KEY(refUtilisateur) REFERENCES Utilisateur(idUtilisateur)
)ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

/*Structure de la table Invite_Reunion*/
CREATE TABLE IF NOT EXISTS Invite_Reunion(
  participe boolean,
  refReunion int,
  refUtilisateur int,
  FOREIGN KEY(refReunion) REFERENCES Reunion(idReunion),
  FOREIGN KEY(refUtilisateur) REFERENCES Utilisateur(idUtilisateur),
  PRIMARY KEY(refReunion, refUtilisateur)
);
USE somanagertest;

SET default_storage_engine=INNODB;

/*Structure de la table Pgl Sprint*/
CREATE TABLE IF NOT EXISTS pgl_sprint (
  idSprint INTEGER AUTO_INCREMENT,
  numero TINYINT NOT NULL,
  dateDebut DATE NOT NULL,
  dateFin DATE NOT NULL,
  nbrHeures INTEGER DEFAULT NULL,
  coefficient FLOAT DEFAULT 1.0,
  refAnnee INTEGER NOT NULL,
  notesPubliees BOOLEAN DEFAULT FALSE,

  PRIMARY KEY(idSprint),
  FOREIGN KEY (refAnnee) REFERENCES AnneeScolaire(idAnneeScolaire) ON DELETE CASCADE
);

/*Structure de la table Matière*/
CREATE TABLE IF NOT EXISTS pgl_matiere (
  idMatiere INTEGER AUTO_INCREMENT,
  libelle VARCHAR(40) NOT NULL,
  coefficient FLOAT NOT NULL DEFAULT 1.0,
  refSprint INTEGER NOT NULL,
  PRIMARY KEY (idMatiere),
  FOREIGN KEY (refSprint) REFERENCES pgl_sprint(idSprint) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS pgl_projet(
  idProjet int(5) NOT NULL AUTO_INCREMENT,
  titre varchar(30) DEFAULT NULL,
  description varchar(80) DEFAULT NULL,
  idProfReferent int(5),
  PRIMARY KEY (idProjet),
  FOREIGN KEY (idProfReferent) REFERENCES Professeur(idProfesseur)
);

CREATE TABLE IF NOT EXISTS pgl_soutenance(
  idSoutenance int(5) NOT NULL AUTO_INCREMENT,
  date datetime DEFAULT NULL,
  libelle varchar(30) DEFAULT NULL,
  PRIMARY KEY (idSoutenance)
);

CREATE TABLE IF NOT EXISTS pgl_compositionjury(
  refSoutenance int(5) NOT NULL,
  refProfesseur int(5) NOT NULL,
  PRIMARY KEY (refSoutenance,refProfesseur),
  FOREIGN KEY (refSoutenance) REFERENCES pgl_soutenance(idSoutenance) ON DELETE CASCADE,
  FOREIGN KEY (refProfesseur) REFERENCES Professeur(idProfesseur) ON DELETE CASCADE
);

/*Structure de la table Equipe*/
CREATE TABLE IF NOT EXISTS pgl_equipe(
  idEquipe INTEGER AUTO_INCREMENT,
  nom VARCHAR(10) NOT NULL,
  refProjet INTEGER DEFAULT NULL,
  refAnnee INTEGER NOT NULL,
  PRIMARY KEY(idEquipe),
  FOREIGN KEY(refProjet) REFERENCES pgl_projet(idProjet),
  FOREIGN KEY(refAnnee) REFERENCES anneescolaire(idAnneeScolaire) ON DELETE CASCADE
);

/*Structure de la table Etudiant*/
CREATE TABLE IF NOT EXISTS pgl_etudiant (
  idEtudiant INTEGER,
  refAnnee INTEGER DEFAULT NULL,
  PRIMARY KEY(idEtudiant),
  FOREIGN KEY(refAnnee) REFERENCES anneescolaire(idAnneeScolaire) ON DELETE CASCADE,
  FOREIGN KEY(idEtudiant) REFERENCES utilisateur(idUtilisateur) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS pgl_roleequipe(
  refEquipe INTEGER NOT NULL,
  refUtilisateur INTEGER NOT NULL,
  refRole INTEGER NOT NULL,

  PRIMARY KEY (refEquipe, refUtilisateur, refRole),
  FOREIGN KEY (refEquipe) REFERENCES pgl_equipe(idEquipe) ON DELETE CASCADE,
  FOREIGN KEY (refUtilisateur) REFERENCES utilisateur(idUtilisateur) ON DELETE CASCADE,
  FOREIGN KEY (refRole) REFERENCES role(idRole) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS pgl_evalue(
  refSoutenance int(5) NOT NULL,
  refEquipe int(5) NOT NULL,
  PRIMARY KEY (refSoutenance,refEquipe),
  FOREIGN KEY (refSoutenance) REFERENCES pgl_soutenance(idSoutenance) ON DELETE CASCADE,
  FOREIGN KEY (refEquipe) REFERENCES pgl_equipe(idEquipe) ON DELETE CASCADE
);

/*Structure de la table noteEquipe*/
CREATE TABLE IF NOT EXISTS pgl_note (
  idNote INTEGER AUTO_INCREMENT,
  note FLOAT NOT NULL,
  refMatiere INTEGER NOT NULL,
  refEtudiant INTEGER NOT NULL,
  refEvaluateur INTEGER NOT NULL,
  PRIMARY KEY (idNote),
  FOREIGN KEY (refMatiere) REFERENCES pgl_matiere(idMatiere) ON DELETE CASCADE,
  FOREIGN KEY (refEvaluateur) REFERENCES utilisateur(idUtilisateur) ON DELETE CASCADE,
  FOREIGN KEY (refEtudiant) REFERENCES pgl_etudiant(idEtudiant),
  CONSTRAINT UC_NoteMatiereEvaluateur UNIQUE (refMatiere,refEtudiant,refEvaluateur)
);

/*Structure de la table BonusMalus*/
CREATE TABLE IF NOT EXISTS pgl_bonusmalus(
  idBonusMalus INTEGER AUTO_INCREMENT,
  valeur FLOAT NOT NULL,
  validation ENUM('attente', 'refuser', 'accepter') DEFAULT 'attente',
  justification VARCHAR(250) NOT NULL,
  refSprint INTEGER NOT NULL,
  refEtudiant INTEGER NOT NULL,
  refEvaluateur INTEGER NOT NULL,
  PRIMARY KEY (idBonusMalus),
  FOREIGN KEY(refSprint) REFERENCES pgl_sprint(idSprint) ON DELETE CASCADE,
  FOREIGN KEY(refEtudiant) REFERENCES pgl_etudiant(idEtudiant) ON DELETE CASCADE,
  FOREIGN KEY(refEvaluateur) REFERENCES utilisateur(idUtilisateur)
);

--
-- Structure de la table EtudiantEquipe
--
CREATE TABLE IF NOT EXISTS pgl_etudiantequipe(
  idEtudiant INTEGER NOT NULL,
  idEquipe INTEGER NOT NULL,
  idSprint INTEGER NOT NULL,
  PRIMARY KEY (idEtudiant, idSprint),
  FOREIGN KEY(idEtudiant) REFERENCES pgl_etudiant(idEtudiant) ON DELETE CASCADE,
  FOREIGN KEY(idEquipe) REFERENCES pgl_equipe(idEquipe) ON DELETE CASCADE,
  FOREIGN KEY(idSprint) REFERENCES pgl_sprint(idSprint) ON DELETE CASCADE
);

--
-- Structure de la table `demandejury`
--

CREATE TABLE IF NOT EXISTS `demandejury` (
  `idDemande` int(5) NOT NULL AUTO_INCREMENT,
  `sujetsConcernes` text NOT NULL,
  `commentaire` text,
  `nomOption` enum('LD','SE','OC','BIO','NRJ','CC','IIT','DSMT','BD') NOT NULL,
  PRIMARY KEY (`idDemande`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Structure de la table `professeurdemande`
--

CREATE TABLE IF NOT EXISTS `professeurdemande` (
  `idProfesseur` int(5) NOT NULL,
  `idDemande` int(5) NOT NULL,
  PRIMARY KEY (`idProfesseur`,`idDemande`),
  KEY `ProfesseurDemande_ibfk_2` (`idDemande`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contraintes pour la table `professeurdemande`
--
ALTER TABLE `professeurdemande`
  ADD CONSTRAINT `ProfesseurDemande_ibfk_1` FOREIGN KEY (`idProfesseur`) REFERENCES `professeur` (`idProfesseur`) ON DELETE CASCADE,
  ADD CONSTRAINT `ProfesseurDemande_ibfk_2` FOREIGN KEY (`idDemande`) REFERENCES `demandejury` (`idDemande`) ON DELETE CASCADE;


--
--
-- Trigger pour inserer les matieres par défaut dans les sprints
--
DELIMITER //
CREATE TRIGGER `MatieresDefaut` AFTER INSERT ON pgl_sprint
  FOR EACH ROW BEGIN
  INSERT INTO pgl_matiere(libelle, coefficient, refSprint) VALUES('Soutenance matin', 1, NEW.idSprint);
  INSERT INTO pgl_matiere(libelle, coefficient, refSprint) VALUES('Soutenance après-midi', 1, NEW.idSprint);
END//
DELIMITER ;


--
-- Vue pour les notes moyennes
-- Affiche la moyenne pour chaque matière d'un etudiant, par sprint
--
CREATE OR REPLACE VIEW pgl_moyennesEtudiantsMatieres AS
  SELECT n.refEtudiant, u.nom, u.prenom, m.libelle, s.idSprint, a.anneeDebut, AVG(n.note) note, n.refMatiere, n.refEvaluateur, NULL idNote
  FROM pgl_note n
    JOIN pgl_etudiant e ON n.refEtudiant = e.idEtudiant
    JOIN pgl_matiere m ON m.idMatiere = n.refMatiere
    JOIN utilisateur u ON u.idUtilisateur = e.idEtudiant
    JOIN pgl_sprint s ON s.idSprint = m.refSprint
    JOIN anneescolaire a ON a.idAnneeScolaire = s.refAnnee
  GROUP BY s.idSprint, m.libelle, e.idEtudiant
  ORDER BY n.refMatiere ASC;


--
-- Vue pour les notes moyennes par sprint
-- Affiche la moyenne obtenue par sprint par etudiant
-- Le bonusMalus affiché est DEJA appliqué à la note
--

CREATE OR REPLACE VIEW pgl_moyennesEtudiantsSprint AS
  SELECT pmEM.refEtudiant, s.idSprint, s.numero, CONCAT('Sprint ', s.numero) libelle, s.coefficient, nom, prenom, pmEM.anneeDebut, NULL idNote,
                                                 GREATEST(0, LEAST(20,
                                                                   ((SUM(pmEM.note * m.coefficient) / SUM(m.coefficient) +
                                                                     IFNULL(SUM(IFNULL(bm.valeur, 0)) * COUNT(DISTINCT bm.idBonusMalus) / COUNT(bm.idBonusMalus), 0)
                                                                   ))
                                                 )) note,
                                                 IFNULL((SUM(IFNULL(bm.valeur,0)) * COUNT(DISTINCT bm.idBonusMalus) / COUNT(bm.idBonusMalus)), 0) bonusMalus

  FROM pgl_moyennesEtudiantsMatieres pmEM
    JOIN pgl_matiere m ON m.idMatiere = pmEM.refMatiere
    JOIN pgl_sprint s ON s.idSprint = m.refSprint
    LEFT JOIN pgl_bonusmalus bm ON bm.refEtudiant = pmEM.refEtudiant AND bm.refSprint = s.idSprint
  GROUP BY pmEM.refEtudiant, pmEM.idSprint;

--
-- Vue pour les notes
-- Affiche toutes les notes obtenues par un étudiant
--
CREATE OR REPLACE VIEW pgl_notesEtudiants AS
  SELECT a.idAnneeScolaire, s.idSprint, ee.idEquipe, u.idUtilisateur, u.nom, u.prenom, n.idNote, n.note, m.idMatiere, m.libelle, m.coefficient, eval.idUtilisateur idEvaluateur, s.notesPubliees
  FROM utilisateur u
    JOIN pgl_etudiant e ON u.idUtilisateur = e.idEtudiant
    JOIN anneescolaire a ON a.idAnneeScolaire = e.refAnnee
    JOIN pgl_sprint s ON s.refAnnee = a.idAnneeScolaire
    JOIN pgl_matiere m ON m.refSprint = s.idSprint
    JOIN pgl_etudiantequipe ee ON ee.idEtudiant = e.idEtudiant
    LEFT JOIN pgl_note n ON n.refMatiere = m.idMatiere AND n.refEtudiant = e.idEtudiant
    LEFT JOIN utilisateur eval ON eval.idUtilisateur = n.refEvaluateur
  ORDER BY s.numero, u.idUtilisateur ASC;

--
-- Récupère la liste des évaluateurs qui ont ajoutés une note par matière
--
CREATE OR REPLACE VIEW pgl_matieresEtudiantEvaluees AS
  SELECT a.idAnneeScolaire, s.idSprint, ee.idEquipe, u.idUtilisateur, u.nom, u.prenom, m.idMatiere, m.libelle, s.notesPubliees,m.coefficient, GROUP_CONCAT(eval.idUtilisateur) evaluateurs
  FROM utilisateur u
    JOIN pgl_etudiant e ON u.idUtilisateur = e.idEtudiant
    JOIN anneescolaire a ON a.idAnneeScolaire = e.refAnnee
    JOIN pgl_sprint s ON s.refAnnee = a.idAnneeScolaire
    JOIN pgl_matiere m ON m.refSprint = s.idSprint
    JOIN pgl_etudiantequipe ee ON ee.idEtudiant = e.idEtudiant
    LEFT JOIN pgl_note n ON n.refEtudiant = e.idEtudiant AND n.refMatiere = m.idMatiere
    LEFT JOIN utilisateur eval ON eval.idUtilisateur = n.refEvaluateur
  GROUP BY u.idUtilisateur, m.idMatiere
  ORDER BY s.numero, u.idUtilisateur ASC;