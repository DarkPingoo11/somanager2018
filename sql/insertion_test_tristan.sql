USE somanager;

-- Rappel des id des roles
-- (1, 'etudiant'),
-- (2, 'admin'),
-- (3, 'prof'),
-- (4, 'profOption'),
-- (5, 'profResponsable'),
-- (6, 'assistant'),
-- (9, 'entrepriseExt'),
-- (10, 'serviceCom');

--  Rappel des id des options
-- (1, 'LD'),
-- (2, 'SE'),
-- (3, 'OC'),
-- (4, 'BIO'),
-- (5, 'NRJ'),
-- (6, 'CC'),
-- (7, 'IIT'),
-- (8, 'DSMT'),
-- (9, 'BD'),
-- (20, 'toutes options');


--
-- INSERTION DE L'UTILISATEUR 'TEST'
--
ALTER TABLE Utilisateur AUTO_INCREMENT=40;
INSERT INTO Utilisateur (idUtilisateur, nom, prenom, identifiant, hash, email, valide)
VALUES (40, 'LE GACQUE', 'Tristan', 'tlegacque', '$2a', 'tesdsqt@tesdqst.fr', 'oui'),
(41, 'BOURDEAU', 'Léo', 'sdq', '$2a', 'tesdsqts@tesdqst.fr', 'oui'),
(42, 'MERAND', 'Celine', 'cmes', '$2a', 'tesdsqtssss@tesdqst.fr', 'oui'),
(43, 'LEFORT', 'Alexis', 'dsqsdq', '$2a', 'tesdqqqsqt@tesdqst.fr', 'oui'),
(44, 'NOUR EDDINE', 'Rebbouh', 'ndqshdqs', '$2a', 'tesddfqssqt@tesdqst.fr', 'oui');

--
-- INSERTION DES ROLES UTILISATEUR
--
INSERT INTO RoleUtilisateur (idUtilisateur, idRole, idOption) VALUES (40, 1, 1);
INSERT INTO RoleUtilisateur (idUtilisateur, idRole, idOption) VALUES (41, 1, 1);
INSERT INTO RoleUtilisateur (idUtilisateur, idRole, idOption) VALUES (42, 1, 1);
INSERT INTO RoleUtilisateur (idUtilisateur, idRole, idOption) VALUES (43, 1, 1);
INSERT INTO RoleUtilisateur (idUtilisateur, idRole, idOption) VALUES (44, 1, 2);

INSERT INTO etudiant (idEtudiant, annee) VALUES
 (40, 2017),
 (41, 2017),
 (42, 2017),
 (43, 2017),
 (44, 2017);

INSERT INTO Equipe(annee, taille, idSujet) VALUES(2017,5,NULL);

INSERT INTO jurysoutenance(idProf1, idProf2) VALUES (3, 2);
INSERT INTO juryposter(idProf1, idProf2, idProf3, date, idEquipe) VALUES (3, 2, NULL, NOW(), '2017_002');
INSERT INTO soutenance (dateSoutenance, idJurySoutenance, idEquipe) VALUES ('2018-05-05', '2017_001', '2017_002');

INSERT INTO etudiantequipe(idEtudiant, idEquipe) VALUES 
 (40, '2017_002'),
 (41, '2017_002'),
 (42, '2017_002'),
 (43, '2017_002'),
 (44, '2017_002');

INSERT INTO notesoutenance (idProfesseur, idEtudiant, idSoutenance, note)
VALUES(3, 40, 1, 12),
(3, 41, 1, 4),
(3, 42, 1, 8),
(3, 43, 1, 16),
(3, 44, 1, 19);

INSERT INTO noteposter (idProfesseur, idEtudiant, idPoster, note)
VALUES(3, 40, 1, 12),
 (3, 41, 1, 11),
 (3, 42, 1, 13),
 (3, 43, 1, 18),
 (3, 44, 1, 7);


