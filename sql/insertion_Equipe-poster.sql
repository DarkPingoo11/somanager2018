-- INSERTION DES ETUDIANTS
--
ALTER TABLE Utilisateur AUTO_INCREMENT=50;
INSERT INTO Utilisateur (idUtilisateur, nom, prenom, identifiant, hash, email, valide)
VALUES (50, 'etu', 'etu', 'bob', '$2a$10$IWaV3IPZ4h2IvTFIU4xn7eJKg4lOqzgXMKLFbi0s2vuEGin5ojsLa', 'testetu@test.fr', 'oui');

INSERT INTO Utilisateur (idUtilisateur, nom, prenom, identifiant, hash, email, valide)
VALUES (51, 'bobi', 'bobi', 'bobi', '$2a$10$IWaV3IPZ4h2IvTFIU4xn7eJKg4lOqzgXMKLFbi0s2vuEGin5ojsLa', 'testbobi@test.fr', 'oui');

--
-- INSERTION DES PROFS
--
INSERT INTO Utilisateur (idUtilisateur, nom, prenom, identifiant, hash, email, valide)
VALUES (52, 'Rousseau', 'Sophie', 'rousseau', '$2a$10$IWaV3IPZ4h2IvTFIU4xn7eJKg4lOqzgXMKLFbi0s2vuEGin5ojsLa', 'rousseau@test.fr', 'oui');

INSERT INTO Utilisateur (idUtilisateur, nom, prenom, identifiant, hash, email, valide)
VALUES (53, 'Hammoudi', 'Slimane', 'hammoudi', '$2a$10$IWaV3IPZ4h2IvTFIU4xn7eJKg4lOqzgXMKLFbi0s2vuEGin5ojsLa', 'hammoudi@test.fr', 'oui');

--
-- Insertion utilisateurs
--
INSERT INTO Utilisateur (idUtilisateur, nom, prenom, identifiant, hash, email, valide)
VALUES (54, 'anne', 'claire', 'vergote', '$2a$10$IWaV3IPZ4h2IvTFIU4xn7eJKg4lOqzgXMKLFbi0s2vuEGin5ojsLa', 'anneclaire@test.fr', 'oui');

INSERT INTO Utilisateur (idUtilisateur, nom, prenom, identifiant, hash, email, valide)
VALUES (55, 'celine', 'merand', 'celine', '$2a$10$IWaV3IPZ4h2IvTFIU4xn7eJKg4lOqzgXMKLFbi0s2vuEGin5ojsLa', 'celine@test.fr', 'oui');



--
-- INSERTION DES ROLES UTILISATEUR
--
INSERT INTO RoleUtilisateur (idUtilisateur, idRole, idOption) VALUES (50, 1, 1);
INSERT INTO RoleUtilisateur (idUtilisateur, idRole, idOption) VALUES (51, 1, 1);
INSERT INTO RoleUtilisateur (idUtilisateur, idRole, idOption) VALUES (54, 1, 1);
INSERT INTO RoleUtilisateur (idUtilisateur, idRole, idOption) VALUES (55, 1, 1);

INSERT INTO RoleUtilisateur (idUtilisateur, idRole, idOption) VALUES (52, 1, 5);
INSERT INTO RoleUtilisateur (idUtilisateur, idRole, idOption) VALUES (52, 1, 4);
INSERT INTO RoleUtilisateur (idUtilisateur, idRole, idOption) VALUES (52, 1, 3);
INSERT INTO RoleUtilisateur (idUtilisateur, idRole, idOption) VALUES (53, 1, 3);


--
-- INSERTION DES PROFESSEURS
--
INSERT INTO Professeur (idProfesseur) VALUES (53);
INSERT INTO Professeur (idProfesseur) VALUES (52);

-- INSERTION DES ETUDIANTS
INSERT INTO Etudiant(idEtudiant, annee, contratPro, noteIntermediaire, noteProjet, noteSoutenance, notePoster) VALUES (50,2018,'non',-1,-1,-1,-1);
INSERT INTO Etudiant(idEtudiant, annee, contratPro, noteIntermediaire, noteProjet, noteSoutenance, notePoster) VALUES (51,2018,'non',-1,-1,-1,-1);
INSERT INTO Etudiant(idEtudiant, annee, contratPro, noteIntermediaire, noteProjet, noteSoutenance, notePoster) VALUES (54,2018,'non',-1,-1,-1,-1);
INSERT INTO Etudiant(idEtudiant, annee, contratPro, noteIntermediaire, noteProjet, noteSoutenance, notePoster) VALUES (55,2018,'non',-1,-1,-1,-1);


-- INSERTION SUJETS
INSERT INTO sujet(idSujet, titre, description, nbrMinEleves, nbrMaxEleves, contratPro, confidentialite, etat, liens, interets, noteInteretTechno, noteInteretSujet, idForm) VALUES (20,'test','test',0,4,0,0,'attribue','le test',NULL,0,0,0);
INSERT INTO sujet(idSujet, titre, description, nbrMinEleves, nbrMaxEleves, contratPro, confidentialite, etat, liens, interets, noteInteretTechno, noteInteretSujet, idForm) VALUES (21,'LD','LD',0,4,0,0,'attribue','LD',NULL,0,0,0);


-- INSERTION OPTION DU SUJET 
INSERT INTO optionsujets(idOption, idSujet) VALUES (1,20);
INSERT INTO optionsujets(idOption, idSujet) VALUES (1,21);

-- INSERTION PROF REFERENT SUJET
INSERT INTO professeursujet(idProfesseur, idSujet, fonction, valide) VALUES (53,20,'referent','oui');
INSERT INTO professeursujet(idProfesseur, idSujet, fonction, valide) VALUES (52,21,'referent','oui');



-- INSERTION EQUIPE
INSERT INTO equipe(idEquipe, annee, taille, idSujet, valide) VALUES ('2O18_003',2018,2,20,'oui');
INSERT INTO equipe(idEquipe, annee, taille, idSujet, valide) VALUES ('2O18_004',2018,2,21,'oui');


-- INSERTION ETUDIANT EQUIPE
INSERT INTO etudiantequipe(idEtudiant, idEquipe) VALUES (50,'2018_003');
INSERT INTO etudiantequipe(idEtudiant, idEquipe) VALUES (51,'2018_003');
INSERT INTO etudiantequipe(idEtudiant, idEquipe) VALUES (54,'2018_004');
INSERT INTO etudiantequipe(idEtudiant, idEquipe) VALUES (55,'2018_004');




