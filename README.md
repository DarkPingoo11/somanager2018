# Somanager

L'application SoManager s'occupe de gérer les projets de
fin d'étude de l'ESEO, ainsi que le projet Génie Logiciel
en seconde année du cursus ingénieur.

Cette application à été conçu par des élèves de l'option LD
dont les noms sont inscrit en fin de fichier.


## Installation de somanager

Dans ce fichier sera détaillée la procédure de mise en marche de l'application.

Prérequis :

* Un serveur apache tomcat 7 ou +
* Une base de données mysql ou mariadb
* le war de l'application


Tout d'abord :
* Vérifier que la variable suivante est bien définie dans /etc/mysql/my.cnf : *lower_case_table_names=1*

Ensuite :

Il suffit de lancer l'application, et d'y acceder. Au
premier lancement, l'application affichera un utilitaire d'installation.


## Copyrights
Cette application à été réalisée par les personnes citées ci-dessous.
Toute reprise de l'application devra comporter les noms des auteurs.
Licence : cc-by
  
**Equipe 2017 - 2018**
* Juan Carlo ORTIZ BETANCUR
* Dimitri JARNEAU
* Tristan LE GACQUE
* Emilien MAMALET
* Damien MAYMARD
* Quentin PICHAVANT
* Gaetan SCUILLER
* Anne Claire VERGOTE
* Céline MERAND (Consultante)

**Equipe 2016 - 2017**
* Julie AVIZOU
* Pierre CESMAT
* Alexandre CLAMENS
* Renaud DESCOURS  
* Maxime LENORMAND
* Hugo MÉNARD
* Thomas MÉNARD
* Cécilia PINSARD

