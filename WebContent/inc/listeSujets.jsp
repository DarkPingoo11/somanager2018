<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!-- Création d'un menu déroulant pour des filtres de sélection -->

<div class="row">

	<!-- </select> par état : <select class="form-control" name="selectEtat"
		id="selectEtat" onchange="giveSelection()">
		<option value=""></option>
		<c:forEach var="etat" items="${listeEtats}">
			<option value="<c:out value="${etat}"/>">
				<c:out value="${etat}" />
			</option>
		</c:forEach>-->

</div>

<div class="row">

	<table class="table table-striped">
		<thead>
			<tr>
				<th>Titre</th>
				<th>État</th>
				<th>Référent</th>
			</tr>
		</thead>
		<tbody id="divSujet">
			<!-- Affichage de la liste des sujets. Ceux-ci deviennent draggables -->
			<c:forEach var="sujet" items="${listeSujets}" varStatus="loop">
				<tr data-option='${optionSujet[loop.index]} ${sujet.etat}'
					data-soutenance='${notesSoutenances[loop.index]}'
					data-poster='${notesPosters[loop.index]}'
					data-travail='${notesTravail[loop.index]}'
					data-projet='${notesProjet[loop.index]}'
					
					id="draggable-${loop.index}" draggable='true'
					onClick='clickOnSujet("${loop.index}");'
					ondragstart="drag(event,${loop.index});"
					onmouseout="fmouseup(${loop.index});"
					ondragend="enddrag(event,${loop.index});"
					onmousedown="fmousedown(${loop.index});"
					onmouseup="fmouseup(${loop.index});">
					
					<th id="titre-${loop.index}"
						ondblclick='drop(event,${loop.index});'><c:out
							value="${sujet.titre}" /></th>
					<th id="etat-${loop.index}"><c:out value="${sujet.etat}" /></th>
						<c:if test="${sujet.etat == etatValide || sujet.etat == etatRefuse || sujet.etat == etatAttribue || sujet.etat == etatPublie}">
							<c:forEach items="${listeProfesseurSujet}" var="professeurSujet"  varStatus="status">
								<c:if test="${professeurSujet.getIdSujet() == sujet.idSujet}">
									<c:forEach items="${listeUtilisateur}" var="utilisateur"  varStatus="status">
										<c:if test="${utilisateur.getIdUtilisateur() == professeurSujet.getIdProfesseur()}">
											<th id="referent-${loop.index}"><c:out value="${utilisateur.getNom()}" /></th>
										</c:if>
									</c:forEach>
								</c:if>
							</c:forEach>
						</c:if>
						<c:if test="${sujet.etat != etatValide}">
							<th id="referent-${loop.index}"><c:out value="" /></th>
						</c:if>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</div>


<%-- <div id='divSujet'
	style="position: absolute; Width: 30%; left: 10%; max-height: 90%; overflow: auto; border: 1px solid #000000;">
	<c:forEach var="sujet" items="${listeSujets}" varStatus="loop">
		<div data-option='${sujet.options}' id="draggable-${loop.index}"
			draggable='true' onClick='clickOnSujet("${loop.index}");'
			ondragstart="drag(event,${loop.index});"
			style="overflow: hidden; border: 1px solid #000000;">
			<big id="titre-${loop.index}"
				ondblclick='drop(event,${loop.index});'> ${sujet.titre} </big> <i
				id="etat-${loop.index}" style="float: right;">${sujet.etat}</i> <br />
			${sujet.description} <br />
		</div>
	</c:forEach>
</div> --%>