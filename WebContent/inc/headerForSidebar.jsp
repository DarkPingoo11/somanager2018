<%--
  Created by IntelliJ IDEA.
  User: dimitrijarneau
  Date: 10/06/2018
  Time: 19:12
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>


<div id="top-sidebar">
    <%-- BOUTON pour max/min la sidebar --%>
    <a role="button" id="sidebarCollapse">
        <i class="fas fa-bars fa-fw" aria-hidden="true"></i>&nbsp;
    </a>


    <%-- Informations utilisateur & connexion --%>
    <div class="pull-right connexion" >
        <!-- Espace Connexion -->
        <c:choose>
            <c:when test="${empty sessionScope.utilisateur}">
                <ul class="nav navbar-nav">
                    <li class="dropdown">
                        <a role="button" class="dropdown-toggle connexion" id="connexion" data-toggle="dropdown">
                            <b>Se Connecter</b>
                            <i class="fas fa-sign-in-alt fa-fw" aria-hidden="true"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-right login-dp">
                            <li>
                                <div class="col-md-12" style="padding-top: 10px">
                                    <form method="post" action="<c:url value="/Connexion"/>">
                                        <fieldset>
                                            <label class="col-md-12" for="identifiant">
                                                Identifiant : <span class="requis">*</span>
                                            </label>
                                            <div class="col-md-12" style="margin-bottom: 10px">
                                                <input type="text" name="identifiant"
                                                       id="identifiantConnexion" class="form-control" autofocus
                                                       required />
                                            </div>

                                            <label class="col-md-12" for="motDePasse">
                                                Mot de passe : <span class="requis">*</span>
                                            </label>
                                            <div class="col-lg-12" style="margin-bottom: 10px">
                                                <input type="password" name="motDePasse"
                                                       id="motDePasseConnexion" class="form-control" required />
                                            </div>

                                            <div class="col-md-12">
                                                <button type="submit" class="btn btn-primary" id="submitConnexion">
                                                    <i class="fas fa-sign-in-alt fa-fw" aria-hidden="true"></i>&nbsp;
                                                    Se Connecter
                                                </button>
                                            </div>
                                            <div class="col-md-12">
                                                <button type="submit" class="btn btn-primary" id="submitConnexion">
                                                    <i class="fas fa-sign-in-alt fa-fw" aria-hidden="true"></i>&nbsp;
                                                    S'inscrire
                                                </button>
                                            </div>
                                        </fieldset>
                                    </form>
                                </div>
                                <div class="bottom text-center">
                                    <br /> Nouveau ici ? <a href="<c:url value="/Inscription"/>"><b>S'inscrire</b></a>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
            </c:when>

            <%-- Utilisateur --%>
            <c:otherwise>
                <ul class="nav navbar-nav navbar-right ">
                    <li class="dropdown nopadding">

                            <%-- TODO : mettre photo --%>
                        <a class="dropdown-toggle" id="connexion" data-toggle="dropdown">
                            <b style="text-transform: uppercase">${sessionScope.utilisateur.nom}</b>
                            <b style="text-transform: capitalize">${sessionScope.utilisateur.prenom}</b>

                        </a>
                        <ul class="dropdown-menu login-dp">
                            <br>
                            <c:choose>
                                <c:when test="${fn:length(roles)>1}">
                                    <li style="text-align: center;"><b> Vos rôles </b></li>
                                </c:when>
                                <c:otherwise>
                                    <li style="text-align: center;"><b> Votre rôle </b></li>
                                </c:otherwise>
                            </c:choose>
                            <br>

                                <%-- Affichage des roles pour l'utilisateur --%>
                            <c:forEach var="role" items="${requestScope.rolesAffichage}"
                                       varStatus="loop">
                                <li><i class="fas fa-${ role.icone } fa-fw"
                                       aria-hidden="true"></i>&nbsp; ${ role.nomComplet }</li>
                            </c:forEach>

                            <li role="separator" class="divider "></li>
                            <li style="text-align: center;"><a class="nav-item "
                                                               href="<c:url value="/ModifierInfoUtilisateur"/>"
                                                               id="ModifierInfoUtilisateur"> <b>Profil</b> <i
                                    class="fas fa-user" " aria-hidden="true"></i>&nbsp;
                            </a></li>
                            <li role="separator" class="divider "></li>
                            <li
                                    style="text-align: center; margin-left: 20px; margin-right: 20px;"><a
                                    class="nav-item" href="<c:url value="/Deconnexion"/>"
                                    id="deconnexion"><b>Se Déconnecter </b> <i
                                    class="fas fa-sign-out-alt fa-fw" aria-hidden="true"></i> </a></li>

                        </ul></li>

                </ul>

            </c:otherwise>
        </c:choose>
    </div>

    <%-- BOUTON pour les notifications --%>
    <c:if test="${not empty sessionScope.utilisateur}">
        <ul class="nav navbar-nav pull-right">
            <li class="dropdown">
                <a data-toggle="dropdown" role="button" class="dropdown-toggle text-white">
                    <i class="far fa-bell fa-fw" aria-hidden="true"></i>&nbsp;
                    <span class="badge badge-notify bg-warning" id="notificationCount">
                        0
                    </span>
                </a>


                <ul class="dropdown-menu dropdown-menu-right login-dp" id="notifSubmenu">
                    <div id="notification" class="col-sm-12 no-padding">
                    </div>
                </ul>
            </li>
        </ul>
    </c:if>
</div>


