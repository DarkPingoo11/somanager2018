<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<header class="row bandeau">
	<div class="panel panel-default ">
		<div class="panel-body">
			<div class="col-lg-3 col-md-3 col-sm-4 col-xs-5">
				<a href="<c:url value="/Dashboard"/>"> <img
					src="<c:url value="/images/eseo_carre.png"/>" width=50%
					alt="Logo ESEO" />
				</a>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-4 col-xs-2">
				<p style="font-size:40%; font-family: 'Merienda',sans-serif"></p>
				<p style="font-size:70%; font-family: 'Merienda',sans-serif">SoManager</p>
				<p style="font-size:40%; font-family: 'Merienda',sans-serif">Gestion de PFE</p>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-4 col-xs-5">
				<a href="<c:url value="/Dashboard"/>"> <img
					src="<c:url value="/images/logoProjet.png"/>" width=55%
					alt="Logo Projet" />
				</a>
			</div>
		</div>
	</div>

</header>