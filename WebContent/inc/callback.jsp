<%--
  Created by IntelliJ IDEA.
  User: Tristan LE GACQUE
  Date: 20/03/2018
  Time: 13:45
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:if test="${ not empty requestScope.callback }">
    <%-- Afficher les messages de callback --%>
    <c:forEach var="callbackObj" items="${requestScope.callback}">
        <c:if test="${not empty callbackObj }">
            <c:set var="callbackTitle" scope="page" value="${
            callbackObj.type eq 'success' ? 'Succès !' :
            callbackObj.type eq 'danger' ? 'Erreur !' :
            callbackObj.type eq 'warning' ? 'Attention !' :
            'Info !'}" />

            <c:set var="callbackIcon" scope="page" value="${
            callbackObj.type eq 'success' ? 'check-circle' :
            callbackObj.type eq 'danger' ? 'exclamation-circle' :
            callbackObj.type eq 'warning' ? 'exclamation-triangle' :
            'info-circle'}" />

            <%-- Afficher le message d'erreur --%>
            <div class="col-sm-12">
                <div id="callbackAlert" class="alert alert-${ callbackObj.type }">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                    <strong>
                        <i class="fas fa-${pageScope.callbackIcon} fa-fw" aria-hidden="true"></i>
                        &nbsp;${pageScope.callbackTitle}
                    </strong>
                        ${ callbackObj.message }
                </div>
            </div>
        </c:if>
    </c:forEach>
</c:if>