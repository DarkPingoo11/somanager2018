<%--
  Created by IntelliJ IDEA.
  User: dimitrijarneau
  Date: 11/06/2018
  Time: 16:17
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!-- Création d'un menu déroulant pour des filtres de sélection -->
<div class="row">
    Sélection par option :

    <select class="form-control"
            name="option" id="option" onchange="giveSelection()">
        <!-- this.value -->
        <option value=""></option>
        <c:forEach var="option" items="${listeOptions}">
            <option value="<c:out value="${option}"/>">
                <c:out value="${option}"/>
            </option>
        </c:forEach>
    </select>
</div>

<!-- Tableau des étudiants disponibles avec leur nom, leur prénom et leur id caché -->
<div class="row">
    <div id="tableAttribution" style="overflow: auto">
        <table class="table table-striped">
            <thead>
            <tr>
                <th>Nom</th>
                <th>Prénom</th>
                <th style="display:none">Id</th>
            </tr>
            </thead>
            <tbody id="divEtudiant">
            <!-- Affichage de la liste des étudiants disponibles. Ceux-ci deviennent draggables -->
            <c:forEach var="etudiant" items="${listeEtudiantsI2Dispos}" varStatus="loop">
                <tr data-option='${optionEtudiant[loop.index]}'
                    id="draggableEtudiant-${loop.index}" draggable='true'
                    ondragstart="dragEtudiant(event,${loop.index});">
                    <td id="nom-${loop.index}"><c:out value="${etudiant.nom}"/></td>
                    <td id="prenom-${loop.index}"><c:out value="${etudiant.prenom}"/></td>
                    <td id="idMembre-${loop.index}" style="display:none"><c:out value="${etudiant.idUtilisateur}"/></td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>
</div>