<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>



<div class="row">

	<table class="table table-striped">
		<thead>
			<tr>
				<th>Titre</th>
				<th style="display:none">Id</th>
			</tr>
		</thead>
		<tbody id="divSujet">
			<!-- Affichage de la liste des sujets. Ceux-ci deviennent draggables -->
			<c:forEach var="sujet" items="${listeSujets}" varStatus="loop">
				<tr
					data-option='${optionSujet[loop.index]}'
					id="draggable-${loop.index}"
					draggable='true' onClick='clickOnSujet("${loop.index}");'
					ondragstart="drag(event,${loop.index});"
					onmouseout="fmouseup(${loop.index});"
					ondragend="enddrag(event,${loop.index});"
					onmousedown="fmousedown(${loop.index});"
					onmouseup="fmouseup(${loop.index});">
					<th id="titre-${loop.index}" ondblclick='drop(event,${loop.index});'><c:out value="${sujet.titre}" /></th>
					<th style="display:none" id="idSujet-${loop.index}"><c:out value="${sujet.idSujet}" /></th>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</div>