<%--
  Created by IntelliJ IDEA.
  User: dimitrijarneau
  Date: 10/06/2018
  Time: 18:18
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<style>

</style>
<!-- Sidebar Holder -->
<nav id="sidebar">
    <div class="sidebar-header">
        <a href="<c:url value="/Dashboard"/>">
            <img class="pull-left" src="<c:url value="/images/eseo_carre.png"/>" alt="Logo ESEO"/>
        </a>
        <h3>So Manager</h3>

        <strong>
            SM
        </strong>
    </div>
    <div class="separator"></div>
    <ul class="list-unstyled components">
        <li>
            <a href="<c:url value="/Dashboard"/>">
                <i class="fas fa-fw fa-home"></i>
                Accueil
            </a>
        </li>
        <li>
            <a href="#sujetSubmenu" data-toggle="collapse" aria-expanded="false">
                <i class="fas fa-fw fa-book" aria-hidden="true"></i>Sujet
            </a>
            <ul class="collapse list-unstyled" id="sujetSubmenu">
                <c:if test="${etudiant eq 'true' || prof eq 'true'|| entrepriseExt eq 'true'|| profOption eq 'true'|| profResponsable eq 'true'|| assistant eq 'true'}">

                    <li>
                        <a href="<c:url value="/AjouterSujet"/>"
                           id="ajouterSujet"><i class="fas fa-fw fa-plus"
                                                aria-hidden="true"></i>Ajouter un sujet</a>
                    </li>
                    <li><a href="<c:url value="/VoirMesSujets"/>"><i
                            class="fas fa-fw fa-pencil-alt" aria-hidden="true"></i>Modifier un sujet</a></li>

                    <c:if test="${assistant eq 'true'}">
                        <li><a id="depot" href="<c:url value="/DepotGoogle"/>"><i
                                class="far fa-fw fa-file" aria-hidden="true"></i>Depot depuis formulaire</a></li>
                    </c:if>
                </c:if>

                <!-- Les roles autorisés à suivre un projet (référent, ...) -->
                <c:if
                        test="${prof eq 'true'||profOption eq 'true' || profResponsable eq 'true'}">
                    <li><a href="<c:url value="/InscriptionProfSujet"/>"><i
                            class="fas fa-fw fa-eye" aria-hidden="true"></i>Postuler</a></li>

                </c:if>
                <c:if
                        test="${prof eq 'true'||profOption eq 'true' || profResponsable eq 'true' || entrepriseExt eq 'true'}">
                    <li><a class="nav-item" href="<c:url value="/SujetSuivi"/>"><i
                            class="far fa-fw fa-star" aria-hidden="true"></i>Mes
                        Sujets </a></li>

                </c:if>
                <c:if test="${etudiantEquipe eq 'true' && etudiant eq 'true' }">
                    <li><a class="nav-item" href="<c:url value="/SujetSuivi"/>"><i
                            class="far fa-fw fa-star" aria-hidden="true"></i>Mon
                        Sujet</a></li>

                </c:if>
                <!-- Les roles autorisés à modifier l'état d'un sujet (valider, publier, refuser) -->
                <c:if test="${profOption eq 'true' || profResponsable eq 'true'}">
                    <li><a href="<c:url value="/GererSujets"/>"><i
                            class="fas fa-fw fa-list-ul" aria-hidden="true"></i>Gérer
                        les sujets</a></li>

                </c:if>

                <!-- Les roles autorisés à regarder les sujets qui sont publiés -->

                <li><a href="<c:url value="/ConsulterSujets"/>"><i
                        class="fas fa-fw fa-list-ul" aria-hidden="true"></i>Tous
                    les sujets</a></li>

            </ul>
        </li>
        <!-- Les roles autorisés à accéder à l'espace Equipe -->
        <c:if test="${ (etudiant eq 'true' && etudiantEquipe eq 'false') || profResponsable eq 'true' || prof eq 'true' || admin eq 'true'}">
            <li>
                <a href="#equipeSubmenu" data-toggle="collapse" aria-expanded="false">
                    <i class="fas fa-fw fa-user-circle" aria-hidden="true"></i>
                    Équipe
                </a>
                <ul class="collapse list-unstyled" id="equipeSubmenu">


                    <!-- Les roles autorisés à créer une équipe -->
                    <c:if test="${etudiantEquipe eq 'false' && etudiant eq 'true' || profResponsable eq 'true' || profReferent eq 'true' }">
                        <li><a href="<c:url value="/CreerEquipesI2"/>"
                               id="creerEquipesI2"><i class="fas fa-fw fa-users"
                                                      aria-hidden="true"></i>Créer/Gérer équipe I2</a></li>
                    </c:if>

                    <!-- Les roles autorisés à créer une équipe -->
                    <c:if test="${etudiantEquipe eq 'false' && etudiant eq 'true'}">
                        <li><a href="<c:url value="/CreerEquipe"/>"
                               id="creerEquipe"><i class="fas fa-fw fa-plus"
                                                   aria-hidden="true"></i>Créer équipe - I3</a></li>
                    </c:if>

                    <!-- Les roles autorisés à gérer une équipe -->
                    <c:if test="${profResponsable eq 'true'}">
                        <li><a href="<c:url value="/GererEquipes"/>"><i
                                class="fas fa-fw fa-list-ul" aria-hidden="true"></i>Gérer
                            les équipes - I3</a></li>
                    </c:if>

                    <!-- Les roles autorisés à voir le trombinoscope -->
                    <c:if
                            test="${profResponsable eq 'true' || prof eq 'true' || admin eq 'true'}">
                        <li><a href="<c:url value="/Trombinoscope"/>"><i
                                class="fas fa-fw fa-camera-retro"
                                aria-hidden="true"></i>Trombinoscope</a>
                        </li>
                    </c:if>
                </ul>
            </li>
        </c:if>

        <!-- Les roles autorisés à accéder à l'espace meetings -->
        <c:if test="${etudiant eq 'true' || prof eq 'true'||  profOption eq 'true'|| profResponsable eq 'true'}">
            <li>
                <a href="<c:url value="/GererMeetings"/>" id="Meetings">
                    <i class="fas fa-fw fa-comments"></i>
                    Meetings
                </a>
            </li>
        </c:if>
        <!-- Les roles autorisés à accéder à l'espace voir ses notes et celles des autres equipes -->


        <!-- Les roles autorisés à accéder à l'espace Professeur -->
        <c:if test="${profResponsable eq 'true'}">
            <li>
                <a href="#profSubmenu" data-toggle="collapse" aria-expanded="false">
                    <i class="fas fa-fw fa-user" aria-hidden="true"></i>Prof
                </a>
                <ul class="collapse list-unstyled" id="profSubmenu">
                    <li><a href="<c:url value="/GererProfesseurs"/>"><i
                            class="fas fa-fw fa-list-ul" aria-hidden="true"></i>Gérer
                        les professeurs
                    </a></li>
                    <li><a class="nav-item"
                           href="<c:url value="/PartagerRole"/>" id="partager"><i
                            class="far fa-fw fa-handshake" aria-hidden="true"></i> Partager
                        Role
                    </a></li>
                </ul>
            </li>
        </c:if>

        <!-- Les roles autorisés à accéder à l'espace de notation -->
        <c:if test="${profResponsable eq 'true' || profOption eq 'true' || prof eq 'true' || assistant eq 'true' || etudiant eq 'true'|| scrumMaster eq 'true' || productOwner eq 'true' || profReferent eq 'true'}">
            <li>
                <a href="#notationSubmenu" data-toggle="collapse" aria-expanded="false">
                    <i class="fas fa-fw fa-calendar-check"
                       aria-hidden="true"></i>Notes
                </a>
                <ul class="collapse list-unstyled" id="notationSubmenu">
                    <c:if test="${profReferent eq 'true' || profResponsable eq 'true' || productOwner eq 'true' || scrumMaster eq 'true'}">
                        <li><a href="<c:url value="/Noter"/>"><i
                                class="fas fa-fw fa-pencil-alt" aria-hidden="true"></i>Noter</a></li>
                    </c:if>
                    <c:if test="${assistant eq 'true'}">
                        <li><a href="<c:url value="/ExporterNotes"/>"><i
                                class="fas fa-fw fa-file-excel" aria-hidden="true"></i>Exporter
                            les notes</a></li>
                    </c:if>
                    <c:if test="${scrumMaster eq 'true' || productOwner eq 'true'}">
                        <li><a href="<c:url value="/BonusMalus"/>"><i
                                class="fas fa-fw fa-balance-scale" aria-hidden="true"></i>Bonus/Malus</a></li>
                    </c:if>
                    <c:if test="${scrumMaster eq 'true' || etudiant eq 'true' || profResponsable eq 'true'}">
                        <li><a href="<c:url value="/VoirLesNotes"/>" id="Notation">
                            <i class="far fa-calendar-check"></i>
                            Notes
                        </a></li>
                    </c:if>
                    <c:if test="${profResponsable eq 'true'}">

                        <li><a href="<c:url value="/GererNotes"/>"><i
                                class="fas fa-fw fa-th" aria-hidden="true"></i>Gérer
                            Notes</a></li>
                    </c:if>
                </ul>
            </li>
        </c:if>

        <!-- Les roles autorisés à accéder à l'espace Jurys -->
        <c:if test="${profResponsable eq 'true' || profReferent eq 'true'}">
            <li>
                <a href="#jurySubmenu" data-toggle="collapse" aria-expanded="false">
                    <i class="fas fa-fw fa-users"
                       aria-hidden="true"></i> Jurys
                </a>
                <ul class="collapse list-unstyled" id="jurySubmenu">
                    <c:if test="${profResponsable eq 'true'}">
                        <li><a href="<c:url value="/GererJuryPFE"/>"><i
                                class="fas fa-fw fa-cog"></i>Gérer Jurys PFE
                        </a></li>
                    </c:if>
                    <c:if test="${profReferent eq 'true'}">
                        <li><a href="<c:url value="/GererJuryPGL"/>"><i
                                class="fas fa-fw fa-cog"></i>Gérer Jurys PGL
                        </a></li>
                    </c:if>

                </ul>
            </li>
        </c:if>

        <!-- Les roles autorisés à accéder à l'espace poster -->
        <c:if test="${assistant eq 'true'}">
            <li>
                <a href="#posterSubmenu" data-toggle="collapse" aria-expanded="false">
                    <i class="fas fa-fw fa-file-pdf" aria-hidden="true" id="Poster"></i>Posters
                </a>
                <ul class="collapse list-unstyled" id="posterSubmenu">
                    <li><a href="<c:url value="/ExporterPoster"/>">
                        <i class="far fa-fw fa-file-archive" id="Exporter">
                        </i>Exporter</a>
                    </li>
                </ul>
            </li>
        </c:if>

        <!-- Les roles autorisés à accéder à l'espace gestion des projets I3 / I2 -->
        <c:if test="${ prof eq 'true'||  profOption eq 'true'|| profResponsable eq 'true' || profReferent eq 'true'}">
            <li>
                <a href="<c:url value="/GestionProjetsEtPFE"/>" id="Sprints">
                    <i class="fas fa-fw fa-cogs"></i>
                    PFE/I2
                </a>
            </li>
        </c:if>

        <!-- Les roles autorisés à accéder à l'espace Administrateur -->
        <c:if test="${admin eq 'true' }">
            <li>
                <a href="#adminSubmenu" data-toggle="collapse" aria-expanded="false">
                    <i class="fas fa-fw fa-key" aria-hidden="true" id="Administrateur"></i>Admin
                </a>
                <ul class="collapse list-unstyled" id="adminSubmenu">
                    <li><a href="<c:url value="/GererApplication"/>"><i class="glyphicon glyphicon-cog"
                                                                        id="ParamApp"></i>Paramètres de
                        l'application</a></li>
                    <li><a href="<c:url value="/GererRoles"/>"><i class="fas fa-fw fa-tag" aria-hidden="true"></i>Gestion
                        Rôles</a></li>
                    <li><a href="<c:url value="/GererComptes"/>"><i class="fas fa-fw fa-wrench" aria-hidden="true"
                                                                    id="GererCompte"></i>Gestion des
                        Comptes</a></li>

                </ul>
            </li>
        </c:if>

    </ul>

    <ul class="list-unstyled CTAs">
        <!-- Espace Connexion -->
        <c:if test="${empty sessionScope.utilisateur}">
            <li>
                <div class="row">
                    <div class="col-md-12">
                        <form method="post" action="<c:url value="/Connexion"/>">
                            <fieldset>
                                <label class="col-md-12" for="identifiant">Identifiant
                                    : <span class="requis">*</span>
                                </label>
                                <div class="col-md-12">
                                    <input type="text" name="identifiant"
                                           id="identifiantConnexion" class="form-control" autofocus
                                           required/> <br>
                                </div>

                                <label class="col-md-12" for="motDePasse">Mot de
                                    passe : <span class="requis">*</span>
                                </label>
                                <div class="col-md-12">
                                    <input type="password" name="motDePasse"
                                           id="motDePasseConnexion" class="form-control" required/>
                                    <br>
                                </div>

                                <div class="col-md-12" style="margin-bottom: 10px">
                                    <button type="submit" class="btn btn-info btn-block" id="submitConnexion">
                                        <i class="fas fa-fw fa-sign-in-alt" aria-hidden="true"></i>&nbsp;
                                        Se Connecter
                                    </button>
                                </div>

                                <div class="col-md-12" style="margin:0;">
                                    <button type="button" class="btn btn-info btn-block" id="inscription"
                                            data-target="/Inscription">
                                        <i class="fas fa-fw fa-plus-circle" aria-hidden="true"></i>&nbsp;
                                        S'inscrire
                                    </button>

                                </div>


                            </fieldset>
                        </form>

                    </div>

                </div>
            </li>

        </c:if>
    </ul>
</nav>