<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!-- Création d'un menu déroulant pour des filtres de sélection -->
<div class="row">
	Sélection par option :
	
	<select class="form-control"
		name="option" id="option" onchange="giveSelection()">
		<!-- this.value -->
		<option value=""></option>
		<c:forEach var="option" items="${listeOptions}">
			<option value="<c:out value="${option}"/>">
				<c:out value="${option}" />
			</option>
		</c:forEach>
	</select>
</div>

<!-- Tableau des étudiants disponibles avec leur nom, leur prénom et leur id caché -->
<div class="row">
	<div id="tableAttribution" style="overflow: auto" >
	<table class="table table-striped">
		<thead>
			<tr>
				<th>Nom</th>
				<th>Prénom</th>
				<th>Contrat Pro</th>
				<th style="display:none">Id</th>
			</tr>
		</thead>
		<tbody id="divEtudiant">
			<!-- Affichage de la liste des étudiants disponibles. Ceux-ci deviennent draggables -->
			<c:forEach var="etudiant" items="${listeEtudiantsDispos}" varStatus="loop">
				<tr data-option='${optionEtudiant[loop.index]}'
					id="draggableEtudiant-${loop.index}" draggable='true'
					ondragstart="dragEtudiant(event,${loop.index});">
					<td id="nom-${loop.index}"><c:out value="${etudiant.nom}" /></td>
					<td id="prenom-${loop.index}"><c:out value="${etudiant.prenom}" /></td>
					<c:forEach var="etudiantI3" items="${listeEtudiantsI3}" varStatus="loop2">
						<c:if test="${etudiantI3.idEtudiant eq etudiant.idUtilisateur}">
							<c:if test="${etudiantI3.contratPro == 'non'}">
								<td id="contratPro-${loop.index}" style="color:red;text-align:center;"><c:out value="${etudiantI3.contratPro}"/></td>
							</c:if>
							<c:if test="${etudiantI3.contratPro == 'oui'}">
								<td id="contratPro-${loop.index}" style="color:blue;text-align:center;"><c:out value="${etudiantI3.contratPro}"/></td>
							</c:if>
						</c:if>
					</c:forEach>
					<td id="idMembre-${loop.index}" style="display:none"><c:out value="${etudiant.idUtilisateur}" /></td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
	</div>
</div>