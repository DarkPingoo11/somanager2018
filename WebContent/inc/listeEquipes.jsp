<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<div class = "row">
	<c:forEach var="equipe" items="${listeEquipes}" varStatus="loop">
		<c:if test="${equipe.valide =='non'}">
			<div id="equipe-${loop.index}" class="panel panel-default" ondrop="dropMembre(event,${loop.index},'${equipe.idEquipe}')" ondragover="allowDrop(event)"
					ondragleave="refuseDrop(event)">
		</c:if>
		<c:if test="${equipe.valide =='oui'}">
			<div id="equipe-${loop.index}" class="panel panel-default">
		</c:if>
			<div class="panel-heading" value="Afficher ou Masquer"
			onClick="AfficherMasquer('${loop.index}')">
				<span id="chevron-${loop.index}"
				class="glyphicon glyphicon-chevron-down pull-right"></span>
				${equipe.idEquipe}
			</div>
			
			<div id="divacacher-${loop.index}" class="panel-body" style="display: none;">
				Taille de l'équipe : <strong>${equipe.taille}</strong> étudiant(s)
				<br>
				Équipe validée : 
					<c:if test="${equipe.valide eq 'oui'}"><strong>oui</strong></c:if>
					<c:if test="${equipe.valide eq 'non'}"><strong>non</strong></c:if>
				<br>
				
				<c:forEach var="sujet" items="${listeSujetsEquipes}" varStatus="loop2">
					<c:if test="${equipe.idSujet eq sujet.idSujet}">
						Titre du sujet : <strong>${sujet.titre}</strong>
						<br>
						Nombre minimum d'étudiants : <strong>${sujet.nbrMinEleves}</strong>
						<br>
						Nombre maximum d'étudiants : <strong>${sujet.nbrMaxEleves}</strong>
						<br>
						Contrat pro : 
						<c:if test="${sujet.contratPro eq 'true'}"><strong>oui</strong></c:if>
						<c:if test="${sujet.contratPro eq 'false'}"><strong>non</strong></c:if>
						<br>
						Option(s) du sujet : 
						<c:forEach var="optionSujet" items="${listeOptionsSujets}" varStatus="loop3">
							<c:if test="${sujet.idSujet eq optionSujet.idSujet}">
								<c:forEach var="option" items="${listeOptionsESEO}" varStatus="loop4">
									<c:if test="${optionSujet.idOption eq option.idOption}">
										<strong>${option.nomOption}</strong>
									</c:if>
								</c:forEach>
							</c:if>
						</c:forEach>
						<br>
						Professeur référent sur le sujet :
						<c:forEach var="professeurSujet" items="${referents}" varStatus="loop9">
							<c:if test="${professeurSujet.idSujet eq sujet.idSujet}">
								<c:forEach var="professeur" items="${referentsUtilisateurs}" varStatus="loop10">
									<c:if test="${professeur.idUtilisateur eq professeurSujet.idProfesseur}">
										<strong>${professeur.prenom} ${professeur.nom}</strong>
									</c:if>
								</c:forEach>
							</c:if>
						</c:forEach>
						
						<br>
						<table class="table table-striped" id="listMembre-${loop.index}">
							<caption style="text-align:center;">Membres de l'équipe</caption>
							<thead>
								<tr>
									<th>Nom</th>
									<th>Prénom</th>
									<th>Option</th>
									<c:if test="${equipe.valide =='non'}">
										<th style="display:none;">Bouton supprimer</th>
									</c:if>
								</tr>
							</thead>
							<tbody >
								<c:forEach var="membreEquipe" items="${listeEtudiantsEquipes}" varStatus="loop5">
									<c:if test="${membreEquipe.idEquipe eq equipe.idEquipe}">
										<tr>
											<c:forEach var="utilisateur" items="${listeUtilisateurs}" varStatus="loop6">
												<c:if test="${utilisateur.idUtilisateur eq membreEquipe.idEtudiant}">
													<td>${utilisateur.nom}</td>
													<td>${utilisateur.prenom}</td>
													<c:forEach var="roleUtilisateur" items="${listeRoleUtilisateur}" varStatus="loop7">
														<c:if test="${roleUtilisateur.idUtilisateur eq utilisateur.idUtilisateur}">
															<c:forEach var="option" items="${listeOptionsESEO}" varStatus="loop8">
																<c:if test="${option.idOption eq roleUtilisateur.idOption}">
																	<td> ${option.nomOption}</td>
																</c:if>
															</c:forEach>
														</c:if>	
													</c:forEach>
													<c:if test="${equipe.valide =='non'}">
														<td>
															<form class="col-lg-6" method="post" action="<c:url value="/GererEquipes"/>">
																<input type="hidden" name="idMembre" id="idMembre" value="${membreEquipe.idEtudiant}"/>
																<input type="hidden" name="tailleEquipe" id="tailleEquipe" value="${equipe.taille}"/>
																<input type="hidden" name="nbrMinEleves" id="nbrMinEleves" value="${sujet.nbrMinEleves}"/>
																<input type="hidden" name="formulaire" id="formulaire" value="supprimerMembre"/>
																<button type="submit" class="btn btn-danger center-block" name="supEtuEqu-${membreEquipe.idEtudiant}" id="supEtuEqu-${membreEquipe.idEtudiant}">
																	<i class="far fa-trash-alt fa-fw" aria-hidden="true"></i>&nbsp;
																</button>
															</form>
														</td>
													</c:if>
												</c:if>
											</c:forEach>
										</tr>
									</c:if>
								</c:forEach>
							</tbody>
						</table>
							<div class="text-center">
								<c:if test="${equipe.valide == 'non'}">
									<form class="col-lg-6" method="post" action="<c:url value="/GererEquipes"/>">
										<input type="hidden" name="idEquipe" id="idEquipe" value="${equipe.idEquipe}"/>
										<input type="hidden" name="formulaire" id="formulaire" value="validerEquipe"/>
										<button type="submit" class="btn btn-primary center-block" name="valEquipe-${equipe.idEquipe}" id="valEquipe-${equipe.idEquipe}">
											<i class="fas fa-check-circle fa-fw" aria-hidden="true"></i>&nbsp; Valider l'équipe sur ce sujet
										</button>
									</form>
								</c:if>
								<form class="col-lg-6" method="post" action="<c:url value="/GererEquipes"/>"/>
									<input type="hidden" name="idEquipe" id="idEquipe" value="${equipe.idEquipe}"/>
									<input type="hidden" name="formulaire" id="formulaire" value="refuserEquipe"/>
									<button type="submit" class = "btn btn-danger center-block" name="supEquipe-${equipe.idEquipe}" id="supEquipe-${equipe.idEquipe}">
										<i class="far fa-trash-alt fa-fw" aria-hidden="true"></i>&nbsp; Supprimer l'équipe
									</button>
								</form>
							</div>
					</c:if>
				</c:forEach>
			</div>
			
		</div>
	</c:forEach>
</div>

<script>
		/* Masque/Affiche une div*/

		function AfficherMasquer(idEquipe) {
			divInfo = document.getElementById("divacacher-" + idEquipe);
			chevron = document.getElementById("chevron-" + idEquipe);

			if (divInfo.style.display == 'none') {
				divInfo.style.display = '';
				chevron.className = 'glyphicon glyphicon-chevron-up pull-right';

			} else {
				divInfo.style.display = 'none';
				chevron.className = 'glyphicon glyphicon-chevron-down pull-right';

			}
		}
		
		var idEtudiant="";
		function allowDrop(ev) {
			ev.preventDefault();
		}

		function refuseDrop(ev) {
			ev.preventDefault();
		}
		
		function dragEtudiant(event,id){
			idEtudiant=id;
		}
		
		function dropMembre(event,id,idEquipe){
			divEtudiant = document.getElementById("draggableEtudiant-" + idEtudiant);
			table = document.getElementById("listMembre-" + id);
			
			if (document.getElementById("divacacher-" + id).style.display == 'none'){
				AfficherMasquer(id);
			}
			// Create an empty <tr> element and add it to the 1st position of the table:
			var row = table.insertRow(1);
			
			// Insert new cells (<td> elements) at the 1st and 2nd position of the "new" <tr> element:
			var cell1 = row.insertCell(0);
			var cell2 = row.insertCell(1);
			var cell3 = row.insertCell(2);
			var cell4 = row.insertCell(3);
			
			var idNouveauMembre= divEtudiant.cells[3].innerHTML;
			// Add some text to the new cells:
			
			cell1.innerHTML = divEtudiant.cells[0].innerHTML;
			cell2.innerHTML = divEtudiant.cells[1].innerHTML;
			cell3.innerHTML = divEtudiant.dataset.option;
			cell4.innerHTML = "<td><form class='col-lg-6' method='post' action='<c:url value='/GererEquipes'/>'>"
			+ "<input type='hidden' name='idMembre' id='idMembre' value='"+idNouveauMembre+"'/>"
			+ "<input type='hidden' name='idEquipe' id='idEquipe' value='"+idEquipe+"'/>"
			+ "<input type='hidden' name='formulaire' id='formulaire' value='ajouterMembre'/>"
			+ "<button type='submit' class='btn btn-success center-block' name='addEtuEqu-"+idNouveauMembre+"' id='addEtuEqu-"+idNouveauMembre+"'>"
			+ "<span class='glyphicon glyphicon-plus'></span></button></form></td>";
			
			divEtudiant.style.display = 'none';
			
			
		}
		
	</script>