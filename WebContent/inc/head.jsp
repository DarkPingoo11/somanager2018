<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%-- Contenu de la balise <head> </head> de chaque page--%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="shortcut icon" type="image/x-icon" href="<c:url value="/images/favicon.ico"/>"/>
<link rel="stylesheet" href="<c:url value="/css/bootstrap.min.css"/>">
<link rel="stylesheet" href="<c:url value="/css/fonts.css"/>">
<link rel="stylesheet" href="<c:url value="/css/style.css"/>">
<link rel="stylesheet" href="<c:url value="/css/fontawesome-all.css"/>">
<link rel="stylesheet" href="<c:url value="/css/colors.css"/>">
<link rel="stylesheet" href="<c:url value="/css/perfect-scrollbar.css"/>">
<link rel="stylesheet" href="<c:url value="/css/toastr.min.css"/>">
<link rel="stylesheet" href="<c:url value="/css/bootstrap-select.min.css"/>"/>
<link rel="stylesheet" href="<c:url value="/css/styleSidebar.css"/>">
