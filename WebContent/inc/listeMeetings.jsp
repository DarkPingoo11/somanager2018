<%--
  Created by IntelliJ IDEA.
  User: dimitrijarneau
  Date: 17/04/2018
  Time: 09:10
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>


<div class="row">

    <table class="table table-striped">
        <thead>
        <c:if test="${empty listeMeeting }">
            <p align="center">Aucun meeting n'a été effectué.</p>
        </c:if>
        <c:if test="${!empty listeMeeting }">
        <tr>
            <th>Titre du meeting</th>
            <th>Date</th>
            <th>Lieu</th>
            <th>Description</th>
                <%--
                <th>Actions</th>
                --%>
        </tr>
        </thead>
        <tbody id="divMeeting">


        <!-- Affichage de la liste des réunions. Ceux-ci deviennent draggables -->
        <c:forEach var="meeting" items="${listeMeeting}" varStatus="loop">
            <tr
                    id="draggable-${loop.index}" draggable='true'
                    onClick='clickOnSujet("${loop.index}");'
                    ondragstart="drag(event,${loop.index});"
                    onmouseout="fmouseup(${loop.index});"
                    ondragend="enddrag(event,${loop.index});"
                    onmousedown="fmousedown(${loop.index});"
                    onmouseup="fmouseup(${loop.index});">
                <th id="idReunion-${loop.index}" style="display:none"><c:out value="${meeting.idReunion}"/></th>
                <th id="titre-${loop.index}" style="max-width: 50px; word-wrap: break-word;"
                    ondblclick='drop(event,${loop.index});'><c:out
                        value="${meeting.titre}"/></th>
                <th id="date-${loop.index}"><c:out value="${(meeting.date).substring(0,16)}"/></th>
                <th id="lieu-${loop.index}" style="max-width: 80px; word-wrap: break-word;"><c:out
                        value="${meeting.lieu}"/></th>
                <th id="description-${loop.index}" style="max-width: 30px; word-wrap: break-word;"><c:out
                        value="${meeting.description}"/></th>
            </tr>
        </c:forEach>
        </c:if>
        </tbody>
    </table>
</div>
