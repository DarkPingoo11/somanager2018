<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<!--  margin:20% -5% 0px 43%; -->
<c:set var="count" value="0" scope="page" />
<!-- margin:0px 0px 0px 45%; background-image: url(<c:url value="/images/commentaire.png"/>); background-size: cover;"-->

<div id="commentairehidden-${loop.index}" style="position:absolute;  display: none;  max-height: 80%; overflow-y: auto; max-width:60%; min-width:60%;">
	<c:forEach var="commentaire" items="${commentaire}" varStatus="loopComment">
	<c:if test="${sujet.idSujet==commentaire.idSujet}">
		<div style="margin:3px 3px 3px 3px; padding: 0px 3px 3px 3px; background-color:#337ab7;" onclick='ecrirehidden("${loopComment.index}","${loop.index}");'>
			<p style="margin:0px 0px 0px 0px; text-decoration:underline; color:white;"> <c:out value="${nomsExpediteursCommentaires[commentaire.idUtilisateur]}"/> </p>
			<p style="margin:0px 0px 0px 0px; background-color:white;"> <c:out value="${commentaire.contenu}"/></p>
		</div>
		<c:set var="count" value="${count + 1}" scope="page"/>
		<c:forEach var="commentaireSec" items="${commentairesSecondaires}" varStatus="loopComSec">
		<c:if test="${commentaire.idCommentaire==commentaireSec.idObservation}">
			<div style="margin:3px 3px 3px 20px; padding: 0px 3px 3px 3px; background-color:#5aa5e8;" onclick='ecrirehidden("${loopComment.index}","${loop.index}");'>
			<p style="margin:0px 0px 0px 0px;  text-decoration:underline; "> <c:out value="${nomsExpediteursCommentaires[commentaireSec.idUtilisateur]}"/> </p>
			<p style="margin:0px 0px 0px 0px; background-color:white; "> <c:out value="${commentaireSec.contenu}"/></p>
			</div>
			<c:set var="count" value="${count + 1}" scope="page"/>
		</c:if>
		</c:forEach>
		<form  id="subComment-${loopComment.index}" style="display: none; background-color:#5aa5e8; margin:0px 3px 3px 20px;" method="post" action="<c:url value="/Commenter"/>" >
			<input  type="hidden" name="idSujet" id="idSujet"  value="<c:out value="${sujet.idSujet}"/>"/>
			<input  type="hidden" name="idObservation" id="idObservation"  value="<c:out value="${commentaire.idCommentaire}"/>"/>
			<input  type="hidden" name="nomSujet" id="nomSujet"  value="<c:out value="${sujet.titre}"/>"/>
			<input type="hidden" name="idUtilisateur" id="idUtilisateur" value="<c:out value="${commentaire.idUtilisateur}"/>"/>
			
		  	<label style="max-width:90%; min-width:90%; margin:3px 3px 3px 3px;"> <textarea style="min-width:100%;  vertical-align:middle;"  name="contenu" id="contenu" placeholder="Répondre à <c:out value="${nomsExpediteursCommentaires[commentaire.idUtilisateur]}"/>" required></textarea></label>
		  	<button style="max-width:5%; min-width:5%; min-height:50px; vertical-align:middle;" type="submit" id="submit">></button>
			
		</form>
	</c:if>
	</c:forEach>
	
	
	<form  style="max-width:99%; min-width:99%; background-color:#337ab7; margin:0px 3px 3px 3px;" method="post" action="<c:url value="/Commenter"/>" >
		<input  type="hidden" name="idSujet" id="idSujet"  value="<c:out value="${sujet.idSujet}"/>"/>
		<input  type="hidden" name="idObservation" id="idObservation"  value="-1"/>
		<input  type="hidden" name="nomSujet" id="nomSujet"  value="<c:out value="${sujet.titre}"/>"/>
		<input type="hidden" name="idUtilisateur" id="idUtilisateur" value="<c:out value="${idPorteursSujets[sujet.idSujet]}"/>"/>
	  	<label style="max-width:90%; min-width:90%; margin:3px 3px 3px 3px;"> <textarea style="max-width:100%; min-width:100%; vertical-align:middle; resize:vertical; "  name="contenu" id="contenu" placeholder="Commenter ce sujet " required></textarea></label>
	  	
	  	<button style=" max-width:5%; min-width:5%; min-height:50px; vertical-align:middle;" type="submit" id="submit">></button>
		
	</form>
</div>
<div style=" margin:0px 0px 0px 5%;" onclick='commentairehidden("${loop.index}");'>
	<div style="position:absolute;z-index:1;" >
      <img src="<c:url value="/images/commentaireNombre.png"/>" width=38px; height=50px >
   	</div>
    <div style="position:absolute; z-index:2; padding:3px 3px 3px 3px;">
    	<p style="color:white; font-size:20px;"> <c:out value="${count}"/>  </p>
    </div> 
</div>

    <!--<center> <c:out value="${listeSujets}"/> </center>-->
  		 

