<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<style>
.navbar-brand {
	display: flex;
	align-items: center;
}

.navbar-brand>img {
	padding: 7px 14px;
}

.dropbtn {
	border: none;
	cursor: pointer;
	background-color: #ffffff;
}

.dropbtn:hover, .dropbtn:focus {
	background-color: #f1f1f1;
}

.dropdown {
	position: relative;
	display: inline-block;
}

.dropdown-content {
	display: none;
	position: absolute;
	background-color: #ffffff;
	min-width: 160px;
	overflow: auto;
	box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
	z-index: 1;
}

.dropdown-content a {
	color: black;
	padding: 12px 16px;
	text-decoration: none;
	display: block;
}

.dropdown a:hover {
	background-color: #ffffff
}

.show {
	display: block;
}

.button {
	background: none;
	border: none;
}

.navbar-brand>.fas {
	color: #3278b4;
}
</style>

<nav class="navbar navbar-default" role="navigation">
	<div class="container-fluid">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed"
				data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
				<span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="<c:url value="/Dashboard"/>"><i
				class="fas fa-home"></i></a>
		</div>

		<div class="collapse navbar-collapse"
			id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav">

				<li class="dropdown"><a href="" class="dropdown-toggle"
					id="sujet" data-toggle="dropdown" role="button"
					aria-haspopup="true" aria-expanded="false"> <i
						class="fas fa-book fa-fw" aria-hidden="true"></i>&nbsp;Sujet&nbsp;
						<span class="caret"></span>
				</a>

					<ul class="dropdown-menu">

						<!-- Les roles autorisés à ajouter et à modifier un sujet -->
						<c:if
							test="${etudiant eq 'true' || prof eq 'true'|| entrepriseExt eq 'true'|| profOption eq 'true'|| profResponsable eq 'true'|| assistant eq 'true'}">

							<li><a href="<c:url value="/AjouterSujet"/>"
								id="ajouterSujet"><i class="fas fa-plus fa-fw"
									aria-hidden="true"></i>&nbsp;Ajouter</a></li>
							<li><a href="<c:url value="/VoirMesSujets"/>"><i
									class="fas fa-pencil-alt fa-fw" aria-hidden="true"></i>&nbsp;Modifier</a></li>
							<li role="separator" class="divider"></li>
							<li><a id="depot" href="<c:url value="/DepotGoogle"/>"><i
									class="far fa-file fa-fw" aria-hidden="true"></i>&nbsp;Depot</a></li>
							<li role="separator" class="divider"></li>
						</c:if>

						<!-- Les roles autorisés à suivre un projet (référent, ...) -->
						<c:if
							test="${prof eq 'true'||profOption eq 'true' || profResponsable eq 'true'}">
							<li><a href="<c:url value="/InscriptionProfSujet"/>"><i
									class="fas fa-eye fa-fw" aria-hidden="true"></i>&nbsp;Postuler</a></li>
							<li role="separator" class="divider"></li>
						</c:if>
						<c:if
							test="${prof eq 'true'||profOption eq 'true' || profResponsable eq 'true' || entrepriseExt eq 'true'}">
							<li><a class="nav-item" href="<c:url value="/SujetSuivi"/>"><i
									class="far fa-star fa-fw" aria-hidden="true"></i>&nbsp;Mes
									Sujets </a></li>
							<li role="separator" class="divider"></li>
						</c:if>
						<c:if test="${etudiantEquipe eq 'true' && etudiant eq 'true' }">
							<li><a class="nav-item" href="<c:url value="/SujetSuivi"/>"><i
									class="far fa-star fa-fw" aria-hidden="true"></i>&nbsp;Mon
									Sujet</a></li>
							<li role="separator" class="divider"></li>
						</c:if>
						<!-- Les roles autorisés à modifier l'état d'un sujet (valider, publier, refuser) -->
						<c:if test="${profOption eq 'true' || profResponsable eq 'true'}">
							<li><a href="<c:url value="/GererSujets"/>"><i
									class="fas fa-list-ul fa-fw" aria-hidden="true"></i>&nbsp;Gérer
									les sujets</a></li>
							<li role="separator" class="divider"></li>
						</c:if>

						<!-- Les roles autorisés à regarder les sujets qui sont publiés -->

						<li><a href="<c:url value="/ConsulterSujets"/>"><i
								class="fas fa-list-ul fa-fw" aria-hidden="true"></i>&nbsp;Tous
								les sujets</a></li>

					</ul></li>

				<!-- Les roles autorisés à accéder à l'espace Equipe -->
				<c:if
					test="${ (etudiant eq 'true' && etudiantEquipe eq 'false') || profResponsable eq 'true' || prof eq 'true' || admin eq 'true'}">

					<li class="dropdown"><a href="" class="dropdown-toggle"
						id="equipe" data-toggle="dropdown" role="button"
						aria-haspopup="true" aria-expanded="false"> <i
							class="fas fa-graduation-cap fa-fw" aria-hidden="true"></i>&nbsp;Équipe&nbsp;
							<span class="caret"></span></a>

						<ul class="dropdown-menu">

							<!-- Les roles autorisés à créer une équipe -->
							<c:if test="${etudiantEquipe eq 'false' && etudiant eq 'true'}">
								<li><a href="<c:url value="/CreerEquipe"/>"
									id="creerEquipe"><i class="fas fa-plus fa-fw"
										aria-hidden="true"></i>&nbsp;Créer</a></li>
							</c:if>

							<!-- Les roles autorisés à gérer une équipe -->
							<c:if test="${profResponsable eq 'true'}">
								<li><a href="<c:url value="GererEquipes"/>"><i
										class="fas fa-list-ul fa-fw" aria-hidden="true"></i>&nbsp;Gérer
										les équipes</a></li>
							</c:if>

							<!-- Les roles autorisés à voir le trombinoscope -->
							<c:if
								test="${profResponsable eq 'true' || prof eq 'true' || admin eq 'true'}">
								<li><a href="<c:url value="Trombinoscope"/>"><i
										class="fas fa-camera-retro fa-fw" aria-hidden="true"></i>&nbsp;Trombinoscope</a></li>
							</c:if>

						</ul></li>
				</c:if>

				<!-- Les roles autorisés à accéder à l'espace meetings -->
                <c:if test="${etudiant eq 'true' || prof eq 'true'||  profOption eq 'true'|| profResponsable eq 'true'}">
                    <li>
                        <a href="<c:url value="/GererMeetings"/>" id="Meetings">
                            <i class="fas fa-comments"></i>
                            &nbsp;Meetings&nbsp;</a>
                    </li>
                </c:if>


                <!-- Les roles autorisés à accéder à l'espace Professeur -->
				<c:if test="${profResponsable eq 'true'}">
					<li class="dropdown"><a href="" class="dropdown-toggle"
						id="professeur" data-toggle="dropdown" role="button"
						aria-haspopup="true" aria-expanded="false"> <i
							class="fas fa-user fa-fw" aria-hidden="true"></i>&nbsp;Prof&nbsp;
							<span class="caret"></span>
					</a>
						<ul class="dropdown-menu">
							<li><a href="<c:url value="/GererProfesseurs"/>"><i
									class="fas fa-list-ul fa-fw" aria-hidden="true"></i>&nbsp;Gérer
									les professeurs</a></li>
							<li><a class="nav-item"
								href="<c:url value="/PartagerRole"/>" id="partager"><i
									class="far fa-handshake fa-fw" aria-hidden="true"></i> Partager
									Role &nbsp; </a></li>
						</ul></li>
				</c:if>
				<!-- Les roles autorisés à accéder à l'espace de notation -->
				<c:if
					test="${profResponsable eq 'true' || profOption eq 'true' || prof eq 'true' || assistant eq 'true' || etudiant eq 'true' || scrumMaster eq 'true' || productOwner eq 'true'}">
					<li class="dropdown"><a class="dropdown-toggle" id="noter"
						data-toggle="dropdown" role="button" aria-haspopup="true"
						aria-expanded="false"> <i class="fas fa-calendar-check fa-fw"
							aria-hidden="true"></i>&nbsp;Notes&nbsp; <span class="caret"></span>
					</a>
						<ul class="dropdown-menu">
							<li><a href="<c:url value="/Noter"/>"><i
									class="fas fa-pencil-alt fa-fw" aria-hidden="true"></i>&nbsp;Noter</a></li>
							<c:if test="${assistant eq 'true'}">
								<li><a href="<c:url value="/ExporterNotes"/>"><i
										class="fas fa-file-excel fa-fw" aria-hidden="true"></i>&nbsp;Exporter
										les notes</a></li>
							</c:if>
							<c:if test="${scrumMaster eq 'true' || productOwner eq 'true'}">
								<li><a href="<c:url value="/BonusMalus"/>"><i
										class="fas fa-balance-scale fa-fw" aria-hidden="true"></i>&nbsp;Bonus/Malus</a></li>
							</c:if>
							<c:if test="${profResponsable eq 'true'}">
								<li role="separator" class="divider"></li>
								<li><a href="<c:url value="/GererNotes"/>"><i
										class="fas fa-th fa-fw" aria-hidden="true"></i>&nbsp;Gérer
										Notes</a></li>
							</c:if>
						</ul></li>
				</c:if>
				<!-- Les roles autorisés à accéder à l'espace Jurys -->
				<c:if test="${profResponsable eq 'true' || productOwner eq 'true'}">
					<li class="dropdown"><a class="dropdown-toggle" id="jurys"
						data-toggle="dropdown" role="button" aria-haspopup="true"
						aria-expanded="false"> <i class="fas fa-users fa-fw"
							aria-hidden="true"></i> &nbsp;Jurys&nbsp; <span class="caret"></span>
					</a>
						<ul class="dropdown-menu">
						<c:if test="${profResponsable eq 'true'}">
							<li><a href="<c:url value="/GererJuryPFE"/>"><span
									class="fas fa-cog fa-fw"></span>&nbsp;Gérer Jurys PFE</a></li></c:if>
						<c:if test="${productOwner eq 'true'}">
							<li><a href="<c:url value="/GererJuryPGL"/>"><span
									class="fas fa-cog fa-fw"></span>&nbsp;Gérer Jurys PGL</a></li></c:if>
						</ul></li>
				</c:if>


                <!-- Les roles autorisés à accéder à l'espace poster -->
                <c:if test="${assistant eq 'true'}">
                    <li class="dropdown"><a href="" class="dropdown-toggle"
                                            data-toggle="dropdown" role="button" aria-haspopup="true"
                                            aria-expanded="false"><i class="fas fa-file-pdf" aria-hidden="true" id="Poster"></i>&nbsp;Posters&nbsp;<span
                            class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="<c:url value="/ExporterPoster"/>"><span	class="far fa-file-archive" id="Exporter"></span>&nbsp;Exporter</a></li>
                            <c:if test="${profResponsable eq 'true'}">
                            <li><a href="<c:url value="/GenererPlanAffichagePoster"/>"><span class="far fa-file-archive" id="Plan"></span>&nbsp;Plan d'affichage</a></li>

                </c:if>

                </ul>
                    </li>
                </c:if>

                <!-- Les roles autorisés à accéder à l'espace gestion des projets I3 / I2 -->
                <c:if test="${ prof eq 'true'||  profOption eq 'true'|| profResponsable eq 'true'}">
                    <li>
                        <a href="<c:url value="/GestionProjetsEtPFE"/>" id="Sprints">
                            <i class="fas fa-cogs"></i>
                            &nbsp;PFE/I2&nbsp;
                    </a></li>
                </c:if>

                <!-- Les roles autorisés à accéder à l'espace Administrateur -->
                <c:if test="${admin eq 'true' }">
                    <li class="dropdown"><a href="" class="dropdown-toggle"
                                            data-toggle="dropdown" role="button" aria-haspopup="true"
                                            aria-expanded="false"><i class="fas fa-key fa-fw" aria-hidden="true" id="Administrateur"></i>&nbsp;Admin&nbsp;<span
                            class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="<c:url value="/GererApplication"/>"><span	class="glyphicon glyphicon-cog" id="ParamApp"></span>&nbsp;Paramètres de l'application</a></li>
                            <li><a href="<c:url value="/GererRoles"/>"><i class="fas fa-tag fa-fw" aria-hidden="true"></i>&nbsp;Gestion Rôles</a></li>
                            <li><a href="<c:url value="/GererComptes"/>"><i	class="fas fa-wrench fa-fw" aria-hidden="true" id="GererCompte"></i>&nbsp;Gestion des Comptes</a></li>

                    </ul>
                    </li>
                </c:if>
            </ul>


			<!-- Espace Connexion -->
			<c:choose>
				<c:when test="${empty sessionScope.utilisateur}">
					<ul class="nav navbar-nav navbar-right">
						<li class="dropdown"><a href="" class="dropdown-toggle"
							id="connexion" data-toggle="dropdown"><b>Se Connecter</b> <span
								class="glyphicon glyphicon-log-in"></span></a>
							<ul id="login-dp" class="dropdown-menu">
								<li>
									<div class="row">
										<div class="col-md-12">
											<form method="post" action="<c:url value="/Connexion"/>">
												<fieldset>
													<label class="col-lg-12" for="identifiant">Identifiant
														: <span class="requis">*</span>
													</label>
													<div class="col-lg-12">
														<input type="text" name="identifiant"
															id="identifiantConnexion" class="form-control" autofocus
															required /> <br>
													</div>

													<label class="col-lg-12" for="motDePasse">Mot de
														passe : <span class="requis">*</span>
													</label>
													<div class="col-lg-12">
														<input type="password" name="motDePasse"
															id="motDePasseConnexion" class="form-control" required />
														<br>
													</div>

													<div class="col-lg-12">
														<button type="submit" class="btn btn-primary"
															id="submitConnexion">
															<i class="fas fa-sign-in-alt fa-fw" aria-hidden="true"></i>&nbsp;
															Se Connecter
														</button>

													</div>
												</fieldset>
											</form>
										</div>
										<div class="bottom text-center">
											<br /> Nouveau ici ? <a href="<c:url value="/Inscription"/>"><b>S'inscrire</b></a>
										</div>
									</div>
								</li>
							</ul></li>
					</ul>
				</c:when>
				<c:otherwise>
					<ul class="nav navbar-nav navbar-right ">
						<li class="dropdown nopadding"><a href=""
							class="dropdown-toggle" id="connexion" data-toggle="dropdown"><b>
									${sessionScope.utilisateur.prenom}
									${sessionScope.utilisateur.nom}</b></a>
							<ul id="login-dp" class="dropdown-menu">
								<br>
								<c:choose>
									<c:when test="${fn:length(roles)>1}">
										<li style="text-align: center;"><b> Vos rôles </b></li>
									</c:when>
									<c:otherwise>
										<li style="text-align: center;"><b> Votre rôle </b></li>
									</c:otherwise>
								</c:choose>
								<br>

								<%-- Affichage des roles pour l'utilisateur --%>
								<c:forEach var="role" items="${requestScope.rolesAffichage}"
									varStatus="loop">
									<li><i class="fas fa-${ role.icone } fa-fw"
										aria-hidden="true"></i>&nbsp; ${ role.nomComplet }</li>
								</c:forEach>

								<li role="separator" class="divider "></li>
								<li style="text-align: center;"><a class="nav-item "
									href="<c:url value="/ModifierInfoUtilisateur"/>"
									id="ModifierInfoUtilisateur"> <b>Profil</b> <i
										class="fas fa-user" " aria-hidden="true"></i>&nbsp;
								</a></li>
								<li role="separator" class="divider "></li>
								<li
									style="text-align: center; margin-left: 20px; margin-right: 20px;"><a
									class="nav-item" href="<c:url value="/Deconnexion"/>"
									id="deconnexion"><b>Se Déconnecter </b> <i
										class="fas fa-sign-out-alt fa-fw" aria-hidden="true"></i> </a></li>

							</ul></li>

					</ul>

				</c:otherwise>
			</c:choose>

			<!-- Espace Notification -->
			<c:if test="${not empty sessionScope.utilisateur}">
				<ul class="nav navbar-nav navbar-right">
					<li><a class="nav-item">
							<div class="dropdown">
								<div class="dropbtn" onClick="functionDropDown()">
									<input class="dropbtn" type="image"
										style="position: absolute; margin: -0px 0px 0px -10px; background-color: transparent;"
										src="<c:url value="/images/bell.png"/>">
									<div
										style="position: absolute; margin: -7px 0px 0px 6px; color: #898989;"
										id=nombreAnnotation>0</div>
								</div>
								<form style="position: absolute; margin: 20px 0px 0px 0px;"
									id="myDropdown" class="dropdown-content" method="post"
									action="<c:url value="/Notifier"/>"></form>
							</div>

					</a></li>
				</ul>
			</c:if>


		</div>
	</div>
</nav>
<script src="<c:url value="/js/notification.js"/>" async defer></script>