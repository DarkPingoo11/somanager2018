<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!-- Scripts a inclure dans chaque page -->
<script type="application/javascript" src="<c:url value="/js/jquery-3.2.0.min.js"/>"></script>
<script type="application/javascript" src="<c:url value="/js/bootstrap.min.js"/>"></script>
<script type="application/javascript" src="<c:url value="/js/validator.min.js"/>"></script>
<script type="application/javascript" src="<c:url value="/js/toastr.js"/>"></script>
<script type="application/javascript" src="<c:url value="/js/scripts/fonctionsUsuelles.js"/>"></script>
<script type="application/javascript" src="<c:url value="/js/scripts/sidebar.js" />"></script>
<script type="application/javascript" src="<c:url value="/js/bootstrap-select.js"/>"></script>
<script type="application/javascript" src="<c:url value="/js/notification.js"/>" async defer></script>

<!-- Equipe de développement : -->
<!-- 2017 - 2018 -->
<!--
Juan Carlo ORTIZ BETANCUR
Dimitri JARNEAU
Tristan LE GACQUE
Emilien MAMALET

Damien MAYMARD
Quentin PICHAVANT
Gaetan SCUILLER
Anne Claire VERGOTE
Consultante : Céline MERAND
-->

<!-- 2016 - 2017 -->
<!--
Julie AVIZOU
Pierre CESMAT
Alexandre CLAMENS
Renaud DESCOURS

Maxime LENORMAND
Hugo MÉNARD
Thomas MÉNARD
Cécilia PINSARD
-->
