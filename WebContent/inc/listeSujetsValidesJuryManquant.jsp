<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>



<div class="row">
	<div class=" text-center" id="divSujet">
		<c:forEach var="sujet" items="${listeSujetsJuryManquant}"
			varStatus="loop">
			<input name="idSujet-${loop.index}" id="idSujet-${loop.index}"
					type="hidden" value="${sujet.idSujet}">
			<button style="width: 300px; overflow: auto;" type="button"
				class="btn btn-default btn-sm" id="draggableSujet-${loop.index}"
				draggable='true' ondragstart="dragSujet(event,${loop.index});"
				data-option='${optionSujet[loop.index]}'>
				<c:out value="${sujet.titre}" />
				<br/>
				<c:out value="${fn:substring(listeSoutenancesJuryManquant[loop.index].dateSoutenance,0,16)}" />
			</button>
		</c:forEach>
	</div>
</div>