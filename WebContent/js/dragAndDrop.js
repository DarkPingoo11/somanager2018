/** Change la couleur de la bordure lors du hover du div drop */
function allowDrop(ev) {
	ev.preventDefault();
	/*var img = new Image(); 
	img.src = 'modifier.gif'; 
	ev.dataTransfer.setDragImage(img, 0, 0);*/
	var divDrop = document.getElementById("drop");
	//ev.preventDefault();
	divDrop.style.border = "3px dashed #30e8db";
}

/** Change la couleur de la bordure lorsque l'on quitte le div drop */
function refuseDrop(ev) {
	ev.preventDefault();
	var divDrop = document.getElementById("drop");
	divDrop.style.border = "0";
}

/** Change la taille allouée des sujets lorsque l'on clique dessus
	(obslete depuis la mise des sujet dans un tableau)*/
function clickOnSujet(idSujet) {
	var node = document.getElementById("draggable-" + idSujet);
	var taille = ((parseFloat((window.getComputedStyle(document
			.getElementById("draggable-" + idSujet), null)
			.getPropertyValue('font-size'))) + 8) + "px");
	if (node.style["max-height"].length <= 4) {
		//document.getElementById("description-"+idSujet).textContent=""+description;
		document.getElementById("draggable-" + idSujet).style["max-height"] = '10000px';
		//document.getElementById("draggable-"+idSujet).style["border"]="2px solid #ffffff";
	} else {
		//document.getElementById("description-"+idSujet).textContent=""+description.substring(0,100)+"...";
		document.getElementById("draggable-" + idSujet).style["max-height"] = taille;
		//document.getElementById("draggable-"+idSujet).style["border"]="1px solid #ffffff";
	}
}

function fmousedown(idSujet){
	var node = document.getElementById("draggable-" + idSujet);
	node.style.cursor='-moz-grabbing';
	node.style.cursor='-webkit-grabbing';
	node.style.cursor='grabbing';
}

function fmouseup(idSujet){
	var node = document.getElementById("draggable-" + idSujet);
	node.style.cursor='-moz-grab';
	node.style.cursor='-webkit-grab';
	node.style.cursor='grab';
}

/*var dragImg = new Image(); // Il est conseillé de précharger l'image, sinon elle risque de ne pas s'afficher pendant le déplacement
dragImg.src = 'modifier.png';*/
/** Set l'ID du sujet que recevra la box drop lorsque le drag commence */
var color;
function drag(ev, id) {
	ev.dataTransfer.setData("id", id);
	//ev.dataTransfer.setData("innerHtml", ev.target.innerHTML);
	/*var img = document.createElement("img");
	img.src = "modifier.png";*/
	color=document.getElementById("draggable-"+id).style.background;
	//document.getElementById("draggable-"+id).style.border = "2px solid #ffbb2b";
	//document.getElementById("draggable-"+id).style.background = "#ffe877";
	//alert(dragImg);
	//ev.dataTransfer.setDragImage(img, 40, 40); // Une position de 40x40 pixels centrera l'image (de 80x80 pixels) sous le curseur
}

function enddrag(ev, id) {
	/*divDrag = document.getElementById("draggable-" + id);
	divDrag.style.border = "1px solid #000000";
	divDrag.style.background = color;*/
}

/** Change la couleur de l'état du sujet et autres implémentations graphiques non implémentées pour l'instant */
function changeColor() {
	var listEtat = document.querySelectorAll('*[id^="etat-"]');
	for (i = 0; i < listEtat.length; i++) {
		var id = listEtat[i].id.replace('etat-', '');
		if (listEtat[i].textContent == "DEPOSE") {
			listEtat[i].style.color = 'Chocolate';
		} else if (listEtat[i].textContent == "VALIDE") {
			listEtat[i].style.color = 'LightSeaGreen  ';
		} else if (listEtat[i].textContent == "ATTRIBUE") {
			listEtat[i].style.color = 'DodgerBlue';
		} else if (listEtat[i].textContent == "REFUSE") {
			listEtat[i].style.color = 'red';
		}else if (listEtat[i].textContent == "PUBLIE") {
			listEtat[i].style.color = 'Goldenrod  ';
		}
	}
}

/** Rend visible le formulaire de changement pour l'ID du sujet passé en paramètre */
function drop(ev, id) {
	var id = (id == undefined) ? ev.dataTransfer.getData("id") : id;

	divDrag = document.getElementById("draggable-" + id);
	//divDrag.style.border = "10px solid #000000";

	var divDrop = document.getElementById("drop");
	divDrop.style.border = "0";

	var options2 = divDrop.querySelectorAll('*[id^="drophidden-"]');
	for (var i = 0; i < options2.length; i++) {
		options2[i].style.display = "none";
	}
	/*divtitre = document.getElementById("resumeSujet");
	alert(divtitre+"//"+id);
	divtitre.innerHTML="${listeSujet[0]}";*/

	divDrop = document.getElementById("drophidden-" + id);
	divDrop.style.display = "block";


}

changeColor();

var sel1 = document.querySelector('#option');
var sel1Etat = document.querySelector('#selectEtat');
var sel2 = document.querySelector('#divSujet');
var options2 = sel2.querySelectorAll('*[id^="draggable-"]');
var sel3 = document.querySelector('#divEtudiant');
if (sel3 != null) {
	var options3 = sel3.querySelectorAll('*[id^="draggableEtudiant-"]');
}

/** Rend le changement d'option dynamique sur les sujets.
 *  Est appelée à chaque changement sur le select option */
/*function giveSelection(selValue) {
	sel2.innerHTML = '';
	var selValue = (selValue == undefined) ? document.getElementById("option").value : selValue;
	for (var i = 0; i < options2.length; i++) {
		if ((options2[i].dataset.option).indexOf(selValue) != -1) {
			sel2.appendChild(options2[i]);
		}
	}
}*/

function giveSelection() {
	var selectEtat= document.getElementById("etat");
	if (selectEtat !== null){
		var etat= selectEtat.value;
	}else{
		var etat = "";
	}
	var option = document.getElementById("option").value;
	sel2.innerHTML = '';
	for (var i = 0; i < options2.length; i++) {
		
		if (((option.length>0)?((options2[i].dataset.option).indexOf(option) != -1):true)&&((etat.length>0)?((options2[i].dataset.option).indexOf(etat) != -1):true)){		
			sel2.appendChild(options2[i]);
		}
	}
	if (sel3 != null) {
		sel3.innerHTML = '';
		for (var i = 0; i < options3.length; i++) {
			//alert(options3[i].dataset.option);
			if ((option.length>0)?((options3[i].dataset.option).indexOf(option) != -1):true){
				sel3.appendChild(options3[i]);
			}
		}
	}
}
