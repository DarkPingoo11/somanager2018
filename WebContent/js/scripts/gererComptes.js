/**
 * Created by Tristan LE GACQUE on 08/01/2018.
 */

const _action_ouvrir_modal_supprimer    =   'ouvrirModalSupprimerCompte',
    _action_ouvrir_modal_changerPw      =   'ouvrirModalChangerPw',
    _action_bouton_suspendre_compte     =   'suspendreCompte',
    _action_bouton_valider_compte       =   'validerCompte',
    _action_bouton_supprimer_compte     =   'supprimerCompteUtilisateur',
    _action_bouton_changerPw            =   'changerPwUtilisateur',
    _action_bouton_genererPw            =   'generatePassword',
    _lien_requete_ajax_supprimerCompte  =   '/GererComptes',
    _lien_requete_ajax_suspendreCompte  =   '/GererComptes',
    _lien_requete_ajax_validerCompte    =   '/GererComptes',
    _lien_requete_ajax_changerPw        =   '/ChangerMotDePasseAdmin',
    _lien_requete_ajax_infosUtilisateur =   '/InfosUtilisateur';

const _identifier_modal_supprimer       =   'supprimerModalId',
    _identifier_modal_changerPw         =   'changerPwModalId',
    _identifier_replace_nomUtilisateur  =   'nomUtilisateur',
    _identifier_form_changerPw          =   'ChangerMotDePasseAdmin';

const _identifier_replace_generatedPw   =   'generatedPassword';


//////////////////////////
//--- Fonction internes
//////////////////////////

function remplacerNomDansModal(nomUtilisateur, modalId) {
    modalId.find("*[data-replace='"+_identifier_replace_nomUtilisateur+"']")
        .html(nomUtilisateur);
}

//.Suppression de compte
/**
 * Ouvre le modal de suppression utilisateur
 */
function ouvrirModalSupprimer() {
    var idUtilisateur = $(this).data('value');

    //On défini temporairement l'id de l'utilisateur dans le modal
    var modalId         = $("#" + _identifier_modal_supprimer);
    var nomUtilisateur  = $(this).data('nom');
    remplacerNomDansModal(nomUtilisateur, modalId);

    //On ouvre le modal et quand il est chargé on enregistre l'action du bouton
    modalId.modal('show');
    modalId.one('shown.bs.modal', function() {
        //Retrait des actions associées précédemment
        unbindButtonAction(_action_bouton_supprimer_compte);
        onButtonAction(_action_bouton_supprimer_compte, function() {
            supprimerCompteUtilisateur(idUtilisateur);
        });
    });
}

/**
 * Ouvre le modal de suppression utilisateur
 */
function ouvrirModalSupprimerGroupe(listeUtilisateurs) {
    //On défini temporairement l'id de l'utilisateur dans le modal
    var nomsConcat = "";
    $.each(listeUtilisateurs, function(id, utilisateur) {
        nomsConcat += "<br />" + "- " + utilisateur.nom;
    });

    var modalId         = $("#" + _identifier_modal_supprimer);
    remplacerNomDansModal(nomsConcat, modalId);

    //On ouvre le modal et quand il est chargé on enregistre l'action du bouton
    modalId.modal('show');
    modalId.one('shown.bs.modal', function() {
        //On retire les actions associées précédemment
        unbindButtonAction(_action_bouton_supprimer_compte);
        onButtonAction(_action_bouton_supprimer_compte, function() {
            $.each(listeUtilisateurs, function(id, utilisateur) {
                supprimerCompteUtilisateur(utilisateur.idUtilisateur);
            });
        });
    });
}

/**
 * Envoies une requête de suppression du compte utilisateur
 * @param idUtilisateur identifiant unique (id) de l'utilisateur à supprimer
 */
function supprimerCompteUtilisateur(idUtilisateur) {
    //On envoie le formulaire
    var params = "formulaire=suppressionCompte&" + "idUtilisateur=" + idUtilisateur;
    submitAjaxRequest(_lien_requete_ajax_supprimerCompte, params, function(resp) {
        if(resp.type === 'success') {
            toastr.success(resp.message);
            id_tableResult.attr(_attr_stick_tableResult, 'true');
            searchUtilisateurs();
        } else {
            toastr.error(resp.message);
        }
    });
}

/**
 * Envoies une requête de suspension de compte utilisateur
 * @param idUtilisateur identifiant unique (id) de l'utilisateur à suspendre
 */
function suspendreCompteUtilisateur(idUtilisateur) {
    //On envoie le formulaire
    var params = "formulaire=validerCompte&" + "idUtilisateur=" + idUtilisateur + "&valider=non";
    submitAjaxRequest(_lien_requete_ajax_suspendreCompte, params, function(resp) {
        if(resp.type === 'success') {
            toastr.success(resp.message);
            id_tableResult.attr(_attr_stick_tableResult, 'true');
            searchUtilisateurs();
        } else {
            toastr.error(resp.message);
        }
    });
}

/**
 * Envoies une requête de validation de compte utilisateur
 * @param idUtilisateur identifiant unique (id) de l'utilisateur à valider
 */
function validerCompteUtilisateur(idUtilisateur) {
    //On envoie le formulaire
    var params = "formulaire=validerCompte&" + "idUtilisateur=" + idUtilisateur + "&valider=oui";
    submitAjaxRequest(_lien_requete_ajax_validerCompte, params, function(resp) {
        if(resp.type === 'success') {
            toastr.success(resp.message);
            id_tableResult.attr(_attr_stick_tableResult, 'true');
            searchUtilisateurs();
        } else {
            toastr.error(resp.message);
        }
    });
}

//.Changement de mot de passe
/**
 * Ouvre le modal de changement de mot de passe
 */
function ouvrirModalModifierPw() {
    var idUtilisateur = $(this).data('value');

    //On défini temporairement l'id de l'utilisateur dans le modal
    var modalId         = $("#" + _identifier_modal_changerPw);
    var nomUtilisateur  = $(this).data('nom');
    remplacerNomDansModal(nomUtilisateur, modalId);

    //Charger l'identifiant de l'utilisateur
    submitAjaxRequest(_lien_requete_ajax_infosUtilisateur, 'idUtilisateur='+idUtilisateur, function(resp){
        if(resp.type === 'success') {
            //On efface les valeurs enregistrées précédemments
            modalId.find("input").val("");

            modalId.find("input[name='identifiant']").val(resp.utilisateur.identifiant);

            //On ouvre le modal et quand il est chargé on enregistre l'action du bouton
            modalId.modal('show');
            modalId.one('shown.bs.modal', function() {

                //On retire les actions associées précédemment
                unbindButtonAction(_action_bouton_changerPw);
                onButtonAction(_action_bouton_changerPw, function() {
                    changerPwUtilisateur(modalId, idUtilisateur);
                });
            });
        } else {
            toastr.error("Une erreur s'est produite : " + resp.message);
        }
    });

}

/**
 * Envoies une requête de changement de mot de passe utilisateur
 * @param idUtilisateur identifiant unique (id) de l'utilisateur
 * @param modalId modal correspondant
 */
function changerPwUtilisateur(modalId, idUtilisateur) {
    //On envoie le formulaire
    var params = $("form[id='"+_identifier_form_changerPw+"']").serialize();
    submitAjaxRequest(_lien_requete_ajax_changerPw, params, function(resp) {
        if(resp.type === 'success') {
            toastr.info(resp.message);
            modalId.modal('hide');
            id_tableResult.attr(_attr_stick_tableResult, 'true');
            searchUtilisateurs();
        } else {
            toastr.error(resp.message);
        }
    });
}

/**
 * Génère un mot de passe sécurisé automatiquement de taille 8
 */
function getGeneratedPassword() {
    var chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOP1234567890";
    var pass = "";
    const regex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$/;

    while(!regex.test(pass)) {
        pass = "";
        for (var x = 0; x < 8; x++) {
            var i = Math.floor(Math.random() * chars.length);
            pass += chars.charAt(i);
        }
    }
    return pass;
}


/////////////////////////////////////
//--- Définition des fonctions publiques
/////////////////////////////////////

/**
 * Met a jour les actions des boutons
 */
function bindActionsBoutons() {
    //Unbind toutes les actions precedentes
    unbindButtonAction(_action_ouvrir_modal_supprimer);
    unbindButtonAction(_action_ouvrir_modal_changerPw);
    unbindButtonAction(_action_bouton_suspendre_compte);
    unbindButtonAction(_action_bouton_valider_compte);

    //Bind les actions
    onButtonAction(_action_ouvrir_modal_supprimer, ouvrirModalSupprimer);
    onButtonAction(_action_ouvrir_modal_changerPw, ouvrirModalModifierPw);
    onButtonAction(_action_bouton_suspendre_compte, function(){
        suspendreCompteUtilisateur($(this).attr('data-value'));
    });
    onButtonAction(_action_bouton_valider_compte, function(){
        validerCompteUtilisateur($(this).attr('data-value'));
    });
}

/**
 * Génère un mot de passse aléatoire et l'ajoute au champ de formulaire
 */
function changeGeneratedPassword() {
    //Récupérer le formulaire parent
    var form = $(this).parents("form");
    console.log(form);
    //Changer les mot de passe
    var pass = getGeneratedPassword();
    form.find("input[type='password']").val(pass);
    form.find("input[data-replace='"+_identifier_replace_generatedPw+"']").val(pass);
}

$(function() {
    onButtonAction(_action_bouton_genererPw, changeGeneratedPassword);
});