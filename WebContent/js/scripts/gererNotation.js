const _idForm = "#actualiserPage";



//Quand la page est chargée
$(function() {

    var form = $(_idForm);

    function actualiserPage() {
        var datas = "";

        form.find("select").each(function() {
            datas += "&" + $(this).attr("name") + "=" + $(this).val();
        });
        datas = "?" + datas.substring(1) + "&page=pgl";
        var url = form.attr('action') + datas;
        window.location.replace(url);
    }

    /**
     * Actualise les champs de selection
     */
    function actualiserChamps() {
        var val     = $(this).val();
        var champ   = $(this).data('champ');

        form.find("option[data-" + champ + "]").hide();
        form.find("option[data-" + champ + "='" + val + "']").show();

        //On change la selection si le champ est caché
        var hidden = form.find("option:selected").filter(":hidden");
        hidden.each(function() {
            $(this).parents("select").first().find("option:visible").first().prop('selected', true);
        });
    }

    function changerNote() {
        var ligne = $(this).parents('tr').first();
        var note = ligne.find('input').val();

        //Check note
        if(0 <= note && note <= 20) {
            var idUser = ligne.data('iduser');
            var idMat = ligne.data('idmatiere');

            var datas    = "idUtilisateur=" + idUser;
            datas       += "&idMatiere=" + idMat;
            datas       += "&note=" + note;
            datas       += "&type=noterPGL";

            submitAjaxRequest("/Noter", datas, function(resp) {
                if(resp.type === "success") {
                    toastr.success(resp.message);
                } else {
                    toastr.error(resp.message);
                }
            });
        }
    }

    function publierNote() {
        var datas = "idSprint=" + $("#selectionnerSprint").val();
        submitAjaxRequest("/Noter", datas + "&type=publierNotes", function(resp) {
            if(resp.type === "success") {
                toastr.success(resp.message);
            } else {
                toastr.error(resp.message);
            }
        });
    }

    function ouvrirSelectionnerModal() {
        $("#confirmerPublier").modal('show');
        replaceText("sprint", $("#selectionnerSprint").find(":selected").data('numero'));
    }

    var champs = form.find("select[data-action='actualiserChamps']");

    //Si un element change
    champs.on('change', actualiserChamps);
    form.find("button").on('click', actualiserPage);

    //Action de bouton
    onButtonAction("envoyerNote", changerNote);
    onButtonAction("publierNotes", publierNote);
    onButtonAction("openSelectionnerSprintModal", ouvrirSelectionnerModal);

    //Trigger
    champs.trigger('change');


    //Mettre la scrollbar
    new PerfectScrollbar('#listeNoteContainer');

});