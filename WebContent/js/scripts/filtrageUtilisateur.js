/**
 * Created by Tristan LE GACQUE on 08/01/2018.
 */

const _action_rechercher_utilisateur    =   'rechercherUtilisateur',
    _action_incrementer_page            =   'incrementerPage',
    _action_decrementer_page            =   'decrementerPage',
    _action_goto_premiere_page          =   'premierePage',
    _action_goto_derniere_page          =   'dernierePage',
    _action_changer_nbr_requetes        =   'changerNbRequetes',
    _action_groupe_supprimerComptes     =   'supprimerTousComptes',
    _action_groupe_suspendreComptes     =   'suspendreTousComptes',
    _action_groupe_validerComptes       =   'validerTousComptes',
    _lien_requete_ajax                  =   '/ListeUtilisateurs';

const _identifier_utilisateurs_tableau  =   'listeUtilisateurs',
    _identifier_tableau_container       =   'listeUtilisateursContainer',
    _identifier_input_nomUtilisateur    =   'nomUtilisateur',
    _identifier_select_anneeUtilisateur =   'anneeUtilisateur',
    _identifier_select_compteValide     =   'compteValide',
    _identifier_select_roleUtilisateur  =   'roleUtilisateur',
    _identifier_select_resultatsPage    =   'nombre',
    _identifier_text_resultatsTot       =   'resultatsTot',
    _identifier_text_pageCourante       =   'pageCourante',
    _identifier_checkbox_selectAll      =   'selectionnerTout';

const _name_checkbox_selectComptes      =   'selectionnerComptes',
    _attr_stick_tableResult             =   'data-stick';

var _ID_UTILISATEUR_COURANT = -1;


//Initialiser la perfect scrollbar
const ps = new PerfectScrollbar('#' + _identifier_tableau_container);

var id_nom          = $('#'+_identifier_input_nomUtilisateur);
var id_role         = $('#'+_identifier_select_roleUtilisateur);
var id_annee        = $('#'+_identifier_select_anneeUtilisateur);
var id_valide        = $('#'+_identifier_select_compteValide);
var id_resultats    = $('#'+_identifier_select_resultatsPage);
var id_resultatsTot = $('#'+_identifier_text_resultatsTot);
var id_pageCourante = $('#'+_identifier_text_pageCourante);
var id_table        = $('#'+_identifier_utilisateurs_tableau);
var id_tableResult  = $('#'+_identifier_tableau_container);

/**
 * Lance une requete AJAX et affiche les utilisateurs correspondants aux filtres
 */
function searchUtilisateurs() {
    //Ajout de tous les elements requis
    var attributs   = [];
    attributs.push(id_nom);
    attributs.push(id_role);
    attributs.push(id_resultats);
    attributs.push(id_annee);
    attributs.push(id_valide);

    //Recherche de la colonne à trier
    //var colTri      = $('th[data-tri]').first();
    //var colTriNom   = colTri.attr('data-col');
    //var colTriOrd   = colTri.attr('data-tri');

    //Mise en forme de la requete
    var params = '';
    attributs.forEach(function(element) {
        params += (params === '' ? '' : '&') + getId(element) + "=" + getVal(element);
    });
    params += "&" + _identifier_text_pageCourante + "=" + id_pageCourante.text();

    submitAjaxRequest(_lien_requete_ajax, params, afficherResultats)
}

//Passer a la page suivante
function incrementerPage() {
    var page = parseInt(id_pageCourante.text());

    //Calcul de la page max
    var resultatsTot    = parseInt(id_resultatsTot.text().split(" ")[0]);
    var resultats       = parseInt(id_resultats.val());

    //Nombre pages = rmax / rpage
    var pages = Math.round(resultatsTot / resultats);

    if(page + 1 <= pages) {
        id_pageCourante.text(page + 1);
        searchUtilisateurs();
    }
}

//Passer a la page précédente
function decrementerPage() {
    var page = parseInt(id_pageCourante.text());

    if(page > 1) {
        id_pageCourante.text(page -1);
        searchUtilisateurs();
    }
}

/**
 * Retourne à la première page
 */
function goToFirstPage() {
    id_pageCourante.text(1);
    searchUtilisateurs();
}

/**
 * Avance jusqu'à la dernière page
 */
function goToLastPage() {
    //Calcul de la page max
    var resultatsTot    = parseInt(id_resultatsTot.text().split(" ")[0]);
    var resultats       = parseInt(id_resultats.val());

    //Nombre pages = rmax / rpage
    id_pageCourante.text(Math.round(resultatsTot / resultats));
    searchUtilisateurs();
}


/**
 * Fonction de callback de l'ajax, qui s'occupe d'afficher les résultats
 * @param resp objet reponse
 */
function afficherResultats(resp) {
    if(resp.type === 'success') {
        //Effacer le tableau actuel
        id_table.html('');

        //Afficher les lignes pour chaque utilisateur
        if(resp.utilisateurs.length > 0) {
            $.each(resp.utilisateurs, function(id, utilisateur) {
                var iconesRoles = "";

                $.each(utilisateur.roles, function(ido, role) {
                    const nomU = role.nom.replace("'", "\'");
                    iconesRoles += '<i class="fas fa-'+role.icone+' fa-fw" aria-hidden="true" title="'+ nomU +'"></i>&nbsp;';
                });

                var checkbox = '<input type="checkbox" name="'+_name_checkbox_selectComptes+'" ' +
                    'value="'+utilisateur.idUtilisateur+'" ' +
                    'data-name="'+utilisateur.nom.toUpperCase()+' '+ utilisateur.prenom+ '">';

                id_table.append(
                    '<tr>' +
                    '   <td class="">'+ (_ID_UTILISATEUR_COURANT === utilisateur.idUtilisateur ? '' : checkbox) +'</td>' +
                    '   <td class="col-sm-2 text-uppercase">'+utilisateur.nom+'</td>' +
                    '   <td class="col-sm-2">'+utilisateur.prenom+'</td>' +
                    '   <td class="col-sm-2">'+utilisateur.identifiant+'</td>' +
                    '   <td class="col-sm-2">'+iconesRoles+'</td>' +
                    '   <td class="col-sm-2">'+utilisateur.annee+'</td>' +
                    '   <td class="col-sm-2">'+ recupererActions(utilisateur) +'</td>' +
                    '</tr>'
                );
            });
            //rafraichir la scrollbar et scrollTop
            ps.update();
            if(id_tableResult.attr(_attr_stick_tableResult) === undefined ||
                id_tableResult.attr(_attr_stick_tableResult) !== 'true') {
                id_tableResult.scrollTop(0);
            } else {
                id_tableResult.removeAttr(_attr_stick_tableResult);
            }

        } else {
            id_table.html('<tr><td colspan="4" align="center">Aucun résultat ¯\\_(ツ)_/¯</td></tr>');
        }

        //Mettre a jour le nombre de résultats ainsi que la page actuelle
        id_resultatsTot.html(resp.resultats + " résultat" + (resp.resultats > 1 ? "s" : ""));
        id_pageCourante.html(resp.page);

        //On rebind les actions
        bindActionsBoutons();

    } else {
        id_table.html('<tr><td colspan="4" align="center">Une erreur est survenue !</td></tr>');
        console.log("Une erreur est survenue");
    }
}

/**
 * Récupère les actions possibles pour un utilisateur
 * @param utilisateur
 */
function recupererActions(utilisateur) {
    var actionSupprimer =
        "<button type='button' class='btn btn-danger btn-sm' data-action='"+_action_ouvrir_modal_supprimer+"' " +
        "data-value='" + utilisateur.idUtilisateur + "' " +
        "data-nom='" + utilisateur.nom.toUpperCase() + " " + utilisateur.prenom + "' " +
        "title='Supprimer le compte'> " +
        "<i class='far fa-trash-alt fa-fw' aria-hidden='true'></i>" +
        "</button>&nbsp;";

    var actionValider =
        "<button type='button' class='btn btn-success btn-sm' data-action='"+_action_bouton_valider_compte+"' " +
        "data-value='" + utilisateur.idUtilisateur + "' " +
        "data-nom='" + utilisateur.nom.toUpperCase() + " " + utilisateur.prenom + "' " +
        "title='Activer/Valider le compte'> " +
        "<i class='far fa-check-circle fa-fw' aria-hidden='true'></i>" +
        "</button>&nbsp;";

    var actionSuspendre =
        "<button type='button' class='btn btn-warning btn-sm' data-action='"+_action_bouton_suspendre_compte+"' " +
        "data-value='" + utilisateur.idUtilisateur + "' " +
        "data-nom='" + utilisateur.nom.toUpperCase() + " " + utilisateur.prenom + "' " +
        "title='Suspendre le compte'> " +
        "<i class='fas fa-ban fa-fw' aria-hidden='true'></i>" +
        "</button>&nbsp;";

    var actionModifierPw =
        "<button type='button' class='btn btn-primary btn-sm' data-action='"+_action_ouvrir_modal_changerPw+"' " +
        "data-value='" + utilisateur.idUtilisateur + "' " +
        "data-nom='" + utilisateur.nom.toUpperCase() + " " + utilisateur.prenom + "' ? " +
        "title='Modifier le mot de passe'> " +
        "<i class='fas fa-key fa-fw' aria-hidden='true'></i>" +
        "</button>&nbsp;";

    //Preparation de toutes les actions
    var actionsHtml = "";
    actionsHtml += actionModifierPw.replace('?', utilisateur.valide === 'oui' ? '' : 'disabled=""');
    actionsHtml += utilisateur.valide === 'oui' ? (actionSuspendre) : (actionValider);
    actionsHtml += actionSupprimer;

    return utilisateur.idUtilisateur === _ID_UTILISATEUR_COURANT ? '' : actionsHtml;
}


/**
 * Initialise la fonction de tri
 */
function initTri() {
    $('th[data-col]').each(function() {
        var defTri  =   '<i class="fa fa-caret-up text-gray" aria-hidden="true"></i>' +
            '<i class="fa fa-caret-down text-gray" aria-hidden="true"></i>';

        //Ajout du visuel par défaut
        $(this).append('&nbsp; <span>' + defTri + '</span>');

        $(this).click(function() {
            var tri = $(this).attr('data-tri');

            //Reinitialisation de toutes les autres colonnes
            $('th[data-col]').each(function() {
                $(this).attr('data-tri', null);
                $(this).find('span').html(defTri);
            });

            //On tri
            if(tri === undefined || tri === null) {
                $(this).attr('data-tri', 'ASC');
                $(this).find('span').html('' +
                    '<i class="fa fa-caret-up" aria-hidden="true"></i>' +
                    '<i class="fa fa-caret-down text-gray" aria-hidden="true"></i>');
            } else if(tri === 'ASC') {
                $(this).attr('data-tri', 'DESC');
                $(this).find('span').html('' +
                    '<i class="fa fa-caret-up text-gray" aria-hidden="true"></i>' +
                    '<i class="fa fa-caret-down" aria-hidden="true"></i>');
            } else {
                $(this).attr('data-tri', null);
                $(this).find('span').html(defTri);
            }
            searchSalle();
        });
    })
}

/**
 * Récupère la liste des utilisateurs{nom, idUtilisateur} selectionnés
 * @returns {Array}
 */
function recupererComptesSelectionnes() {
    var listeUtilisateurs = [];

    $("input[type='checkbox'][name='" + _name_checkbox_selectComptes +"']:checked").each(function() {
        var utilisateur = {};
        utilisateur.nom = $(this).attr('data-name');
        utilisateur.idUtilisateur = $(this).val();
        if(!(utilisateur.nom === undefined || utilisateur.idUtilisateur === undefined
            || utilisateur.idUtilisateur === _ID_UTILISATEUR_COURANT)) {
            listeUtilisateurs.push(utilisateur);
        }
    });

    return listeUtilisateurs;
}

$(function() {
    onButtonAction(_action_rechercher_utilisateur, searchUtilisateurs);
    onButtonAction(_action_incrementer_page, incrementerPage);
    onButtonAction(_action_decrementer_page, decrementerPage);
    onButtonAction(_action_goto_premiere_page, goToFirstPage);
    onButtonAction(_action_goto_derniere_page, goToLastPage);
    onButtonAction(_action_groupe_supprimerComptes, function() {
        if(recupererComptesSelectionnes().length > 0) {
            ouvrirModalSupprimerGroupe(recupererComptesSelectionnes());
        } else {
            toastr.info("Il faut tout d'abord sélectionner des comptes");
        }
    });
    onButtonAction(_action_groupe_validerComptes, function() {
        if(recupererComptesSelectionnes().length > 0) {
            $.each(recupererComptesSelectionnes(), function(id, utilisateur) {
                validerCompteUtilisateur(utilisateur.idUtilisateur);
            });
        } else {
            toastr.info("Il faut tout d'abord sélectionner des comptes");
        }
    });
    onButtonAction(_action_groupe_suspendreComptes, function() {
        if(recupererComptesSelectionnes().length > 0) {
            $.each(recupererComptesSelectionnes(), function(id, utilisateur) {
                suspendreCompteUtilisateur(utilisateur.idUtilisateur);
            });
        } else {
            toastr.info("Il faut tout d'abord sélectionner des comptes");
        }
    });



    onSelectChange(_action_rechercher_utilisateur, searchUtilisateurs);
    onSelectChange(_action_changer_nbr_requetes, goToFirstPage);

    onInputChange(_action_rechercher_utilisateur, searchUtilisateurs);

    //Affecter l'action à la checkbox : Selectionner tout / Deselectionner tout
    $("#" + _identifier_checkbox_selectAll).on('change', function() {
        var name = getName($(this));

        //Check toutes les autres box si check, uncheck sinon
        $("input[type='checkbox'][name='" + name +"']").prop('checked',$(this).is(":checked"));
    });

    //Récupérer l'ID et le stocker en global
    submitAjaxRequest(_lien_requete_ajax_infosUtilisateur, 'idUtilisateur=-1', function(resp) {
        if(resp.type === 'success') {
            _ID_UTILISATEUR_COURANT = resp.utilisateur.idUtilisateur;
            //Recherche par défaut
            searchUtilisateurs();
            //Initialiser tri
            //initTri();
            //TODO - Si le temps, ajouter le tri ordonné (ORDER BY ... [ASC|DESC])
        }
    });
});