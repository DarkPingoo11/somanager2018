/**
 * Created by Tristan LE GACQUE on 31/05/2018.
 */

const _action_testerConnexiondb     =   'testerConnexionDb',
    _action_testerAccesRoot         =   'testerAccesRoot',
    _action_createBDD               =   'createBDD',
    _lien_requete_ajax_infosUtilisateur =   '/InfosUtilisateur';

const _identifier_input_bdd             =   '#bdd',
    _identifier_input_hote              =   '#hote',
    _identifier_input_driver            =   '#driver',
    _identifier_input_username          =   '#username',
    _identifier_input_password          =   '#password',
    _identifier_input_usernameroot      =   '#rootusername',
    _identifier_input_passwordroot      =   '#rootpassword',
    _identifier_form_bdd                =   '#validerBDD',
    _identifier_button_scriptbddnext    =   '#scriptBDDnext',
    _identifier_button_scriptbdd        =   '#createBDD',
    _identifier_scriptdebut             =   '#bddScriptDebut',
    _identifier_scriptfin               =   '#bddScriptFin',
    _identifier_bddLoading              =   '#bddLoading',
    _identifier_bddLoadingEchec         =   '#bddLoadingEchec',
    _identifier_bddLoadingSucces        =   '#bddLoadingSucces';




const replace_callbacktestdb = 'callbacktestdb',
    replace_callbacktestacces = 'callbacktestacces';


//////////////////////////
//--- Fonction internes
//////////////////////////
var submitBDD = $(_identifier_form_bdd).find('button[type="submit"]');
var submitScriptBDD = $(_identifier_button_scriptbddnext);
var buttonDemarrerScriptBDD = $(_identifier_button_scriptbdd);

function testerConnexionBDD() {
    var hote = $(_identifier_input_hote).val();
    var bdd = $(_identifier_input_bdd).val();
    var driver = $(_identifier_input_driver).val();
    var username = $(_identifier_input_username).val();
    var password = $(_identifier_input_password).val();

    var replace = $('span[data-replace="'+replace_callbacktestdb+'"]');
    replace.css('color', 'black');
    replace.text("Test en cours...");

    var data = "formulaire=testerBdd&bdd="+bdd+"&hote="+hote+"&driver="+driver+"&username="+username+"&password="+password;
    submitAjaxRequest("/Installation", data, function(resp) {
        if(resp.type === 'success') {
            replace.css('color', 'green');
            replace.text(resp.message);
            //Débloquer le bouton submit
            submitBDD.prop('disabled', false);
        } else {
            replace.css('color', 'red');
            replace.text(resp.message);
        }
    });
}

function testerDroitsBDD() {
    var username = $(_identifier_input_usernameroot).val();
    var password = $(_identifier_input_passwordroot).val();
    var replace = $('span[data-replace="'+replace_callbacktestacces+'"]');

    var data = "formulaire=testerAccess&username="+username+"&password="+password;
    replace.css('color', 'black');
    replace.text("Test en cours...");
    submitAjaxRequest("/Installation", data, function(resp) {
        if(resp.type === 'success') {
            replace.css('color', 'green');
            replace.text(resp.message);
            buttonDemarrerScriptBDD.prop('disabled', false);
        } else {
            replace.css('color', 'red');
            replace.text(resp.message);
        }
    });
}

function creerBDD() {
    var username = $(_identifier_input_usernameroot).val();
    var password = $(_identifier_input_passwordroot).val();

    var data = "formulaire=creerBDD&username="+username+"&password="+password;
    $(_identifier_scriptdebut).hide();
    $(_identifier_scriptfin).show();

    //Ouvrir un modal pendant la création
    submitAjaxRequestFull("/Installation", data, function(resp) {
        $(_identifier_bddLoading).remove();
        if(resp.type === 'success') {
            $(_identifier_bddLoadingSucces).show();
            submitScriptBDD.prop('disabled', false);
        } else {
            $(_identifier_bddLoadingEchec).show();
            setTimeout(function() {submitScriptBDD.prop('disabled', false)}, 10000);
        }
    }, function() {
        $(_identifier_bddLoadingEchec).show();
        setTimeout(function() {submitScriptBDD.prop('disabled', false)}, 10000);
    }, 120000);
}

$(function() {
    onButtonAction(_action_createBDD, creerBDD);
    onButtonAction(_action_testerAccesRoot, testerDroitsBDD);
    onButtonAction(_action_testerConnexiondb, testerConnexionBDD);
    $(_identifier_form_bdd).on('change, keypress', function() {
        submitBDD.prop('disabled', true);
    });
    $(_identifier_form_bdd).on('change, keypress', function() {
        buttonDemarrerScriptBDD.prop('disabled', true);
    });

    //On test avec les identifiants par défaut
    testerDroitsBDD();

});