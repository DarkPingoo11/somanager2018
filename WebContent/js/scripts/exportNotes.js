/**
 * Created by Tristan LE GACQUE on 08/01/2018.
 */

const _identifier_modele_container      =   'excel-file',
    _identifier_assistant               =   'assistantCreation',
    _identifier_select_modele           =   'choixModele',
    _identifier_modal_supprimermodele   =   'supprimerModeleModal';

const _data_action_startAssistant   = 'startAssistant',
    _data_action_supprimerModele    = 'supprimerModele';

const _data_replace_ligneselectionnee   = "ligneSelectionnee",
    _data_replace_colonneselectionnee   = "colonneSelectionnee";

const _color_select                     = "rgba(0, 255, 0, 0.5)";

var id_modele_excel = $('#'+_identifier_modele_container);
var id_assistant    = $('#'+_identifier_assistant);
var id_selectmodele = $('#'+_identifier_select_modele);


/**
 * Affiche la ligne et la colonne cliquée
 */
function afficherLigneEtColonneSelectionnee() {
    $("[data-replace='"+_data_replace_ligneselectionnee+"']").text(getLigne($(this)));
    $("[data-replace='"+_data_replace_colonneselectionnee+"']").text(getColonne($(this)));
}

function getColonne(tdElement) {
    return tdElement.parent().children().index(tdElement);
}

function getLigne(tdElement) {
    return tdElement.parent().parent().children().index(tdElement.parent());
}


/**
 * Colore les lignes et colonnes du modèle en fonction des paramètres
 */
function colorerLignesEtColonnesParam() {
    id_modele_excel.find('.paramSelected').removeClass('paramSelected');
    var ligne_header_index  = id_assistant.find('input[data-info="ligne"]').first().val();

    if(ligne_header_index !== null) {
        var ligne_header        = id_modele_excel.find('tr').get(ligne_header_index);
        id_assistant.find('input').each(function(index) {
            //On colore chaque ligne / colonne
            if($(this).attr("data-info") !== "ligne" && ligne_header !== null) {
                var val = $(this).val();
                if(val !== null) {
                    $($(ligne_header).find('td').get(val)).addClass("paramSelected");
                }
            }
        });

        $(ligne_header).addClass("paramSelected");
    }
}

/**
 * Ouvre le modal de suppression du modele
 */
function ouvrirModalSupprimerModele() {
    var modalId = $("#" + _identifier_modal_supprimermodele);
    modalId.modal('show');
}

$(function() {

    //Module ASSISTANT CONFIGURATION MODELE
    var assistant = (function() {
        var params = id_assistant.find(".parametre");
        var started = false;
        /**
         * Démarre l'assistant de paramétrage
         */
        var startAssistant = function() {
            started = true;
            //On reinitialise les valeurs de tous les parametres de l'assistant
            params.val(0);

            //On reinitialise leur couleur
            params.parent().css("background-color", "inherit");

            //On les rends Indéfini
            params.addClass("indefini");

            //On verrouille les boutons de l'assistant et on retire la couleur
            id_assistant.find("button").prop("disabled", true);
            colorerLignesEtColonnesParam();

            //On bind les actions
            id_modele_excel.find("td").on("click", affectation);
            //Et on démarre l'assistant de configuration
            toastr.options.timeOut = 0;
            toastr.warning("Démarrage de l'assistant de configuration du modèle de note");
            nextElement();
        };

        var isStarted = function() {
            return started;
        };

        var affectation = function() {
            //Récupérer l'indéfini actuel
            var indefini = getIndefini();

            var elem = $(this);
            //On affecte la ligne / colonne en fonction de ce qu'il souhaite
            if(indefini.attr("data-info") === "ligne") {
                affecterValeur(getLigne(elem));
            } else {
                affecterValeur(getColonne(elem));
            }
        };

        /**
         * Affecte la valeur à l'element en cours d'affectation
         * @param valeur
         */
        var affecterValeur = function(valeur) {
            //On récupère l'élément indéfini
            var indefini = getIndefini();
            indefini.parent().css("background-color", "inherit");
            indefini.val(parseInt(valeur));
            indefini.removeClass("indefini");
            //Retrait des anciennes informations toastr
            toastr.remove();
            nextElement();
        };

        /**
         * Récupère le premier element indéfini
         * @returns {*}
         */
        var getIndefini = function() {
            var indefinis = params.filter(".indefini");
            return indefinis.length === 0 ? undefined : indefinis.first();
        };

        /**
         * Met en couleur le prochain element
         */
        var nextElement = function() {
            //On prends le premier non défini et on le met en couleur
            var indefini = getIndefini();
            if(indefini === undefined) {
                id_modele_excel.find("td").off('click', affectation);
                toastr.clear();
                toastr.success("La configuration du modèle est terminée");
                started = false;
                //On déverrouille les boutons de l'assistant et on colorie
                id_assistant.find("button").prop("disabled", false);
                colorerLignesEtColonnesParam();
                var formid = id_assistant.parent("form");
                if(formid.length === 1) {
                    formid.trigger('submit');
                }
            } else {
                indefini.parent().css("background-color", _color_select);
                var info = indefini.parent().attr("data-text");
                toastr.info("Cliquez sur " + info);
            }
        };

        //API
        return {
            startAssistant : startAssistant,
            isStarted : isStarted
        };
    })();


    //Quand on clique sur une ligne du modèle
    id_modele_excel.find("td").on("click", afficherLigneEtColonneSelectionnee);

    //on demarre l'assistant
    onButtonAction(_data_action_startAssistant, function() {
        if(!assistant.isStarted()) {
            assistant.startAssistant();
            colorerLignesEtColonnesParam();
        }
    });

    //On cache l'assistant et le modèle si aucun modèle n'est selectionné
    if(id_selectmodele.val() === null) {
        id_assistant.parents('.panel').first().hide();
        id_modele_excel.parents('.panel').first().hide();
    } else {
        colorerLignesEtColonnesParam();
        onButtonAction(_data_action_supprimerModele, ouvrirModalSupprimerModele);
    }

    //Définir les toastr vers le bas
    toastr.options.newestOnTop = false;
});