/**
 * Fonctions pour afficher la sidebar
 **/

const _menu_defaut = "/Dashboard";

/**
 * Affiche le menu actif de la sidebar
 */
function afficherMenuActif() {
    var menuLien = window.location.pathname.substr(window.location.pathname.lastIndexOf("/"));
    if(menuLien.indexOf("?") > 0) {
        menuLien = menuLien(0, menuLien.lastIndexOf("/"));
    }

    //Parcourir tous les éléments de la sidebar
    var sidebar = $("nav#sidebar");

    //Trouver le menu avec le lien actif
    var menu = sidebar.find("a[href='" + menuLien + "']");

    if(menu.length === 0) {
        menu = sidebar.find("a[href='" + _menu_defaut + "']");
    }

    //Activer le <li>
    menu.parents("li").addClass("selected");

    //Si c'est un sous-menu, on active le parent
    var parent = menu.parents("ul").first();
    var buttonId = parent.attr("id");

    var button = sidebar.find("a[href='#" + buttonId + "']");
    button.trigger('click');

}


//Démarrer le script
//Ne pas attendre que la page soit chargée

//Collapse la sidebar
$('#sidebarCollapse').on('click', function () {
    $('#sidebar').toggleClass('active');
});

//Ouvrir/Fermer le menu et fermer les autres
var menus = $("#sidebar").find("a[href^='#']");

menus.on('click', function() {
    var element = menus.not($(this)).filter("[aria-expanded='true']");
    element.addClass("collapsed");
    element.parent('li').find('ul').collapse('hide');
});

//Afficher le menu
afficherMenuActif();

$("#inscription").on('click', function() {
    redirect(_CONTEXTPATH + $(this).data('target'));
});