/**
 * Created by Tristan LE GACQUE on 14/01/2018.
 */

//URL contexte
const _CONTEXTPATH  = window.location.pathname.substr(0,
    window.location.pathname.lastIndexOf("/")
);

/**
 * Appelle la fonction callback quand un element button avec un data-action est appuyé
 * @param data_action data-action à observer
 * @param callback fonction
 */
function onButtonAction(data_action, callback) {
    $('button[data-action="' + data_action + '"]').on('click', callback);
    $('a[data-action="' + data_action + '"]').on('click', callback);
}

function replaceText(data_replace, texte) {
    $('[data-replace="' + data_replace + '"]').text(texte);
}

/**
 * Retire les listeners de Click sur un bouton possédant le data-action spécifié
 * @param data_action data-action du bouton
 */
function unbindButtonAction(data_action) {
    $('button[data-action="' + data_action + '"]').off('click');
    $('a[data-action="' + data_action + '"]').off('click');
}

/**
 * Appelle la fonction callback quand un element select avec un data-action est changé
 * @param data_action data-action à observer
 * @param callback fonction
 */
function onSelectChange(data_action, callback) {
    $('select[data-action="' + data_action + '"]').on('change', callback);
}

/**
 * Appelle la fonction callback quand un element input avec un data-action est changé
 * @param data_action data-action à observer
 * @param callback fonction
 */
function onInputChange(data_action, callback) {
    $('input[data-action="' + data_action + '"]').on('input', callback);
}

/**
 * Récupère la valeur d'un élément
 * @param element element jQuery
 * @returns {*|jQuery}
 */
function getVal(element) {
    return element.val();
}

/**
 * Récupère l'attribut 'name' d'un élément
 * @param element element jQuery
 * @returns {*|string}
 */
function getName(element) {
    return element.attr('name');
}

/**
 * Récupère l'attribut 'id' d'un élément
 * @param element element jQuery
 * @returns {*|string}
 */
function getId(element) {
    return element.attr('id');
}

/**
 * Redirige la page vers l'url
 * @param url
 */
function redirect(url) {
    window.location.replace(url);
}

//Fonctions maison
$(function(){
    $.fn.pressEnter = function(callback) {
        this.keyup(function(e) {
            if (e.keyCode === 13) {
                callback();
            }
        });
    }
});

/**
 * Created by Tristan LE GACQUE on 08/01/2018.
 */

/**
 * Execute une requete AJAX vers l'url fourni, et execute la fonction de callback en fonction du résultat
 * @param url string Url vers lequel envoyer la requête
 * @param datas string Données au format key1=value1&key2=value2&...
 * @param doneCallback Fonction de callback en cas de succès de la requete
 * La fonction de callback est toujours appelée avec la reponse ajax
 */
function submitAjaxRequest(url, datas, doneCallback) {
    $.ajax({
        url: _CONTEXTPATH + url,
        type: 'POST',
        dataType: 'json',
        data: datas,
        timeout: 5000,
        error: function() {
            console.log("Erreur de requete !");
            console.log("URL=> " + url);
            console.log("TYPE=> POST / json");
            console.log("data=> " + datas);
        }
    }).done(function (resp) {
        doneCallback(resp);
    });
}

/**
 * Execute une requete AJAX vers l'url fourni, et execute les fonctions de callback en fonction du résultat
 * @param url string Url vers lequel envoyer la requête
 * @param datas string Données au format key1=value1&key2=value2&...
 * @param doneCallback Fonction de callback en cas de succès de la requete
 * @param errorCallback Fonction de callback en cas d'erreur ou de timeout de la requete
 * @param timeout Temps de timeout
 * La fonction de callback est toujours appelée avec la reponse ajax
 */
function submitAjaxRequestFull(url, datas, doneCallback, errorCallback, timeout) {
    $.ajax({
        url: _CONTEXTPATH + url,
        type: 'POST',
        dataType: 'json',
        data: datas,
        timeout: timeout,
        error: errorCallback
    }).done(function (resp) {
        doneCallback(resp);
    });
}
