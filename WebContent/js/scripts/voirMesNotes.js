const _idForm = "#actualiserPage";



//Quand la page est chargée
$(function() {

    var form = $(_idForm);

    function actualiserPage() {
        var datas = "";

        form.find("select").each(function() {
            datas += "&" + $(this).attr("name") + "=" + $(this).val();
        });
        datas = "?" + datas.substring(1);
        var url = form.attr('action') + datas;
        window.location.replace(url);
    }

    //Si un element change
   
    form.find("select").on('change', actualiserPage);

    
    //Trigger
    champs.trigger('change');


    //Mettre la scrollbar
    new PerfectScrollbar('#listeNoteContainer');

});