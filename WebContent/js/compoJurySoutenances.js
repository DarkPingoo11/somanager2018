// Concernant les professeurs

var counter = 0;
var dragNumber = -1;

function ajouterProf(ev, divName) {

	ajouterMembre(divName);
	dropProf(ev, counter);
}

function ajouterMembre(divName) {
	if (counter === 0) {
		divDrop = document.getElementById("dynamicInput");
		divDrop.innerHTML = "Membre "
				+ (counter + 1)
				+ " <br /><div id='membre"
				+ (counter + 1)
				+ "' type='text'  class='form-control' ondrop='dropProf(event,"
				+ (counter + 1) + ")'ondragover='allowDropProf(event,"
				+ (counter + 1) + ")' ondragleave='refuseDropProf(event,"
				+ (counter + 1) + ")' draggable='false'> </br></div>";
	} else {
		var newdiv = document.createElement('div');
		newdiv.innerHTML = "Membre "
				+ (counter + 1)
				+ " <br/><div id='membre"
				+ (counter + 1)
				+ "' type='text'  class='form-control' ondrop='dropProf(event,"
				+ (counter + 1) + ")'ondragover='allowDropProf(event,"
				+ (counter + 1) + ")' ondragleave='refuseDropProf(event,"
				+ (counter + 1) + ")' draggable='false'> </br></div>";
		document.getElementById(divName).appendChild(newdiv);
	}
	counter++;
}

function supprimerMembre() {
	var list = document.getElementById("dynamicInput");
	if (counter != 1) {
		list.removeChild(list.childNodes[list.childNodes.length - 1]);
		document.getElementById("profDrag-" + dragNumber).style.display = "";
		counter--;
		dragNumber--;
	}

}

function dragProf(ev, id) {
	// numero de la ligne de tableau où est contenu l'étudiant
	dragNumber = id;
}
/**   */
function dropProf(ev, id) {
	// var id = (id == undefined) ? ev.dataTransfer.getData("id") : id;

	// zone dans laquelle on souhaite inclure l'étudiant
	// on la sélectionne par son id
	divDrop = document.getElementById("membre" + id);

	var text = divDrop.innerHTML;

	// On vérifie si un étudiant n'est pas déjà présent dans la case membre
	// si c'est le cas il sera remis dans le tableau
	if (text.length > 5) {
		// récuparation de l'id de la ligne du professeur à remettre dans le
		// tableau
		var id = text.substring(text.indexOf("*blou*") + 6, text
				.indexOf("*/blou*"));
		// ré-affichage de l'étudiant dans le tableau
		document.getElementById("profDrag-" + id).style.display = "";
	}

	// on cache l'étudiant du tableau qu'on vient de dropper dans la case membre
	document.getElementById("profDrag-" + dragNumber).style.display = "none";

	// on met les informations nécessaires dans la case membre
	divDrop.innerHTML = document.getElementById("profDrag-" + dragNumber).innerHTML
	+ " "
	+ "<input name='idProf" + id + "' id='idProf" + id
	+ "' type ='hidden' value='" 
	+ document.getElementById("idProff" + dragNumber).value 
	+ "'/>";
}

/** Change la couleur de la bordure lors du hover du div drop */
function allowDropProf(ev, id) {
	ev.preventDefault();
}

/** Change la couleur de la bordure lorsque l'on quitte le div drop */
function refuseDropProf(ev, id) {
	ev.preventDefault();
}

// Concernant les sujets

var counterSujet = 0;
var dragNumberSujet = -1;

function ajouterSujet(ev, divName) {

	ajouterCaseSujet(divName);
	dropSujet(ev, counterSujet);
}

function ajouterCaseSujet(divName) {
	if (counterSujet === 0) {
		divDrop = document.getElementById("dynamicInputSujet");
		divDrop.innerHTML = "Sujet "
				+ (counterSujet + 1)
				+ " <br /><div id='sujet"
				+ (counterSujet + 1)
				+ "' type='text'  style='overflow: auto;' class='form-control' ondrop='dropSujet(event,"
				+ (counterSujet + 1) + ")'ondragover='allowdropSujet(event,"
				+ (counterSujet + 1) + ")' ondragleave='refusedropSujet(event,"
				+ (counterSujet + 1) + ")' draggable='false'> </br></div>";
	} else {
		var newdiv = document.createElement('div');
		newdiv.innerHTML = "Sujet "
				+ (counterSujet + 1)
				+ " <br/><div id='sujet"
				+ (counterSujet + 1)
				+ "' type='text'  style='overflow: auto;' class='form-control' ondrop='dropSujet(event,"
				+ (counterSujet + 1) + ")'ondragover='allowdropSujet(event,"
				+ (counterSujet + 1) + ")' ondragleave='refusedropSujet(event,"
				+ (counterSujet + 1) + ")' draggable='false'> </br></div>";
		document.getElementById(divName).appendChild(newdiv);
	}
	counterSujet++;
}

function supprimerSujet() {
	var list = document.getElementById("dynamicInputSujet");
	if (counterSujet != 1 	) {
		list.removeChild(list.childNodes[list.childNodes.length - 1]);
		document.getElementById("draggableSujet-" + dragNumberSujet).style.display = "";
		counterSujet--;
		dragNumberSujet--;
	}

}

function dragSujet(ev, id) {
	// numero de la ligne de tableau où est contenu le sujet
	dragNumberSujet = id;
}
/**   */
function dropSujet(ev, id) {
	// var id = (id == undefined) ? ev.dataTransfer.getData("id") : id;

	// zone dans laquelle on souhaite inclure l'étudiant
	// on la sélectionne par son id
	divDrop = document.getElementById("sujet" + id);

	var text = divDrop.innerHTML;

	// On vérifie si un sujet n'est pas déjà présent dans la case 
	// si c'est le cas il sera remis dans le tableau
	if (text.length > 5) {
		// récuparation de l'id de la ligne du sujet à remettre dans le
		// tableau
		var id = text.substring(text.indexOf("*blou*") + 6, text
				.indexOf("*/blou*"));
		// ré-affichage du sujet dans le tableau
		document.getElementById("draggableSujet-" + id).style.display = "";
	}

	// on cache le sujet du tableau qu'on vient de dropper dans la case sujet
	document.getElementById("draggableSujet-" + dragNumberSujet).style.display = "none";

	// on met les informations nécessaires dans la case sujet
	divDrop.innerHTML = document.getElementById("draggableSujet-" + dragNumberSujet).innerHTML
	+ " "
	+ "<input name='idSujet" + id + "' id='idSujet" + id
	+ "' type ='hidden' value='"
	+ document.getElementById("idSujet-" + dragNumberSujet).value
	+ "'/>";
}

/** Change la couleur de la bordure lors du hover du div drop */
function allowdropSujet(ev, id) {
	ev.preventDefault();
}

/** Change la couleur de la bordure lorsque l'on quitte le div drop */
function refusedropSujet(ev, id) {
	ev.preventDefault();
}

function changerTailleScroll() {
	document.getElementById("tableAttribution").style.height = document
			.getElementById('compoEquipe').clientHeight
			+ 100 + "px";
}
window.onload = changerTailleScroll;