var notif;
	
function notification() {
	// Voyons si le navigateur supporte les notifications
	if (!("Notification" in window)) {
		alert("Ce navigateur ne supporte pas les notifications desktop");
	}

	// Nous avons besoin de la permission de l'utilisateur
	// Note : Chrome n'implémente pas la propriété statique permission
	// Donc, nous devons vérifier s'il n'y a pas 'denied' à la place de 'default'
	else if (Notification.permission !== 'denied') {
		Notification
				.requestPermission(function(permission) {

					// Quelle que soit la réponse de l'utilisateur, nous nous assurons de stocker cette information
					if (!('permission' in Notification)) {
						Notification.permission = permission;
					}
					
				});
	}
}

// Comme ça, si l'utlisateur a refusé toute notification, et que vous respectez ce choix,
// il n'y a pas besoin de l'ennuyer à nouveau.

notification();