/* When the user clicks on the button, 
	toggle between hiding and showing the dropdown content */
function functionDropDown() {
    document.getElementById("myDropdown").classList.toggle("show");
}

// Close the dropdown if the user clicks outside of it
window.onclick = function(event) {
    if (!event.target.matches('.dropbtn')) {

        var dropdowns = document.getElementsByClassName("dropdown-content");
        var i;
        for (i = 0; i < dropdowns.length; i++) {
            var openDropdown = dropdowns[i];
            if (openDropdown.classList.contains('show')) {
                openDropdown.classList.remove('show');
            }
        }
    }
}

function start() {
    /*var eventSource = new EventSource("Notifier");
    eventSource.onmessage = function(event) {
        document.getElementById('myDropdown').innerHTML = event.data;
    };*/
    var eventSource = new EventSource("Notifier");
    eventSource.onerror = eventSourceErrorFunction;
    var eventSourceErrorFunction = function(event)
    {
        if (event.eventPhase == EventSource.CLOSED) {
            that.eventSource.close();
            console.log("Event Source Closed");
        }
    }

    eventSource.addEventListener('toute_notifs', function(event) {
        $("#notification").html(event.data);
        loadLien();
    }, false);

    eventSource.addEventListener('new_notifs', function(event) {
        notification(event.data);
        var v = $("#notificationCount");
        var act = v.text();
        v.text(parseInt(act) + 1);
        loadLien();
    }, false);

}
function notification(commentaire){
    // Voyons si le navigateur supporte les notifications
    if (!("Notification" in window)) {
        return;
    }
    // Voyons si l'utilisateur est OK pour recevoir des notifications
    else if (Notification.permission === "granted") {
        // Si c'est ok, créons une notification
        var value = commentaire.split("/");
        var notification = new Notification(value[0],{icon: './images/eseo_courbe.gif', data: value[1]});
        notification.onclick = function(e) {
            window.location.href = e.target.data;
        }
        setTimeout(notification.close.bind(notification), 10000);

    }
}

//method="post" action="<c:url value="/AjouterSujet"/>"
window.onload = start;
//setTimeout(start, 1000);

function post(path, params, method) {
    method = method || "post"; // Set method to post by default if not specified.

    // The rest of this code assumes you are not using a library.
    // It can be made less wordy if you use one.
    var form = document.createElement("form");
    form.setAttribute("method", method);
    form.setAttribute("action", path);
    form.setAttribute("id", "notificationRedirector");

    for(var key in params) {
        if(params.hasOwnProperty(key)) {
            var hiddenField = document.createElement("input");
            hiddenField.setAttribute("type", "hidden");
            hiddenField.setAttribute("name", key);
            hiddenField.setAttribute("value", params[key]);

            form.appendChild(hiddenField);
        }
    }

    document.body.appendChild(form);
    document.getElementById("notificationRedirector").submit();

}

function loadLien() {
    $("a.notificationLien").one('click', function() {
        //Rediriger vers Notifier
        var data = $(this).attr('value');
        post("/Notifier", {data: data});
    });
}