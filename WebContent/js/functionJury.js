var dragId = "";

function drag(event, id) {
	dragId = id;
}

function drop(idDrop) {
	var divDrag = document.getElementById(dragId);
	var divDrop = document.getElementById(idDrop);
	divDrop.innerHTML = divDrag.innerHTML;
	divDrop.getElementsByTagName('input')[0].name = divDrop.id
			+ divDrop.getElementsByTagName('input')[0].name;
}



/** Change la couleur de la bordure lors du hover du div drop */
function allowDrop(ev) {
	ev.preventDefault();
	// var divDrop = document.getElementById("drop");
	// divDrop.style.border = "3px dashed #30e8db";
}

/** Change la couleur de la bordure lorsque l'on quitte le div drop */
function refuseDrop(ev) {
	ev.preventDefault();
	// var divDrop = document.getElementById("drop");
	// divDrop.style.border = "1px solid #000000";
}
var everyJury = document.querySelectorAll('*[id^="jury-"]');

function juryAlea() {
	var everyProf = document.querySelectorAll('*[id^="profDrag-"]');
	if (everyProf.length < 3) {
		alert("Pas assez de professeurs dans cette option pour utiliser cette fonctionnalité");
		return;
	}
	var listEveryProf = everyProf;
	var array = Array.from(Array(everyProf.length).keys());
	var noDouble = -1;
	var noTriple = -1;
	for (var i = 0; i < everyJury.length; i++) {
		rand = Math.floor((Math.random() * array.length) + 0);
		var randValue = array[rand];
		everyJury[i].innerHTML = listEveryProf[randValue].innerHTML;
		// alert(array +" db :"+noDouble + "trp :"+ noTriple);
		array.splice(rand, 1);

		if ((noDouble != -1) && (everyProf.length - listEveryProf.length > 1)) {
			array.push(noDouble);
			array.push(noTriple);
			noDouble = -1;
		}
		if (array.length == 1) {
			noDouble = randValue;
			noTriple = array[0];
			i++;
			everyJury[i].innerHTML = listEveryProf[noTriple].innerHTML;

			array = Array.from(Array(everyProf.length).keys());
			array.splice((noDouble > noTriple ? noDouble : noTriple), 1);
			array.splice((noDouble > noTriple ? noTriple : noDouble), 1);
		}
		everyJury[i].getElementsByTagName('input')[0].name = everyJury[i].id
				+ everyJury[i].getElementsByTagName('input')[0].name;
	}
}

$('.form_date').datetimepicker({
	language : 'fr',
	weekStart : 1,
	todayBtn : 1,
	autoclose : 1,
	todayHighlight : 1,
	startView : 2,
	minView : 0,
	forceParse : 0
});

var sel2 = document.querySelector('#divProfs');
var options2 = sel2.querySelectorAll('*[id^="profDrag-"]');

function changeProfOption() {
	var option = document.getElementById("divOption").value;
	sel2.innerHTML = "";
	for (var i = 0; i < options2.length; i++) {
		if ((option.length > 0) ? ((options2[i].dataset.option).indexOf(option) != -1)
				: true) {
			sel2.appendChild(options2[i]);
		}
	}
}

function changeProfOption() {
	var option = document.getElementById("divOption").value;
	sel2.innerHTML = "";
	for (var i = 0; i < options2.length; i++) {
		if ((option.length > 0) ? ((options2[i].dataset.option).indexOf(option) != -1)
				: true) {
			sel2.appendChild(options2[i]);
		}
	}
}
changeProfOption()

function sendOnlyOneJury(id) {
	document.getElementById("numDivForm").value = id;

}