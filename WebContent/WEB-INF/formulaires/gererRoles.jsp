<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <c:import url="/inc/head.jsp"/>
    <title>SoManager - Gérer Roles</title>
    <link rel="stylesheet" href="<c:url value="/css/jquery.typeahead.css"/>">
</head>
<body>
<div class="wrapper">
    <c:import url="/Navbar"/>
    <!-- Page Content Holder -->
    <div id="content">
        <c:import url="/inc/headerForSidebar.jsp"/>


        <div class="panel panel-primary ">
            <div class="panel-heading">Admin - Gestion des roles
            </div>

            <div class="panel-body">
                <c:import url="/inc/callback.jsp"/>

                <ul class="nav nav-tabs" role="tablist" style="margin-bottom: 20px">
                    <li class="active"><a data-toggle="tab" href="#chercherUtilisateur">
                        <i class="fas fa-search-plus fa-fw" aria-hidden="true"></i>&nbsp;
                        Chercher un utilisateur
                    </a></li>
                    <li><a data-toggle="tab" href="#importerListeRoles">
                        <i class="far fa-list-alt fa-fw" aria-hidden="true"></i>&nbsp;
                        Importer une liste de roles
                    </a></li>
                </ul>

                <%-- Contenu des différentes tab --%>
                <div class="tab-content">

                    <%-- Gérer les meetings--%>
                    <div id="chercherUtilisateur" class="tab-pane fade in active">
                        <div class="row">
                            <c:import url="/WEB-INF/formulaires/gererRoles/chercherUtilisateur.jsp"/>
                        </div>
                    </div>

                    <%-- Ajouter meeting --%>
                    <div id="importerListeRoles" class="tab-pane fade">
                        <div class="row">
                            <c:import url="/WEB-INF/formulaires/gererRoles/importerRoles.jsp"/>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
</body>
<c:import url="/inc/scripts.jsp"/>
<script src="<c:url value="/js/jquery.typeahead.js"/>" type="text/javascript"></script>

<script>
    var listeImporte = "<c:out value="${listeNomPrenomID}"/>".split(",")
    var data = [""]

    for (var iter = 0; iter < listeImporte.length; iter++) {
        data.push(listeImporte[iter]);
    }

    typeof $.typeahead === 'function' && $.typeahead({
        input: ".js-typeahead",
        minLength: 1,
        maxItem: 5,
        order: "asc",
        hint: false,
        emptyTemplate: 'Aucun resultat pour "{{query}}"',
        source: data
    });

    $(function () {
        //Capturer le changement de fichier dans un evenement
        $(document).on('change', ':file', function () {
            var input = $(this),
                label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
            input.trigger('fileselect', [label]);
        });

        //Quand on séléctionne un fichier à envoyer
        $(":file").on("fileselect", function (event, label) {
            var nomFichier = $("#nomFichier");
            var sendCsvB = $("#sendCsvButton");

            //Si le fichier est un .csv on autorise
            if (label.indexOf(".csv") !== -1) {
                nomFichier.val(label);
                sendCsvB.prop('disabled', false);
            } else {
                nomFichier.val("");
                sendCsvB.prop('disabled', true);
            }
        });

    });

    function afficherFormulaire(nomCase) {
        var formulaireAnneeSco = document.getElementById("formulaireAnneeSco");
        var formulaireContratPro = document.getElementById("formulaireContratPro");
        var listeOption = document.getElementById("listeOption");
        var colonneOptions = document.getElementById("colonneOptions");
        switch (nomCase) {
            case "etudiant":

                formulaireAnneeSco.style.display = "block";
                //afficherContratPro(radioAnneeSco);
                //formulaireContratPro.style.display = "block";
                listeOption.style.display = "block";
                colonneOptions.style.display = "block";
                break;
            case "admin":
                formulaireAnneeSco.style.display = "none";
                formulaireContratPro.style.display = "none";
                listeOption.style.display = "none";
                colonneOptions.style.display = "none";
                break;
            case "serviceCom":
                formulaireAnneeSco.style.display = "none";
                formulaireContratPro.style.display = "none";
                listeOption.style.display = "none";
                colonneOptions.style.display = "none";
                break;
            default:
                formulaireAnneeSco.style.display = "none";
                formulaireContratPro.style.display = "none";
                listeOption.style.display = "block";
                colonneOptions.style.display = "block";
        }
    }

    function afficherContratPro() {
        var selectedOption = $("input:radio[name=radioAnneeScolaire]:checked").val();
        var formulaireContratPro = document.getElementById("formulaireContratPro");

        switch (selectedOption) {
            case "I2":
                formulaireContratPro.style.display = "none";
                break;
            case "I3":
                formulaireContratPro.style.display = "block";
        }
    }
</script>
</html>