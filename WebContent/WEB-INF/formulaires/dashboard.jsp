<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<c:import url="/inc/head.jsp" />
<title>SoManager - Tableau de bord</title>
</head>
<body>
	<!--  Partie formulaires Jury -->
	<form action="<c:url value="GererJuryPFE"/>" method="post"
		id="RemplacerJury">
		<input type="hidden" name="formulaire" id="formulaire"
			value="RemplacerJury" />
	</form>

	<div class="wrapper">
		<c:import url="/Navbar" />
		<div id="content">

			<c:import url="/inc/headerForSidebar.jsp" />

			<div class="panel panel-primary">
				<div class="panel-heading">
					<i class="fas fa-list-ul fa-fw" aria-hidden="true"></i>&nbsp;
					&emsp; <b> Tableau de bord </b>
				</div>
				<div class="panel-body">
					<c:import url="/inc/callback.jsp" />
					<div class="col-lg-6 col-md-6">
						<c:if test="${!empty monSujet}">
							<div class="panel panel-default ">
								<div class="panel-heading">
									<i class="fas fa-book fa-fw" aria-hidden="true"></i>&nbsp; <b>
										&emsp; Mon Sujet </b>
								</div>
								<div class="panel-body">
									<form method="get" action="<c:url value="/SujetSuivi"/>">
										<p>
											<b><c:out value="${monSujet.titre}" /></b>
										</p>
										<p>
											<c:out value="${monSujet.description}" />
										</p>
										<p>
											<b>Membres d'équipe :</b>
										</p>
										<table class="table table-condensed">
											<tbody>
												<c:forEach var="collegue" items="${monEquipe}"
													varStatus="loop">
													<tr>
														<td><c:out value="${collegue.nom}" /></td>
														<td><c:out value="${collegue.prenom}" /></td>
													</tr>
												</c:forEach>
											</tbody>
										</table>
										<p>
											<b>Dates limites :</b>
										</p>

										<button type="submit" class="btn btn-primary center-block"
											id="submit">Voir mon sujet</button>
									</form>
								</div>
							</div>
						</c:if>
						<c:if test="${!empty sujetInterressant}">
							<div class="panel panel-default ">
								<div class="panel-heading">
									<i class="fas fa-book fa-fw" aria-hidden="true"></i>&nbsp; <b>
										&emsp; Des sujets de votre option qui pourraient vous
										intérresser : </b>
								</div>
								<div class="panel-body">
									<form method="get" action="<c:url value="/ConsulterSujets"/>">
										<table class="table table-condensed">
											<thead>
												<tr>
													<th>Titre</th>
													<th>Description</th>
												</tr>
											</thead>
											<tbody>
												<c:forEach var="sujet" items="${sujetInterressant}"
													varStatus="loop">
													<tr>
														<td><c:out value="${sujet.titre}" /></td>
														<td><c:out value="${sujet.description}" /></td>
													</tr>
												</c:forEach>
											</tbody>
										</table>
										<button type="submit" class="btn btn-primary center-block"
											id="submit">Voir les sujets</button>
									</form>
								</div>
							</div>
						</c:if>
						<c:if test="${bonusMalusPresent eq true}">
							<div class="panel panel-default ">
								<div class="panel-heading">
									<i class="fas fa-book fa-fw"></i>&nbsp; <b> &emsp; Vous
										avez un bonus/malus à valider : </b>
								</div>
								<div class="panel-body">
									<form method="get" action="<c:url value="Dashboard"/>">
										<table class="table table-condensed">
											<thead>
												<tr>
													<th>Justification et bonus:</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td><c:out value="${justificationBonusMalus}" /></td>
													<td><c:out value="${valeurBonusMalus}" /></td>
												</tr>
											</tbody>
										</table>

										<div style="text-align: center;">
											<input type="hidden" value="" name="validationDuBanusMalus"
												id="idValidationDuBanusMalus">

											<button type="submit" class="btn btn-primary"
												onclick="sendBonusMalusAccepter()" id="submit"
												style="background-color: #34C924; border-color: grey; text-align: center; width: 90px">Accepter</button>

											<button type="submit" class="btn btn-primary"
												onclick="sendBonusMalusRefuser()" id="submit"
												style="background-color: #FE1B00; border-color: grey; text-align: center; width: 90px">Refuser</button>
										</div>
									</form>
								</div>
							</div>
						</c:if>
						<c:if test="${!empty sujetSuivi}">
							<div class="panel panel-default ">
								<div class="panel-heading">
									<i class="fas fa-book fa-fw" aria-hidden="true"></i>&nbsp;
									&emsp; <b> Le<c:if test="${fn:length(sujetSuivi) > 1}">s</c:if>
										sujet<c:if test="${fn:length(sujetSuivi) > 1}">s</c:if> que
										vous suivez :
									</b>
								</div>
								<div class="panel-body">
									<form method="get" action="<c:url value="/SujetSuivi"/>">
										<table class="table table-condensed">
											<thead>
												<tr>
													<th>Titre</th>
												</tr>
											</thead>
											<tbody>
												<c:forEach var="sujet" items="${sujetSuivi}"
													varStatus="loop">
													<tr>
														<td><c:out value="${sujet.titre}" /></td>
													</tr>
												</c:forEach>
											</tbody>
										</table>
										<button type="submit" class="btn btn-primary center-block"
											id="submit">Voir les sujets</button>
									</form>
								</div>
							</div>
						</c:if>
						<c:if test="${!empty sujetValidation || !empty sujetPublication}">
							<div class="panel panel-default ">
								<div class="panel-heading">
									<i class="fas fa-book fa-fw" aria-hidden="true"></i>&nbsp;
									&emsp; <b> Sujet(s) en attente de validation/publication :
									</b>
								</div>
								<div class="panel-body">
									<form method="get" action="<c:url value="/GererSujets"/>">
										<table class="table table-condensed">
											<thead>
												<tr>
													<th>Titre</th>
													<th>Etat</th>
												</tr>
											</thead>
											<tbody>
												<c:forEach var="sujet" items="${sujetValidation}"
													varStatus="loop">
													<c:if test="${sujet.etat == etatDepose}">
														<tr>
															<td><c:out value="${sujet.titre}" /></td>
															<td><c:out value="${sujet.etat}" /></td>
														</tr>
													</c:if>
												</c:forEach>
												<c:forEach var="sujet" items="${sujetPublication}"
													varStatus="loop">
													<c:if test="${sujet.etat == etatDepose}">
														<tr>
															<td><c:out value="${sujet.titre}" /></td>
															<td><c:out value="${sujet.etat}" /></td>
														</tr>
													</c:if>
												</c:forEach>
											</tbody>
										</table>
										<button type="submit" class="btn btn-primary center-block"
											id="submit">Gérer les sujets</button>
									</form>
								</div>
							</div>
						</c:if>
						<c:if test="${!empty professeurValider}">
							<div class="panel panel-default ">
								<div class="panel-heading">
									<i class="fas fa-user fa-fw" aria-hidden="true"></i>&nbsp;
									&emsp; <b> Professeur<c:if
											test="${fn:length(professeurValider) > 1}">s</c:if> en
										attente d'une approbation :
									</b>
								</div>
								<div class="panel-body">
									<form method="get" action="<c:url value="/GererProfesseurs"/>">
										<table class="table table-condensed">
											<thead>
												<tr>
													<th>Nom Prenom</th>
													<th>Sujet</th>
													<th>fonction</th>
												</tr>
											</thead>
											<tbody>
												<c:forEach var="prof" items="${professeurValider}"
													varStatus="loop">
													<tr>
														<td><c:out value="${prof[0]} ${prof[1]}" /></td>
														<td><c:out value="${prof[2]}" /></td>
														<td><c:out value="${prof[3]}" /></td>
													</tr>
												</c:forEach>
											</tbody>
										</table>
										<button type="submit" class="btn btn-primary center-block"
											id="submit">Gérer les professeurs</button>
									</form>
								</div>
							</div>
						</c:if>
					</div>
					<div class="col-lg-6 col-md-6">
						<c:if test="${!empty profDemandeJury}">
							<div class="panel panel-default ">
								<div class="panel-heading">
									<i class="fas fa-pencil-alt fa-fw" aria-hidden="true"></i>&nbsp;
									&emsp; <b> Inscriptions : </b>
								</div>
								<div class="panel-body">
									<form action="<c:url value="GererSoutenanceDemandeInc"/>"
										method="post" id="DemandeInc">
										<input type="hidden" name="formulaire" id="formulaire"
											value="ValiderInc" /> <input name="idSujet" id="idSujet"
											type="hidden" value="">
										<p
											class="pv-top-card-section__summary-text text-align-left mt4 ember-view">
											${commentaire}</p>
										<table class="table table-condensed">
											<thead>
												<tr>
													<th>Sujet</th>
													<th>Date</th>
													<th>Jury n°1</th>
													<th>Jury n°2</th>
													<th>Inscription</th>

												</tr>
											</thead>
											<tbody>
												<c:forEach var="sujet" items="${listeSujetsChoisis}"
													varStatus="loop">
													<input name="idSujet-${loop.index}"
														id="idSujet-${loop.index}" type="hidden"
														value="${sujet.idSujet}">
													<tr>
														<td><c:out value="${sujet.titre}" /></td>
														<td><fmt:formatDate timeStyle="short" type="both"
																value="${listeSoutenancesChoisies[loop.index].dateSoutenance}" /></td>
														<td><c:out
																value="${listeProfesseursChoisis1[loop.index].prenom}" />
															<c:out
																value="${listeProfesseursChoisis1[loop.index].nom}" /></td>
														<td><c:out
																value="${listeProfesseursChoisis2[loop.index].prenom}" />
															<c:out
																value="${listeProfesseursChoisis2[loop.index].nom}" /></td>
														<td><input class="btn btn-primary center-block"
															type="button" value="S'inscrire"
															onclick="submitInscription(${loop.index});"></td>
													</tr>
												</c:forEach>
											</tbody>
										</table>
									</form>
								</div>
							</div>
						</c:if>
					</div>
					<div class="col-lg-6 col-md-6">
						<c:if
							test="${!empty sujetsReferent || !empty sujetsPoster || !empty sujetsSoutenance}">
							<div class="panel panel-default ">
								<div class="panel-heading">
									<i class="fas fa-pencil-alt fa-fw" aria-hidden="true"></i>&nbsp;
									&emsp; <b> Merci de vérifier vos disponibilités et d'attribuer les
										notes : </b>
								</div>
								<div class="panel-body">


									<c:if test="${!empty sujetsReferent }">
										<table class="table table-condensed">
											<thead>
												<tr>
													<th>Suivi de projet</th>
													<th></th>
												</tr>
											</thead>
											<tbody>
												<c:forEach var="note" items="${manqueNoteSuivi}"
													varStatus="loop">
													<tr>
														<td><c:out
																value="${sujetsReferent[loop.index].titre}" /></td>
														<c:choose>
															<c:when test="${note.equals(\"toutes\")}">
																<td><span class="erreur"><i
																		class="fas fa-times fa-fw" aria-hidden="true"></i>&nbsp;&nbsp;Vous
																		devez entrer les notes intermédiaires et de projet
																		pour cette équipe</span></td>
															</c:when>
															<c:when test="${note.equals(\"intermediaire\")}">
																<td><span class="erreur"><i
																		class="fas fa-times fa-fw" aria-hidden="true"></i>&nbsp;&nbsp;Vous
																		devez entrer les notes intermédiaires pour cette
																		équipe</span></td>
															</c:when>
															<c:when test="${note.equals(\"projet\")}">
																<td><span class="erreur"><i
																		class="fas fa-times fa-fw" aria-hidden="true"></i>&nbsp;&nbsp;Vous
																		devez entrer les notes de projet pour cette équipe</span></td>
															</c:when>
															<c:when test="${note.equals(\"rien\")}">
																<td><i class="far fa-check-circle fa-fw"
																	aria-hidden="true"></i>&nbsp;Vous n'avez aucune note à
																	attribuer</td>
															</c:when>
															<c:otherwise>
																<td><i class="far fa-check-circle fa-fw"
																	aria-hidden="true"></i>&nbsp;Notes attribuées</td>
															</c:otherwise>
														</c:choose>
													</tr>
												</c:forEach>
											</tbody>
										</table>
									</c:if>

									<form action="<c:url value="Dashboard"/>" method="post"
										id="JuryPosters">
										<input type="hidden" name="formulaire" id="formulaire"
											value="RemplacerJury" />
										<c:if test="${!empty sujetsPoster }">

											<table class="table table-condensed">
												<thead>
													<tr>
														<th>Jury de poster</th>
														<th>Date</th>
														<th>Disponibilités</th>
														<th>Notes</th>

													</tr>
												</thead>
												<tbody>
													<c:forEach var="note" items="${sujetsPoster}"
														varStatus="loop">
														<tr>

															<td><c:out value="${note.titre}" /></td>
															<td><fmt:formatDate timeStyle="short" type="both"
																	value="${datePoster[loop.index]}" /></td>
															<td>
																<p id="calendars" style="text-align: center;">

																	<i class="far fa-calendar-check"
																		style="font-size: 28px; color: grey"> </i> <input
																		type="hidden" name="idPoster" id="idPoster"
																		value='<c:out value="${note.idSujet}"/>' />
																	<button type="button"
																		style="border: none; background: none"
																		data-toggle="modal" data-target="#remplacerJuryPoster">
																		<i class="far fa-calendar-times"
																			style="font-size: 28px; color: red"> </i>
																	</button>
																</p>

															</td>

															<c:choose>
																<c:when test="${!empty manqueNotePoster[loop.index]}">
																	<td><span class="erreur"><i
																			class="fas fa-times fa-fw" aria-hidden="true"></i>&nbsp;&nbsp;Vous
																			devez entrer les notes pour cette équipe</span></td>
																</c:when>
																<c:otherwise>
																	<td><i class="far fa-check-circle fa-fw"
																		aria-hidden="true"></i>&nbsp;Notes attribuées</td>
																</c:otherwise>
															</c:choose>
														</tr>
													</c:forEach>
												</tbody>
											</table>
										</c:if>
									</form>
									<form method="get" action="<c:url value="/Noter"/>">
										<c:if test="${!empty sujetsSoutenance }">
											<table class="table table-condensed">
												<thead>
													<tr>
														<th>Jury de soutenance</th>
														<th></th>
														<th></th>
													</tr>
												</thead>
												<tbody>
													<c:forEach var="note" items="${sujetsSoutenance}"
														varStatus="loop">
														<tr>
															<td><c:out value="${note.titre}" /></td>
															<td><fmt:formatDate timeStyle="short" type="both"
																	value="${dateSoutenance[loop.index]}" /></td>
															<c:choose>
																<c:when
																	test="${!empty manqueNoteSoutenance[loop.index]}">
																	<td><span class="erreur"><i
																			class="fas fa-times fa-fw" aria-hidden="true"></i>&nbsp;&nbsp;Vous
																			devez entrer les notes pour cette équipe</span></td>
																</c:when>
																<c:otherwise>
																	<td><i class="far fa-check-circle fa-fw"
																		aria-hidden="true"></i>&nbsp;Notes attribuées</td>
																</c:otherwise>
															</c:choose>
														</tr>
													</c:forEach>
												</tbody>
											</table>
										</c:if>
										<button type="submit" class="btn btn-primary center-block"
											id="submit">Noter</button>
									</form>
								</div>
							</div>
						</c:if>
						<c:if test="${!empty equipeValider}">
							<div class="panel panel-default ">
								<div class="panel-heading">
									<i class="fas fa-graduation-cap fa-fw" aria-hidden="true"></i>&nbsp;
									&emsp; <b> Equipe<c:if
											test="${fn:length(equipeValider) > 1}">s</c:if> en attente de
										validation :
									</b>
								</div>
								<div class="panel-body">
									<form method="get" action="<c:url value="/GererEquipes"/>">
										<table class="table table-condensed">
											<thead>
												<tr>
													<th>Sujet</th>
													<th>Minimum membre</th>
													<th>Maximum membre</th>
													<th>Nombre membre</th>
												</tr>
											</thead>
											<tbody>
												<c:forEach var="equipe" items="${equipeValider}"
													varStatus="loop">
													<tr>
														<td><c:out value="${equipe[0]}" /></td>
														<td><p align="center">
																<c:out value="${equipe[1]}" />
															</p></td>
														<td><p align="center">
																<c:out value="${equipe[2]}" />
															</p></td>
														<td><p align="center">
																<c:out value="${equipe[3]}" />
															</p></td>
													</tr>
												</c:forEach>
											</tbody>
										</table>
										<button type="submit" class="btn btn-primary center-block"
											id="submit">Gérer les équipes</button>
									</form>

								</div>
							</div>
						</c:if>
						<c:if test="${!empty mesSujets}">
							<div class="panel panel-default ">
								<div class="panel-heading">
									<i class="fas fa-book fa-fw" aria-hidden="true"></i>&nbsp; <b>
										&emsp; Etat de <c:if test="${fn:length(mesSujets) > 1}">vos sujets déposés</c:if>
										<c:if test="${fn:length(mesSujets) == 1}">votre sujet déposé</c:if>
									</b>
								</div>
								<div class="panel-body">
									<form method="get" action="<c:url value="/VoirMesSujets"/>">
										<table class="table table-condensed">
											<thead>
												<tr>
													<th>Titre</th>
													<th>Etat</th>
												</tr>
											</thead>
											<tbody>
												<c:forEach var="sujet" items="${mesSujets}" varStatus="loop">
													<tr>
														<td><c:out value="${sujet.titre}" /></td>
														<td><c:out value="${sujet.etat}" /></td>
													</tr>
												</c:forEach>
											</tbody>
										</table>
										<button type="submit" class="btn btn-primary center-block"
											id="submit">Gérer mes sujets</button>
									</form>
								</div>
							</div>
						</c:if>
					</div>
					<c:if test="${!empty compteValider}">
						<div class="col-lg-6 col-md-6">
							<div class="panel panel-default ">
								<div class="panel-heading">
									<i class="fas fa-user fa-fw" aria-hidden="true"></i>&nbsp;
									&emsp; <b> Utilisateur<c:if
											test="${fn:length(sujetPublication) > 1}">s</c:if> en attente
										de validation :
									</b>
								</div>
								<div class="panel-body">
									<form method="get" action="<c:url value="/GererComptes"/>">
										<table class="table table-condensed">
											<thead>
												<tr>
													<th>Nom</th>
													<th>Prenom</th>
													<th>Id ESEO</th>
													<th>Email</th>
												</tr>
											</thead>
											<tbody>
												<c:forEach var="utilisateur" items="${compteValider}"
													varStatus="loop">
													<tr>
														<td><c:out value="${utilisateur.nom}" /></td>
														<td><c:out value="${utilisateur.prenom}" /></td>
														<td><c:out value="${utilisateur.identifiant}" /></td>
														<td><c:out value="${utilisateur.email}" /></td>
													</tr>
												</c:forEach>
											</tbody>
										</table>
										<button type="submit" class="btn btn-primary center-block"
											id="submit">Gérer les utilisateurs</button>
									</form>
								</div>
							</div>
						</div>
					</c:if>
				</div>
			</div>
		</div>
	</div>
	<c:import url="/inc/scripts.jsp" />
	<%-- Modal : Avertissement de modification : Remplacement professeur --%>
	<div id="remplacerJuryPoster" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
					<div class="alert alert-danger" role="alert">
						<h4 class="alert-heading">Attention !</h4>
						<p>
							<b>Vous allez être remplacé</b>
						</p>
						<p class="mb-0">
							<b>Etes-vous sûr de vouloir quitter ce jury de poster ?</b>
						</p>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary" data-dismiss="modal">Annuler</button>
					<button type="button" class="btn btn-danger" data-dismiss="modal"
						onclick="submitRemplacerJury();">Oui</button>
				</div>
			</div>
		</div>
	</div>
	<script>
		function submitRemplacerJury() {
			document.getElementById('JuryPosters').submit();
		}

		function sendBonusMalusAccepter() {
			document.getElementById('idValidationDuBanusMalus').value = "ACCEPTER";
		}
		function sendBonusMalusRefuser() {
			document.getElementById('idValidationDuBanusMalus').value = "REFUSER";
		}
	</script>
	<script>var listeSujetsChoisisJ = "${listeSujetsChoisis}";</script>	
	<script type="text/javascript" src="<c:url value="/js/inscription-jury-soutenance-ajax.js"/>"></script>
</body>
</html>