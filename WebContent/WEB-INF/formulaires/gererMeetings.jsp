<%--
  Created by IntelliJ IDEA.
  User: dimitrijarneau
  Date: 19/05/2018
  Time: 10:14
  To change this template use File | Settings | File Templates.
--%>
<%--
  Created by IntelliJ IDEA.
  User: dimitrijarneau
  Date: 10/04/2018
  Time: 08:31
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <c:import url="/inc/head.jsp"/>
    <title>SoManager - Gérer mes Meetings</title>
    <link rel="stylesheet" href="<c:url value="/css/jquery.typeahead.css"/>">
    <link rel="stylesheet" media="screen" href="<c:url value="/css/bootstrap-datetimepicker.min.css"/>">
</head>
<body>
<div class="wrapper">
    <c:import url="/Navbar"/>
    <!-- Page Content Holder -->
    <div id="content">
        <c:import url="/inc/headerForSidebar.jsp"/>

        <div class="panel panel-primary ">
            <div class="panel-heading">Meetings - Mes meetings
            </div>

            <div class="panel-body">

                <c:import url="/inc/callback.jsp"/>

                <ul class="nav nav-tabs" role="tablist" style="margin-bottom: 20px">
                    <li class="active"><a data-toggle="tab" href="#voirMeeting">
                        <i class="fas fa-edit fa-fw" aria-hidden="true"></i>&nbsp;
                        Gérer mes meetings
                    </a></li>
                    <li><a data-toggle="tab" href="#ajouterM">
                        <i class="fas fa-comments fa-fw" aria-hidden="true"></i>&nbsp;
                        Ajouter un meeting
                    </a></li>
                    <li><a data-toggle="tab" href="#ajoutParticipant">
                        <i class="fas fa-user-plus fa-fw" aria-hidden="true"></i>&nbsp;
                        Ajouter un participant
                    </a></li>
                    <li><a data-toggle="tab" href="#supprimerParticipant">
                        <i class="fas fa-user-times fa-fw" aria-hidden="true"></i>&nbsp;
                        Supprimer un participant
                    </a></li>
                </ul>

                <%-- Contenu des différentes tab --%>
                <div class="tab-content">

                    <%-- Gérer les meetings--%>
                    <div id="voirMeeting" class="tab-pane fade in active">
                        <div class="row">
                            <c:import url="/WEB-INF/formulaires/gererMeetings/voirMeetings.jsp"/>
                        </div>
                    </div>

                    <%-- Ajouter meeting --%>
                    <div id="ajouterM" class="tab-pane fade">
                        <div class="row">
                            <c:import url="/WEB-INF/formulaires/gererMeetings/ajouterMeetings.jsp"/>
                        </div>
                    </div>

                    <%-- Ajouter un participant --%>
                    <div id="ajoutParticipant" class="tab-pane fade">
                        <div class="row">
                            <c:import url="/WEB-INF/formulaires/gererMeetings/ajouterParticipants.jsp"/>
                        </div>
                    </div>
                    <%-- Supprimer un participant --%>
                    <div id="supprimerParticipant" class="tab-pane fade">
                        <div class="row">
                            <c:import url="/WEB-INF/formulaires/gererMeetings/supprimerParticipants.jsp"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<c:import url="/inc/scripts.jsp"/>
<script type="text/javascript" src="<c:url value="/js/dragAndDrop.js"/>"></script>
<script src="<c:url value="/js/jquery.typeahead.js"/>" type="text/javascript"></script>
<script type="text/javascript" src="<c:url value="/js/bootstrap-datetimepicker.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/js/bootstrap-datetimepicker.fr.js"/>"></script>
<script>
    var listeImporte = "<c:out value="${listeNomPrenomID}"/>".split(",")
    var data = [""]

    for (var iter = 0; iter < listeImporte.length; iter++) {
        data.push(listeImporte[iter]);
    }

    typeof $.typeahead === 'function' && $.typeahead({
        input: ".js-typeahead",
        minLength: 1,
        maxItem: 5,
        order: "asc",
        hint: false,
        emptyTemplate: 'Aucun resultat pour "{{query}}"',
        source: data
    });

    $(function () {
        $('#datetimepicker').datetimepicker({
            icons: {
                time: 'glyphicon glyphicon-time',
                date: 'glyphicon glyphicon-calendar',
                up: 'glyphicon glyphicon-chevron-up',
                down: 'glyphicon glyphicon-chevron-down',
                previous: 'glyphicon glyphicon-chevron-left',
                next: 'glyphicon glyphicon-chevron-right',
                today: 'glyphicon glyphicon-screenshot',
                clear: 'glyphicon glyphicon-trash',
                close: 'glyphicon glyphicon-remove'
            }
        });

    });

    $('.form_date').datetimepicker({
        language: 'fr',
        weekStart: 1,
        todayBtn: 1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 2,
        minView: 2,
        forceParse: 0
    });
    $('.form_time').datetimepicker({
        language: 'fr',
        weekStart: 1,
        todayBtn: 1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 1,
        minView: 0,
        maxView: 1,
        forceParse: 0
    });
</script>
</body>
</html>

