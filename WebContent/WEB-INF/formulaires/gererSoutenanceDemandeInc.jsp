<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<c:import url="/inc/head.jsp" />
<title>SoManager - Gérer Jurys Soutenance</title>
<link rel="stylesheet" href="<c:url value="/css/jquery.typeahead.css"/>">
<link rel="stylesheet" media="screen"
	href="<c:url value="/css/bootstrap-datetimepicker.min.css"/>">
</head>
<body>
	<div class="wrapper">
		<c:import url="/Navbar" />
		<!-- Page Content Holder -->
		<div id="content">
			<c:import url="/inc/headerForSidebar.jsp" />

			<div class="panel panel-primary ">
				<div class="panel-heading">Jurys - Gérer les Jurys</div>

				<div class="panel-body">
					<div class="col-lg-12 col-md-8">
						<c:if test="${!empty profDemandeJury}">
							<div class="panel panel-default ">
								<div class="panel-heading">

									&emsp; <b> Demande de jury en cours : </b>
								</div>
								<div class="panel-body">
									<form action="<c:url value="GererSoutenanceDemandeInc"/>"
										method="post" id="DemandeInc">
										<input type="hidden" name="formulaire" id="formulaire"
											value="SupprimerDemande" /> <input name="idSujet"
											id="idSujet" type="hidden" value="">
										<table class="table table-condensed">
											<thead>
												<tr>
													<th>Sujet</th>
													<th>Date</th>
													<th>Jury n°1</th>
													<th>Jury n°2</th>

												</tr>
											</thead>
											<tbody>
												<c:forEach var="sujet" items="${listeSujetsChoisis}"
													varStatus="loop">
													<input name="idSujet-${loop.index}"
														id="idSujet-${loop.index}" type="hidden"
														value="${sujet.idSujet}">
													<tr>
														<td><c:out value="${sujet.titre}" /></td>
														<td><fmt:formatDate timeStyle="short" type="both"
																value="${listeSoutenancesChoisies[loop.index].dateSoutenance}" /></td>
														<td><c:out
																value="${listeProfesseursChoisis1[loop.index].prenom}" />
															<c:out
																value="${listeProfesseursChoisis1[loop.index].nom}" /></td>
														<td><c:out
																value="${listeProfesseursChoisis2[loop.index].prenom}" />
															<c:out
																value="${listeProfesseursChoisis2[loop.index].nom}" /></td>
													</tr>
												</c:forEach>
											</tbody>
										</table>

										<button class="btn btn-warning center-block"
											id="supprimerDemande">
											<i class="fas fa-eraser fa-fw" aria-hidden="true"></i>&nbsp;
											Supprimer la demande d'inscription
										</button>
									</form>
								</div>
							</div>
						</c:if>
					</div>

					<div class="col-lg-4 col-md-4">
						<div class="panel panel-default ">
							<div class="panel-heading" align="center">Professeurs :</div>
							<div class="panel-body">

								<select class="form-control" name="divOption" id="divOption"
									onchange="changeProfOption();">
									<option value="${optionPrincipal}">${optionPrincipal}</option>
								</select>

								<div class=" text-center" id="divProfs">
									<c:forEach var="prof" items="${listeProfesseurs}"
										varStatus="loop">

										<button style="width: 300px" type="button"
											class="btn btn-default btn-sm" id="profDrag-${loop.index}"
											draggable='true' ondragstart="dragProf(event,${loop.index});"
											data-option='${mapOptionsProf[prof.idUtilisateur]}'>
											<c:out value="${prof.nom}" />
											<c:out value="${prof.prenom}" />
											<input name="idProff${loop.index}" id="idProff${loop.index}"
												type="hidden" value="${prof.idUtilisateur}" />
										</button>

									</c:forEach>
								</div>
							</div>

						</div>
					</div>


					<div class="col-lg-4">
						<div class="panel panel-default" id="compoEquipe">
							<div class="panel-heading">Demande d'inscription</div>
							<div class="panel-body">
								<form action="<c:url value="/GererSoutenanceDemandeInc"/>"
									method="post">
									<input type="hidden" name="formulaire" id="formulaire"
										value="CreationDemande" /> <label for="sujet">Sujets
										choisis : </label>

									<div id="dynamicInputSujet"></div>

									<br />
									<div class="form-control" id="sujetChoisi"
										ondrop="ajouterSujet(event,'dynamicInputSujet')"
										draggable="false" ondragover="allowDrop(event)"
										ondragleave="refuseDrop(event)">
										Ajouter un sujet <br />
									</div>

									<br> <input class="btn btn-danger center-block"
										type="button" value="Supprimer un sujet"
										onClick="supprimerSujet();"> <br /> <br />

									<p>
										<strong><br>Professeurs : </strong>
									</p>

									<div id="dynamicInput"></div>

									<br />
									<div class="form-control"
										ondrop="ajouterProf(event,'dynamicInput')" draggable="false"
										ondragover="allowDrop(event)" ondragleave="refuseDrop(event)">
										Ajouter un professeur <br />
									</div>

									<br> <input class="btn btn-danger center-block"
										type="button" value="Supprimer un membre"
										onClick="supprimerMembre();"> <br /> <br />

									<div class="form-group has-feedback">
										Commentaire :
										<textarea maxlength="180" name="commentaire"
											class="form-control" id="commentaire"></textarea>
									</div>

									<button class="btn btn-primary center-block" id="submitEquipe">
										<i class="fas fa-plus fa-fw" aria-hidden="true"></i>&nbsp;
										Créer la demande d'inscription
									</button>
								</form>
							</div>
						</div>
					</div>

					<div class="col-lg-4">
						<div class="panel panel-default">
							<div class="panel-heading">Sujets disponibles sans jury</div>
							<div class="panel-body">
								<div class="col-lg-12">
									<c:import url="/inc/listeSujetsValidesJuryManquant.jsp" />
								</div>
							</div>
						</div>
					</div>


				</div>
			</div>
		</div>
	</div>

	<script src="<c:url value="/js/dragAndDrop.js"/>"></script>
	<c:import url="/inc/scripts.jsp" />
	<script src="<c:url value="/js/jquery.typeahead.js"/>"
		type="text/javascript"></script>
	<script type="application/javascript"
		src="<c:url value="/js/perfect-scrollbar.min.js"/>"></script>
	<script type="text/javascript"
		src="<c:url value="/js/bootstrap-datetimepicker.min.js"/>"></script>
	<script type="text/javascript"
		src="<c:url value="/js/bootstrap-datetimepicker.fr.js"/>"></script>
	<script type="text/javascript"
		src="<c:url value="/js/compoJurySoutenances.js"/>"></script>
	<script type="text/javascript"
		src="<c:url value="/js/functionJury.js"/>"></script>
</body>
</html>
