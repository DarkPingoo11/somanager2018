<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <c:import url="/inc/head.jsp"/>
    <title>SoManager - Inscription</title>
</head>
<body>
<div class="wrapper">
    <c:import url="/Navbar"/>
    <div id="content">
        <c:import url="/inc/headerForSidebar.jsp"/>
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2">
                <div class="panel panel-primary ">
                    <div class="panel-heading">Inscription</div>
                    <div class="panel-body">
                        <form data-toggle="validator" role="form" method="post" action="<c:url value="/Inscription"/>">
                            <fieldset>
                                <div class="form-group has-feedback">
                                    <label for="nom" class="control-label">Nom : <span class="requis">*</span></label>
                                    <input type="text" name="nom" class="form-control" id="nom" autofocus required/>
                                    <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                    <div class="help-block with-errors"></div>
                                </div>

                                <div class="form-group has-feedback">
                                    <label for="prenom" class="control-label">Prenom : <span
                                            class="requis">*</span></label>
                                    <input type="text" name="prenom" class="form-control" id="prenom" required/>
                                    <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                    <div class="help-block with-errors"></div>
                                </div>

                                <div class="form-group has-feedback">
                                    <label for="identifiant" class="control-label">Identifiant : <span
                                            class="requis">*</span></label>
                                    <input type="text" name="identifiant" class="form-control" id="identifiant"
                                           required/>
                                    <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                    <div class="help-block with-errors"></div>
                                </div>

                                <div class="form-group has-feedback">
                                    <label for="motDePasse" class="control-label">Mot de passe : <span
                                            class="requis">*</span></label>
                                    <input type="password" name="motDePasse" class="form-control" id="motDePasse"
                                           required/>
                                    <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                    <div class="help-block with-errors"></div>
                                </div>

                                <div class="form-group has-feedback">
                                    <label for="email" class="control-label">Adresse e-mail : <span
                                            class="requis">*</span></label>
                                    <div class="input-group">
                                        <span class="input-group-addon">@</span>
                                        <input type="email" name="email" class="form-control" id="email" required/>
                                    </div>
                                    <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                    <div class="help-block with-errors"></div>
                                </div>

                                <div class="text-center">
                                    <c:choose>
                                        <c:when test="${empty erreurs}">
                                            <span class="succes">${resultat}</span>
                                        </c:when>
                                        <c:otherwise>
                                            <span class="erreur">${resultat} ${erreurs['identifiant']} ${erreurs['email']}</span>
                                        </c:otherwise>
                                    </c:choose>
                                </div>
                                <br>

                                <div class="text-center">
                                    <button type="submit" class="btn btn-primary" id="submitUtilisateur">
                                        <i class="fas fa-plus fa-fw" aria-hidden="true"></i>&nbsp; S'inscrire
                                    </button>
                                    <button type="reset" class="btn btn-primary" id="reset">
                                        <i class="fas fa-redo fa-fw" aria-hidden="true"></i>&nbsp; Remettre à zéro
                                    </button>
                                </div>
                                <br>
                            </fieldset>
                        </form>
                        <p class="text-center">
                            <em>Lien vers le formulaire de connexion : </em>
                            <a href="<c:url value="/Connexion"/>">Connexion</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<c:import url="/inc/scripts.jsp"/>
<script src="<c:url value="/js/jquery.typeahead.js"/>" type="text/javascript"></script>


</body>
</html>