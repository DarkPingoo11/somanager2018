<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<c:import url="/inc/head.jsp" />
<title>SoManager - Gérer les Jurys</title>
<link rel="stylesheet" href="<c:url value="/css/jquery.typeahead.css"/>">
<link rel="stylesheet" media="screen"
	href="<c:url value="/css/bootstrap-datetimepicker.min.css"/>">
<link rel="stylesheet" href="<c:url value="/css/popup.css"/>">
</head>
<body>
	<div class="wrapper">
		<c:import url="/Navbar" />
		<!-- Page Content Holder -->
		<div id="content">
			<c:import url="/inc/headerForSidebar.jsp" />


			<div class="panel panel-primary ">
				<div class="panel-heading">Jurys - Gérer les Jurys</div>

				<div class="panel-body">

					<c:import url="/inc/callback.jsp" />

					<ul class="nav nav-tabs" role="tablist" style="margin-bottom: 20px">
						<li
							<c:if test="${onglet eq 'jurySprintReview' or  empty onglet }">class="active"</c:if>>
							<a data-toggle="tab" href="#jurySprintReview"> <i
								class="fas fa-file-alt fa-fw" aria-hidden="true"></i>&nbsp; Jury
								de sprint Review
						</a>
						</li>
						<li
							<c:if test="${onglet eq 'jurySprintDemo' }">class="active"</c:if>>
							<a data-toggle="tab" href="#jurySprintDemo"> <i
								class="fas fa-laptop fa-fw" aria-hidden="true"></i>&nbsp; Jury
								de sprint Démonstration
						</a>
						</li>
					</ul>

					<%-- Contenu des différentes tab --%>
					<div class="tab-content">

						<%-- Gérer les jurys de soutenance --%>
						<div id="jurySprintReview"
							class="tab-pane fade <c:if test="${onglet eq 'jurySprintReview' or  empty onglet }">in active</c:if>">
							<div class="row">
								<c:import
									url="/WEB-INF/formulaires/gererJurys/jurySprintReview.jsp" />
							</div>
						</div>

						<%-- Gérer les jurys de poster --%>
						<div id="jurySprintDemo"
							" class="tab-pane fade <c:if test="${onglet eq 'jurySprintDemo' }">in active</c:if>">
							<div class="row">
								<c:import
									url="/WEB-INF/formulaires/gererJurys/jurySprintDemo.jsp" />
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>

	<script src="<c:url value="/js/dragAndDrop.js"/>"></script>
	<c:import url="/inc/scripts.jsp" />
	<script src="<c:url value="/js/jquery.typeahead.js"/>"
		type="text/javascript"></script>
	<script type="application/javascript"
		src="<c:url value="/js/perfect-scrollbar.min.js"/>"></script>
	<script type="text/javascript"
		src="<c:url value="/js/bootstrap-datetimepicker.min.js"/>"></script>
	<script type="text/javascript"
		src="<c:url value="/js/bootstrap-datetimepicker.fr.js"/>"></script>
	<script>
	new PerfectScrollbar('#' + 'listeMembresJuryDemoContainer');
	new PerfectScrollbar('#' + 'listeMembresJuryReviewContainer');
		typeof $.typeahead === 'function' && $.typeahead({
			input : ".js-typeahead",
			minLength : 1,
			maxItem : 5,
			order : "asc",
			hint : false,
			emptyTemplate : 'Aucun resultat pour "{{query}}"',
			source : data
		});
	</script>
</body>
</html>

