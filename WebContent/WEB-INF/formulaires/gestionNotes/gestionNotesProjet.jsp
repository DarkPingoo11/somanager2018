

<%@ page language="java" contentType="text/html; charset=UTF-8"
		 pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<
<div class="container">

	

		<div class="panel-body">
			<div class="col-lg-7 col-md-7">
				<div class="panel panel-default ">
					<div class="panel-heading" align="center">Notes manquantes :</div>
					<div class="panel-body">
						<c:if test="${!empty etuManq}">
							<table class="table table-condensed">
								<thead>
								<tr>
									<th>Nom, Prenom de l'étudiant</th>
									<th>Nom, Prenom du professeur</th>
									<th>Note Manquante</th>
								</tr>
								</thead>
								<tbody>
								<c:forEach var="etu" items="${etuManq}" varStatus="loop">
									<tr>
										<td><c:out value="${etu.nom}, ${etu.prenom}"/></td>
										<td><c:out value="${profManq[loop.index].nom}, ${profManq[loop.index].prenom}"/></td>
										<td><c:out value="${noteManq[loop.index]}"/></td>
									</tr>
								</c:forEach>
								</tbody>
							</table>
						</c:if>
						<c:if test="${empty etuManq}">
							<p align="center">Aucunes notes disponibles</p>
						</c:if>
					</div>
				</div>
			</div>
			<div class="col-lg-5 col-md-5">
				<div class="panel panel-default ">
					<div class="panel-heading" align="center">Exporter les notes :</div>
					<div class="panel-body">
						<form method="post" action="<c:url value="/GererNotes"/>" id="telecharger">
							<div class="col-lg-12 col-md-12">
								<div class="checkbox col-lg-offset-1 col-md-offset-1 col-sm-offset-1">
									<c:choose>
										<c:when test="${empty dispoNotesIntermediaire}">
											<input type="checkbox" name="notesIntermediaire" id="notesIntermediaire" value="notesIntermediaire" disabled/>
											<label for="notesIntermediaire"><strong>Notes Intermédiaire</strong></label>
											<p></p>
											<span class="erreur">
													<p> Aucunes notes disponibles </p>
												</span>
										</c:when>
										<c:otherwise>
											<input type="checkbox" name="notesIntermediaire" id="notesIntermediaire" value="notesIntermediaire" />
											<label for="notesIntermediaire"><strong>Notes Intermédiaire</strong></label>
											<p></p>
											<c:if test="${!empty toutesNotesIntermediaire}">
													<span class="erreur">
														<p> Attention il manque une note pour un ou plusieurs étudiant(s) </p>
													</span>
											</c:if>
										</c:otherwise>
									</c:choose>
								</div>
							</div>

							<div class="col-lg-12 col-md-12">
								<div class="checkbox col-lg-offset-1 col-md-offset-1 col-sm-offset-1">
									<c:choose>
										<c:when test="${empty dispoNotesProjet}">
											<input type="checkbox" name="notesProjet" id="notesProjet" value="notesProjet" disabled/>
											<label for="notesProjet"><strong>Notes Intermédiaire</strong></label>
											<p></p>
											<span class="erreur">
													<p> Aucunes notes disponibles </p>
												</span>
										</c:when>
										<c:otherwise>
											<input type="checkbox" name="notesProjet" id="notesProjet" value="notesProjet" />
											<label for="notesProjet"><strong>Notes de Projet</strong></label>
											<p></p>
											<c:if test="${!empty toutesNotesProjet}">
													<span class="erreur">
														<p> Attention il manque une note pour un ou plusieurs étudiant(s) </p>
													</span>
											</c:if>
										</c:otherwise>
									</c:choose>
								</div>
							</div>
							<div class="col-lg-12 col-md-12">
								<div class="checkbox col-lg-offset-1 col-md-offset-1 col-sm-offset-1">
									<c:choose>
										<c:when test="${empty dispoNotesPoster}">
											<input type="checkbox" name="notesPoster" id="notesPoster" value="notesPoster" disabled/>
											<label for="notesPoster"><strong>Notes de Poster</strong></label>
											<p></p>
											<span class="erreur">
													<p> Aucunes notes disponibles </p>
												</span>
										</c:when>
										<c:otherwise>
											<input type="checkbox" name="notesPoster" id="notesPoster" value="notesPoster" />
											<label for="notesPoster"><strong>Notes de Poster</strong></label>
											<p></p>
											<c:if test="${!empty toutesNotesPoster}">
													<span class="erreur">
														<p> Attention il manque une note pour un ou plusieurs étudiant(s) </p>
													</span>
											</c:if>
										</c:otherwise>
									</c:choose>
								</div>
							</div>
							<div class="col-lg-12 col-md-12">
								<div class="checkbox col-lg-offset-1 col-md-offset-1 col-sm-offset-1">
									<c:choose>
										<c:when test="${empty dispoNotesSoutenance}">
											<input type="checkbox" name="notesSoutenance" id="notesSoutenance" value="notesSoutenance" disabled/>
											<label for="notesSoutenance"><strong>Notes de Soutenance</strong></label>
											<p></p>
											<span class="erreur">
													<p> Aucunes notes disponibles </p>
												</span>
										</c:when>
										<c:otherwise>
											<input type="checkbox" name="notesSoutenance" id="notesSoutenance" value="notesSoutenance" />
											<label for="notesSoutenance"><strong>Notes de Soutenance</strong></label>
											<p></p>
											<c:if test="${!empty toutesNotesSoutenance}">
													<span class="erreur">
														<p> Attention il manque une note pour un ou plusieurs étudiant(s) </p>
													</span>
											</c:if>
										</c:otherwise>
									</c:choose>
								</div>
							</div>
							<div class="text-center">
								<button type="submit" class="btn btn-primary center-block" id="telecharger">
									<i class="fas fa-download fa-fw" aria-hidden="true"></i> Télécharger
								</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
</div>
