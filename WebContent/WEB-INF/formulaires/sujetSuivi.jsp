<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <c:import url="/inc/head.jsp"/>
    <title>SoManager - Sujets Suivis</title>
</head>
<body>
<div class="wrapper">
    <c:import url="/Navbar"/>
    <!-- Page Content Holder -->
    <div id="content">
        <c:import url="/inc/headerForSidebar.jsp"/>

        <div class="panel panel-primary ">
            <div class="panel-heading">Sujet - Sujet(s) Suivi(s)</div>
            <div class="panel-body">
                <c:choose>
                    <c:when test="${empty listeSujets}">
                        <c:if test="${prof eq 'true'||profOption eq 'true' || profResponsable eq 'true' || entrepriseExt eq 'true'}">
                            <p align="center"><strong>Vous ne suivez actuellement aucun sujet ou le professeur
                                responsable n'a pas validé cette demande </strong></p>
                            <br>
                            <p align="center"><strong>Vous pouvez postuler pour suivre un projet </strong><a
                                    style="font-size: 16px;" href="<c:url value="/InscriptionProfSujet"/>">ici</a></p>
                        </c:if>
                        <c:if test="${etudiant eq 'true'}">
                            <p align="center"><strong>Vous ne faites partie d'aucune équipe ou le professeur référent
                                n'a pas validé la demande </strong></p>
                            <br>
                            <p align="center"><strong>Vous pouvez créer une équipe </strong><a style="font-size: 16px;"
                                                                                               href="<c:url value="/CreerEquipe"/>">ici</a>
                            </p>
                        </c:if>
                    </c:when>
                    <c:otherwise>
                        <c:forEach var="i" begin="0" end="${listeSujets.size()-1}">
                            <div class="panel panel-default ">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-lg-8 col-md-8">
                                            <div class="list-group">

                                                <a class="list-group-item active">
                                                    <h5 class="list-group-item-heading">Nom du sujet :</h5>
                                                    <h4 class="list-group-item-text">
                                                        <c:out value="${listeSujets.get(i).titre}"/>
                                                    </h4>
                                                </a>
                                                <c:if test="${prof eq 'true'||profOption eq 'true' || profResponsable eq 'true' || entrepriseExt eq 'true'}">
                                                    <a class="list-group-item">
                                                        <h4 class="list-group-item-heading">Mon rôle :</h4>
                                                        <h5 class="list-group-item-text">
                                                            <c:out value="${listeFonctions.get(i)}"/>
                                                        </h5>
                                                    </a>
                                                </c:if>

                                                <a class="list-group-item">
                                                    <div id="description-${loop.index}">
                                                        <h4 class="list-group-item-heading">Description :</h4>
                                                        <h5 class="list-group-item-text">
                                                            <c:out value="${listeSujets.get(i).description}"/>
                                                        </h5>
                                                    </div>
                                                </a>
                                                <a class="list-group-item">
                                                    <h4 class="list-group-item-heading">Etat :</h4>
                                                    <h5 class="list-group-item-text">
                                                        <c:out value="${listeSujets.get(i).etat}"/>
                                                    </h5>
                                                </a>
                                                <c:if test="${etudiantEquipe eq 'true'}">
                                                    <c:set var="listeLiens"
                                                           value="${fn:split(listeSujets.get(i).liens, ';')}"/>
                                                    <c:if test="${fn:length(listeLiens)>0 && listeLiens[0]!=''}">
                                                        <div class="list-group-item">
                                                            <h4 class="list-group-item-heading">Liens utiles :</h4>
                                                            <c:forEach var="lienSujet" items="${listeLiens}">
                                                                <a href="${lienSujet}" target="_blank"
                                                                   class="list-group-item"
                                                                   style="overflow: hidden;text-overflow: ellipsis;  "><i
                                                                        class="fas fa-link fa-fw"
                                                                        aria-hidden="true"></i>&nbsp; <c:out
                                                                        value="${lienSujet}"/></a>
                                                            </c:forEach>
                                                        </div>
                                                    </c:if>
                                                    <a class="list-group-item">
                                                        <h4 class="list-group-item-heading">Nombre d'étudiants :</h4>
                                                        <h5 class="list-group-item-text">Entre <c:out
                                                                value="${listeSujets.get(i).nbrMinEleves}"/> et <c:out
                                                                value="${listeSujets.get(i).nbrMaxEleves}"/></h5>
                                                    </a>

                                                    <a class="list-group-item">
                                                        <h4 class="list-group-item-heading">Intérêts :</h4>
                                                        <h5 class="list-group-item-text"><c:out
                                                                value="${listeSujets.get(i).interets}"/></h5>
                                                    </a>
                                                </c:if>

                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-4">
                                            <div class="row">
                                                <div class="col-lg-12 col-md-12">
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading">
                                                            <div class="panel-title ">
                                                                <a style="font-size: 14px" data-toggle="collapse"
                                                                   href="#${listeSujets.get(i).idSujet}"><i
                                                                        class="fas fa-users fa-fw"
                                                                        aria-hidden="true"></i>&nbsp; Membres de
                                                                    l'équipe<span
                                                                            class="glyphicon glyphicon-chevron-down pull-right"></span>
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <div id="${listeSujets.get(i).idSujet}"
                                                             class="panel-collapse collapse">
                                                            <ul class="list-group">
                                                                <c:choose>
                                                                    <c:when test="${!empty utilisateursSurLeSujet[listeSujets[i].idSujet]}">

                                                                        <c:forEach var="utilisateur"
                                                                                   items="${utilisateursSurLeSujet[listeSujets[i].idSujet]}"
                                                                                   varStatus="loop">
                                                                            <li class="list-group-item list-group-item-default">
                                                                                <a href="mailto:${utilisateur.email}"
                                                                                   style="text-decoration:none;"><i
                                                                                        class="fas fa-caret-right fa-fw"
                                                                                        aria-hidden="true"></i>&nbsp; ${utilisateur.nom} ${utilisateur.prenom}
                                                                                </a>
                                                                            </li>
                                                                        </c:forEach>
                                                                    </c:when>
                                                                    <c:otherwise>
                                                                        <li class="list-group-item list-group-item-default">
                                                                            <div class="alert alert-warning"
                                                                                 role="alert" align="center">Aucune
                                                                                équipe n'a encore été créée
                                                                            </div>
                                                                        </li>
                                                                    </c:otherwise>
                                                                </c:choose>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 col-md-12">
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading">
                                                            <div class="panel-title ">
                                                                <a style="font-size: 14px" data-toggle="collapse"
                                                                   href="#${listeSujets.get(i).idSujet}Prof"><i
                                                                        class="fas fa-users fa-fw"
                                                                        aria-hidden="true"></i>&nbsp; Professeurs
                                                                    concernés<span
                                                                            class="glyphicon glyphicon-chevron-down pull-right"></span>
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <div id="${listeSujets.get(i).idSujet}Prof"
                                                             class="panel-collapse collapse">
                                                            <ul class="list-group">
                                                                <c:choose>
                                                                    <c:when test="${!empty professeursSurLeSujet[listeSujets[i].idSujet]}">
                                                                        <c:forEach var="entree"
                                                                                   items="${professeursSurLeSujet[listeSujets[i].idSujet]}"
                                                                                   varStatus="loop">
                                                                            <li class="list-group-item list-group-item-default">
                                                                                <i class="fas fa-caret-right fa-fw"
                                                                                   aria-hidden="true"></i>&nbsp; ${entree}
                                                                            </li>
                                                                        </c:forEach>
                                                                    </c:when>
                                                                    <c:otherwise>
                                                                        <li class="list-group-item list-group-item-default">
                                                                            <div class="alert alert-warning"
                                                                                 role="alert" align="center">Aucun
                                                                                professeur n'est attribué à ce suejt
                                                                            </div>
                                                                        </li>
                                                                    </c:otherwise>
                                                                </c:choose>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 col-md-12">
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading">
                                                            <div class="panel-title ">
                                                                <a style="font-size: 14px" data-toggle="collapse"
                                                                   href="#${listeSujets.get(i).idSujet}Poster"><span
                                                                        class="glyphicon glyphicon-blackboard"></span>
                                                                    Poster<span
                                                                            class="glyphicon glyphicon-chevron-down pull-right"></span>
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <div id="${listeSujets.get(i).idSujet}Poster"
                                                             class="panel-collapse collapse">
                                                            <ul class="list-group">
                                                                <c:choose>
                                                                    <c:when test="${!empty posterSurLeSujet[listeSujets[i].idSujet]}">
                                                                        <!-- Quand le poster a été déposé !-->

                                                                        <c:if test="${posterSurLeSujet[listeSujets[i].idSujet].valide=='non'}">
                                                                            <!-- Quand le poster a été déposé mais non validé !-->
                                                                            <li class="list-group-item list-group-item-default">
                                                                                <div class="alert alert-warning"
                                                                                     role="alert">Le poster a été déposé
                                                                                    le ${posterSurLeSujet[listeSujets[i].idSujet].date}
                                                                                    mais n'est pas encore validé par le
                                                                                    professeur référent
                                                                                </div>
                                                                            </li>
                                                                            <li class="list-group-item list-group-item-default">
                                                                                <form action="<c:url value="/TelechargerPoster?action=afficher"/>"
                                                                                      method="post">
                                                                                    <input type="hidden" name="idSujet"
                                                                                           id="idSujet"
                                                                                           value="<c:out value="${listeSujets[i].idSujet}" />"/>
                                                                                    <button type="submit"
                                                                                            class="btn btn-primary center-block"
                                                                                            id="submit">
                                                                                        <i class="fas fa-image fa-fw"
                                                                                           aria-hidden="true"></i>&nbsp;
                                                                                        Afficher
                                                                                    </button>
                                                                                </form>
                                                                            </li>
                                                                        </c:if>

                                                                        <c:if test="${posterSurLeSujet[listeSujets[i].idSujet].valide=='non' && listeFonctions.get(i).fonction=='référent' }">
                                                                            <!-- Quand le poster a été déposé mais non validé et prof referent connecté !-->
                                                                            <form method="post" id="validerPoster"
                                                                                  action="<c:url value="/SujetSuivi"/>">

                                                                                <br>

                                                                                <input type="hidden" name="formulaire"
                                                                                       id="formulaire"
                                                                                       value="validerPoster"/>
                                                                                <input type="hidden"
                                                                                       name="idPosterChoisi"
                                                                                       id="idPosterChoisi"
                                                                                       value="<c:out value="${posterSurLeSujet[listeSujets[i].idSujet].idPoster}" />"/>
                                                                                <input type="hidden"
                                                                                       name="idSujetChoisi"
                                                                                       id="idSujetChoisi"
                                                                                       value="<c:out value="${listeSujets[i].idSujet}" />"/>

                                                                                <button type="submit"
                                                                                        class="btn btn-success center-block"
                                                                                        type="submit">
                                                                                    <i class="far fa-check-circle fa-fw"
                                                                                       aria-hidden="true"></i>&nbsp;
                                                                                    Valider
                                                                                </button>
                                                                                <br>
                                                                            </form>
                                                                        </c:if>

                                                                        <c:if test="${posterSurLeSujet[listeSujets[i].idSujet].valide=='non' && listeFonctions.get(i).fonction=='INTERESSE' && entrepriseExt eq 'true' }">
                                                                            <!-- Quand le poster a été déposé mais non validé et entreprise exterieur connecté !-->
                                                                            <form method="post" id="validerPoster"
                                                                                  action="<c:url value="/SujetSuivi"/>">

                                                                                <br>

                                                                                <input type="hidden" name="formulaire"
                                                                                       id="formulaire"
                                                                                       value="validerPoster"/>
                                                                                <input type="hidden"
                                                                                       name="idPosterChoisi"
                                                                                       id="idPosterChoisi"
                                                                                       value="<c:out value="${posterSurLeSujet[listeSujets[i].idSujet].idPoster}" />"/>
                                                                                <input type="hidden"
                                                                                       name="idSujetChoisi"
                                                                                       id="idSujetChoisi"
                                                                                       value="<c:out value="${listeSujets[i].idSujet}" />"/>

                                                                                <button type="submit"
                                                                                        class="btn btn-success center-block"
                                                                                        type="submit">
                                                                                    <i class="far fa-check-circle fa-fw"
                                                                                       aria-hidden="true"></i>&nbsp;
                                                                                    Valider
                                                                                </button>
                                                                                <br>
                                                                            </form>
                                                                        </c:if>
                                                                        <c:if test="${posterSurLeSujet[listeSujets[i].idSujet].valide=='oui'}">
                                                                            <!-- Quand le poster a été déposé validé -->
                                                                            <li class="list-group-item list-group-item-default">
                                                                                <div class="alert alert-success"
                                                                                     role="alert">Le poster a été déposé
                                                                                    le ${posterSurLeSujet[listeSujets[i].idSujet].date}
                                                                                    et a été validé par le professeur
                                                                                    référent
                                                                                </div>
                                                                            </li>

                                                                            <li class="list-group-item list-group-item-default">
                                                                                <form action="<c:url value="/TelechargerPoster?action=telecharger"/>"
                                                                                      method="post">
                                                                                    <input type="hidden" name="idSujet"
                                                                                           id="idSujet"
                                                                                           value="<c:out value="${listeSujets[i].idSujet}" />"/>
                                                                                    <button type="submit"
                                                                                            class="btn btn-primary center-block"
                                                                                            id="submit">
                                                                                        <i class="fas fa-download fa-fw"
                                                                                           aria-hidden="true"></i>&nbsp;
                                                                                        Télécharger
                                                                                    </button>
                                                                                </form>
                                                                                <br>
                                                                                <form action="<c:url value="/TelechargerPoster?action=afficher"/>"
                                                                                      method="post">
                                                                                    <input type="hidden" name="idSujet"
                                                                                           id="idSujet"
                                                                                           value="<c:out value="${listeSujets[i].idSujet}" />"/>
                                                                                    <button type="submit"
                                                                                            class="btn btn-primary center-block"
                                                                                            id="submit">
                                                                                        <i class="fas fa-image fa-fw"
                                                                                           aria-hidden="true"></i>&nbsp;
                                                                                        Afficher
                                                                                    </button>
                                                                                </form>
                                                                            </li>
                                                                        </c:if>
                                                                        <c:if test="${dateLimitePassee eq 'false'}">
                                                                            <!-- Quand le poster a été déposé validé et que la date de dépot n'est pas passée -->
                                                                            <li class="list-group-item list-group-item-default">
                                                                                <p align="center">Vous pouvez encore
                                                                                    modifier le poster : </p>
                                                                                <form action="<c:url value="/ModifierPoster"/>"
                                                                                      method="post"
                                                                                      enctype="multipart/form-data">
                                                                                    <input type="file"
                                                                                           accept="application/pdf"
                                                                                           id="newPoster"
                                                                                           name="newPoster"/>
                                                                                    <br>
                                                                                    <button type="submit"
                                                                                            class="btn btn-primary center-block"
                                                                                            id="submit">
                                                                                        <i class="fas fa-pencil-alt fa-fw"
                                                                                           aria-hidden="true"></i>&nbsp;
                                                                                        Modifier
                                                                                    </button>
                                                                                </form>
                                                                            </li>
                                                                        </c:if>
                                                                    </c:when>
                                                                    <c:otherwise>
                                                                        <li class="list-group-item list-group-item-default">
                                                                            <div class="alert alert-warning"
                                                                                 role="alert" align="center">Aucun
                                                                                poster n'a été déposé pour ce sujet
                                                                            </div>
                                                                        </li>

                                                                        <c:if test="${dateLimitePassee eq 'true'}">
                                                                            <li class="list-group-item list-group-item-default">
                                                                                <div class="alert alert-danger"
                                                                                     role="alert" align="center">La date
                                                                                    limite du dépôt de poster est
                                                                                    dépassée vous ne pouvez plus en
                                                                                    déposer
                                                                                </div>
                                                                            </li>
                                                                        </c:if>

                                                                        <c:if test="${dateLimitePassee eq 'false'}">
                                                                            <li class="list-group-item list-group-item-default">
                                                                                <form action="<c:url value="/DeposerPoster"/>"
                                                                                      method="post"
                                                                                      enctype="multipart/form-data">
                                                                                    <p align="center"> Déposer mon
                                                                                        poster</p>
                                                                                    <label for="fichier">Emplacement du
                                                                                        poster <span
                                                                                                class="requis">*</span></label>
                                                                                    <input type="file"
                                                                                           accept="application/pdf"
                                                                                           id="poster" name="poster"/>
                                                                                    <input type="hidden"
                                                                                           name="dateLimiteDepot"
                                                                                           value="${sujetPFE.idSujet}"/>
                                                                                    <span class="succes"><c:out
                                                                                            value="${fichier}"/></span>
                                                                                    <br/>
                                                                                    <button type="submit"
                                                                                            class="btn btn-primary center-block"
                                                                                            id="submit">
                                                                                        <i class="fas fa-plus fa-fw"
                                                                                           aria-hidden="true"></i>&nbsp;
                                                                                        Déposer
                                                                                    </button>
                                                                                </form>
                                                                            </li>
                                                                        </c:if>
                                                                    </c:otherwise>
                                                                </c:choose>
                                                                <c:if test="${etudiantEquipe eq 'false'}">
                                                                    <c:choose>
                                                                        <c:when test="${!empty informationsJuryPoster[listeSujets[i].idSujet].get(0)}">

                                                                            <li class="list-group-item list-group-item-default">
                                                                                <i class="fas fa-caret-right fa-fw"
                                                                                   aria-hidden="true"></i>&nbsp; Jury 1
                                                                                : ${informationsJuryPoster[listeSujets[i].idSujet].get(0)}
                                                                            </li>
                                                                            <li class="list-group-item list-group-item-default">
                                                                                <i class="fas fa-caret-right fa-fw"
                                                                                   aria-hidden="true"></i>&nbsp; Jury 2
                                                                                : ${informationsJuryPoster[listeSujets[i].idSujet].get(1)}
                                                                            </li>
                                                                            <li class="list-group-item list-group-item-default">
                                                                                <i class="fas fa-caret-right fa-fw"
                                                                                   aria-hidden="true"></i>&nbsp; Jury 3
                                                                                : ${informationsJuryPoster[listeSujets[i].idSujet].get(2)}
                                                                            </li>
                                                                        </c:when>
                                                                        <c:otherwise>
                                                                            <li class="list-group-item list-group-item-default">
                                                                                <div class="alert alert-warning"
                                                                                     role="alert" align="center">Aucun
                                                                                    jury n'est encore programmé pour
                                                                                    noter le poster
                                                                                </div>
                                                                            </li>
                                                                        </c:otherwise>
                                                                    </c:choose>
                                                                </c:if>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 col-md-12">
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading">
                                                            <div class="panel-title ">
                                                                <a style="font-size: 14px" data-toggle="collapse"
                                                                   href="#${listeSujets.get(i).idSujet}Evaluation"><i
                                                                        class="fas fa-graduation-cap fa-fw"
                                                                        aria-hidden="true"></i>&nbsp; Soutenance<span
                                                                        class="glyphicon glyphicon-chevron-down pull-right"></span>
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <div id="${listeSujets.get(i).idSujet}Evaluation"
                                                             class="panel-collapse collapse">
                                                            <ul class="list-group">
                                                                <c:choose>
                                                                    <c:when test="${!empty informationsSoutenance[listeSujets[i].idSujet].get(0)}">

                                                                        <li class="list-group-item list-group-item-default">
                                                                            <p align="center"><i
                                                                                    class="far fa-calendar-alt fa-fw"
                                                                                    aria-hidden="true"></i>&nbsp; Date
                                                                                Soutenance
                                                                                : ${informationsSoutenance[listeSujets[i].idSujet].get(0)}
                                                                            </p>
                                                                        </li>
                                                                        <c:choose>
                                                                            <c:when test="${empty informationsSoutenance[listeSujets[i].idSujet].get(1) && empty informationsSoutenance[listeSujets[i].idSujet].get(2)}">
                                                                                <li class="list-group-item list-group-item-default">
                                                                                    <div class="alert alert-warning"
                                                                                         role="alert" align="center">Le
                                                                                        jury de la soutenance n'est pas
                                                                                        encore défini
                                                                                    </div>
                                                                                </li>
                                                                            </c:when>
                                                                            <c:otherwise>
                                                                                <li class="list-group-item list-group-item-default">
                                                                                    <i class="fas fa-caret-right fa-fw"
                                                                                       aria-hidden="true"></i>&nbsp;
                                                                                    Jury 1
                                                                                    : ${informationsSoutenance[listeSujets[i].idSujet].get(1)}
                                                                                </li>
                                                                                <li class="list-group-item list-group-item-default">
                                                                                    <i class="fas fa-caret-right fa-fw"
                                                                                       aria-hidden="true"></i>&nbsp;
                                                                                    Jury 2
                                                                                    : ${informationsSoutenance[listeSujets[i].idSujet].get(2)}
                                                                                </li>
                                                                            </c:otherwise>
                                                                        </c:choose>
                                                                    </c:when>
                                                                    <c:otherwise>
                                                                        <li class="list-group-item list-group-item-default">
                                                                            <div class="alert alert-warning"
                                                                                 role="alert" align="center">Aucune
                                                                                soutenance n'est programmée pour ce
                                                                                sujet
                                                                            </div>
                                                                        </li>
                                                                    </c:otherwise>
                                                                </c:choose>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 col-md-12">
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading">
                                                            <div class="panel-title ">
                                                                <a style="font-size: 14px" data-toggle="collapse"
                                                                   href="#${listeSujets.get(i).idSujet}Date"><i
                                                                        class="far fa-calendar-alt fa-fw"
                                                                        aria-hidden="true"></i>&nbsp; Dates
                                                                    importantes<span
                                                                            class="glyphicon glyphicon-chevron-down pull-right"></span>
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <div id="${listeSujets.get(i).idSujet}Date"
                                                             class="panel-collapse collapse">
                                                            <ul class="list-group">
                                                                <c:forEach var="entree"
                                                                           items="${informationsAnneeScolaire[listeSujets[i].idSujet]}"
                                                                           varStatus="loop">
                                                                    <li class="list-group-item list-group-item-default">

                                                                        <table class="table table-striped table-hover">

                                                                            <caption>Dates pour l'option :
                                                                                <c:forEach var="optionESEO"
                                                                                           items="${informationsOptionsSujet[listeSujets[i].idSujet]}">
                                                                                    <c:if test="${optionESEO.idOption==entree.idOption}">
                                                                                        ${optionESEO.nomOption}
                                                                                    </c:if>
                                                                                </c:forEach>
                                                                            </caption>
                                                                            <tr>
                                                                                <td align="center">Date de début de
                                                                                    projet
                                                                                </td>
                                                                                <td><fmt:formatDate pattern="dd/MM/yyyy"
                                                                                                    value="${entree.dateDebutProjet}"/>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="center">Date de
                                                                                    mi-avancement
                                                                                </td>
                                                                                <td><fmt:formatDate pattern="dd/MM/yyyy"
                                                                                                    value="${entree.dateMiAvancement}"/>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="center">Date de dépot de
                                                                                    poster (indicatif)
                                                                                </td>
                                                                                <td><fmt:formatDate pattern="dd/MM/yyyy"
                                                                                                    value="${entree.dateDepotPoster}"/>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="center">Date soutenance
                                                                                    (indicatif)
                                                                                </td>
                                                                                <td><fmt:formatDate pattern="dd/MM/yyyy"
                                                                                                    value="${entree.dateSoutenanceFinale}"/>
                                                                            </tr>
                                                                        </table>
                                                                    </li>
                                                                </c:forEach>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </c:forEach>
                    </c:otherwise>
                </c:choose>
            </div>
        </div>
    </div>
</div>
<c:import url="/inc/scripts.jsp"/>


</body>
</html>