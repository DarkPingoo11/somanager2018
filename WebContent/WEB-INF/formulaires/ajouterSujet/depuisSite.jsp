<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<div class="row">
    <div class="col-sm-12">
        <div class="col-sm-12">
            <form data-toggle="validator" role="form" method="post" action="<c:url value="/AjouterSujet"/>"
                  id="valider">
                <fieldset>
                    <div class="form-group has-feedback">
                        <label for="titre" class="control-label">Titre : <span
                                class="requis">*</span></label>
                        <input type="text" name="titre" class="form-control" id="titre" autofocus required/>
                        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                        <div class="help-block with-errors"></div>
                    </div>

                    <div class="form-group has-feedback">
                        <label for="description" class="control-label">Description : <span
                                class="requis">*</span></label>
                        <textarea name="description" class="form-control" id="description"
                                  required/></textarea>
                        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                        <div class="help-block with-errors"></div>
                    </div>

                    <label for="porteur">Porteur du projet :</label>
                    <input type="text" name="porteur" class="form-control" id="porteur"
                           value="<c:out value="${sessionScope.utilisateur.nom} ${sessionScope.utilisateur.prenom}"/>"
                           disabled="disabled"/>
                    <br>

                    <div class="form-group has-feedback">
                        <label for="nbrMinEleves" class="control-label">Nombre minimum d'élèves : <span
                                class="requis">*</span></label>
                        <input type="number" name="nbrMinEleves" class="form-control" id="nbrMinEleves"
                               min="1" step="1" required/>
                        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                        <div class="help-block with-errors"></div>
                    </div>

                    <div class="form-group has-feedback">
                        <label for="nbrMaxEleves" class="control-label">Nombre maximum d'élèves : <span
                                class="requis">*</span></label>
                        <input type="number" name="nbrMaxEleves" class="form-control" id="nbrMaxEleves"
                               min="1" step="1" required/>
                        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                        <div class="help-block with-errors"></div>
                    </div>

                    <div class="form-group has-feedback">
                        <fieldset>
                            <label for="contratPro" class="control-label">Est-ce un sujet de contrat pro ?
                                <span class="requis">*</span></label>
                            <div class="col-lg-12">
                                <div class="radio col-lg-offset-1 col-md-offset-1 col-sm-offset-1">
                                    <input type="radio" name="contratPro" id="contratProVrai" value="true"
                                           data-toggle="modal" data-target="#modalContratPro" required/>
                                    <label for="contratProVrai"><strong>Oui</strong></label>
                                </div>
                                <div class="radio col-lg-offset-1 col-md-offset-1 col-sm-offset-1">
                                    <input type="radio" name="contratPro" id="contratProFaux" value="false"
                                           required/>
                                    <label for="contratProFaux"><strong>Non</strong></label>
                                </div>
                            </div>
                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                            <div class="help-block with-errors"></div>
                        </fieldset>
                    </div>

                    <div class="form-group has-feedback">
                        <fieldset>
                            <label for="confidentialite" class="control-label">Est-ce un sujet confidentiel
                                ? <span class="requis">*</span></label>
                            <div class="col-lg-12">
                                <div class="radio col-lg-offset-1 col-md-offset-1 col-sm-offset-1">
                                    <input type="radio" name="confidentialite" id="confidentialiteVrai"
                                           value="true" required/>
                                    <label for="confidentialiteVrai"><strong>Oui</strong></label>
                                </div>
                                <div class="radio col-lg-offset-1 col-md-offset-1 col-sm-offset-1">
                                    <input type="radio" name="confidentialite" id="confidentialiteFaux"
                                           value="false" required/>
                                    <label for="confidentialiteFaux"><strong>Non</strong></label>
                                </div>
                            </div>
                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                            <div class="help-block with-errors"></div>
                        </fieldset>
                    </div>

                    <fieldset>
                        <label class="control-label">Option(s) concernée(s) : <span class="requis">*</span></label>
                        <div class="col-lg-12">
                            <div class="checkbox col-lg-offset-1 col-md-offset-1 col-sm-offset-1">
                                <input type="checkbox" name="CC" id="CC" value="CC"/>
                                <label for="CC"><strong>Cloud Computing</strong></label>
                            </div>
                            <div class="checkbox col-lg-offset-1 col-md-offset-1 col-sm-offset-1">
                                <input type="checkbox" name="IIT" id="IIT" value="IIT"/>
                                <label for="IIT"><strong>Infrastructures IT</strong></label>
                            </div>
                            <div class="checkbox col-lg-offset-1 col-md-offset-1 col-sm-offset-1">
                                <input type="checkbox" name="LD" id="LD" value="LD"/>
                                <label for="LD"><strong>Logiciels et Données</strong></label>
                            </div>
                            <div class="checkbox col-lg-offset-1 col-md-offset-1 col-sm-offset-1">
                                <input type="checkbox" name="SE" id="SE" value="SE"/>
                                <label for="SE"><strong>Systèmes Embarqués</strong></label>
                            </div>
                            <div class="checkbox col-lg-offset-1 col-md-offset-1 col-sm-offset-1">
                                <input type="checkbox" name="BIO" id="BIO" value="BIO"/>
                                <label for="BIO"><strong>Biomédical</strong></label>
                            </div>
                            <div class="checkbox col-lg-offset-1 col-md-offset-1 col-sm-offset-1">
                                <input type="checkbox" name="OC" id="OC" value="OC"/>
                                <label for="OC"><strong>Objets Connectés</strong></label>
                            </div>
                            <div class="checkbox col-lg-offset-1 col-md-offset-1 col-sm-offset-1">
                                <input type="checkbox" name="DSMT" id="DSMT" value="DSMT"/>
                                <label for="DSMT"><strong>Data Sciences, Multimedia &
                                    Telecom</strong></label>
                            </div>
                            <div class="checkbox col-lg-offset-1 col-md-offset-1 col-sm-offset-1">
                                <input type="checkbox" name="NRJ" id="NRJ" value="NRJ"/>
                                <label for="NRJ"><strong>Énergie et Environnement</strong></label>
                            </div>
                            <div class="checkbox col-lg-offset-1 col-md-offset-1 col-sm-offset-1">
                                <input type="checkbox" name="BD" id="BD" value="BD"/>
                                <label for="BD"><strong>Big Data</strong></label>
                            </div>
                        </div>
                    </fieldset>

                    <label>Type(s) d'intérêt :</label>
                    <div class="col-lg-12">
                        <div class="checkbox col-lg-offset-1 col-md-offset-1 col-sm-offset-1">
                            <input type="checkbox" name="JPO" id="JPO"/>
                            <label for="JPO"><strong>Journée Portes Ouvertes</strong></label>
                        </div>
                        <div class="checkbox col-lg-offset-1 col-md-offset-1 col-sm-offset-1">
                            <input type="checkbox" name="GP" id="GP"/>
                            <label for="GP"><strong>Gestion de Projet (I1)</strong></label>
                        </div>
                    </div>
                    <br>

                    <label for="autres">Autres :</label>
                    <input type="text" name="autres" class="form-control" id="autres"/>
                    <div class="help-block with-errors">Séparez les intérets par des ;</div>
                    <br>

                    <label for="liens">Liens externes :</label>
                    <textarea name="liens" class="form-control" id="liens"></textarea>
                    <div class="help-block with-errors">Séparez les liens par des ;</div>
                    <br>

                    <c:if test="${!empty erreurs}">
                        <div class="text-center">
                            <span class="erreur">${resultat} ${erreurs['nbrMinEleves']} ${erreurs['options']}</span>
                        </div>
                        <br>
                    </c:if>

                    <div class="text-center">
                        <button type="submit" class="btn btn-primary center-block" id="soumettre">
                            <i class="fas fa-pencil-alt fa-fw" aria-hidden="true"></i>&nbsp; Soumettre
                        </button>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>

    <div id="modalContratPro" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="alert alert-danger" role="alert">
                        <h4 class="alert-heading">Attention!</h4>
                        <p><b>Vous devez avoir l'accord de votre professeur responsable pour déposer un sujet de contrat
                            pro.</b></p>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="noPro();">Annuler
                    </button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">J'ai l'accord de mon professeur.
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function noPro() {
        document.getElementById('contratProFaux').checked = "checked";
    }
</script>