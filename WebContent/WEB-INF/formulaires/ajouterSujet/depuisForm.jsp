<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="row">
    <div class="col-sm-12">
        <c:choose>
            <c:when test="${not empty requestScope.lienForm}">
                <iframe src="${requestScope.lienForm}&embedded=true"
                        width="100%" height="1500px"
                        frameborder="0" marginheight="0" marginwidth="0">
                    Chargement en cours...
                </iframe>

            </c:when>
            <c:otherwise>
                <div class="col-sm-12">
                    <div class="alert alert-danger">
                        <strong>Désole !</strong> Aucun lien de formulaire n'est encore défini.
                    </div>
                </div>
            </c:otherwise>


        </c:choose>

    </div>
</div>