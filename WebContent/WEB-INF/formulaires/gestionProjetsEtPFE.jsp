<%--
  Created by IntelliJ IDEA.
  User: dimitrijarneau
  Date: 30/05/2018
  Time: 11:28
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<c:import url="/inc/head.jsp" />
<title>SoManager - Gestion PFE et Projets I2</title>
<link rel="stylesheet" media="screen"
	href="<c:url value="/css/bootstrap-datetimepicker.min.css"/>">
</head>
<body>
	<div class="wrapper">
		<c:import url="/Navbar" />
		<!-- Page Content Holder -->
		<div id="content">
			<c:import url="/inc/headerForSidebar.jsp" />


			<div class="panel panel-primary ">
				<div class="panel-heading">Gestion - PFE et Projets I2</div>

				<div class="panel-body">
					<c:import url="/inc/callback.jsp" />

					<ul class="nav nav-tabs" role="tablist" style="margin-bottom: 20px">
						<li
							<c:if test="${onglet eq 'planAnnee' or  empty onglet }">class="active"</c:if>>
							<a data-toggle="tab"
							href="#planifierAnnée">
								<i class="fas fa-edit fa-fw" aria-hidden="true"></i>&nbsp; PFE /
								I2 - Planifier l'année
						</a></li>
						<%--                     <c:if test="${prof eq 'true'|| profOption eq 'true' || profResponsable eq 'true'}"> --%>
						<li
							<c:if test="${onglet eq 'configurerSprints' }">class="active"</c:if>>
							<a data-toggle="tab" href="#configurerSprints"> <i
								class="far fa-edit" aria-hidden="true"></i>&nbsp; Projets I2 -
								Configuration des sprints
						</a>
						</li>
						<%--                     </c:if> --%>
					</ul>

					<%-- Contenu des différentes tab --%>
					<div class="tab-content">

						<%-- Gérer les meetings--%>
						<div id="planifierAnnée" class="tab-pane fade <c:if test="${onglet eq 'planAnnee' or  empty onglet }">in active</c:if>">
							<div class="row">
								<c:import
									url="/WEB-INF/formulaires/gestionProjetsEtPFE/planifierAnneeScolaire.jsp" />
							</div>
						</div>

						<%-- Ajouter meeting --%>
						<div id="configurerSprints" class="tab-pane fade <c:if test="${onglet eq 'configurerSprints'}">in active</c:if>">
							<div class="row">
								<c:import
									url="/WEB-INF/formulaires/gestionProjetsEtPFE/configurationSprints.jsp" />
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>
	<c:import url="/inc/scripts.jsp" />

	<script type="text/javascript"
		src="<c:url value="/js/bootstrap-datetimepicker.min.js"/>"></script>
	<script type="text/javascript"
		src="<c:url value="/js/bootstrap-datetimepicker.fr.js"/>"></script>
	<script type="text/javascript">
		$('.years').datetimepicker({
			format : "yyyy",
			startView : 'decade',
			minView : 'decade',
			viewSelect : 'decade',
			autoclose : true,
		});

		$(function() {
			$('#datetimepicker').datetimepicker({
				icons : {
					time : 'glyphicon glyphicon-time',
					date : 'glyphicon glyphicon-calendar',
					up : 'glyphicon glyphicon-chevron-up',
					down : 'glyphicon glyphicon-chevron-down',
					previous : 'glyphicon glyphicon-chevron-left',
					next : 'glyphicon glyphicon-chevron-right',
					today : 'glyphicon glyphicon-screenshot',
					clear : 'glyphicon glyphicon-trash',
					close : 'glyphicon glyphicon-remove'
				}
			});

		});

		$('.form_date').datetimepicker({
			language : 'fr',
			weekStart : 1,
			todayBtn : 1,
			autoclose : 1,
			todayHighlight : 1,
			startView : 2,
			minView : 2,
			forceParse : 0
		}).on('hide', function(event) {
			event.preventDefault();
			event.stopPropagation();
		});

		$('#exampleModal').on('shown.bs.modal', function(event) { // modification du show --> shown pour
			// eviter les conflits avec le datepicker
			var button = $(event.relatedTarget) // Button that triggered the modal
			var dateDebut = button.data('debut') // Extract info from data-* attributes
			var dateFin = button.data('fin')
			var nbrHeures = button.data('heures')
			var coefficient = button.data('coefficient')
			var sprint = button.data('sprint')
			var idsprint = button.data('sprintselect')
			var refannee = button.data('annee')
			// If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
			// Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
			var modal = $(this)
			modal.find('.modal-title').text("Sprint : " + sprint);
			modal.find('.modal-body #sprint').val(sprint);
			modal.find('.modal-body #idDateDebut').val(dateDebut);
			modal.find('.modal-body #idDateFin').val(dateFin);
			modal.find('.modal-body #idNombreHeures').val(nbrHeures);
			modal.find('.modal-body #idCoefficient').val(coefficient);
			modal.find('.modal-body #idSprintSelect').val(idsprint);
			modal.find('.modal-body #refIdAnnee').val(refannee);

			//modal.find('.modal-body input').val(recipient)
		})
	</script>
</body>
</html>
