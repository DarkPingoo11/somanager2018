<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<body>
	<div class="col-sm-12">
		<form action="<c:url value='GererJuryPFE' />" method="post"
			id="jurysPoster">
			<input type="hidden" id="ongletJuryPoster" name="ongletJuryPoster"
				value="juryPoster">
			<!-- En tête du tableau des Jurys -->
			<div class="panel-body table-container">
				<table class="table table-bordered" style="margin-bottom: -1px">
					<tr class="info unselectable">
						<th class="col-sm-2">Membres</th>
						<th class="col-sm-2">Date</th>
						<th class="col-sm-2">Sujet</th>
						<th class="col-sm-2">Modifier</th>
					</tr>
				</table>
				<!-- /En tête du tableau des Jurys -->

				<!-- Contenu du tableau -->
				<div class="row">
					<div class="col-sm-12" id="listeMembresJuryContainer"
						style="height: 300px; overflow-y: scroll">
						<table class="table table-hover" style="margin-bottom: 0">
							<c:forEach var="juryPoster" items="${listeJurysPoster}"
								varStatus="loop">
								<tbody id="listeMembresJury">
									<tr>
										<td class="col-sm-2"><c:forEach var="user"
												items="${listeUtilisateurs}">
												<c:if test="${juryPoster.idProf1 eq user.idUtilisateur}">
												${user.nom} ${user.prenom} <br>
												</c:if>
												<c:if test="${juryPoster.idProf2 eq user.idUtilisateur}">
													${user.nom}
														${user.prenom} <br>
												</c:if>
												<c:if test="${juryPoster.idProf3 eq user.idUtilisateur}">
													${user.nom}
														${user.prenom} <br>
												</c:if>
											</c:forEach></td>


										<td class="col-sm-2">
											${listeJurysPoster[loop.index].date}</td>
										<td class="col-sm-2"><c:forEach var="sujet"
												items="${listeSujets}">
												<c:forEach var="poster" items="${listePosters}">
													<c:if
														test="${listeJurysPoster[loop.index].idEquipe eq poster.idEquipe}">
														<c:if test="${poster.idSujet eq sujet.idSujet}">
												${sujet.titre}
												</c:if>
													</c:if>
												</c:forEach>
											</c:forEach></td>
										<!-- Les boutons de modifications -->
										<td class="col-sm-2">
											<div class="box">
												<a type="button" name="btn" href="#modalModifierJury"
													data-toggle="modal" data-target="#modalModifierJury"
													class="btn btn-primary " id="BtnModifierJuryPoster"
													OnClick="sendModifierJuryPoster('${listeJurysPoster[loop.index].idJuryPoster}');">
													<span class="fas fa-edit fa-fw"></span>
												</a> <input type="hidden" id="numJuryPoster"
													name="numJuryDePoster" value=""> <input
													type="hidden" id="modifierJuryPoster"
													name="modifierJuryDePoster" value=""> <input
													type="hidden" id="delJuryPoster" name="delJuryDePoster"
													value="">
												<button type="button"
													OnClick="sendDelJuryPoster('${listeJurysPoster[loop.index].idJuryPoster}');"
													class="btn btn-primary " id="BtnSupprimerJurPoster"
													style="background-color: #ff5050; border-color: #ff5050"
													data-toggle="modal" data-target="#supprimerJuryPoster">
													<span class="fas fa-trash-alt fa-fw"></span>
												</button>
											</div>
										</td>
										<!-- /Les boutons de modifications -->
									</tr>
								</tbody>
							</c:forEach>
						</table>
						<!-- /Contenu du tableau -->

						<!-- Fenêtre POP-UP -->
						<div id="modalModifierJury" class="modal fade" role="dialog"
							aria-labelledby="popup1" aria-hidden="true">
							<div class="modal-dialog" role="document">
								<div class="modal-content">
									<div class="modal-header  modal-header bg-success">
										<h2 class="modal-title" id="exampleModalLabel">Modification
											du jury de Poster</h2>
										<button type="button" class="close" data-dismiss="modal"
											aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
									</div>
									<div class="modal-body">
										<label for="professeurs" class="control-label">Professeurs
											: <span class="requis">*</span>
										</label><br>
										<c:forEach var="prof" items="${listeProfesseurs}"
											varStatus="loop">
											<input type="checkbox" name="checkbox-${loop.index}"
												value="${prof.idUtilisateur}"> ${prof.nom} ${prof.prenom}<br>
										</c:forEach>
										<br> <label class="control-label"> Date et heure
											du jury : <span class="requis">*</span>
										</label><br> <input id="datetime" name="datePopup"
											type="datetime-local"><br>
									</div>
									<div class="modal-footer">
										<button type="submit" class="btn btn-primary center-block">
											<span class="fas fa-pencil-alt fa-fw"></span> Modifier le
											jury
										</button>
									</div>
								</div>
							</div>
						</div>
						<!-- /Fenêtre POP-UP -->



					</div>
				</div>
			</div>
			<br>
			<div id=creationJury class="col-lg-6 col-lg-offset-4">
				<input type="hidden" id="inputValue" name="actionButton" value="">
				<button type="submit" name="btn" class="btn btn-primary "
					id="BtnCreerJuryPoster" onClick="sentCreerJuryPoster()">
					<span class="glyphicon glyphicon-plus"></span> Générer un Jury
					aléatoire
				</button>
				<button onClick="sentPlanAffichage()" class="btn btn-primary "
					id="planAffichage"
					style="background-color: #04B45F; border-color: #04B45F">
					<span class="fas fa-map-marker-alt fa-fw"></span> Générer le plan
					d'affichage
				</button>

			</div>
		</form>
		<%-- Modal : Avertissement de modification : Remplacement professeur --%>
		<div id="supprimerJuryPoster" class="modal fade" role="dialog">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-body">
						<div class="alert alert-danger" role="alert">
							<h4 class="alert-heading">Attention !</h4>
							<p>
								<b>Vous allez supprimer le jury</b>
							</p>
							<p class="mb-0">
								<b>Etes-vous sûr de vouloir supprimer ce Jury ?</b>
							</p>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-primary" data-dismiss="modal">Annuler</button>
						<button type="button" class="btn btn-danger" data-dismiss="modal"
							onclick="submitSupprimerJuryPoster();">Oui</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script>
		function sendModifierJuryPoster(idJuryDePoster) {
			document.getElementById("modifierJuryPoster").value = "modifierLeJuryPoster";
			document.getElementById("numJuryPoster").value = idJuryDePoster;
			document.getElementById("inputValue").value = "";
		}
		function sendDelJuryPoster(idJuryDePoster) {
			document.getElementById("numJuryPoster").value = idJuryDePoster;
		}
		function submitSupprimerJuryPoster() {
			document.getElementById("delJuryPoster").value = "SupprimerJuryPoster";
			document.getElementById("inputValue").value = "";
			document.getElementById("jurysPoster").submit();
		}
		function sentCreerJuryPoster() {
			document.getElementById("inputValue").value = "creerJuryPoster";
		}
		function sentPlanAffichage() {
			document.getElementById("inputValue").value = "PlanAffichagePoster";
		}
	</script>
</body>
</html>