<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<body>

	<div class="col-sm-12">
		<form action="<c:url value='GererJuryPGL' />" method="post"
			id="jurysReview">
			<!-- En tête du tableau des Jurys -->
			<div class="panel-body table-container">
				<table class="table table-bordered" style="margin-bottom: -1px">
					<tr class="info unselectable">
						<th class="col-sm-2">Membres</th>
						<th class="col-sm-2">Date</th>
						<th class="col-sm-2">Sprint</th>
						<th class="col-sm-2">Modifier</th>
					</tr>
				</table>
				<!-- /En tête du tableau des Jurys -->

				<!-- Contenu du tableau -->
				<div class="row">
					<div class="col-sm-12" id="listeMembresJuryReviewContainer"
						style="height: 300px; overflow-y: scroll">
						<table class="table table-hover" style="margin-bottom: 0">
							<c:forEach var="soutenance" items="${listeSoutenanceReview}"
								varStatus="loop">
								<tbody id="listeMembresJury">
									<tr>
										<td class="col-sm-2"><c:forEach var="jury"
												items="${listCompositionJury}">
												<c:if
													test="${soutenance.idSoutenance eq jury.refSoutenance}">
													<c:forEach var="prof" items="${listeUtilisateur}">
														<c:if test="${prof.idUtilisateur eq jury.refProfesseur}">
                    			 				${prof.nom} ${prof.prenom}<br>
														</c:if>
													</c:forEach>
												</c:if>
											</c:forEach></td>
										<td class="col-sm-2">
											${listeSoutenanceReview[loop.index].date}</td>
										<td class="col-sm-2">Sprint n° <c:forEach var="sprint"
												items="${listeSprint}">
												<c:if
													test="${sprint.dateDebut < listeSoutenanceReview[loop.index].date && sprint.dateFin > listeSoutenanceReview[loop.index].date}">
											${sprint.numero} 
											</c:if>

											</c:forEach></td>
										<!-- Les boutons de modifications -->
										<td class="col-sm-2">
											<div class="box">
												<a type="button" name="btn" href="#modalModifierJuryReview"
													data-toggle="modal" data-target="#modalModifierJuryReview"
													class="btn btn-primary " id="modifier"
													OnClick="sendModifSoutenance(${listeSoutenanceReview[loop.index].idSoutenance})">
													<span class="fas fa-edit fa-fw"></span>
												</a> <input type="hidden" id="numSoutenance"
													name="numSoutenance" value=""> <input type="hidden"
													id="modifierJury" name="modifierJury" value=""> <input
													type="hidden" id="delSout" name="delSout" value="">
												<button type="button"
													OnClick="sendIdSoutenance(${listeSoutenanceReview[loop.index].idSoutenance})"
													class="btn btn-primary " id="supprimer"
													style="background-color: #ff5050; border-color: #ff5050"
													data-toggle="modal" data-target="#supprimerJuryReview">
													<span class="fas fa-trash-alt fa-fw"></span>
												</button>

											</div>
										</td>
										<!-- /Les boutons de modifications -->
									</tr>
								</tbody>
							</c:forEach>
						</table>
						<!-- /Contenu du tableau -->

						<!-- Fenêtre POP-UP -->
						<div id="modalModifierJuryReview" class="modal fade" role="dialog"
							aria-labelledby="popup1" aria-hidden="true">
							<div class="modal-dialog" role="document">
								<div class="modal-content">
									<div class="modal-header  modal-header bg-success">
										<h2 class="modal-title" id="exampleModalLabel">Modification
											du jury</h2>
										<button type="button" class="close" data-dismiss="modal"
											aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
									</div>
									<div class="modal-body">
										<label for="professeurs" class="control-label">Professeurs
											: <span class="requis">*</span>
										</label><br>
										<c:forEach var="prof" items="${listeProfesseursOptionsLD}"
											varStatus="loop">
											<input type="checkbox" name="checkbox-${loop.index}"
												value="${prof.idUtilisateur}"> ${prof.nom} ${prof.prenom}<br>
										</c:forEach>
										<br> <label class="control-label"> Date et heure
											du jury : <span class="requis">*</span>
										</label><br> <input id="datetime" name="datePopup"
											type="datetime-local"><br>
									</div>
									<div class="modal-footer">
										<button type="submit" class="btn btn-primary center-block">
											<span class="fas fa-pencil-alt fa-fw"></span> Modifier le
											jury
										</button>
									</div>
								</div>
							</div>
						</div>

						<!-- /Fenêtre POP-UP -->

					</div>
				</div>
			</div>
			<br>
			<div id=creationJury class="col-md-2 col-md-offset-5">
				<input type="hidden" id="creerSoutenance" name="creerSoutenance"
					value=""> <a type="button" name="btn" href="#popup1"
					class="btn btn-primary center-block" id="modifier"
					OnClick="sentCreerSoutenance()"> <span
					class="glyphicon glyphicon-plus"></span> Créer un jury
				</a>
			</div>
		</form>
		<%-- Modal : Avertissement de modification : Remplacement professeur --%>
		<div id="supprimerJuryReview" class="modal fade" role="dialog">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-body">
						<div class="alert alert-danger" role="alert">
							<h4 class="alert-heading">Attention !</h4>
							<p>
								<b>Vous allez supprimer le jury</b>
							</p>
							<p class="mb-0">
								<b>Etes-vous sûr de vouloir supprimer ce Jury ?</b>
							</p>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-primary" data-dismiss="modal">Annuler</button>
						<button type="button" class="btn btn-danger" data-dismiss="modal"
							onclick="submitSupprimerJuryReview();">Oui</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script>
function sendModifSoutenance(id){
	document.getElementById("numSoutenance").value=id;
	document.getElementById("modifierJury").value="modifierReview";
}
function sendIdSoutenance(id){
	document.getElementById("numSoutenance").value=id;
}
function submitSupprimerJuryReview(){
	document.getElementById("delSout").value="supprimer";
	document.getElementById("jurysReview").submit();
}
function sentCreerSoutenance(){
	document.getElementById("creerSoutenance").value="creerJurySprintReview";
}
</script>
</body>
</html>