<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<body>

	<div class="col-sm-12">
		<form action="<c:url value='GererJuryPGL' />" method="post"
			id="jurysDemo">
			<input type="hidden" id="ongletJuryDemo" name="ongletJuryDemo"
				value="jurySprintDem">
			<!-- En tête du tableau des Jurys -->
			<div class="panel-body table-container">
				<table class="table table-bordered" style="margin-bottom: -1px">
					<tr class="info unselectable">
						<th class="col-sm-2">Membres</th>
						<th class="col-sm-2">Date</th>
						<th class="col-sm-2">Sprint</th>
						<th class="col-sm-2">Modifier</th>
					</tr>
				</table>
				<!-- /En tête du tableau des Jurys -->

				<!-- Contenu du tableau -->
				<div class="row">
					<div class="col-sm-12" id="listeMembresJuryDemoContainer"
						style="height: 300px; overflow-y: scroll">
						<table class="table table-hover" style="margin-bottom: 0">
							<c:forEach var="soutenance" items="${listeSoutenanceDemo}"
								varStatus="loop">
								<tbody id="listeMembresJury">
									<tr>
										<td class="col-sm-2"><c:forEach var="jury"
												items="${listCompositionJury}">
												<c:if
													test="${soutenance.idSoutenance eq jury.refSoutenance}">
													<c:forEach var="prof" items="${listeUtilisateur}">
														<c:if test="${prof.idUtilisateur eq jury.refProfesseur}">
                    			 				${prof.nom} ${prof.prenom}<br>
														</c:if>
													</c:forEach>
												</c:if>
											</c:forEach></td>
										<td class="col-sm-2">
											${listeSoutenanceDemo[loop.index].date}</td>
										<td class="col-sm-2">Sprint n° <c:forEach var="sprint"
												items="${listeSprint}">
												<c:if
													test="${sprint.dateDebut < listeSoutenanceDemo[loop.index].date && sprint.dateFin > listeSoutenanceDemo[loop.index].date}">
											${sprint.numero} 
											</c:if>

											</c:forEach></td>
										<!-- Les boutons de modifications -->
										<td class="col-sm-2">
											<div class="box">
												<a type="button" name="btn" href="#modalModifierJuryDemo"
													data-toggle="modal" data-target="#modalModifierJuryDemo"
													class="btn btn-primary " id="modifier"
													OnClick="sendModfiSoutenance2(${listeSoutenanceDemo[loop.index].idSoutenance})">
													<span class="fas fa-edit fa-fw"></span>
												</a> <input type="hidden" id="numSoutenance2"
													name="numSoutenance" value=""> <input type="hidden"
													id="modifierJury2" name="modifierJury2" value=""> <input
													type="hidden" id="delSout2" name="delSout" value="">
												<button type="button"
													OnClick="sendDelSoutenance2(${listeSoutenanceDemo[loop.index].idSoutenance})"
													class="btn btn-primary " id="supprimer2"
													style="background-color: #ff5050; border-color: #ff5050"
													data-toggle="modal" data-target="#supprimerJuryDemo">
													<span class="fas fa-trash-alt fa-fw"></span>
												</button>

											</div>
										</td>
										<!-- /Les boutons de modifications -->
									</tr>
								</tbody>
							</c:forEach>
						</table>
						<!-- /Contenu du tableau -->

						<!-- Fenêtre POP-UP -->
						<div id="modalModifierJuryDemo" class="modal fade" role="dialog"
							aria-labelledby="popup1" aria-hidden="true">
							<div class="modal-dialog" role="document">
								<div class="modal-content">
									<div class="modal-header  modal-header bg-success">
										<h2 class="modal-title" id="exampleModalLabel">Modification
											du jury</h2>
										<button type="button" class="close" data-dismiss="modal"
											aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
									</div>
									<div class="modal-body">
										<label for="professeurs" class="control-label">Professeurs
											: <span class="requis">*</span>
										</label><br>
										<c:forEach var="prof" items="${listeProfesseursOptionsLD}"
											varStatus="loop">
											<input type="checkbox" name="checkbox-${loop.index}"
												value="${prof.idUtilisateur}"> ${prof.nom} ${prof.prenom}<br>
										</c:forEach>
										<br> <label class="control-label"> Date et heure
											du jury : <span class="requis">*</span>
										</label><br> <input id="datetime" name="datePopup"
											type="datetime-local"><br>
									</div>
									<div class="modal-footer">
										<button type="submit" class="btn btn-primary center-block">
											<span class="fas fa-pencil-alt fa-fw"></span> Modifier le
											jury
										</button>
									</div>
								</div>
							</div>
						</div>

						<!-- /Fenêtre POP-UP -->

					</div>
				</div>
			</div>
			<br>
			<div id=creationJury class="col-md-2 col-md-offset-5">
				<input type="hidden" id="creerSoutenance2" name="creerSoutenance2"
					value=""> <a type="button" name="btn" href="#popup2"
					class="btn btn-primary center-block" id="modifier"
					OnClick="sentCreerSoutenance2()"> <span
					class="glyphicon glyphicon-plus"></span> Créer un jury
				</a>
			</div>
		</form>
		<%-- Modal : Avertissement de modification : Remplacement professeur --%>
		<div id="supprimerJuryDemo" class="modal fade" role="dialog">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-body">
						<div class="alert alert-danger" role="alert">
							<h4 class="alert-heading">Attention !</h4>
							<p>
								<b>Vous allez supprimer le jury</b>
							</p>
							<p class="mb-0">
								<b>Etes-vous sûr de vouloir supprimer ce Jury ?</b>
							</p>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-primary" data-dismiss="modal">Annuler</button>
						<button type="button" class="btn btn-danger" data-dismiss="modal"
							onclick="submitSupprimerJuryDemo();">Oui</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script>
		function sendModfiSoutenance2(id) {
			document.getElementById("numSoutenance2").value=id;
			document.getElementById("modifierJury2").value = "modifierDemo";
		}
		function sendDelSoutenance2(id) {
			document.getElementById("numSoutenance2").value=id;
		}
		function submitSupprimerJuryDemo() {
			document.getElementById("delSout2").value = "supprimer";
			document.getElementById("jurysDemo").submit();
		}
		function sentCreerSoutenance2() {
			document.getElementById("creerSoutenance2").value = "creerJurySprintDemo";
		}
	</script>
</body>
</html>