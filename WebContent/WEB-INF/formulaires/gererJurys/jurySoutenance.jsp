<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<body>
 <div class="panel-body">

			<div class="row">

				<div class="col-lg-8 col-md-8">
					<div class="panel panel-primary ">
						<div class="panel-heading" align="center">Sujets de PFE	attribués</div>
						<div class="panel-body">

							<form action="<c:url value="/GererJuryPFE"/>" method="post" id="soutenances">
								<input type="hidden" value="" id="numDivForm" name="numDivForm">
								<input type="hidden" name="formulaire" id="formulaire" value="jurySoutenance" />
								<c:forEach var="sujet" items="${listeSujets}" varStatus="loop">
									<div class="panel panel-default">
										<div class="panel-heading" style="font-size:20px" align="center"><u>${sujet.titre}</u></div>

										<div class="row">
											<div class="col-lg-6 col-md-6">

												<input name="idSoutenance-${loop.index}" id="idSoutenance-${loop.index}" type="hidden" value="${listeSoutenances[loop.index].idSoutenance}" />
												<input name="idEquipe-${loop.index}" id="idEquipe-${loop.index}" type="hidden" value="${listeSoutenances[loop.index].idEquipe}" />
												<input name="idSujet-${loop.index}" id="idSujet-${loop.index}" type="hidden" value="${sujet.idSujet}" />
												<input name="idJury-${loop.index}" id="idJury-${loop.index}" type="hidden" value="${listeSoutenances[loop.index].idJurySoutenance}" />

												<label class="col-lg-12 col-md-12">
													<br />Date et heure de la soutenance :
													<span class="requis">*</span>
												</label>
												<div class="col-lg-12 col-md-12">
													<div class="input-group date form_date" data-date="" data-date-format="dd MM yyyy hh ii" data-link-field="dateSoutenance-${loop.index}"	data-link-format="yyyy-mm-dd hh:ii">
														<input class="form-control"	id="dateSoutenance-${loop.index}"	name="dateSoutenance-${loop.index}" size="16" type="text" value="${fn:substring(listeSoutenances[loop.index].dateSoutenance,0,16)}" readonly>
														<span class="input-group-addon">
														<span class="glyphicon glyphicon-remove">
														</span></span>
														<span class="input-group-addon">
														<span class="glyphicon glyphicon-calendar">
														</span></span>
													</div>
												</div>
												<br/>
											</div>
											<div class="col-lg-6 col-lg-6 text-center">

												<br>
												<button style="width:300px" type="button" class="btn btn-default btn-sm disabled"   draggable="false" >
													<c:choose>
														<c:when test="${not empty referentSujets[loop.index].idProfesseur}">
															${mapIdProfesseur[referentSujets[loop.index].idProfesseur].prenom} ${mapIdProfesseur[referentSujets[loop.index].idProfesseur].nom}
															<input name="jury-${loop.index}-prof-0" type="hidden" value="${referentSujets[loop.index].idProfesseur}">
														</c:when>
														<%-- <c:when test="${not empty jurysSoutenances[listeSoutenances[loop.index].idJury].idProf1}">
															${nomjury[jurysSoutenances[listeSoutenances[loop.index].idJury].idProf1]}
															<input name="jury-${loop.index}-prof-1" type="hidden" value="${jurysSoutenances[listeSoutenances[loop.index].idJury].idProf1}">
														</c:when> --%>

														<c:otherwise>
															Pas de référent
														</c:otherwise>

													</c:choose>
												</button>
												<br>
												<button style="width:300px" type="button" class="btn btn-default btn-sm" id="jury-${loop.index}-1" ondrop="drop('jury-'+${loop.index}+'-1')" draggable="false" ondragover="allowDrop(event)" ondragleave="refuseDrop(event)">
													<c:choose>
														<c:when test="${not empty jurysSoutenances[listeSoutenances[loop.index].idJurySoutenance].idProf1}">
															${nomjury[jurysSoutenances[listeSoutenances[loop.index].idJurySoutenance].idProf1]}
															<input name="jury-${loop.index}-prof-1" type="hidden" value="${jurysSoutenances[listeSoutenances[loop.index].idJurySoutenance].idProf1}">
														</c:when>
														<c:otherwise>
															Ajouter un membre du Jury
														</c:otherwise>
													</c:choose>
												</button>
												<br>
												<button style="width:300px" type="button" class="btn btn-default btn-sm" id="jury-${loop.index}-2" ondrop="drop('jury-'+${loop.index}+'-2')" draggable="false" ondragover="allowDrop(event)" ondragleave="refuseDrop(event)">
													<c:choose>
														<c:when test="${not empty jurysSoutenances[listeSoutenances[loop.index].idJurySoutenance].idProf2}">
															${nomjury[jurysSoutenances[listeSoutenances[loop.index].idJurySoutenance].idProf2]}
															<input name="jury-${loop.index}-prof-2" type="hidden" value="${jurysSoutenances[listeSoutenances[loop.index].idJurySoutenance].idProf2}">
														</c:when>
														<c:otherwise>
															Ajouter un membre du Jury
														</c:otherwise>
													</c:choose>
												</button>
											</div>
										</div>
										<c:choose>
											<c:when test="${not empty listeSoutenances[loop.index].idSoutenance}">
												<br>
												<button type="submit" class="btn btn-primary center-block"  onclick="sendOnlyOneJury('${loop.index}');">
													<i class="fas fa-pencil-alt fa-fw" aria-hidden="true"></i>&nbsp; Modifier le jury
												</button>
												<br>
											</c:when>
											<c:otherwise>
												<br>
												<button type="submit" class="btn btn-primary center-block"  onclick="sendOnlyOneJury('${loop.index}');">
													<i class="fas fa-plus fa-fw" aria-hidden="true"></i>&nbsp; Créer la soutenance
												</button>
												<br>
											</c:otherwise>
										</c:choose>
									</div>
								</c:forEach>
								<button type="submit" class="btn btn-primary center-block">
									<i class="far fa-save fa-fw" aria-hidden="true"></i>&nbsp; Sauvegarder tout
								</button>
							</form>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-4">
					<div class="panel panel-default ">
						<div class="panel-heading" align="center">Afficher les professeurs par option :</div>
						<div class="panel-body">
							<select class="form-control" name="divOption" id="divOption" onchange="changeProfOption();" >
								<option value="${optionPrincipal}"> ${optionPrincipal} </option>
								<c:forEach var="option" items="${allOptions}">
									<option value="<c:out value="${option}"/>">
										<c:out value="${option}" />
									</option>
								</c:forEach>
							</select>
							<h5 align="center">Professeur de cette Option :</h5>

							<div class=" text-center" id="divProfs">
								<c:forEach var="prof" items="${listeProfesseurs}" varStatus="loop">
									<button style="width:300px" type="button" class="btn btn-default btn-sm" id="profDrag-${loop.index}" draggable='true' ondragstart="drag(event,'profDrag-'+${loop.index});" data-option='${mapOptionsProf[prof.idUtilisateur]}'>
										<c:out value="${prof.nom}" />
										<c:out value="${prof.prenom}" />
										<input name="prof-${loop.index}" type="hidden" value="${prof.idUtilisateur}">
									</button>
								</c:forEach>
							</div>
						</div>
						<br>
						<button onclick="juryAlea();" class="btn btn-primary center-block">
							<i class="fas fa-random fa-fw" aria-hidden="true"></i>&nbsp; Composer les jurys aléatoirement
						</button>
						<br>
					</div>
				</div>
			</div>
		</div>
</body>
</html>