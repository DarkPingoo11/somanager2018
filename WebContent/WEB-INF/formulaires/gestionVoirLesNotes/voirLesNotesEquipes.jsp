<%@ page language="java" contentType="text/html; charset=UTF-8"
		 pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<div class="col-sm-12">
	<div class="panel panel-primary" style="max-height: 100%;">
		<div class="panel-heading" style="padding-bottom: 0">
			<div class="row">
				
</div>
</div>

		<!-- Tableau de r�sultats -->
		<div class="panel-body table-container">
			<table class="table table-bordered" style="margin-bottom: -1px">
				<tr class="info unselectable">
					<th class="col-sm-6">Equipe</th>
					<th class="col-sm-3">Note</th>
					<th class ="col-sm-3">Sprint</th>
					
				</tr>
			</table>

			<!-- Contenu du tableau -->
			<div class="row">
				<div class="col-sm-12" id="listeNotesEquipesContainer"
					 style="height: 300px;">
					<table class="table table-hover" style="margin-bottom: 0">
						<tbody id="notesEquipes">
						<%-- Affichage de la moyenne des notes des �quipes  --%>
					
							<c:forEach var="map" items="${map}">
							
							<c:forEach var="entity" items="${map.value.notes }">
										
							<td class="col-sm-6">${map.key.nom } 
							</td>
							
							
							<td class="col-sm-3">${entity.value } 
							</td>
							
							
							<td class="col-sm-3">${entity.key.libelle } 
							</td>
							
							
							<tr>
							</c:forEach>
							</c:forEach>
							
							
							
						
						
						
						
						</tbody>

					</table>


				</div>
			</div>
			
			
		</div>
		</div>
</div>





