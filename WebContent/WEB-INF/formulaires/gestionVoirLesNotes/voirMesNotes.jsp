<%@ page language="java" contentType="text/html; charset=UTF-8"
		 pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<div class="col-sm-12">
	<div class="panel panel-primary" style="max-height: 100%;">
		<div class="panel-heading" style="padding-bottom: 0">
			<div class="row">
				<form action="<c:url value="/VoirLesNotes"/>" method="get" id="actualiserPage">
					<!-- Filtres -->
					<div class="col-sm-12" style="margin-bottom: 15px">
						<!-- Filtre ann�e scolaire -->
						<div class="col-sm-3">

							Sprint :
							<select class="form-control" name="sprint" data-champ="idSprint">
								<option value=""></option>
								<c:forEach var="sprint" items="${sprintsAnnee}">
									<option
											value="<c:out value="${sprint.idSprint}"/>"
											<c:if test="${ sprintS eq sprint.idSprint}"> selected</c:if>
									>
										<c:out value="${sprint.numero}" />
										
									</option>
								</c:forEach>
							</select>
						</div>
						</div>
</form>
</div>
</div>

		<!-- Tableau de r�sultats -->
		<div class="panel-body table-container">
			<table class="table table-bordered" >
				<tr class="info unselectable">
				
					
					<th class="col-sm-5">Mati�re</th>
					<th class="col-sm-5">Note</th>
					<th class="col-sm-2">Coeff</th>
					
				</tr>
			</table>

			<!-- Contenu du tableau -->
			<div class="row">
				<div class="col-sm-12" id="listeMesNotesContainer"
					 style="height: 300px;">
					<table class="table table-hover" style="margin-bottom: 0">
						<tbody id="notes">
						<%-- Affichage de la moyenne des notes de l'utilisateur  --%>
						<c:forEach var="note" items="${notes}">
						<%--pour le I2 connect� --%>
						<c:forEach var="matiere" items="${matieres}">
						
						<c:if test="${ note.refMatiere eq matiere.idMatiere}">
							
							<td class="col-sm-5">${matiere.libelle } 
							</td>
							<td class="col-sm-5">${note.note }
							</td>
							<td class="col-sm-2">${matiere.coefficient }
							</td>
							
						
						</c:if>
						<tr>
						</c:forEach>
						</c:forEach>
						
						</tbody>

					</table>


				</div>
			</div>
		</div>
	</div>
</div>


