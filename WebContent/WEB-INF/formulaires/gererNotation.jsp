<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <c:import url="/inc/head.jsp"/>
    <title>SoManager - Noter</title>
    <link rel="stylesheet" media="screen"
          href="<c:url value="/css/bootstrap-datetimepicker.min.css"/>">
</head>
<body>
<div class="wrapper">
    <c:import url="/Navbar"/>
    <!-- Page Content Holder -->
    <div id="content">
        <c:import url="/inc/headerForSidebar.jsp"/>


        <div class="panel panel-primary ">
            <div class="panel-heading">Noter
            </div>

            <div class="panel-body">
                <c:import url="/inc/callback.jsp"/>

                <ul class="nav nav-tabs" role="tablist" style="margin-bottom: 20px">
                    <li class="<c:if test="${requestScope.ongletActif eq 'PFE'}">active</c:if>">
                        <a data-toggle="tab" href="#noterPFE">
                            <i class="fas fa-edit fa-fw" aria-hidden="true"></i>&nbsp;
                            PFE
                        </a>
                    </li>
                    <li class="<c:if test="${requestScope.ongletActif eq 'PGL'}">active</c:if>">
                        <a data-toggle="tab" href="#noterPGL">
                            <i class="far fa-edit" aria-hidden="true"></i>&nbsp;
                            PGL
                        </a>
                    </li>
                    <c:if test="${profReferent eq 'true' }">
                        <li class="<c:if test="${requestScope.ongletActif eq 'Publier'}">active</c:if>">
                            <a data-toggle="tab" href="#publierNotes">
                                <i class="far fa-paper-plane" aria-hidden="true"></i>&nbsp;
                                Publication
                            </a>
                        </li>
                    </c:if>
                </ul>

                <%-- Contenu des différentes tab --%>
                <div class="tab-content">

                    <%-- Gérer les notes PFE --%>
                    <div id="noterPFE"
                         class="tab-pane fade <c:if test="${requestScope.ongletActif eq 'PFE'}">in active</c:if>">
                        <div class="row">
                            <c:import url="/WEB-INF/formulaires/gererNotation/noter.jsp"/>
                        </div>
                    </div>

                    <%-- Gérer les notes PGL --%>
                    <div id="noterPGL"
                         class="tab-pane fade <c:if test="${requestScope.ongletActif eq 'PGL'}">in active</c:if>">
                        <div class="row">
                            <c:import url="/WEB-INF/formulaires/gererNotation/noterPGL.jsp"/>
                        </div>
                    </div>

                    <%-- Publier notes --%>
                    <c:if test="${profReferent eq 'true' }">
                        <div id="publierNotes" class="tab-pane fade">
                            <div class="row">
                                <c:import url="/WEB-INF/formulaires/gererNotation/publierNotes.jsp"/>
                            </div>
                        </div>
                    </c:if>

                </div>
            </div>
        </div>
    </div>
</div>
<c:import url="/inc/scripts.jsp"/>
<script type="text/javascript" src="<c:url value="/js/bootstrap-datetimepicker.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/js/bootstrap-datetimepicker.fr.js"/>"></script>
<script type="text/javascript" src="<c:url value="/js/perfect-scrollbar.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/js/scripts/gererNotation.js"/>"></script>
<script type="text/javascript">
    $('.years').datetimepicker({
        format: "yyyy",
        startView: 'decade',
        minView: 'decade',
        viewSelect: 'decade',
        autoclose: true,
    });
    $('.form_date').datetimepicker({
        language: 'fr',
        weekStart: 1,
        todayBtn: 1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 2,
        minView: 2,
        forceParse: 0
    });
</script>
</body>
</html>
