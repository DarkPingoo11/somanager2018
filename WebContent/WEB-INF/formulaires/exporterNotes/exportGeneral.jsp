<%--
  Created by IntelliJ IDEA.
  User: Tristan LE GACQUE
  Date: 06/05/2018
  Time: 09:50
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:set var="modele" value="${requestScope.modeleDonnees}" scope="page" />

<div class="col-sm-12">
    <div class="panel panel-default ">
        <div class="panel-heading">
            <i class="fas fa-download fa-fw" aria-hidden="true"></i>&nbsp;
            Export des notes de jury de I3
        </div>
        <div class="panel-body">
            <form method="post" action="<c:url value="/ExporterNotes"/>">
                <input type="hidden" name="formulaire" value="exporterNotes">
                <div class="row">
                    <div class="col-sm-12">
                        <p>Vous pouvez à partir de cette page exporter les notes de poster et de soutenance
                            des élèves de I3. Pour cela, merci de sélectionner l'année scolaire souhaitée,
                            ainsi que l'option pour laquelle vous souhaitez exporter les notes. Enfin, le type d'export
                            des note est à spécifier.
                            <br />
                            En sélectionnant <b>Avec liste définie</b>, l'export des notes sera pris en compte uniquement pour les
                            etudiants définis dans le modèle.
                            <br />
                            En sélectionnant <b>Avec autocomplétion</b>, le système ajoutera chaque utilisateur de l'année
                            choisie pour l'option choisi dans le modèle.
                            <br />
                            <br />
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 no-padding">
                        <%-- Choisir l'année scolaire --%>
                        <div class="col-sm-4">
                            <label for="anneeSelect">Année scolaire : </label>
                            <select id="anneeSelect" class="selectpicker form-control" name="anneeChoisi">
                                <c:forEach var="annee" items="${requestScope.anneesScolaires}">
                                    <option value="${ annee.anneeDebut }">${ annee.anneeDebut }</option>
                                </c:forEach>
                            </select>
                        </div>

                        <%-- Choisir le type d'année I2/I3--%>
                        <div class="col-sm-4">
                            <label for="exportAnneeSelect">PFE ou PGL: </label>
                            <select id="exportAnneeSelect" class="selectpicker form-control" name="exportAnneeChoisi">
                                <option value="pfe" selected>I3 - PFE</option>
                                <option value="pgl">I2 - PGL</option>
                            </select>
                        </div>

                        <%-- Choisir l'option scolaire --%>
                        <div class="col-sm-4">
                            <label for="optionSelect">Option : </label>
                            <select id="optionSelect" class="selectpicker form-control" name="optionChoisi">
                                <option value="-1">Toutes</option>
                                <c:forEach var="option" items="${requestScope.optionsESEO}">
                                    <option value="${ option.idOption }">${ option.nomOption }</option>
                                </c:forEach>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="row" style="margin-top: 20px">
                    <div class="col-sm-12 no-padding">
                        <%-- Afficher le nom du modèle --%>
                        <div class="col-sm-4">
                            <label for="nomFichierModele">Modèle utilisé : </label>
                            <input id="nomFichierModele" name="nomFichier" type="text" class="form-control" readonly
                                   value="${modele.nomFichier}">
                        </div>

                        <%-- Choisir le type d'export --%>
                        <div class="col-sm-4">
                            <label for="exportSelect">Type d'export : </label>
                            <select id="exportSelect" class="selectpicker form-control" name="exportChoisi">
                                <option value="1" selected>Autocomplétion</option>
                                <option value="0">Liste définie</option>
                            </select>
                        </div>

                        <%-- Bouton --%>
                        <div class="col-sm-4" style="padding-top: 25px">
                            <button class="btn btn-success btn-block" type="submit">
                                <i class="fas fa-download fa-fw" aria-hidden="true"></i>&nbsp;
                                Exporter les notes des jury
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>