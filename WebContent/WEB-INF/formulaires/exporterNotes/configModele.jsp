<%--
  Created by IntelliJ IDEA.
  User: Tristan LE GACQUE
  Date: 06/05/2018
  Time: 09:50
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%-- Première ligne --%>
<div class="col-sm-12 no-padding">

    <%-- Colonne de gauche --%>
    <div class="col-sm-6 no-padding">
        <%-- Importer un nouveau modèle --%>
        <div class="col-sm-12">
            <div class="panel panel-default" style="margin-bottom: 48px;">
                <div class="panel-heading">
                    <i class="fas fa-upload fa-fw" aria-hidden="true"></i>&nbsp;
                    Importer un nouveau modèle
                    <a role="button" class="pull-right" data-toggle="modal" data-target="#consignesModal">
                        <i class="fas fa-question-circle fa-fw text-blue" aria-hidden="true"></i>
                    </a>
                </div>
                <div class="panel-body">
                    <div class="col-sm-12">
                        <form method="post" enctype="multipart/form-data"
                              action="<c:url value="/ExporterNotes"/>">
                            <input type="hidden" name="formulaire" value="envoyerModele">
                            Uploadez un modèle de note au format excel (.xlsx)
                            <div class="col-sm-12 no-padding" style="margin-top: 10px; margin-bottom: 15px">
                                <div class="input-group">
                                    <label class="input-group-btn">
                                                <span class="btn btn-primary btn-file">
                                                    Parcourir…
                                                    <input type="file" id="file_selected" name="file_selected" accept=".csv,.xlsx" onchange="checkCSVFile(this);" style="display: none;">
                                                </span>
                                    </label>

                                    <input id="nomFichier" class="form-control" readonly="" type="text">
                                    <label class="input-group-btn">
                                        <button class="btn btn-success" type="submit" id="sendCsvButton" disabled="">
                                            <i class="fas fa-upload fa-fw" aria-hidden="true"></i>
                                            &nbsp;Envoyer
                                        </button>
                                    </label>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <%-- Choisir un modèle --%>
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fas fa-sync-alt fa-fw" aria-hidden="true"></i>&nbsp;
                    Choisir le modèle d'export des notes
                </div>
                <div class="panel-body">
                    <div class="col-sm-12">
                        Choisissez le modèle de note à utiliser
                        <div class="col-md-12 no-padding" style="margin-top: 10px">
                            <form method="post" action="<c:url value="/ExporterNotes"/>">
                                <input type="hidden" name="formulaire" value="choisirModele">
                                <div class="col-sm-8 no-padding">
                                    <div class="form-group">
                                        <select id="choixModele" name="choixModele" class="form-control selectpicker">
                                            <c:forEach var="modeleDispo" items="${ requestScope.modelesDispo }">
                                                <c:set var="mselect" value="" />
                                                <c:if test="${modeleDispo.nomFichier eq requestScope.modeleSelect}">
                                                    <c:set var="mselect" value="selected" />
                                                </c:if>
                                                <option value="${ modeleDispo.nomFichier }" ${mselect}>
                                                        ${modeleDispo.nomFichier}
                                                </option>
                                            </c:forEach>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <button type="submit" class="btn btn-success btn-block">
                                        Valider
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <%-- Colonne de droite --%>
    <%-- Assistant de configuation --%>
    <div class="col-sm-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fas fa-magic fa-fw" aria-hidden="true"></i>&nbsp;
                Assistant de configuration du modèle
            </div>
            <form method="post" action="<c:url value="/ExporterNotes"/>">
                <div class="panel-body" id="assistantCreation">
                    <input type="hidden" name="choixModele" value="${ modeleDonnees.nomFichier }" />
                    <div class="col-sm-12 no-padding" data-text="la ligne contenant les entêtes (n°, nomUtilisateur,  note1, etc...)">
                        <label for="ligneHeader">Entête de la liste utilisateur</label>
                        <input type="number" class="parametre pull-right" name="ligneHeader" id="ligneHeader"
                               value="${ modeleDonnees.ligneHeader }" data-info="ligne">
                    </div>

                    <div class="col-sm-12 no-padding" data-text="la colonne contenant l'identifiant (n°) des utilisateurs">
                        <label for="colId">Colonne identifiant utilisateur</label>
                        <input type="number" class="parametre pull-right" name="colId" id="colId"
                               value="${ modeleDonnees.colId }">
                    </div>

                    <div class="col-sm-12 no-padding" data-text="la colonne contenant le nom et prenom des utilisateurs">
                        <label for="colNom">Colonne nom utilisateur</label>
                        <input type="number" class="parametre pull-right" name="colNom" id="colNom"
                               value="${ modeleDonnees.colNom }">
                    </div>

                    <div class="col-sm-12 no-padding" data-text="la colonne contenant la note">
                        <label for="colNote">Colonne note</label>
                        <input type="number" class="parametre pull-right" name="colNote" id="colNote"
                               value="${ modeleDonnees.colNote }">
                    </div>

                    <div class="col-sm-12 no-padding" data-text="la colonne contenant le commentaire de la note">
                        <label for="colCom">Colonne commentaire</label>
                        <input type="number" class="parametre pull-right" name="colCom" id="colCom"
                               value="${ modeleDonnees.colCom }">
                    </div>

                    <div class="col-sm-12 no-padding" data-text="la colonne contenant la moyenne des deux notes">
                        <label for="colMoyenne">Colonne moyenne saisi</label>
                        <input type="number" class="parametre pull-right" name="colMoyenne" id="colMoyenne"
                               value="${ modeleDonnees.colMoyenne }">
                    </div>

                    <div class="col-sm-12 no-padding" data-text="la colonne contenant le grade de l'utilisateur">
                        <label for="colGrade">Colonne grade saisi</label>
                        <input type="number" class="parametre pull-right" name="colGrade" id="colGrade"
                               value="${ modeleDonnees.colGrade }">
                    </div>

                    <%-- Boutons --%>
                    <div class="col-sm-12 no-padding" style="margin-top: 20px">
                        <div class="col-sm-6 no-padding">
                            <button class="btn btn-primary btn-block" role="button" type="button" data-action="startAssistant">
                                <i class="fas fa-magic fa-fw" aria-hidden="true"></i>&nbsp;
                                Démarrer l'assistant
                            </button>
                        </div>
                        <div class="col-sm-6">
                            <input type="hidden" name="formulaire" value="parametrerModele">
                            <button class="btn btn-success btn-block" type="submit">
                                <i class="fas fa-save fa-fw" aria-hidden="true"></i>&nbsp;
                                Sauvegarder
                            </button>
                        </div>
                    </div>

                </div>
            </form>
        </div>
    </div>

</div>



<%-- 2e ligne - Affichage du modèle --%>
<div class="col-sm-12">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <b>Modèle : </b> ${ requestScope.modeleDonnees.nomFichier }
            <span class="pull-right">
                                Ligne : <span data-replace="ligneSelectionnee"></span>
                                &nbsp;|
                                Colonne : <span data-replace="colonneSelectionnee"></span>
                            </span>
        </div>
        <div class="panel-body excel-container">
            <table id="excel-file" class="excel-file">
                <c:forEach var="ligne" items="${requestScope.modeleContenu}">
                    <tr>
                        <c:forEach var="col" items="${ligne}">
                            <td class="excel-file"
                                style="background-color: ${col.hexColorBackground};
                                        max-width:${col.width}px;
                                        max-height:${col.height}px">
                                    ${col.donnees}
                            </td>
                        </c:forEach>
                    </tr>
                </c:forEach>
            </table>
        </div>
        <div class="panel-footer">
            <b><u>Note</u></b> : Le modèle ci-dessous est une représentation résumée du modèle original sous excel.
            <button role="button" type="button" class="btn btn-danger pull-right" data-action="supprimerModele">
                <i class="far fa-trash-alt fa-fw" aria-hidden="true"></i>&nbsp;Supprimer le modèle
            </button>
            <br />De plus, les balises suivantes seront remplacées dans le fichier excel par les valeurs associées
            <br />
            <ul>
                <li><b>{date}</b> : Date du jour</li>
                <li><b>{resp_module}</b> : Responsable du module</li>
                <li><b>{moderateur}</b> : Modérateur</li>
            </ul>
        </div>
    </div>
</div>

<%-- Supprimer modèle --%>
<div id="supprimerModeleModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="alert alert-danger" role="alert">
                    <h4 class="alert-heading">Attention!</h4>
                    <p>
                        Etes-vous sûr de vouloir supprimer le modèle <strong>${requestScope.modeleDonnees.nomFichier}</strong>
                    </p>
                </div>
            </div>
            <div class="modal-footer">
                <form method="post" action="<c:url value="/ExporterNotes"/>">
                    <input type="hidden" name="formulaire" value="supprimerModele">
                    <input type="hidden" name="choixModele" value="${requestScope.modeleDonnees.nomFichier}">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Annuler</button>
                    <button type="submit" class="btn btn-danger">Supprimer le modèle</button>
                </form>
            </div>
        </div>
    </div>
</div>

<%-- Consignes --%>
<div id="consignesModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <i class="fas fa-question-circle fa-fw" aria-hidden="true"></i>&nbsp;
                Consignes pour l'importation d'un modèle
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" style="text-align: justify">
                Avant d'importer un modèle dans le système, il est important de vérifier que les consignes ci-dessous
                sont respectées. Chaque modèle doit comporter un header, qui contient les informations suivantes
                (le nom des colonnes n'est pas important, seule les donnes contenues le sont):
                <b>Id ou n°</b> | <b>Nom prénom</b> | <b>Note PFE1</b> | <b>Commentaire PFE1</b> | <b>Note PFE2</b>
                 | <b>Commentaire PFE2</b> | <b>Moyenne saisie</b> | <b>Grade saisi</b>
                <h3><i class="fas fa-user-circle fa-fw" aria-hidden="true"></i>&nbsp;Liste définie</h3>
                <div style="text-align: justify">
                    <b>Un modèle défini</b> est un modèle possédant la liste des noms utilisateurs. Lors d'un export avec
                    ce type de modèle, seuls les utilisateurs présents dans le modèle seront exportés.
                    La <b>contrainte</b> de ce modèle est d'écrire la nom d'un utilisateur par ligne,
                    dans la même colonne.
                    <br />
                    <i class="fas fa-cloud-download-alt fa-fw" aria-hidden="true"></i>&nbsp;
                    <a href="<c:url value="/download/modele_defini.xlsx"/>">Télécharger l'exemple : modèle défini</a>
                </div>

                <h3><i class="fas fa-users fa-fw" aria-hidden="true"></i>&nbsp;Autocomplétion</h3>
                <div style="text-align: justify">
                    <b>Un modèle d'autocomplétion</b> est un modèle qui est général, et qui sera complété automatiquement en
                    fonction des utilisateurs que l'on souhaite exporter. La <b>seule contrainte</b> de ce modèle est
                    qu'il faut, en plus du header, ajouter une ligne par défaut, avec le style souhaité, qui sera copiée
                    pour chaque utilisateur exporté.
                    <br />
                    <i class="fas fa-cloud-download-alt fa-fw" aria-hidden="true"></i>&nbsp;
                    <a href="<c:url value="/download/modele_auto.xlsx"/>">Télécharger l'exemple : modèle autocomplétion</a>
                </div>
            </div>
        </div>
    </div>
</div>

