<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <c:import url="/inc/head.jsp"/>
    <title>SoManager - Paramètres de l'application</title>
    <link rel="stylesheet" href="<c:url value="/css/codemirror.css"/>">
    <style type="text/css">
        .CodeMirror {
            border-top: 1px solid #888;
            border-bottom: 1px solid #888;
            height: 500px;
        }
    </style>
</head>
<body>

<div class="wrapper">
    <c:import url="/Navbar"/>
    <!-- Page Content Holder -->
    <div id="content">
        <c:import url="/inc/headerForSidebar.jsp"/>


        <div class="panel panel-primary ">
            <div class="panel-heading">Administrateur - Paramètres de l'application</div>
            <div class="panel-body">

                <div class="row">
                    <c:import url="/inc/callback.jsp"/>
                </div>

                <%-- Configuration du serveur LDAP --%>
                <div class="panel panel-default ">
                    <div class="panel-heading" align="center">Configuration serveur LDAP :</div>
                    <div class="panel-body">

                        <%-- Elements de formulaire --%>
                        <div class="row">
                            <%-- Modifier les paramètres LDAP --%>
                            <form method="post" action="<c:url value="/GererApplication"/>" id="modifier">

                                <label class="col-lg-3 col-md-3" for="serveur">Adresse du serveur : <span
                                        class="requis">*</span></label>
                                <div class="col-lg-3 col-md-3">
                                    <input type="text" name="serveur" id="serveur"
                                           class="form-control" value="<c:out value="${ldap[1]}"/>"
                                           autofocus required/> <br>
                                </div>

                                <label class="col-lg-3 col-md-3" for="port">Numéro de port : <span
                                        class="requis">*</span></label>
                                <div class="col-lg-3 col-md-3">
                                    <input type="text" name="port" id="port" class="form-control"
                                           value="<c:out value="${ldap[2]}"/>" autofocus required/> <br>
                                </div>

                                <label class="col-lg-3 col-md-3" for="contexteDN">Contexte DN : <span
                                        class="requis">*</span></label>
                                <div class="col-lg-3 col-md-3">
                                    <input type="text" name="contexteDN" id="contexteDN"
                                           class="form-control" value="<c:out value="${ldap[0]}"/>"
                                           autofocus required/> <br>
                                </div>

                                <label class="col-lg-3 col-md-3" for="contexteDNUser">Contexte DN Utilisateur : <span
                                        class="requis">*</span></label>
                                <div class="col-lg-3 col-md-3">
                                    <input type="text" name="contexteDNUser" id="contexteDNUser"
                                           class="form-control" value="<c:out value="${ldap[7]}"/>"
                                           autofocus required/> <br>
                                </div>

                                <label class="col-lg-3 col-md-3" for="nom">Nom de l'attribut "nom" : <span
                                        class="requis">*</span></label>
                                <div class="col-lg-3 col-md-3">
                                    <input type="text" name="nom" id="nom" class="form-control"
                                           value="<c:out value="${ldap[3]}"/>" autofocus required/> <br>
                                </div>

                                <label class="col-lg-3 col-md-3" for="prenom">Nom de l'attribut "prenom" : <span
                                        class="requis">*</span></label>
                                <div class="col-lg-3 col-md-3">
                                    <input type="text" name="prenom" id="prenom" class="form-control"
                                           value="<c:out value="${ldap[4]}"/>" autofocus required/> <br>
                                </div>

                                <label class="col-lg-3 col-md-3" for="mail">Nom de l'attribut "mail" : <span
                                        class="requis">*</span></label>
                                <div class="col-lg-3 col-md-3">
                                    <input type="text" name="mail" id="mail" class="form-control"
                                           value="<c:out value="${ldap[5]}"/>" autofocus required/> <br>
                                </div>

                                <label class="col-lg-3 col-md-3" for="identifiant">Nom de l'attribut "identifiant" :
                                    <span class="requis">*</span></label>
                                <div class="col-lg-3 col-md-3">
                                    <input type="text" name="identifiant" id="identifiant"
                                           class="form-control" value="<c:out value="${ldap[6]}"/>"
                                           autofocus required/> <br>
                                </div>

                                <label class="col-lg-3 col-md-3" for="compteUtilisateur">Commun Name (CN) compte
                                    utilisateur (lecture) : <span class="requis">*</span></label>
                                <div class="col-lg-3 col-md-3">
                                    <input type="text" name="compteUtilisateur" id="compteUtilisateur"
                                           class="form-control"
                                           value="<c:out value="${ldap[8]}"/>" autofocus required/> <br>
                                </div>

                                <label class="col-lg-3 col-md-3" for="mdpUtilisateur">Mot de passe du compte
                                    (utilisateur): <span class="requis">*</span></label>
                                <div class="col-lg-3 col-md-3">
                                    <input type="password" name="mdpUtilisateur" id="mdpUtilisateur"
                                           class="form-control" value="<c:out value="${ldap[9]}"/>" autofocus required/>
                                    <br>
                                </div>

                                <label class="col-lg-3 col-md-3" for="compteAdministrateur">Contexte DN administrateur
                                    (ecriture) : <span class="requis">*</span></label>
                                <div class="col-lg-3 col-md-3">
                                    <input type="text" name="compteAdministrateur" id="compteAdministrateur"
                                           class="form-control" value="<c:out value="${ldap[10]}"/>" autofocus
                                           required/> <br>
                                </div>

                                <label class="col-lg-3 col-md-3" for="mdpAdministrateur">Mot de passe du compte (admin):
                                    <span class="requis">*</span></label>
                                <div class="col-lg-3 col-md-3">
                                    <input type="password" name="mdpAdministrateur" id="mdpAdministrateur"
                                           class="form-control" value="<c:out value="${ldap[11]}"/>" autofocus
                                           required/> <br>
                                </div>

                                <input type="hidden" name="formulaire" id="formulaire" value="modifierConfLDAP"/>

                                <div class="col-lg-12 col-md-12">
                                    <button type="button" class="btn btn-primary center-block" data-toggle="modal"
                                            data-target="#modifierConfModal">
                                        <i class="fas fa-pencil-alt fa-fw" aria-hidden="true"></i>&nbsp; Modifier
                                    </button>
                                </div>
                            </form>

                            <%-- Reinitialiser les paramètres LDAP --%>
                            <form method="post" action="<c:url value="/GererApplication"/>" id="reinitialiser">
                                <input type="hidden" name="formulaire" id="formulaire" value="defautConfLDAP"/>
                                <div class="col-lg-12 col-md-12">
                                    <br/>
                                    <button type="button" class="btn btn-primary center-block" data-toggle="modal"
                                            data-target="#parDefautModal">
                                        <i class="fas fa-redo fa-fw" aria-hidden="true"></i>&nbsp; Réinitialiser
                                        paramètres
                                    </button>
                                    <br>
                                </div>
                            </form>
                        </div>

                        <%-- Test de connexion LDAP --%>
                        <div class="row">
                            <div class="col-lg-12 col-md-12">
                                <c:choose>
                                    <c:when test="${ldapConnexion}">
                                        <div class="alert alert-success" style="text-align: center" ; role="alert">
                                            Test de connexion : Tout semble fonctionner
                                        </div>
                                    </c:when>
                                    <c:when test="${!ldapConnexion}">
                                        <div class="alert alert-danger" style="text-align: center" ; role="alert">
                                            Test de connexion : Il semble qu'il y ai une erreur de connexion avec le
                                            serveur LDAP, veuillez vérifier les paramètres
                                        </div>
                                    </c:when>
                                </c:choose>
                            </div>
                        </div>
                    </div>
                </div>
                <%-- ./Configuration du serveur LDAP --%>


                <%-- Configuration de la base de données --%>
                <div class="panel panel-default ">
                    <div class="panel-heading" align="center">Configuration de la base de données :</div>
                    <div class="panel-body">

                        <div class="row">
                            <form method="post" action="<c:url value="/GererApplication" />" id="modifierBDD">
                                <label class="col-lg-3 col-md-3" for="serveur">Adresse de la base de données : <span
                                        class="requis">*</span></label>
                                <div class="col-lg-3 col-md-3">
                                    <input type="text" name="adresseBdd" id="adresseBdd"
                                           class="form-control" value="<c:out value="${bdd[0]}"/>"
                                           autofocus required/> <br>
                                </div>

                                <label class="col-lg-3 col-md-3" for="port">Driver :</label>
                                <div class="col-lg-3 col-md-3">
                                    <input type="text" name="driver" id="driver" class="form-control"
                                           value="com.mysql.jdbc.Driver" disabled="disabled"/> <br>
                                </div>

                                <label class="col-lg-3 col-md-3" for="contexteDN">Utilisateur : <span
                                        class="requis">*</span></label>
                                <div class="col-lg-3 col-md-3">
                                    <input type="text" name="utilisateurBDD" id="utilisateurBDD"
                                           class="form-control" value="<c:out value="${bdd[2]}"/>"
                                           autofocus required/> <br>
                                </div>

                                <label class="col-lg-3 col-md-3" for="contexteDNUser">Mot de passe : <span
                                        class="requis">*</span></label>
                                <div class="col-lg-3 col-md-3">
                                    <input type="password" name="mdpBDD" id="mdpBDD"
                                           class="form-control" value="<c:out value="${bdd[3]}"/>"
                                           autofocus required/> <br>
                                </div>

                                <input type="hidden" name="formulaire" id="formulaire" value="modifierConfBdd"/>

                                <div class="col-lg-12 col-md-12">
                                    <button type="button" class="btn btn-primary center-block" data-toggle="modal"
                                            data-target="#modifierBddModal">
                                        <i class="fas fa-pencil-alt fa-fw" aria-hidden="true"></i>&nbsp; Modifier
                                    </button>
                                </div>
                            </form>

                            <%-- Reinitialiser les paramètres BDD --%>
                            <form method="post" action="<c:url value="/GererApplication"/>" id="reinitialiserBdd">
                                <input type="hidden" name="formulaire" id="formulaire" value="defautConfBDD"/>
                                <div class="col-lg-12 col-md-12">
                                    <br/>
                                    <button type="button" class="btn btn-primary center-block" data-toggle="modal"
                                            data-target="#modifierBddDefautModal">
                                        <i class="fas fa-redo fa-fw" aria-hidden="true"></i>&nbsp; Réinitialiser
                                        paramètres
                                    </button>
                                    <br>
                                </div>
                            </form>
                        </div>
                        <%-- ./Configuration de la base de données --%>

                        <%-- Test de connexion BDD --%>
                        <div class="row">
                            <div class="col-lg-12 col-md-12">
                                <c:choose>
                                    <c:when test="${bddConnexion}">
                                        <div class="alert alert-success" style="text-align: center" ; role="alert">
                                            Test de connexion : Tout semble fonctionner
                                        </div>
                                    </c:when>
                                    <c:when test="${!bddConnexion}">
                                        <div class="alert alert-danger" style="text-align: center" ; role="alert">
                                            Test de connexion : Echec du test, la modification n'a pas été prise en
                                            compte !
                                        </div>
                                    </c:when>
                                </c:choose>
                            </div>
                        </div>
                    </div>
                </div>


                <%-- Configuration des informations de stockage --%>
                <div class="panel panel-default ">
                    <div class="panel-heading" align="center">Informations de stockage :</div>
                    <div class="panel-body">


                        <div class="row">
                            <form method="post" action="<c:url value="/GererApplication" />" id="modifierStockage">

                                <label class="col-lg-3 col-md-3" for="cheminEnregPosters">Localisation du dépôt des
                                    posters: <span class="requis">*</span></label>
                                <div class="col-lg-3 col-md-3">
                                    <input type="text" name="cheminEnregPosters" id="cheminEnregPosters"
                                           class="form-control" value="<c:out value="${stockage[0]}"/>" autofocus
                                           required/> <br>
                                </div>

                                <label class="col-lg-3 col-md-3" for="cheminEnregSlides">Localisation du dépôt des
                                    slides: <span class="requis">*</span></label>
                                <div class="col-lg-3 col-md-3">
                                    <input type="text" name="cheminEnregSlides" id="cheminEnregSlides"
                                           class="form-control" value="<c:out value="${stockage[1]}"/>" autofocus
                                           required/> <br>
                                </div>

                                <label class="col-lg-3 col-md-3" for="tailleTamponFichier">Taille maximale
                                    d'enregistrement des fichiers (en Ko): <span class="requis">*</span></label>
                                <div class="col-lg-3 col-md-3">
                                    <input type="text" name="tailleTamponFichier" id="tailleTamponFichier"
                                           class="form-control" value="<c:out value="${stockage[2]}"/>" autofocus
                                           required/> <br>
                                </div>


                                <div class="col-lg-12 col-md-12">
                                    <button type="button" class="btn btn-primary center-block" data-toggle="modal"
                                            data-target="#modifierConfStockageModal">
                                        <i class="fas fa-redo fa-fw" aria-hidden="true"></i>&nbsp; Modifier
                                    </button>

                                </div>
                                <input type="hidden" name="formulaire" id="formulaire" value="modifierConfStockage"/>
                            </form>

                            <form method="post" action="<c:url value="/GererApplication"/>" id="reinitialiserStockage">
                                <input type="hidden" name="formulaire" id="formulaire" value="defaultConfStockage"/>
                                <div class="col-lg-12 col-md-12">
                                    <br/>
                                    <button type="button" class="btn btn-primary center-block" data-toggle="modal"
                                            data-target="#parDefautStockageModal">
                                        <i class="fas fa-redo fa-fw" aria-hidden="true"></i>&nbsp; Réinitialiser
                                        paramètres
                                    </button>
                                    <br>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>


                <%-- ./Configuration des informations de stockage --%>

                <%-- Modification de la charte graphique --%>
                <div class="panel panel-primary ">
                    <div class="panel-heading" align="center">Configuration de la charte graphique :</div>
                    <div class="panel-body">
                        <div class="row">
                            <%-- Modification du fichier de style --%>
                            <div class="col-lg-4 col-md-4">
                                <div class="panel panel-default ">
                                    <div class="panel-heading" align="center">Modifier fichier de style :</div>
                                    <div class="panel-body">
                                        <div id="code"></div>
                                        <br>

                                        <form method="post" action="<c:url value="/GererApplication"/>"
                                              id="formulaireChangerCss">

                                            <input type="hidden" name="formulaire" id="formulaire" value="modifierCss"/>

                                            <input type="hidden" name="texteCss" id="texteCss" value=""/>

                                            <button type="button" class="btn btn-primary center-block"
                                                    onclick="envoyerNouveauCSS();">
                                                <i class="fas fa-pencil-alt fa-fw" aria-hidden="true"></i>&nbsp;
                                                Modifier
                                            </button>
                                        </form>
                                        <br>
                                        <form method="post" action="<c:url value="/GererApplication"/>">
                                            <input type="hidden" name="formulaire" id="formulaire"
                                                   value="defautStyleCss"/>
                                            <button type="submit" class="btn btn-primary center-block" id="submit">
                                                <i class="fas fa-redo fa-fw" aria-hidden="true"></i>&nbsp; Réinitialiser
                                                paramètres
                                            </button>
                                        </form>

                                    </div>
                                </div>
                            </div>
                            <%-- ./Modification du fichier de style --%>

                            <%-- Modification des images --%>
                            <div class="col-lg-8 col-md-8">
                                <div class="panel panel-default ">
                                    <div class="panel-heading" align="center">Modifier les images de l'application :
                                    </div>
                                    <div class="panel-body">
                                        <form method="post" action="<c:url value="/GererApplication"/>"
                                              enctype="multipart/form-data" id="formulaireImage">
                                            <table class="table table-striped table-bordered">
                                                <tr>
                                                    <th>Image</th>
                                                    <th>Titre</th>
                                                    <th>Modifier</th>
                                                </tr>

                                                <c:forEach var="image" items="${imagesDansImages}">

                                                    <tr>
                                                        <td><img src="<c:url value = "/images/${image}"/>"
                                                                 class="img-responsive img-thumbnail" width="25%"
                                                                 height="25%"></td>
                                                        <td><c:out value="${image}"/></td>
                                                        <td><input type="file" class="form-control-file"
                                                                   id="<c:out value="${image}" />"
                                                                   name="<c:out value="${image}" />"
                                                                   onchange="ValidateSize(this)">

                                                        </td>
                                                    </tr>
                                                </c:forEach>
                                            </table>
                                            <button type="submit" class="btn btn-primary center-block">
                                                <i class="fas fa-pencil-alt fa-fw" aria-hidden="true"></i>&nbsp; Mettre
                                                à jour
                                            </button>
                                            <div class="text-center">
                                                <span class="erreur">${erreurs['extensionErreur']}</span>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <p align="center">Nous vous invitons à redémarrer votre navigateur après la modification</p>
                            <%-- ./Modification des images --%>

                        </div>
                    </div>
                </div>

                <%-- ./Modification de la charte graphique --%>

            </div>
        </div>

        <c:import url="/inc/footer.jsp"/>

        <%-- Modal : Avertissement de modification : Configuration LDAP --%>
        <div id="modifierConfModal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="alert alert-danger" role="alert">
                            <h4 class="alert-heading">Attention !</h4>
                            <p><b>Le changement des paramètres d'accès au serveur LDAP n'est pas sans risque.</b></p>
                            <p class="mb-0">En comprenant les risques, la modification des paramètres prendra effet dans
                                les prochainnes secondes et <b>vous serez déconnecté.</b></p>
                            <p class="mb-0">Il est aussi <b>vivement conseillé d'utiliser un compte admin présent dans
                                la base locale</b> et non sur le serveur LDAP. Si les paramètres rentrés sont faux, cela
                                permettra d'avoir un accès à l'application...</p>
                            <p class="mb-0"><b>Etes-vous sûr de vouloir changer ?</b></p>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Annuler</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal" onclick="submitModifier();">
                            Oui je comprends les risques
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <%-- ./Modal : Avertissement de modification : Configuration LDAP --%>

        <%-- Modal : Avertissement de modification : Configuration BDD --%>
        <div id="modifierBddModal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="alert alert-danger" role="alert">
                            <h4 class="alert-heading">Attention !</h4>
                            <p><b>Le changement des paramètres d'accès à la base de données n'est pas sans risque.</b>
                            </p>
                            <p class="mb-0">En comprenant les risques, la modification des paramètres prendra effet
                                après quelques secondes et <b>vous serez déconnecté.</b></p>
                            <p class="mb-0">Un test préalable va être effectué pour vérifier si le changement est
                                possible.</p>
                            <p class="mb-0"><b>Etes-vous sûr de vouloir changer ?</b></p>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Annuler</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal"
                                onclick="submitModifierBdd();">Oui je comprends les risques
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <%-- ./Modal : Avertissement de modification : Configuration BDD --%>

        <%-- Modal : Avertissement de modification : Configuration BDD par defaut --%>
        <div id="modifierBddDefautModal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="alert alert-danger" role="alert">
                            <h4 class="alert-heading">Attention !</h4>
                            <p><b>Le changement des paramètres d'accès à la base de données n'est pas sans risque.</b>
                            </p>
                            <p class="mb-0">En comprenant les risques, la modification des paramètres prendra effet
                                après quelques secondes et <b>vous serez déconnecté.</b></p>
                            <p class="mb-0">Un test préalable va être effectué pour vérifier si le changement est
                                possible.</p>
                            <p class="mb-0"><b>Etes-vous sûr de vouloir changer ?</b></p>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Annuler</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal"
                                onclick="submitModifierBddDefaut();">Oui je comprends les risques
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <%-- ./Modal : Avertissement de modification : Configuration BDD par defaut --%>

        <%-- Modal : Avertissement de modification : Configuration Stockage --%>
        <div id="modifierConfStockageModal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="alert alert-danger" role="alert">
                            <h4 class="alert-heading">Attention !</h4>
                            <p><b>Le changement des paramètres de stockage n'est pas sans risque.</b></p>
                            <p class="mb-0">En comprenant les risques, la modification des paramètres prendra effet dans
                                les prochainnes secondes et <b>vous serez déconnecté.</b></p>
                            <p class="mb-0">Il est aussi <b>vivement conseillé d'utiliser un compte admin présent dans
                                la base locale</b> et non sur le serveur LDAP. Si les paramètres rentrés sont faux, cela
                                permettra d'avoir un accès à l'application...</p>
                            <p class="mb-0"><b>Etes-vous sûr de vouloir changer ?</b></p>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Annuler</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal"
                                onclick="submitModifierStockage();">Oui je comprends les risques
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <%-- ./Modal : Avertissement de modification : Configuration Stockage --%>

        <%-- Modal : Avertissement de modification : Reinitialisation parametres --%>
        <div id="parDefautModal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="alert alert-danger" role="alert">
                            <h4 class="alert-heading">Attention!</h4>
                            <p><b>Le changement des paramètres d'accès au serveur LDAP n'est pas sans risque.</b></p>
                            <p class="mb-0">En comprenant les risques, la remise par défaut des paramètres prendra effet
                                immédiatement et <b>vous serez déconnecté.</b></p>
                            <p class="mb-0">Il est aussi <b>vivement conseillé d'utiliser un compte admin présent dans
                                la base locale</b> et non sur le serveur LDAP. Si les paramètres rentrés sont faux, cela
                                permettra d'avoir un accès à l'application...</p>
                            <p class="mb-0">Etes-vous sûr de vouloir changer ?</p>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Annuler</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal" onclick="submitParDefaut();">
                            Oui je comprends les risques
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <%-- Modal : Avertissement de modification : Reinitialisation parametres --%>

        <%-- Modal : Avertissement de modification : Reinitialisation parametres Stockage --%>
        <div id="parDefautStockageModal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="alert alert-danger" role="alert">
                            <h4 class="alert-heading">Attention!</h4>
                            <p><b>Le changement des paramètres de stockage n'est pas sans risque.</b></p>
                            <p class="mb-0">En comprenant les risques, la remise par défaut des paramètres prendra effet
                                immédiatement et <b>vous serez déconnecté.</b></p>
                            <p class="mb-0">Il est aussi <b>vivement conseillé d'utiliser un compte admin présent dans
                                la base locale</b> et non sur le serveur LDAP. Si les paramètres rentrés sont faux, cela
                                permettra d'avoir un accès à l'application...</p>
                            <p class="mb-0">Etes-vous sûr de vouloir changer ?</p>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Annuler</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal"
                                onclick="submitParDefautStockage();">Oui je comprends les risques
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<%-- Modal : Avertissement de modification : Reinitialisation parametres Stockage --%>

<c:import url="/inc/scripts.jsp"/>
<script src="<c:url value="/js/codemirror/codemirror.js"/>" charset="utf-8" type="application/javascript"></script>
<script src="<c:url value="/js/codemirror/css.js"/>" charset="utf-8" type="application/javascript"></script>
<script charset="UTF-8" type="application/javascript">

    function submitModifier() {
        document.getElementById('modifier').submit();
    }

    function submitModifierBdd() {
        document.getElementById('modifierBDD').submit();
    }

    function submitModifierBddDefaut() {
        document.getElementById('reinitialiserBdd').submit();
    }

    function submitModifierStockage() {
        document.getElementById('modifierStockage').submit();
    }

    function submitParDefaut() {
        document.getElementById('reinitialiser').submit();
    }

    function submitParDefautStockage() {
        document.getElementById('reinitialiserStockage').submit();
    }

    //Au chargement de la page
    window.onload = function () {
        editor = CodeMirror(document.getElementById("code"), {
            mode: "text/css",
            lineNumbers: true,
            lineSeparator: "¿",
            value: "<c:out value="${contenuStyleCss}" escapeXml="false"/>"
        });

        window.scrollTo(0, 0);
    };

    function envoyerNouveauCSS() {
        document.getElementById("texteCss").setAttribute('value', editor.getValue());
        document.getElementById('formulaireChangerCss').submit();
    }

    function confirmation() {
        return confirm("Êtes-vous sûr de vouloir continuer ? ATTENTION : Cette action est irréversible en cas d'erreur(s)");
    }

    function ValidateSize(file) {
        var FileSize = file.files[0].size / 1024 / 1024; // in MB
        if (FileSize > 2) {
            alert('Votre fichier est trop lourd. Limite : 2 MB');
            document.getElementById("formulaireImage").reset();
        } else {

        }
    }
</script>
</body>
</html>