<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <c:import url="/inc/head.jsp"/>
    <title>SoManager - Gestion des comptes</title>
</head>
<body>
<div class="wrapper">
    <c:import url="/Navbar"/>
    <!-- Page Content Holder -->
    <div id="content">
        <c:import url="/inc/headerForSidebar.jsp"/>

        <div class="panel panel-primary ">
            <div class="panel-heading">Administrateur - Gestion des Comptes</div>
            <div class="panel-body">
                <c:import url="/inc/callback.jsp"/>

                <ul class="nav nav-tabs" role="tablist" style="margin-bottom: 20px">
                    <li class="active"><a data-toggle="tab" href="#gererComptes">
                        <i class="fas fa-key fa-fw" aria-hidden="true"></i>&nbsp;
                        Gérer les comptes
                    </a></li>
                    <li><a data-toggle="tab" href="#importerListeUtilisateurs">
                        <i class="fas fa-address-book fa-fw" aria-hidden="true"></i>&nbsp;
                        Importer utilisateurs
                    </a></li>
                    <li><a data-toggle="tab" href="#ajoutUtilisateurBDD">
                        <i class="fas fa-user-plus fa-fw" aria-hidden="true"></i>&nbsp;
                        Ajouter un utilisateur
                    </a></li>
                    <li><a data-toggle="tab" href="#synchroniser">
                        <i class="fas fa-sync-alt fa-fw" aria-hidden="true"></i>&nbsp;
                        Synchroniser l'annuaire
                    </a></li>
                </ul>

                <%-- Contenu des différentes tab --%>
                <div class="tab-content">

                    <%-- Gérer les comptes--%>
                    <div id="gererComptes" class="tab-pane fade in active">
                        <div class="row">
                            <c:import url="/WEB-INF/formulaires/gererComptes/listeUtilisateurs.jsp"/>
                        </div>
                    </div>

                    <%-- Importer liste d'utilisateurs --%>
                    <div id="importerListeUtilisateurs" class="tab-pane fade">
                        <div class="row">
                            <c:import url="/WEB-INF/formulaires/gererComptes/importerListeUtilisateurs.jsp"/>
                        </div>
                    </div>

                    <%-- Ajouter manuellement un utilisateur --%>
                    <div id="ajoutUtilisateurBDD" class="tab-pane fade">
                        <div class="row">
                            <c:import url="/WEB-INF/formulaires/gererComptes/ajouterUtilisateurManuel.jsp"/>
                        </div>
                    </div>

                    <%-- Synchroniser l'annuaire--%>
                    <div id="synchroniser" class="tab-pane fade">
                        <div class="row">
                            <c:import url="/WEB-INF/formulaires/gererComptes/synchroniserAgenda.jsp"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<%-- Modal : Avertissement suppression de comptes inutiles : Gestion des comptes --%>
<div id="supprimerModalId" class="modal fade" role="dialog">
    <c:import url="/WEB-INF/formulaires/gererComptes/modalConfirmerSuppression.jsp"/>
</div>
<%-- ./Modal : Avertissement de suppression de comptes inutiles : Gestion des comptes --%>

<%-- Modal : Changement de mot de passe utilisateur --%>
<div id="changerPwModalId" class="modal fade" role="dialog">
    <c:import url="/WEB-INF/formulaires/gererComptes/modalMotDePasse.jsp"/>
</div>
<%-- ./Modal : Changement de mot de passe utilisateur --%>

<!-- Scripts -->
<c:import url="/inc/scripts.jsp"/>
<script type="application/javascript" src="<c:url value="/js/scripts/gererComptes.js"/>"></script>
<script type="application/javascript" src="<c:url value="/js/perfect-scrollbar.min.js"/>"></script>
<script type="application/javascript" src="<c:url value="/js/scripts/filtrageUtilisateur.js"/>"></script>
</body>
</html>