<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <c:import url="/inc/head.jsp"/>
    <title>SoManager - Bonus et Malus</title>
    <link rel="stylesheet" href="<c:url value="/css/jquery.typeahead.css"/>">
    <link rel="stylesheet" media="screen"
          href="<c:url value="/css/bootstrap-datetimepicker.min.css"/>">
    <link rel="stylesheet" href="<c:url value="/css/popup.css"/>">
</head>
<body>

<div class="wrapper">
    <c:import url="/Navbar"/>
    <!-- Page Content Holder -->
    <div id="content">
        <c:import url="/inc/headerForSidebar.jsp"/>

        <div class="panel panel-primary ">
            <div class="panel-heading">Gérer les bonus et les malus</div>
            <div class="panel-body">
                <c:import url="/inc/callback.jsp"/>

                <div class="col-sm-12">

                    <form action="<c:url value='BonusMalus' />" method="post">

                        <div class="panel panel-primary" style="max-height: 100%;">
                            <div class="panel-heading" style="padding-bottom: 0">
                                <div class="row">
                                    <div class="col-sm-6 no-padding">
                                        <!-- Filtre année scolaire -->

                                        <div class="col-sm-4">
                                            <div class="form-group full-width">
                                                <label for="anneeDispo">Année: </label> <select
                                                    id="anneeDispo" class="form-control input-sm selectpicker"
                                                    style="text-transform: capitalize" name="anneeSelect">
                                                <option value="none" selected>...</option>
                                                <c:forEach var="listeAnneeScolaires"
                                                           items="${listeAnneeScolaire}" varStatus="loop">
                                                    <option value="${listeAnneeScolaires.idAnneeScolaire}"
                                                            id="annee-${listeAnneeScolaires.idAnneeScolaire}"
                                                            <c:if test="${iDanneeSelect eq listeAnneeScolaires.idAnneeScolaire}"> selected</c:if>>${listeAnneeScolaires.anneeDebut}-${listeAnneeScolaires.anneeFin}</option>
                                                </c:forEach>
                                            </select>
                                            </div>
                                        </div>

                                        <div class="col-sm-4">
                                            <div class="form-group full-width">
                                                <label for="teamDispo">Team: </label> <select id="teamDispo"
                                                                                              class="form-control input-sm selectpicker"
                                                                                              style="text-transform: capitalize"
                                                                                              name="equipeSelect">
                                                <option value="none" selected>...</option>

                                                <c:if test="${productOwner eq 'true'}">
                                                    <c:forEach var="listePglEquipe" items="${listePglEquipe}"
                                                               varStatus="loop">
                                                        <option value="${listePglEquipe.idEquipe}"
                                                                id="equipe-${listePglEquipe.nom}"
                                                                <c:if test="${iDequipeSelect eq listePglEquipe.idEquipe}"> selected</c:if>>${listePglEquipe.nom}</option>
                                                    </c:forEach>
                                                </c:if>

                                                <c:if test="${scrumMaster eq 'true'}">
                                                    <c:forEach var="listePglEquipe" items="${listePglEquipe}"
                                                               varStatus="loop">
                                                        <c:forEach var="listePglEtudiantEquipe"
                                                                   items="${listePglEtudiantEquipe}">
                                                            <c:if test="${listePglEquipe.idEquipe eq listePglEtudiantEquipe.idEquipe
																	&& listePglEtudiantEquipe.idEtudiant eq utilisateur.getIdUtilisateur()}">
                                                                <option value="${listePglEquipe.idEquipe}"
                                                                        id="equipe-${listePglEquipe.nom}"
                                                                        <c:if test="${iDequipeSelect eq listePglEquipe.idEquipe}"> selected</c:if>>${listePglEquipe.nom}</option>
                                                            </c:if>
                                                        </c:forEach>
                                                    </c:forEach>
                                                </c:if>

                                            </select>
                                            </div>
                                        </div>

                                        <div class="col-sm-4">
                                            <div class="form-group full-width">
                                                <label for="sprintPresent">Sprint n°: </label>
                                                <select
                                                        id="sprintPresent"
                                                        class="form-control input-sm selectpicker"
                                                        style="text-transform: capitalize" name="sprintSelect">
                                                    <option value="none" selected>...</option>
                                                    <c:forEach var="listeSprintDAO" items="${listeSprintDAO}"
                                                               varStatus="loop">
                                                        <option value="${listeSprintDAO.idSprint}"
                                                                id="sprint-${listeSprintDAO.numero}"
                                                                <c:if test="${iDsprintSelect eq listeSprintDAO.idSprint}"> selected</c:if>>${listeSprintDAO.numero}</option>
                                                    </c:forEach>
                                                </select>
                                            </div>
                                        </div>

                                    </div>

                                </div>
                                <!-- /.panel-header -->
                            </div>

                            <!-- Tableau de résultats -->
                            <div class="panel-body table-container">
                                <table class="table table-bordered" style="margin-bottom: -1px">
                                    <tr class="info unselectable">
                                        <th class="col-sm-2">Membres</th>
                                        <th class="col-sm-5">Bonus/Malus</th>
                                        <th class="col-sm-3">Justification</th>
                                        <th class="col-sm-1">Etat</th>
                                    </tr>
                                </table>

                                <!-- Contenu du tableau -->
                                <div class="row">
                                    <div class="col-sm-12" id="listeBonusMalusContainer"
                                         style="height: 300px;">
                                        <table class="table table-hover" style="margin-bottom: 0">
                                            <tbody id="listeBonusMalus">

                                            <c:forEach var="listeBonusMalus" items="${listeBonusMalus}"
                                                       varStatus="loop">


                                                <input type="hidden" name="id-${loop.index}"
                                                       value="${listeBonusMalus.idBonusMalus}">
                                                <input type="hidden" name="refSprint-${loop.index}"
                                                       value="${listeBonusMalus.refSprint}">
                                                <input type="hidden" name="refEvaluateur-${loop.index}"
                                                       value="${listeBonusMalus.refEvaluateur}">

                                                <!-- Le nom des membres de l'équipes -->
                                                <tr>
                                                    <td class="col-sm-2" width="100"><c:forEach
                                                            var="listeUtilisateur" items="${listeUtilisateurs}"
                                                            varStatus="loop2">
                                                        <c:if
                                                                test="${listeBonusMalus.refEtudiant eq listeUtilisateur.idUtilisateur}">
                                                            ${listeUtilisateur.nom} ${listeUtilisateur.prenom} <input
                                                                type="hidden" name="etudiant-${loop.index}"
                                                                value="${listeBonusMalus.refEtudiant}">
                                                        </c:if>
                                                    </c:forEach></td>
                                                    <!-- Les boutons radios pour les malus et bonus -->
                                                    <td class="col-sm-5">
                                                        <div style="text-align: center;">

                                                            <c:choose>
                                                                <c:when test="${listeBonusMalus.valeur eq '-4'}">
                                                                    <input type="radio" name="radio-${loop.index}"
                                                                           value="-4" checked>
                                                                    <label for="contactChoice">-4</label>
                                                                </c:when>
                                                                <c:otherwise>
                                                                    <input type="radio" name="radio-${loop.index}"
                                                                           value="-4">
                                                                    <label for="contactChoice">-4</label>
                                                                </c:otherwise>
                                                            </c:choose>

                                                            <font class="fas fa-chevron-left"
                                                                  style="color: #FE1B00"></font>

                                                            <c:choose>
                                                                <c:when test="${listeBonusMalus.valeur eq '-3'}">
                                                                    <input type="radio" name="radio-${loop.index}"
                                                                           value="-3" checked>
                                                                    <label for="contactChoice2">-3</label>
                                                                </c:when>
                                                                <c:otherwise>
                                                                    <input type="radio" name="radio-${loop.index}"
                                                                           value="-3">
                                                                    <label for="contactChoice2">-3</label>
                                                                </c:otherwise>
                                                            </c:choose>

                                                            <font class="fas fa-chevron-left"
                                                                  style="color: #F4661B"></font>

                                                            <c:choose>
                                                                <c:when test="${listeBonusMalus.valeur eq '-2'}">
                                                                    <input type="radio" name="radio-${loop.index}"
                                                                           value="-2" checked>
                                                                    <label for="contactChoice3">-2</label>
                                                                </c:when>
                                                                <c:otherwise>
                                                                    <input type="radio" name="radio-${loop.index}"
                                                                           value="-2">
                                                                    <label for="contactChoice3">-2</label>
                                                                </c:otherwise>
                                                            </c:choose>

                                                            <font class="fas fa-chevron-left"
                                                                  style="color: #FFA500"></font>

                                                            <c:choose>
                                                                <c:when test="${listeBonusMalus.valeur eq '-1'}">
                                                                    <input type="radio" name="radio-${loop.index}"
                                                                           value="-1" checked>
                                                                    <label for="contactChoice4">-1</label>
                                                                </c:when>
                                                                <c:otherwise>
                                                                    <input type="radio" name="radio-${loop.index}"
                                                                           value="-1">
                                                                    <label for="contactChoice4">-1</label>
                                                                </c:otherwise>
                                                            </c:choose>

                                                            <font class="fas fa-chevron-left"
                                                                  style="color: #FDEE00"></font>

                                                            <c:choose>
                                                                <c:when
                                                                        test="${listeBonusMalus.valeur eq '0' || listeBonusMalus.valeur eq null}">
                                                                    <input type="radio" name="radio-${loop.index}"
                                                                           value="0" checked>
                                                                    <label for="contactChoice5">0</label>
                                                                </c:when>
                                                                <c:otherwise>
                                                                    <input type="radio" name="radio-${loop.index}"
                                                                           value="0">
                                                                    <label for="contactChoice5">0</label>
                                                                </c:otherwise>
                                                            </c:choose>

                                                            <font class="fas fa-chevron-right"
                                                                  style="color: #C2F732"></font>

                                                            <c:choose>
                                                                <c:when test="${listeBonusMalus.valeur eq '1'}">
                                                                    <input type="radio" name="radio-${loop.index}"
                                                                           value="1" checked>
                                                                    <label for="contactChoice6">1</label>
                                                                </c:when>
                                                                <c:otherwise>
                                                                    <input type="radio" name="radio-${loop.index}"
                                                                           value="1">
                                                                    <label for="contactChoice6">1</label>
                                                                </c:otherwise>
                                                            </c:choose>

                                                            <font class="fas fa-chevron-right"
                                                                  style="color: #ADFF2F"></font>

                                                            <c:choose>
                                                                <c:when test="${listeBonusMalus.valeur eq '2'}">
                                                                    <input type="radio" name="radio-${loop.index}"
                                                                           value="2" checked>
                                                                    <label for="contactChoice7">2</label>
                                                                </c:when>
                                                                <c:otherwise>
                                                                    <input type="radio" name="radio-${loop.index}"
                                                                           value="2">
                                                                    <label for="contactChoice7">2</label>
                                                                </c:otherwise>
                                                            </c:choose>

                                                            <font class="fas fa-chevron-right"
                                                                  style="color: #3AF24B"></font>

                                                            <c:choose>
                                                                <c:when test="${listeBonusMalus.valeur eq '3'}">
                                                                    <input type="radio" name="radio-${loop.index}"
                                                                           value="3" checked>
                                                                    <label for="contactChoice8">3</label>
                                                                </c:when>
                                                                <c:otherwise>
                                                                    <input type="radio" name="radio-${loop.index}"
                                                                           value="3">
                                                                    <label for="contactChoice8">3</label>
                                                                </c:otherwise>
                                                            </c:choose>

                                                            <font class="fas fa-chevron-right"
                                                                  style="color: #34C924"></font>

                                                            <c:choose>
                                                                <c:when test="${listeBonusMalus.valeur eq '4'}">
                                                                    <input type="radio" name="radio-${loop.index}"
                                                                           value="4" checked>
                                                                    <label for="contactChoice9"> 4</label>
                                                                </c:when>
                                                                <c:otherwise>
                                                                    <input type="radio" name="radio-${loop.index}"
                                                                           value="4">
                                                                    <label for="contactChoice9"> 4</label>
                                                                </c:otherwise>
                                                            </c:choose>

                                                        </div>
                                                    </td>

                                                    <!-- Champs de text, commentaire -->
                                                    <td class="col-sm-3"><input type="text"
                                                                                class="form-control input-sm" size="47"
                                                                                value="${listeBonusMalus.justification}"
                                                                                name="justification-${loop.index}"/>
                                                    </td>

                                                    <!-- Les boutons d'attente refus et acceptation -->
                                                    <td class="col-sm-1" style="text-align: center;">

                                                        <c:choose>
                                                            <c:when test="${nonEnvoyer eq false}">


                                                                <c:choose>
                                                                    <c:when
                                                                            test="${listeBonusMalus.validation eq 'ATTENTE'}">
                                                                        <font class="far fa-clock" size="4"
                                                                              style="color: #FFA500"></font>
                                                                        <input type="hidden"
                                                                               value="${listeBonusMalus.validation}"
                                                                               name="validation-${loop.index}">
                                                                    </c:when>
                                                                    <c:otherwise>
                                                                        <font class="far fa-clock" size="4"
                                                                              style="color: grey"></font>
                                                                        <input type="hidden"
                                                                               value="${listeBonusMalus.validation}"
                                                                               name="validation-${loop.index}">
                                                                    </c:otherwise>
                                                                </c:choose> <c:choose>
                                                                <c:when
                                                                        test="${listeBonusMalus.validation eq 'REFUSER'}">
                                                                    <font class="far fa-times-circle" size="4"
                                                                          style="color: red"></font>
                                                                    <input type="hidden"
                                                                           value="${listeBonusMalus.validation}"
                                                                           name="validation-${loop.index}">
                                                                </c:when>
                                                                <c:otherwise>
                                                                    <font class="far fa-times-circle" size="4"
                                                                          style="color: grey"></font>
                                                                    <input type="hidden"
                                                                           value="${listeBonusMalus.validation}"
                                                                           name="validation-${loop.index}">
                                                                </c:otherwise>
                                                            </c:choose> <c:choose>
                                                                <c:when
                                                                        test="${listeBonusMalus.validation eq 'ACCEPTER'}">
                                                                    <font class="far fa-check-circle" size="4"
                                                                          style="color: #34C924"></font>
                                                                    <input type="hidden"
                                                                           value="${listeBonusMalus.validation}"
                                                                           name="$validation-${loop.index}">
                                                                </c:when>
                                                                <c:otherwise>
                                                                    <font class="far fa-check-circle" size="4"
                                                                          style="color: grey"></font>
                                                                    <input type="hidden"
                                                                           value="${listeBonusMalus.validation}"
                                                                           name="validation-${loop.index}">
                                                                </c:otherwise>
                                                            </c:choose>

                                                            </c:when>
                                                            <c:otherwise>
                                                                <font class="far fa-clock" size="4"
                                                                      style="color: grey"></font>
                                                                <input type="hidden"
                                                                       value="${listeBonusMalus.validation}"
                                                                       name="validation-${loop.index}">
                                                                <font class="far fa-times-circle" size="4"
                                                                      style="color: grey"></font>
                                                                <input type="hidden"
                                                                       value="${listeBonusMalus.validation}"
                                                                       name="validation-${loop.index}">
                                                                <font class="far fa-check-circle" size="4"
                                                                      style="color: grey"></font>
                                                                <input type="hidden"
                                                                       value="${listeBonusMalus.validation}"
                                                                       name="validation-${loop.index}">
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </td>
                                                </tr>
                                            </c:forEach>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                            <!-- Options de validation definitive et d'envoie -->
                            <div class="panel-footer" style="padding-bottom: 10">
                                <div class="row">
                                    <div style="text-align:center;">

                                        <c:choose>
                                            <c:when test="${elementPresentDansTableau eq true}">
                                                <div>

                                                    <c:choose>
                                                        <c:when test="${toutValide eq false}">
                                                            <button type="submit" class="btn btn-primary"
                                                                    id="envoyerBonusMalus">
                                                                <span class="fas fa-share-square fa-fw"></span> Valider
                                                                les bonus/malus
                                                            </button>
                                                        </c:when>
                                                        <c:otherwise>
                                                            <button type="submit" class="btn btn-primary"
                                                                    id="envoyerBonusMalus" disabled="disabled">
                                                                <span class="fas fa-share-square fa-fw"></span> Valider
                                                                les bonus/malus
                                                            </button>
                                                        </c:otherwise>
                                                    </c:choose>

                                                </div>

                                            </c:when>
                                            <c:otherwise>
                                                <div>

                                                    <button type="submit" class="btn btn-primary"
                                                            id="envoyerBonusMalus" disabled="disabled">
                                                        <span class="fas fa-share-square fa-fw"></span> Valider
                                                        les bonus/malus
                                                    </button>

                                                </div>
                                            </c:otherwise>
                                        </c:choose>


                                    </div>
                                </div>
                            </div>
                        </div>

                    </form>

                </div>
            </div>
        </div>
    </div>
</div>


<!-- Scripts -->
<c:import url="/inc/scripts.jsp"/>
<script type="application/javascript"
        src="<c:url value="/js/perfect-scrollbar.min.js"/>"></script>
<script type="application/javascript"
        src="<c:url value="/js/scripts/filtrageUtilisateur.js"/>"></script>
<script type="application/javascript">

    $(function () {
        function changePage() {
            var ad = $("#anneeDispo").val();
            var es = $("#teamDispo").val();
            var ss = $("#sprintPresent").val();
            window.location.replace(_CONTEXTPATH + "/BonusMalus?anneeSelect=" + ad + "&equipeSelect=" + es + "&sprintSelect=" + ss);
        };
        $("#anneeDispo").on('change', changePage);
        $("#teamDispo").on('change', changePage);
        $("#sprintPresent").on('change', changePage);

    });

</script>

</body>
</html>

