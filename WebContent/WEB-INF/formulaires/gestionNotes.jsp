<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <c:import url="/inc/head.jsp"/>
    <title>SoManager - Gérer les Notes</title>
    <link rel="stylesheet" href="<c:url value="/css/jquery.typeahead.css"/>">
    <link rel="stylesheet" media="screen"
          href="<c:url value="/css/bootstrap-datetimepicker.min.css"/>">
</head>
<body>
<div class="wrapper">
    <c:import url="/Navbar"/>
    <!-- Page Content Holder -->
    <div id="content">
        <c:import url="/inc/headerForSidebar.jsp"/>


        <div class="panel panel-primary ">
            <div class="panel-heading">Notes - Gestion des notes</div>

            <div class="panel-body">

                <c:import url="/inc/callback.jsp"/>

                <ul class="nav nav-tabs" role="tablist" style="margin-bottom: 20px">
                    <li class="active"><a data-toggle="tab" href="#gererNotes">
                        <i class="fas fa-edit fa-fw" aria-hidden="true"></i>&nbsp;
                        PFE
                    </a></li>
                    <li><a data-toggle="tab" href="#gestionPGL_Notes">
                        <i class="fas fa-comments fa-fw" aria-hidden="true"></i>&nbsp;
                        PGL
                    </a></li>
                </ul>

                <%-- Contenu des différentes tab --%>
                <div class="tab-content">

                    <%-- Gérer les notesPDE--%>
                    <div id="gererNotes" class="tab-pane fade in active">
                        <div class="row">
                            <c:import url="/WEB-INF/formulaires/gestionNotes/gestionNotesProjet.jsp"/>
                        </div>
                    </div>

                    <%-- Gerer notes pgl --%>
                    <div id="gestionPGL_Notes" class="tab-pane fade">
                        <div class="row">
                            <c:import url="/WEB-INF/formulaires/gestionNotes/gestionPGL_Notes.jsp"/>
                        </div>
                    </div>

                </div>

            </div>
            s</div>
    </div>
</div>
<script src="<c:url value="/js/dragAndDrop.js"/>"></script>
<c:import url="/inc/scripts.jsp"/>
<script src="<c:url value="/js/jquery.typeahead.js"/>"  type="text/javascript"></script>
<script type="application/javascript" src="<c:url value="/js/perfect-scrollbar.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/js/bootstrap-datetimepicker.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/js/bootstrap-datetimepicker.fr.js"/>"></script>
<script>
    typeof $.typeahead === 'function' && $.typeahead({
        input: ".js-typeahead",
        minLength: 1,
        maxItem: 5,
        order: "asc",
        hint: false,
        emptyTemplate: 'Aucun resultat pour "{{query}}"',
        source: data
    });
</script>
</body>
</html>

