<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <c:import url="/inc/head.jsp"/>
    <link rel="stylesheet" href="<c:url value="/css/trombi.css"/>">
    <title>SoManager - Modifier mes informations</title>
</head>
<body>
<div class="wrapper">
    <c:import url="/Navbar"/>
    <!-- Page Content Holder -->
    <div id="content">
        <c:import url="/inc/headerForSidebar.jsp"/>

        <div class="row">
            <div class="col-lg-8 col-lg-offset-2">
                <div class="panel panel-primary ">
                    <div class="panel-heading">Profil - Mes informations</div>
                    <div class="panel-body">
                        <form data-toggle="validator" role="form" method="post"
                              onsubmit="return checkForm(this)"
                              action="<c:url value="ModifierInfoUtilisateur"/>"
                              enctype="multipart/form-data">
                            <fieldset>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-lg-2">
                                            <div class="cadrephoto">
                                                <object class="photoprofil"
                                                        data="<c:url value="/images/users/${sessionScope.utilisateur.idUtilisateur}.jpg"/>">
                                                    <img src="<c:url value="/images/users/def_user.png"/>"
                                                         alt="Impossible d'afficher la photo.">
                                                </object>
                                                <input type="file" id="file_photo" name="file_photo"
                                                       accept=".jpg, .png" class="submitprofil">
                                            </div>
                                        </div>
                                        <div class="col-lg-9 col-lg-push-1">
                                            <label for="nom"> Nom : </label> <input type="text"
                                                                                    name="nom" class="form-control"
                                                                                    id="nom"
                                                                                    value="<c:out value="${sessionScope.utilisateur.nom}"/>"/>

                                            <label for="prenom" style="margin-top: 10px"> Prénom
                                                : </label> <input type="text" name="prenom" class="form-control"
                                                                  id="prenom"
                                                                  value="<c:out value="${sessionScope.utilisateur.prenom}"/>"/>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="email" class=""> Email : </label> <input
                                        type="text" name="email" class="form-control" id="email"
                                        value="<c:out value="${sessionScope.utilisateur.email} "/>"/>
                                </div>

                                <div class="form-group">
                                    <label for="role" class="control-label">Role(s) :</label>

                                    <ul>
                                        <c:forEach var="role" items="${roles}" varStatus="loop">
                                            <li>${role.nomRole}</li>
                                        </c:forEach>
                                    </ul>
                                </div>
                                <div class="form-group">
                                    <label for="option" class="control-label">Option(s) : </label>

                                    <ul>
                                        <c:forEach var="option" items="${listeOptions}"
                                                   varStatus="loop">
                                            <c:if test="${ not empty option.nomOption }">
                                                <li>${option.nomOption}</li>
                                            </c:if>

                                        </c:forEach>
                                    </ul>

                                </div>
                                <br>
                            </fieldset>
                            <div class="text-center">
                                <button type="submit" class="btn btn-primary"
                                        id="modifierInfoUtilisateur">
                                    <i class="fas fa-pencil-alt"></i>&nbsp; Valider mes
                                    informations
                                </button>
                                <a href="ChangerMotDePasse" class="btn btn-primary"
                                   id="changerMotDePasse"> <i class="fas fa-pencil-alt"></i>&nbsp;
                                    Modifier mon mot de passe
                                </a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<c:import url="/inc/scripts.jsp"/>
<script src="<c:url value="/js/jquery.typeahead.js"/>"
        type="text/javascript"></script>


</body>
</html>