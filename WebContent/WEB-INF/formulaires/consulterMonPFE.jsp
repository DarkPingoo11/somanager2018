<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <c:import url="/inc/head.jsp"/>
    <title>SoManager - Mon sujet de PFE</title>
</head>
<body>
<div class="wrapper">
    <c:import url="/Navbar"/>
    <!-- Page Content Holder -->
    <div id="content">
        <c:import url="/inc/headerForSidebar.jsp"/>

        <div class="panel panel-primary ">

            <div class="panel-heading">Sujet - Consulter mon sujet de PFE</div>
            <div class="panel-body">
                <form action="<c:url value="/DeposerPoster"/>" method="post" enctype="multipart/form-data">
                    <fieldset>
                        <legend> Dépôt du poster</legend>
                        <label for="fichier">Emplacement du poster <span
                                class="requis">*</span></label> <input type="file"
                                                                       accept="application/pdf" id="poster"
                                                                       name="poster"/> <input
                            type="hidden" name="idSujet" value="${sujet.idSujet}"/> <input
                            type="hidden" name="dateLimiteDepot" value="${sujetPFE.idSujet}"/>
                        <span class="succes"><c:out value="${fichier}"/></span> <br/>
                        <input type="submit" value="Deposer"/>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
</div>
<c:import url="/inc/scripts.jsp"/>
</body>
</html>