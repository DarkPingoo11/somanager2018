<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <c:import url="/inc/head.jsp"/>
    <title>SoManager - Partager mon role</title>
    <link rel="stylesheet" href="<c:url value="/css/jquery.typeahead.css"/>">
    <link rel="stylesheet" media="screen"
          href="<c:url value="/css/bootstrap-datetimepicker.min.css"/>">
</head>
<body>
<div class="wrapper">
    <c:import url="/Navbar"/>
    <!-- Page Content Holder -->
    <div id="content">
        <c:import url="/inc/headerForSidebar.jsp"/>

        <div class="panel panel-primary ">
            <div class="panel-heading">Professeur - Partager mon rôle</div>
            <div class="panel-body">
                <div class="col-lg-6 col-md-6">
                    <div class="panel panel-default ">
                        <div class="panel-heading" align="center">Partage en cours :</div>
                        <div class="panel-body">
                            <c:choose>
                                <c:when test="${!empty listePartage}">
                                    <table class="table table-striped table-hover">
                                        <tr>
                                            <th>ID du partage</th>
                                            <th>Utilisateur</th>
                                            <th>Date de début</th>
                                            <th>Date de fin</th>
                                            <th>En cours</th>
                                            <th>Supprimer</th>

                                        </tr>

                                        <c:forEach items="${listePartage}" var="element">
                                        <form method="post" action="<c:url value="/PartagerRole"/>">
                                            <tr>

                                                <td><c:out value="${element[0]}"/></td>
                                                <td><c:out value="${element[1]}"/></td>
                                                <td><c:out value="${element[2]}"/></td>
                                                <td><c:out value="${element[3]}"/></td>
                                                <td><c:out value="${element[4]}"/></td>
                                                <td>
                                                    <button type="submit" class="btn btn-danger center-block"
                                                            id="submit"><span class="glyphicon glyphicon-remove"></span>Supprimer
                                                    </button>
                                                </td>
                                            </tr>
                                            <input type="hidden" name="formulaire" id="formulaire"
                                                   value="supprimerPartage"/>
                                            <input type="hidden" name="partageRoleChoisi" id="partageRoleChoisi"
                                                   value="<c:out value="${element[0]}" />"/>
                                            </from>
                                            </c:forEach>
                                    </table>
                                </c:when>
                                <c:otherwise>
                                    <p align="center">Il n'y a aucun partage en cours</p>
                                </c:otherwise>
                            </c:choose>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6">
                    <div class="panel panel-default ">
                        <div class="panel-heading" align="center">Ajouter un partage</div>
                        <div class="panel-body">
                            <form method="post" action="<c:url value="/PartagerRole"/>">

                                <div class="col-lg-12 col-md-12">
                                    <select class="form-control" id="selectRole" name="selectRole">
                                        <c:forEach items="${texteProfResponsable}" var="element">
                                            <option value="${element}">${element}</option>
                                        </c:forEach>
                                    </select>
                                </div>

                                <label class="col-lg-12 col-md-12"><br/>Nom du professeur : <span
                                        class="requis">*</span></label>

                                <div class="col-lg-12 col-md-12">
                                    <div class="typeahead__container">
                                        <div class="typeahead__field">
											<span class="typeahead__query">
											<input class="js-typeahead" name="utilisateurRecherche"
                                                   id="utilisateurRecherche" type="search" autofocus autocomplete="off"> </span>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-12 col-md-12" align="center">
                                    <span class="erreur">${erreurs['professeur']}</span>
                                </div>

                                <label class="col-lg-12 col-md-12"><br/>Date et heure de début du partage : <span
                                        class="requis">*</span></label>
                                <div class="col-lg-6 col-md-6">
                                    <div class="input-group date form_date" data-date="" data-date-format="dd MM yyyy"
                                         data-link-field="dateDebut" data-link-format="yyyy-mm-dd">
                                        <input class="form-control" size="16" type="text" value="" readonly>
                                        <span class="input-group-addon">
										<span class="glyphicon glyphicon-remove"></span></span>
                                        <span class="input-group-addon">
										<span class="glyphicon glyphicon-calendar"></span></span>
                                    </div>
                                    <input type="hidden" id="dateDebut" name="dateDebut" value=""/><br/>
                                </div>

                                <div class="col-lg-6 col-md-6">
                                    <div class="input-group date form_time" data-date="" data-date-format="hh:ii">
                                        <input class="form-control" id="heureDebut" name="heureDebut" size="16"
                                               type="text" value="" readonly>
                                        <span class="input-group-addon">
										<span class="glyphicon glyphicon-remove"></span></span>
                                        <span class="input-group-addon">
										<span class="glyphicon glyphicon-time"></span></span>
                                    </div>
                                </div>


                                <label class="col-lg-12 col-md-12"><br/>Date et heure de fin du partage : <span
                                        class="requis">*</span></label>
                                <div class="col-lg-6 col-md-6">
                                    <div class="input-group date form_date" data-date="" data-date-format="dd MM yyyy"
                                         data-link-field="dateFin" data-link-format="yyyy-mm-dd">
                                        <input class="form-control" size="16" type="text" value="" readonly>
                                        <span class="input-group-addon">
										<span class="glyphicon glyphicon-remove"></span></span>
                                        <span class="input-group-addon">
										<span class="glyphicon glyphicon-calendar"></span></span>
                                    </div>
                                    <input type="hidden" id="dateFin" name="dateFin" value=""/><br/>
                                </div>

                                <div class="col-lg-6 col-md-6">
                                    <div class="input-group date form_time" data-date="" data-date-format="hh:ii">
                                        <input class="form-control" id="heureFin" name="heureFin" size="16" type="text"
                                               value="" readonly>
                                        <span class="input-group-addon">
										<span class="glyphicon glyphicon-remove"></span></span>
                                        <span class="input-group-addon">
										<span class="glyphicon glyphicon-time"></span></span>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-6" align="center">
                                    <span class="erreur">${erreurs['dateHeure']}<br/></span>
                                </div>

                                <button type="submit" class="btn btn-primary center-block" id="submit">
                                    <span class="glyphicon glyphicon-plus"></span> Ajouter
                                </button>

                                <input type="hidden" name="formulaire" id="formulaire" value="partagerRole"/>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<c:import url="/inc/scripts.jsp"/>
<script src="<c:url value="/js/jquery.typeahead.js"/>" type="text/javascript"></script>

<script type="text/javascript" src="<c:url value="/js/bootstrap-datetimepicker.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/js/bootstrap-datetimepicker.fr.js"/>"></script>

<script>
    var listeImporte = "<c:out value="${listeNomPrenomID}"/>".split(",")
    var data = [""]

    for (var iter = 0; iter < listeImporte.length; iter++) {
        data.push(listeImporte[iter]);
    }

    typeof $.typeahead === 'function' && $.typeahead({
        input: ".js-typeahead",
        minLength: 1,
        maxItem: 5,
        order: "asc",
        hint: false,
        emptyTemplate: 'Aucun resultat pour "{{query}}"',
        source: data
    });


    $('.form_date').datetimepicker({
        language: 'fr',
        weekStart: 1,
        todayBtn: 1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 2,
        minView: 2,
        forceParse: 0
    });
    $('.form_time').datetimepicker({
        language: 'fr',
        weekStart: 1,
        todayBtn: 1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 1,
        minView: 0,
        maxView: 1,
        forceParse: 0
    });

</script>
</body>

</html>