<%--
  Created by IntelliJ IDEA.
  User: dimitrijarneau
  Date: 06/06/2018
  Time: 14:21
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<div class="panel-body">
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="alert alert-info alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <strong><i class="fas fa-exclamation-triangle fa-fw"></i>&nbsp; Attention </strong> pour
                pouvoir importer une liste de rôles, il est demandé
                d'utiliser des fichiers (*.csv). Il est aussi obligatoire que les comptes utilisateurs
                soient déjà créés.<br> De plus le format d'une entrée doit être sous la forme :<br>
                <p align="center" style="font-size: 16px"><strong>Pour les étudiants I2 : <br/>
                    identifiant;mail@reseau.eseo.fr;NOM;Prénom;I2;nomOption</strong>
                <p align="center" style="font-size: 16px"><strong>OU</strong></p>

                <p align="center" style="font-size: 16px"><strong>Pour les étudiants I3 : <br/>
                    identifiant;mail@reseau.eseo.fr;NOM;Prénom;I3;nomOption</strong>
                </p>
                <p align="center" style="font-size: 16px"><strong>OU</strong></p>
                <p align="center" style="font-size: 16px"><strong>Pour les professeurs : <br/>
                    identifiant;mail@reseau.eseo.fr;NOM;Prénom;Enseignant;nomOption;typeDeProfesseur</strong>
                </p>
                <p align="center"><br>Les options doivent être sous la forme :
                    [LD,SE,OC,BIO,NRJ,CC,IIT,DSMT,BD] <br> et le type de professeur doit être sous la
                    forme [prof,profOption,profResponsable]
            </div>
        </div>
        <form action="<c:url value="/GererRoles"/>" method="post" enctype="multipart/form-data">
            <input type="hidden" name="formulaire" id="formulaire" value="defaut"/>
            <div class="col-md-4 col-md-4">
                <div class="panel panel-default ">
                    <div class="panel-heading"><i class="fas fa-cloud-upload-alt"></i>
                        &nbsp;Importer une liste d'étudiants ou de professeurs
                    </div>
                    <div class="panel-body">
                        <label for="fichier">Emplacement du fichier : <span
                                class="requis">*</span></label>
                        <div class="input-group">
                            <label class="input-group-btn">
                                                <span class="btn btn-primary btn-file">
                                                    <i class="fas fa-folder-open fa-fw"></i>
                                                    &nbsp;
                                                    <input type="file" id="fichier" name="fichier" accept=".csv,.txt"
                                                           style="display: none;">
                                                </span>

                            </label>

                            <input id="nomFichier" class="form-control" readonly="" type="text">
                            <label class="input-group-btn">
                                <button class="btn btn-success" type="submit" id="sendCsvButton"
                                        disabled="">
                                    <i class="fas fa-upload fa-fw" aria-hidden="true"></i>
                                    &nbsp;Importer
                                </button>
                            </label>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <i class="fas fa-info-circle fa-fw" aria-hidden="true"></i>&nbsp;
                        <strong>Information importation étudiants I3: </strong> <br/>
                        Pour pouvoir importer une liste de rôles,
                        et ainsi rattacher des étudiants à des options,
                        il faut que l'année de l'option soit planifiée.
                    </div>
                </div>
            </div>
        </form>
        <div class="col-md-8 col-md-8">
            <div class="panel panel-default ">
                <div class="panel-heading"><i class="far fa-folder-open"></i>
                    &nbsp;Modèles CSV à télécharger
                </div>
                <div class="panel-body">

                    <i class="far fa-check-circle fa-fw" aria-hidden="true"></i>&nbsp;
                    Modèle étudiants I2/PGL : &nbsp;
                    <a role="button" class="btn btn-light btn-xs" target="_blank"
                       href="<c:url value="/download/exemple_etudiants_I2.csv"/>">
                        <i class="fas fa-download fa-fw" aria-hidden="true"></i> Télécharger le fichier
                    </a>

                    <br>
                    <br/>

                    <i class="far fa-check-circle fa-fw" aria-hidden="true"></i>&nbsp;
                    Modèle étudiants I3 : &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <a role="button" class="btn btn-light btn-xs" target="_blank"
                       href="<c:url value="/download/exemple_etudiants_I3.csv"/>">
                        <i class="fas fa-download fa-fw" aria-hidden="true"></i>&nbsp; Télécharger le
                        fichier
                    </a>
                    <br>

                    <br/>

                    <i class="far fa-check-circle fa-fw" aria-hidden="true"></i>&nbsp;
                    Modèle professeurs : &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <a role="button" class="btn btn-light btn-xs" target="_blank"
                       href="<c:url value="/download/exemple_professeurs.csv"/>">
                        <i class="fas fa-download fa-fw" aria-hidden="true"></i>&nbsp; Télécharger le
                        fichier
                    </a>
                </div>
            </div>
        </div>
        <div class="col-md-8 col-md-8">
            <div class="panel panel-default ">
                <div class="panel-heading"><i class="fas fa-info"></i>
                    &nbsp;Résultat du précédent import
                </div>
                <div class="panel-body">
                    <c:choose>
                        <c:when
                                test="${(!empty nombreInsertion)||(!empty nombreEchec)}">
                            <ul class="list-group">
                                <li
                                        class="list-group-item list-group-item-action list-group-item-success justify-content-between">
                                    Nombre d'insertions <span
                                        class="badge badge-default badge-pill">${nombreInsertion}</span>
                                </li>
                                <li
                                        class="list-group-item list-group-item-action list-group-item-danger justify-content-between">
                                    Nombre d'échecs <span
                                        class="badge badge-default badge-pill">${nombreEchec}</span>

                                    <div class="panel-group">
                                        <br>
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <div class="panel-title ">
                                                    <a style="font-size: 14px" data-toggle="collapse"
                                                       href="#collapse1">Liste des erreurs
                                                        <span class="glyphicon glyphicon-chevron-down pull-right"></span>
                                                    </a>
                                                </div>
                                            </div>
                                            <div id="collapse1" class="panel-collapse collapse">
                                                <ul class="list-group">
                                                    <c:forEach var="erreurImport"
                                                               items="${erreursImportation}">
                                                        <li class="list-group-item list-group-item-default">
                                                            <strong>Erreur :</strong> ${erreurImport}
                                                        </li>
                                                    </c:forEach>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </c:when>
                        <c:otherwise>
                            <p align="center">Veuillez importer un fichier.</p>
                        </c:otherwise>
                    </c:choose>
                </div>
            </div>
        </div>
    </div>
</div>

