<%--
  Created by IntelliJ IDEA.
  User: dimitrijarneau
  Date: 06/06/2018
  Time: 14:24
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>


<div class="col-lg-4 col-lg-offset-4 col-md-4 col-md-offset-4">

    <form method="post" action="<c:url value="/GererRoles"/>">
        <div class="col-lg-10 col-md-10">
            <div class="typeahead__container">
                <div class="typeahead__field">
									<span class="typeahead__query">
									<input class="js-typeahead" name="utilisateurRecherche" id="utilisateurRecherche"
                                           type="search" autofocus autocomplete="off">
									</span>
                </div>
            </div>
        </div>

        <div class="col-lg-2 col-md-2">
            <button type="submit" class="btn btn-primary center-block" id="submit">
                <i class="fas fa-search fa-fw" aria-hidden="true"></i>&nbsp; Chercher
            </button>
        </div>
        <input type="hidden" name="formulaire" id="formulaire" value="chercherUtilisateur"/>
    </form>
</div>
<br/>
<br/>

<div class="panel-body">
    <div class="alert alert-info alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                aria-hidden="true">&times;</span></button>
        <i class="fas fa-info-circle fa-fw" aria-hidden="true"></i>&nbsp;
        <strong>Information : </strong> <br/>
        Entrer le nom/prenom/identifiant d'un utilisateur pour avoir des informations le concernant.
    </div>
</div>

<c:choose>
    <c:when test="${!empty utilisateurChoisi}">
        <div class="panel-body">
            <div class="row">
                <div class="col-lg-5 col-md-5">
                    <div class="panel panel-default ">
                        <div class="panel-heading"><i class="fas fa-clipboard-list fa-fw"></i>
                            &nbsp;Résumé
                        </div>
                        <div class="panel-body">
                            <table class="table table-striped table-bordered table-hover">
                                <tr>
                                    <th>Attribut</th>
                                    <th>Valeur</th>
                                </tr>
                                <tr>
                                    <td>ID</td>
                                    <td><c:out value="${utilisateurChoisi.idUtilisateur}"/></td>
                                </tr>
                                <tr>
                                    <td>Nom</td>
                                    <td><c:out value="${utilisateurChoisi.nom}"/></td>
                                </tr>
                                <tr>
                                    <td>Prenom</td>
                                    <td><c:out value="${utilisateurChoisi.prenom}"/></td>
                                </tr>
                                <tr>
                                    <td>Email</td>
                                    <td><c:out value="${utilisateurChoisi.email}"/></td>
                                </tr>
                                <tr>
                                    <td>Identifiant de connexion</td>
                                    <td><c:out value="${utilisateurChoisi.identifiant}"/></td>
                                </tr>

                            </table>


                            <form method="post" action="<c:url value="/GererRoles"/>">

                                <c:choose>
                                    <c:when test="${utilisateurChoisi.valide=='oui'}">
                                        <button type="submit" class="btn btn-danger center-block" id="submit">
                                            <i class="fas fa-ban fa-fw" aria-hidden="true"></i>&nbsp; Suspendre
                                            le compte
                                        </button>
                                        <input type="hidden" name="choixValidation"
                                               id="choixValidation" value="non"/>
                                    </c:when>
                                    <c:otherwise>
                                        <button type="submit" class="btn btn-success center-block"
                                                id="submit">
                                            <i class="far fa-check-circle fa-fw" aria-hidden="true"></i>&nbsp;
                                            Activer le
                                            compte
                                        </button>
                                        <input type="hidden" name="choixValidation"
                                               id="choixValidation" value="oui"/>
                                    </c:otherwise>
                                </c:choose>
                                <input type="hidden" name="utilisateurChoisi"
                                       id="utilisateurChoisi"
                                       value="<c:out value="${utilisateurChoisi.idUtilisateur}"/>"/>
                                <input type="hidden" name="formulaire" id="formulaire"
                                       value="suspendreCompte"/>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-lg-7 col-md-7">
                    <div class="panel panel-default ">
                        <div class="panel-heading"><i class="far fa-check-square fa-fw"></i>
                            &nbsp;Autorisation de
                            l'utilisateur
                        </div>
                        <div class="panel-body">
                            <c:choose>
                                <c:when test="${!empty listeRolesUtilisateur}">
                                    <table class="table table-hover">
                                        <thead>
                                        <tr>
                                            <th>Role</th>
                                            <th>Option</th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <c:forEach var="i" begin="0" end="${listeRolesUtilisateur.size()-1}">
                                            <form method="post" action="<c:url value="/GererRoles"/>">
                                                <input type="hidden" name="utilisateurChoisi"
                                                       id="utilisateurChoisi"
                                                       value="<c:out value="${utilisateurChoisi.idUtilisateur}"/>"/>
                                                <input type="hidden" name="roleUtilisateur" id="roleUtilisateur"
                                                       value="<c:out value="${listeRolesUtilisateur.get(i).nomRole}" />"/>
                                                <input type="hidden" name="optionUtilisateur"
                                                       id="optionUtilisateur"
                                                       value="<c:out value="${listeOptionsUtilisateur.get(i).nomOption}" />"/>
                                                <input type="hidden" name="formulaire" id="formulaire"
                                                       value="supprimerAutorisation"/>

                                                <tr>
                                                    <td>${listeRolesUtilisateur.get(i).nomRole}</td>
                                                    <td>${listeOptionsUtilisateur.get(i).nomOption}</td>
                                                    <td>
                                                        <button type="submit"
                                                                class="btn btn-danger center-block" id="submit">
                                                            <i class="far fa-trash-alt fa-fw"
                                                               aria-hidden="true"></i>&nbsp;
                                                        </button>
                                                    </td>
                                                </tr>
                                            </form>
                                        </c:forEach>
                                        </tbody>
                                    </table>
                                </c:when>
                                <c:otherwise>
                                    <p align="center">Aucun rôle et option ne sont attribués à l'utilisateur</p>
                                </c:otherwise>
                            </c:choose>
                        </div>
                    </div>
                    <div class="panel panel-default ">
                        <div class="panel-heading"><i class="far fa-plus-square fa-fw"></i>
                            &nbsp;Rajouter un rôle et une option à
                            l'utilisateur
                        </div>
                        <div class="panel-body">
                            <form method="post" id="ajouterRole"
                                  action="<c:url value="/GererRoles"/>">
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th>Role</th>
                                        <th id="colonneOptions" style="display: none;">Option</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tr>
                                        <td>
                                            <c:forEach var="role" items="${listeRoles}">
                                                <div class="form-check">
                                                    <input class="form-check-input" type="radio"
                                                           name="radioRole"
                                                           id="<c:out value="${role.nomRole}"/>"
                                                           onchange="afficherFormulaire('${role.nomRole}');"
                                                           value="<c:out value="${role.nomRole}"/>">
                                                    <label for="<c:out value="${role.nomRole}" />"><c:out
                                                            value="${role.nomRole}"/></label>
                                                </div>
                                            </c:forEach>
                                        </td>
                                        <td id="listeOption" style="display: none;">
                                            <c:forEach var="option" items="${listeOptions}">
                                                <div class="form-check">
                                                    <label class="form-check-label">
                                                        <c:choose>
                                                            <c:when test="${!empty option.nomOption}">
                                                                <input class="form-check-input" type="radio"
                                                                       name="radioOption" id="radioOption"
                                                                       value="<c:out value="${option.nomOption}"/>">
                                                                <c:out value="${option.nomOption}"/>
                                                            </c:when>
                                                        </c:choose>
                                                    </label>
                                                </div>
                                            </c:forEach>
                                        </td>
                                        <td>
                                            <div id="formulaireAnneeSco"
                                                 style="display: none; text-align: center;">
                                                <p align="center">Etudiant en I2 ou I3 ?</p>
                                                <div class="btn-group" data-toggle="buttons">
                                                    <label class="btn btn-primary active">
                                                        <input class="form-check-input" type="radio"
                                                               name="radioAnneeScolaire" onchange="afficherContratPro()"
                                                               id="radioAnneeScolaire"
                                                               value="I2" autocomplete="off" checked/>
                                                        I2
                                                    </label>
                                                    <label class="btn btn-primary">
                                                        <input class="form-check-input" type="radio"
                                                               name="radioAnneeScolaire" onchange="afficherContratPro()"
                                                               id="radioAnneeScolaire"
                                                               value="I3" autocomplete="off"/>
                                                        I3
                                                    </label>
                                                </div>
                                                <hr width="%">
                                            </div>
                                            <div id="formulaireContratPro"
                                                 style="display: none; text-align: center;">

                                                <p align="center">Etudiant en Contrat Pro ?</p>
                                                <div class="btn-group" data-toggle="buttons">
                                                    <label class="btn btn-primary active">
                                                        <input class="form-check-input" type="radio"
                                                               name="radioContratPro" id="radioContratPro"
                                                               value="oui" autocomplete="off" checked/>
                                                        Oui
                                                    </label>
                                                    <label class="btn btn-primary">
                                                        <input class="form-check-input" type="radio"
                                                               name="radioContratPro" id="radioContratPro"
                                                               value="non" autocomplete="off"/>
                                                        Non
                                                    </label>
                                                </div>
                                                <hr width="%">
                                            </div>
                                            <button type="submit" class="btn btn-primary center-block"
                                                    type="submit">
                                                <i class="fas fa-plus fa-fw" aria-hidden="true"
                                                   id="ajouterAutorisation"></i>&nbsp;
                                                Ajouter
                                            </button>

                                        </td>
                                    </tr>
                                </table>
                                <input type="hidden" name="formulaire" id="formulaire"
                                       value="ajouterAutorisation"/>
                                <input type="hidden" name="utilisateurChoisi" id="utilisateurChoisi"
                                       value="<c:out value="${utilisateurChoisi.idUtilisateur}"/>"/>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </c:when>
</c:choose>
