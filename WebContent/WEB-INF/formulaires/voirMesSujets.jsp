<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <c:import url="/inc/head.jsp"/>
    <title>SoManager - Voir mes sujets</title>
</head>
<body>
<div class="wrapper">
    <c:import url="/Navbar"/>
    <!-- Page Content Holder -->
    <div id="content">
        <c:import url="/inc/headerForSidebar.jsp"/>


        <div class="panel panel-primary ">
            <div class="panel-heading">Sujet - Mes sujets</div>
            <div class="panel-body">

                <div class="col-lg-6">
                    <div class="panel panel-default ">
                        <div class="panel-heading">Rechercher des sujets</div>
                        <div class="panel-body">
                            <div class="col-lg-12">
                                <c:import url="/inc/listeSujets.jsp"/>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-6">
                    <div class="panel panel-default ">
                        <div class="panel-heading">Modifier un sujet</div>
                        <div class="panel-body">
                            <div class="col-lg-12">
                                <!--  Création du div drop qui permet la modification -->
                                <div id="drop" ondrop="drop(event)" draggable="false"
                                     ondragover="allowDrop(event)" ondragleave="refuseDrop(event)">
                                    <div id="drophidden--1"
                                         style="display: absolute; text-align: center; text-align: middle">
                                        <p>Faites Glisser-déposer un sujet ici pour le modifier ou Double-cliquez
                                            dessus</p>
                                        <img src="<c:url value="/images/giphy.gif"/>"
                                             alt="Smiley face" height="42" width="42">
                                    </div>

                                    <!--  Création d'un formulaire invisible tant qu'un id n'a pas été passé dans le div drop -->
                                    <c:forEach var="sujet" items="${listeSujets}" varStatus="loop">
                                        <div id="drophidden-${loop.index}" style="display: none">
                                            <form data-toggle="validator" role="form" method="post"
                                                  action="<c:url value="/VoirMesSujets"/>">
                                                <fieldset>
                                                    <legend>
                                                        Modifier le sujet <br />
                                                        <b><c:out value="${sujet.titre}"/></b>
                                                    </legend>

                                                    <c:if test="${not empty sujet.idForm}">
                                                        <div class="col-sm-12 text-red text-justify no-padding" style="margin-bottom: 10px">
                                                            <i class="fas fa-exclamation-triangle fa-fw" aria-hidden="true"></i>&nbsp;
                                                            La modification locale d'un sujet envoyée depuis un formulaire n'est pas sûr.
                                                            Merci de modifier le sujet depuis le formulaire.
                                                        </div>
                                                    </c:if>


                                                    <input type="hidden" name="idSujet" id="idSujet"
                                                           value="<c:out value="${sujet.idSujet}"/>"/>
                                                    <input type="hidden" name="etatSujet" id="etatSujet"
                                                           value="<c:out value="${sujet.etat}"/>"/>


                                                    <div class="form-group has-feedback">
                                                        <label for="titre" class="control-label">Titre : <span
                                                                class="requis">*</span></label>
                                                        <input type="text" name="titre" class="form-control" id="titre"
                                                               value="<c:out value="${sujet.titre}"/>" autofocus
                                                               required/>
                                                        <div class="help-block with-errors"></div>
                                                    </div>

                                                    <div class="form-group has-feedback">
                                                        <label for="description" class="control-label">Description :
                                                            <span class="requis">*</span></label>
                                                        <textarea name="description" class="form-control"
                                                                  id="description"
                                                                  required/>${sujet.description} </textarea>
                                                        <div class="help-block with-errors"></div>
                                                    </div>


                                                    <label for="porteur">Porteur du projet :</label>
                                                    <input type="text" name="porteur" class="form-control" id="porteur"
                                                           value="<c:out value="${sessionScope.utilisateur.nom} ${sessionScope.utilisateur.prenom}"/>"
                                                           disabled="disabled"/>
                                                    <br>

                                                    <div class="form-group has-feedback">
                                                        <label for="nbrMinEleves" class="control-label">Nombre minimum
                                                            d'élèves : <span class="requis">*</span></label>
                                                        <input type="number" name="nbrMinEleves" class="form-control"
                                                               id="nbrMinEleves" min="1" step="1"
                                                               value="<c:out value="${sujet.nbrMinEleves}"/>" required/>
                                                        <div class="help-block with-errors"></div>
                                                    </div>

                                                    <div class="form-group has-feedback">
                                                        <label for="nbrMaxEleves" class="control-label">Nombre maximum
                                                            d'élèves : <span class="requis">*</span></label>
                                                        <input type="number" name="nbrMaxEleves" class="form-control"
                                                               id="nbrMaxEleves" min="1" step="1"
                                                               value="<c:out value="${sujet.nbrMaxEleves}"/>" required/>
                                                        <div class="help-block with-errors"></div>
                                                    </div>

                                                    <div class="form-group has-feedback">
                                                        <fieldset>
                                                            <label for="contratPro" class="control-label">Est-ce un
                                                                sujet de contrat pro ? <span
                                                                        class="requis">*</span></label>
                                                            <div class="col-lg-12">
                                                                <div class="radio col-lg-offset-1 col-md-offset-1 col-sm-offset-1">
                                                                    <input type="radio" name="contratPro"
                                                                           id="contratProVrai"
                                                                           value="true"  ${fn:contains(sujet.contratPro,'true') ? 'checked' : ''}
                                                                           required/>
                                                                    <label for="contratProVrai"><strong>Oui</strong></label>
                                                                </div>
                                                                <div class="radio col-lg-offset-1 col-md-offset-1 col-sm-offset-1">
                                                                    <input type="radio" name="contratPro"
                                                                           id="contratProFaux"
                                                                           value="false" ${fn:contains(sujet.contratPro,'false') ? 'checked' : ''}
                                                                           required/>
                                                                    <label for="contratProFaux"><strong>Non</strong></label>
                                                                </div>
                                                            </div>
                                                            <div class="help-block with-errors"></div>
                                                        </fieldset>
                                                    </div>

                                                    <div class="form-group has-feedback">
                                                        <fieldset>
                                                            <label for="confidentialite" class="control-label">Est-ce un
                                                                sujet confidentiel ? <span
                                                                        class="requis">*</span></label>
                                                            <div class="col-lg-12">
                                                                <div class="radio col-lg-offset-1 col-md-offset-1 col-sm-offset-1">
                                                                    <input type="radio" name="confidentialite"
                                                                           id="confidentialiteVrai"
                                                                           value="true" ${fn:contains(sujet.confidentialite,'true') ? 'checked' : ''}
                                                                           required/>
                                                                    <label for="confidentialiteVrai"><strong>Oui</strong></label>
                                                                </div>
                                                                <div class="radio col-lg-offset-1 col-md-offset-1 col-sm-offset-1">
                                                                    <input type="radio" name="confidentialite"
                                                                           id="confidentialiteFaux"
                                                                           value="false" ${fn:contains(sujet.confidentialite,'false') ? 'checked' : ''}
                                                                           required/>
                                                                    <label for="confidentialiteFaux"><strong>Non</strong></label>
                                                                </div>
                                                            </div>
                                                            <div class="help-block with-errors"></div>
                                                        </fieldset>
                                                    </div>

                                                    <fieldset>
                                                        <label class="control-label">Option(s) concernée(s) : <span
                                                                class="requis">*</span></label>

                                                        <div class="col-lg-12">
                                                            <div
                                                                    class="checkbox col-lg-offset-1 col-md-offset-1 col-sm-offset-1">
                                                                <input type="checkbox" name="CC" id="CC"
                                                                       value="CC" ${fn:contains(optionSujet[loop.index],'CC') ? 'checked' : ''} />
                                                                <label
                                                                        for="CC"><strong>Cloud
                                                                    Computing</strong></label>
                                                            </div>
                                                            <div
                                                                    class="checkbox col-lg-offset-1 col-md-offset-1 col-sm-offset-1">
                                                                <input type="checkbox" name="IIT" id="IIT"
                                                                       value="IIT" ${fn:contains(optionSujet[loop.index],'IIT') ? 'checked' : ''}  /><label
                                                                    for="IIT"> <strong>Infrastructures
                                                                IT</strong></label>
                                                            </div>
                                                            <div
                                                                    class="checkbox col-lg-offset-1 col-md-offset-1 col-sm-offset-1">
                                                                <input type="checkbox" name="LD" id="LD"
                                                                       value="LD" ${fn:contains(optionSujet[loop.index],'LD') ? 'checked' : ''} />
                                                                <label
                                                                        for="LD"><strong>Logiciels et
                                                                    Données</strong></label>
                                                            </div>
                                                            <div
                                                                    class="checkbox col-lg-offset-1 col-md-offset-1 col-sm-offset-1">
                                                                <input type="checkbox" name="SE" id="SE"
                                                                       value="SE" ${fn:contains(optionSujet[loop.index],'SE') ? 'checked' : ''} />
                                                                <label
                                                                        for="SE"><strong>Systèmes
                                                                    Embarqués</strong></label>
                                                            </div>
                                                            <div
                                                                    class="checkbox col-lg-offset-1 col-md-offset-1 col-sm-offset-1">
                                                                <input type="checkbox" name="BIO" id="BIO"
                                                                       value="BIO" ${fn:contains(optionSujet[loop.index],'BIO') ? 'checked' : ''} />
                                                                <label
                                                                        for="BIO"><strong>Biomédical</strong></label>
                                                            </div>
                                                            <div
                                                                    class="checkbox col-lg-offset-1 col-md-offset-1 col-sm-offset-1">
                                                                <input type="checkbox" name="OC" id="OC"
                                                                       value="OC" ${fn:contains(optionSujet[loop.index],'OC') ? 'checked' : ''} />
                                                                <label
                                                                        for="OC"><strong>Objets
                                                                    Connectés</strong></label>
                                                            </div>
                                                            <div
                                                                    class="checkbox col-lg-offset-1 col-md-offset-1 col-sm-offset-1">
                                                                <input type="checkbox" name="DSMT" id="DSMT"
                                                                       value="DSMT" ${fn:contains(optionSujet[loop.index],'DSMT') ? 'checked' : ''} />
                                                                <label
                                                                        for="DSMT"><strong>Data Sciences, Multimedia &
                                                                    Telecom</strong></label>
                                                            </div>
                                                            <div
                                                                    class="checkbox col-lg-offset-1 col-md-offset-1 col-sm-offset-1">
                                                                <input type="checkbox" name="NRJ" id="NRJ"
                                                                       value="NRJ" ${fn:contains(optionSujet[loop.index],'NRJ') ? 'checked' : ''} />
                                                                <label
                                                                        for="NRJ"><strong>Énergie et
                                                                    Environnement</strong></label>
                                                            </div>
                                                            <div
                                                                    class="checkbox col-lg-offset-1 col-md-offset-1 col-sm-offset-1">
                                                                <input type="checkbox" name="BD" id="BD"
                                                                       value="BD" ${fn:contains(optionSujet[loop.index],'BD') ? 'checked' : ''} />
                                                                <label
                                                                        for="BD"><strong>Big Data</strong></label>
                                                            </div>
                                                            <br>
                                                        </div>
                                                    </fieldset>


                                                    <label>Type(s) d'intérêt :</label>
                                                    <div class="col-lg-12">
                                                        <div class="checkbox col-lg-offset-1 col-md-offset-1 col-sm-offset-1">
                                                            <input type="checkbox" name="JPO"
                                                                   id="JPO" ${fn:contains(sujet.interets,'JPO') ? 'checked' : ''} />
                                                            <label for="JPO"><strong>Journée
                                                                Portes Ouvertes</strong></label>
                                                        </div>
                                                        <div class="checkbox col-lg-offset-1 col-md-offset-1 col-sm-offset-1">
                                                            <input type="checkbox" name="GP"
                                                                   id="GP"  ${fn:contains(sujet.interets,'GP') ? 'checked' : ''} />
                                                            <label for="GP"><strong>Gestion
                                                                de Projet (I1)</strong></label>
                                                        </div>
                                                        <br>
                                                    </div>
                                                    <label for="autres">Autres :</label>
                                                    <input type="text" name="autres" class="form-control" id="autres"
                                                           value="<c:out value="${fn:substringAfter(sujet.interets, 'Autres;')}"/>"/>
                                                    <div class="help-block with-errors">Séparez les intérets par des ;
                                                    </div>
                                                    <br>


                                                    <label for="liens">Liens externes :</label>
                                                    <textarea name="liens" class="form-control" id="liens"><c:out
                                                            value="${sujet.liens}"/></textarea>
                                                    <br>

                                                    <c:if test="${!empty erreurs}">
                                                        <div class="text-center">
                                                            <span class="erreur">${resultat} ${erreurs['nbrMinEleves']} ${erreurs['options']}</span>
                                                        </div>
                                                        <br>
                                                    </c:if>

                                                    <c:if test="${sujet.etat eq 'DEPOSE'}">
                                                        <div class="text-center">
                                                            <button type="submit" class="btn btn-primary" id="submit">
                                                                <i class="fas fa-pencil-alt fa-fw"
                                                                   aria-hidden="true"></i>&nbsp; Modifier
                                                            </button>
                                                        </div>
                                                    </c:if>
                                                </fieldset>
                                            </form>
                                            <div style="margin:0px 0px 0px 0%;  float: right;">

                                                <%@ include file="/inc/commenter.jsp" %>

                                            </div>


                                        </div>
                                    </c:forEach>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>
</div>


<c:import url="/inc/scripts.jsp"/>


<script src="<c:url value="/js/dragAndDrop.js"/>"></script>
<script>
    function commentairehidden(id) {
        divCommentaire = document.getElementById("commentairehidden-" + id);
        if (divCommentaire.style.display === "none") {
            divCommentaire.style.display = "block";
            var h = divCommentaire.clientHeight + 3;
            //var w = divCommentaire.clientWidth-20;
            divCommentaire.style.margin = "-" + h + "px 0px 0px -50%";
        } else {
            divCommentaire.style.display = "none";
            divCommentaire.style.margin = "0px 0px 0px 0px";
        }

    }

    function ecrirehidden(id, idBis) {
        divEcrire = document.getElementById("subComment-" + id);
        divCommentaire = document.getElementById("commentairehidden-" + idBis);
        if (divEcrire.style.display === "none") {
            divEcrire.style.display = "block";
            var h = divCommentaire.clientHeight + 3;
            divCommentaire.style.margin = "-" + h + "px 0px 0px -50%";
        } else {
            divEcrire.style.display = "none";
            var h = divCommentaire.clientHeight + 3;
            divCommentaire.style.margin = "-" + h + "px 0px 0px -50%";
        }
    }
</script>
</body>
</html>