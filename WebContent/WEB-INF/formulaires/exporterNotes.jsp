<%--
  Created by IntelliJ IDEA.
  User: tristanlegacque
--%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <c:import url="/inc/head.jsp"/>
    <link rel="stylesheet" href="<c:url value="/css/exportNotes.css"/>">
    <title>SoManager - Export des notes</title>
</head>
<body>
<div class="wrapper">
    <c:import url="/Navbar"/>
    <!-- Page Content Holder -->
    <div id="content">
        <c:import url="/inc/headerForSidebar.jsp"/>

        <div class="panel panel-primary ">
            <div class="panel-heading">Export des notes de jury de I3</div>
            <div class="panel-body">
                <c:import url="/inc/callback.jsp"/>

                <c:if test="${assistant eq 'true'}">
                    <c:set var="onglet" value="${requestScope.ongletChoisi}" scope="page"/>

                    <%-- Différents onglets --%>
                    <ul class="nav nav-tabs" role="tablist" style="margin-bottom: 20px">
                        <li class="${onglet eq 'general' or empty onglet ? 'active' : ''}">
                            <a data-toggle="tab" href="#general">
                                <i class="fas fa-key fa-fw" aria-hidden="true"></i>&nbsp;
                                Export des notes
                            </a>
                        </li>
                        <li class="${onglet eq 'configuration' ? 'active' : ''}">
                            <a data-toggle="tab" href="#configuration">
                                <i class="fas fa-cog fa-fw" aria-hidden="true"></i>&nbsp;
                                Configuration
                            </a>
                        </li>
                    </ul>

                    <%-- Contenu des différents onglets --%>
                    <div class="tab-content">

                            <%-- Exporter les notes --%>
                        <div id="general"
                             class="tab-pane fade${onglet eq 'general' or empty onglet ? ' in active' : ''}">
                            <div class="row">
                                <c:import url="/WEB-INF/formulaires/exporterNotes/exportGeneral.jsp"/>
                            </div>
                        </div>

                            <%-- Configurer les modèles --%>
                        <div id="configuration" class="tab-pane fade${onglet eq 'configuration' ? ' in active' : ''}">
                            <div class="row">
                                <c:import url="/WEB-INF/formulaires/exporterNotes/configModele.jsp"/>
                            </div>
                        </div>
                    </div>
                </c:if>
            </div>
        </div>
    </div>
</div>
<c:import url="/inc/scripts.jsp"/>
<script type="application/javascript" src="<c:url value="/js/scripts/exportNotes.js"/>"></script>
<script type="text/javascript" language="javascript">
    $(function () {
        //Capturer le changement de fichier dans un evenement
        $(document).on('change', ':file', function () {
            var input = $(this),
                label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
            input.trigger('fileselect', [label]);
        });

        //Quand on séléctionne un fichier à envoyer
        $(":file").on("fileselect", function (event, label) {
            var nomFichier = $("#nomFichier");
            var sendCsvB = $("#sendCsvButton");

            //Si le fichier est un .csv ou .xlsx on autorise
            if (label.indexOf(".csv") !== -1 || label.indexOf(".xlsx")) {
                nomFichier.val(label);
                sendCsvB.prop('disabled', false);
            } else {
                nomFichier.val("");
                sendCsvB.prop('disabled', true);
            }
        });

    });
</script>

</body>
</html>
