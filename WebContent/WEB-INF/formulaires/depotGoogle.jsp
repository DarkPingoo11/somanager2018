<%--
  Created by IntelliJ IDEA.
  User: dimitrijarneau
  Date: 14/03/2018
  Time: 16:39
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <c:import url="/inc/head.jsp"/>
    <title>SoManager - Depot Google</title>
</head>
<body>
<div class="wrapper">
    <c:import url="/Navbar"/>
    <!-- Page Content Holder -->
    <div id="content">
        <c:import url="/inc/headerForSidebar.jsp"/>

        <div class="panel panel-primary ">
            <!-- TODO : Faire un PDF Explicatif pour la création d'un formulaire et la récupération de csv (Microsoft + google)-->
            <div class="panel-heading">Sujet - Depot depuis un formulaire</div>
            <div class="panel-body">
                <c:import url="/inc/callback.jsp"/>

                <%-- Enregistrement/ affichage du lien google --%>
                <div class="col-md-6">
                    <div class="panel panel-default ">

                        <div class="panel-heading"><i class="fas fa-list-ul fa-fw" aria-hidden="true"></i>&nbsp;
                            Lien vers le formulaire Google Forms
                        </div>
                        <div class="panel-body">
                            <div class="col-md-12">
                                <p class="text-justify">
                                    Il vous est possible d'ajouter un sujet
                                    à partir d'un formulaire Google Forms. Pour cela, veuillez remplir le formulaire
                                    suivant.
                                    <br/>
                                </p>
                                <div class="col-sm-12 no-padding" style="margin-top: 20px">
                                    <%-- Affichage du lien --%>
                                    <c:choose>
                                        <c:when test="${ not empty requestScope.lienFormulaireGoogle }">
                                            <a title="Formulaire" class="btn btn-primary"
                                               href="${ requestScope.lienFormulaireGoogle }"
                                               target="_blank" role="button" id="formulaireG">
                                                <i class="fas fa-paperclip fa-fw" aria-hidden="true"></i>&nbsp;
                                                Accéder au formulaire
                                            </a>
                                        </c:when>
                                        <c:otherwise>
                                            <a title="Formulaire" class="btn btn-danger"
                                               role="button" disabled="">
                                                <i class="fas fa-question-circle fa-fw" aria-hidden="true"></i>&nbsp;
                                                Aucun lien enregistré
                                            </a>
                                        </c:otherwise>
                                    </c:choose>
                                </div>
                            </div>
                        </div>
                    </div>
                    <%--
                                  La modification du lien du formulaire doit etre accessible par l'assistant seulement
                                --%>

                    <%-- Set Modification lien formulaire   --%>
                    <c:if test="${assistant eq 'true' }">
                        <div class="panel panel-default ">

                            <div class="panel-heading"><i class="fas fa-wrench fa-fw" aria-hidden="true"></i>&nbsp;
                                Modifier lien vers le formulaire Google
                            </div>
                            <div class="panel-body">
                                <div class="col-md-12l">
                                    <p class="text-justify">
                                        Si vous souhaitez changer le lien de formulaire Google Form, veuillez
                                        entrer un nouveau lien ci-dessous.<br/>
                                    </p>
                                    <form method="post" action="<c:url value="/DepotGoogle"/>">
                                        <input type="hidden" name="formulaire" value="changerLien">
                                        <div class="col-sm-12 no-padding" style="margin-top: 20px">
                                            <div class="input-group">
                                                <input type="text" class="form-control" name="nouveauLien"
                                                       id="nouveauLien"
                                                       value="${ requestScope.lienFormulaireGoogle }"/>
                                                    <%-- <input type="button" value="Affecter Modification" onclick="affecterModification(linkToChange);" /> --%>
                                                <label class="input-group-btn">
                                                    <button class="btn btn-success" value="Affecter Modification"
                                                            onclick="changerLien();" type="submit" id="affecterModif">
                                                        <i class="fas fa-lock fa-fw" aria-hidden="true"></i>&nbsp;
                                                        Affecter Modification
                                                    </button>
                                                </label>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </c:if>
                </div>


                <%--
                  La synchronisation doit etre effectuée par l'assistant et pas par l'étudiant
                --%>

                <%-- Synchronisation des sujets par csv--%>
                <c:if test="${assistant eq 'true' }">
                    <div class="col-md-6">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <i class="fas fa-sync-alt fa-fw" aria-hidden="true"></i>&nbsp;
                                Synchroniser les sujets via formulaire
                            </div>
                            <div class="panel-body">
                                <div class="col-md-12">
                                    <p class="text-justify">
                                        Afin de synchroniser les sujets, merci de récupérer le fichier<b
                                            class="text-primary">.csv</b> contenant les réponses
                                        au formulaire d'ajout de sujet. Ce fichier doit respecter le format indiqué dans
                                        le
                                        fichier type
                                        disponible ci-dessous.<br/>
                                    </p>
                                    <form method="post" enctype="multipart/form-data"
                                          action="<c:url value="/DepotGoogle"/>">
                                        <input type="hidden" name="formulaire" value="synchroniserSujets">
                                        <div class="col-sm-12 no-padding" style="margin-top: 20px">
                                            <div class="input-group">
                                                <label class="input-group-btn">
                                                <span class="btn btn-primary btn-file">
                                                    Parcourir…
                                                    <input type="file" id="file_selected" name="file_selected"
                                                           accept=".csv,.txt" onchange="checkCSVFile(this);"
                                                           style="display: none;">
                                                </span>
                                                </label>

                                                <input id="nomFichier" class="form-control" readonly="" type="text">
                                                <label class="input-group-btn">
                                                    <button class="btn btn-success" type="submit" id="sendCsvButton"
                                                            disabled="">
                                                        <i class="fas fa-upload fa-fw" aria-hidden="true"></i>
                                                        &nbsp;Envoyer
                                                    </button>
                                                </label>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div class="panel-footer">
                                <i class="fas fa-info-circle fa-fw" aria-hidden="true"></i>&nbsp;
                                Télécharger le fichier type de formulaire : &nbsp;
                                <a role="button" class="btn btn-primary btn-xs"
                                   href="<c:url value="/download/formulaire_sujet_type.csv"/>">
                                    <i class="fas fa-download fa-fw" aria-hidden="true"></i> Télécharger le fichier
                                </a>
                            </div>
                        </div>
                    </div>
                </c:if>
            </div>
        </div>
    </div>
</div>
<c:import url="/inc/scripts.jsp"/>
<script type="text/javascript" language="javascript">
    var linkToChange;

    function checkCSVFile(sender) {
        var validExts = new Array(".csv");
        var fileExt = sender.value;
        fileExt = fileExt.substring(fileExt.lastIndexOf('.'));
        if (validExts.indexOf(fileExt) < 0) {
            alert("Le fichier doit être de type " + validExts.toString());
            return false;
        }
        else return true;
    }

    function changerLien() {
        $(document).ready(function () {
            linkToChange = document.getElementById("nouveauLien").value;
            $('a').on('click', function (e) {
                $(document.getElementById("formulaireGoogle"))
                    .attr("href", linkToChange);
                // $(this).attr("href", "new Url");
                $(document.getElementById("formulaireGoogle")).attr("target", "_blank");

            })
        })
    }

    /// Last version
    function affecterModification(linkToChange) {
        linkToChange = document.getElementById("nouveauLien").value;
        //alert(linkToChange);
        ///document.getElementById("formulaireGoogle").value=linkToChange;
        document.getElementById("formulaireGoogle").setAttribute('href', linkToChange);
    }

    ///


    $(function () {
        //Capturer le changement de fichier dans un evenement
        $(document).on('change', ':file', function () {
            var input = $(this),
                label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
            input.trigger('fileselect', [label]);
        });

        //Quand on séléctionne un fichier à envoyer
        $(":file").on("fileselect", function (event, label) {
            var nomFichier = $("#nomFichier");
            var sendCsvB = $("#sendCsvButton");

            //Si le fichier est un .csv on autorise
            if (label.indexOf(".csv") !== -1) {
                nomFichier.val(label);
                sendCsvB.prop('disabled', false);
            } else {
                nomFichier.val("");
                sendCsvB.prop('disabled', true);
            }
        });

        //Récupérer un formulaire google
        function getCsv() {
            //Key = Clé du sheet
            var formURL = 'https://docs.google.com/spreadsheets/d/1IEUXk_u_hUeN5lA8h3ChgIunf7erxY5xfpgvYU3RdTw/gviz/tq?tqx=out:csv';
            $.ajax({
                url: formURL,
                type: 'GET',
                dataType: 'text',
                timeout: 5000,
                error: function() {
                    console.log("Erreur de requete !");
                }
            }).done(function (resp) {
                console.log(resp)
            });
        }

    });
</script>
<%-- TODO : COntinuer cette US --%>

</body>
</html>
