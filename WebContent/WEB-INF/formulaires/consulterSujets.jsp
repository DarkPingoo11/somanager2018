<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <c:import url="/inc/head.jsp"/>
    <title>SoManager - Consulter les sujets</title>
</head>
<body>
<div class="wrapper">
    <c:import url="/Navbar"/>
    <!-- Page Content Holder -->
    <div id="content">
        <c:import url="/inc/headerForSidebar.jsp"/>

        <div class="panel panel-primary ">

            <div class="panel-heading">Sujet - Consulter la liste des sujets</div>
            <div class="panel-body">

                <div class="col-lg-6 col-md-6">
                    <div class="panel panel-default ">

                        <div class="panel-heading">Rechercher des sujets</div>
                        <div class="panel-body">
                            <input class="form-control" id="searchbar" type="text"
                                   placeholder="Rechercher par mots-clés" value=""
                                   onkeyup="rechercher()"/>
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                Afficher les sujets par option :
                                <select class="form-control" name="option" id="option" onchange="giveSelection()">
                                    <option value=""></option>
                                    <c:forEach var="option" items="${listeOptions}">
                                        <option value="<c:out value="${option}"/>">
                                            <c:out value="${option}"/>
                                        </option>
                                    </c:forEach>
                                </select>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                Trier par état:
                                <select class="form-control" name="etat" id="etat" onchange="giveSelection()">
                                    <option value=""></option>
                                    <c:if test="${not empty sessionScope.utilisateur}">
                                        <option value="DEPOSE">DEPOSE</option>
                                        <option value="VALIDE">VALIDE</option>
                                    </c:if>
                                    <option value="ATTRIBUE">ATTRIBUE</option>
                                    <option value="PUBLIE">PUBLIE</option>
                                    <c:if test="${not empty sessionScope.utilisateur}">
                                        <option value="REFUSE">REFUSE</option>
                                    </c:if>
                                </select>
                            </div>
                            <div class="col-lg-12">
                                <c:import url="/inc/listeSujets.jsp"/>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-6 col-md-6">
                    <div class="panel panel-default ">
                        <div class="panel-heading">Voir les détails du sujet</div>
                        <div class="panel-body">
                            <div id="drop" ondrop="drop(event)" draggable="false" ondragover="allowDrop(event)"
                                 ondragleave="refuseDrop(event)">

                                <div id="drophidden--1" style="display: block; text-align: center;">
                                    <p>Faites Glisser-déposer un sujet ici pour le voir en détail ou Double-cliquez
                                        dessus</p>
                                    <img src="<c:url value="/images/giphy.gif"/>" alt="Smiley face" height="42"
                                         width="42">
                                </div>

                                <!--  Création d'un résumé invisible tant qu'un id n'a pas été passé dans le div drop -->
                                <c:forEach var="sujet" items="${listeSujets}" varStatus="loop">
                                <fieldset id="drophidden-${loop.index}" style="display: none; overflow: visible;">
                                    <div class="list-group">

                                        <a class="list-group-item active">
                                            <h5 class="list-group-item-heading">Nom du sujet :</h5>
                                            <h4 class="list-group-item-text"><c:out value="${sujet.titre}"/></h4>
                                        </a>
                                        <div class=" list-group-item" style="text-align: center;">
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6"
                                            " >
                                            <c:choose>
                                                <c:when test="${prof eq 'true'||profOption eq 'true' || profResponsable eq 'true'}">
                                                    <form name="formInteretTechno-${loop.index}" method="post"
                                                          action="<c:url value="/EvaluerInteret"/>"
                                                          id="formInteretTechno-${loop.index}">
                                                        <h4>Valeur technologique :</h4>
                                                        <input name="idSujet" type="hidden" value="${sujet.idSujet}">
                                                        <div style="margin-left: auto; margin-right: auto; max-width: 113px; ">
                                                            <div id="colorInteretTechno-${loop.index}"
                                                                 value="${sujet.noteInteretTechno}"
                                                                 style="position:absolute;z-index:1;width:0px;height:25px;margin: 1px;background-color:#ffe419; "></div>
                                                            <img id="imgInteretTechno-${loop.index}"
                                                                 src="<c:url value="/images/etoiles.png"/>"
                                                                 alt="etoiles"
                                                                 style="position:absolute;z-index:2; left: 50%;transform: translate(-50%, 0%); "
                                                                 width="113" height="27"
                                                                 onmousemove="mouseOverEtoiles(event,'colorInteretTechno-${loop.index}');"
                                                                 onmouseout="mouseOutEtoiles('colorInteretTechno-${loop.index}');"
                                                                 onmouseenter="mouseEnterEtoiles('colorInteretTechno-${loop.index}');"
                                                                 draggable="false"
                                                                 onclick="formSubmit('colorInteretTechno-${loop.index}','imgInteretTechno-${loop.index}','${sujet.idSujet}','techno');"
                                                            >
                                                        </div>
                                                    </form>
                                                </c:when>
                                                <c:otherwise>


                                                    <h4>Valeur technologique :</h4>
                                                    <input name="idSujet" type="hidden" value="${sujet.idSujet}">
                                                    <div style="margin-left: auto; margin-right: auto; max-width: 113px; ">
                                                        <div id="colorInteretTechno-${loop.index}"
                                                             value="${sujet.noteInteretTechno}"
                                                             style="position:absolute;z-index:1;width:0px;height:25px;margin: 1px;background-color:#ffe419;"></div>

                                                        <img id="imgInteretTechno-${loop.index}"
                                                             src="<c:url value="/images/etoiles.png"/>" alt="etoiles"
                                                             draggable="false"
                                                             style="position:absolute;z-index:2; left: 50%; transform: translate(-50%, 0%);"
                                                             width="113" height="27">
                                                    </div>
                                                </c:otherwise>
                                            </c:choose>

                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                            <c:choose>
                                                <c:when test="${prof eq 'true'||profOption eq 'true' || profResponsable eq 'true'}">
                                                    <form name="formInteretSujet-${loop.index}" method="post"
                                                          action="<c:url value="/EvaluerInteret"/>"
                                                          id="formInteretSujet-${loop.index}">
                                                        <h4>Intérêt du sujet :</h4>
                                                        <input name="idSujet" type="hidden" value="${sujet.idSujet}">

                                                        <div style="margin-left: auto; margin-right: auto; max-width: 113px; ">
                                                            <div id="colorInteretSujet-${loop.index}"
                                                                 value="${sujet.noteInteretSujet}"
                                                                 style="position:absolute;z-index:1;width:0px;height:25px;margin: 1px;background-color:#ffe419;"></div>
                                                            <img id="imgInteretSujet-${loop.index}"
                                                                 src="<c:url value="/images/etoiles.png"/>"
                                                                 alt="etoiles" draggable="false"
                                                                 style="position:absolute;z-index:2;left: 50%; transform: translate(-50%, 0%);"
                                                                 width="113" height="27"
                                                                 onmousemove="mouseOverEtoiles(event,'colorInteretSujet-${loop.index}');"
                                                                 onmouseout="mouseOutEtoiles('colorInteretSujet-${loop.index}');"
                                                                 onmouseenter="mouseEnterEtoiles('colorInteretSujet-${loop.index}');"
                                                                 onclick="formSubmit('colorInteretSujet-${loop.index}','imgInteretSujet-${loop.index}','${sujet.idSujet}','sujet');"
                                                            >
                                                        </div>

                                                    </form>
                                                </c:when>
                                                <c:otherwise>

                                                    <h4>Intérêt du sujet :</h4>
                                                    <input name="idSujet" type="hidden" value="${sujet.idSujet}">
                                                    <div style="margin-left: auto; margin-right: auto; max-width: 113px; ">
                                                        <div id="colorInteretSujet-${loop.index}"
                                                             value="${sujet.noteInteretSujet}"
                                                             style="position:absolute;z-index:1;width:0px;height:25px;margin: 1px;background-color:#ffe419;"></div>

                                                        <img id="imgInteretSujet-${loop.index}"
                                                             src="<c:url value="/images/etoiles.png"/>"
                                                             draggable="false" alt="etoiles"
                                                             style="position:absolute;z-index:2;left: 50%; transform: translate(-50%, 0%);"
                                                             width="113" height="27">
                                                    </div>
                                                </c:otherwise>
                                            </c:choose>

                                        </div>
                                        </br></br></br></br>
                                    </div>

                                    <a class="list-group-item">
                                        <div id="description-${loop.index}">
                                            <h4 class="list-group-item-heading">Description :</h4>
                                            <h5 class="list-group-item-text"><c:out value="${sujet.description}"/></h5>
                                        </div>
                                    </a>

                                    <a class="list-group-item">
                                        <h4 class="list-group-item-heading">Porteur :</h4>
                                        <h5 class="list-group-item-text"><c:out
                                                value="${nomsPorteursSujets[sujet.idSujet]}"/></h5>
                                    </a>

                                    <a class="list-group-item">
                                        <h4 class="list-group-item-heading">Options :</h4>
                                        <h5 class="list-group-item-text"><c:out
                                                value="${optionSujet[loop.index]}"/></h5>
                                    </a>

                                    <a class="list-group-item">
                                        <h4 class="list-group-item-heading">Nombre d'étudiant :</h4>
                                        <h5 class="list-group-item-text">Entre <c:out value="${sujet.nbrMinEleves}"/> et
                                            <c:out value="${sujet.nbrMaxEleves}"/></h5>
                                    </a>

                                        <c:set var="listeLiens" value="${fn:split(sujet.liens, ';')}"/>
                                    <c:if test="${fn:length(listeLiens)>0 && listeLiens[0]!=''}">
                                    <div class="list-group-item">
                                        <h4 class="list-group-item-heading">Liens utiles :</h4>
                                        <c:forEach var="lienSujet" items="${listeLiens}">
                                            <a href="${lienSujet}" target="_blank" class="list-group-item"
                                               style="overflow: hidden;text-overflow: ellipsis;  ">
                                                <i class="fas fa-link fa-fw" aria-hidden="true"></i>&nbsp; <c:out
                                                    value="${lienSujet}"/>
                                            </a>
                                        </c:forEach>
                                    </div>
                                    </c:if>

                                    <a class="list-group-item">
                                        <h4 class="list-group-item-heading">Intérêts :</h4>
                                        <h5 class="list-group-item-text"><c:out value="${sujet.interets}"/></h5>
                                    </a>

                                    <a class="list-group-item">
                                        <h4 class="list-group-item-heading">Etat :</h4>
                                        <h5 class="list-group-item-text"><c:out value="${sujet.etat}"/></h5>
                                    </a>


                                    <c:if test="${prof eq 'true' || profOption eq 'true' || profResponsable eq 'true'}">
                                    <a class="list-group-item">
                                        <h4 class="list-group-item-heading">Note du poster :</h4>
                                        <h5 class="list-group-item-text"><c:out
                                                value="${notesPosters[loop.index]}"/></h5>
                                    </a>

                                    <a class="list-group-item">
                                        <h4 class="list-group-item-heading">Soutenance :</h4>
                                        <h5 class="list-group-item-text"><c:out
                                                value="${notesSoutenances[loop.index]}"/></h5>
                                    </a>

                                    <a class="list-group-item">
                                        <h4 class="list-group-item-heading">Note intermediaire :</h4>
                                        <h5 class="list-group-item-text"><c:out
                                                value="${notesTravail[loop.index]}"/></h5>
                                    </a>

                                    <a class="list-group-item">
                                        <h4 class="list-group-item-heading">Projet :</h4>
                                        <h5 class="list-group-item-text"><c:out
                                                value="${notesProjet[loop.index]}"/></h5>
                                    </a>
                                    </c:if>

                                    <a class="list-group-item">
                                        <h4 class="list-group-item-heading">Confidentialité :</h4>
                                        <c:choose>
                                            <c:when test="${sujet.confidentialite eq 'true'}">
                                                <h5 class="list-group-item-text">Oui</h5>
                                            </c:when>
                                            <c:otherwise>
                                                <h5 class="list-group-item-text">Non</h5>
                                            </c:otherwise>
                                        </c:choose>
                                    </a>

                                    <a class="list-group-item">
                                        <h4 class="list-group-item-heading">Contrat Pro :</h4>
                                        <c:choose>
                                            <c:when test="${sujet.contratPro eq 'true'}">
                                                <h5 class="list-group-item-text">Oui</h5>
                                            </c:when>
                                            <c:otherwise>
                                                <h5 class="list-group-item-text">Non</h5>
                                            </c:otherwise>
                                        </c:choose>
                                    </a>
                                    <a class="list-group-item">
                                        <h4 class="list-group-item-heading">Possede Poster :</h4>
                                        <h5 class="list-group-item-text"><c:out
                                                value="${mapPossedePoster[loop.index]}"/></h5>

                                        <c:if test="${(prof eq 'true' || profOption eq 'true' || profResponsable eq 'true' || serviceCom eq 'true' || etudiant eq 'true') && mapPossedePoster[loop.index] eq 'Poster publié'}">
                                            <form action="<c:url value="/TelechargerPoster?action=telecharger"/>"
                                                  method="post">
                                                <input type="hidden" name="idSujet" id="idSujet"
                                                       value="<c:out value="${sujet.idSujet}" />"/>
                                                <button type="submit" class="btn btn-primary center-block"
                                                        id="submit">
                                                    <i class="fas fa-download fa-fw" aria-hidden="true"></i>&nbsp;
                                                    Télécharger
                                                </button>
                                            </form>
                                            <br>
                                            <form
                                                    action="<c:url value="/TelechargerPoster?action=afficher"/>"
                                                    method="post">
                                                <input type="hidden" name="idSujet" id="idSujet"
                                                       value="<c:out value="${sujet.idSujet}" />"/>
                                                <button type="submit" class="btn btn-primary center-block"
                                                        id="submit">
                                                    <i class="fas fa-image fa-fw" aria-hidden="true"></i>&nbsp;
                                                    Afficher
                                                </button>
                                            </form>

                                        </c:if>
                                    </a>


                                    <div style="margin:0px 0px 0px 0%;  float: right;">

                                        <c:if test="${not empty sessionScope.utilisateur}">
                                            <%@ include file="/inc/commenter.jsp" %>
                                        </c:if>

                                    </div>
                            </div>


                            </fieldset>

                            </c:forEach>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

<c:import url="/inc/scripts.jsp"/>

<script src="<c:url value="/js/dragAndDrop.js"/>" type="text/javascript"></script>
<script defer type="text/javascript">
    var sel2 = document.querySelector('#divSujet');
    var options2 = sel2.querySelectorAll('*[id^="draggable-"]');
    var sujetValue = document.querySelectorAll('*[id^="drophidden-"]');

    /**
     * Garde les div sujets qui possède le string écrit dans la searchbar
     * Appelé sur chaque changement de searchbar
     */
    function rechercher() {
        sel2.innerHTML = '';
        var textInput = document.getElementById("searchbar").value
            .toLowerCase();
        for (var i = 0; i < options2.length; i++) {
            //var description = sujetValue[i+1].getElementById("description-");
            if ((sujetValue[i + 1].textContent).toLowerCase().indexOf(
                textInput) != -1) {
                sel2.appendChild(options2[i]);
            }
        }
    }

    /*function garderEtat(){
        var divSujets = sel2.querySelectorAll('*[id^="draggable-"]');
        sel2.innerHTML='';
        var etat = document.getElementById("etat").value;
        for (var i = 0; i < divSujets.length; i++) {
            if ((etat.length>0)?((divSujets[i].dataset.option).indexOf(etat) != -1):true){
                sel2.appendChild(divSujets[i]);
            }
        }
    }*/
</script>
<script type="text/javascript">
    function commentairehidden(id) {
        divCommentaire = document.getElementById("commentairehidden-" + id);

        if (divCommentaire.style.display === "none") {

            divCommentaire.style.display = "block";
            var h = divCommentaire.clientHeight + 3;

            //var w = (document.getElementById("ecrire-" + id)).scrollWidth/2;
            var w = divCommentaire.clientWidth - 20;
            //alert(h);
            divCommentaire.style.margin = "-" + h + "px 0px 0px -" + w + "px";
            //divCommentaire.style.margin = "-" + h + "px 0px 0px 0px";
        } else {
            divCommentaire.style.display = "none";
            divCommentaire.style.margin = "0px 0px 0px 0px";
        }
    }

    function ecrirehidden(id, idBis) {
        divEcrire = document.getElementById("subComment-" + id);
        divCommentaire = document.getElementById("commentairehidden-"
            + idBis);

        if (divEcrire.style.display === "none") {
            divEcrire.style.display = "block";
            var h = divCommentaire.clientHeight + 3;
            var w = divCommentaire.clientWidth - 20;
            divCommentaire.style.margin = "-" + h + "px 0px 0px -" + w + "px";
        } else {
            divEcrire.style.display = "none";
            var h = divCommentaire.clientHeight + 3;
            var w = divCommentaire.clientWidth - 20;
            divCommentaire.style.margin = "-" + h + "px 0px 0px -" + w + "px";
        }
    }

    //ON arrive dans la partie étoilés
    var rectangleWidth;

    function mouseEnterEtoiles(id) {
        var divEtoiles = document.getElementById(id);
        rectangleWidth = divEtoiles.style.width;
    }

    function mouseOverEtoiles(event, id) {
        var divEtoiles = document.getElementById(id);
        var offsets = divEtoiles.getBoundingClientRect();
        var left = event.clientX - offsets.left;
        divEtoiles.style.width = left + "px";
    }

    function mouseOutEtoiles(id) {

        var divEtoiles = document.getElementById(id);
        divEtoiles.style.width = rectangleWidth;
    }

    var sel2Drop = document.querySelector('#drop');
    var options2Drop = sel2Drop.querySelectorAll('*[id^="colorInteret"]');

    /** T'as vu celui là il est français */
    function colorierEtoiles() {
        for (var i = 0; i < options2Drop.length; i++) {
            var value = options2Drop[i].getAttribute('value');

            options2Drop[i].style.width = (value / 5) * 112 + "px";

        }
    }

    colorierEtoiles()

    function formSubmit(idColor, idForm, idSujet, nameNote) {
        var divEtoiles = document.getElementById(idColor);
        var divForm = document.getElementById(idForm);
        var intEtoiles = divEtoiles.style.width.substring(0, divEtoiles.style.width.length - 2);
        var intForm = divForm.width;
        var score = ((intEtoiles * 5) / intForm);

        $.ajax({
            url: 'EvaluerInteret',
            type: "POST",
            data: {"idSujet": idSujet, "score": score, "nameNote": nameNote},
            success: function (data) {
                getEtoile(idSujet, nameNote, idColor)
            }, error: function () {
                alert("error");
            }
        });
    }

    function getEtoile(idSujet, nameNote, idColor) {
        var divEtoiles = document.getElementById(idColor);
        $.ajax({
            url: 'EvaluerInteret',
            type: "GET",
            data: {"idSujet": idSujet, "nameNote": nameNote},
            success: function (data) {
                divEtoiles.style.width = (data / 5) * 112 + "px";
                rectangleWidth = (data / 5) * 112 + "px";
            }, error: function () {
                alert("error");
            }
        });
    }


</script>
</body>
</html>