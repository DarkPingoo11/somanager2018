<%--
  Created by IntelliJ IDEA.
  User: tristanlegacque
--%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <c:import url="/inc/head.jsp" />
    <title>Installation de SoManager</title>
</head>
<body>
<div class="container">

    <div class="row" style="margin-top: 30px;margin-bottom: 20px">
        <div class="col-sm-12" style="text-align: center">
            <h3><i class="fas fa-magic fa-fw" aria-hidden="true"></i>&nbsp;
                Installation de SoManager</h3>
        </div>
    </div>
    <div class="row" style="margin: auto auto 8px auto;max-width: 600px">
        <c:import url="/inc/callback.jsp" />
    </div>


    <%-- 1. Configuration de la BDD --%>
    <c:if test="${requestScope.state eq 'BDDPARAM' or empty requestScope.state}">
        <div class="panel panel-primary" style="max-width: 600px; margin: auto">
            <div class="panel-heading">1. Paramètrage de base de données</div>
            <div class="panel-body">
                <form action="<c:url value="/Installation"/>" method="POST" id="validerBDD">
                    <input type="hidden" name="formulaire" value="validerBDD">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="hote">Hôte</label>
                            <input type="text" class="form-control" id="hote" name="hote"
                                   value="${requestScope.hote}">
                        </div>
                        <div class="form-group">
                            <label for="bdd">Base de données</label>
                            <input type="text" class="form-control" id="bdd" name="bdd"
                                   value="${requestScope.bdd}">
                        </div>
                        <div class="form-group">
                            <label for="driver">Driver</label>
                            <input type="text" class="form-control" id="driver" name="driver"
                                   value="${requestScope.driver}" readonly>
                        </div>
                        <div class="form-group">
                            <label for="username">Nom d'utilisateur</label>
                            <input type="text" class="form-control" id="username" name="username" placeholder="Nom d'utilisateur"
                                   value="${requestScope.nomUtilisateur}" required>
                        </div>
                        <div class="form-group">
                            <label for="password">Mot de passe</label>
                            <input type="password" class="form-control" id="password" name="password"
                                   value="${requestScope.motDePasse}" required>
                        </div>

                        <div class="col-sm-12 no-padding">
                            <div class="col-sm-4 no-padding">
                                <button type="button" class="btn btn-success" data-action="testerConnexionDb">
                                    Tester la connexion
                                </button>
                            </div>
                            <div class="col-sm-8">
                                <span data-replace="callbacktestdb"></span>
                            </div>
                        </div>

                        <div class="col-sm-12 no-padding">
                            <button type="submit" class="btn btn-primary pull-right" disabled>
                                Suivant <i class="fas fa-arrow-right fa-fw" aria-hidden="true"></i>&nbsp;
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </c:if>

    <%-- 2. Insertion des schemas SQL --%>
    <c:if test="${requestScope.state eq 'BDDSCRIPT'}">
        <div class="panel panel-primary" style="max-width: 600px; margin: auto" id="bddScriptDebut">
            <div class="panel-heading">2. Création de la base de données</div>
            <div class="panel-body">
                <div class="alert alert-info" style="text-align: justify">
                    <strong>
                        <i class="fas fa-info-circle fa-fw" aria-hidden="true"></i>
                        &nbsp;Important !
                    </strong>
                    Les scripts de création de la base de données pour somanager vont être executés.
                    Les bases suivantes vont être <b>supprimées</b> avant d'être recrées :
                    <br /><i class="fas fa-caret-right fa-fw" aria-hidden="true"></i>&nbsp;somanager
                    <br /><i class="fas fa-caret-right fa-fw" aria-hidden="true"></i>&nbsp;somanagertest
                </div>
                <div class="alert alert-warning" style="text-align: justify">
                    <strong>
                        <i class="fas fa-exclamation-triangle fa-fw" aria-hidden="true"></i>
                        &nbsp;Important !
                    </strong>
                    Un compte avec <b>les accès root</b> est necessaire pour cette opération, si le compte que vous avez
                    rentré précédemment n'a pas les accès necessaires, merci de rentrer un nouvel utilisateur
                    ci-dessous.
                </div>

                <form action="<c:url value="/Installation"/>" method="POST" id="scriptBDD">
                    <input type="hidden" name="formulaire" value="creerBDD">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="rootusername">Nom d'utilisateur root</label>
                            <input type="text" class="form-control" id="rootusername" name="username"
                                   value="${requestScope.nomUtilisateur}">
                        </div>
                        <div class="form-group">
                            <label for="rootpassword">Mot de passe root</label>
                            <input type="password" class="form-control" id="rootpassword" name="password"
                                   value="${requestScope.motDePasse}">
                        </div>

                        <div class="col-sm-12 no-padding">
                            <div class="col-sm-4 no-padding">
                                <button type="button" class="btn btn-success" data-action="testerAccesRoot">
                                    <i class="fas fa-check fa-fw" aria-hidden="true"></i>&nbsp;
                                    Tester les accès
                                </button>
                            </div>
                            <div class="col-sm-8">
                                <span data-replace="callbacktestacces"></span>
                            </div>
                        </div>

                        <div class="col-sm-12 no-padding" style="margin-top: 5px">
                            <button type="button" class="btn btn-primary pull-left" data-action="createBDD" id="createBDD" disabled>
                                <i class="fas fa-magic fa-fw" aria-hidden="true"></i>&nbsp;
                                Créer la base de données
                            </button>
                        </div>

                    </div>
                </form>
            </div>
        </div>

        <div class="panel panel-primary" style="max-width: 600px; margin: auto;display:none" id="bddScriptFin">
            <div class="panel-heading">2. Création de la base de données</div>
            <div class="panel-body">
                <div class="alert alert-info" style="text-align: justify" id="bddLoading">
                    <strong>
                        <i class="fas fa-spinner fa-pulse fa-fw" aria-hidden="true"></i>&nbsp;
                        Création de la base de données en cours...
                    </strong>
                </div>

                <div class="alert alert-success" style="text-align: justify;display:none" id="bddLoadingSucces">
                    <strong>
                        <i class="fas fa-check fa-fw" aria-hidden="true"></i>&nbsp;
                        Succès
                    </strong>
                    La base de données à bien été créée !
                </div>

                <div id="bddLoadingEchec" style="display:none;text-align: justify;margin-bottom: 20px;">
                    <div class="alert alert-danger" style="text-align: justify;margin-bottom: 10px;">
                        <strong>
                            <i class="fas fa-times fa-fw" aria-hidden="true"></i>&nbsp;
                            Echec !
                        </strong>
                        Une erreur est survenue lors de la création de la base.
                    </div>
                    Cette erreur est probablement du à la présence de TRIGGER ou de PROCEDURE dans le schéma SQL.
                    Pour le bon fonctionnement de l'application, veuillez effectuer cette étape manuellement.
                    <br /><b>Etapes :</b>
                    <br /><i class="fas fa-caret-right fa-fw" aria-hidden="true"></i>&nbsp;1. Télécharger le fichier.sql
                    <br /><i class="fas fa-caret-right fa-fw" aria-hidden="true"></i>&nbsp;2. Se connecter à PhpMyAdmin
                    <br /><i class="fas fa-caret-right fa-fw" aria-hidden="true"></i>&nbsp;3. Importer le fichier.sql
                    <br /><span style="color:red">Une fois ces 3 étapes réalisées, cliquez sur <b>Suivant</b></span>
                    <br />
                    <br />

                    <a href="<c:url value="/download/somanager.sql"/>" type="button" class="btn btn-primary pull-left">
                        <i class="fas fa-download fa-fw" aria-hidden="true"></i>&nbsp;Télécharger le script
                    </a>
                </div>

                <form action="<c:url value="/Installation"/>" method="post">
                    <input type="hidden" name="formulaire" value="finirCreerBdd">
                    <button type="submit" class="btn btn-primary pull-right" id="scriptBDDnext" disabled>
                        Suivant <i class="fas fa-arrow-right fa-fw" aria-hidden="true"></i>&nbsp;
                    </button>
                </form>


            </div>
        </div>
    </c:if>

    <%-- 3. Récapitulatif --%>
    <c:if test="${requestScope.state eq 'RECAP'}">
        <div class="panel panel-primary" style="max-width: 600px; margin: auto">
            <div class="panel-heading">3. Finalisation de l'installation</div>
            <div class="panel-body">
                <div class="alert alert-success" style="text-align: justify">
                    <strong>
                        <i class="fas fa-check fa-fw" aria-hidden="true"></i>
                    </strong>
                    &nbsp;Paramétrage de la base de données <b class="pull-right">Terminé !</b>
                </div>
                <div class="alert alert-success" style="text-align: justify">
                    <strong>
                        <i class="fas fa-check fa-fw" aria-hidden="true"></i>
                    </strong>
                    &nbsp;Création des tables somanager et somanagertest <b class="pull-right">Terminé !</b>
                </div>
                <div class="alert alert-success" style="text-align: justify">
                    <strong>
                        <i class="fas fa-check fa-fw" aria-hidden="true"></i>
                    </strong>
                    &nbsp;Insertion des valeurs par défaut <b class="pull-right">Terminé !</b>
                </div>
                <div class="alert alert-success" style="text-align: justify">
                    <strong>
                        <i class="fas fa-check fa-fw" aria-hidden="true"></i>
                    </strong>
                    &nbsp;Création du compte administrateur <b class="pull-right">Terminé !</b>
                </div>

                <div class="alert alert-info" style="text-align: justify">
                    Le compte administrateur par défaut est le suivant :
                    <br /><b><i class="fas fa-user fa-fw" aria-hidden="true"></i>&nbsp;admin</b>
                    <br /><b><i class="fas fa-key fa-fw" aria-hidden="true"></i>&nbsp;network</b>
                </div>
                <b>
                    <i class="fas fa-caret-right fa-fw" aria-hidden="true"></i>&nbsp;
                    Veuillez redémarrer l'application pour finaliser l'installation.
                </b>

            </div>
        </div>
    </c:if>

</div>
<c:import url="/inc/scripts.jsp" />
<script type="application/javascript" src="<c:url value="/js/scripts/installation.js"/>"></script>
</body>
</html>