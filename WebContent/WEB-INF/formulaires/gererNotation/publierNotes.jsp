
<%@ page language="java" contentType="text/html; charset=UTF-8"
		 pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<div class="col-sm-12">
	<p style="text-align: justify;">
		Vous allez publier les notes d'un sprint. Une fois cette op�ration effectu�e, les notes seront disponibles
		pour tous les �tudiants, et ne seront par cons�quent plus modifiables. Veuillez vous assurer que toutes les
		personnes concern�es ont bien rentr� leurs notes. Veuillez choisir un sprint pour commencer.
	</p>
	<!-- Filtre Sprint -->
	<div class="col-sm-6 no-padding">
		<div class="col-sm-6 no-padding">
			<select class="selectpicker" name="sprint" id="selectionnerSprint">
				<c:forEach var="sprint" items="${requestScope.sprintsAnnee}">
					<option value="<c:out value="${sprint.idSprint}"/>"
							<c:if test="${sprint.notesPubliees eq true}"> data-icon="fa-lock fa-fw" disabled</c:if>
							data-numero="${sprint.numero}"
					>
						Sprint <c:out value="${sprint.numero}" />
					</option>
				</c:forEach>
			</select>
		</div>
		<div class="col-sm-6">
			<button class="btn btn-primary pull-right" role="button" data-action="openSelectionnerSprintModal">
				<i class="fas fa-paper-plane fa-fw" aria-hidden="true"></i>&nbsp;
				Publier les notes
			</button>
		</div>
	</div>
</div>

<%-- Modal de confirmation de publier --%>
<div id="confirmerPublier" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title text-red">
					<i class="fas fa-exclamation-triangle fa-fw" aria-hidden="true"></i>&nbsp;
					Attention - Action critique
				</h4>
			</div>
			<div class="modal-body">
				<p class="text-red text-justify">
					�tes vous sur de vouloir publier les notes ? Une fois cette action r�alis�e, les notes
					seront publi�es et visible par tous les �l�ves, et il ne sera plus possible de les modifier.
				</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
				<button type="button" class="btn btn-primary" data-dismiss="modal" data-action="publierNotes">
					<i class="fas fa-paper-plane fa-fw" aria-hidden="true"></i>&nbsp;
					Publier les notes du sprint <span data-replace="sprint"></span>
				</button>
			</div>
		</div>

	</div>
</div>

