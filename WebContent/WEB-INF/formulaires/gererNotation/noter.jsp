<%@ page language="java" contentType="text/html; charset=UTF-8"
		 pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<div class="panel-body">
	<c:if test="${empty sujetSuivi && empty sujetPosters && empty sujetSoutenances }">
		<p align="center">Vous n'avez aucune notes � saisir pour le moment</p>
	</c:if>
	<c:if test="${!empty sujetSuivis }">
		<div class="col-lg-12 col-md-12">
			<div class="panel panel-default ">
				<div class="panel-heading" align="center">Notes de suivi :</div>
				<div class="panel-body">
					<form method="post" action="<c:url value="/Noter"/>">
						<input type="hidden" id="type" name="type" value="suivi"/>
						<c:forEach var="sujet" items="${sujetSuivis}" varStatus="loop">
							<div class="text-center">
								<p> <b> Equipe <c:out value="${loop.index + 1}"/> &emsp; - &emsp;
									Sujet : <c:out value="${sujet.titre}"/> </b> </p>
							</div>
							<c:if test="${!empty utilisateurSuivis[loop.index]}">
								<table class="table table-condensed">
									<thead>
									<tr>
										<th>Nom</th>
										<th>Prenom</th>
										<th>Note Mi Avancement</th>
										<th>Note Projet</th>
									</tr>
									</thead>
									<tbody>
									<c:forEach var="utilisateur" items="${utilisateurSuivis[loop.index]}" varStatus="loop1">
										<tr>
											<td><c:out value="${utilisateur.nom}"/></td>
											<td><c:out value="${utilisateur.prenom}"/></td>
											<td><input type="number" id="noteIntermediaire<c:out value="${utilisateur.idUtilisateur}"/>"
													   name="noteIntermediaire<c:out value="${utilisateur.idUtilisateur}"/>"
													   value="<c:out value="${etudiantSuivis[loop.index][loop1.index].noteIntermediaire}"/>" step="0.1" min="-1" max="20"/></td>
											<td><input type="number" id="noteProjet<c:out value="${utilisateur.idUtilisateur}"/>"
													   name="noteProjet<c:out value="${utilisateur.idUtilisateur}"/>"
													   value="<c:out value="${etudiantSuivis[loop.index][loop1.index].noteProjet}"/>" step="0.1" min="-1" max="20"/></td>
										</tr>
									</c:forEach>
									</tbody>
								</table>
							</c:if>
							<c:if test="${empty utilisateurSuivis[loop.index]}">
								<p align="center">Aucune �quipe n'est inscrite sur le sujet pour le moment</p>
							</c:if>
							<br/>
						</c:forEach>
						<div class="text-center">
							<button type="submit" class="btn btn-primary" id="submitSujet">
								<i class="fas fa-check-circle fa-fw" aria-hidden="true"></i>&nbsp; Envoyer notes
							</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</c:if>
	<c:if test="${!empty sujetPosters }">
		<div class="col-lg-6 col-md-6">
			<div class="panel panel-default ">
				<div class="panel-heading" align="center">Notes poster :</div>
				<div class="panel-body">
					<form method="post" action="<c:url value="/Noter"/>">
						<input type="hidden" id="type" name="type" value="poster"/>
						<c:forEach var="sujet" items="${sujetPosters}" varStatus="loop">
							<div class="text-center">
								<p> <b> Equipe <c:out value="${loop.index + 1}"/> &emsp; - &emsp;
									Sujet : <c:out value="${sujet.titre}"/> </b> </p>
							</div>
							<table class="table table-condensed">
								<thead>
								<tr>
									<th>Nom</th>
									<th>Prenom</th>
									<th>Note Poster</th>
								</tr>
								</thead>
								<tbody>
								<c:forEach var="utilisateur" items="${utilisateurPosters[loop.index]}" varStatus="loop1">
									<tr>
										<td><c:out value="${utilisateur.nom}"/></td>
										<td><c:out value="${utilisateur.prenom}"/></td>
										<td><input type="number" name="notePoster<c:out value="${utilisateur.idUtilisateur}"/>"
												   id="notePoster<c:out value="${utilisateur.idUtilisateur}"/>"
												   value="<c:out value="${notePosters[loop.index][loop1.index].note}"/>" step="0.1" min="-1" max="20"/></td>
									</tr>
								</c:forEach>
								</tbody>
							</table>
							<br/>
						</c:forEach>
						<div class="text-center">
							<button type="submit" class="btn btn-primary" id="submitSujet">
								<i class="fas fa-check-circle fa-fw" aria-hidden="true"></i>&nbsp; Envoyer notes
							</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</c:if>
	<c:if test="${!empty sujetSoutenances }">
		<div class="col-lg-6 col-md-6">
			<div class="panel panel-default ">
				<div class="panel-heading" align="center">Notes soutenance :</div>
				<div class="panel-body">
					<form method="post" action="<c:url value="/Noter"/>">
						<input type="hidden" id="type" name="type" value="soutenance"/>
						<c:forEach var="sujet" items="${sujetSoutenances}" varStatus="loop">
							<div class="text-center">
								<p> <b> Equipe <c:out value="${loop.index + 1}"/> &emsp; - &emsp;
									Sujet : <c:out value="${sujet.titre}"/> </b> </p>
							</div>
							<table class="table table-condensed">
								<thead>
								<tr>
									<th>Nom</th>
									<th>Prenom</th>
									<th>Note Soutenance</th>
								</tr>
								</thead>
								<tbody>
								<c:forEach var="utilisateur" items="${utilisateurSoutenances[loop.index]}" varStatus="loop1">
									<tr>
										<td><c:out value="${utilisateur.nom}"/></td>
										<td><c:out value="${utilisateur.prenom}"/></td>
										<td><input type="number" id="noteSoutenance<c:out value="${utilisateur.idUtilisateur}"/>"
												   name="noteSoutenance<c:out value="${utilisateur.idUtilisateur}"/>"
												   value="<c:out value="${noteSoutenances[loop.index][loop1.index].note}"/>" step="0.1" min="-1" max="20"/></td>
									</tr>
								</c:forEach>

								</tbody>
							</table>
							<br/>
						</c:forEach>
						<div class="text-center">
							<button type="submit" class="btn btn-primary" id="submitSujet">
								<i class="fas fa-check-circle fa-fw" aria-hidden="true"></i>&nbsp; Envoyer notes
							</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</c:if>
</div>
	