
<%@ page language="java" contentType="text/html; charset=UTF-8"
		 pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<div class="col-sm-12">
	<div class="panel panel-primary" style="max-height: 100%;">
		<div class="panel-heading" style="padding-bottom: 0">
			<div class="row">
				<form action="<c:url value="/Noter"/>" method="get" id="actualiserPage">
					<!-- Filtres -->
					<div class="col-sm-12" style="margin-bottom: 15px">
						<!-- Filtre ann�e scolaire -->
						<div class="col-sm-3">

							Ann�e Scolaire :
							<select class="form-control" name="annee" data-action="actualiserChamps" data-champ="idAnnee">
								<option value=""></option>
								<c:forEach var="anneeScolaire" items="${anneeScolaires}">
									<option
											value="<c:out value="${anneeScolaire.idAnneeScolaire}"/>"
											<c:if test="${ anneeS eq anneeScolaire.idAnneeScolaire}"> selected</c:if>
									>
										<c:out value="${anneeScolaire.anneeDebut}" />
										<c:out value="${anneeScolaire.anneeFin}" />
									</option>
								</c:forEach>
							</select>
						</div>
						<!-- Filtre Sprint -->
						<div class="col-sm-2">

							Sprint : <select class="form-control" name="sprint" data-action="actualiserChamps" data-champ="idSprint">
							<option value=""></option>
							<c:forEach var="sprint" items="${sprints}">
								<option value="<c:out value="${sprint.idSprint}"/>"
										data-idAnnee="${sprint.refAnnee}"
										<c:if test="${ sprintS eq sprint.idSprint}"> selected</c:if>
								>
									<c:out value="${sprint.numero}" />
								</option>
							</c:forEach>
						</select>

						</div>

						<!-- Filtre Equipe -->
						<div class="col-sm-3">
							Equipe : <select class="form-control" name="equipe">
							<option value=""></option>
							<c:forEach var="equipe" items="${pglEquipes}">
								<option value="<c:out value="${equipe.idEquipe}" />"
										data-idAnnee="${equipe.refAnnee}"
										<c:if test="${ equipeS eq equipe.idEquipe}"> selected</c:if>
								>
									<c:out value=" ${equipe.nom}" />
								</option>
							</c:forEach>
						</select>
						</div>
						<!-- Filtre Mati�re -->
						<div class="col-sm-3">

							Mati�re : <select class="form-control" name="matiere">
							<option value=""></option>
							<c:forEach var="matiere" items="${matieres}">
								<option value="<c:out value="${matiere.idMatiere}"/>"
										data-idSprint="${matiere.refSprint}"
										<c:if test="${ matiereS eq matiere.idMatiere}"> selected</c:if>
								>
									<c:out value=" ${matiere.libelle}" />
								</option>
							</c:forEach>
						</select>

						</div>
						<div class="col-sm-1">
							&nbsp;
							<button class="btn btn-block form-control" role="button" type="button">
								<i class="fa fa-search fa-fw" aria-hidden="true"></i>&nbsp;
							</button>

						</div>
					</div>
				</form>
			</div>
		</div>


		<!-- Tableau de r�sultats -->
		<div class="panel-body table-container">
			<table class="table table-bordered" style="margin-bottom: -1px">
				<tr class="info unselectable">
					<th class="col-sm-3">Nom Pr�nom</th>
					<th class="col-sm-3">Mati�re</th>
					<th class="col-sm-2">Note</th>
					<th class="col-sm-2">Coeff</th>
					<th class="col-sm-2">Actions</th>
				</tr>
			</table>

			<!-- Contenu du tableau -->
			<div class="row">
				<div class="col-sm-12" id="listeNoteContainer"
					 style="height: 300px;">
					<table class="table table-hover" style="margin-bottom: 0">
						<tbody id="resultats">

						<%-- Pour chaque utilisateur --%>
						<c:forEach var="resultats" items="${resultats}">

							<%-- Pour chaque Mati�re+Note --%>
							<c:forEach var="entity" items="${resultats.value}">
								<!-- Le nom des membres de l'�quipes -->
								<tr data-idUser="${resultats.key.idUtilisateur}" data-idMatiere="${entity.key.idMatiere}">
									<td class="col-sm-3"> ${resultats.key.nom} ${resultats.key.prenom} </td>
									<td class="col-sm-3"> ${entity.key.libelle } </td>
									<td class="col-sm-2">
										<input class="input-sm form-control" type="number" min="0" max="20" step="0.1"
											   name="note"
											   value="${entity.value.note}"
											   <c:if test="${requestScope.estPublie eq true}">disabled</c:if>
										>
									</td>
									<td class="col-sm-2"> ${entity.key.coefficient } </td>
									<td class="col-sm-2">
										<button class="btn btn-success btn-sm btn-block" data-action="envoyerNote"
												<c:if test="${requestScope.estPublie eq true}">disabled</c:if>
										>
											Noter&nbsp;
											<i class="fas fa-check" aria-hidden="true"></i>
										</button>
									</td>
								</tr>

							</c:forEach>
						</c:forEach>
						</tbody>
					</table>

				</div>
			</div>
		</div>
	</div>
</div>


