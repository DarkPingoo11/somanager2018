
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <c:import url="/inc/head.jsp"/>
    <title>SoManager - Notes </title>
    <link rel="stylesheet" media="screen"
          href="<c:url value="/css/bootstrap-datetimepicker.min.css"/>">
</head>
<body>
<div class="wrapper">
    <c:import url="/Navbar"/>
    <!-- Page Content Holder -->
    <div id="content">
        <c:import url="/inc/headerForSidebar.jsp"/>


        <div class="panel panel-primary ">
            <div class="panel-heading">Les Notes
            </div>

            <div class="panel-body">
                <c:import url="/inc/callback.jsp"/>

                <ul class="nav nav-tabs" role="tablist" style="margin-bottom: 20px">
                    <li class="active"><a data-toggle="tab" href="#mesNotes">
                        <i class="fas fa-calendar-check" aria-hidden="true"></i>&nbsp;
                        Mes Notes
                    </a></li>
                    <li><a data-toggle="tab" href="#notesEquipes">
                        <i class="far fa-calendar-check" aria-hidden="true"></i>&nbsp;
                        Notes Equipes
                    </a></li>
                </ul>

                <%-- Contenu des différentes tab --%>
                <div class="tab-content">

                    <%-- Gérer les meetings--%>
                    <div id="mesNotes" class="tab-pane fade in active">
                        <div class="row">
                            <c:import url="/WEB-INF/formulaires/gestionVoirLesNotes/voirMesNotes.jsp"/>
                        </div>
                    </div>

                    <%-- Ajouter meeting --%>
                    <div id="notesEquipes" class="tab-pane fade">
                        <div class="row">
                            <c:import url="/WEB-INF/formulaires/gestionVoirLesNotes/voirLesNotesEquipes.jsp"/>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<c:import url="/inc/scripts.jsp"/>

<script type="text/javascript" src="<c:url value="/js/bootstrap-datetimepicker.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/js/bootstrap-datetimepicker.fr.js"/>"></script>
<script type="text/javascript" src="<c:url value="/js/scripts/voirMesNotes.js"/>"></script>
<script type="text/javascript">
    $('.years').datetimepicker({
        format: "yyyy",
        startView: 'decade',
        minView: 'decade',
        viewSelect: 'decade',
        autoclose: true,
    });

    $(function () {
        $('#datetimepicker').datetimepicker({
            icons: {
                time: 'glyphicon glyphicon-time',
                date: 'glyphicon glyphicon-calendar',
                up: 'glyphicon glyphicon-chevron-up',
                down: 'glyphicon glyphicon-chevron-down',
                previous: 'glyphicon glyphicon-chevron-left',
                next: 'glyphicon glyphicon-chevron-right',
                today: 'glyphicon glyphicon-screenshot',
                clear: 'glyphicon glyphicon-trash',
                close: 'glyphicon glyphicon-remove'
            }
        });

    });

    $('.form_date').datetimepicker({
        language: 'fr',
        weekStart: 1,
        todayBtn: 1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 2,
        minView: 2,
        forceParse: 0
    }).on('hide', function (event) {
        event.preventDefault();
        event.stopPropagation();
    });

 


</script>
</body>
</html>
