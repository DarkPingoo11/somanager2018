<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <c:import url="/inc/head.jsp"/>
    <title>SoManager - Connexion</title>
</head>
<body>
<div class="wrapper">
    <c:import url="/Navbar"/>
    <div id="content">
        <c:import url="/inc/headerForSidebar.jsp"/>
        <div class="row">
            <div class="col-lg-4 col-lg-offset-4">
                <div class="panel panel-primary ">
                    <div class="panel-heading">Se Connecter</div>
                    <div class="panel-body">
                        <form data-toggle="validator" role="form" method="post" action="<c:url value="/Connexion"/>">
                            <fieldset>
                                <div class="form-group has-feedback">
                                    <label for="identifiant" class="control-label">Identifiant : <span
                                            class="requis">*</span></label>
                                    <input type="text" name="identifiant" class="form-control" id="identifiant"
                                           autofocus required/>
                                    <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                    <div class="help-block with-errors"></div>
                                </div>

                                <div class="form-group has-feedback">
                                    <label for="motDePasse" class="control-label">Mot de passe : <span
                                            class="requis">*</span></label>
                                    <input type="password" name="motDePasse" class="form-control" id="motDePasse"
                                           required/>
                                    <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                    <div class="help-block with-errors"></div>
                                </div>
                                <br>

                                <c:if test="${!empty erreurs}">
                                    <div class="text-center">
                                        <span class="erreur">${resultat} ${erreurs['motDePasse']}</span>
                                    </div>
                                    <br>
                                </c:if>

                                <div class="text-center">
                                    <button type="submit" class="btn btn-primary" id="submit">
                                        <i class="fas fa-sign-in-alt fa-fw" aria-hidden="true"></i>&nbsp; Se Connecter
                                    </button>
                                </div>
                                <br>
                            </fieldset>
                        </form>
                        <p class="text-center">
                            <em>Pas encore de compte ? </em>
                            <a href="<c:url value="/Inscription"/>">Inscription</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<c:import url="/inc/scripts.jsp"/>

<script src="<c:url value="/js/permissionNotif.js"/>" type="text/javascript"></script>

</body>
</html>