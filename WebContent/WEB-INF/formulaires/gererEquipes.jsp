<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <c:import url="/inc/head.jsp"/>
    <title>SoManager - Gérer les équipes</title>
</head>
<body>
<div class="wrapper">
    <c:import url="/Navbar"/>
    <!-- Page Content Holder -->
    <div id="content">
        <c:import url="/inc/headerForSidebar.jsp"/>


        <div class="panel panel-primary ">
            <div class="panel-heading">Equipe - Gestion des équipes</div>
            <div class="panel-body">

                <div class="col-lg-4 col-md-6 col-sm-12">
                    <div class="panel panel-default" id="listeElevesNonAttribues">
                        <div class="panel-heading">Élèves non attribués</div>
                        <div class="panel-body">
                            <div class="col-lg-12">
                                <c:import url="/inc/listeElevesI3.jsp"/>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-8 col-md-6 col-sm-12">


                    <div class="panel panel-default" id="sujetSansEquipe">
                        <div class="panel-heading">Sujets sans équipe</div>
                        <div class="panel-body">
                            <div class="col-lg-12">
                                <c:import url="/inc/listeSujetsValidesInfos.jsp"/>
                            </div>
                            <a href="<c:url value="/CreerEquipe"/>" class="btn btn-primary"><i class="fas fa-plus fa-fw"
                                                                                               aria-hidden="true"></i>&nbsp;
                                Créer une équipe</a>
                        </div>
                    </div>

                    <div class="panel panel-default" id="equipesCreees">
                        <div class="panel-heading">Équipes créées</div>
                        <div class="panel-body">

                            <div class="col-lg-12">
                                <c:import url="/inc/listeEquipes.jsp"/>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<c:import url="/inc/scripts.jsp"/>

<script src="<c:url value="/js/dragAndDrop.js"/>"></script>
<script>
    function changerTailleScroll() {
        document.getElementById("tableAttribution").style.height = (document.getElementById('sujetSansEquipe').clientHeight + document.getElementById('equipesCreees').clientHeight) + "px";
    }

    window.onload = changerTailleScroll;
</script>
</body>
</html>