<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<c:import url="/inc/head.jsp" />
<link rel="stylesheet" href="<c:url value="/css/trombi.css"/>">
<title>SoManager - Trombinoscope</title>
</head>
<body>
	<div class="wrapper">
		<c:import url="/Navbar" />
		<!-- Page Content Holder -->
		<div id="content">
			<c:import url="/inc/headerForSidebar.jsp" />


			<c:if test="${admin eq 'true'}">
				<div class="panel panel-primary ">
					<div class="panel-heading">Importer toutes les photos</div>
					<div class="panel-body">
						<div class="col-lg-12">
							<div class="alert alert-info">
								<p align="center" style="font-size: 12px">Le fichier utilis�
									doit �tre de type *.zip</p>
								<p align="center" style="font-size: 12px">
									Chaque image composant l'archive doit �tre sous la forme : <strong>identifiant.jpg</strong>
								</p>
							</div>
							<form action="<c:url value="Trombinoscope"/>" method="post"
								enctype="multipart/form-data">
								<center>
									<input type="hidden" name="formulaire" id="formulaire"
										value="defaut" /> <label for="fichier"> Emplacement du
										fichier <span class="requis">*</span>
									</label> <input type="file" id="fichier" name="fichier" />
								</center>
								<div class="text-center">
									<span class="erreur">${erreurs['extensionErreur']}</span>
								</div>
								<br>
								<button type="submit" class="btn btn-primary center-block"
									id="submit">
									<i class="fas fa-upload fa-fw"></i>&nbsp; Importer
								</button>
							</form>
						</div>
					</div>
				</div>
			</c:if>

			<div class="panel panel-primary ">
				<div class="panel-heading">Consulter les photos des �tudiants
					I2</div>
				<div class="panel-body">
					<div class="col-lg-12">
						<!-- Menu d�roulant : filtres de s�lection -->
						<div class="row">
							<div class="col-lg-6 col-md-6 col-sm-6">
								<div class="row">
									S�lection par �quipe : <select class="form-control"
										name="equipe" id="selEquipeI2" onchange="actualisationEqpI2()">
										<option value=""></option>
										<c:forEach var="equipe" items="${listeEquipesI2}">
											<option value="<c:out value="${equipe.getIdEquipe()}"/>">
												<c:out value="Equipe ${equipe.getIdEquipe()}" />
											</option>
										</c:forEach>
									</select>
								</div>
							</div>
							<div class="col-lg-6 col-md-6 col-sm-6">
								<div class="row">
									S�lection par option : <select class="form-control"
										name="option" id="selOptionI2" onchange="actualisationOptI2()">
										<option value=""></option>
										<c:forEach var="option" items="${listeOptionsI2}">
											<option value="<c:out value="${option}"/>">
												<c:out value="Option ${option}" />
											</option>
										</c:forEach>
									</select>
								</div>
							</div>
						</div>
						<!-- Tableau : Etudiants inscrits -->
						<div class="row" id="divEtusI2">
							<c:forEach var="etudiant" items="${listeEtudiantsI2}"
								varStatus="loop">
								<div class="memberI2"
									data-equipe="${equipesEtIdsStringI2.get(loop.count-1)}"
									data-option="${optionsEtIdsStringI2.get(loop.count-1)}">
									<div>
										<object class="membpict"
											data="<c:url value="images/users/${etudiant.idUtilisateur}.jpg"/>">
											<img src="<c:url value="images/users/def_user.png"/>"
												alt="Impossible d'afficher la photo.">
										</object>
									</div>
									<div class="membdata">
										<c:out value="${etudiant.prenom}" />
										<c:out value="${etudiant.nom}" />
									</div>
								</div>
							</c:forEach>
						</div>
					</div>
				</div>
			</div>

			<div class="panel panel-primary ">
				<div class="panel-heading">Consulter les photos des �tudiants
					I3</div>
				<div class="panel-body">
					<div class="col-lg-12">
						<!-- Menu d�roulant : filtres de s�lection -->
						<div class="row">
							<div class="col-lg-6 col-md-6 col-sm-6">
								<div class="row">
									S�lection par �quipe : <select class="form-control"
										name="equipe" id="selEquipe" onchange="actualisationEqp()">
										<option value=""></option>
										<c:forEach var="equipe" items="${listeEquipes}">
											<option value="<c:out value="${equipe.getIdEquipe()}"/>">
												<c:out value="Equipe ${equipe.getIdEquipe()}" />
											</option>
										</c:forEach>
									</select>
								</div>
							</div>
							<div class="col-lg-6 col-md-6 col-sm-6">
								<div class="row">
									S�lection par option : <select class="form-control"
										name="option" id="selOption" onchange="actualisationOpt()">
										<option value=""></option>
										<c:forEach var="option" items="${listeOptions}">
											<option value="<c:out value="${option}"/>">
												<c:out value="Option ${option}" />
											</option>
										</c:forEach>
									</select>
								</div>
							</div>
						</div>
						<!-- Tableau : Etudiants inscrits -->
						<div class="row" id="divEtus">
							<c:forEach var="etudiant" items="${listeEtudiants}"
								varStatus="loop">
								<div class="member"
									data-equipe="${equipesEtIdsString.get(loop.count-1)}"
									data-option="${optionsEtIdsString.get(loop.count-1)}">
									<div>
										<object class="membpict"
											data="<c:url value="images/users/${etudiant.idUtilisateur}.jpg"/>">
											<img src="<c:url value="images/users/def_user.png"/>"
												alt="Impossible d'afficher la photo.">
										</object>
									</div>
									<div class="membdata">
										<c:out value="${etudiant.prenom}" />
										<c:out value="${etudiant.nom}" />
									</div>
								</div>
							</c:forEach>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
	<c:import url="/inc/scripts.jsp" />
	<script>
	
    var trombiI2 = document.querySelector('#divEtusI2');
    var photosI2 = trombiI2.querySelectorAll('.memberI2');

    function actualisationOptI2() {
        var option = document.getElementById("selOptionI2").value;
        trombiI2.innerHTML = "";
        for (var i = 0; i < photosI2.length; i++) {
            if ((option.length > 0) ? ((photosI2[i].dataset.option)
                .indexOf(option) != -1) : true) {
                trombiI2.appendChild(photosI2[i]);
            }
        }
    }

    function actualisationEqpI2() {
        var equipe = document.getElementById("selEquipeI2").value;
        trombiI2.innerHTML = "";
        for (var i = 0; i < photosI2.length; i++) {
            if ((equipe.length > 0) ? ((photosI2[i].dataset.equipe)
                .indexOf(equipe) != -1) : true) {
                trombiI2.appendChild(photosI2[i]);
            }
        }
    }
	
    var trombi = document.querySelector('#divEtus');
    var photos = trombi.querySelectorAll('.member');

    function actualisationOpt() {
        var option = document.getElementById("selOption").value;
        trombi.innerHTML = "";
        for (var i = 0; i < photos.length; i++) {
            if ((option.length > 0) ? ((photos[i].dataset.option)
                .indexOf(option) != -1) : true) {
                trombi.appendChild(photos[i]);
            }
        }
    }

    function actualisationEqp() {
        var equipe = document.getElementById("selEquipe").value;
        trombi.innerHTML = "";
        for (var i = 0; i < photos.length; i++) {
            if ((equipe.length > 0) ? ((photos[i].dataset.equipe)
                .indexOf(equipe) != -1) : true) {
                trombi.appendChild(photos[i]);
            }
        }
    }

</script>
</body>
</html>