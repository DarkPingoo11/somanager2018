<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="ISO-8859-1" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <c:import url="/inc/head.jsp"/>
    <title>SoManager - Postuler</title>
</head>
<body>

<div class="wrapper">
    <c:import url="/Navbar"/>
    <!-- Page Content Holder -->
    <div id="content">
        <c:import url="/inc/headerForSidebar.jsp"/>


        <div class="panel panel-primary ">

            <div class="panel-heading">Sujet - Postuler � un ou des
                sujet(s) en tant que...
            </div>

            <div class="panel-body">
                Afficher les sujets par option : <select class="form-control"
                                                         name="option" id="option" onchange="giveSelection()">
                <option value=""></option>
                <c:forEach var="option" items="${listeOptions}">
                    <option value="<c:out value="${option}"/>">
                        <c:out value="${option}"/>
                    </option>
                </c:forEach>
            </select> </br>

                <div id="divSujet">
                    <c:forEach var="sujet" items="${listeSujets}" varStatus="loop">


                        <div id="draggable-${loop.index}" class="panel panel-default"
                             data-option='${optionSujet[loop.index]}'>

                            <div class="panel-heading" value="Afficher ou Masquer"
                                 onClick="AfficherMasquer('${loop.index}')">
								<span id="chevron-${loop.index}"
                                      class="glyphicon glyphicon-chevron-down pull-right"></span>
                                &nbsp;${sujet.titre}
                            </div>

                            <div id="divacacher-${loop.index}" class="panel-body"
                                 style="display: none;">


                                <form method="post"
                                      action="<c:url value="/InscriptionProfSujet"/>">

                                    <div class="col-lg-6">
                                        <input type="hidden" name="idSujet" value="${sujet.idSujet}">
                                        <div class="col-lg-3">
                                            <input type="radio" name="fonction" value="co-encadrant"
                                                ${fn:contains(fonctionsUtilisateurSession[loop.index],'co-encadrant') ? 'checked' : ''}>
                                            Co-encadrant
                                        </div>

                                        <div class="col-lg-3">
                                            <input type="radio" name="fonction" value="consultant"
                                                ${fn:contains(fonctionsUtilisateurSession[loop.index],'consultant') ? 'checked' : ''}>
                                            Consultant
                                        </div>
                                        <div class="col-lg-3">
                                            <input type="radio" name="fonction" value="interesse"
                                                ${fn:contains(fonctionsUtilisateurSession[loop.index],'interess�') ? 'checked' : ''}>
                                            Int�ress�
                                        </div>
                                        <c:if
                                                test="${profOption eq 'true' || profResponsable eq 'true'}">
                                            <div class="col-lg-3">
                                                <input type="radio" name="fonction" value="referent"
                                                    ${fn:contains(fonctionsUtilisateurSession[loop.index],'r�f�rent') ? 'checked' : ''}
                                                    ${fn:contains(referents[loop.index],'r�f�rent') ? 'disabled' : ''}>
                                                R�f�rent
                                            </div>
                                        </c:if>
                                    </div>


                                    <div class="col-lg-6">
                                        <button type="submit" class="btn btn-primary" id="submitSujet">
                                            <i class="fas fa-check-circle fa-fw" aria-hidden="true"></i>&nbsp; Valider
                                        </button>
                                    </div>

                                </form>


                                <div class="col-lg-12">
                                    <table class="table table-striped">
                                        <div>Professeurs attribu�s</div>
                                        <thead>
                                        <tr>
                                            <th>Nom</th>
                                            <th>Pr�nom</th>
                                            <th>Fonction</th>
                                            <th>Valid�</th>
                                            <th style="display: none">Id</th>
                                        </tr>
                                        </thead>


                                        <tbody id="divProfesseur">
                                        <c:forEach var="professeurSujet"
                                                   items="${listeProfesseursAttribuesAUnSujet}"
                                                   varStatus="loop2">
                                            <c:if test="${sujet.idSujet eq professeurSujet.idSujet}">
                                                <tr>
                                                    <c:forEach var="utilisateur" items="${utilisateurs}"
                                                               varStatus="loop3">
                                                        <c:if
                                                                test="${professeurSujet.idProfesseur eq utilisateur.idUtilisateur}">
                                                            <td id="nom-${loop3.index}"><c:out
                                                                    value="${utilisateur.nom}"/></td>
                                                            <td id="prenom-${loop3.index}"><c:out
                                                                    value="${utilisateur.prenom}"/></td>
                                                        </c:if>
                                                    </c:forEach>

                                                    <td id="fonction-${loop2.index}"><c:out
                                                            value="${professeurSujet.fonction}"/></td>
                                                    <c:if
                                                            test="${professeurSujet.fonction eq 'CONSULTANT' || professeurSujet.fonction eq 'COENCADRANT' || professeurSujet.fonction eq 'INTERESSE'}">
                                                        <td id="valide-${loop2.index}"><c:out value="--"/></td>
                                                    </c:if>
                                                    <c:if test="${professeurSujet.fonction eq 'REFERENT'}">
                                                        <td id="valide-${loop2.index}"><c:out
                                                                value="${professeurSujet.valide}"/></td>
                                                    </c:if>
                                                </tr>
                                            </c:if>

                                        </c:forEach>
                                        </tbody>
                                    </table>

                                </div>
                            </div>
                        </div>

                    </c:forEach>
                </div>

            </div>
        </div>
    </div>
</div>
<c:import url="/inc/scripts.jsp"/>
<script src="<c:url value="/js/dragAndDrop.js"/>"></script>

<script>
    /* Masque/Affiche une div*/

    function AfficherMasquer(idSujet) {
        divInfo = document.getElementById("divacacher-" + idSujet);
        chevron = document.getElementById("chevron-" + idSujet);

        if (divInfo.style.display == 'none') {
            divInfo.style.display = '';
            chevron.className = 'glyphicon glyphicon-chevron-up pull-right';

        } else {
            divInfo.style.display = 'none';
            chevron.className = 'glyphicon glyphicon-chevron-down pull-right';

        }

    }
</script>

</body>
</html>