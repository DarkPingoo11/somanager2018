<%--
  Created by IntelliJ IDEA.
  User: dimitrijarneau
  Date: 31/05/2018
  Time: 15:36
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>

<div class="col-lg-6 col-md-6">
    <div class="panel panel-default ">
        <div class="panel-heading" align="center"><i class="far fa-calendar-alt"></i>&nbsp; Années planifiées :</div>
        <div class="panel-body">
            <c:if test="${!empty anneesPlanifiees }">
                <table class="table table-condensed">
                    <thead>
                    <tr>
                        <th>Année</th>
                        <th>Option</th>
                        <th>Date début projet</th>
                        <th>Date mi-avancement</th>
                        <th>Date depot poster</th>
                        <th>Date soutenance finale</th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach var="annee" items="${anneesPlanifiees}" varStatus="loop">
                        <tr>
                            <td><c:out value="${annee.anneeDebut} ${annee.anneeFin}"/></td>
                            <td><c:out value="${optionAnnees[loop.index]}"/></td>
                            <td><fmt:formatDate type="date" value="${annee.dateDebutProjet}"/></td>
                            <td><fmt:formatDate type="date" value="${annee.dateMiAvancement}"/></td>
                            <td><fmt:formatDate type="date" value="${annee.dateDepotPoster}"/></td>
                            <td><fmt:formatDate type="date" value="${annee.dateSoutenanceFinale}"/></td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </c:if>
            <c:if test="${empty anneesPlanifiees }">
                <p align="center">Aucune planification n'a été effectuée</p>
            </c:if>
        </div>
    </div>
</div>
<div class="col-lg-6 col-md-6">
    <div class="panel panel-default ">
        <div class="panel-heading" align="center"><i class="far fa-calendar-plus"></i>&nbsp; Plannifier l'année :</div>
        <div class="panel-body">
            <form data-toggle="validator" role="form" method="post" action="<c:url value="/GestionProjetsEtPFE"/>">
                <div class="col-lg-6 col-md-6">
                    <label><br/>Date début année <span class="requis">*</span></label>
                    <div class="input-group date years" data-date="" data-date-format="yyyy"
                         data-link-field="anneeDebut" data-link-format="yyyy">
                        <input class="form-control" size="16" type="number" value="<c:out value="${anneeDebut}"/>"
                               readonly>
                        <span class="input-group-addon">
										<span class="glyphicon glyphicon-remove"></span></span>
                        <span class="input-group-addon">
										<span class="glyphicon glyphicon-calendar"></span></span>
                        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                        <div class="help-block with-errors"></div>
                    </div>
                    <c:if test="${!empty erreurs}">
										<span class="erreur">
                                                ${erreurs['anneeDebut']}
                                        </span>
                    </c:if>
                    <input type="hidden" id="anneeDebut" name="anneeDebut" value="${anneeDebut}"/><br/>
                </div>

                <div class="col-lg-6 col-md-6">
                    <label><br/>Date fin année <span class="requis">*</span></label>
                    <div class="input-group date years" data-date="" data-date-format="yyyy" data-link-field="anneeFin"
                         data-link-format="yyyy">
                        <input class="form-control" size="16" type="number" value="${anneeFin}" readonly>
                        <span class="input-group-addon">
										<span class="glyphicon glyphicon-remove"></span></span>
                        <span class="input-group-addon">
										<span class="glyphicon glyphicon-calendar"></span></span>
                    </div>
                    <c:if test="${!empty erreurs}">
										<span class="erreur">
                                                ${erreurs['anneeFin']}
                                        </span>
                    </c:if>
                    <input type="hidden" id="anneeFin" name="anneeFin" value="${anneeFin}"/><br/>
                </div>

                <div class="col-lg-6 col-md-6">
                    <label><br/>Date début projet <span class="requis">*</span></label>
                    <div class="input-group date form_date" data-date="" data-date-format="dd MM yyyy"
                         data-link-field="dateDebutProjet" data-link-format="yyyy-mm-dd">
                        <input class="form-control" size="16" type=text value="${dateDebutProjet}" readonly>
                        <span class="input-group-addon">
										<span class="glyphicon glyphicon-remove"></span></span>
                        <span class="input-group-addon">
										<span class="glyphicon glyphicon-calendar"></span></span>
                    </div>
                    <c:if test="${!empty erreurs}">
										<span class="erreur">
                                                ${erreurs['dateDebutProjet']}
                                        </span>
                    </c:if>
                    <input type="hidden" id="dateDebutProjet" name="dateDebutProjet" value="${dateDebutProjet}"/><br/>
                </div>

                <div class="col-lg-6 col-md-6">
                    <label><br/>Date mi avancement <span class="requis">*</span></label>
                    <div class="input-group date form_date" data-date="" data-date-format="dd MM yyyy"
                         data-link-field="dateMiAvancement" data-link-format="yyyy-mm-dd">
                        <input class="form-control" size="16" type="text" value="${dateMiAvancement}" readonly>
                        <span class="input-group-addon">
										<span class="glyphicon glyphicon-remove"></span></span>
                        <span class="input-group-addon">
										<span class="glyphicon glyphicon-calendar"></span></span>
                    </div>
                    <c:if test="${!empty erreurs}">
										<span class="erreur">
                                                ${erreurs['dateMiAvancement']}
                                        </span>
                    </c:if>
                    <input type="hidden" id="dateMiAvancement" name="dateMiAvancement"
                           value="${dateMiAvancement}"/><br/>
                </div>


                <div class="col-lg-6 col-md-6">
                    <label><br/>Date dépôt poster <span class="requis">*</span></label>
                    <div class="input-group date form_date" data-date="" data-date-format="dd MM yyyy"
                         data-link-field="dateDepotPoster" data-link-format="yyyy-mm-dd">
                        <input class="form-control" size="16" type="text" value="${dateDepotPoster}" readonly>
                        <span class="input-group-addon">
										<span class="glyphicon glyphicon-remove"></span></span>
                        <span class="input-group-addon">
										<span class="glyphicon glyphicon-calendar"></span></span>
                    </div>
                    <c:if test="${!empty erreurs}">
										<span class="erreur">
                                                ${erreurs['dateDepotPoster']}
                                        </span>
                    </c:if>
                    <input type="hidden" id="dateDepotPoster" name="dateDepotPoster" value="${dateDepotPoster}"/><br/>
                </div>

                <div class="col-lg-6 col-md-6">
                    <label><br/>Date soutenance finale <span class="requis">*</span></label>
                    <div class="input-group date form_date" data-date="" data-date-format="dd MM yyyy"
                         data-link-field="dateSoutenanceFinale" data-link-format="yyyy-mm-dd">
                        <input class="form-control" size="16" type="text" value="${dateSoutenanceFinale}" readonly>
                        <span class="input-group-addon">
										<span class="glyphicon glyphicon-remove"></span></span>
                        <span class="input-group-addon">
										<span class="glyphicon glyphicon-calendar"></span></span>
                    </div>
                    <c:if test="${!empty erreurs}">
										<span class="erreur">
                                                ${erreurs['dateSoutenanceFinale']}
                                        </span>
                    </c:if>
                    <input type="hidden" id="dateSoutenanceFinale" name="dateSoutenanceFinale"
                           value="${dateSoutenanceFinale}"/><br/>
                </div>

                <div class="col-lg-12 col-md-12">
                    <label><br/>Options concernées : <span class="requis">*</span></label>
                    <div class="checkbox col-lg-offset-1 col-md-offset-1 col-sm-offset-1">
                        <input type="checkbox" name="CC" id="CC" value="CC"/>
                        <label for="CC"><strong>Cloud Computing</strong></label>
                    </div>
                    <div class="checkbox col-lg-offset-1 col-md-offset-1 col-sm-offset-1">
                        <input type="checkbox" name="IIT" id="IIT" value="IIT"/>
                        <label for="IIT"> <strong>Infrastructures IT</strong></label>
                    </div>
                    <div class="checkbox col-lg-offset-1 col-md-offset-1 col-sm-offset-1">
                        <input type="checkbox" name="LD" id="LD" value="LD"/>
                        <label for="LD"><strong>Logiciels et Données</strong></label>
                    </div>
                    <div class="checkbox col-lg-offset-1 col-md-offset-1 col-sm-offset-1">
                        <input type="checkbox" name="SE" id="SE" value="SE"/>
                        <label for="SE"><strong>Systèmes Embarqués</strong></label>
                    </div>
                    <div class="checkbox col-lg-offset-1 col-md-offset-1 col-sm-offset-1">
                        <input type="checkbox" name="BIO" id="BIO" value="BIO"/>
                        <label for="BIO"><strong>Biomédical</strong></label>
                    </div>
                    <div class="checkbox col-lg-offset-1 col-md-offset-1 col-sm-offset-1">
                        <input type="checkbox" name="OC" id="OC" value="OC"/>
                        <label for="OC"><strong>Objets Connectés</strong></label>
                    </div>
                    <div class="checkbox col-lg-offset-1 col-md-offset-1 col-sm-offset-1">
                        <input type="checkbox" name="DSMT" id="DSMT" value="DSMT"/>
                        <label for="DSMT"><strong>Data Sciences, Multimedia & Telecom</strong></label>
                    </div>
                    <div class="checkbox col-lg-offset-1 col-md-offset-1 col-sm-offset-1">
                        <input type="checkbox" name="NRJ" id="NRJ" value="NRJ"/>
                        <label for="NRJ"><strong>Énergie et Environnement</strong></label>
                    </div>
                    <div class="checkbox col-lg-offset-1 col-md-offset-1 col-sm-offset-1">
                        <input type="checkbox" name="BD" id="BD" value="BD"/>
                        <label for="BD"><strong>Big Data</strong></label>
                    </div>
                    <c:if test="${!empty erreurs}">
										<span class="erreur">
											${erreurs['options']}
											<br/>
										</span>
                    </c:if>
                </div>

                <div class="col-lg-12 col-md-12">
                    <div class="text-center">
                        <button type="submit" class="btn btn-primary" id="submitSujet">
                            <i class="fas fa-check-circle fa-fw" aria-hidden="true"></i>&nbsp; Soumettre l'année
                        </button>
                        <input type="hidden" name="formulaire" id="formulaire" value="ajouterAnneeScolaire"/>

                        <button type="reset" class="btn btn-primary" id="reset">
                            <i class="fas fa-redo fa-fw" aria-hidden="true"></i>&nbsp; Remettre à zéro
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

