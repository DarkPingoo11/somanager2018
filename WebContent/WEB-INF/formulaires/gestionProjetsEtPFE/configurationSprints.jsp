<%--
  Created by IntelliJ IDEA.
  User: dimitrijarneau
  Date: 31/05/2018
  Time: 15:10
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<link rel="stylesheet" href="<c:url value="/css/gererNote.css"/>">

<c:if test="${profResponsable eq 'true' }">
<div class="col-lg-12 col-md-6">
	<div class="panel panel-default ">
		<div class="panel-heading" align="center">
			<i class="far fa-plus-square"></i>&nbsp; Configuration du PGL :
		</div>
		<div class="panel-body">
			<form method="post" action="<c:url value="/GestionProjetsEtPFE"/>">
				<%-- Premiere colonne --%>
				<div class="col-sm-6">
					<div class="form-group has-feedback">
						<label>Choix de l'année à configurer : <span
							class="requis">*</span></label> <br /> <select class="form-control"
							id="selectAnnee" name="selectAnnee" onchange="location.reload()">
							<c:forEach items="${anneesPGLPlanifiees}" var="element">
								<option value="${element}">Option ${optionLD} :
									${element.anneeDebut} / ${element.anneeFin}</option>
							</c:forEach>
						</select>
					</div>
				</div>
				<%-- Deuxieme colonne --%>
				<div class="col-sm-6">
					<div class="form-group has-feedback">
						<label for="nombreSprint">Nombre de sprints pour cette
							année : <span class="requis">*</span>
						</label> <br /> <select class="form-control" id="nombreSprint"
							name="nombreSprint">
							<option>1</option>
							<option>2</option>
							<option>3</option>
							<option>4</option>
							<option>5</option>
							<option>6</option>
							<option>7</option>
							<option>8</option>
						</select>
					</div>
				</div>
				<input type="hidden" name="formulaire" id="formulaire"
					value="selectNbrSprint" /> <br />
				<button type="submit" class="btn btn-success center-block"
					id="submit">
					<i class="fas fa-plus-circle"></i> Créer sprints
				</button>
				<br />
			</form>
		</div>
		<div class="panel-footer">
			<i class="fas fa-info-circle fa-fw" aria-hidden="true"></i>&nbsp; <strong>Informations
				:</strong><br /> Lorsque vous cliquerez sur "créer sprints": <br /> - les
			sprints précédemment configurés seront supprimés. <br /> - les
			sprints sans matière seront supprimés. <br /> - les matières sans
			notes reliées au sprints seront supprimées <br /> <br /> Les
			sprints reliés à des matières contenant des notes ne seront pas
			supprimés. <br />
		</div>
	</div>
</div>
</c:if>

<div id="sprintsConfiguration" class="col-lg-12 col-md-6">
	<div class="panel panel-default ">
		<div class="panel-heading" align="center">
			<i class="far fa-sticky-note"></i>&nbsp; Configuration des sprints de l'année en cours :
		</div>
		<!-- Editable table -->
		<div class="card">
			<div class="panel-body">
				<c:if test="${not empty listeSprints }">
					<div class="interfaceSprint">
						<div class="col-lg-4 col-sm-4">
							<div class="divMenu">
							<input type="hidden" value="${refAnneeLdEnCours }"/>
								<ul class="menuSprint">
									<c:forEach var="sprint" items="${listeSprints }">
									<c:if test="${sprint.refAnnee == refAnneeLdEnCours }">
										<c:set var="nbrSprint" scope="session" value="${sprint.numero }"></c:set>
											<li>
												<button type="button" onclick="afficher_cacher('detailSprint${sprint.idSprint}')">
													Sprint <c:out value="${sprint.numero }"></c:out>
												</button>
											</li>
									</c:if>
									</c:forEach>
								</ul>
							</div>
						</div>
						<div class="col-lg-8 col-sm-8">
							<c:forEach var="sprint" items="${listeSprints }">
							<c:if test="${sprint.refAnnee == refAnneeLdEnCours }">
							<input type="hidden" id="tailleListeSprint" value="${sprint.numero }">
									<div id="detailSprint${sprint.idSprint }" class="nomSprint" hidden="true" style="display:none">
										<div class=detailSprint>
											<h3 class="col-xs-7">Sprint ${sprint.numero }</h3>
											<h5 class="col-xs-2"><em>Heures :</em> ${sprint.nbrHeures }</h5>
											<h5 class="col-xs-3"><em>Coefficient :</em> ${sprint.coefficient }</h5>
											<br/>
											<br/>
										</div>
										<div class="detailMatiere">
										<form method="post" onsubmit="return checkForm(this)" action="<c:url value="/GestionProjetsEtPFE"/>">
											<input type="hidden" id="idSprint" name="idSprint" value="${sprint.idSprint }"/>
											<input type="hidden" name="formulaire" id="formulaire"
												value="modifierCoefficientIntra" />
											<div id="matierePart1">
												<ul>
													<c:forEach var="matiere" items="${listeMatieres }">
														<c:if test="${sprint.idSprint == matiere.refSprint }">
															<li style="border-bottom:1px solid #337ab7">
															<input type="hidden" id="matiereLibelle${matiere.idMatiere }" name="matiereLibelle${matiere.idMatiere }" value="${matiere.libelle }"/>
															<input type="hidden" id="matiereIdentifiant${matiere.idMatiere }" name="matiereIdentifiant${matiere.idMatiere }" value="${matiere.idMatiere }"/>
																<div>
																	<h5 class="col-xs-9" id="matiereLibelle"><strong>${matiere.libelle }</strong></h5>
																	<h6 class="col-xs-3"><em>Coefficient :</em> 
																		<input type="text" placeholder="${matiere.coefficient }" class="coeffMatiere" id="matiereCoefficient${matiere.idMatiere }" name="matiereCoefficient${matiere.idMatiere }"/>
																	</h6>
																	<br/>
																</div>
															</li>
														</c:if>
													</c:forEach>													
												</ul>
											</div>
											<div id="matierePart2">
											<input type="hidden" id="presentOuNon${sprint.numero }" name="presentOuNon" value="absent"/>
											<ul id="listeMatiere${sprint.numero }"></ul>
												<c:if test="${profResponsable eq 'true' || profReferent eq 'true'}">
												<div>
													<div style="padding-left:55px">
														<button type="button" class="btn btn-primary" onclick="creer_matiere(${sprint.numero})" id="buttonListeMatiere" name="buttonListeMatiere" value="${ sprint.numero}"><i class="fas fa-plus-square"></i></button>
													</div>
												</div>
												</c:if>
												
												<div>
													<div class="box" style="text-align:center">
													<c:if test="${profResponsable eq 'true' || profReferent eq 'true'}">
														<button type="button" class="btn btn-primary"
															data-toggle="modal" data-target="#exampleModal"
															data-debut="${sprint.dateDebut}"
															data-whatever="${sprint.idSprint}"
															data-fin="${sprint.dateFin}" data-heures="${sprint.nbrHeures}"
															data-coefficient="${sprint.coefficient}"
															data-sprintselect="${sprint.idSprint}"
															data-annee="${sprint.refAnnee}" data-sprint="${sprint.numero}">
															<i class="fas fa-pencil-alt"></i> Modifier le sprint
														</button>
													</c:if>
														
														<button type="submit" class="btn btn-primary">
															<i class="fas fa-pencil-alt"></i> Valider informations
														</button>
													</div>
												</div>
											</div>
											
											</form>
											
										</div>
									</div>
									</c:if>
							</c:forEach>
						</div>
					</div>
			</div>	
			

				<div class="modal fade" id="exampleModal" tabindex="-1"
					role="dialog" aria-labelledby="exampleModalLabel"
					aria-hidden="true">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-header  modal-header">
								<h2 class="modal-title" id="exampleModalLabel">New message</h2>
								<button type="button" class="close" data-dismiss="modal"
									aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<div class="modal-body">
								<form data-toggle="validator" role="form" method="post"
									action="<c:url value="/GestionProjetsEtPFE"/>">
									<div class="form-group">
										<input type="hidden" class="form-control" id="sprint"
											name="sprint"> <input type="hidden"
											class="form-control" id="idSprintSelect"
											name="idSprintSelect"> <label>Date de début
											de sprint : <span class="requis">*</span>
										</label>
										<div class="input-group date form_date" data-date=""
											data-date-format="dd MM yyyy" data-link-field="idDateDebut"
											data-link-format="yyyy-mm-dd">
											<input class="form-control" size="16" type="date"
												name="idDateDebut" id="idDateDebut" value="" required>
											<span class="glyphicon form-control-feedback"
												aria-hidden="true"></span>
											<div class="help-block with-errors"></div>
											<span class="input-group-addon"> <span
												class="glyphicon glyphicon-remove"></span></span>
												<span class="input-group-addon"></span> <i
												class="far fa-calendar-alt"></i>
										</div>
										<input type="hidden" id="idDateDebut" name="idDateDebut"
											value="" /><br />
										<c:if test="${!empty erreurs}">
											<span class="erreur"> ${erreurs['dateDebutErreur']} </span>
										</c:if>
									</div>

									<div class="form-group">

										<label>Date de fin de sprint : <span class="requis">*</span></label>
										<div class="input-group date form_date" data-date=""
											data-date-format="dd MM yyyy" data-link-field="idDateFin"
											data-link-format="yyyy-mm-dd">
											<input class="form-control" size="16" name="idDateFin"
												id="idDateFin" type="text" value="" required> <span
												class="glyphicon form-control-feedback" aria-hidden="true"></span>
											<div class="help-block with-errors"></div>
											<span class="input-group-addon"> <span
												class="glyphicon glyphicon-remove"></span></span>
												<span class="input-group-addon"><i
												class="far fa-calendar-alt"></i></span>
										</div>
										<input type="hidden" id="idDateFin" name="idDateFin" value="" /><br />

										<c:if test="${!empty erreurs}">
											<span class="erreur"> ${erreurs['dateFinErreur']} </span>
										</c:if>
									</div>

									<div class="form-group">
										<label for="idNombreHeures" class="col-form-label">Nombre
											d'heures :</label> <input type="text" class="form-control"
											id="idNombreHeures" name="idNombreHeures" maxlength="4">
										<c:if test="${!empty erreurs}">
											<span class="erreur"> ${erreurs['nbrHErreur']} </span>
										</c:if>
									</div>

									<div class="form-group">
										<label for="idCoefficient" class="col-form-label">Coefficient
											du Sprint:</label> <input type="text" class="form-control"
											id="idCoefficient" name="idCoefficient" maxlength="10">
										<c:if test="${!empty erreurs}">
											<span class="erreur"> ${erreurs['coeffErreur']} </span>
										</c:if>
									</div>

									<input type="hidden" class="form-control" id="refIdAnnee"
										name="refIdAnnee">
							</div>
							<div class="modal-footer">
								<div class="btn-group btn-group-justified" role="group"
									aria-label="group button">
									<div class="btn-group" role="group">
										<button type="button" class="btn btn-danger"
											data-dismiss="modal" role="button">&nbsp; Fermer</button>
									</div>
									<div class="btn-group" role="group">
										<button type="submit" id="submit"
											class="btn btn-primary btn-hover-green" data-action="save"
											role="button">&nbsp; Enregistrer</button>
										<input type="hidden" name="formulaire" id="formulaire"
											value="modifierSprint" />

									</div>
								</div>
							</div>
							</form>
						</div>
					</div>
				</div>
			</c:if>
			<c:if test="${empty listeSprints}">
				<p align="center">Aucune planification n'a été effectuée</p>
			</c:if>
		</div>
	</div>
</div>
<!-- Editable table -->

</div>
</div>

<script type="text/javascript">
	var compteurCoefficient = 1;
	var compteurLibelle = 1;
	var compteurMatiere = 0;

	function creer_matiere(numero) {
		document.getElementById("presentOuNon"+numero).value = "present";
		if(compteurMatiere >=10){
			alert("Vous ne pouvez pas créer plus de 10 matières.")
		}
		else{
			compteurMatiere ++;
			var libelleListeMatiere="listeMatiere";
			var newLi = document.createElement("li");
			
			var newInputLibelle = document.createElement("input");
			newInputLibelle.id=("libelle" + compteurLibelle).toString();
			newInputLibelle.type="text";
			newInputLibelle.name=("libelle" + compteurLibelle).toString();
			var labelLibelle = document.createElement("label");
			var textLibelle = document.createTextNode("Libelle : ");
			labelLibelle.appendChild(textLibelle);
			labelLibelle.setAttribute("style", "padding-left:17px");
			
			var newInputCoefficient = document.createElement("input");
			newInputCoefficient.id=("coefficient" + compteurCoefficient).toString();
			newInputCoefficient.type="text";
			newInputCoefficient.name=("coefficient" + compteurCoefficient).toString();
			newInputCoefficient.setAttribute("class", "coeffMatiere");
			newInputCoefficient.setAttribute("style", "width:30px");

			if(!document.getElementById("inputCompteur")){
				var inputCompteur = document.createElement("input");
				inputCompteur.id="inputCompteur";
				inputCompteur.type="hidden";
				inputCompteur.name="inputCompteur";
				inputCompteur.value=compteurMatiere;
				document.getElementById("listeMatiere"+numero).appendChild(inputCompteur);
			}else{
				document.getElementById("inputCompteur").value=compteurMatiere;
			}
			
			var labelCoefficient = document.createElement("label");
			var textCoefficient = document.createTextNode("Coefficient : ");
			labelCoefficient.appendChild(textCoefficient);
			labelCoefficient.setAttribute("style", "padding-left:310px");
			labelCoefficient.setAttribute("class", "labelCoef");
			
			newLi.setAttribute("style", "border-bottom:1px solid #337ab7");
			
			newLi.appendChild(labelLibelle);
			newLi.appendChild(newInputLibelle);
			newLi.appendChild(labelCoefficient);
			newLi.appendChild(newInputCoefficient);
			
			document.getElementById("listeMatiere"+numero).appendChild(newLi);
			compteurCoefficient ++;
			compteurLibelle ++;
			
		}		
	}
	
</script>


<script type="text/javascript">
	function afficher_cacher(id) {
		var elements = document.getElementsByClassName("nomSprint");
		var x = document.getElementById(id);
		for(var i=0; i<elements.length; i++){
			if(!x.isSameNode(elements[i])){
				elements[i].style.display="none";
		}
		}
		if(x.style.display=="none"){
			x.style.display="block";
		}else{
			x.style.display="none";
		}
	}
</script>




