<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <c:import url="/inc/head.jsp"/>
    <title>SoManager - Ajouter Sujet</title>
</head>
<body>
<div class="wrapper">
    <c:import url="/Navbar"/>
    <!-- Page Content Holder -->
    <div id="content">
        <c:import url="/inc/headerForSidebar.jsp"/>


        <div class="panel panel-primary ">
            <div class="panel-heading">Sujet - Ajouter des sujets</div>

            <div class="panel-body">

                <c:import url="/inc/callback.jsp"/>

                <ul class="nav nav-tabs" role="tablist" style="margin-bottom: 20px">
                    <li class="active"><a data-toggle="tab" href="#depuisSite">
                        <i class="fas fa-plus fa-fw" aria-hidden="true"></i>&nbsp;
                        Ajouter localement
                    </a></li>
                    <li><a data-toggle="tab" href="#depuisForm">
                        <i class="fas fa-cloud fa-fw" aria-hidden="true"></i>&nbsp;
                        Ajouter en ligne
                    </a></li>
                </ul>

                <%-- Contenu des différentes tab --%>
                <div class="tab-content">

                    <%-- Modifier depuis le site --%>
                    <div id="depuisSite" class="tab-pane fade in active">
                        <div class="row">
                            <c:import url="/WEB-INF/formulaires/ajouterSujet/depuisSite.jsp"/>
                        </div>
                    </div>

                    <%-- Modifier depuis le formulaire --%>
                    <div id="depuisForm" class="tab-pane fade">
                        <div class="row">
                            <c:import url="/WEB-INF/formulaires/ajouterSujet/depuisForm.jsp"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<c:import url="/inc/scripts.jsp"/>
<script src="<c:url value="/js/dragAndDrop.js"/>"></script>
<script type="application/javascript" src="<c:url value="/js/perfect-scrollbar.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/js/bootstrap-datetimepicker.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/js/bootstrap-datetimepicker.fr.js"/>"></script>
</body>
</html>

