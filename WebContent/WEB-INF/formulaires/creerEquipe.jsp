<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <c:import url="/inc/head.jsp"/>
    <title>SoManager - Création équipes</title>
</head>
<body>
<div class="wrapper">
    <c:import url="/Navbar"/>
    <!-- Page Content Holder -->
    <div id="content">
        <c:import url="/inc/headerForSidebar.jsp"/>
        <div class="panel panel-primary ">

            <div class="panel-heading">Equipe - Création des équipes</div>
            <div class="panel-body">


                <div class="col-lg-4">
                    <div class="panel panel-default">
                        <div class="panel-heading">Élèves disponibles</div>
                        <div class="panel-body">

                            <div class="col-lg-12">
                                <c:import url="/inc/listeElevesI3.jsp"/>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="col-lg-4">
                    <div class="panel panel-default" id="compoEquipe">
                        <div class="panel-heading">Composition de l'équipe</div>
                        <div class="panel-body">
                            <form action="<c:url value="/CreerEquipe"/>" method="post">
                                <label for="sujet">Sujet choisi : </label>
                                <div id="sujetChoisi" type="text" style="overflow: auto;" class="form-control"
                                     ondrop="dropSujet(event)" draggable="false" ondragover="allowDrop(event)"
                                     ondragleave="refuseDrop(event)"></div>

                                <p><strong><br>Membre(s) de l'équipe : </strong></p>

                                <div id="dynamicInput">
                                    <c:if test="${etudiant eq 'true'}">
                                        Membre 1 <br/>

                                        <div id="membre1" type="text" disabled="disabled" class="form-control">
                                            <c:out value="${utilisateur.nom} ${utilisateur.prenom}"/>
                                            <input name="idMembre1" id="idMembre1" type="hidden"
                                                   value="<c:out value="${utilisateur.idUtilisateur}"/>"/>
                                        </div>
                                    </c:if>
                                </div>

                                <br/>
                                <div class="form-control" ondrop="ajouterEtudiant(event,'dynamicInput')"
                                     draggable="false"
                                     ondragover="allowDrop(event)" ondragleave="refuseDrop(event)">
                                    Ajouter un membre <br/>
                                </div>
                                <br>
                                <input class="btn btn-danger center-block" type="button" value="Supprimer un membre"
                                       onClick="supprimerMembre();"> <br/>
                                <br/>

                                <div class="text-center">
                                    <c:choose>
                                        <c:when test="${empty erreurs}">
                                            <span class="succes">${resultat}</span>
                                        </c:when>
                                        <c:otherwise>
                                            <span class="erreur">${resultat} <br> ${erreurs['nbrMinEleves']} ${erreurs['nbrMaxEleves']} ${erreurs['contratPro']} ${erreurs['options']} ${erreurs['membre']} ${erreurs['sujet']}</span>
                                        </c:otherwise>
                                    </c:choose>
                                </div>
                                <button class="btn btn-primary center-block" id="submitEquipe">
                                    <i class="fas fa-plus fa-fw" aria-hidden="true"></i>&nbsp; Créer l'équipe pour ce
                                    sujet
                                </button>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4">
                    <div class="panel panel-default">
                        <div class="panel-heading">Sujets disponibles</div>
                        <div class="panel-body">
                            <div class="col-lg-12">
                                <c:import url="/inc/listeSujetsValides.jsp"/>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<c:import url="/inc/scripts.jsp"/>

<script src="<c:url value="/js/dragAndDrop.js"/>" type="text/javascript"></script>
<script type="text/javascript">
    if (document.getElementById("membre1") === null) {
        var counter = 0;
    } else {
        var counter = 1;
    }
    var dragNumber = -1;

    function ajouterEtudiant(ev, divName) {

        ajouterMembre(divName);
        dropEtudiant(ev, counter);
    }


    function ajouterMembre(divName) {
        if (counter === 0) {
            divDrop = document.getElementById("dynamicInput");
            divDrop.innerHTML = "Membre "
                + (counter + 1)
                + " <br /><div id='membre"
                + (counter + 1)
                + "' type='text'  class='form-control' ondrop='dropEtudiant(event,"
                + (counter + 1) + ")'ondragover='allowDropEtudiant(event,"
                + (counter + 1)
                + ")' ondragleave='refuseDropEtudiant(event,"
                + (counter + 1) + ")' draggable='false'> </br></div>";
        } else {
            var newdiv = document.createElement('div');
            newdiv.innerHTML = "Membre "
                + (counter + 1)
                + " <br/><div id='membre"
                + (counter + 1)
                + "' type='text'  class='form-control' ondrop='dropEtudiant(event,"
                + (counter + 1) + ")'ondragover='allowDropEtudiant(event,"
                + (counter + 1)
                + ")' ondragleave='refuseDropEtudiant(event,"
                + (counter + 1) + ")' draggable='false'> </br></div>";
            document.getElementById(divName).appendChild(newdiv);
        }
        counter++;
    }

    function supprimerMembre() {
        var list = document.getElementById("dynamicInput");
        if (counter != 1) {
            list.removeChild(list.childNodes[list.childNodes.length - 1]);
            document.getElementById("draggableEtudiant-" + dragNumber).style.display = "";
            counter--;
            dragNumber--;
        }

    }

    function dragEtudiant(ev, id) {
        // numero de la ligne de tableau où est contenu l'étudiant
        dragNumber = id;
    }

    /**   */
    function dropEtudiant(ev, id) {
        //var id = (id == undefined) ? ev.dataTransfer.getData("id") : id;

        // zone dans laquelle on souhaite inclure l'étudiant
        // on la sélectionne par son id
        divDrop = document.getElementById("membre" + id);

        var text = divDrop.innerHTML;

        //On vérifie si un étudiant n'est pas déjà présent dans la case membre
        // si c'est le cas il sera remis dans le tableau
        if (text.length > 5) {
            //récuparation de l'id de la ligne de l'étudiant à remettre dans le tableau
            var id = text.substring(text.indexOf("*blou*") + 6, text
                .indexOf("*/blou*"));
            //ré-affichage de l'étudiant dans le tableau
            document.getElementById("draggableEtudiant-" + id).style.display = "";
        }

        //on cache l'étudiant du tableau qu'on vient de dropper dans la case membre
        document.getElementById("draggableEtudiant-" + dragNumber).style.display = "none";

        //on met les informations nécessaires dans la case membre
        divDrop.innerHTML = document.getElementById("nom-" + dragNumber).innerHTML
            + " "
            + document.getElementById("prenom-" + dragNumber).innerHTML
            + "<input name='idMembre" + id + "' id='idMembre"
            + id
            + "' type ='hidden' value='"
            + document.getElementById("idMembre-" + dragNumber).innerHTML
            + "'/>";
    }

    /** Change la couleur de la bordure lors du hover du div drop */
    function allowDropEtudiant(ev, id) {
        ev.preventDefault();
    }

    /** Change la couleur de la bordure lorsque l'on quitte le div drop */
    function refuseDropEtudiant(ev, id) {
        ev.preventDefault();
    }

    function dropSujet(ev, id) {
        var id = (id == undefined) ? ev.dataTransfer.getData("id") : id;

        divDrag = document.getElementById("draggable-" + id);

        var divDrop = document.getElementById("sujetChoisi");

        divDrop.innerHTML = document.getElementById("titre-" + id).innerHTML
            + "<input name='idSujet' id='idSujet' type ='hidden' value='"
            + document.getElementById("idSujet-" + id).innerHTML
            + "'/>";

    }

    function changerTailleScroll() {
        document.getElementById("tableAttribution").style.height = document.getElementById('compoEquipe').clientHeight + 100 + "px";
    }

    window.onload = changerTailleScroll;

</script>

</body>
</html>