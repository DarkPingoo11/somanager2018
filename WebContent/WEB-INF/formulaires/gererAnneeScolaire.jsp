<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="ISO-8859-1" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <c:import url="/inc/head.jsp"/>
    <title>SoManager - Planifier l'ann�e</title>
    <link rel="stylesheet" media="screen"
          href="<c:url value="/css/bootstrap-datetimepicker.min.css"/>">
</head>
<body>
<div class="wrapper">
    <c:import url="/Navbar"/>
    <!-- Page Content Holder -->
    <div id="content">
        <c:import url="/inc/headerForSidebar.jsp"/>

        <div class="panel panel-primary ">
            <div class="panel-heading">Jurys - Gestion de l'ann�e scolaire</div>
            <div class="panel-body">
                <div class="col-lg-6 col-md-6">
                    <div class="panel panel-default ">
                        <div class="panel-heading" align="center">Ann�es planifi�es :</div>
                        <div class="panel-body">
                            <c:if test="${!empty anneesPlanifiees }">
                                <table class="table table-condensed">
                                    <thead>
                                    <tr>
                                        <th>Option</th>
                                        <th>Date d�but projet</th>
                                        <th>Date mi-avancement</th>
                                        <th>Date depot poster</th>
                                        <th>Date soutenance finale</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <c:forEach var="annee" items="${anneesPlanifiees}" varStatus="loop">
                                        <tr>
                                            <td><c:out value="${optionAnnees[loop.index]}"/></td>
                                            <td><fmt:formatDate type="date" value="${annee.dateDebutProjet}"/></td>
                                            <td><fmt:formatDate type="date" value="${annee.dateMiAvancement}"/></td>
                                            <td><fmt:formatDate type="date" value="${annee.dateDepotPoster}"/></td>
                                            <td><fmt:formatDate type="date" value="${annee.dateSoutenanceFinale}"/></td>
                                        </tr>
                                    </c:forEach>
                                    </tbody>
                                </table>
                            </c:if>
                            <c:if test="${empty anneesPlanifiees }">
                                <p align="center">Aucune planification n'a �t� effectu�e</p>
                            </c:if>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6">
                    <div class="panel panel-default ">
                        <div class="panel-heading" align="center">Plannifier l'ann�e :</div>
                        <div class="panel-body">
                            <form data-toggle="validator" role="form" method="post"
                                  action="<c:url value="/GererAnneeScolaire"/>">
                                <div class="col-lg-6 col-md-6">
                                    <label><br/>Date d�but ann�e <span class="requis">*</span></label>
                                    <div class="input-group date years" data-date="" data-date-format="yyyy"
                                         data-link-field="anneeDebut" data-link-format="yyyy">
                                        <input class="form-control" size="16" type="number"
                                               value="<c:out value="${anneeDebut}"/>" readonly>
                                        <span class="input-group-addon">
										<span class="glyphicon glyphicon-remove"></span></span>
                                        <span class="input-group-addon">
										<span class="glyphicon glyphicon-calendar"></span></span>
                                        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                    <c:if test="${!empty erreurs}">
										<span class="erreur">
                                                ${erreurs['anneeDebut']}
                                        </span>
                                    </c:if>
                                    <input type="hidden" id="anneeDebut" name="anneeDebut" value="${anneeDebut}"/><br/>
                                </div>

                                <div class="col-lg-6 col-md-6">
                                    <label><br/>Date fin ann�e <span class="requis">*</span></label>
                                    <div class="input-group date years" data-date="" data-date-format="yyyy"
                                         data-link-field="anneeFin" data-link-format="yyyy">
                                        <input class="form-control" size="16" type="number" value="${anneeFin}"
                                               readonly>
                                        <span class="input-group-addon">
										<span class="glyphicon glyphicon-remove"></span></span>
                                        <span class="input-group-addon">
										<span class="glyphicon glyphicon-calendar"></span></span>
                                    </div>
                                    <c:if test="${!empty erreurs}">
										<span class="erreur">
                                                ${erreurs['anneeFin']}
                                        </span>
                                    </c:if>
                                    <input type="hidden" id="anneeFin" name="anneeFin" value="${anneeFin}"/><br/>
                                </div>

                                <div class="col-lg-6 col-md-6">
                                    <label><br/>Date d�but projet <span class="requis">*</span></label>
                                    <div class="input-group date form_date" data-date="" data-date-format="dd MM yyyy"
                                         data-link-field="dateDebutProjet" data-link-format="yyyy-mm-dd">
                                        <input class="form-control" size="16" type=text value="${dateDebutProjet}"
                                               readonly>
                                        <span class="input-group-addon">
										<span class="glyphicon glyphicon-remove"></span></span>
                                        <span class="input-group-addon">
										<span class="glyphicon glyphicon-calendar"></span></span>
                                    </div>
                                    <c:if test="${!empty erreurs}">
										<span class="erreur">
                                                ${erreurs['dateDebutProjet']}
                                        </span>
                                    </c:if>
                                    <input type="hidden" id="dateDebutProjet" name="dateDebutProjet"
                                           value="${dateDebutProjet}"/><br/>
                                </div>

                                <div class="col-lg-6 col-md-6">
                                    <label><br/>Date mi avancement <span class="requis">*</span></label>
                                    <div class="input-group date form_date" data-date="" data-date-format="dd MM yyyy"
                                         data-link-field="dateMiAvancement" data-link-format="yyyy-mm-dd">
                                        <input class="form-control" size="16" type="text" value="${dateMiAvancement}"
                                               readonly>
                                        <span class="input-group-addon">
										<span class="glyphicon glyphicon-remove"></span></span>
                                        <span class="input-group-addon">
										<span class="glyphicon glyphicon-calendar"></span></span>
                                    </div>
                                    <c:if test="${!empty erreurs}">
										<span class="erreur">
                                                ${erreurs['dateMiAvancement']}
                                        </span>
                                    </c:if>
                                    <input type="hidden" id="dateMiAvancement" name="dateMiAvancement"
                                           value="${dateMiAvancement}"/><br/>
                                </div>


                                <div class="col-lg-6 col-md-6">
                                    <label><br/>Date d�p�t poster <span class="requis">*</span></label>
                                    <div class="input-group date form_date" data-date="" data-date-format="dd MM yyyy"
                                         data-link-field="dateDepotPoster" data-link-format="yyyy-mm-dd">
                                        <input class="form-control" size="16" type="text" value="${dateDepotPoster}"
                                               readonly>
                                        <span class="input-group-addon">
										<span class="glyphicon glyphicon-remove"></span></span>
                                        <span class="input-group-addon">
										<span class="glyphicon glyphicon-calendar"></span></span>
                                    </div>
                                    <c:if test="${!empty erreurs}">
										<span class="erreur">
                                                ${erreurs['dateDepotPoster']}
                                        </span>
                                    </c:if>
                                    <input type="hidden" id="dateDepotPoster" name="dateDepotPoster"
                                           value="${dateDepotPoster}"/><br/>
                                </div>

                                <div class="col-lg-6 col-md-6">
                                    <label><br/>Date soutenance finale <span class="requis">*</span></label>
                                    <div class="input-group date form_date" data-date="" data-date-format="dd MM yyyy"
                                         data-link-field="dateSoutenanceFinale" data-link-format="yyyy-mm-dd">
                                        <input class="form-control" size="16" type="text"
                                               value="${dateSoutenanceFinale}" readonly>
                                        <span class="input-group-addon">
										<span class="glyphicon glyphicon-remove"></span></span>
                                        <span class="input-group-addon">
										<span class="glyphicon glyphicon-calendar"></span></span>
                                    </div>
                                    <c:if test="${!empty erreurs}">
										<span class="erreur">
                                                ${erreurs['dateSoutenanceFinale']}
                                        </span>
                                    </c:if>
                                    <input type="hidden" id="dateSoutenanceFinale" name="dateSoutenanceFinale"
                                           value="${dateSoutenanceFinale}"/><br/>
                                </div>

                                <div class="col-lg-12 col-md-12">
                                    <label><br/>Options concern�es : <span class="requis">*</span></label>
                                    <div class="checkbox col-lg-offset-1 col-md-offset-1 col-sm-offset-1">
                                        <input type="checkbox" name="CC" id="CC" value="CC"/>
                                        <label for="CC"><strong>Cloud Computing</strong></label>
                                    </div>
                                    <div class="checkbox col-lg-offset-1 col-md-offset-1 col-sm-offset-1">
                                        <input type="checkbox" name="IIT" id="IIT" value="IIT"/>
                                        <label for="IT"> <strong>Infrastructures IT</strong></label>
                                    </div>
                                    <div class="checkbox col-lg-offset-1 col-md-offset-1 col-sm-offset-1">
                                        <input type="checkbox" name="LD" id="LD" value="LD"/>
                                        <label for="LD"><strong>Logiciels et Donn�es</strong></label>
                                    </div>
                                    <div class="checkbox col-lg-offset-1 col-md-offset-1 col-sm-offset-1">
                                        <input type="checkbox" name="SE" id="SE" value="SE"/>
                                        <label for="SE"><strong>Syst�mes Embarqu�s</strong></label>
                                    </div>
                                    <div class="checkbox col-lg-offset-1 col-md-offset-1 col-sm-offset-1">
                                        <input type="checkbox" name="BIO" id="BIO" value="BIO"/>
                                        <label for="BIO"><strong>Biom�dical</strong></label>
                                    </div>
                                    <div class="checkbox col-lg-offset-1 col-md-offset-1 col-sm-offset-1">
                                        <input type="checkbox" name="OC" id="OC" value="OC"/>
                                        <label for="OC"><strong>Objets Connect�s</strong></label>
                                    </div>
                                    <div class="checkbox col-lg-offset-1 col-md-offset-1 col-sm-offset-1">
                                        <input type="checkbox" name="DSMT" id="DSMT" value="DSMT"/>
                                        <label for="DSMT"><strong>Data Sciences, Multimedia & Telecom</strong></label>
                                    </div>
                                    <div class="checkbox col-lg-offset-1 col-md-offset-1 col-sm-offset-1">
                                        <input type="checkbox" name="NRJ" id="NRJ" value="NRJ"/>
                                        <label for="NRJ"><strong>�nergie et Environnement</strong></label>
                                    </div>
                                    <div class="checkbox col-lg-offset-1 col-md-offset-1 col-sm-offset-1">
                                        <input type="checkbox" name="BD" id="BD" value="BD"/>
                                        <label for="BD"><strong>Big Data</strong></label>
                                    </div>
                                    <c:if test="${!empty erreurs}">
										<span class="erreur">
											${erreurs['options']} 
											<br/>
										</span>
                                    </c:if>
                                </div>

                                <div class="col-lg-12 col-md-12">
                                    <div class="text-center">
                                        <button type="submit" class="btn btn-primary" id="submitSujet">
                                            <i class="fas fa-check-circle fa-fw" aria-hidden="true"></i>&nbsp; Soumettre
                                            l'ann�e
                                        </button>
                                        <button type="reset" class="btn btn-primary" id="reset">
                                            <i class="fas fa-redo fa-fw" aria-hidden="true"></i>&nbsp; Remettre � z�ro
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<c:import url="/inc/scripts.jsp"/>

<script type="text/javascript" src="<c:url value="/js/bootstrap-datetimepicker.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/js/bootstrap-datetimepicker.fr.js"/>"></script>
<script type="text/javascript">
    $('.years').datetimepicker({
        format: "yyyy",
        startView: 'decade',
        minView: 'decade',
        viewSelect: 'decade',
        autoclose: true,
    });
    $('.form_date').datetimepicker({
        language: 'fr',
        weekStart: 1,
        todayBtn: 1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 2,
        minView: 2,
        forceParse: 0
    });
</script>
</body>
</html>