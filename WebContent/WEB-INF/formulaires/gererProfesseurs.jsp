<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="ISO-8859-1" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <c:import url="/inc/head.jsp"/>
    <title>SoManager - G�rer les professeurs</title>
    <link rel="shortcut icon" type="image/x-icon"
          href="<c:url value="/images/favicon.ico"/>"/>
</head>
<body>

<div class="wrapper">
    <c:import url="/Navbar"/>
    <!-- Page Content Holder -->
    <div id="content">
        <c:import url="/inc/headerForSidebar.jsp"/>


        <div class="panel panel-primary ">
            <div class="panel-heading">Professeur - Gestion des professeurs</div>
            <div class="panel-body">

                <div class="panel panel-primary ">
                    <div class="panel-heading">Valider/Refuser les fonctions des
                        professeurs
                    </div>
                    <div class="panel-body">


                        <div class="panel panel-default">
                            <div class="panel-heading">Liste des r�f�rents � valider</div>
                            <div class="panel-body">

                                <form method="post" action="<c:url value="/GererProfesseurs"/>">
                                    <table class="table table-striped">
                                        <thead>
                                        <tr>
                                            <th>Nom</th>
                                            <th>Pr�nom</th>
                                            <th>Sujet</th>
                                            <th class="text-center">Valider</th>

                                        </tr>
                                        </thead>

                                        <tbody id="divProfesseurAValider">
                                        <c:forEach var="prof" items="${professeursReferentsAValider}"
                                                   varStatus="loop">
                                            <tr>
                                                <td id="nom-${loop.index}"><c:out value="${prof.nom}"/></td>
                                                <td id="prenom-${loop.index}"><c:out
                                                        value="${prof.prenom}"/></td>
                                                <td id="titre-${loop.index}"><c:out
                                                        value="${sujetsDesReferentsAValider[loop.index].titre}"/></td>
                                                <td align="center" id="valider-${loop.index}"><label>Oui
                                                    <input id="optradio-${loop.index}" type="radio"
                                                           name="optradio-${loop.index}" value="true"
                                                           checked="checked">
                                                </label> <label>Non <input id="optradio-${loop.index}"
                                                                           type="radio" name="optradio-${loop.index}"
                                                                           value="false"></label></td>

                                            </tr>

                                        </c:forEach>
                                        </tbody>
                                    </table>

                                    <div class="text-center">
                                        <button type="submit" class="btn btn-primary" id="valider">
                                            <i class="fas fa-check-circle fa-fw" aria-hidden="true"></i>&nbsp; Valider
                                        </button>
                                    </div>
                                    <input type="hidden" name="formulaire" id="formulaire"
                                           value="validerReferent"/>
                                </form>

                            </div>
                        </div>


                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <a style="font-size: 14px; color: black" data-toggle="collapse"
                                   href="#suppProf"> Supprimer un professeur sur un sujet <span
                                        class="glyphicon glyphicon-chevron-down pull-right"></span>
                                </a>

                            </div>
                            <div id="suppProf" class="panel-collapse collapse panel-body">

                                <form method="post" action="<c:url value="/GererProfesseurs"/>">
                                    <table class="table table-striped">
                                        <thead>
                                        <tr>
                                            <th>Nom</th>
                                            <th>Pr�nom</th>
                                            <th>Sujet</th>
                                            <th>Fonction</th>
                                            <th class="text-center">Supprimer</th>

                                        </tr>
                                        </thead>

                                        <tbody id="divProfesseurAValider">
                                        <c:forEach var="profSujet" items="${listeProfesseurSujets}"
                                                   varStatus="loop4">
                                            <tr>
                                                <td id="nom-${loop4.index}"><c:out
                                                        value="${professeurs[loop4.index].nom}"/></td>
                                                <td id="prenom-${loop4.index}"><c:out
                                                        value="${professeurs[loop4.index].prenom}"/></td>
                                                <td id="titre-${loop4.index}"><c:out
                                                        value="${sujets[loop4.index].titre}"/></td>
                                                <td id="fonction-${loop4.index}"><c:out
                                                        value="${profSujet.fonction}"/></td>
                                                <td align="center" id="valider-${loop4.index}"><input
                                                        id="optradio-${loop4.index}" type="checkbox"
                                                        name="optradio-${loop4.index}" value="true"></td>

                                            </tr>

                                        </c:forEach>
                                        </tbody>
                                    </table>

                                    <div class="text-center">
                                        <button type="submit" class="btn btn-primary" id="valider">
                                            <i class="fas fa-check-circle fa-fw" aria-hidden="true"></i>&nbsp; Valider
                                        </button>
                                    </div>
                                    <input type="hidden" name="formulaire" id="formulaire"
                                           value="supprimerFonction"/>
                                </form>

                            </div>
                        </div>

                    </div>

                </div>

                <div class="panel panel-primary ">
                    <div class="panel-heading">G�rer les attributions des
                        r�f�rents
                    </div>
                    <div class="panel-body">

                        <div class="col-lg-6">
                            <div class="panel panel-default">
                                <div class="panel-heading">Liste des profeseurs pouvant
                                    �tre r�f�rents
                                </div>
                                <div class="panel-body">

                                    <form method="post" action="<c:url value="/GererProfesseurs"/>">
                                        <div id="tableAttribution" style="overflow: auto">
                                            <table class="table table-striped">
                                                <thead>
                                                <tr>
                                                    <th>Nom</th>
                                                    <th>Pr�nom</th>
                                                    <th align="center">Nombre de fois r�f�rent</th>
                                                </tr>
                                                </thead>

                                                <tbody id="divProfesseurSansSujet">
                                                <c:forEach var="prof" items="${professeursAAttribuer}"
                                                           varStatus="loop2">
                                                    <tr data-option='${optionEtudiant[loop2.index]}'
                                                        id="draggableProf-${prof.idUtilisateur}" draggable='true'
                                                        ondragstart="dragReferent(event,${prof.idUtilisateur});">
                                                        <td id="nomProfesseurSansSujet-${loop2.index}"><c:out
                                                                value="${prof.nom}"/></td>
                                                        <td id="prenomProfesseurSansSujet-${loop2.index}"><c:out
                                                                value="${prof.prenom}"/></td>
                                                        <td id="nbReferent-${loop2.index}"
                                                            style="text-align: center;"><c:out
                                                                value="${nombreDeFoisReferent[loop2.index]}"/></td>

                                                    </tr>

                                                </c:forEach>
                                                </tbody>
                                            </table>
                                        </div>
                                    </form>

                                </div>

                            </div>
                        </div>


                        <div class="col-lg-6">
                            <div class="panel panel-default" id="sujetSansReferent">
                                <div class="panel-heading">Liste des sujets sans r�f�rent</div>
                                <div class="panel-body">
                                    <form method="post" id="divSujet"
                                          action="<c:url value="/GererProfesseurs"/>">

                                        <c:forEach var="sujet" items="${sujetsSansReferent}"
                                                   varStatus="loop3">
                                            <div class="panel panel-default"
                                                 ondrop="dropReferent(event,${loop3.index})"
                                                 ondragover="allowDrop(event,${loop3.index})"
                                                 ondragleave="refuseDrop(event,${loop3.index})">

                                                <div class="panel-heading" value="Afficher ou Masquer"
                                                     onClick="AfficherMasquer('${loop3.index}')">
													<span id="chevron-${loop3.index}"
                                                          class="glyphicon glyphicon-chevron-down pull-right"></span>
                                                    &nbsp;${sujet.titre}
                                                </div>

                                                <div id="divacacher-${loop3.index}" class="panel-body"
                                                     style="display: none;">


                                                    <div id="dynamicInput">
                                                        <div id="referent-${loop3.index}" type="text"
                                                             class="form-control" draggable='false'>
                                                            </br>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </c:forEach>
                                        <button type="submit" class="btn btn-primary center-block">
                                            <i class="far fa-save fa-fw" aria-hidden="true"></i>&nbsp;
                                            Enregistrer
                                        </button>
                                        <input type="hidden" name="formulaire" id="formulaire"
                                               value="attribuerReferent"/>
                                    </form>


                                </div>
                            </div>

                            <div class="panel panel-default" id="resultatAttribution">
                                <div class="panel-heading">R�sultat de l'attribution des
                                    professeurs
                                </div>
                                <div class="panel-body">
                                    <c:choose>
                                        <c:when
                                                test="${(!empty nombreAttributions)||(!empty nombreEchecs)}">
                                            <ul class="list-group">
                                                <li
                                                        class="list-group-item list-group-item-action list-group-item-success justify-content-between">
                                                    Nombre d'attributions <span
                                                        class="badge badge-default badge-pill">${nombreAttributions}</span>
                                                </li>
                                                <li
                                                        class="list-group-item list-group-item-action list-group-item-danger justify-content-between">
                                                    Nombre d'�checs <span
                                                        class="badge badge-default badge-pill">${nombreEchecs}</span>

                                                    <div class="panel-group">
                                                        <br>
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading">
                                                                <div class="panel-title ">
                                                                    <a style="font-size: 14px" data-toggle="collapse"
                                                                       href="#collapse1">Liste des erreurs <span
                                                                            class="glyphicon glyphicon-chevron-down pull-right"></span>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div id="collapse1" class="panel-collapse collapse">
                                                                <ul class="list-group">
                                                                    <c:forEach var="erreurAttribution"
                                                                               items="${erreursAttributionReferents}">
                                                                        <li class="list-group-item list-group-item-default">
                                                                            <strong>Erreur
                                                                                :</strong> ${erreurAttribution}</li>
                                                                    </c:forEach>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </c:when>
                                        <c:otherwise>
                                            <p align="center">Veuillez attribuer des professeurs</p>
                                        </c:otherwise>
                                    </c:choose>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
            </div>
        s</div>
    </div>
</div>

<script src="<c:url value="/js/dragAndDrop.js"/>"></script>
<script>
    /* Masque/Affiche une div*/

    function AfficherMasquer(idSujet) {
        divInfo = document.getElementById("divacacher-" + idSujet);
        chevron = document.getElementById("chevron-" + idSujet);

        if (divInfo.style.display == 'none') {
            divInfo.style.display = '';
            chevron.className = 'glyphicon glyphicon-chevron-up pull-right';

        } else {
            divInfo.style.display = 'none';
            chevron.className = 'glyphicon glyphicon-chevron-down pull-right';

        }

    }

    var dragNumber = -1;

    /*Drag and drop*/
    function dragReferent(ev, id) {
        // numero de la ligne de tableau o� est contenu l'�tudiant
        dragNumber = id;
    }

    /**   */
    function dropReferent(ev, id) {
        //var id = (id == undefined) ? ev.dataTransfer.getData("id") : id;
        if (document.getElementById("divacacher-" + id).style.display == 'none') {
            AfficherMasquer(id);
        }
        // zone dans laquelle on souhaite inclure l'�tudiant
        // on la s�lectionne par son id
        divDrag = document.getElementById("draggableProf-" + dragNumber);
        divDrop = document.getElementById("referent-" + id);

        divDrop.innerHTML = divDrag.innerHTML
            + "<input type='hidden' name='idReferent-" + id + "' value=" + dragNumber + " >";
    }

    /** Change la couleur de la bordure lors du hover du div drop */
    function allowDrop(ev, id) {
        ev.preventDefault();
    }

    /** Change la couleur de la bordure lorsque l'on quitte le div drop */
    function refuseDrop(ev, id) {
        ev.preventDefault();
    }

    function changerTailleScroll() {
        document.getElementById("tableAttribution").style.height = (document.getElementById('sujetSansReferent').clientHeight + document.getElementById('resultatAttribution').clientHeight) + "px";
    }

    window.onload = changerTailleScroll;
</script>
<c:import url="/inc/scripts.jsp"/>
</body>
</html>