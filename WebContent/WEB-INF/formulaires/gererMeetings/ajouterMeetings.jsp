<%--
  Created by IntelliJ IDEA.
  User: dimitrijarneau
  Date: 19/05/2018
  Time: 11:00
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>


<div class="col-sm-12">
    <%--
    Ajout meeting
    --%>
    <form data-toggle="validator" role="form" method="post"
          action="<c:url value="/GererMeetings"/>"
          id="valider">
        <%-- Premiere colonne --%>
        <div class="col-sm-6">

            <div class="form-group has-feedback">
                <label for="titre" class="control-label">Titre : <span
                        class="requis">*</span></label>
                <input maxlength="30" type="text" name="titre" class="form-control" id="titre" required/>
                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                <div class="help-block with-errors"></div>
            </div>

            <div class="form-group has-feedback">
                <label for="description" class="control-label">Description : <span
                        class="requis">*</span></label>
                <textarea maxlength="80" name="description" class="form-control" style="resize:none" rows="2"
                          id="description"
                          required></textarea></textarea>
                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                <div class="help-block with-errors"></div>
            </div>

            <div class="form-group has-feedback">
                <label for="lieu" class="control-label">Lieu : <span class="requis">*</span></label>
                <input maxlength="30" type="text" name="lieu" class="form-control" id="lieu" required/>
                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                <div class="help-block with-errors"></div>
            </div>
        </div>

        <%-- Deuxième colonne--%>
        <div class="col-sm-6">


            <div class="form-group has-feedback">
                <label for="nomCreateurMeeting" class="control-label">Créateur du
                    meeting
                    : <span class="requis">*</span></label>
                <input type="text" name="porteur" class="form-control" id="nomCreateurMeeting"
                       value="<c:out value="${sessionScope.utilisateur.nom} ${sessionScope.utilisateur.prenom}"/>"
                       disabled="disabled"/>
            </div>

            <label>Date et heure de début : <span
                    class="requis">*</span></label>

            <div class="row">
                <div class="col-md-6 col-md-6">
                    <div class="input-group date form_date" data-date=""
                         data-date-format="dd MM yyyy"
                         data-link-field="dateDebut" data-link-format="yyyy-mm-dd">
                        <input class="form-control" size="16" type="text" value="" required>
                        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                        <div class="help-block with-errors"></div>
                        <span class="input-group-addon">
										<span class="glyphicon glyphicon-remove"></span></span>
                        <span class="input-group-addon">
                                        <i class="far fa-calendar-alt"></i>
                    </div>
                    <input type="hidden" id="dateDebut" name="dateDebut" value=""/><br/>
                </div>

                <div class="col-md-6 col-md-4">
                    <div class="input-group date form_time" data-date="" data-date-format="hh:ii">
                        <input class="form-control" id="heureDebut" name="heureDebut" size="16"
                               type="text" value="" required>
                        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                        <div class="help-block with-errors"></div>
                        <span class="input-group-addon">
										<span class="glyphicon glyphicon-remove"></span></span>
                        <span class="input-group-addon">
                                        <i class="far fa-clock"></i>
                    </div>
                    <input type="hidden" id="heureDebut" name="heureDebut" value=""/><br/>

                </div>
            </div>


            <div class="form-group has-feedback">
                <label for="compteRendu" class="control-label">Compte rendu : <span
                        class="requis">*</span></label>
                <i>
                                    <textarea maxlength="200" name="compteRendu" class="form-control" style="resize:none"
                                              id="compteRendu"
                                              disabled="disabled"/>Compte rendu à ajouter.</textarea>
                </i>
                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                <div class="help-block with-errors"></div>
                <input type="hidden" id="compteRendu" name="compteRendu" value=""/>
            </div>

        </div>


        <div class="text-center">
            <button type="submit" class="btn btn-primary" id="soumettre">
                <i class="fas fa-pencil-alt fa-fw" aria-hidden="true"></i>&nbsp; Créer meeting
            </button>
            <input type="hidden" name="formulaire" id="formulaire" value="ajouterMeeting"/>
        </div>
    </form>
</div>