<%--
  Created by IntelliJ IDEA.
  User: dimitrijarneau
  Date: 19/05/2018
  Time: 11:30
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<div class="col-sm-12">
    <form method="post" action="<c:url value="/GererMeetings"/>">
        <%-- Premiere colonne --%>
        <div class="col-sm-6">
            <div class="form-group has-feedback">
                <label>Mes meetings : <span class="requis">*</span></label>
                <select class="form-control" id="selectRole" name="selectRole">
                    <c:forEach items="${listeMeeting_A}" var="element">
                        <option value="${element}">${element.substring(0,element.indexOf('%/%'))}</option>
                        <%-- element.substring(0,element.indexOf("%/%"))--%>
                    </c:forEach>
                </select>
            </div>
        </div>
        <%-- Deuxieme colonne --%>
        <div class="col-sm-6">
            <div class="form-group has-feedback">
                <label>Nom de l'utilisateur à inviter : <span
                        class="requis">*</span></label>
                <div class="typeahead__container">
                    <div class="typeahead__field">
											<span class="typeahead__query">
											<input class="js-typeahead" name="utilisateurRecherche"
                                                   id="utilisateurRecherche" type="search" autocomplete="off"> </span>
                    </div>
                </div>
            </div>
        </div>


        <div class="col-lg-12 col-md-12" align="center">
            <span class="erreur">${erreurs['professeur']}</span>
        </div>

        <button type="submit" class="btn btn-success center-block" id="submit">
            <i class="fas fa-plus-circle"></i> Ajouter
        </button>
        <input type="hidden" name="formulaire" id="formulaire" value="ajouterInvite"/>

        <br>

        <div class="alert alert-info alert-dismissible">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong>Informations :</strong><br/>
            - Pour inviter un utilisateur à un de vos meetings,
            choisissez un meeting et un utilisateur puis cliquez sur "Ajouter". <br/>
            - Seul le créateur d'un meeting peut lui ajouter des participants.
        </div>

    </form>
</div>



