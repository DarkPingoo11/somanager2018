<%--
  Created by IntelliJ IDEA.
  User: dimitrijarneau
  Date: 19/05/2018
  Time: 10:50
  To change this template use File | Settings | File Templates.
--%>
<%--
  Created by IntelliJ IDEA.
  User: dimitrijarneau
  Date: 10/04/2018
  Time: 08:31
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>


<div class="col-md-6">
    <div class="panel panel-default ">
        <div class="panel-heading">
            <i class="far fa-calendar-alt"></i>&nbsp;
            Voir mes meetings
        </div>
        <div class="panel-body">
            <div class="col-md-12">
                <c:import url="/inc/listeMeetings.jsp"/>
            </div>
        </div>
    </div>
</div>

<%-- Modification d'un meeting --%>
<div class="col-lg-6">
    <div class="panel panel-default ">
        <div class="panel-heading">
            <i class="fas fa-wrench"></i>&nbsp;
            Modifier un de mes meeting
        </div>
        <div class="panel-body">
            <div class="col-lg-12">
                <!--  Création du div drop qui permet la modification -->
                <div id="drop" ondrop="drop(event)" draggable="false" ondragover="allowDrop(event)"
                     ondragleave="refuseDrop(event)">
                    <div id="drophidden--1"
                         style="display: block; text-align: center;">
                        <p>Faites Glisser-déposer un meeting ici pour le modifier ou
                            Double-cliquez sur le titre du meeting.</p>
                        <img src="<c:url value="/images/giphy.gif"/>"
                             alt="Smiley face" height="42" width="42">
                    </div>

                    <!--  Création d'un formulaire invisible tant qu'un id n'a pas été passé dans le div drop -->
                    <c:forEach var="meeting" items="${listeMeeting}" varStatus="loop">
                        <div id="drophidden-${loop.index}" style="display: none; overflow: visible;">
                            <form data-toggle="validator" role="form" method="post"
                                  action="<c:url value="/GererMeetings"/>">
                                <fieldset>
                                    <legend>
                                        <div class="col-md-2">
                                            <input type="hidden" name="formulaire" value="modifierMeeting">
                                            <div class="text-center">
                                                <button type="submit" class="btn btn-success" id="submit">
                                                    <i class="far fa-save fa-lg"></i>
                                                </button>
                                            </div>
                                        </div>
                                        Modifier le meeting " <c:out value="${meeting.titre}"/> "
                                    </legend>

                                    <input type="hidden" name="idReunion" id="idReunion"
                                           value="<c:out value="${meeting.idReunion}"/>"/>
                                    <input type="hidden" name="dateMeeting" id="dateMeeting"
                                           value="<c:out value="${meeting.date}"/>"/>
                                    <input type="hidden" name="idCreateurMeeting"
                                           id="idCreateurMeeting"
                                           value="<c:out value="${meeting.refUtilisateur}"/>"
                                    />

                                    <div class="form-group has-feedback">
                                        <label for="titre" class="control-label">Titre : <span
                                                class="requis">*</span></label>
                                        <input maxlength="30" type="text" name="titre" class="form-control" id="titre"
                                               value="<c:out value="${meeting.titre}"/>" autofocus
                                               required/>
                                        <div class="help-block with-errors"></div>
                                    </div>

                                    <div class="form-group has-feedback">
                                        <label for="description" class="control-label">Description : <span
                                                class="requis">*</span></label>
                                        <textarea maxlength="80" name="description" style="resize:none"
                                                  class="form-control" id="description"
                                                  required/>${meeting.description} </textarea>
                                        <div class="help-block with-errors"></div>
                                    </div>

                                    <div class="form-group has-feedback">
                                        <label for="lieu" class="control-label">Lieu : <span
                                                class="requis">*</span></label>
                                        <input maxlength="30" type="text" name="lieu" class="form-control" id="lieu"
                                               min="1" step="1" value="<c:out value="${meeting.lieu}"/>"
                                               required/>
                                        <div class="help-block with-errors"></div>
                                    </div>


                                    <div class="form-group has-feedback">
                                        <label for="nomCreateurMeeting" class="control-label">Créateur du
                                            meeting
                                            : <span class="requis">*</span></label>
                                        <input type="text" name="createur" class="form-control"
                                               id="nomCreateurMeeting"
                                               value="<c:out value="${nomCreateurMeeting[meeting.idReunion]}"/>"
                                               disabled="disabled"/>
                                        <input type="hidden" name="idCreateurMeeting" class="form-control"
                                               id="idCreateurMeeting"
                                               value="<c:out value="${idCreateurMeeting[meeting.idReunion]}"/>"
                                               disabled="disabled"/>
                                    </div>


                                    <div class="form-group has-feedback">
                                        <label for="dateHeure" class="control-label">Date et heure de début
                                            : <span class="requis">*</span></label>
                                        <input type="text" name="dateHeure" class="form-control"
                                               id="dateHeure"
                                               value="<c:out value="${(meeting.date).substring(0,16)}"/>"
                                               required/>
                                        <div class="help-block with-errors"></div>
                                    </div>

                                    <div class="form-group has-feedback">
                                        <label for="dateHeure" class="control-label">Liste des invités
                                            : <span class="requis">*</span></label>
                                        <textarea name="listeInvites" style="resize:none"
                                                  class="form-control" id="listeInvites"
                                                  disabled="disabled"/>${nomsInvitesMeetings[meeting.idReunion]} </textarea>
                                        <div class="help-block with-errors"></div>
                                    </div>

                                    <c:if test="${hmapMeeting.get(String.valueOf(meeting.idReunion)) == false}">
                                        <div class="form-group has-feedback">
                                            <label for="compteRendu" class="control-label">Compte rendu du
                                                meeting : <span class="requis">*</span></label>
                                            <textarea maxlength="200"
                                                      name="compteRendu" style="resize:none"
                                                      class="form-control" id="compteRendu"
                                                      required/>${meeting.compteRendu} </textarea>

                                            <div class="help-block with-errors"></div>
                                        </div>
                                    </c:if>

                                    <c:if test="${hmapMeeting.get(String.valueOf(meeting.idReunion)) == true}">
                                        <div class="form-group has-feedback">
                                            <label for="compteRendu" class="control-label">Compte rendu du
                                                meeting : <span class="requis">*</span></label>
                                            <textarea maxlength="200"
                                                      name="compteRendu" style="resize:none"
                                                      class="form-control" id="compteRendu"
                                                      disabled="disabled"
                                                      required/>${meeting.compteRendu} </textarea>

                                            <div class="help-block with-errors"></div>
                                        </div>
                                    </c:if>

                                </fieldset>

                                <div class="col-md-4">
                                    <input type="hidden" name="formulaire" value="modifierMeeting">
                                    <div style="text-align:left">
                                        <button type="submit" class="btn btn-success" id="submit">
                                            <i class="far fa-save fa-lg"></i>&nbsp; Enregistrer
                                        </button>
                                    </div>
                                </div>
                            </form>

                            <form data-toggle="validator" role="form" method="post" id="validerCompteRendu"
                                  action="<c:url value="/GererMeetings"/>">
                                <div class="col-md-4">
                                    <input type="hidden" name="idReunion" id="idReunion"
                                           value="<c:out value="${meeting.idReunion}"/>"/>
                                    <input type="hidden" name="formulaire" value="validerCompteRenduMeeting">
                                    <c:if test="${estProf == true}">
                                        <div style="text-align:center">
                                            <button type="submit" class="btn btn-success"
                                                    id="validerCompteRenduMeeting">
                                                <i class="fas fa-check"></i>&nbsp; Valider le compte rendu
                                            </button>
                                        </div>
                                    </c:if>
                                </div>
                            </form>

                            <form data-toggle="validator" role="form" method="post" id="supprimer"
                                  action="<c:url value="/GererMeetings"/>">
                                <div class="col-md-4">
                                    <input type="hidden" name="idReunion" id="idReunion"
                                           value="<c:out value="${meeting.idReunion}"/>"/>
                                    <input type="hidden" name="formulaire" value="supprimerMeeting">
                                    <div style="text-align:right">
                                        <button type="submit" class="btn btn-danger" id="supprimerMeeting"
                                                data-toggle="modal" data-target="#supprimerMeetingConfModal">
                                            <i class="far fa-trash-alt"></i>
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </c:forEach>

                </div>
            </div>
        </div>

    </div>
</div>



