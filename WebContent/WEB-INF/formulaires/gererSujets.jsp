<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <c:import url="/inc/head.jsp"/>
    <title>SoManager - Gerer les sujets</title>
</head>
<body>
<div class="wrapper">
    <c:import url="/Navbar"/>
    <!-- Page Content Holder -->
    <div id="content">
        <c:import url="/inc/headerForSidebar.jsp"/>


        <div class="panel panel-primary ">

            <div class="panel-heading">Sujet - Gestion des sujets</div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="panel panel-default ">

                            <div class="panel-heading">Rechercher des sujets</div>
                            <div class="panel-body">
                                <input class="form-control" id="searchbar" type="text"
                                       placeholder="Rechercher par mots-clés" value=""
                                       onkeyup="rechercher()"/>
                                <p align="center">Afficher les sujets de mon option :</p>
                                <div class="col-lg-12">
                                    <c:import url="/inc/listeSujets.jsp"/>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class=col-lg-6>
                        <div id="drop" ondrop="drop(event)" draggable="false"
                             ondragover="allowDrop(event)" ondragleave="refuseDrop(event)">
                            <div id="drophidden--1" style="display: block; text-align: center;">
                                <p>Faites Glisser-déposer un sujet ici pour le voir en détail
                                    ou Double-cliquez dessus</p>
                                <img src="<c:url value="/images/giphy.gif"/>" alt="Smiley face"
                                     height="42" width="42">
                            </div>

                            <!--  Création d'un résumé invisible tant qu'un id n'a pas été passé dans le div drop -->
                            <c:forEach var="sujet" items="${listeSujets}" varStatus="loop">
                                <fieldset id="drophidden-${loop.index}"
                                          style="display: none; overflow: visible;">
                                    <div class="list-group">

                                        <a class="list-group-item active">
                                            <h5 class="list-group-item-heading">Nom du sujet :</h5>
                                            <h4 class="list-group-item-text"><c:out value="${sujet.titre}"/></h4>
                                        </a>

                                        <a class="list-group-item">
                                            <h4 class="list-group-item-heading">Description :</h4>
                                            <h5 class="list-group-item-text"><c:out value="${sujet.description}"/></h5>
                                        </a>

                                        <a class="list-group-item">
                                            <h4 class="list-group-item-heading">Porteur :</h4>
                                            <h5 class="list-group-item-text"><c:out
                                                    value="${nomsPorteursSujets[sujet.idSujet]}"/></h5>
                                        </a>

                                        <a class="list-group-item">
                                            <h4 class="list-group-item-heading">Options :</h4>
                                            <h5 class="list-group-item-text"><c:out
                                                    value="${optionSujet[loop.index]}"/></h5>
                                        </a>

                                        <a class="list-group-item">
                                            <h4 class="list-group-item-heading">Etat :</h4>
                                            <h5 class="list-group-item-text"><c:out value="${sujet.etat}"/></h5>
                                        </a>

                                        <a class="list-group-item">
                                            <h4 class="list-group-item-heading">Confidentialite :</h4>
                                            <c:choose>
                                                <c:when test="${sujet.confidentialite eq 'true'}">
                                                    <h5 class="list-group-item-text">Oui</h5>
                                                </c:when>
                                                <c:otherwise>
                                                    <h5 class="list-group-item-text">Non</h5>
                                                </c:otherwise>
                                            </c:choose>
                                        </a>

                                        <a class="list-group-item">
                                            <h4 class="list-group-item-heading">Contrat Pro :</h4>
                                            <c:choose>
                                                <c:when test="${sujet.contratPro eq 'true'}">
                                                    <h5 class="list-group-item-text">Oui</h5>
                                                </c:when>
                                                <c:otherwise>
                                                    <h5 class="list-group-item-text">Non</h5>
                                                </c:otherwise>
                                            </c:choose>
                                        </a>
                                        <a class="list-group-item">
                                            <h4 class="list-group-item-heading">Nombre d'étudiants :</h4>
                                            <h5 class="list-group-item-text">Entre <c:out
                                                    value="${sujet.nbrMinEleves}"/> et <c:out
                                                    value="${sujet.nbrMaxEleves}"/></h5>
                                        </a>
                                        <br>
                                        <c:set var="etat" scope="session" value="${sujet.etat}"/>
                                        <c:if test="${etat eq 'VALIDE' && profResponsable eq 'true'}">
                                            <form action="<c:url value="/GererSujets"/>" method="post"
                                                  enctype="multipart/form-data">
                                                <input type="hidden" name="idSujet" value="${sujet.idSujet}"/>
                                                <button type="submit" class="btn btn-primary center-block">
                                                    <i class="fas fa-check-circle fa-fw" aria-hidden="true"></i>&nbsp;
                                                    Publier le sujet
                                                </button>
                                            </form>
                                        </c:if>
                                        <c:if test="${etat eq 'DEPOSE'}">
                                            <form action="<c:url value="/GererSujets"/>" method="post"
                                                  enctype="multipart/form-data">
                                                <input type="hidden" name="idSujet" value="${sujet.idSujet}"/>
                                                <input type="hidden" name="actionValiderRefuser" value="publier"/>
                                                <button type="submit" class="btn btn-primary center-block">
                                                    <i class="fas fa-check-circle fa-fw" aria-hidden="true"></i>&nbsp;
                                                    Valider le sujet
                                                </button>
                                            </form>
                                            <br>
                                            <form action="<c:url value="/GererSujets"/>" method="post"
                                                  enctype="multipart/form-data">
                                                <input type="hidden" name="idSujet" value="${sujet.idSujet}"/>
                                                <input type="hidden" name="actionValiderRefuser" value="refuser"/>
                                                <button type="submit" class="btn btn-danger center-block ">
                                                    <i class="fas fa-ban fa-fw" aria-hidden="true"></i>&nbsp; Refuser le
                                                    sujet
                                                </button>
                                            </form>
                                        </c:if>

                                        <div style="margin:0px 0px 0px 0%;  float: right;">
                                            <%@ include file="/inc/commenter.jsp" %>


                                        </div>
                                    </div>
                                </fieldset>
                            </c:forEach>
                        </div>
                    </div>
                </div>
                <form action="<c:url value="/TelechargerPostersOption"/>" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="optionProf" value="${listNomsOptionUtilisateur}"/>
                    <br>
                    <button type="submit" class="btn btn-primary center-block">
                        <i class="fas fa-file-archive fa-fw" aria-hidden="true"></i>&nbsp;Télécharger l'ensemble des
                        posters de l'option ${listNomsOptionUtilisateur}
                    </button>
                    <%-- 					<input type="submit" value="Télécharger l'ensemble des posters de l'option ${listNomsOptionUtilisateur}" /> --%>
                </form>
            </div>
        </div>
    </div>
</div>

<c:import url="/inc/scripts.jsp"/>

<script src="<c:url value="/js/dragAndDrop.js"/>"></script>
<script defer>
    var sel2 = document.querySelector('#divSujet');
    var options2 = sel2.querySelectorAll('*[id^="draggable-"]');
    var sujetValue = document.querySelectorAll('*[id^="drophidden-"]');

    /**
     * Garde les div sujets qui possède le string écrit dans la searchbar
     * Appelé sur chaque changement de searchbar
     */
    function rechercher() {
        sel2.innerHTML = '';
        var textInput = document.getElementById("searchbar").value
            .toLowerCase();
        for (var i = 0; i < options2.length; i++) {
            //var description = sujetValue[i+1].getElementById("description-");
            if ((sujetValue[i + 1].textContent).toLowerCase().indexOf(
                textInput) != -1) {
                sel2.appendChild(options2[i]);
            }
        }
    }
</script>
<script>
    function commentairehidden(id) {
        divCommentaire = document.getElementById("commentairehidden-" + id);
        if (divCommentaire.style.display === "none") {
            divCommentaire.style.display = "block";
            var h = divCommentaire.clientHeight + 3;
            //var w = divCommentaire.clientWidth-20;
            divCommentaire.style.margin = "-" + h + "px 0px 0px -50%";
        } else {
            divCommentaire.style.display = "none";
            divCommentaire.style.margin = "0px 0px 0px 0px";
        }

    }

    function ecrirehidden(id, idBis) {
        divEcrire = document.getElementById("subComment-" + id);
        divCommentaire = document.getElementById("commentairehidden-" + idBis);
        if (divEcrire.style.display === "none") {
            divEcrire.style.display = "block";
            var h = divCommentaire.clientHeight + 3;
            divCommentaire.style.margin = "-" + h + "px 0px 0px -50%";
        } else {
            divEcrire.style.display = "none";
            var h = divCommentaire.clientHeight + 3;
            divCommentaire.style.margin = "-" + h + "px 0px 0px -50%";
        }
    }
</script>
</body>
</html>