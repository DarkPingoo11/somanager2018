<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <c:import url="/inc/head.jsp"/>
    <title>SoManager - Exporter des posters</title>
    <link rel="stylesheet" href="<c:url value="/css/jquery.typeahead.css"/>">
</head>
<body>
<div class="wrapper">
    <c:import url="/Navbar"/>
    <!-- Page Content Holder -->
    <div id="content">
        <c:import url="/inc/headerForSidebar.jsp"/>
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2">
                <div class="panel panel-primary ">
                    <div class="panel-heading">Poster - Exporter</div>
                    <div class="panel-body">
                        <form action="<c:url value="/ExporterPoster"/>" method="post" enctype="multipart/form-data">

                            <br>
                            <button type="submit" class="btn btn-primary center-block">
                                <i class="fas fa-file-archive fa-fw" aria-hidden="true"></i>&nbsp;Télécharger l'ensemble
                                des posters valides
                            </button>

                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<c:import url="/inc/scripts.jsp"/>

<script src="<c:url value="/js/dragAndDrop.js"/>" type="text/javascript"></script>
				