<%--
  Created by IntelliJ IDEA.
  User: dimitrijarneau
  Date: 13/06/2018
  Time: 19:23
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <c:import url="/inc/head.jsp"/>
    <title>SoManager - Création équipes</title>
</head>
<body>
<div class="wrapper">
    <c:import url="/Navbar"/>
    <!-- Page Content Holder -->
    <div id="content">
        <c:import url="/inc/headerForSidebar.jsp"/>
        <div class="panel panel-primary ">

            <div class="panel-heading">Equipe - Création des équipes</div>
            <div class="panel-body">

                <c:import url="/inc/callback.jsp"/>

                <div class="col-sm-12">
                    <form action="<c:url value='CreerEquipesI2' />" method="post"
                          id="jurysReview">
                        <div class="panel panel-primary" style="max-height: 100%;">
                            <div class="panel-heading" style="padding-bottom: 0">
                                <div class="row">
                                    <div class="col-sm-6 no-padding">
                                        <div class="col-sm-12">
                                            <div class="form-group full-width">
                                                <label>En cours: Sprint n° </label>
                                                <label title="Formulaire" class="text"
                                                       value="${ requestScope.sprintActuel }">${ requestScope.sprintActuel }
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.panel-header -->
                            </div>

                            <!-- En tête du tableau des Jurys -->
                            <div class="panel-body table-container">
                                <table class="table table-bordered" style="margin-bottom: -1px">
                                    <tr class="info unselectable">
                                        <th class="col-sm-2">Nom</th>
                                        <th class="col-sm-2">Membres</th>
                                        <th class="col-sm-2">Année</th>
                                        <th class="col-sm-2">Projet</th>
                                        <th class="col-sm-2">Product Owner(s)</th>
                                        <c:if test="${prof eq 'true' || profResponsable eq 'true' || profReferent eq 'true'}">
                                            <th class="col-sm-2">Modifier</th>
                                        </c:if>
                                    </tr>
                                </table>
                                <!-- /En tête du tableau des Jurys -->

                                <!-- Contenu du tableau -->
                                <div class="row">
                                    <div class="col-sm-12" id="listeMembresEquipeContainer">
                                        <table class="table table-hover" style="margin-bottom: 0">
                                            <c:forEach var="equipe" items="${listeEquipe}" varStatus="loop">
                                                <tbody id="listeMembresEquipe">
                                                <tr>
                                                    <td class="col-sm-2">
                                                            ${listeEquipe[loop.index].nom}</td>
                                                    </td>
                                                        <%-- Liste des membres --%>
                                                    <td class="col-sm-2">
                                                        <c:if test="${!empty listeCompositionEquipe}">
                                                            <c:forEach var="membreEquipe"
                                                                       items="${listeCompositionEquipe}">
                                                                <c:if test="${equipe.idEquipe eq membreEquipe.idEquipe}">
                                                                    <c:forEach var="etudiant"
                                                                               items="${listeUtilisateur}">
                                                                        <c:if test="${etudiant.idUtilisateur eq membreEquipe.idEtudiant}">
                                                                            ${etudiant.nom} ${etudiant.prenom}<br>
                                                                        </c:if>
                                                                    </c:forEach>
                                                                </c:if>
                                                            </c:forEach>
                                                        </c:if>

                                                        <c:if test="${empty listeCompositionEquipe}">
                                                            Aucun membre actuellement
                                                        </c:if>
                                                    </td>
                                                        <%-- Année de l'équipe --%>
                                                    <td class="col-sm-2">
                                                        <c:forEach var="annee" items="${listeAnneeScolaire}">
                                                            <c:if test="${annee.idAnneeScolaire eq equipe.refAnnee}">
                                                                ${annee.anneeDebut} / ${annee.anneeFin}<br>
                                                            </c:if>
                                                        </c:forEach>
                                                    </td>
                                                        <%-- Projet de l'équipe --%>
                                                    <td class="col-sm-2">
                                                        <c:forEach var="projet" items="${listeProjetEquipe}">
                                                            <c:if test="${projet.idProjet eq equipe.refProjet}">
                                                                ${projet.titre}<br>
                                                            </c:if>
                                                        </c:forEach>
                                                    </td>
                                                        <%-- Projduct Owner de l'équipe --%>
                                                    <td class="col-sm-2">
                                                        <c:forEach var="pOwner" items="${listeRolesEquipe}">
                                                            <c:if test="${pOwner.refEquipe eq equipe.idEquipe}">
                                                                <c:forEach var="productOwner"
                                                                           items="${listeUtilisateur}">
                                                                    <c:if test="${productOwner.idUtilisateur eq pOwner.refUtilisateur}">
                                                                        ${productOwner.nom} ${productOwner.prenom}
                                                                        <br>
                                                                    </c:if>
                                                                </c:forEach>
                                                            </c:if>
                                                        </c:forEach>
                                                    </td>


                                                    <c:if test="${prof eq 'true' || profResponsable eq 'true' || profReferent eq 'true'}">

                                                        <!-- Les boutons de modifications -->
                                                        <td class="col-sm-2">
                                                            <div class="box">
                                                                <a type="button" name="ajouterMembre"
                                                                   href="#modalAjouterMembre"
                                                                   data-toggle="modal"
                                                                   data-target="#modalAjouterMembre"
                                                                   data-titre="${equipe.nom}"
                                                                   data-idequipe="${equipe.idEquipe}"
                                                                   data-membres="${listeCompositionEquipe}"
                                                                   class="btn btn-success" id="supprimerMembre">
                                                                    <i class="fas fa-user-plus"></i>
                                                                </a>
                                                                <a type="button" name="supprimerMembre"
                                                                   href="#modalSupprimerMembre"
                                                                   data-toggle="modal"
                                                                   data-target="#modalSupprimerMembre"
                                                                   data-titre="${equipe.nom}"
                                                                   data-idequipe="${equipe.idEquipe}"
                                                                   data-membres="${listeCompositionEquipe}"
                                                                   class="btn btn-warning" id="supprimerMembre">
                                                                    <i class="fas fa-user-times"></i>
                                                                </a>


                                                                <a type="button" name="btnDelete"
                                                                   href="#modalSuppressionEquipe"
                                                                   data-toggle="modal"
                                                                   data-target="#modalSuppressionEquipe"
                                                                   class="btn btn-danger" id="supprimer"
                                                                   data-titre="${equipe.nom}"
                                                                   data-idequipe="${equipe.idEquipe}">
                                                                    <span class="fas fa-trash-alt fa-fw"></span>
                                                                </a>
                                                            </div>
                                                        </td>
                                                        <!-- /Les boutons de modifications -->

                                                    </c:if>

                                                </tr>
                                                </tbody>
                                            </c:forEach>
                                        </table>
                                        <!-- /Contenu du tableau -->

                                        <!-- Fenêtre Suppression des équipes -->
                                        <div id="modalSuppressionEquipe" class="modal fade" role="dialog"
                                             aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header  modal-header bg-danger">
                                                        <h2 class="modal-title" id="exampleModalLabel"> ... </h2>
                                                        <button type="button" class="close" data-dismiss="modal"
                                                                aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <form data-toggle="validator" role="form" method="post"
                                                              action="<c:url value="/CreerEquipesI2"/>">
                                                            <input type="hidden" id="idEquipeDelete"
                                                                   name="idEquipeDelete">
                                                            <div class="form-group">
                                                                <p> Etes vous sur de vouloir supprimer cette équipe
                                                                    ?</p>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <div class="btn-group btn-group-justified" role="group"
                                                                     aria-label="group button">
                                                                    <div class="btn-group" role="group">
                                                                        <button type="button" class="btn btn-primary"
                                                                                data-dismiss="modal"
                                                                                role="button">&nbsp; Non
                                                                        </button>
                                                                    </div>
                                                                    <div class="btn-group" role="group">
                                                                        <button type="submit" id="submit"
                                                                                class="btn btn-danger btn-hover-green"
                                                                                data-action="save" role="button">&nbsp;
                                                                            Oui
                                                                        </button>
                                                                        <input type="hidden" name="formulaire"
                                                                               id="formulaire"
                                                                               value="supprimerEquipeI2"/>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                        <!-- /Fenêtre POP-UP -->
                                        <!-- Fenêtre Suppression des membres -->
                                        <div id="modalSupprimerMembre" class="modal fade" role="dialog"
                                             aria-labelledby="popup1" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header  modal-header bg-warning">
                                                        <h2 class="modal-title" id="exampleModalLabel">...</h2>
                                                        <button type="button" class="close" data-dismiss="modal"
                                                                aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <form data-toggle="validator" role="form" method="post"
                                                              action="<c:url value="/CreerEquipesI2"/>">
                                                            <div class="form-group">
                                                                <input type="hidden" class="form-control"
                                                                       id="idEquipe"
                                                                       name="idEquipe">

                                                                <label for="anneeScolaireEquipe"
                                                                       class="control-label">Pour
                                                                    et à partir du sprint:
                                                                    <span class="requis">*</span>
                                                                </label><br>
                                                                <%-- Choix du sprint --%>
                                                                <select class="form-control" id="sprintSelect"
                                                                        name="sprintSelect">
                                                                    <c:forEach var="sprint"
                                                                               items="${listeSprintDAO}">
                                                                        <option value="${sprint.idSprint}">${sprint.numero}</option>
                                                                    </c:forEach>
                                                                </select>

                                                                <br/>

                                                                <label for="etudiant" class="control-label">Etudiants
                                                                    à supprimer : <span class="requis">*</span>
                                                                </label><br>
                                                                <c:forEach var="etudiant"
                                                                           items="${listeEtuDisp}" varStatus="loop">
                                                                    <input type="checkbox"
                                                                           name="checkbox-${loop.index}"
                                                                           value="${etudiant.idUtilisateur}"> ${etudiant.nom} ${etudiant.prenom}
                                                                    <br>
                                                                </c:forEach>

                                                                <br>

                                                                <div class="modal-footer">
                                                                    <div class="btn-group btn-group-justified"
                                                                         role="group"
                                                                         aria-label="group button">
                                                                        <div class="btn-group" role="group">
                                                                            <button type="button"
                                                                                    class="btn btn-danger"
                                                                                    data-dismiss="modal"
                                                                                    role="button">&nbsp; Fermer
                                                                            </button>
                                                                        </div>
                                                                        <div class="btn-group" role="group">
                                                                            <button type="submit" id="submit"
                                                                                    class="btn btn-primary btn-hover-green"
                                                                                    data-action="save"
                                                                                    role="button">
                                                                                &nbsp; Supprimer
                                                                            </button>
                                                                            <input type="hidden" name="formulaire"
                                                                                   id="formulaire"
                                                                                   value="supprimerMembreEquipeI2"/>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- /Fenêtre POP-UP --><!-- Fenêtre Ajouter membres équipes -->
                                        <div id="modalAjouterMembre" class="modal fade" role="dialog"
                                             aria-labelledby="popup1" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header  modal-header bg-success">
                                                        <h2 class="modal-title" id="exampleModalLabel">...</h2>
                                                        <button type="button" class="close" data-dismiss="modal"
                                                                aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <form data-toggle="validator" role="form" method="post"
                                                              action="<c:url value="/CreerEquipesI2"/>">
                                                            <div class="form-group">
                                                                <input type="hidden" class="form-control" id="idEquipe"
                                                                       name="idEquipe">

                                                                <label for="anneeScolaireEquipe" class="control-label">Pour
                                                                    et à partir du sprint:
                                                                    <span class="requis">*</span>
                                                                </label><br>
                                                                <%-- Choix du sprint --%>
                                                                <select class="form-control" id="sprintSelect"
                                                                        name="sprintSelect">
                                                                    <c:forEach var="sprint" items="${listeSprintDAO}">
                                                                        <option value="${sprint.idSprint}">${sprint.numero}</option>
                                                                    </c:forEach>
                                                                </select>

                                                                <br/>

                                                                <label class="control-label">Etudiants
                                                                    disponibles
                                                                    : <span class="requis">*</span>
                                                                </label><br>
                                                                <c:if test="${!empty listeEtudiantsI2Dispos}">
                                                                    <c:forEach var="etudiant"
                                                                               items="${listeEtudiantsI2Dispos}"
                                                                               varStatus="loop">
                                                                        <input type="checkbox"
                                                                               name="checkbox-${loop.index}"
                                                                               value="${etudiant.idUtilisateur}"> ${etudiant.nom} ${etudiant.prenom}
                                                                        <br>
                                                                    </c:forEach>
                                                                </c:if>
                                                                <c:if test="${empty listeEtudiantsI2Dispos}">
                                                                    <p align="center">Aucun étudiant de disponible.</p>
                                                                </c:if>
                                                                <br>

                                                                <div class="modal-footer">
                                                                    <div class="btn-group btn-group-justified"
                                                                         role="group"
                                                                         aria-label="group button">
                                                                        <div class="btn-group" role="group">
                                                                            <button type="button" class="btn btn-danger"
                                                                                    data-dismiss="modal"
                                                                                    role="button">&nbsp; Fermer
                                                                            </button>
                                                                        </div>
                                                                        <div class="btn-group" role="group">
                                                                            <button type="submit" id="submit"
                                                                                    class="btn btn-primary btn-hover-green"
                                                                                    data-action="save" role="button">
                                                                                &nbsp; Ajouter
                                                                            </button>
                                                                            <input type="hidden" name="formulaire"
                                                                                   id="formulaire"
                                                                                   value="ajouterMembreEquipeI2"/>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- /Fenêtre POP-UP -->

                                    </div>
                                </div>
                            </div>

                            <!-- Options de validation definitive et d'envoie -->
                            <div class="panel-footer" style="padding-bottom: 10">
                                <div class="row">
                                    <div style="text-align:center;">


                                        <div id=creationEquipe class="col-md-2 col-md-5">
                                            <input type="hidden" id="creerEquipe" name="creerEquipe"
                                                   value="">
                                            <a type="button" name="btn" href="#modalCreationEquipe"
                                               data-toggle="modal"
                                               data-target="#modalCreationEquipe"
                                               class="btn btn-primary" id="modifier">
                                                <i class="fas fa-plus fa-fw"></i>
                                                &nbsp; Créer équipe &nbsp;
                                            </a>
                                        </div>
                                        
                                        <c:if test="${etudiant eq 'true'}">
                                            <div id=ajouterScrumMaster class="col-md-2 col-md-offset-2">
                                                <input type="hidden" id="scrumMaster" name="scrumMaster"
                                                       value="">
                                                <a type="button" name="btn" href="#modalScrumMaster"
                                                   data-toggle="modal"
                                                   data-target="#modalScrumMaster"
                                                   class="btn btn-info" id="btnModalScrumMaster">
                                                    <i class="fab fa-product-hunt fa-fw"></i>&nbsp; Affecter Scrum Master
                                                </a>
                                            </div>
                                        </c:if>

                                        <c:if test="${prof eq 'true' || profResponsable eq 'true'|| profReferent eq 'true'}">
                                            <div id=ajouterProductOwner class="col-md-2 col-md-offset-1">
                                                <input type="hidden" id="productOwner" name="productOwner"
                                                       value="">
                                                <a type="button" name="btn" href="#modalProductOwner"
                                                   data-toggle="modal"
                                                   data-target="#modalProductOwner"
                                                   class="btn btn-info" id="btnModalProductOwner">
                                                    <i class="fab fa-product-hunt fa-fw"></i>&nbsp; Affecter P.O
                                                </a>
                                            </div>


                                            <div id=modificationMembre class="col-md-2 col-md-2">
                                                <input type="hidden" id="transfererMembre" name="transfererMembre"
                                                       value="">
                                                <a type="button" name="btn" href="#modalModificationMembre"
                                                   data-toggle="modal"
                                                   data-target="#modalModificationMembre"
                                                   class="btn btn-warning" id="btnTransfererMembre">
                                                    <i class="fas fa-exchange-alt fa-fw"></i>&nbsp; Transferer un membre
                                                </a>
                                            </div>
                                        </c:if>
                                    </div>
                                </div>
                            </div>

                            <!-- Fenêtre Modif membres des équipes -->
                            <div id="modalModificationMembre" class="modal fade" role="dialog"
                                 aria-labelledby="popup2" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header  modal-header bg-warning">
                                            <h2>Transferer un ou plusieurs étudiants </h2>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <form data-toggle="validator" role="form" method="post"
                                                  action="<c:url value="/CreerEquipesI2"/>">
                                                <div class="form-group">

                                                    <label for="anneeScolaireEquipe" class="control-label">Equipe
                                                        de destination
                                                        <span class="requis">*</span>
                                                    </label><br>
                                                    <%-- Choix de l'equipe de destination de l'utilisateur--%>
                                                    <select class="form-control" id="equipeEquipe" name="equipeEquipe">
                                                        <c:forEach items="${listeEquipe}" var="equipe">
                                                            <option value="${equipe.idEquipe}">${equipe.nom}</option>
                                                        </c:forEach>
                                                    </select>

                                                    <br/>


                                                    <label for="anneeScolaireEquipe" class="control-label">Pour
                                                        et à partir du sprint:
                                                        <span class="requis">*</span>
                                                    </label><br>
                                                    <%-- Choix du sprint --%>
                                                    <select class="form-control" id="sprintSelect"
                                                            name="sprintSelect">
                                                        <c:forEach var="sprint" items="${listeSprintDAO}">
                                                            <option value="${sprint.idSprint}">${sprint.numero}</option>
                                                        </c:forEach>
                                                    </select>

                                                    <br/>

                                                    <label class="control-label">Etudiants disponibles
                                                        : <span class="requis">*</span>
                                                    </label><br>
                                                    <c:if test="${!empty listeEtuDisp}">
                                                        <c:forEach var="etudiant" items="${listeEtuDisp}"
                                                                   varStatus="loop">
                                                            <input type="checkbox" name="checkbox-${loop.index}"
                                                                   value="${etudiant.idUtilisateur}"> ${etudiant.nom} ${etudiant.prenom}
                                                            <br>
                                                        </c:forEach>
                                                    </c:if>
                                                    <c:if test="${empty listeEtuDisp}">
                                                        <p align="center">Aucun étudiant de disponible.</p>
                                                    </c:if>
                                                    <br>

                                                    <div class="modal-footer">
                                                        <div class="btn-group btn-group-justified" role="group"
                                                             aria-label="group button">
                                                            <div class="btn-group" role="group">
                                                                <button type="button" class="btn btn-danger"
                                                                        data-dismiss="modal"
                                                                        role="button">&nbsp; Fermer
                                                                </button>
                                                            </div>
                                                            <div class="btn-group" role="group">
                                                                <button type="submit" id="submit"
                                                                        class="btn btn-primary btn-hover-green"
                                                                        data-action="save" role="button">&nbsp;
                                                                    Enregistrer
                                                                </button>
                                                                <input type="hidden" name="formulaire" id="formulaire"
                                                                       value="modifierMembreEquipeI2"/>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <!-- Fenêtre modalProductOwner des équipes -->
                            <div id="modalProductOwner" class="modal fade" role="dialog"
                                 aria-labelledby="popup2" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header  modal-header bg-aqua">
                                            <h2>Affecter un ou plusieurs Product Owner(s) à une équipe </h2>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <form data-toggle="validator" role="form" method="post"
                                                  action="<c:url value="/CreerEquipesI2"/>">
                                                <div class="form-group">

                                                    <label for="equipeEquipe" class="control-label">Equipe
                                                        d'affectation :
                                                        <span class="requis">*</span>
                                                    </label><br>
                                                    <%-- Choix de l'equipe de destination de l'utilisateur--%>
                                                    <select class="form-control" id="equipeEquipe" name="equipeEquipe">
                                                        <c:forEach items="${listeEquipe}" var="equipe">
                                                            <option value="${equipe.idEquipe}">${equipe.nom}</option>
                                                        </c:forEach>
                                                    </select>

                                                    <br/>

                                                    <label class="control-label">Professeurs disponibles pour être
                                                        Product Owner d'une équipe
                                                        : <span class="requis">*</span>
                                                    </label><br>
                                                    <div class="text-justify">
                                                        <c:if test="${!empty listeProf}">
                                                            <c:forEach var="etudiant" items="${listeProf}"
                                                                       varStatus="loop">
                                                                <input type="checkbox" name="checkbox-${loop.index}"
                                                                       value="${etudiant.idUtilisateur}"> ${etudiant.nom} ${etudiant.prenom}
                                                                <br>
                                                            </c:forEach>
                                                        </c:if>
                                                        <c:if test="${empty listeProf}">
                                                            <p align="center">Aucun PO de disponible.</p>
                                                        </c:if>
                                                    </div>

                                                    <br>

                                                    <div class="modal-footer">
                                                        <div class="btn-group btn-group-justified" role="group"
                                                             aria-label="group button">
                                                            <div class="btn-group" role="group">
                                                                <button type="button" class="btn btn-danger"
                                                                        data-dismiss="modal"
                                                                        role="button">&nbsp; Fermer
                                                                </button>
                                                            </div>
                                                            <div class="btn-group" role="group">
                                                                <button type="submit" id="submit"
                                                                        class="btn btn-primary btn-hover-green"
                                                                        data-action="save" role="button">&nbsp;
                                                                    Enregistrer
                                                                </button>
                                                                <input type="hidden" name="formulaire"
                                                                       id="formulaire"
                                                                       value="affecterProductOwnerEquipeI2"/>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <!-- Fenêtre modalScrumMaster des équipes -->
                            <div id="modalScrumMaster" class="modal fade" role="dialog"
                                 aria-labelledby="popup2" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header  modal-header bg-aqua">
                                            <h2>Affecter un Scrum Master à une équipe </h2>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <form data-toggle="validator" role="form" method="post"
                                                  action="<c:url value="/CreerEquipesI2"/>">
                                                <div class="form-group">

                                                    <label for="equipeEquipe" class="control-label">Equipe
                                                        d'affectation :
                                                        <span class="requis">*</span>
                                                    </label><br>
                                                    <%-- Choix de l'equipe de destination de l'utilisateur--%>
                                                    <select class="form-control" id="equipeEquipe" name="equipeEquipe">
                                                        <c:forEach items="${listeEquipe}" var="equipe">
                                                            <option value="${equipe.idEquipe}">${equipe.nom}</option>
                                                        </c:forEach>
                                                    </select>

                                                    <br/>

                                                    <label class="control-label">Etudiants disponibles pour être
                                                        Scrum Master d'une équipe
                                                        : <span class="requis">*</span>
                                                    </label><br>
                                                    <div class="text-justify">
                                                        <c:if test="${!empty listeEtudiantsI2}">
                                                            <c:forEach var="etudiant" items="${listeEtudiantsI2}"
                                                                       varStatus="loop">
                                                                <input type="checkbox" name="checkbox-${loop.index}"
                                                                       value="${etudiant.idUtilisateur}"> ${etudiant.nom} ${etudiant.prenom}
                                                                <br>
                                                            </c:forEach>
                                                        </c:if>
                                                        <c:if test="${empty listeEtudiantsI2}">
                                                            <p align="center">Aucun étudiant de disponible.</p>
                                                        </c:if>
                                                    </div>

                                                    <br>

                                                    <div class="modal-footer">
                                                        <div class="btn-group btn-group-justified" role="group"
                                                             aria-label="group button">
                                                            <div class="btn-group" role="group">
                                                                <button type="button" class="btn btn-danger"
                                                                        data-dismiss="modal"
                                                                        role="button">&nbsp; Fermer
                                                                </button>
                                                            </div>
                                                            <div class="btn-group" role="group">
                                                                <button type="submit" id="submit"
                                                                        class="btn btn-primary btn-hover-green"
                                                                        data-action="save" role="button">&nbsp;
                                                                    Enregistrer
                                                                </button>
                                                                <input type="hidden" name="formulaire"
                                                                       id="formulaire"
                                                                       value="affecterScrumMasterEquipeI2"/>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- /Fenêtre POP-UP -->
                        </div><!-- Fenêtre Création des équipes -->
                        <div id="modalCreationEquipe" class="modal fade" role="dialog"
                             aria-labelledby="popup2" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header  modal-header bg-primary">
                                        <h2>Création d'une équipe</h2>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <form data-toggle="validator" role="form" method="post"
                                              action="<c:url value="/CreerEquipesI2"/>">
                                            <div class="form-group">

                                                <%-- Choix du nom--%>
                                                <label for="nomEquipe" class="control-label">Nom de l'équipe :
                                                    <span class="requis">*</span>
                                                </label><br>
                                                <input type="text" class="form-control" id="nomEquipe"
                                                       name="nomEquipe">
                                                <br>
                                                <label for="projetEquipe" class="control-label">Projet :
                                                    <span class="requis">*</span>
                                                </label><br>
                                                <%-- Choix du projet--%>
                                                <select class="form-control" id="projetEquipe" name="projetEquipe">
                                                    <c:forEach items="${listeProjetEquipe}" var="projet">
                                                        <option value="${projet.idProjet}">${projet.titre}</option>
                                                    </c:forEach>
                                                </select>

                                                <br/>

                                                <label for="anneeScolaireEquipe" class="control-label">Année :
                                                    <span class="requis">*</span>
                                                </label><br>
                                                <%-- Choix de l'année --%>
                                                <select class="form-control" id="anneeScolaireEquipe"
                                                        name="anneeScolaireEquipe">
                                                    <c:forEach var="annee" items="${listeAnneeScolaire}">
                                                        <option value="${annee.idAnneeScolaire}">${annee.anneeDebut}
                                                            / ${annee.anneeFin}</option>
                                                    </c:forEach>
                                                </select>

                                                <br/>


                                                <label class="control-label">Etudiants disponibles (sans équipe)
                                                    : <span class="requis">*</span>
                                                </label><br>
                                                <c:if test="${!empty listeEtudiantsI2Dispos}">
                                                    <c:forEach var="etudiant" items="${listeEtudiantsI2Dispos}"
                                                               varStatus="loop">
                                                        <input type="checkbox" name="checkbox-${loop.index}"
                                                               value="${etudiant.idUtilisateur}"> ${etudiant.nom} ${etudiant.prenom}
                                                        <br>
                                                    </c:forEach>
                                                </c:if>
                                                <c:if test="${empty listeEtudiantsI2Dispos}">
                                                    <p align="center">Aucun étudiant de disponible.</p>
                                                </c:if>
                                                <br/>


                                                <div class="modal-footer">
                                                    <div class="btn-group btn-group-justified" role="group"
                                                         aria-label="group button">
                                                        <div class="btn-group" role="group">
                                                            <button type="button" class="btn btn-danger"
                                                                    data-dismiss="modal"
                                                                    role="button">&nbsp; Fermer
                                                            </button>
                                                        </div>
                                                        <div class="btn-group" role="group">
                                                            <button type="submit" id="submit"
                                                                    class="btn btn-primary btn-hover-green"
                                                                    data-action="save" role="button">&nbsp;
                                                                Enregistrer
                                                            </button>
                                                            <input type="hidden" name="formulaire" id="formulaire"
                                                                   value="ajouterEquipeI2"/>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!-- /Fenêtre POP-UP -->
                </div>

            </div>
        </div>
    </div>
</div>
</div>
<c:import url="/inc/scripts.jsp"/>
<script type="application/javascript"
        src="<c:url value="/js/perfect-scrollbar.min.js"/>"></script>
<script type="text/javascript"
        src="<c:url value="/js/bootstrap-datetimepicker.min.js"/>"></script>
<script type="text/javascript"
        src="<c:url value="/js/bootstrap-datetimepicker.fr.js"/>"></script>
<script>
    typeof $.typeahead === 'function' && $.typeahead({
        input: ".js-typeahead",
        minLength: 1,
        maxItem: 5,
        order: "asc",
        hint: false,
        emptyTemplate: 'Aucun resultat pour "{{query}}"',
        source: data
    });
</script>

<script type="text/javascript">

    $('#modalModificationEquipe').on('shown.bs.modal', function (event) { // modification du show --> shown pour
        // eviter les conflits avec le datepicker
        var button = $(event.relatedTarget) // Button that triggered the modal
        var nomEquipe = button.data('titre')
        var idEquipe = button.data('idequipe')
        var membresEquipes = button.data('membres')
        // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
        // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
        var modal = $(this)
        modal.find('.modal-title').text("Modification des membres de l'équipe : " + nomEquipe);
        modal.find('.modal-body #idMembresEquipe').val(membresEquipes);
        modal.find('.modal-body #idEquipe').val(idEquipe);

        modal.find('.modal-body #sprint').val(sprint);
        //modal.find('.modal-body input').val(recipient)
    })
    $('#modalSuppressionEquipe').on('shown.bs.modal', function (event) { // modification du show --> shown pour
        // eviter les conflits avec le datepicker
        var button = $(event.relatedTarget) // Button that triggered the modal
        var nomEquipe = button.data('titre')
        var idEquipe = button.data('idequipe')
        // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
        // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
        var modal = $(this)
        modal.find('.modal-title').text("Suppression de l'équipe : " + nomEquipe);
        modal.find('.modal-body #idEquipeDelete').val(idEquipe);
    })

    $('#modalSupprimerMembre').on('shown.bs.modal', function (event) { // modification du show --> shown pour
        // eviter les conflits avec le datepicker
        var button = $(event.relatedTarget) // Button that triggered the modal
        var nomEquipe = button.data('titre')
        var idEquipe = button.data('idequipe')
        var membresEquipes = button.data('membres')
        // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
        // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
        var modal = $(this)
        modal.find('.modal-title').text("Supprimer membres de l'équipe : " + nomEquipe);
        modal.find('.modal-body #idMembresEquipe').val(membresEquipes);
        modal.find('.modal-body #idEquipe').val(idEquipe);

        modal.find('.modal-body #sprint').val(sprint);
        //modal.find('.modal-body input').val(recipient)
    })

    $('#modalAjouterMembre').on('shown.bs.modal', function (event) { // modification du show --> shown pour
        // eviter les conflits avec le datepicker
        var button = $(event.relatedTarget) // Button that triggered the modal
        var nomEquipe = button.data('titre')
        var idEquipe = button.data('idequipe')
        var membresEquipes = button.data('membres')
        // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
        // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
        var modal = $(this)
        modal.find('.modal-title').text("Ajouter membres à l'équipe : " + nomEquipe);
        modal.find('.modal-body #idMembresEquipe').val(membresEquipes);
        modal.find('.modal-body #idEquipe').val(idEquipe);

        modal.find('.modal-body #sprint').val(sprint);
        //modal.find('.modal-body input').val(recipient)
    })
</script>

</body>
</html>