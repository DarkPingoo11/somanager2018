<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-body">
            <div class="alert alert-danger" role="alert">
                <h4 class="alert-heading">Attention!</h4>
                <p>
                    Etes-vous sûr de vouloir supprimer le compte de <strong data-replace="nomUtilisateur"></strong>
                </p>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-primary" data-dismiss="modal">Annuler</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal" data-action="supprimerCompteUtilisateur">Supprimer</button>
        </div>
    </div>
</div>