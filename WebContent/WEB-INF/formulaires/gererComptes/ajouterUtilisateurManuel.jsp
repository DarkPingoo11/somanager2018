<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<div class="col-sm-12">
    <form data-toggle="validator" role="form" method="post" action="<c:url value="/GererComptes"/>">
        <%-- Première colonne --%>
        <div class="col-sm-6">
            <%-- Nom --%>
            <div class="form-group has-feedback">
                <label for="nom" class="control-label">Nom : <span
                        class="requis">*</span></label> <input type="text" name="nom"
                                                               class="form-control" id="nom" required /> <span
                    class="glyphicon form-control-feedback" aria-hidden="true"></span>
                <div class="help-block with-errors"></div>
            </div>

            <%-- Prenom --%>
            <div class="form-group has-feedback">
                <label for="prenom" class="control-label">Prenom :<span
                        class="requis">*</span></label> <input type="text" name="prenom"
                                                               class="form-control" id="prenom" required /> <span
                    class="glyphicon form-control-feedback" aria-hidden="true"></span>
                <div class="help-block with-errors"></div>
            </div>

            <%-- Mot de passe --%>
            <div class="form-group has-feedback">
                <label for="motDePasse" class="control-label">Mot de
                    passe : <span class="requis">*</span>
                </label> <input type="password" name="motDePasse"
                                class="form-control" id="motDePasse" required /> <span
                    class="glyphicon form-control-feedback" aria-hidden="true"></span>
                <div class="help-block with-errors"></div>
            </div>
        </div>

        <%-- Deuxième colonne --%>
        <div class="col-sm-6">
            <%-- Identifiant --%>
            <div class="form-group has-feedback">
                <label for="identifiant" class="control-label">Identifiant
                    : <span class="requis">*</span>
                </label> <input type="text" name="identifiant" class="form-control"
                                id="identifiant" required /> <span
                    class="glyphicon form-control-feedback" aria-hidden="true"></span>
                <div class="help-block with-errors"></div>
            </div>

            <%-- Email --%>
            <div class="form-group has-feedback">
                <label for="email" class="control-label">Email : <span
                        class="requis">*</span></label>
                <div class="input-group">
                    <span class="input-group-addon">@</span> <input
                        type="email" name="email" class="form-control" id="email"
                        required />
                </div>
                <span class="glyphicon form-control-feedback"
                      aria-hidden="true"></span>
                <div class="help-block with-errors"></div>
            </div>

            <%-- Génération automatique de mot de passe --%>
            <div style="margin-top: 40px">
                    <c:import url="/WEB-INF/formulaires/gererComptes/generationMotDePasse.jsp" />
            </div>

        </div>

        <%-- Derniers elements du formulaire --%>
        <input type="hidden" name="formulaire" id="formulaire" value="ajouterUtilisateur" />

        <div class="text-center">
            <c:choose>
                <c:when test="${empty erreurs}">
                    <span class="succes">${resultat}</span>
                </c:when>
                <c:otherwise>
                    <span class="erreur">
                        ${resultat}
                        ${erreurs['identifiant']}
                        ${erreurs['email']}
                    </span>
                </c:otherwise>
            </c:choose>
        </div>

        <br>

        <div class="col-lg-12 col-md-12">
            <button type="submit" class="btn btn-primary center-block" id="submit">
                <i class="fas fa-plus fa-fw" aria-hidden="true"></i>&nbsp;Inscription
            </button>
        </div>
    </form>
</div>

<%-- Resultat de l'ajout --%>
<c:if test="${(!empty nombreInsertion)||(!empty nombreEchec)}">
    <div class="col-lg-12">
        <div class="panel panel-default ">
            <div class="panel-heading">Résultat du précédent import</div>
            <div class="panel-body">

                <ul class="list-group">
                    <li
                            class="list-group-item list-group-item-action list-group-item-success justify-content-between">
                        Nombre d'insertions <span
                            class="badge badge-default badge-pill">${nombreInsertion}</span>
                    </li>
                    <li
                            class="list-group-item list-group-item-action list-group-item-danger justify-content-between">
                        Nombre d'échecs <span class="badge badge-default badge-pill">${nombreEchec}</span>

                        <div class="panel-group">
                            <br>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <div class="panel-title ">
                                        <a style="font-size: 14px" data-toggle="collapse"
                                           href="#collapse1">Liste des erreurs <span
                                                class="glyphicon glyphicon-chevron-down pull-right"></span>
                                        </a>
                                    </div>
                                </div>
                                <div id="collapse1" class="panel-collapse collapse">
                                    <ul class="list-group">
                                        <c:forEach var="erreurImport"
                                                   items="${erreursImportation}">
                                            <li class="list-group-item list-group-item-default"><strong>Erreur
                                                :</strong>
                                                <p style="overflow: hidden; text-overflow: ellipsis;">${erreurImport}</p></li>
                                        </c:forEach>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</c:if>
<!-- /.panel -->