<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<div class="col-sm-12">
    <div class="panel panel-primary" style="max-height: 100%;">
        <div class="panel-heading" style="padding-bottom: 0">
            <div class="row">
                <!-- Filtre nomUtilisateur -->
                <div class="col-sm-4">
                    <div class="form-group full-width">
                        <label for="nomUtilisateur">Utilisateur : </label>
                        <input type="text" class="form-control" id="nomUtilisateur"
                               title="Nom ou prenom de l'utilisateur" placeholder="Rechercher un utilisateur"
                               data-action="rechercherUtilisateur">
                    </div>
                </div>
                <!-- Filtres -->
                <div class="col-sm-6 no-padding">
                    <!-- Filtre du rôle -->
                    <div class="col-sm-4">
                        <div class="form-group full-width">
                            <label for="roleUtilisateur">Rôle : </label>
                            <select id="roleUtilisateur" class="form-control input-sm selectpicker" style="text-transform: capitalize"
                                    data-action="rechercherUtilisateur">
                                <option value="-1" selected>Tous</option>
                                <c:forEach var="role" items="${ requestScope.rolesDispo }">
                                    <option data-icon="fa-${ role.icone }" value="${ role.idRole }">${ role.nomComplet }</option>
                                </c:forEach>
                            </select>
                        </div>
                    </div>

                    <!-- Filtre année scolaire -->
                    <div class="col-sm-4">
                        <div class="form-group full-width">
                            <label for="anneeUtilisateur">Année scolaire : </label>
                            <select id="anneeUtilisateur" class="input-sm form-control selectpicker" data-action="rechercherUtilisateur">
                                <option value="-1" selected>Tous</option>
                                <c:forEach var="annee" items="${requestScope.annees}">
                                    <option value="${ annee.anneeDebut }">${annee.anneeDebut} - ${annee.anneeFin}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </div>

                    <!-- Filtre validité -->
                    <div class="col-sm-4">
                        <div class="form-group full-width">
                            <label for="compteValide">Comptes : </label>
                            <select id="compteValide" class="input-sm form-control selectpicker" data-action="rechercherUtilisateur">
                                <option value="-1" selected>Tous</option>
                                <option data-icon="fa-check-circle text-green" value="oui">Valide</option>
                                <option data-icon="fa-clock text-orange" value="non">A valider</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="col-sm-2">
                    <div class="form-group full-width">
                        <label>&nbsp;</label>
                        <button class="btn btn-block form-control" data-action="rechercherUtilisateur">
                            <i class="fa fa-search fa-fw" aria-hidden="true"></i>&nbsp;
                            Rechercher
                        </button>
                    </div>
                </div>
                <!-- /Filtres -->
            </div>
        </div>
        <!-- /.panel-header -->

        <!-- Tableau de résultats -->
        <div class="panel-body table-container">
            <table class="table table-bordered" style="margin-bottom:-1px">
                <tr class="info unselectable">
                    <th><input type="checkbox" title="Tout selectionner" name="selectionnerComptes" id="selectionnerTout"></th>
                    <th class="col-sm-2" data-col="nom" role="button" data-action="rechercherUtilisateur">Nom
                    </th>
                    <th class="col-sm-2" data-col="code" role="button" data-action="rechercherUtilisateur">Prenom
                    </th>
                    <th class="col-sm-2" data-col="nope" role="button" data-action="rechercherUtilisateur">Identifiant
                    </th>
                    <th class="col-sm-2" data-col="nope" role="button" data-action="rechercherUtilisateur">Rôles
                    </th>
                    <th class="col-sm-2" data-col="nope" role="button" data-action="rechercherUtilisateur">Année scolaire
                    </th>
                    <th class="col-sm-2">Actions
                    </th>
                </tr>
            </table>

            <%-- Tableau remplis par JS --%>
            <div class="row">
                <div class="col-sm-12" id="listeUtilisateursContainer" style="height: 300px; overflow-y: scroll">
                    <table class="table table-hover" style="margin-bottom: 0">
                        <tbody id="listeUtilisateurs">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- /.panel-body -->

        <!-- Options de recherche -->
        <div class="panel-footer" style="padding-bottom: 0">
            <div class="row">
                <div class="col-sm-12 no-padding">
                    <!-- Actions de groupe -->
                    <div class="col-sm-2">
                        <div class="dropdown full-width">
                            <button class="btn btn-primary dropdown-toggle btn-block" type="button" data-toggle="dropdown">Action
                                <span class="caret"></span></button>
                            <ul class="dropdown-menu">
                                <li>
                                    <a role="button" data-action="supprimerTousComptes">
                                        <i class="fas fa-trash-alt text-red fa-fw" aria-hidden="true"></i>&nbsp;
                                        Supprimer les comptes
                                    </a>
                                </li>
                                <li>
                                    <a role="button" data-action="suspendreTousComptes">
                                        <i class="fas fa-ban text-warning fa-fw" aria-hidden="true"></i>&nbsp;
                                        Suspendre les comptes
                                    </a>
                                </li>
                                <li>
                                    <a role="button" data-action="validerTousComptes">
                                        <i class="fas fa-check-circle text-green fa-fw" aria-hidden="true"></i>&nbsp;
                                        Valider les comptes
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <!-- Résultats par page -->
                    <div class="col-sm-3">
                        <div class="form-group full-width">
                            <select id="nombre" class="form-control input-sm selectpicker" style="text-transform: capitalize"
                                    data-action="changerNbRequetes" title="Nombre de résultats par page">
                                <option value="20" selected>20 résultats par page</option>
                                <option value="50">50 résultats par page</option>
                                <option value="100">100 résultats par page</option>
                                <option value="200">200 résultats par page</option>
                            </select>
                        </div>
                    </div>

                    <!-- Résultats totaux -->
                    <div class="col-sm-3">
                        <label class="form-control" readonly="">
                            <span id="resultatsTot">?</span> en tout
                        </label>
                    </div>

                    <!-- -->
                    <div class="col-sm-1"></div>

                    <!-- Numéro de page -->
                    <div class="col-sm-4" style="text-align: right;">
                        <button class="btn btn-primary" data-action="premierePage">
                            <i class="fas fa-step-backward fa-fw" aria-hidden="true"></i>&nbsp;
                        </button>
                        <button class="btn btn-primary" data-action="decrementerPage">
                            <i class="fas fa-caret-left fa-fw" aria-hidden="true"></i>&nbsp;
                        </button>
                        <strong style="padding: 0 5px 0 5px">
                            Page <span class="" id="pageCourante">1</span>
                        </strong>
                        <button class="btn btn-primary" data-action="incrementerPage">
                            <i class="fas fa-caret-right fa-fw" aria-hidden="true"></i>&nbsp;
                        </button>
                        <button class="btn btn-primary" data-action="dernierePage">
                            <i class="fas fa-step-forward fa-fw" aria-hidden="true"></i>&nbsp;
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /.panel -->