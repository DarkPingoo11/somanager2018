<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<div class="col-sm-12">
    <div class="alert alert-info" role="alert">
        <p align="center" style="font-size: 12px">
            Le fichier utilisé doit être de type *.csv (valeurs séparées par le caractère ";")
        </p>
        <p align="center" style="font-size: 12px">
            Chaque ligne doit être sous la forme :
            <strong>identifiant;mail@reseau.eseo.fr;NOM;Prénom;motDePasse</strong>
        </p>
    </div>
    <form action="<c:url value="/GererComptes"/>" method="post" enctype="multipart/form-data">
        <input type="hidden" name="formulaire" id="formulaire" value="defaut" />
        <label for="fichier">
            Emplacement du fichier <span class="requis">*</span>
        </label>
        <input type="file" id="fichier" name="fichier" />
        <br />

        <div class="text-center">
            <span class="erreur">${ erreurs['extensionErreur'] }</span>
        </div>
        <br>

        <button type="submit" class="btn btn-primary center-block" id="submit">
            <i class="fas fa-upload fa-fw" aria-hidden="true"></i>&nbsp;
            Importer
        </button>
    </form>

    <hr width="100%">
    <form action="<c:url value="/GererComptes"/>" method="post">
        <input type="hidden" name="formulaire" id="formulaire" value="choixPreferenceAjout" />
        <label for="fichier">
            Emplacement de l'importation : <span class="requis">*</span>
        </label>
        <div class="btn-group center-block" data-toggle="buttons">
            <c:choose>
                <c:when test="${choixImportCompte}">
                    <label class="btn btn-primary active"> <input
                            type="radio" name="optradio" id="option1"
                            autocomplete="off" value="true" checked>Annuaire
                    </label>
                    <label class="btn btn-primary"> <input
                            type="radio" name="optradio" id="option2"
                            autocomplete="off" value="false">Base De Données
                    </label>
                </c:when>
                <c:otherwise>
                    <label class="btn btn-primary"> <input
                            type="radio" name="optradio" id="option1"
                            autocomplete="off" value="true">Annuaire
                    </label>
                    <label class="btn btn-primary active"> <input
                            type="radio" name="optradio" id="option2"
                            autocomplete="off" value="false"checked">Base De
                        Données
                    </label>
                </c:otherwise>
            </c:choose>
        </div>
        <button type="submit" class="btn btn-primary center-block" id="submit">
            <i class="fas fa-check-circle fa-fw" aria-hidden="true"></i>&nbsp; Valider
        </button>
    </form>
</div>