<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<div class="col-sm-12">
    <form method="post" action="<c:url value="/GererComptes"/>">
        <br />
        <input type="hidden" name="formulaire" id="formulaire" value="synchroniserBDD" />
        <button type="submit" class="btn btn-primary center-block" id="submit">
            <i class="fas fa-redo fa-fw" aria-hidden="true"></i>&nbsp;
            Synchroniser
        </button>
    </form>
</div>