<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <i class="fas fa-key fa-fw" aria-hidden="true"></i>&nbsp;
            Changer le mot de passe de <strong class="text-primary" data-replace="nomUtilisateur"></strong>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-sm-12">
                    <form data-toggle="validator" role="form" method="post" onsubmit="return checkForm(this)"
                          action="<c:url value="ChangerMotDePasseAdmin"/>"
                          id="ChangerMotDePasseAdmin">
                        <fieldset>
                            <div class="form-group has-feedback">
                                <label for="nom" class="control-label">Identifiant
                                    : <span class="requis">*</span>
                                </label>
                                <input name="identifiant" class="form-control"
                                       id="identifiant" type="text" readonly/> <span
                                    class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                <div class="help-block with-errors"></div>
                            </div>

                            <div class="form-group has-feedback">
                                <label for="motDePasse" class="control-label">Nouveau
                                    mot de passe : <span class="requis">*</span>
                                </label>
                                <input type="password" name="motDePasse"
                                       class="form-control" id="motDePasse1" required />
                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                <div class="help-block with-errors"></div>
                            </div>

                            <div class="form-group has-feedback">
                                <label for="motDePasse" class="control-label">Retaper
                                    le mot de passe : <span class="requis">*</span>
                                </label> <input type="password" name="nouveauMDP2"
                                                class="form-control"
                                                data-match-error="Les deux mots de passe ne concordent pas"
                                                data-match="#motDePasse1" id="nouveauMDP2" required /> <span
                                    class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="text-center">
                                <c:choose>
                                    <c:when test="${empty erreursMdp}">
                                        <span class="succes">${resultatMdp}</span>
                                    </c:when>
                                    <c:otherwise>
                                            <span class="erreur">${resultatMdp} <!--  ${erreurs['motDePasse']}-->
                                                ${erreursMdp['utilisateur']}
                                            </span>
                                    </c:otherwise>
                                </c:choose>
                            </div>
                        </fieldset>

                        <%-- Génération automatique de mot de passe --%>
                        <c:import url="/WEB-INF/formulaires/gererComptes/generationMotDePasse.jsp" />

                    </form>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-primary" data-dismiss="modal">Annuler</button>
            <button type="button" class="btn btn-success" data-action="changerPwUtilisateur">
                <i class="fas fa-key fa-fw" aria-hidden="true"></i>&nbsp;
                Modifier le mot de passe
            </button>
        </div>
    </div>
</div>