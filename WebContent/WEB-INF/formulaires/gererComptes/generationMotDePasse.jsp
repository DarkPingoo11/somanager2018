<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%-- Génération automatique de mot de passe --%>
<div class="col-sm-8 no-padding">
    <button type="button" class="btn btn-warning" data-action="generatePassword">
        <i class="fas fa-random fa-fw" aria-hidden="true"></i>&nbsp;
        Générer un mot de passe aléatoirement
    </button>
</div>
<div class="col-sm-4">
    <input type="text" class="form-control" data-replace="generatedPassword"
           title="Mot de passe généré" readonly>
</div>
<%-- ./Génération automatique de mot de passe --%>