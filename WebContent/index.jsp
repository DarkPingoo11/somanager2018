<%--
  Created by IntelliJ IDEA.
  User: dimitrijarneau
  Date: 10/06/2018
  Time: 15:09
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <c:import url="/inc/head.jsp"/>
    <title>SoManager - Accueil</title>

</head>
<body>


<div class="wrapper">
    <c:import url="/Navbar"/>
    <!-- Page Content Holder -->
    <div id="content">
        <c:import url="/inc/headerForSidebar.jsp"/>

        <div class="panel panel-primary ">
            <div class="panel-heading">Bienvenue sur l'application
                SoManager
            </div>
            <div class="panel-body">
                <p>
                    Cette application vous permet, selon vos droits et devoirs,
                    d'effectuer différentes actions en rapport avec les projets de fin
                    d'étude de l'ESEO.<br>Connectez vous et accédez aux
                    différentes fonctionnalités de l'application via la barre de
                    navigation ci-dessus.
                </p>
                <br>
                <div class="col-lg-6 col-md-6">
                    <div class="panel panel-default ">
                        <div class="panel-heading">
                            <center>
                                <i class="fas fa-graduation-cap fa-fw" aria-hidden="true"></i>&nbsp;Equipe
                                de développement 2017-2018
                            </center>
                        </div>
                        <div class="panel-body">
                            <br><br>
                            <div class="row">
                                <div class="col-lg-3">Juan Carlo ORTIZ BETANCUR</div>
                                <div class="col-lg-3">Dimitri JARNEAU</div>
                                <div class="col-lg-3">Tristan LE GACQUE</div>
                                <div class="col-lg-3">Emilien MAMALET</div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-lg-3">Damien MAYMARD</div>
                                <div class="col-lg-3">Quentin PICHAVANT</div>
                                <div class="col-lg-3">Gaetan SCUILLER</div>
                                <div class="col-lg-3">Anne Claire VERGOTE</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6">
                    <div class="panel panel-default ">
                        <div class="panel-heading">
                            <center>
                                <i class="fas fa-graduation-cap fa-fw" aria-hidden="true"></i>&nbsp;Equipe
                                de développement 2016-2017
                            </center>
                        </div>
                        <div class="panel-body">
                            <br><br>
                            <div class="row">
                                <div class="col-lg-3">Julie AVIZOU</div>
                                <div class="col-lg-3">Pierre CESMAT</div>
                                <div class="col-lg-3">Alexandre CLAMENS</div>
                                <div class="col-lg-3">Renaud DESCOURS</div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-lg-3">Maxime LENORMAND</div>
                                <div class="col-lg-3">Hugo MÉNARD</div>
                                <div class="col-lg-3">Thomas MÉNARD</div>
                                <div class="col-lg-3">Cécilia PINSARD</div>
                            </div>
                            <br/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<c:import url="/inc/scripts.jsp"/>
</body>
</html>
