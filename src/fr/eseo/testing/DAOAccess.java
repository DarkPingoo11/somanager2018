package fr.eseo.testing;

import fr.eseo.ld.beans.*;
import fr.eseo.ld.beans.Equipe;
import fr.eseo.ld.beans.Etudiant;
import fr.eseo.ld.beans.EtudiantEquipe;
import fr.eseo.ld.beans.Soutenance;
import fr.eseo.ld.pgl.beans.*;

/**
 * Comprends des méthodes pour acceder aux objets communs pour les DAO
 */
public class DAOAccess {

    private static final Integer ANNEE_COURANTE = 2017;
    private static final String EQUIPEID = ANNEE_COURANTE + "_001";
    private static final Long ID_DEFAULT = 1L;
    private static final String JURYID = ANNEE_COURANTE + "_001";

    DAOAccess() {
    }

    /**
     * Ecrire toutes les méthodes d'accès aux objets
     */

    public JuryPoster getJuryPoster() {
        JuryPoster juryPoster = new JuryPoster();
        juryPoster.setIdProf1(ID_DEFAULT);
        juryPoster.setIdProf2(ID_DEFAULT);
        juryPoster.setIdProf3(ID_DEFAULT);
        juryPoster.setIdEquipe(EQUIPEID);
        juryPoster.setIdJuryPoster(JURYID);
        return juryPoster;
    }

    public JurySoutenance getJurySoutenance() {
        JurySoutenance jurySoutenance = new JurySoutenance();
        jurySoutenance.setIdProf1(ID_DEFAULT);
        jurySoutenance.setIdProf2(ID_DEFAULT);
        jurySoutenance.setIdJurySoutenance(JURYID);
        return jurySoutenance;
    }

    public Poster getPoster() {
        Poster poster = new Poster();
        poster.setChemin("chemin");
        poster.setValide("oui");
        poster.setIdSujet(ID_DEFAULT);
        poster.setIdPoster(ID_DEFAULT);
        poster.setIdEquipe(EQUIPEID);
        poster.setDatePoster("2018-05-05 12:11:00.0");
        return poster;
    }

    public AnneeScolaire getAnneeScolaire() {
        AnneeScolaire anneeScolaire = new AnneeScolaire();
        anneeScolaire.setAnneeDebut(ANNEE_COURANTE);
        anneeScolaire.setAnneeFin(ANNEE_COURANTE+1);
        anneeScolaire.setIdAnneeScolaire(ID_DEFAULT);
        anneeScolaire.setIdOption(ID_DEFAULT);
        return anneeScolaire;
    }

    public OptionESEO getOptionESEO() {
        OptionESEO option = new OptionESEO();
        option.setIdOption(ID_DEFAULT);
        option.setNomOption("Test");
        return option;
    }


    public Equipe getEquipe() {
        Equipe equipe = new Equipe();
        equipe.setIdSujet(ID_DEFAULT);
        equipe.setIdEquipe(EQUIPEID);
        equipe.setTaille(10);
        equipe.setValide("non");
        equipe.setAnnee(ANNEE_COURANTE);

        return equipe;
    }

    public EtudiantEquipe getEtudiantEquipe() {
        EtudiantEquipe etudiantEquipe = new EtudiantEquipe();
        etudiantEquipe.setIdEtudiant(ID_DEFAULT);
        etudiantEquipe.setIdEquipe(EQUIPEID);
        return etudiantEquipe;
    }

    public fr.eseo.ld.pgl.beans.EtudiantEquipe getEtudiantEquipePgl() {
        fr.eseo.ld.pgl.beans.EtudiantEquipe etudiantEquipe = new  fr.eseo.ld.pgl.beans.EtudiantEquipe();
        etudiantEquipe.setIdEtudiant(ID_DEFAULT);
        etudiantEquipe.setIdEquipe(ID_DEFAULT);
        etudiantEquipe.setIdSprint(ID_DEFAULT);
        return etudiantEquipe;
    }

    public Etudiant getEtudiant() {
        Etudiant etudiant = new Etudiant();
        etudiant.setIdEtudiant(ID_DEFAULT);
        etudiant.setNoteProjet(-1f);
        etudiant.setNoteSoutenance(-1f);
        etudiant.setNotePoster(-1f);
        etudiant.setNoteIntermediaire(-1f);
        etudiant.setContratPro("non");
        etudiant.setAnnee(ANNEE_COURANTE);
        return etudiant;
    }

    public NotePoster getNotePoster() {
        NotePoster notePoster = new NotePoster();
        notePoster.setIdProfesseur(ID_DEFAULT);
        notePoster.setIdEtudiant(ID_DEFAULT);
        notePoster.setIdPoster(ID_DEFAULT);
        notePoster.setNote(10.0f);
        return notePoster;
    }

    public NoteSoutenance getNoteSoutenance() {
        NoteSoutenance noteSoutenance = new NoteSoutenance();
        noteSoutenance.setIdProfesseur(ID_DEFAULT);
        noteSoutenance.setIdEtudiant(ID_DEFAULT);
        noteSoutenance.setIdSoutenance(ID_DEFAULT);
        noteSoutenance.setNote(10.0f);
        return noteSoutenance;
    }

    public Soutenance getSoutenance() {
        Soutenance soutenance = new Soutenance();
        soutenance.setIdSoutenance(ID_DEFAULT);
        soutenance.setIdEquipe(EQUIPEID);
        soutenance.setIdJurySoutenance(JURYID);
        return soutenance;
    }

    public Meeting getMeeting() {
        Meeting meeting = new Meeting();
        meeting.setTitre("Meeting");
        meeting.setIdReunion(ID_DEFAULT);
        meeting.setRefUtilisateur(ID_DEFAULT);
        meeting.setCompteRendu("Compte rendu");
        meeting.setDate("2020-10-10 12:10:00");
        meeting.setDescription("Description");
        meeting.setLieu("ESEO");
        return meeting;
    }

    public Invite getInvite() {
        Invite invite = new Invite();
        invite.setRefReunion(ID_DEFAULT);
        invite.setRefUtilisateur(ID_DEFAULT);
        invite.setParticipe("1");
        return invite;
    }

    public Sujet getSujet() {
        Sujet s = new Sujet();
        s.setIdSujet(ID_DEFAULT);
        s.setDescription("Description");
        s.setTitre("Titre");
        s.setContratPro(false);
        s.setConfidentialite(false);
        s.setNbrMinEleves(0);
        s.setNbrMaxEleves(1);
        s.setNoteInteretTechno(0.0f);
        s.setNoteInteretSujet(0.0f);
        s.setEtat(EtatSujet.DEPOSE);
        return s;
    }

    public Utilisateur getUtilisateur() {
        Utilisateur u = new Utilisateur();
        u.setIdUtilisateur(ID_DEFAULT);
        u.setEmail("no@no.fr");
        u.setPrenom("PRenom");
        u.setNom("Nom");
        u.setValide("oui");
        u.setIdentifiant("testtesttest");
        u.setHash("hash");
        return u;
    }

    public Professeur getProfesseur() {
        Professeur prof = new Professeur();
        prof.setIdProfesseur(ID_DEFAULT);
        return prof;
    }

    public Commentaire getCommentaire() {
        Commentaire commentaire = new Commentaire();
        commentaire.setIdCommentaire(ID_DEFAULT);
        commentaire.setIdUtilisateur(ID_DEFAULT);
        commentaire.setIdSujet(ID_DEFAULT);
        commentaire.setIdObservation(ID_DEFAULT);
        commentaire.setContenu("Contenu");
        return commentaire;
    }

    public Role getRole() {
        Role role = new Role();
        role.setIdRole(ID_DEFAULT);
        role.setNomRole("nomR");
        role.setType("applicatif");
        return role;
    }

    public PartageRole getPartageRole() {
        PartageRole pr = new PartageRole();
        pr.setIdOption(ID_DEFAULT);
        pr.setIdPartage(ID_DEFAULT);
        pr.setIdRole(ID_DEFAULT);
        pr.setIdUtilisateur1(ID_DEFAULT);
        pr.setIdUtilisateur2(ID_DEFAULT + 1L);
        pr.setActif(ID_DEFAULT);
        pr.setDateDebut("2018-01-01 10:00:00.0");
        pr.setDateFin("2018-02-01 10:00:00.0");
        return pr;
    }

    public RoleUtilisateur getRoleUtilisateur() {
        RoleUtilisateur roleUtilisateur = new RoleUtilisateur();
        roleUtilisateur.setIdRole(ID_DEFAULT);
        roleUtilisateur.setIdOption(ID_DEFAULT);
        roleUtilisateur.setIdUtilisateur(ID_DEFAULT);
        return roleUtilisateur;
    }

    public Notification getNotification() {
        Notification notification = new Notification();
        notification.setCommentaire("Commentaire");
        notification.setVue(0);
        notification.setLien("Lien");
        notification.setIdUtilisateur(ID_DEFAULT);
        notification.setIdNotification(ID_DEFAULT);
        return notification;
    }

    public OptionSujet getOptionSujet() {
        OptionSujet optionSujet = new OptionSujet();
        optionSujet.setIdOption(ID_DEFAULT);
        optionSujet.setIdSujet(ID_DEFAULT);
        return optionSujet;
    }

    public Note getNote() {
        Note note = new Note();
        note.setIdNote(ID_DEFAULT);
        note.setRefEvaluateur(ID_DEFAULT);
        note.setRefEtudiant(ID_DEFAULT);
        note.setRefMatiere(ID_DEFAULT);
        note.setNote(10.0f);

        return note;
    }

    public Sprint getSprint() {
        Sprint sprint = new Sprint();
        sprint.setIdSprint(ID_DEFAULT);
        sprint.setDateDebut("2018-01-01");
        sprint.setDateFin("2018-05-01");
        sprint.setNbrHeures(10);
        sprint.setNumero(1);
        sprint.setRefAnnee(ID_DEFAULT);
        sprint.setCoefficient(1.0f);
        sprint.setNotesPubliees(false);
        return sprint;
    }

    public PorteurSujet getPorteurSujet() {
        PorteurSujet porteurSujet = new PorteurSujet();
        porteurSujet.setAnnee(ANNEE_COURANTE);
        porteurSujet.setIdSujet(ID_DEFAULT);
        porteurSujet.setIdUtilisateur(ID_DEFAULT);
        return porteurSujet;
    }

    public ProfesseurSujet getProfesseurSujet() {
        ProfesseurSujet professeurSujet = new ProfesseurSujet();
        professeurSujet.setIdSujet(ID_DEFAULT);
        professeurSujet.setIdProfesseur(ID_DEFAULT);
        professeurSujet.setValide("oui");
        professeurSujet.setFonction(FonctionProfesseurSujet.REFERENT);
        return professeurSujet;
    }

    public BonusMalus getBonusMalus() {
        BonusMalus bonusMalus = new BonusMalus();
        bonusMalus.setIdBonusMalus(ID_DEFAULT);
        bonusMalus.setJustification("Aucune");
        bonusMalus.setRefEtudiant(ID_DEFAULT.intValue());
        bonusMalus.setRefSprint(ID_DEFAULT.intValue());
        bonusMalus.setRefEvaluateur(ID_DEFAULT.intValue());
        bonusMalus.setValeur(2f);
        bonusMalus.setValidation(BonusMalus.VALIDATION_TYPE.ATTENTE);
        return bonusMalus;
    }

    public fr.eseo.ld.pgl.beans.Equipe getEquipePGL() {
        fr.eseo.ld.pgl.beans.Equipe equipe = new fr.eseo.ld.pgl.beans.Equipe();
        equipe.setIdEquipe(ID_DEFAULT);
        equipe.setNom("equipe1");
        equipe.setRefAnnee(ID_DEFAULT);
        equipe.setRefProjet(null);
        return equipe;
    }

    public Matiere getMatiere() {
        Matiere matiere = new Matiere();
        matiere.setCoefficient(1f);
        matiere.setIdMatiere(ID_DEFAULT);
        matiere.setLibelle("matiere");
        matiere.setRefSprint(ID_DEFAULT);
        return matiere;
    }

    public fr.eseo.ld.pgl.beans.Etudiant getEtudiantPgl() {
        fr.eseo.ld.pgl.beans.Etudiant etudiant = new fr.eseo.ld.pgl.beans.Etudiant();
        etudiant.setIdEtudiant(ID_DEFAULT);
        etudiant.setRefAnnee(ID_DEFAULT);
        return etudiant;
    }

    public fr.eseo.ld.pgl.beans.Soutenance getSoutenancePGL() {
        fr.eseo.ld.pgl.beans.Soutenance soutenance = new fr.eseo.ld.pgl.beans.Soutenance();
        soutenance.setIdSoutenance(ID_DEFAULT);
        soutenance.setDate("2018-01-01 10:11:00.0");
        soutenance.setLibelle("Soutenance");
        return soutenance;
    }

    public CompositionJury getCompositionJury() {
        CompositionJury compositionJury = new CompositionJury();
        compositionJury.setRefProfesseur(ID_DEFAULT);
        compositionJury.setRefSoutenance(ID_DEFAULT);
        return compositionJury;
    }

    public Projet getProjet() {
        Projet projet = new Projet();
        projet.setIdProjet(ID_DEFAULT);
        projet.setIdProfReferent(ID_DEFAULT);
        projet.setDescription("DESC");
        projet.setTitre("Titre");

        return projet;
    }

    public RoleEquipe getRoleEquipe() {
        RoleEquipe roleEquipe = new RoleEquipe();
        roleEquipe.setRefUtilisateur(ID_DEFAULT);
        roleEquipe.setRefRole(ID_DEFAULT);
        roleEquipe.setRefEquipe(ID_DEFAULT);

        return roleEquipe;
    }

    public DemandeJury getDemandeJury() {
        DemandeJury demandeJury = new DemandeJury();
        demandeJury.setIdDemande(ID_DEFAULT);
        demandeJury.setCommentaire("Commantaire");
        demandeJury.setNomOption("LD");
        demandeJury.setSujetsConcernes("Aucun");

        return demandeJury;
    }
}
