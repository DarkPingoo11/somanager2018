package fr.eseo.testing;

import fr.eseo.ld.dao.DAO;
import fr.eseo.ld.dao.DAOFactory;
import fr.eseo.ld.dao.DAOUtilitaire;
import fr.eseo.ld.utils.IOUtilitaire;
import org.apache.log4j.*;

import java.io.ByteArrayOutputStream;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.fail;
import static org.junit.Assert.assertTrue;

public class DAOTesting<T> extends DAOAccess {

    private static final String FICHIER_PROPERTIES = "/fr/eseo/testing/dao.properties";
    private static final String PROPERTY_URL = "url";
    private static final String PROPERTY_NOM_UTILISATEUR = "nomUtilisateur";
    private static final String PROPERTY_MOT_DE_PASSE = "motDePasse";

    private DAOFactory daoFactory;
    private DAO<T> dao;

    public enum CRUD{CREER, TROUVER, MODIFIER, SUPPRIMER}
    private HashMap<Class, String> alias;
    private List<Class> usedClasses;

    public DAOTesting() {
        this.daoFactory = createDAOFactory();
        this.alias = new HashMap<>();
        this.usedClasses = new ArrayList<>();
        try(Connection c = this.daoFactory.getConnection(); Statement s = c.createStatement()) {
            s.execute("SET FOREIGN_KEY_CHECKS=0");
        } catch(SQLException e) {
            String c = this.daoFactory.getUrlBdd() + "  " + this.daoFactory.getUserBdd() + "@" + this.daoFactory.getMdpBdd();
            fail("Impossible d'instancier DAOTesting : " + e.getMessage() + " | " + c);
        }
    }

    /**
     * Initialise le dao
     * @param dao Dao souhaité
     */
    public void initDao(DAO<T> dao) {
        this.dao = dao;
    }

    /**
     * Ajoute un alias de table pour la classe spécifiée
     * @param clazz Classe
     * @param alias Alias de table
     */
    public void addAlias(Class clazz, String alias) {
        this.alias.put(clazz, alias);
    }

    /**
     * Vide les tables une fois testées
     * @throws SQLException Probleme
     */
    public void truncateTable() throws SQLException {
        this.checkConstraints();

        this.truncateTable(getGenericClass());
    }

    /**
     * Vide toutes les tables qui ont été utilisées pendant ce tests
     * L'appel de cette méthode doit être fait en fin de test
     * @throws SQLException en cas de probleme SQL
     */
    public void truncateUsedTables() throws SQLException {
        for(Class clazz : this.usedClasses) {
            truncateTable(clazz);
        }
        truncateTable();
        //Clear pour le nouveau test
        this.usedClasses.clear();
    }

    /**
     * Vide les tables une fois testée
     * @param clazz Classe du dao referencant la table
     * @throws SQLException Probleme
     */
    public void truncateTable(Class clazz) throws SQLException {
        this.checkConstraints();

        String className = alias.containsKey(clazz) ? alias.get(clazz) : clazz.getSimpleName();
        StringBuilder truncateB = new StringBuilder();
        truncateB.append("TRUNCATE TABLE ").append(className);
        try(Connection c = this.daoFactory.getConnection(); Statement s = c.createStatement()) {
            s.execute("SET FOREIGN_KEY_CHECKS=0");
            s.execute(truncateB.toString());
            s.execute("SET FOREIGN_KEY_CHECKS=1");
        }

    }

    /**
     * Récupère le DAOFactory pour les tests
     * Il faut avoir crée la base somanagertest
     * @return DaoFactory
     */
    public DAOFactory getDaoFactory() {
        return this.daoFactory;
    }

    /**
     * Créer un DAOFactory de test
     * @return DaoFactory
     */
    private DAOFactory createDAOFactory() {
        final Map<String, String> properties = IOUtilitaire.lireFichierProperties(FICHIER_PROPERTIES);

        String url = properties.get(PROPERTY_URL);
        String nomUtilisateur = properties.get(PROPERTY_NOM_UTILISATEUR);
        String motDePasse = properties.get(PROPERTY_MOT_DE_PASSE);

        return new DAOFactory(url, nomUtilisateur, motDePasse);
    }



    /**
     * Test la création du DAO
     * @param object Objet a créer
     * @param expectedObject Objet attendu
     */
    public void testCreerCRUD(T object, T expectedObject) {
        this.test(CRUD.CREER, object, expectedObject, null);
    }

    /**
     * Test la methode TROUVER du DAO
     * @param object Objet a trouver
     * @param expectedObject Objet attendu
     */
    public void testTrouverCRUD(T object, T expectedObject) {
        this.test(CRUD.TROUVER, object, expectedObject, null);
    }

    /**
     * Test la methode MODIFIER du DAO
     * @param object Objet a trouver
     * @param expectedObject Objet attendu
     * @param modifiedObject Objet a modifier
     */
    public void testModifierCRUD(T object, T modifiedObject, T expectedObject) {
        this.test(CRUD.MODIFIER, object, expectedObject, modifiedObject);
    }

    /**
     * Test la methode SUPPRIMER du DAO
     * @param object Objet a supprimer
     */
    public void testSupprimerCRUD(T object) {
        this.test(CRUD.SUPPRIMER, object, null, null);
    }

    /**
     * Test la methode LISTER du DAO
     * @param objets objets a ajouter
     * @param expectedObjects Objets attendus
     */
    public void testListerCRUD(T[] objets, T[] expectedObjects) {
        try {
            this.testLister(objets, expectedObjects);
        } catch(IllegalAccessException e) {
            fail(e.getMessage());
        }
    }

    /**
     * Test la gestion des erreurs pour les CRUD passés en paramètre
     * @param cruds CRUD pour lesquels il faut tester le logging
     */
    public void testLogging(CRUD... cruds) {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        Layout layout = new SimpleLayout();
        Appender appender = new WriterAppender(layout, out);
        Logger logger = Logger.getLogger(this.dao.getClass().getName());
        Logger loggerDAO = Logger.getLogger(DAOUtilitaire.class.getName());
        logger.addAppender(appender);
        loggerDAO.addAppender(appender);


        T object = null;
        try {
            object = (T) getGenericClass().newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            fail(e.getMessage());
        }

        final String echecMsg = "Échec";
        try {
            for(CRUD crud : cruds) {
                switch(crud) {
                    case CREER:
                        this.dao.creer(object);
                        assertTrue("Mauvais logging pour créer", out.toString().contains(echecMsg));
                        out.reset();
                        break;
                    case TROUVER:
                        this.dao.trouver(object);
                        assertTrue("Mauvais logging pour trouver", out.toString().contains(echecMsg));
                        out.reset();
                        break;
                    case MODIFIER:
                        this.dao.modifier(object);
                        assertTrue("Mauvais logging pour modifier", out.toString().contains(echecMsg));
                        out.reset();
                        break;
                    case SUPPRIMER:
                        this.dao.supprimer(object);
                        assertTrue("Mauvais logging pour supprimer", out.toString().contains(echecMsg));
                        out.reset();
                        break;
                    default:
                        break;
                }
            }







        } finally {
            /* Configuraton initiale du logger */
            logger.removeAppender(appender);
        }
    }

    /**
     * Test les erreurs de CRUD pour CREER, TROUVER, MODIFIER et SUPPRIMER
     */
    public void testLoggingCRUD() {
        this.testLogging(CRUD.CREER, CRUD.MODIFIER, CRUD.SUPPRIMER, CRUD.TROUVER);
    }

    /**
     * Teste une methode CRUD du DAO.
     * @param methode Methde a tester
     * @param object Objet a tester
     * @param expectedObject Objet attendu
     */
    private void test(CRUD methode, T object, T expectedObject, T modifiedObject){
        this.checkConstraints();

        try {
            switch(methode) {
                case CREER:
                    testCreer(object, expectedObject);
                    break;
                case TROUVER:
                    testTrouver(object, expectedObject);
                    break;
                case MODIFIER:
                    testModifier(object, expectedObject, modifiedObject);
                    break;
                case SUPPRIMER:
                    testSupprimer(object);
                    break;
            }
        } catch (IllegalAccessException e) {
            fail(e.getMessage());
        }
    }

    /**
     * Teste la methode creer
     * La methode testTrouver doit passer les tests pour que celle-ci fonctionne
     * @param object Objet a créer
     * @param expectedObject Objet attendu
     */
    private void testCreer(T object, T expectedObject) {
        //Création de l'objet
        dao.creer(object);

        //Puis on verifie qu'il existe dans la base
        List<T> found = dao.trouver(object);

        assertTrue("Aucun résultat n'est retourné", !found.isEmpty());
        assertEquals("Trop de résultats sont retournés", 1, found.size());

        //Verification que les objets sont identifique
        compareObjects(expectedObject, found.get(0));
    }

    /**
     * Teste la methode trouver
     * @param object Objet a chercher
     * @param expectedObject Objet attendu
     * @throws IllegalAccessException En cas de probleme d'accès
     */
    private void testTrouver(T object, T expectedObject) throws IllegalAccessException {
        //Créer l'objet en base
        this.createObjectInDatabase(object);

        //Recherche de l'objet
        List<T> found = dao.trouver(object);

        //Verification que les objets sont identifique
        assertEquals("Aucun objet n'a été trouvé", 1, found.size());
        compareObjects(expectedObject, found.get(0));
    }

    /**
     * Teste la methode modifuier
     * La methode testTrouver doit passer les tests pour que celle-ci fonctionne
     * @param object Objet a créer
     * @param expectedObject Objet attendu
     * @param modifiedObject Objet a modifier
     * @throws IllegalAccessException En cas de probleme d'accès
     */
    private void testModifier(T object, T expectedObject, T modifiedObject) throws IllegalAccessException {
        //Créer l'objet en base
        this.createObjectInDatabase(object);

        //Modification
        dao.modifier(modifiedObject);

        //Puis on verifie qu'il existe dans la base
        List<T> found = dao.trouver(modifiedObject);

        //Verification que les objets sont identifique
        assertEquals("Aucun objet n'a été trouvé", 1, found.size());
        compareObjects(expectedObject, found.get(0));
    }

    /**
     * Compare deux objets en utilisant la reflectivité
     * fail si les objets sont différents
     * @param expected Objet souhaité
     * @param current Objet a comparer
     */
    public void compareObjects(Object expected, Object current) {
        try {
            for(Field f : expected.getClass().getDeclaredFields()) {
                f.setAccessible(true);
                if(!f.isSynthetic()) {
                    Object key1 = getValueOfField(f, expected);
                    Object key2 = getValueOfField(f, current);

                    //Vérification
                    assertEquals("Les objets ne sont pas les mêmes pour le champ " + f.getName(), key1, key2);
                }
            }
        } catch (IllegalAccessException e) {
            fail("Les deux objets ne sont pas identiques");
        }
    }

    /**
     * Teste la methode supprimer
     * La methode testTrouver doit passer les tests pour que celle-ci fonctionne
     * @param object Objet a supprimer
     * @throws IllegalAccessException En cas de probleme d'accès
     */
    private void testSupprimer(T object) throws IllegalAccessException {
        //Créer l'objet en base
        this.createObjectInDatabase(object);

        //Modification
        dao.supprimer(object);

        //Puis on verifie qu'il existe dans la base
        List<T> found = dao.trouver(object);

        assertTrue("L'objet n'a pas été supprimé", found.isEmpty());
    }

    /**
     * Teste la methode supprimer
     * La methode testTrouver doit passer les tests pour que celle-ci fonctionne
     * @param objects Objet a supprimer
     * @param expectedObjects Objets attendus
     * @throws IllegalAccessException En cas de probleme d'accès
     */
    private void testLister(T[] objects, T[] expectedObjects) throws IllegalAccessException {
        //Créer les objets en base
        for(Object o : objects) {
            this.createObjectInDatabase(o);
        }

        //Modification
        List<T> liste = dao.lister();

        //Puis on verifie que les objets correspondent
        assertEquals("Les objets n'ont pas la même taille", expectedObjects.length, liste.size());

        //Verification que les objets sont identifique
        for(int i = 0; i < liste.size(); i++) {
            T o = liste.get(i);
            T expectedO = expectedObjects[i];
            compareObjects(expectedO, o);
        }
    }

    /**
     * Créer un objet en base via sql
     * @param object Objet a créer
     * @throws IllegalAccessException Exception
     */
    public void createObjectInDatabase(Object object) throws IllegalAccessException {
        //Creation de la requete SQL
        StringBuilder champs = new StringBuilder();
        StringBuilder valeurs = new StringBuilder();
        List<String> fieldsName = new ArrayList<>();

        for(Field f : object.getClass().getDeclaredFields()) {
            f.setAccessible(true);
            if(!f.isSynthetic()) {
                champs.append(",").append(f.getName());
                valeurs.append(",").append(encapsulate(f.get(object)));
                fieldsName.add(f.getName());
            }
        }

        //Vérification de l'héritage
        Class currentClass = object.getClass();
        while(currentClass.getSuperclass() != null) {
            currentClass = currentClass.getSuperclass();
            for(Field f : currentClass.getDeclaredFields()) {
                f.setAccessible(true);
                if(!f.isSynthetic() && !fieldsName.contains(f.getName())) {
                    champs.append(",").append(f.getName());
                    valeurs.append(",").append(encapsulate(f.get(object)));
                }
            }
        }


        //Retrait des virgules de début
        retraitVirgule(champs);
        retraitVirgule(valeurs);


        //Appends des bouts de sql
        Class clazz = object.getClass();
        String className = alias.containsKey(clazz) ? alias.get(clazz) : clazz.getSimpleName();
        StringBuilder sql = new StringBuilder();
        sql.append("INSERT INTO ").append(className);
        sql.append("(").append(champs).append(")");
        sql.append(" ").append("VALUES(").append(valeurs).append(")");

        //Ajout a la liste used
        if(!this.usedClasses.contains(clazz)) {
            this.usedClasses.add(clazz);
        }

        //Creation de l'objet en base
        try(Connection c = this.daoFactory.getConnection(); Statement s = c.createStatement()) {
            s.executeUpdate(sql.toString());
        } catch (SQLException e) {
            fail("Erreur sql : " + e.getMessage() + " => " + sql);
        }
    }

    /**
     * Retire les virgules de début
     * @param champs Texte
     */
    private void retraitVirgule(StringBuilder champs) {
        if(champs.length() > 0) {
            champs.deleteCharAt(0);
        }
    }

    /**
     * Vérifie les contraintes
     */
    private void checkConstraints() {
        if(this.dao == null) {
            throw new IllegalStateException("Il faut appeler la fonction initDAO avant de lancer les tests");
        }
    }

    /**
     * Encapsule un objet pour le sql
     * @param o objet a encapsuler
     * @return Objet encapsulé
     */
    private String encapsulate(Object o) {
        if(o instanceof String) {
            return "'" + o.toString().replaceAll("'", "\\'") + "'";
        } else if(o instanceof Long || o instanceof Integer ||o instanceof Float) {
            return o.toString();
        }else if(o instanceof Enum){
            return "'" + o.toString() + "'";
        }else if(o != null){
            return o.toString();
        }else {
            return "NULL";
        }
    }

    /**
     * Récupère la valeur du champ
     * @param f champ
     * @param o objet possédant le champ
     * @return Valeur du champ
     * @throws IllegalAccessException Si le champ est protégé
     */
    private Object getValueOfField(Field f, Object o) throws IllegalAccessException {
        f.setAccessible(true);
        Class clazz = f.getClass();

        if(clazz == String.class) {
            return f.get(o);
        } else if(clazz == Integer.class || clazz == int.class) {
            return f.getInt(o);
        } else if(clazz == Long.class || clazz == long.class) {
            return f.getLong(o);
        }
        return f.get(o);
    }

    /**
     * Récupère la classe générique de ce daoTesting
     * @return Classe générique
     */
    private Class<?> getGenericClass() {
        ParameterizedType parameterizedType = (ParameterizedType) this.dao.getClass().getGenericSuperclass();
        return (Class<?>)parameterizedType.getActualTypeArguments()[0];
    }

}
