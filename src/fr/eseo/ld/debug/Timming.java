package fr.eseo.ld.debug;

/**
 * Classe permettant de définir le temps d'execution d'une méthode
 */
public class Timming {

    private long startTime;
    private long endTime;

    /**
     * Constructeur
     */
    private Timming() {

    }

    /**
     * Démarre ou redemarre le compteur
     */
    public void start() {
        this.startTime = System.nanoTime();
    }

    /**
     * Arrête le compteur
     */
    public void stop() {
        this.endTime = System.nanoTime();
    }

    /**
     * Renvoies la durée du timming en nano secondes (10^-9)
     * @return La durée en nano secondes
     */
    public long getDurationInNano() {
        return (endTime - startTime);
    }

    /**
     * Renvoies la durée du timming en millisecondes (10^-3)
     * @return La durée en millisecondes
     */
    public long getDurationInMillis() {
        return this.getDurationInNano() / 1000000;
    }

    /**
     * Démarre un nouveau timming
     * @return Le timming
     */
    public static Timming startNewTimming() {
        Timming timming = new Timming();
        timming.start();

        return timming;
    }
}
