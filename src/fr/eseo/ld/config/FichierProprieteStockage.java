package fr.eseo.ld.config;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

/**
 * Classe implémentant les méthodes et fonctions permettant de modifier le
 * fichier de configuration de stockage.
 * 
 * @author
 */
public class FichierProprieteStockage extends Stockage {

	private Properties properties = new Properties();
	private URL url;

	private static Logger logger = Logger.getLogger(FichierProprieteStockage.class.getName());

	/**
	 * Constructeur par défaut.
	 */
	public FichierProprieteStockage() {
		super();
	}

	/**
	 * Renvoie la liste des attributs contenus dans le fichier de propriétés.
	 * 
	 * @return listeAttributs la liste des attributs contenus dans le fichier de
	 *         propriétés.
	 */
	public List<String> lireFichierStockage() {
		this.chargerFichierProperties();

		// Relire les variables depuis le fichier
		this.cheminEnregPosters = this.properties.getProperty(Stockage.PROPERTY_CHEMIN_ENREG_POSTERS);
		this.cheminEnregSlides = this.properties.getProperty(Stockage.PROPERTY_CHEMIN_ENREG_SLIDES);
		this.tailleTamponFichier = this.properties.getProperty(Stockage.PROPERTY_TAILLE_TAMPON_FICHIER);

		List<String> listeAttributs = new ArrayList<>();
		listeAttributs.add(this.cheminEnregPosters);
		listeAttributs.add(this.cheminEnregSlides);
		listeAttributs.add(this.tailleTamponFichier);

		return listeAttributs;
	}

	/**
	 * Modifie les paramètres de stockage en prenant en compte les nouveaux
	 * attributs.
	 * 
	 * @param nouveauxAttributs
	 *            les nouveaux paramètres de la classe.
	 */
	public void modifierFichierStockage(String[] nouveauxAttributs) {
		/*
		 * Création des différentes variables utilisées lors de la lecture du fichier de
		 * propriétés
		 */
		this.chargerFichierProperties();

		try {
			// on vérifie que le tableau d'attributs contient bien 3 attributs
			if (nouveauxAttributs.length != 3) {
				throw new IOException();
			}

			// on change les différentes propriétés avec les attributs rentrés en paramètre
			// de la méthode
			changerAttributs(nouveauxAttributs);

			this.getClass().getClassLoader();
			// on récupère le chemin du fichier de paramètres
			this.url = this.getClass().getResource("stockage.properties");

			// on remplace les attributs de l'annuaire avec les nouvelles valeurs
			this.ecrireDansFichierStockage();
		} catch (IOException | URISyntaxException e) {
			logger.log(Level.WARN, "Impossible de lire ou de modifier le fichier de paramètres de stockage ", e);
		}
	}

	public void chargerFichierProperties() {
		this.properties = new Properties();
		ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
		InputStream fichierProperties = classLoader.getResourceAsStream(Stockage.FICHIER_PROPERTIES_STOKAGE);

		// on charge le fichier de propriétés
		try {
			this.properties.load(fichierProperties);
		} catch (IOException e) {
			logger.log(Level.WARN, "Impossible de charger le fichier", e);
		}
	}

	/**
	 * Ecrit dans un fichier.
	 * 
	 * chemin du fichier. fichier de propriétés à réécrire.
	 * 
	 * @throws URISyntaxException
	 * @throws IOException
	 */
	private void ecrireDansFichierStockage() throws URISyntaxException, IOException {
		/* Changement des paramètres présent dans annuaire.properties */
		File fileObject = new File(this.url.toURI());
		FileOutputStream out = new FileOutputStream(fileObject);
		this.properties.store(out, null);
		out.close();
	}

	/**
	 * Change les paramètres de la classe avec les attributs présents dans le
	 * tableau de Strings mis en paramètre.
	 * 
	 * @param nouveauxAttributs
	 *            les nouveaux paramètres de la classe.
	 */
	private void changerAttributs(String[] nouveauxAttributs) {
		this.properties.setProperty(Stockage.PROPERTY_CHEMIN_ENREG_POSTERS, nouveauxAttributs[0]);
		this.cheminEnregPosters = nouveauxAttributs[0];
		this.properties.setProperty(Stockage.PROPERTY_CHEMIN_ENREG_SLIDES, nouveauxAttributs[1]);
		this.cheminEnregSlides = nouveauxAttributs[1];
		this.properties.setProperty(Stockage.PROPERTY_TAILLE_TAMPON_FICHIER, nouveauxAttributs[2]);
		this.tailleTamponFichier = nouveauxAttributs[2];

	}

}