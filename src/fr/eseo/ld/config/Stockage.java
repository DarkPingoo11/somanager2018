package fr.eseo.ld.config;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

/**
 *
 * 
 * @author
 */
public class Stockage {

	protected static final String FICHIER_PROPERTIES_STOKAGE = "fr/eseo/ld/config/stockage.properties";
	protected static final String FICHIER_PROPERTIES_STOCKAGE_DEFAUT = "fr/eseo/ld/config/stockageDefaut.properties";
	protected static final String PROPERTY_CHEMIN_ENREG_POSTERS = "CHEMIN_ENREG_POSTERS";
	protected static final String PROPERTY_CHEMIN_ENREG_SLIDES = "CHEMIN_ENREG_SLIDES";
	protected static final String PROPERTY_TAILLE_TAMPON_FICHIER = "TAILLE_TAMPON_FICHIER";

	protected String cheminEnregPosters;
	protected String cheminEnregSlides;
	protected String tailleTamponFichier;

	private static Logger logger = Logger.getLogger(Stockage.class.getName());

	/**
	 * Constructeur de Stockage.
	 */
	public Stockage() {
		List<String> liste = lireFichierProprieteStockage(FICHIER_PROPERTIES_STOKAGE);
		this.cheminEnregPosters = liste.get(0);
		this.cheminEnregSlides = liste.get(1);
		this.tailleTamponFichier = liste.get(2);
	}

	/**
	 * Lit le fichier de stockage.properties.
	 * 
	 * @param fichierURL
	 *            l'URL du fichier.
	 * @return listeAttributs la liste comportant les attributs contenus dans le
	 *         fichier de propriété.
	 */
	private List<String> lireFichierProprieteStockage(String fichierURL) {
		List<String> listeAttributs = new ArrayList<>();
		Properties properties = new Properties();
		ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
		InputStream fichierProperties = classLoader.getResourceAsStream(fichierURL);
		try {
			properties.load(fichierProperties);
			listeAttributs.add(properties.getProperty(PROPERTY_CHEMIN_ENREG_POSTERS));
			listeAttributs.add(properties.getProperty(PROPERTY_CHEMIN_ENREG_SLIDES));
			listeAttributs.add(properties.getProperty(PROPERTY_TAILLE_TAMPON_FICHIER));

		} catch (IOException e) {
			logger.log(Level.WARN, "Echec de la lecture du fichier de propriété : " + e.getMessage(), e);
		}
		return listeAttributs;
	}

	/**
	 * Renvoie la liste des attributs du fichier de propriété par défaut.
	 * 
	 * @return la liste des attributs du fichier de propriété par défaut.
	 */
	public List<String> lireFichierProprieteStockageParDefaut() {
		return lireFichierProprieteStockage(FICHIER_PROPERTIES_STOCKAGE_DEFAUT);
	}

}