package fr.eseo.ld.config;

import fr.eseo.ld.dao.DAOFactory;
import fr.eseo.ld.servlets.Installation;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

/**
 * Classe d'Initialisation de la DAOFactory.
 * 
 * <p>
 * Cette classe gère l'instanciation de la DAOFactory au démarrage de
 * l'application.
 * </p>
 * 
 * @author Thomas MENARD et Maxime LENORMAND
 * @see fr.eseo.ld.dao.DAOFactory
 */
@WebListener
public class InitialisationDAOFactory implements ServletContextListener {

	private static final String ATT_DAO_FACTORY = "daoFactory";

	@Override
	public void contextInitialized(ServletContextEvent event) {

		//SI l'application est installée on la démarre normalement
		if(Installation.isSomanagerInstalled(event.getServletContext())) {
			/* Récupération du ServletContext lors du chargement de l'application */
			ServletContext servletContext = event.getServletContext();
			/* Instanciation de notre DAOFactory */
			DAOFactory daoFactory = DAOFactory.getInstancePool();
			/* Enregistrement dans un attribut ayant pour portée toute l'application */
			servletContext.setAttribute(ATT_DAO_FACTORY, daoFactory);
		}
	}

	@Override
	public void contextDestroyed(ServletContextEvent event) {
		/* Rien à réaliser lors de la fermeture de l'application... */
	}

}