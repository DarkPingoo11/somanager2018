package fr.eseo.ld.objets.exportexcel;

import fr.eseo.ld.beans.Utilisateur;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.SheetUtil;

import java.io.IOException;
import java.io.OutputStream;
import java.util.*;
import java.util.stream.Collectors;

public class ExcelNotesJury {

    public enum ExportAnnee{ PFE, PGL }

    private static final Integer SHEET_INDEX    = 0;

    private Workbook workbook;
    private Sheet sheet;
    private Map<ENJParams, Integer> colonnesNotes;
    private Integer headerRow;


    /**
     * Constructeur pour lire uniquement le fichier
     * @param workbook Excel
     */
    public ExcelNotesJury(Workbook workbook) {
        this(workbook, new HashMap<>(), null);
    }

    /**
     * Constructeur pour travailler sur le fichier en profondeur
     * @param workbook Excel
     * @param colonnesNotes Parametres
     * @param headerRow Ligne de header
     */
    public ExcelNotesJury(Workbook workbook, Map<ENJParams, Integer> colonnesNotes, Integer headerRow) {
        this.workbook = workbook;
        this.sheet = workbook.getSheetAt(SHEET_INDEX);
        this.colonnesNotes = colonnesNotes;
        this.headerRow = headerRow;
    }

    /**
     * Insere dans le document les notes des utilisateurs
     * @param notes Hashmap, Nomprenom -> NotePoster , NoteSoutenance
     * @param type type (normal ou auto)
     */
    public void insererNotes(Map<Utilisateur, ENJNotes> notes, List<ENJMatiere> matieres, ExportType type) {

        //Redefinition des parametres
        this.colonnesNotes.put(ENJParams.COL_MOYENNE, this.getColonnesNotes().get(ENJParams.COL_MOYENNE) + 2*(matieres.size()-1));
        this.colonnesNotes.put(ENJParams.COL_GRADE, this.getColonnesNotes().get(ENJParams.COL_GRADE) + 2*(matieres.size()-1));

        //Préparer les colonnes
        copierColonnesNotes(this.getColonnesNotes().get(ENJParams.COL_NOTE), this.getSheet().getLastRowNum()-1, matieres);


        if(type.equals(ExportType.NORMAL)) {
            Map<Utilisateur, Integer> utilisateurs = this.getEtudiantsANoterFromExcel();
            insererNotesNormal(notes, utilisateurs);
        } else if(type.equals(ExportType.AUTOCOMPLETE)){
            notes = this.trierNotesParUtilisateurInv(notes);

            //Pour chaque utilisateur
            int index = notes.size();

            for(Map.Entry<Utilisateur, ENJNotes> entryUtilisateur : notes.entrySet()) {
                Utilisateur utilisateur = entryUtilisateur.getKey();
                ENJNotes note = entryUtilisateur.getValue();

                //On copie la ligne de référence (HEADER+1) sauf pour le dernier utilisateur où on écris dessus
                if(index > 1) {
                    this.copyRow(this.getHeaderRow() + 1, this.getHeaderRow() + 2);
                }

                //Définition d'un id de 1 à n
                utilisateur.setIdUtilisateur((long)index--);

                //On récupère les infos
                noterUtilisateur(this.getHeaderRow() + (index == 0 ? 1 : 2), utilisateur, note);

            }
        }
    }

    /**
     * Insere les notes dans le fichier excel (modèle défini)
     * @param notes
     * @param utilisateurs
     */
    private void insererNotesNormal(Map<Utilisateur, ENJNotes> notes, Map<Utilisateur, Integer> utilisateurs) {
        //Pour chaque utilisateur, faire la correspondance
        for(Map.Entry<Utilisateur,ENJNotes> euBDD : notes.entrySet()) {
            Utilisateur uBDD = euBDD.getKey();
            String nomprenomU = uBDD.getNom() + " " + uBDD.getPrenom();
            for(Map.Entry<Utilisateur,Integer> euSheet : utilisateurs.entrySet()) {
                Utilisateur uSheet = euSheet.getKey();
                String nomPrenomUS = uSheet.getNom() + uSheet.getPrenom();
                //Si c'est le même utilisateur
                if(nomPrenomUS.equalsIgnoreCase(nomprenomU)) {
                    uBDD.setIdUtilisateur(uSheet.getIdUtilisateur());
                    noterUtilisateur(euSheet.getValue(), uBDD, euBDD.getValue());
                    break;
                }
            }
        }
    }

    /**
     * Note un utilisateur
     * @param rowNum ligne de notation
     * @param utilisateur utilisateur
     * @param note notes associées
     */
    private void noterUtilisateur(int rowNum, Utilisateur utilisateur, ENJNotes note) {

        String nomPrenom        = utilisateur.getNom().toUpperCase() + " " + utilisateur.getPrenom();
        Integer idUtilisateur   = utilisateur.getIdUtilisateur().intValue();


        //Et on remplis les infos
        Row row = this.getSheet().getRow(rowNum);

        //Nom & Prenom
        setCellVal(row, this.getColonnesNotes().get(ENJParams.COL_ID_UTILISATEUR), idUtilisateur);
        setCellVal(row, this.getColonnesNotes().get(ENJParams.COL_NOM_UTILISATEUR), nomPrenom);

        int indexNote = this.getColonnesNotes().get(ENJParams.COL_NOTE);
        final Map<ENJMatiere, Float> notes = note.getNotes();

        //Note & moyenne
        int sommeNotes = 0;
        int sommeCoeff = 0;
        for(Map.Entry<ENJMatiere, Float> nmatiere : notes.entrySet()) {
            //Recherche de la colonne correspondante
            for(int i = indexNote; i < indexNote + 2*notes.size(); i+= 2) {
                Cell cell = SheetUtil.getCell(this.getSheet(), this.getHeaderRow()-1, i);

                if(cell.getStringCellValue().equalsIgnoreCase(nmatiere.getKey().getLibelle())) {
                    setCellVal(row, i, nmatiere.getValue());

                    sommeNotes += nmatiere.getValue() * nmatiere.getKey().getCoefficient();
                    sommeCoeff += nmatiere.getKey().getCoefficient();
                    break;
                }
            }
        }

        //Définir la moyenne
        float moyenne = sommeCoeff > 0 ? ((float)sommeNotes)/sommeCoeff : 0;
        setCellVal(row, this.getColonnesNotes().get(ENJParams.COL_MOYENNE), moyenne);
    }

    /**
     * Copie les colonnes de note et de commentaire, et peuple les noms
     * @param colNote Index de la colonne de note
     * @param matieres Matières à noter
     */
    private void copierColonnesNotes(int colNote, int end, List<ENJMatiere> matieres) {
        //Si il y a n notes, ajouter n-1 fois
        for(int i = 0; i < matieres.size() - 1; i++) {
            decalerColonnes(colNote, 2, 0, end);
        }

        //Merge les intitulé de note
        int header = this.getHeaderRow()-1;
        for(int i = 1; i < matieres.size(); i++) {
            int fcol = (colNote)+(2*i);
            definirIntituleEtCoeff(fcol, matieres, header, i);
            if(!isPartOfMergedRegion(header, fcol)) {
                this.getSheet().addMergedRegion(new CellRangeAddress(header,header,fcol,fcol+1));
            }
        }

        //Changer l'intitulé de la première colonne
        definirIntituleEtCoeff(colNote, matieres, header, 0);

    }

    private void definirIntituleEtCoeff(int colNote, List<ENJMatiere> matieres, int header, int i2) {
        //Calcul du coeff total
        int coeffTot = 0;
        for(ENJMatiere m : matieres) {
            coeffTot += m.getCoefficient();
        }

        coeffTot = coeffTot > 0 ? coeffTot : 1;

        SheetUtil.getCell(this.getSheet(), header, colNote).setCellValue(matieres.get(i2).getLibelle());
        SheetUtil.getCell(this.getSheet(), header + 1, colNote).setCellValue(Math.round((matieres.get(i2).getCoefficient() * 100 / coeffTot)) + " %");
    }

    /**
     * Décale les colonnes
     * @param colIndex Première colonne
     * @param shift Nombre de colonnes de décalage
     * @param beginRow Première ligne
     * @param endRow Dernière ligne
     */
    private void decalerColonnes(int colIndex, int shift, int beginRow, int endRow) {
        //Récupérer la dernière cellule
        int lastCell = this.getSheet().getRow(this.getHeaderRow()).getLastCellNum();

        for(int i = beginRow; i < endRow+1; i++) {
            Row row = this.getSheet().getRow(i);

            if(row != null) {
                //Bouger toutes les cellules en partant de la fin (créer une nouvelle colonne)
                for(int j = lastCell-1; j > colIndex-1; j--) {
                    Cell oldCell = row.getCell(j);
                    Cell newCell = row.createCell(j+shift);

                    if(oldCell == null) {
                        continue;
                    }

                    this.getSheet().setColumnWidth(j+shift, this.getSheet().getColumnWidth(j));
                    cloneCellule(oldCell, newCell);
                }
            }
        }
    }


    /**
     * Clone les cellules
     * @param oldCell ancienne
     * @param newCell nouvelle
     */
    private void cloneCellule(Cell oldCell, Cell newCell) {
        // Copy style from old cell and apply to new cell
        CellStyle newCellStyle = workbook.createCellStyle();
        newCellStyle.cloneStyleFrom(oldCell.getCellStyle());
        newCell.setCellStyle(newCellStyle);

        // If there is a cell comment, copy
        if (oldCell.getCellComment() != null) {
            newCell.setCellComment(oldCell.getCellComment());
        }

        // If there is a cell hyperlink, copy
        if (oldCell.getHyperlink() != null) {
            newCell.setHyperlink(oldCell.getHyperlink());
        }

        // Set the cell data type
        newCell.setCellType(oldCell.getCellTypeEnum());

        // Set the cell data value
        switch (oldCell.getCellTypeEnum()) {
            case BLANK:
                newCell.setCellValue(oldCell.getStringCellValue());
                break;
            case BOOLEAN:
                newCell.setCellValue(oldCell.getBooleanCellValue());
                break;
            case ERROR:
                newCell.setCellErrorValue(oldCell.getErrorCellValue());
                break;
            case FORMULA:
                newCell.setCellFormula(oldCell.getCellFormula());
                break;
            case _NONE:
                break;
            case NUMERIC:
                newCell.setCellValue(oldCell.getNumericCellValue());
                break;
            case STRING:
                newCell.setCellValue(oldCell.getRichStringCellValue());
                break;
            default:
                newCell.setCellValue("");
                break;

        }
    }

    /**
     * Enregistre le fichier excel
     * @param os OutputStream
     * @throws IOException Si la destination est protégée
     */
    public void enregistrerFichier(OutputStream os) throws IOException {
        workbook.write(os);
    }

    /**
     * Récupère une liste des noms prenoms des etudiants à noter
     * @return Hashmap des etudiants (nomprenom -> ligne)
     */
    public Map<Utilisateur, Integer> getEtudiantsANoterFromExcel() {
        //Récupération des utilisateurs à noter
        HashMap<Utilisateur, Integer> utilisateurs = new HashMap<>();

        int index = this.getHeaderRow() + 1;

        //Premier tour
        Row row = this.getSheet().getRow(index);
        Cell idCell     = null;
        Cell nomCell    = null;
        if(row != null) {
            idCell     = row.getCell(this.getColonnesNotes().get(ENJParams.COL_ID_UTILISATEUR));
            nomCell  = row.getCell(this.getColonnesNotes().get(ENJParams.COL_NOM_UTILISATEUR));
        }

        index++;

        //Tant que la ligne correspond à un utilisateur
        while(idCell != null && !idCell.getCellTypeEnum().equals(CellType.BLANK)) {
            //On lit les informations
            Utilisateur utilisateur = new Utilisateur();
            utilisateur.setIdUtilisateur((long)idCell.getNumericCellValue());
            utilisateur.setNom(nomCell.getStringCellValue());
            utilisateur.setPrenom("");
            utilisateurs.put(utilisateur, index-1);


            //On lit la ligne suivante
            row        = this.getSheet().getRow(index);
            idCell     = row.getCell(this.getColonnesNotes().get(ENJParams.COL_ID_UTILISATEUR));
            nomCell    = row.getCell(this.getColonnesNotes().get(ENJParams.COL_NOM_UTILISATEUR));
            index++;
        }

        return utilisateurs;
    }

    /**
     * Récupère la sheet des notes
     * @return Sheet
     */
    public Sheet getSheet() {
        return sheet;
    }

    /**
     * Récupère le workbook
     * @return workbook
     */
    public Workbook getWorkbook() {
        return workbook;
    }

    /**
     * Récupère la map des paramètres
     * @return Map
     */
    public Map<ENJParams, Integer> getColonnesNotes() {
        return colonnesNotes;
    }

    /**
     * Défini la position d'une colonne
     * @param param Colonne en question
     * @param value position
     */
    public void setColonneNote(ENJParams param, Integer value) {
        this.getColonnesNotes().put(param, value);
    }

    /**
     * Récupère la ligne du header des notes
     * @return HeaderRow
     */
    public Integer getHeaderRow() {
        return headerRow;
    }

    /**
     * Copie une ligne vers une autre
     * @author qwerty
     * @apiNote https://stackoverflow.com/questions/5785724
     * @param sourceRowNum Ligne a copier
     * @param destinationRowNum Vers cette ligne
     */
    private void copyRow(int sourceRowNum, int destinationRowNum) {
        // Get the source / new row
        Row newRow = this.getSheet().getRow(destinationRowNum);
        Row sourceRow = this.getSheet().getRow(sourceRowNum);

        // If the row exist in destination, push down all rows by 1 else create a new row
        if (newRow != null) {
            this.getSheet().shiftRows(destinationRowNum, this.getSheet().getLastRowNum(), 1);
            newRow = this.getSheet().createRow(destinationRowNum);
        } else {
            newRow = this.getSheet().createRow(destinationRowNum);
        }

        // Loop through source columns to add to new row
        for (int i = 0; i < sourceRow.getLastCellNum(); i++) {
            // Grab a copy of the old/new cell
            Cell oldCell = sourceRow.getCell(i);
            Cell newCell = newRow.createCell(i);

            // If the old cell is null jump to next cell
            if (oldCell == null) {
                continue;
            }

            //Cloner la cellule
            cloneCellule(oldCell, newCell);
        }

        // If there are are any merged regions in the source row, copy to new row
        for (int i = 0; i < this.getSheet().getNumMergedRegions(); i++) {
            CellRangeAddress cellRangeAddress = this.getSheet().getMergedRegion(i);
            if (cellRangeAddress.getFirstRow() == sourceRow.getRowNum()) {
                CellRangeAddress newCellRangeAddress = new CellRangeAddress(newRow.getRowNum(),
                        (newRow.getRowNum() +
                                (cellRangeAddress.getLastRow() - cellRangeAddress.getFirstRow()
                                )),
                        cellRangeAddress.getFirstColumn(),
                        cellRangeAddress.getLastColumn());
                this.getSheet().addMergedRegion(newCellRangeAddress);
            }
        }
    }

    /**
     * Défini la valeur d'une cellule
     * @param row ligne
     * @param cellNum numero de cellule | null
     * @param val valeur
     */
    private void setCellVal(Row row, Integer cellNum, Object val) {
        val = val == null ? "" : val;
        if(cellNum != null) {
            Cell cell = row.getCell(cellNum);
            if(val instanceof String) {
                cell.setCellValue((String) val);
            } else if(val instanceof Float) {
                cell.setCellValue((Float) val);
            } else if(val instanceof Integer) {
                cell.setCellValue((Integer) val);
            } else if(val instanceof Boolean) {
                cell.setCellValue((Boolean) val);
            } else {
                cell.setCellValue("");
            }
        }
    }

    private Map<Utilisateur, ENJNotes> trierNotesParUtilisateurInv(Map<Utilisateur, ENJNotes> notes){
        //Comparateur alphabétique inverse
        Comparator<Map.Entry<Utilisateur, ENJNotes>> comparateur =
                (u1, u2) -> (u2.getKey().getNom() + u2.getKey().getPrenom())
                        .compareTo((u1.getKey().getNom() + u1.getKey().getPrenom()));

        return notes.entrySet().stream().sorted(comparateur).
                collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,
                        (e1, e2) -> e1, LinkedHashMap::new));
    }

    private boolean isPartOfMergedRegion(int row, int col) {
        boolean isPart = false;
        for (int i = 0; i < this.getSheet().getNumMergedRegions(); i++) {
            CellRangeAddress cellRangeAddress = this.getSheet().getMergedRegion(i);
            if(cellRangeAddress.isInRange(row, col)) {
                isPart = true;
                break;
            }
        }
        return isPart;
    }
}

