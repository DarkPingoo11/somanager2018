package fr.eseo.ld.objets.exportexcel;

public class ENJMatiere {

    private String libelle;
    private Float coefficient;

    public ENJMatiere() {
    }

    public ENJMatiere(String libelle, Float coefficient) {
        this.libelle = libelle;
        this.coefficient = coefficient;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public Float getCoefficient() {
        return coefficient;
    }

    public void setCoefficient(Float coefficient) {
        this.coefficient = coefficient;
    }
}
