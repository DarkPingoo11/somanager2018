package fr.eseo.ld.objets.exportexcel;

public enum ENJParams {

    COL_ID_UTILISATEUR,
    COL_NOM_UTILISATEUR,
    COL_NOTE,
    COL_COMMENTAIRE,
    COL_MOYENNE,
    COL_GRADE
}
