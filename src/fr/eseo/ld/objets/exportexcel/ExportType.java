package fr.eseo.ld.objets.exportexcel;

public enum ExportType {
    NORMAL(0),
    AUTOCOMPLETE(1);

    private int id;
    ExportType(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public static ExportType getExportFromId(int id) {
        for(ExportType type : ExportType.values()) {
            if(type.getId() == id) {
                return type;
            }
        }
        return null;
    }
}
