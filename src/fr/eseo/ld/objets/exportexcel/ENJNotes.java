package fr.eseo.ld.objets.exportexcel;

import java.util.HashMap;
import java.util.Map;

/**
 * Contient les différentes notes d'un utilisateur
 */
public class ENJNotes {

    private Map<ENJMatiere, Float> notes;

    public ENJNotes() {
        this.notes = new HashMap<>();
    }

    public Map<ENJMatiere, Float> getNotes() {
        return notes;
    }

    public void setNotes(Map<ENJMatiere, Float> notes) {
        this.notes = notes;
    }

    public void addNote(ENJMatiere matiere, Float note) {
        this.notes.put(matiere, note);
    }
}
