package fr.eseo.ld.objets.sql;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.LineNumberReader;
import java.io.Reader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class SqlScriptRunner {

    private static final String DEFAULT_DELIMETER = ";";
    private static Logger logger = Logger.getLogger(SqlScriptRunner.class.getName());

    private final Connection connection;

    /**
     * Lecteur de script sql
     * @param connection Connexion a la db
     */
    public SqlScriptRunner(final Connection connection) {
        this.connection = connection;
    }

    /**
     * Execute le script SQL
     * @param reader Script.Sql
     */
    public boolean runScript(final Reader reader) {
        return this.runScript(this.connection, reader);
    }

    /**
     * Execute le script SQL
     * @param connection Connexion
     * @param reader Script SQL
     */
    private boolean runScript(final Connection connection, final Reader reader) {
        List<String> requetes = formatString(reader);
        int erreurs = 0;

        for (String script : requetes) {
            try (PreparedStatement statement = connection.prepareStatement(script)){
                statement.execute();
            } catch (SQLException e) {
                erreurs++;
                logger.log(Level.WARN, "Erreur de requête : " + script, e);
            }
        }

        return erreurs == 0;
    }

    /**
     * Décode le fichier de script (prise en charge de DELIMITER)
     * @param reader Script
     * @return Liste de queries
     */
    private List<String> formatString(final Reader reader) {
        List<String> queries = new ArrayList<>();

        String line;
        String delimiter = DEFAULT_DELIMETER;
        final LineNumberReader lineReader = new LineNumberReader(reader);

        try {
            StringBuilder result = new StringBuilder();
            while ((line = lineReader.readLine()) != null) {
                //Si ce n'est pas un commentaire
                if (!line.startsWith("--") && line.trim().length() > 0) {

                    //Retrait des commentaires
                    if (line.contains("--")) {
                        line = line.substring(0, line.indexOf("--"));
                    }

                    //Ajout de la ligne
                    ajoutDeLaLigne(line, result);


                    //Si c'est la fin de la requete, on ajoute au tableau
                    if (line.endsWith(delimiter)) {
                        queries.add(result.toString().substring(0, result.toString().length() - delimiter.length()));
                        result = new StringBuilder();
                    }

                    //Si c'est un DELIMITER
                    if (line.startsWith("DELIMITER")) {
                        delimiter = line.replace("DELIMITER ", "");
                        result = new StringBuilder();
                    }
                }
            }
        } catch (IOException ex) {
            logger.log(Level.ERROR, "Erreur de lecture du fichier", ex);
        }

        return queries;
    }

    /**
     * Ajoute la ligne avec un espace si la requete en necessite un
     * @param line Ligne
     * @param result StringBuilder
     */
    private void ajoutDeLaLigne(String line, StringBuilder result) {
        result.append(result.length() > 0 ? " " : "").append(line);
    }
}
