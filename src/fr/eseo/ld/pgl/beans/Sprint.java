package fr.eseo.ld.pgl.beans;
/**
 * Classe du bean Sprint.
 *
 * <p>
 * Définition des attributs de l'objet Sprint ainsi que des accesseurs et
 * des mutateurs.
 * </p>
 *
 **/
public class Sprint {
	private Long idSprint;
	private Integer numero;
    private String dateDebut;
    private String dateFin;
    private Integer nbrHeures;
    private Float coefficient;
    private Long refAnnee;
    private Boolean notesPubliees;


    /**
     * Constructeur par défaut.
     */
    public Sprint() {
        super();
    }

    /* Accesseurs et Mutateurs */

    /**
     * Accessseur pour récupérer l'ID du sprint
     *
     * @return idSprint
     */
    public Long getIdSprint() {
        return idSprint;
    }

    /**
     * Mutateur pour modifier l'ID du sprint
     *
     * @param idSprint
     */
    public void setIdSprint(Long idSprint) {
        this.idSprint = idSprint;
    }

    /**
     * Accessseur pour récupérer la date de debut du sprint
     *
     * @return dateDebut
     */
    public String getDateDebut() {
        return dateDebut;
    }

    /**
     * Mutateur pour modifier la date de debut du sprint
     *
     * @param dateDebut
     */
    public void setDateDebut(String dateDebut) {
        this.dateDebut = dateDebut;
    }

    /**
     * Accessseur pour récupérer la date de fin du sprint
     *
     * @return dateFin
     */
    public String getDateFin() {
        return dateFin;
    }

    /**
     * Mutateur pour modifier la date de fin du sprint
     *
     * @param dateFin
     */
    public void setDateFin(String dateFin) {
        this.dateFin = dateFin;
    }

    /**
     * Accessseur pour récupérer le nombre d'heure du sprint
     *
     * @return nbrHeures
     */
    public Integer getNbrHeures() {
        return nbrHeures;
    }

    /**
     * Mutateur pour modifier le nombre d'heure du sprint
     *
     * @param nbrHeures
     */
    public void setNbrHeures(Integer nbrHeures) {
        this.nbrHeures = nbrHeures;
    }

    /**
     * Accessseur pour récupérer le coefficient du sprint
     *
     * @return coefficient
     */
    public Float getCoefficient() {
        return coefficient;
    }

    /**
     * Mutateur pour modifier le coefficient du sprint
     *
     * @param coefficient
     */
    public void setCoefficient(Float coefficient) {
        this.coefficient = coefficient;
    }

    /**
     * Accessseur pour récupérer la ref de l'annee du sprint
     *
     * @return annee
     */
    public Long getRefAnnee() {
        return refAnnee;
    }

    /**
     * Mutateur pour modifier la ref de l'annee du sprint
     *
     * @param refAnnee
     */
    public void setRefAnnee(Long refAnnee) {
        this.refAnnee = refAnnee;
    }

    /**
     * Redéfinition de la méthode toString().
     */
    @Override
    public String toString() {
        return "Sprint{" +
                "idSprint=" + idSprint +
                ", dateDebut=" + dateDebut +
                ", dateFin=" + dateFin +
                ", nbrHeures=" + nbrHeures +
                ", coefficient=" + coefficient +
                ", refAnnee=" + refAnnee +
                '}';
    }


    public Integer getNumero() {
        return numero;
    }

    public void setNumero(Integer numero) {
        this.numero = numero;
    }

    public Boolean getNotesPubliees() {
        return notesPubliees;
    }

    public void setNotesPubliees(Boolean notesPubliees) {
        this.notesPubliees = notesPubliees;
    }
}
