package fr.eseo.ld.pgl.beans;

/**
 * Classe du bean Equipe.
 *
 * <p>
 * Définition des attributs de l'objet Equipe ainsi que des accesseurs et
 * des mutateurs.
 * </p>
 *
 **/
public class Equipe {
	
	private Long idEquipe;
	private String nom;
	private Long refProjet;
	private Long refAnnee;
	
	/**
     * Constructeur par défaut.
     */
	public Equipe() {
		super();
	}

	/**
     * Accessseur pour récupérer l'ID de l'Equipe
     *
     * @return idEquipe
     */
	public Long getIdEquipe() {
		return idEquipe;
	}

	/**
     * Mutateur pour modifier l'ID de l'Equipe
     *
     * @param idEquipe
     */
	public void setIdEquipe(Long idEquipe) {
		this.idEquipe = idEquipe;
	}

	/**
     * Accessseur pour récupérer le nom de l'Equipe
     *
     * @return nom
     */
	public String getNom() {
		return nom;
	}
	
	/**
     * Mutateur pour modifier le nom de l'Equipe
     *
     * @param nom
     */
	public void setNom(String nom) {
		this.nom = nom;
	}
	
	/**
     * Accessseur pour récupérer la ref du Projet de l'Equipe
     *
     * @return refProjet
     */
	public Long getRefProjet() {
		return refProjet;
	}
	
	/**
     * Mutateur pour modifier la ref du projet de l'Equipe
     *
     * @param refProjet
     */
	public void setRefProjet(Long refProjet) {
		this.refProjet = refProjet;
	}

	/**
     * Accessseur pour récupérer la ref de l'annee de l'Equipe
     *
     * @return refAnnee
     */
	public Long getRefAnnee() {
		return refAnnee;
	}

	/**
     * Mutateur pour modifier la ref de l'annee de l'Equipe
     *
     * @param refAnnee
     *
     */
	public void setRefAnnee(Long refAnnee) {
		this.refAnnee = refAnnee;
	}
	


}
