package fr.eseo.ld.pgl.beans;
/**
 * Classe du bean CompositionJury.
 *
 * <p>
 * Définition des attributs de l'objet CompositionJury ainsi que des accesseurs et
 * des mutateurs.
 * </p>
 *
 **/
public class CompositionJury {
	
	private Long refSoutenance;
	private Long refProfesseur;
	
	/**
     * Constructeur par défaut.
     */
	public CompositionJury() {
		super();
	}
	
	/**
     * Accessseur pour récupérer la ref de la soutenance pr la composition du Jury
     *
     * @return refSoutenance
     */
	public Long getRefSoutenance() {
		return refSoutenance;
	}

	/**
     * Mutateur pour modifier la ref de la soutenance pr la composition du Jury
     *
     * @param refSoutenance
     */
	public void setRefSoutenance(Long refSoutenance) {
		this.refSoutenance = refSoutenance;
	}

	/**
     * Accessseur pour récupérer la ref du prof pr la composition du Jury
     *
     * @return refProfesseur
     */
	public Long getRefProfesseur() {
		return refProfesseur;
	}
	
	/**
     * Mutateur pour modifier la ref du prof pr la composition du Jury
     *
     * @param refProfesseur
     */
	public void setRefProfesseur(Long refProfesseur) {
		this.refProfesseur = refProfesseur;
	}

}
