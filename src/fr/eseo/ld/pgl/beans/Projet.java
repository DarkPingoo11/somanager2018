package fr.eseo.ld.pgl.beans;
/**
 * Classe du bean Projet.
 *
 * <p>
 * Définition des attributs de l'objet Projet ainsi que des accesseurs et
 * des mutateurs.
 * </p>
 *
 **/
public class Projet {
	
	private Long idProjet;
	private String titre;
	private String description;
	private Long idProfReferent;
	
	/**
     * Constructeur par défaut.
     */
	public Projet() {
		super();
	}
	/**
     * Accessseur pour récupérer l'ID du Projet
     *
     * @return idProjet
     */
	public Long getIdProjet() {
		return idProjet;
	}
	/**
     * Mutateur pour modifier l'ID du Projet
     *
     * @param idProjet
     */
	public void setIdProjet(Long idProjet) {
		this.idProjet = idProjet;
	}
	/**
     * Accessseur pour récupérer le titre du Projet
     *
     * @return titre
     */
	public String getTitre() {
		return titre;
	}
	/**
     * Mutateur pour modifier le titre du Projet
     *
     * @param titre 
     */
	public void setTitre(String titre) {
		this.titre = titre;
	}
	/**
     * Accessseur pour récupérer la description du Projet
     *
     * @return description
     */
	public String getDescription() {
		return description;
	}
	/**
     * Mutateur pour modifier la description du Projet
     *
     * @param description
     */
	public void setDescription(String description) {
		this.description = description;
	}
	
	/**
     * Accessseur pour récupérer l'ID du profReferent du Projet
     *
     * @return idProfReferent
     */
	public Long getIdProfReferent() {
		return idProfReferent;
	}
	/**
     * Mutateur pour modifier l'ID du profReferent du Projet
     *
     * @param idProfReferent
     */
	public void setIdProfReferent(Long idProfReferent) {
		this.idProfReferent = idProfReferent;
	}

}
