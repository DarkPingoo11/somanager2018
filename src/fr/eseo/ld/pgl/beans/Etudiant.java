package fr.eseo.ld.pgl.beans;
/**
 * Classe du bean Etudiant.
 *
 * <p>
 * Définition des attributs de l'objet Etudiant ainsi que des accesseurs et
 * des mutateurs.
 * </p>
 *
 **/
public class Etudiant {
	
	private Long idEtudiant;
	private Long refAnnee;

	/**
     * Constructeur par défaut.
     */
	public Etudiant() {
		super();
	}

	/**
     * Accessseur pour récupérer l'ID de l'Etudiant
     *
     * @return idEtudiant
     */
	public Long getIdEtudiant() {
		return idEtudiant;
	}

	/**
     * Mutateur pour modifier l'ID de l'Etudiant
     *
     * @param idEtudiant etudiant
     */
	public void setIdEtudiant(Long idEtudiant) {
		this.idEtudiant = idEtudiant;
	}

	/**
     * Accessseur pour récupérer la ref de l'annee de l'Etudiant
     *
     * @return refAnnee
     */
	public Long getRefAnnee() {
		return refAnnee;
	}

	/**
     * Mutateur pour modifier l'ID de l'Etudiant
     *
     * @param refAnnee annee
     */
	public void setRefAnnee(Long refAnnee) {
		this.refAnnee = refAnnee;
	}


}
