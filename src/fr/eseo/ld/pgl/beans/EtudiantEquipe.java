package fr.eseo.ld.pgl.beans;

/**
 * Classe du bean EtudiantEquipe.
 * 
 * <p>
 * Définition des attributs de l'objet EtudiantEquipe ainsi que des accesseurs
 * et des mutateurs.
 * </p>
 */
public class EtudiantEquipe {

	/* Attributs */
	private Long idEtudiant;
	private Long idEquipe;
	private Long idSprint;

	/**
	 * Constructeur par défaut.
	 */
	public EtudiantEquipe() {
		super();
	}


	public Long getIdEtudiant() {
		return idEtudiant;
	}

	public void setIdEtudiant(Long idEtudiant) {
		this.idEtudiant = idEtudiant;
	}

	public Long getIdEquipe() {
		return idEquipe;
	}

	public void setIdEquipe(Long idEquipe) {
		this.idEquipe = idEquipe;
	}

	public Long getIdSprint() {
		return idSprint;
	}

	public void setIdSprint(Long idSprint) {
		this.idSprint = idSprint;
	}
}