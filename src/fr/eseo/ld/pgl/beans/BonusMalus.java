package fr.eseo.ld.pgl.beans;
/**
 * Classe du bean BonusMalus.
 *
 * <p>
 * Définition des attributs de l'objet BonusMalus ainsi que des accesseurs et
 * des mutateurs.
 * </p>
 *
 **/
public class BonusMalus {
	
	public enum VALIDATION_TYPE {ATTENTE, REFUSER, ACCEPTER}
	
	private Long idBonusMalus;
	private Float valeur; 
	private String validation;
	private String justification;
	private int refSprint;
	private int refEtudiant;
	private int refEvaluateur;
	
	/**
     * Constructeur par défaut.
     */
	public BonusMalus() {
		super();
	}
	
	/**
     * Accessseur pour récupérer l'ID du BonusMalus
     *
     * @return idBonusMalus
     */
	public Long getIdBonusMalus() {
		return idBonusMalus;
	}


	/**
     * Mutateur pour modifier l'ID du BonusMalus
     *
     * @param idBonusMalus
     */
	public void setIdBonusMalus(Long idBonusMalus) {
		this.idBonusMalus = idBonusMalus;
	}
	

	/**
     * Accessseur pour récupérer la valeur du BonusMalus
     *
     * @return valeur
     */
	public Float getValeur() {
		return valeur;
	}

	/**
     * Mutateur pour modifier la valeur du BonusMalus
     *
     * @param valeur
     */
	public void setValeur(Float valeur) {
		this.valeur = valeur;
	}
	
	/**
     * Accessseur pour récupérer l'etat du BonusMalus
     *
     * @return attente
     */
	public String getValidation() {
		return validation;
	}
	
	/**
     * Mutateur pour modifier l'etat du BonusMalus
     *
     */
	public void setValidation(String validationTypeString) {
	    for(VALIDATION_TYPE v : VALIDATION_TYPE.values()) {
	        if(v.toString().equalsIgnoreCase(validationTypeString)) {
	            this.validation = v.toString();
	            return;
            }
        }
	}

	public void setValidation(VALIDATION_TYPE validation) {
		this.validation = validation.toString();
	}

	/**
     * Accessseur pour récupérer la valeur du BonusMalus
     *
     * @return justification
     */
	public String getJustification() {
		return justification;
	}

	/**
     * Mutateur pour modifier la valeur du BonusMalus
     *
     * @param justification
     */
	public void setJustification(String justification) {
		this.justification = justification;
	}
	
	/**
     * Accessseur pour récupérer la ref de la note de l'equipe 
     *
     * @return refSprint
     */
	public int getRefSprint() {
		return refSprint;
	}

	/**
     * Mutateur pour modifier la ref de la note de l'equipe
     *
     * @param refSprint
     */
	public void setRefSprint(int refSprint) {
		this.refSprint = refSprint;
	}

	/**
     * Accessseur pour récupérer la ref de l'etudiant
     *
     * @return refEtudiant
     */
	public int getRefEtudiant() {
		return refEtudiant;
	}
	
	/**
     * Mutateur pour modifier la ref de l'etudiant 
     *
     * @param refEtudiant
     */
	public void setRefEtudiant(int refEtudiant) {
		this.refEtudiant = refEtudiant;
	}

	/**
     * Accessseur pour récupérer la ref de l'utilisateur 
     *
     * @return refEvaluateur
     */
	public int getRefEvaluateur() {
		return refEvaluateur;
	}

	/**
     * Mutateur pour modifier la ref de l'utilisateur 
     *
     * @param refEvaluateur
     */
	public void setRefEvaluateur(int refEvaluateur) {
		this.refEvaluateur = refEvaluateur;
	}
	
}
