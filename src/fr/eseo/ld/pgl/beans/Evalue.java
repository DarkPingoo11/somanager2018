package fr.eseo.ld.pgl.beans;
/**
 * Classe du bean Evalue.
 *
 * <p>
 * Définition des attributs de l'objet Evalue ainsi que des accesseurs et
 * des mutateurs.
 * </p>
 *
 **/
public class Evalue {

	
	private Long refSoutenance; 
	private Long refEquipe; 
	
	/**
     * Constructeur par défaut.
     */
	public Evalue() {
		super();
	}

	/**
     * Accessseur pour récupérer la ref de la soutenance
     *
     * @return refSoutenance
     */
	public Long getRefSoutenance() {
		return refSoutenance;
	}

	/**
     * Mutateur pour récupérer la ref de la soutenance
     *
     * @param refSoutenance
     */
	public void setRefSoutenance(Long refSoutenance) {
		this.refSoutenance = refSoutenance;
	}

	/**
     * Accessseur pour récupérer la ref de l'equipe
     *
     * @return refEquipe
     */
	public Long getRefEquipe() {
		return refEquipe;
	}

	/**
     * Mutateur pour modifier la ref de l'equipe
     *
     * @param refEquipe
     */
	public void setRefEquipe(Long refEquipe) {
		this.refEquipe = refEquipe;
	}
	  
}
