package fr.eseo.ld.pgl.beans;
/**
 * Classe du bean Soutenance.
 *
 * <p>
 * Définition des attributs de l'objet Soutenance ainsi que des accesseurs et
 * des mutateurs.
 * </p>
 *
 **/
public class Soutenance {
	
	private Long idSoutenance;
	private String date;
	private String libelle;
	
	/**
     * Constructeur par défaut.
     */
	public Soutenance() {
		super();
	}
	/**
     * Accessseur pour récupérer l'ID de la Soutenance
     *
     * @return idSoutenance
     */
	public Long getIdSoutenance() {
		return idSoutenance;
	}
	
	/**
     * Mutateur pour modifier l'ID de la Soutenance
     *
     * @param idSoutenance
     */
	public void setIdSoutenance(Long idSoutenance) {
		this.idSoutenance = idSoutenance;
	}

	/**
     * Accessseur pour récupérer la date de la Soutenance
     *
     * @return date
     */
	public String getDate() {
		return date;
	}

	/**
     * Mutateur pour modifier la date de la Soutenance
     *
     * @param date
     */
	public void setDate(String date) {
		this.date = date;
	}

	/**
     * Accessseur pour récupérer le libelle de la Soutenance
     *
     * @return libelle
     */
	public String getLibelle() {
		return libelle;
	}

	/**
     * Mutateur pour modifier le libelle de la Soutenance
     *
     * @param libelle 
     */
	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

}
