package fr.eseo.ld.pgl.beans;
/**
 * Classe du bean Matiere.
 *
 * <p>
 * Définition des attributs de l'objet Matiere ainsi que des accesseurs et
 * des mutateurs.
 * </p>
 *
 **/

public class Matiere {
	
	private Long idMatiere;
	private String libelle;
	private Float coefficient;
	private Long refSprint;
	
	 /**
     * Constructeur par défaut.
     */
    public Matiere() {
        super();
    }
    /**
     * Accessseur pour récupérer l'ID de la matiere
     *
     * @return idMatiere
     */
	public Long getIdMatiere() {
		return idMatiere;
	}
	/**
     * Mutateur pour modifier l'ID de la matiere 
     *
     * @param idMatiere
     */
	public void setIdMatiere(Long idMatiere) {
		this.idMatiere = idMatiere;
	}
	/**
     * Accessseur pour récupérer le libelle de la matiere
     *
     * @return libelle
     */
	public String getLibelle() {
		return libelle;
	}
	/**
     * Mutateur pour modifier le libelle de la matiere
     *
     * @param libelle
     */
	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}
	/**
     * Accessseur pour récupérer le coeff de la matiere
     *
     * @return coefficient
     */
	public Float getCoefficient() {
		return coefficient;
	}
	/**
     * Mutateur pour modifier le coeff de la matiere
     *
     * @param coefficient
     */
	public void setCoefficient(Float coefficient) {
		this.coefficient = coefficient;
	}
	/**
     * Accessseur pour récupérer la reference de la matiere du sprint
     *
     * @return refSprint
     */
	public Long getRefSprint() {
		return refSprint;
	}
	
	/**
     * Mutateur pour modifier la reference du sprint de la matiere
     *
     * @param refSprint 
     */
	public void setRefSprint(Long refSprint) {
		this.refSprint = refSprint;
	}
	

}
