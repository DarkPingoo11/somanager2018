package fr.eseo.ld.pgl.beans;

//Beans, les attributs correspondent aux noms des colonnes en BDD
@SuppressWarnings({"squid:S1700"})
public class Note {

    private Long idNote;
    private Float note;
    private Long refEvaluateur;
    private Long refEtudiant;
    private Long refMatiere;

    public Note() { super(); }

    public Long getIdNote() {
        return idNote;
    }

    public void setIdNote(Long idNote) {
        this.idNote = idNote;
    }

    public Float getNote() {
        return note;
    }

    public void setNote(Float note) {
        this.note = note;
    }

    public Long getRefEvaluateur() {
        return refEvaluateur;
    }

    public void setRefEvaluateur(Long refEvaluateur) {
        this.refEvaluateur = refEvaluateur;
    }

    public Long getRefEtudiant() {
        return refEtudiant;
    }

    public void setRefEtudiant(Long refEtudiant) {
        this.refEtudiant = refEtudiant;
    }

    public Long getRefMatiere() {
        return refMatiere;
    }

    public void setRefMatiere(Long refMatiere) {
        this.refMatiere = refMatiere;
    }
}
