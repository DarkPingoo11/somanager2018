package fr.eseo.ld.pgl.beans;
/**
 * Classe du bean RoleEquipe.
 *
 * <p>
 * Définition des attributs de l'objet RoleEquipe ainsi que des accesseurs et
 * des mutateurs.
 * </p>
 *
 **/
public class RoleEquipe {
	
	private Long refEquipe; 
	private Long refUtilisateur; 
	private Long refRole;

	/**
     * Constructeur par défaut.
     */
	public RoleEquipe() {
		super();
	}

	/**
     * Accessseur pour récupérer la ref de l'equipe
     *
     * @return refEquipe
     */
	public Long getRefEquipe() {
		return refEquipe;
	}

	/**
     * Mutateur pour modifier la ref de l'equipe
     *
     * @param refEquipe
     */
	public void setRefEquipe(Long refEquipe) {
		this.refEquipe = refEquipe;
	}

	/**
     * Accessseur pour récupérer la ref de l'utilisateur
     *
     * @return refUtilisateur
     */
	public Long getRefUtilisateur() {
		return refUtilisateur;
	}

	/**
     * Mutateur pour modifier la ref de l'utilisateur
     *
     * @param refUtilisateur
     */
	public void setRefUtilisateur(Long refUtilisateur) {
		this.refUtilisateur = refUtilisateur;
	}

	/**
     * Accessseur pour récupérer la ref du role
     *
     * @return refRole
     */
	public Long getRefRole() {
		return refRole;
	}

	/**
     * Mutateur pour modifier la ref du role
     *
     * @param refRole
     */
	public void setRefRole(Long refRole) {
		this.refRole = refRole;
	}
	
}
