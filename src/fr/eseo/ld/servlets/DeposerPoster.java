package fr.eseo.ld.servlets;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Properties;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import fr.eseo.ld.beans.Equipe;
import fr.eseo.ld.beans.Poster;
import fr.eseo.ld.beans.Utilisateur;
import fr.eseo.ld.dao.DAOFactory;
import fr.eseo.ld.dao.EquipeDAO;
import fr.eseo.ld.dao.NotificationDAO;
import fr.eseo.ld.dao.PosterDAO;

/**
 * Servlet permettant l'ajout d'un Poster par un utilisateur.
 */
@WebServlet("/DeposerPoster")
@MultipartConfig
public class DeposerPoster extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected static final String VUE_SUJET_SUIVI = "/WEB-INF/formulaires/sujetSuivi.jsp";
	protected static final String FICHIER_PROPERTIES_STOCKAGE = "fr/eseo/ld/config/stockage.properties";
	protected static final String PROPERTY_CHEMIN_ENREG_POSTERS = "CHEMIN_ENREG_POSTERS";
	protected static final String PROPERTY_TAILLE_TAMPON_FICHIER = "TAILLE_TAMPON_FICHIER";

	// chemin d'enregistrement de poster, pour le serveur par défaut:
	// /home/etudiant/Posters
	protected static final String CHEMIN_ENREG_POSTER_DEFAUT = "/home/etudiant/Posters";
	protected static String cheminEnregPoster;

	protected static final int TAILLE_TAMPON = 10240; // 10 ko par défaut
	protected static int tailleTampon;

	protected PosterDAO posterDAO;
	protected NotificationDAO notificationDAO;
	protected EquipeDAO equipeDAO;

	private static Logger logger = Logger.getLogger(DeposerPoster.class.getName());

	@Override
	public void init() throws ServletException {
		/* Récupération d'instances de DAO */
		DAOFactory daoFactory = (DAOFactory) this.getServletContext().getAttribute(ServletUtilitaire.CONF_DAO_FACTORY);
		this.posterDAO = daoFactory.getPosterDao();
		this.notificationDAO = daoFactory.getNotificationDao();
		this.equipeDAO = daoFactory.getEquipeDAO();

		/*
		 * Récupération des infos d'enregistrement des posters à partir de
		 * annuaire.properties
		 */
		DeposerPoster.recupererInfosPosters(FICHIER_PROPERTIES_STOCKAGE);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		/* Récupération de la session depuis la requête */
		HttpSession session = request.getSession();

		/* Récupération de l'ID de l'Utilisateur en session */
		Utilisateur utilisateurSession = (Utilisateur) session.getAttribute(ServletUtilitaire.ATT_SESSION_USER);
		Long idUtilisateurSession = utilisateurSession.getIdUtilisateur();

		// on retrouve l'équipe de l'utilisateur afin de pouvoir ensuite retrouver l'id
		// du sujet sur lequel il travaille
		Equipe equipePFE = this.equipeDAO.trouver(idUtilisateurSession);

		Poster poster = new Poster();
		/*
		 * Les données reçues sont multipart, on doit donc utiliser la méthode getPart()
		 * pour traiter le champ d'envoi de fichiers.
		 */

		Part part = request.getPart("poster");

		/*
		 * création/récupération des attributs qui seront attribués au poster dans la
		 * BDD
		 */

		Long idSujet = equipePFE.getIdSujet();
		SimpleDateFormat formatDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		String date = formatDate.format(new Date());

		SimpleDateFormat formatAnnee = new SimpleDateFormat("yyyy");
		String annee = formatAnnee.format(new Date());
		String valide = "non";
		String idEquipe = equipePFE.getIdEquipe();

		/*
		 * Il faut déterminer s'il s'agit d'un champ classique ou d'un champ de type
		 * fichier : on délègue cette opération à la méthode utilitaire getNomPoster().
		 */
		String nomPoster = getNomPoster(part, String.valueOf(idSujet));

		// création du fichier de stockage annuel s'il n'existe pas encore (cas du
		// premier poster de l'année)
		File file = new File(cheminEnregPoster, annee);
		if (!file.exists()) {
			file.mkdirs();
		}

		/* ecriture du poster sur le disque si le nom n'est pas null et au bon format */
		if (nomPoster != null && !nomPoster.isEmpty() && bonFormatPoster(nomPoster)) {

			// verification que le poster à déposer n'existe pas déjà à ce nom là
			while (nomPosterExistant(nomPoster, cheminEnregPoster + "/" + annee)) {
				nomPoster = nomPoster.concat("1");
			}

			/* Écriture du fichier sur le disque */
			ecrirePoster(part, nomPoster, cheminEnregPoster + "/" + annee);

			/* Insertion du poster dans la BDD via posterDAO */
			poster.setDatePoster(date);
			poster.setIdSujet(idSujet);
			poster.setChemin(cheminEnregPoster + "/" + annee + "/" + nomPoster);
			poster.setValide(valide);
			poster.setIdEquipe(idEquipe);
			this.posterDAO.creer(poster);

			/* Ajout d'une notification */
			this.notificationDAO.ajouterNotification(idUtilisateurSession, "Votre poster a bien été ajouté");

		} else {
			/* Ajout d'une notification */
			this.notificationDAO.ajouterNotification(idUtilisateurSession, "une erreur est apparue");
		}

		/* Redirection vers la page de dépot de sujet */
		response.sendRedirect(request.getHeader(ServletUtilitaire.HEADER));
	}

	/*
	 * Méthode utilitaire qui a pour unique but d'analyser l'en-tête
	 * "content-disposition", et de vérifier si le paramètre "filename" y est
	 * présent. Si oui, alors le champ traité est de type File et la méthode
	 * retourne son nom avec en entète l'id du sujet concerné, sinon il s'agit d'un
	 * champ de formulaire classique et la méthode retourne null.
	 */
	protected static String getNomPoster(Part part, String idSujet) {
		/* Boucle sur chacun des paramètres de l'en-tête "content-disposition". */
		for (String contentDisposition : part.getHeader("content-disposition").split(";")) {
			/* Recherche de l'éventuelle présence du paramètre "filename". */
			if (contentDisposition.trim().startsWith("filename")) {
				/*
				 * Si "filename" est présent, alors renvoi de sa valeur, c'est-à-dire du nom de
				 * fichier.
				 */
				String nomPoster = contentDisposition.substring(contentDisposition.indexOf('=') + 1).trim()
						.replace("\"", "");

				/*
				 * Antibug pour Internet Explorer, qui transmet pour une raison mystique le
				 * chemin du fichier local à la machine du client...
				 */

				nomPoster = nomPoster.substring(nomPoster.lastIndexOf('/') + 1)
						.substring(nomPoster.lastIndexOf('\\') + 1);
				nomPoster = idSujet + "_" + nomPoster;
				return nomPoster;
			}
		}
		/* Et pour terminer, si rien n'a été trouvé... */
		return null;
	}

	/*
	 * Méthode utilitaire qui a pour but de vérifier que le fichier déposé est bien
	 * au format pdf.
	 */

	protected boolean bonFormatPoster(String nomPoster) {
		return "pdf".equalsIgnoreCase(
				nomPoster.substring(nomPoster.lastIndexOf('.') + 1).substring(nomPoster.lastIndexOf('\\') + 1));
	}

	/*
	 * Méthode utilitaire qui a pour but d'écrire le poster passé en paramètre sur
	 * le disque, dans le répertoire donné et avec le nom donné.
	 */
	protected void ecrirePoster(Part part, String nomPoster, String chemin){

		File newFichier = new File(chemin, nomPoster);
		try(InputStream inStream = part.getInputStream();OutputStream out = new FileOutputStream(newFichier)) {
			int read;
			byte[] bytes = new byte[DeposerPoster.tailleTampon];

			while ((read = inStream.read(bytes)) != -1) {
				out.write(bytes, 0, read);
			}
		} catch (IOException e) {
			logger.warn("Erreur lors de l'écriture du poster", e);
		}
	}

	protected boolean nomPosterExistant(String nomPoster, String chemin) {
		File dossierPosters = new File(chemin);
		File[] listeDePosters = dossierPosters.listFiles();
		ArrayList<String> nomsPosters = new ArrayList<>();
		if (listeDePosters != null) {
			for (File poster : listeDePosters) {
				if (poster.isFile()) {
					nomsPosters.add(poster.getName());
				}
			}

			for (String nomDePoster : nomsPosters) {
				if (nomDePoster.equals(nomPoster)) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Lit le fichier de propriété pour récupérer le chemin d'enregistrement des
	 * posters et la taille maximale des posters.
	 * 
	 * @param fichierURL
	 *            l'URL du fichier.
	 * 
	 */
	private static void recupererInfosPosters(String fichierURL) {
		Properties properties = new Properties();
		ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
		InputStream fichierProperties = classLoader.getResourceAsStream(fichierURL);
		try {
			properties.load(fichierProperties);

			cheminEnregPoster = properties.getProperty(PROPERTY_CHEMIN_ENREG_POSTERS);
			tailleTampon = Integer.parseInt(properties.getProperty(PROPERTY_TAILLE_TAMPON_FICHIER));

		} catch (IOException e) {
			logger.log(Level.WARN, "Echec de la lecture du fichier de propriété de stockage: " + e.getMessage(), e);
			cheminEnregPoster = CHEMIN_ENREG_POSTER_DEFAUT;
		}
	}

}