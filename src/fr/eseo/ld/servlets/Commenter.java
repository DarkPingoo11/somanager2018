package fr.eseo.ld.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.eseo.ld.beans.Commentaire;
import fr.eseo.ld.beans.Utilisateur;
import fr.eseo.ld.dao.CommentaireDAO;
import fr.eseo.ld.dao.DAOFactory;
import fr.eseo.ld.dao.NotificationDAO;

/**
 * Servlet permettant à l'utilisateur de commenter un sujet.
 */
@WebServlet("/Commenter")
public class Commenter extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private NotificationDAO notificationDAO;
	private CommentaireDAO commentaireDAO;

	@Override
	public void init() throws ServletException {
		/* Récupération d'instances de DAO */
		DAOFactory daoFactory = (DAOFactory) this.getServletContext().getAttribute(ServletUtilitaire.CONF_DAO_FACTORY);
		this.notificationDAO = daoFactory.getNotificationDao();
		this.commentaireDAO = daoFactory.getCommentaireDao();
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		/* Récupération de la session depuis la requête */
		HttpSession session = request.getSession();

		/* Récupération d'attributs de l'Utilisateur en session */
		Utilisateur utilisateur = (Utilisateur) session.getAttribute(ServletUtilitaire.ATT_SESSION_USER);
		Long idUtilisateur = utilisateur.getIdUtilisateur();
		String nomExpediteur = utilisateur.getPrenom() + " " + utilisateur.getNom();

		/* Récupération des données saisies */
		String contenu = request.getParameter(ServletUtilitaire.CHAMP_CONTENU);
		Long idSujet = Long.parseLong(request.getParameter(ServletUtilitaire.CHAMP_ID_SUJET));
		Long idObservation = Long.parseLong(request.getParameter(ServletUtilitaire.CHAMP_ID_OBSERVATION));
		Long idUtilisateurNotif = Long.parseLong(request.getParameter(ServletUtilitaire.CHAMP_ID_UTILISATEUR));
		String titreSujet = request.getParameter(ServletUtilitaire.CHAMP_TITRE_SUJET);

		/* Insertion du commentaire dans la BDD via commentaireDAO */
		Commentaire commentaire = new Commentaire();
		commentaire.setContenu(contenu);
		commentaire.setIdUtilisateur(idUtilisateur);
		commentaire.setIdSujet(idSujet);
		commentaire.setIdObservation(idObservation);
		this.commentaireDAO.creer(commentaire);

		/* Ajout d'une notification */
		this.notificationDAO.ajouterNotification(idUtilisateurNotif, nomExpediteur, " a commenté sur ", titreSujet);

		/* Redirection vers la page précédente */
		response.sendRedirect(request.getHeader(ServletUtilitaire.HEADER));
	}

}