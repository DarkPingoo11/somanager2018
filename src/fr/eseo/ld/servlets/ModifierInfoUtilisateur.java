package fr.eseo.ld.servlets;

import fr.eseo.ld.beans.Utilisateur;
import fr.eseo.ld.dao.DAOFactory;
import fr.eseo.ld.dao.OptionESEODAO;
import fr.eseo.ld.dao.UtilisateurDAO;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Servlet permettant la modification de certaines informations d'un utilisateur
 */
@WebServlet("/ModifierInfoUtilisateur")
@MultipartConfig()
public class ModifierInfoUtilisateur extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private static final String VUE_FORM = "/WEB-INF/formulaires/modifierInfoUtilisateur.jsp";
	private static final String CHEMIN_FICHIER = "/images/users";
	private UtilisateurDAO utilisateurDAO;
	private OptionESEODAO optionDAO;
	private static Logger logger = Logger.getLogger(ModifierInfoUtilisateur.class.getName());

	@Override
	public void init() throws ServletException {
		/* Récupération d'instances de DAO */
		DAOFactory daoFactory = (DAOFactory) this.getServletContext().getAttribute(ServletUtilitaire.CONF_DAO_FACTORY);
		this.utilisateurDAO = daoFactory.getUtilisateurDao();
		this.optionDAO = daoFactory.getOptionESEODAO();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		/* A la réception d'une requête GET, simple affichage du formulaire */
		HttpSession session = request.getSession();

		/* Récupération de l'utilisateur en session */
		Utilisateur utilisateurSession = (Utilisateur) session.getAttribute(ServletUtilitaire.ATT_SESSION_USER);

		request.setAttribute("listeOptions", this.optionDAO.trouverOptionUtilisateur(utilisateurSession));
		this.getServletContext().getRequestDispatcher(VUE_FORM).forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		/* Récupération de la session depuis la requête */
		HttpSession session = request.getSession();

		/* Récupération de l'utilisateur en session */
		Utilisateur utilisateurSession = (Utilisateur) session.getAttribute(ServletUtilitaire.ATT_SESSION_USER);

		//Récupération de la photo utilisateur
		Part photo = request.getPart("file_photo");
		String chemin = getServletContext().getRealPath(CHEMIN_FICHIER);
		File photoUtilisateur = new File(chemin, utilisateurSession.getIdUtilisateur()+".jpg");
		try(InputStream photoContent = photo.getInputStream()){
			FileUtils.copyInputStreamToFile(photoContent, photoUtilisateur);
		} catch(Exception e) {
			logger.log(Level.WARN, "Echec de création du fichier contenant la photo de l'utilisateur", e);
		}


		//Création De l'utilisateur modifié
		String prevNom = utilisateurSession.getNom();
		String prevPrenom = utilisateurSession.getPrenom();
		String prevEmail = utilisateurSession.getEmail();

		Utilisateur utilisateurModifie = utilisateurSession;
		utilisateurModifie.setEmail(request.getParameter("email"));
		utilisateurModifie.setNom(request.getParameter("nom"));
		utilisateurModifie.setPrenom(request.getParameter("prenom"));

		Map<String, String> erreurs = verificationInfoUtilisateur(utilisateurDAO, utilisateurModifie.getEmail(),
				utilisateurModifie.getNom(), utilisateurModifie.getPrenom(), utilisateurSession.getIdUtilisateur());
		String resultat;
		if(erreurs.isEmpty()) {
			utilisateurDAO.modifier(utilisateurModifie);
			session.setAttribute(ServletUtilitaire.ATT_SESSION_USER, utilisateurModifie);
		} else {
			resultat = "Échec du changement des informations. " + erreurs;
			logger.log(Level.WARN, resultat);
			request.setAttribute(ServletUtilitaire.ATT_RESULTAT, resultat);
			request.setAttribute(ServletUtilitaire.ATT_ERREURS, erreurs);
			utilisateurSession.setEmail(prevEmail);
			utilisateurSession.setNom(prevNom);
			utilisateurSession.setPrenom(prevPrenom);
			/* Ré-affichage du formulaire avec les erreurs */
		}

		doGet(request, response);
	}

	private Map<String, String> verificationInfoUtilisateur(UtilisateurDAO utilisateurDAO, String email, String nom, String prenom, Long idUtilisateur){
		//Test de l'adresse email
		Utilisateur utilisateur = new Utilisateur();
		utilisateur.setEmail(email);
		utilisateur.setNom(nom);
		utilisateur.setPrenom(prenom);
		List<Utilisateur> utilisateurs = utilisateurDAO.trouver(utilisateur);
		Map<String, String> erreurs = new HashMap<>();
		if(!utilisateurs.isEmpty() && utilisateurs.get(0).getIdUtilisateur().intValue() != idUtilisateur.intValue()) {
			erreurs.put(ServletUtilitaire.CHAMP_EMAIL, "Email déjà utilisée");
		}
		if(!verificationChaineString(utilisateur.getNom())){
			erreurs.put(ServletUtilitaire.CHAMP_NOM, "Un nom ne contient que des lettres");
		}
		if(!verificationChaineString(utilisateur.getPrenom())) {
			erreurs.put(ServletUtilitaire.CHAMP_PRENOM, "Un nom ne contient que des lettres");
		}
		return erreurs;
	}

	/**
	 * Verifie que la chaine de caractères est composée uniquement de lettres
	 * @param chaine string
	 * @return vrai si vrai
	 */
	private boolean verificationChaineString(String chaine) {
		boolean estUnString = false;
		if(chaine.matches("[a-zA-Z]")) {
			estUnString = true;
		}
		return estUnString;
	}









}
