package fr.eseo.ld.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.eseo.ld.beans.Commentaire;
import fr.eseo.ld.beans.EtatSujet;
import fr.eseo.ld.beans.Etudiant;
import fr.eseo.ld.beans.OptionESEO;
import fr.eseo.ld.beans.Poster;
import fr.eseo.ld.beans.ProfesseurSujet;
import fr.eseo.ld.beans.Sujet;
import fr.eseo.ld.beans.Utilisateur;
import fr.eseo.ld.dao.CommentaireDAO;
import fr.eseo.ld.dao.DAOFactory;
import fr.eseo.ld.dao.EtudiantDAO;
import fr.eseo.ld.dao.OptionESEODAO;
import fr.eseo.ld.dao.PosterDAO;
import fr.eseo.ld.dao.ProfesseurSujetDAO;
import fr.eseo.ld.dao.SujetDAO;
import fr.eseo.ld.dao.UtilisateurDAO;

/**
 * Servlet permettant l'affichage de tous les sujets.
 */
@WebServlet("/ConsulterSujets")
@MultipartConfig
public class ConsulterSujets extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private static final String VUE_FORM = "/WEB-INF/formulaires/consulterSujets.jsp";

	private static final int SOUTENANCE = 1;
	private static final int POSTER = 2;
	private static final int TRAVAIL = 3;
	private static final int PROJET = 4;

	private UtilisateurDAO utilisateurDAO;
	private SujetDAO sujetDAO;
	private CommentaireDAO commentaireDAO;
	private OptionESEODAO optionESEODAO;
	private PosterDAO posterDAO;
	private EtudiantDAO etudiantDAO;
	private ProfesseurSujetDAO professeurSujetDAO;

	@Override
	public void init() throws ServletException {
		/* Récupération d'instances de DAO */
		DAOFactory daoFactory = (DAOFactory) this.getServletContext().getAttribute(ServletUtilitaire.CONF_DAO_FACTORY);
		this.utilisateurDAO = daoFactory.getUtilisateurDao();
		this.sujetDAO = daoFactory.getSujetDao();
		this.commentaireDAO = daoFactory.getCommentaireDao();
		this.optionESEODAO = daoFactory.getOptionESEODAO();
		this.posterDAO = daoFactory.getPosterDao();
		this.etudiantDAO = daoFactory.getEtudiantDao();
		this.professeurSujetDAO = daoFactory.getProfesseurSujetDAO();
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		/* Recherche de tous les sujets via sujetDao */
		List<Sujet> listeSujets = this.sujetDAO.lister();

		/* Récupération de la session depuis la requête */
		HttpSession session = request.getSession();

		/* Récupération de l'ID de l'Utilisateur en session */
		Utilisateur utilisateur = (Utilisateur) session.getAttribute(ServletUtilitaire.ATT_SESSION_USER);

		/* Récupération des options si l'utilisateur est connecté */
		List<OptionESEO> optionsUtilisateur = new ArrayList<>();
		if (utilisateur != null) {
			/* Recuperation des options de l'utilisateur */
			Utilisateur u = new Utilisateur();
			u.setIdUtilisateur(utilisateur.getIdUtilisateur());
			optionsUtilisateur = this.optionESEODAO.trouverOptionUtilisateur(u);
		}

		List<Utilisateur> listeUtilisateur;
		listeUtilisateur = this.utilisateurDAO.lister();
		request.setAttribute("listeUtilisateur", listeUtilisateur);

		List<ProfesseurSujet> listeProfesseurSujet;
		listeProfesseurSujet = this.professeurSujetDAO.lister();
		request.setAttribute("listeProfesseurSujet", listeProfesseurSujet);

		request.setAttribute(ServletUtilitaire.CHAMP_ETAT_REFUSE, EtatSujet.REFUSE);
		request.setAttribute(ServletUtilitaire.CHAMP_ETAT_PUBLIE, EtatSujet.PUBLIE);
		request.setAttribute(ServletUtilitaire.CHAMP_ETAT_VALIDE, EtatSujet.VALIDE);
		request.setAttribute(ServletUtilitaire.CHAMP_ETAT_ATTIBUE, EtatSujet.ATTRIBUE);


		/*
		 * Changement du format des listes pour pouvoir appliquer des filtres de
		 * sélection dans les JSP
		 */
		request.setAttribute(ServletUtilitaire.ATT_LISTE_SUJETS, listeSujets);


		/* Jointure de Utilisateur, Sujet et Commentaire via idUtilisateur */
		Map<Long, String> nomsPorteursSujets = new HashMap<>();
		Map<Long, Long> idPorteursSujets = new HashMap<>();
		Map<Long, String> nomsExpediteursCommentaires = new HashMap<>();

		List<String> etatPublication = new ArrayList<>();
		List<String> notesPosters = new ArrayList<>();
		List<String> notesSoutenances = new ArrayList<>();
		List<String> notesTravail = new ArrayList<>();
		List<String> notesProjet = new ArrayList<>();

		List<Commentaire> commentaires = new ArrayList<>();
		List<Commentaire> commentairesSecondaires = new ArrayList<>();
		List<String> options = new ArrayList<>();
		List<String> optionsSujet = new ArrayList<>();

		for (Iterator<Sujet> iter = listeSujets.listIterator(); iter.hasNext();) {
			Sujet sujet = iter.next();

			/* Recherche de l'utilisateur qui a déposé ce sujet */
			List<Utilisateur> listePorteurSujet = utilisateurDAO.trouverPorteurSujet(sujet);

			Long idUtilisateurSujet = null;
			if (!listePorteurSujet.isEmpty()) {
				idUtilisateurSujet = listePorteurSujet.get(0).getIdUtilisateur();
			}
			Utilisateur utilisateurSujet = new Utilisateur();
			utilisateurSujet.setIdUtilisateur(idUtilisateurSujet);

			/* Recherche les options du sujet */
			List<OptionESEO> recupOptions = this.optionESEODAO.trouverOptionSujet(sujet);
			StringBuilder optionSujet = new StringBuilder();
			boolean afficher = isAfficherSujetPourBonneOption(optionsUtilisateur, options, recupOptions, optionSujet);
			boolean afficherVisiteur = isVisiteur(utilisateur, sujet);

			if ((afficher || !sujet.getConfidentialite()) && afficherVisiteur) {
				// passe si le sujet appartient a
				// l'option ou s'il n'est pas
				// confidentiel, si la condition
				// précédente a été passé
				/* Ajout du sujet à la liste */
				optionsSujet.add(optionSujet.toString());

				/* Correspondance entre l'ID d'un utilisateur et son nom */
				String nom;
				Long id = -1L;
				if (!listePorteurSujet.isEmpty()) { // sécurité dans le cas où la BDD ait mal été remplie
					nom = listePorteurSujet.get(0).getNom() + " " + listePorteurSujet.get(0).getPrenom();
					id = listePorteurSujet.get(0).getIdUtilisateur();
				} else {
					nom = "Sujet sans porteur";
				}
				nomsPorteursSujets.put(sujet.getIdSujet(), nom);
				idPorteursSujets.put(sujet.getIdSujet(), id);

				/* Récupération de la note du poster */
				etatPublication.add(getPosterPublie(sujet.getIdSujet()));
				notesPosters.add(this.getNote(sujet.getIdSujet(), POSTER));

				/* Récupération de la note de la soutenance */
				notesSoutenances.add(this.getNote(sujet.getIdSujet(), SOUTENANCE));

				/* Récupération de la note de travail */
				notesTravail.add(this.getNote(sujet.getIdSujet(), TRAVAIL));

				/* Récupération de la note de projet */
				notesProjet.add(this.getNote(sujet.getIdSujet(), PROJET));

				/* Recherche des commentaires du sujet */
				Commentaire commentaireSujet = new Commentaire();
				commentaireSujet.setIdSujet(sujet.getIdSujet());
				List<Commentaire> commentairesTrouves = this.commentaireDAO.trouver(commentaireSujet);

				afficherCommentaires(nomsExpediteursCommentaires, commentaires, commentairesSecondaires, commentairesTrouves);
			} else { // me sujet est confidentiel
				/* Suppression du sujet dans la liste à afficher */
				iter.remove();
			}
		}

		request.setAttribute(ServletUtilitaire.ATT_NOMS_PORTEURS_SUJETS, nomsPorteursSujets);
		request.setAttribute(ServletUtilitaire.ATT_NOMS_EXPEDITEURS_COMMENTAIRES, nomsExpediteursCommentaires);
		request.setAttribute(ServletUtilitaire.ATT_COMMENTAIRES, commentaires);
		request.setAttribute(ServletUtilitaire.ATT_COMMENTAIRES_SECONDAIRES, commentairesSecondaires);
		request.setAttribute("idPorteursSujets", idPorteursSujets);
		request.setAttribute("optionSujet", optionsSujet);
		request.setAttribute(ServletUtilitaire.ATT_LISTE_OPTIONS, options);
		request.setAttribute("mapPossedePoster", etatPublication);
		request.setAttribute("notesPosters", notesPosters);
		request.setAttribute("notesSoutenances", notesSoutenances);
		request.setAttribute("notesTravail", notesTravail);
		request.setAttribute("notesProjet", notesProjet);
		/* Affichage du formulaire */
		this.getServletContext().getRequestDispatcher(VUE_FORM).forward(request, response);
	}

	private void afficherCommentaires(Map<Long, String> nomsExpediteursCommentaires, List<Commentaire> commentaires, List<Commentaire> commentairesSecondaires, List<Commentaire> commentairesTrouves) {
		String nom;
		for (Commentaire commentaire : commentairesTrouves) {
			/* Recherche des utilisateurs ayant commenté ce sujet */
			Long idUtilisateurCommentaire = commentaire.getIdUtilisateur();
			Utilisateur utilisateurCommentaire = new Utilisateur();
			utilisateurCommentaire.setIdUtilisateur(idUtilisateurCommentaire);
			List<Utilisateur> listeUtilisateurCommentaire = this.utilisateurDAO.trouver(utilisateurCommentaire);

			/* Correspondance entre l'ID d'un utilisateur et son nom */
			if (!listeUtilisateurCommentaire.isEmpty()) { // sécurité dans le cas où la BDD a mal été remplie
				nom = listeUtilisateurCommentaire.get(0).getPrenom() + " "
						+ listeUtilisateurCommentaire.get(0).getNom();
			} else {
				nom = "";
			}
			nomsExpediteursCommentaires.put(idUtilisateurCommentaire, nom);

			/* Ajout d'un commentaire primaire ou secondaire */
			if (commentaire.getIdObservation() != -1) { // s'il s'agit d'un commentaire secondaire
				commentairesSecondaires.add(commentaire);
			} else {
				commentaires.add(commentaire);
			}
		}
	}

	private boolean isAfficherSujetPourBonneOption(List<OptionESEO> optionsUtilisateur, List<String> options, List<OptionESEO> recupOptions, StringBuilder optionSujet) {
		for (OptionESEO option : recupOptions) {
			optionSujet.append(option.getNomOption()).append(" ");
			if (!options.contains(option.getNomOption())) {
				options.add(option.getNomOption());
			}
			for (OptionESEO optionU : optionsUtilisateur) {
				if (optionU.getIdOption().equals(option.getIdOption())) { // pour savoir si le sujet est proposé
					// dans l'option de l'utilisateur
					return true;
				}
			}
		}
		return false;
	}

	private boolean isVisiteur(Utilisateur utilisateur, Sujet sujet) {
		return sujet.getEtat() == EtatSujet.PUBLIE || sujet.getEtat() == EtatSujet.ATTRIBUE || utilisateur != null;
	}

	/**
	 * Récupère l'évaluation sous forme d'un String.
	 *
	 * @param idSujet
	 *            l'ID du sujet concerné.
	 * @param eval
	 *            l'évaluation faite sur le sujet.
	 * @return evaluation l'évaluation sous forme d'un String.
	 */
	private String getNote(Long idSujet, int eval) {
		List<Etudiant> listeEtudiant = this.etudiantDAO.trouverNotePoster(idSujet);
		String evaluation = "Pas d'evaluation";
		float note = (float) 0.0;
		int nbNote = 0;
		for (Etudiant etudiantIter : listeEtudiant) {
			Float noteAAjouter = 0f;

			switch (eval) {
				case SOUTENANCE:
					noteAAjouter = etudiantIter.getNoteSoutenance();
					break;
				case POSTER:
					noteAAjouter = etudiantIter.getNotePoster();
					break;
				case TRAVAIL:
					noteAAjouter = etudiantIter.getNoteIntermediaire();
					break;
				case PROJET:
					noteAAjouter = etudiantIter.getNoteProjet();
					break;
				default:
					break;
			}

			if (isNoteCorrecte(noteAAjouter)) {
				note = note + noteAAjouter;
				nbNote++;
			}
		}
		if (note != (float) 0.0) {
			note = note / nbNote;
			evaluation = "" + Float.toString(note) + "/20";
		}

		return evaluation;
	}

	private boolean isNoteCorrecte(Float note) {
		return !note.isNaN() && !note.equals((float) -1.0);
	}
	/**
	 * Récupère un String indiquant si le poster est publié ou non.
	 *
	 * @param idSujet
	 *            l'ID du sujet lié au poster.
	 * @return publie un String indiquant si le poster est publié ou non.
	 */
	private String getPosterPublie(Long idSujet) {
		Poster poster = this.posterDAO.trouverPoster(idSujet);
		String publie;
		if (poster != null) {
			if (poster.getValide().equals("non")) {
				publie = "Poster en attente de validation";
			} else {
				publie = "Poster publié";
			}

		} else {
			publie = "Pas de poster";
		}

		return publie;
	}

}