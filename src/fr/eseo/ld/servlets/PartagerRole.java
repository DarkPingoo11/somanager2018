package fr.eseo.ld.servlets;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import fr.eseo.ld.beans.OptionESEO;
import fr.eseo.ld.beans.PartageRole;
import fr.eseo.ld.beans.Role;
import fr.eseo.ld.beans.Utilisateur;
import fr.eseo.ld.dao.DAOFactory;
import fr.eseo.ld.dao.NotificationDAO;
import fr.eseo.ld.dao.OptionESEODAO;
import fr.eseo.ld.dao.PartageRoleDAO;
import fr.eseo.ld.dao.RoleDAO;
import fr.eseo.ld.dao.UtilisateurDAO;

/**
 * Servlet implementation class GererRole
 */
@WebServlet("/PartagerRole")
public class PartagerRole extends HttpServlet {
	private static final long serialVersionUID = 5344654200549670782L;

	private static final String ATT_UTILISATEURS = "listeNomPrenomID";
	private static final String ATT_TEXTE_PROF_RESPONSABLE = "texteProfResponsable";
	private static final String ATT_LISTE_PARTAGES_EN_COURS = "listePartage";
	private static final String ATT_FORMULAIRE = "formulaire";

	private static final String VUE_FORM = "/WEB-INF/formulaires/partagerRole.jsp";

	private static final String CHAMP_PROFESSEUR = "professeur";
	private static final String CHAMP_HEURE_DATE = "dateHeure";

	private UtilisateurDAO utilisateurDAO;
	private NotificationDAO notificationDAO;
	private PartageRoleDAO partageRoleDAO;
	private OptionESEODAO optionDAO;
	private RoleDAO roleDAO;

	// Initialisation du logger
	private static Logger logger = Logger.getLogger(PartagerRole.class.getName());

	@Override
	public void init() throws ServletException {
		/* Récupération d'instances de DAO */
		DAOFactory daoFactory = (DAOFactory) this.getServletContext().getAttribute(ServletUtilitaire.CONF_DAO_FACTORY);
		this.utilisateurDAO = daoFactory.getUtilisateurDao();
		this.notificationDAO = daoFactory.getNotificationDao();
		this.partageRoleDAO = daoFactory.getPartageDAO();
		this.optionDAO = daoFactory.getOptionESEODAO();
		this.roleDAO = daoFactory.getRoleDAO();
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// Chargement des différents attributs utilisés sur la page JSP
		HttpServletRequest nouvelleRequete = chargerElements(request);

		// On redirige vers la page formulaire
		this.getServletContext().getRequestDispatcher(VUE_FORM).forward(nouvelleRequete, response);

	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// Si le formulaire utilisé est "partagerRole"
		if ("partagerRole".equalsIgnoreCase(request.getParameter(ATT_FORMULAIRE))) {
			partagerRole(request, response);
		}
		if ("supprimerPartage".equalsIgnoreCase(request.getParameter(ATT_FORMULAIRE))) {
			supprimerPartage(request, response);
		}
	}

	private void partagerRole(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		boolean quitter = false;

		// Lancement de la vérification des données rentrées :
		while (!quitter) {
			/* Validation des champs */
			Map<String, String> erreurs = new HashMap<>();

			// On recupère le professeur rentré en paramètre
			String professeurRecherche = request.getParameter("utilisateurRecherche");

			// On récupère les différents horaires et dates
			String dateDebut = request.getParameter("dateDebut");
			String heureDebut = request.getParameter("heureDebut");
			String dateFin = request.getParameter("dateFin");
			String heureFin = request.getParameter("heureFin");

			// On vérifie d'abord si les dates sont du bon formats et que la date de début
			// est inférieur à la date de fin
			erreurs.putAll(verifDateHeure(dateDebut, heureDebut, dateFin, heureFin));

			// On utilise la méthode utilitaire qui permet de vérifier la cohérence de
			// l'utilisateur rentré et de vérifier qu'il existe
			// La fonction renvoie null si l'utilisateur n'existe pas
			Utilisateur utilisateurChoisi = ServletUtilitaire.rechercheUtilisateurAutoCompletion(professeurRecherche,
					this.utilisateurDAO);
			if (utilisateurChoisi == null) {
				erreurs.put(CHAMP_PROFESSEUR, "Ce professeur est inexistant");
			}
			// Si l'utilisateur renvoyé est null ou la date ne correspond pas aux critères,
			// on affiche l'erreur
			if (!erreurs.isEmpty() || utilisateurChoisi == null) {
				HttpServletRequest nouvelleRequete = chargerElements(request);
				nouvelleRequete.setAttribute(ServletUtilitaire.ATT_ERREURS, erreurs);
				this.getServletContext().getRequestDispatcher(VUE_FORM).forward(nouvelleRequete, response);
				quitter = true;
				continue;
			}

			// Sinon toutes les conditions sont réunies pour pouvoir créer le partage de
			// role
			// On recupere le choix du role et de l'utilisateur
			String choix = request.getParameter("selectRole");
			String[] choixTab = choix.split(" ");
			Role role = new Role();
			OptionESEO option = new OptionESEO();
			role.setNomRole(choixTab[0]);
			option.setNomOption(choixTab[2]);
			Role roleChoisie = roleDAO.trouver(role).get(0);
			OptionESEO optionchoisie = optionDAO.trouver(option).get(0);

			Utilisateur utilisateurSession = (Utilisateur) request.getSession()
					.getAttribute(ServletUtilitaire.ATT_SESSION_USER);

			PartageRole partageRole = new PartageRole();
			partageRole.setIdUtilisateur1(utilisateurSession.getIdUtilisateur());
			partageRole.setIdUtilisateur2(utilisateurChoisi.getIdUtilisateur());
			partageRole.setDateDebut(dateDebut + " " + heureDebut);
			partageRole.setDateFin(dateFin + " " + heureFin);
			partageRole.setIdRole(roleChoisie.getIdRole());
			partageRole.setIdOption(optionchoisie.getIdOption());
			partageRole.setActif(0L);

			// On vérifie que le professeur qui crée le partage, ne bénéficie pas déjà d'un
			// partage de ce role et de cette option
			if (verificationDroitPartage(partageRole)) {
				partageRoleDAO.creer(partageRole);
				// On signale à l'utilisateur2 qui bénéficie d'un nouveau role pour un temps
				// limité.
				this.notificationDAO.ajouterNotification(utilisateurChoisi.getIdUtilisateur(),
						utilisateurSession.getNom().toUpperCase() + " " + utilisateurSession.getPrenom()
								+ " vous a partagé son rôle de responsable option");
				this.notificationDAO.ajouterNotification(utilisateurSession.getIdUtilisateur(),
						"Le partage a été pris en compte");
			} else {
				this.notificationDAO.ajouterNotification(utilisateurSession.getIdUtilisateur(),
						"Vous bénéficiez déjà du partage de ce role");
				erreurs.put(CHAMP_HEURE_DATE, "Impossible de créer le partage");
			}
			HttpServletRequest nouvelleRequete = chargerElements(request);
			nouvelleRequete.setAttribute(ServletUtilitaire.ATT_ERREURS, erreurs);
			this.getServletContext().getRequestDispatcher(VUE_FORM).forward(nouvelleRequete, response);
			quitter = true;
		}
	}

	private void supprimerPartage(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// On récupère l'ID du partage à supprimer
		String partageRoleChoisi = request.getParameter("partageRoleChoisi");

		// On crée le bean associé et on le supprime de la bdd
		PartageRole partageRole = new PartageRole();
		partageRole.setIdPartage(Long.valueOf(partageRoleChoisi));
		partageRoleDAO.supprimer(partageRole);

		Utilisateur utilisateurSession = (Utilisateur) request.getSession()
				.getAttribute(ServletUtilitaire.ATT_SESSION_USER);
		this.notificationDAO.ajouterNotification(utilisateurSession.getIdUtilisateur(),
				"Le partage a été supprimé avec succès");

		// On recharge la liste et on redirige vers la vue formulaire
		HttpServletRequest nouvelleRequete = chargerElements(request);
		this.getServletContext().getRequestDispatcher(VUE_FORM).forward(nouvelleRequete, response);
	}

	/**
	 * Fonction permettant de charger dans la requete les élements redondants comme
	 * la liste des noms prénoms et id et le texte au dessus du formulaire d'ajout
	 */
	@SuppressWarnings("unchecked")
	private HttpServletRequest chargerElements(HttpServletRequest requete) {
		Utilisateur utilisateurSession = (Utilisateur) requete.getSession()
				.getAttribute(ServletUtilitaire.ATT_SESSION_USER);
		List<Role> listeRolesUtilisateur = (List<Role>) requete.getSession()
				.getAttribute(ServletUtilitaire.ATT_SESSION_ROLES);
		List<OptionESEO> listeOptionsUtilisateur = (List<OptionESEO>) requete.getSession()
				.getAttribute(ServletUtilitaire.ATT_SESSION_OPTIONS);

		requete.setAttribute(ATT_UTILISATEURS, chargerListesProfesseurs(utilisateurSession));
		requete.setAttribute(ATT_TEXTE_PROF_RESPONSABLE,
				chargerListeRolesOptions(listeRolesUtilisateur, listeOptionsUtilisateur));
		requete.setAttribute(ATT_LISTE_PARTAGES_EN_COURS, chargerListePartageRole(utilisateurSession));
		return requete;
	}

	/**
	 * Fonction permettant de charger dans un string la totalité des professeurs
	 * présents dans la base de données. Le string renvoyé est un string séparé par
	 * le caractère "," interprété comme le caractère de séparation dans la JSP. On
	 * retire cependant le professeur actuellement connecté de cette liste.
	 */
	private String chargerListesProfesseurs(Utilisateur utilisateurSession) {
		// On liste tous les professeurs
		List<Utilisateur> listeUtilisateurs = utilisateurDAO.listerProfesseur();

		StringBuilder liste = new StringBuilder();
		boolean premier = false;
		for (Utilisateur utilisateur : listeUtilisateurs) {
			// Si l'utilisateur <utilisateur> n'est pas celui qui est en
			// session, on peut l'ajouter à la liste
			if (!utilisateur.getIdUtilisateur().equals(utilisateurSession.getIdUtilisateur())) {
				if (!premier) {
					liste.append(utilisateur.getNom().toUpperCase().replace(" ", "") + " "
							+ utilisateur.getPrenom().replace(" ", "") + " - "
							+ utilisateur.getIdentifiant().replace(" ", ""));
					premier = true;
				} else
					liste.append("," + utilisateur.getNom().toUpperCase().replace(" ", "") + " "
							+ utilisateur.getPrenom().replace(" ", "") + " - "
							+ utilisateur.getIdentifiant().replace(" ", ""));
			}
		}
		return liste.toString();
	}

	/**
	 * Génère une liste de Strings présentant les options/roles sur lesquels le
	 * professeur est responsable
	 * 
	 * @param listeRoles
	 *            de l'utilisateur en session
	 * @param listeOptions
	 *            de l'utilisateur en session
	 */
	private List<String> chargerListeRolesOptions(List<Role> listeRoles, List<OptionESEO> listeOptions) {
		List<String> listeRoleOption = new ArrayList<>();
		for (int i = 0; i < listeRoles.size(); i++) {
			if ("profResponsable".equals(listeRoles.get(i).getNomRole())) {
				listeRoleOption.add(listeRoles.get(i).getNomRole() + " - " + listeOptions.get(i).getNomOption());
			}
		}
		return listeRoleOption;
	}

	/**
	 * Fonction permettant de renvoyer la liste des partages en cours du prof
	 * responsable en session
	 * 
	 * @param utilisateurSession
	 * @return
	 */
	private Object[] chargerListePartageRole(Utilisateur utilisateurSession) {
		PartageRole partageRole = new PartageRole();
		partageRole.setIdUtilisateur1(utilisateurSession.getIdUtilisateur());

		ArrayList<ArrayList<String>> matrice = new ArrayList<>();

		// On cherche les partages ayant pour utilisateur1, l'utilisateur en session
		List<PartageRole> listePartage = this.partageRoleDAO.trouver(partageRole);
		for (PartageRole partage : listePartage) {
			// On récupère le bean utilisateur
			Utilisateur utilisateur = new Utilisateur();
			utilisateur.setIdUtilisateur(partage.getIdUtilisateur2());
			utilisateur = utilisateurDAO.trouver(utilisateur).get(0);

			ArrayList<String> colonne = new ArrayList<>(Arrays.asList(String.valueOf(partage.getIdPartage()),
					utilisateur.getNom() + " " + utilisateur.getPrenom(), partage.getDateDebut(), partage.getDateFin(),
					String.valueOf(partage.getActif())));

			matrice.add(colonne);
		}
		return matrice.toArray();
	}

	/**
	 * Fonction vérifiant les dates rentrées dans le formulaires. Vérifie qu'elles
	 * ne sont pas vides. Vérifie qu'elles sont du bon format. Vérifie que la date
	 * de debut et inférieure à la date de fin
	 * 
	 * Renvoi un Map<String,String> contenant les erreurs.
	 * 
	 * @param dateDebut
	 * @param heureDebut
	 * @param dateFin
	 * @param heureFin
	 * @return Map<String, String> erreurs
	 */
	private Map<String, String> verifDateHeure(String dateDebut, String heureDebut, String dateFin, String heureFin) {
		/* Validation des champs */
		Map<String, String> erreurs = new HashMap<>();

		if (dateDebut.isEmpty() || heureDebut.isEmpty() || heureFin.isEmpty() || dateFin.isEmpty()) {
			erreurs.put(CHAMP_HEURE_DATE, "Veuillez selectionner une horaire et/ou une date ");
			return erreurs;
		}

		String[] dateSplit = dateDebut.split("-");
		String[] heureSplit = heureDebut.split(":");
		String[] date2Split = dateFin.split("-");
		String[] heure2Split = heureFin.split(":");

		if (heureSplit.length != 2 || dateSplit.length != 3 || heure2Split.length != 2 || date2Split.length != 3) {
			erreurs.put(CHAMP_HEURE_DATE, "Une des dates et/ou horaires est incorrecte");
			return erreurs;
		}

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			Date dateD = sdf.parse(dateDebut + " " + heureDebut + ":00");
			Date dateF = sdf.parse(dateFin + " " + heureFin + ":00");
			if (dateF.compareTo(dateD) < 0) {
				erreurs.put(CHAMP_HEURE_DATE, "La date de debut est inférieur à la date de fin");
				return erreurs;
			}
		} catch (ParseException e) {
			logger.log(Level.WARN, "erreur de conversion de la date rentrée : ", e);
		}
		return erreurs;
	}

	/**
	 * On vérifie si le professeur qui veut partager le role, ne bénéficie pas lui
	 * même du partage de role
	 * 
	 * @param partageRoleVoulu
	 *            le partage qu'on va créer
	 * @return true si il n'est pas bénéficiaire du partage de ce role
	 */
	private boolean verificationDroitPartage(PartageRole partageRoleVoulu) {
		PartageRole partageRoleRecherche = new PartageRole();
		partageRoleRecherche.setIdOption(partageRoleVoulu.getIdOption());
		partageRoleRecherche.setIdRole(partageRoleVoulu.getIdRole());
		partageRoleRecherche.setIdUtilisateur2(partageRoleVoulu.getIdUtilisateur1());
		List<PartageRole> listePartage = partageRoleDAO.trouver(partageRoleRecherche);
		return listePartage.isEmpty();
	}

}