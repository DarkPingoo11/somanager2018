package fr.eseo.ld.servlets;


import fr.eseo.ld.beans.Callback;
import fr.eseo.ld.beans.CallbackType;
import fr.eseo.ld.beans.Poster;
import fr.eseo.ld.dao.DAOFactory;
import fr.eseo.ld.dao.PosterDAO;
import fr.eseo.ld.utils.CallbackUtilitaire;
import fr.eseo.ld.utils.IOUtilitaire;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * Servlet permettant l'exportation des posters valides groupés en zip .
 *
 * @author Anne-Claire VERGOTE
 */
@WebServlet("/ExporterPoster")
public class ExporterPoster extends HttpServlet {
    private static final long serialVersionUID = 1L;

    public static final int TAILLE_TAMPON = 10240; // 10 ko

    private static final String CHEMIN_POSTER = "/home/etudiant/Posters";
    private static final String VUE_FORM = "/WEB-INF/formulaires/exporterPoster.jsp";

    private PosterDAO posterDAO;


    @Override
    public void init() throws ServletException {
        /* Récupération d'instances de DAO */
        DAOFactory daoFactory = (DAOFactory) this.getServletContext().getAttribute(ServletUtilitaire.CONF_DAO_FACTORY);

        this.posterDAO = daoFactory.getPosterDao();

    }

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ExporterPoster() {
        super();
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        /* Chargement de la liste des utilisateurs non validés */
        request.setAttribute(ServletUtilitaire.ATT_LISTE_POSTERS_VALIDES, this.chargerPosterValide());
        this.getServletContext().getRequestDispatcher(VUE_FORM).forward(request, response);
    }

    private List<Poster> chargerPosterValide() {
        /* Recherche des posters semblables au type de poster que l'on recherche */
        return this.posterDAO.listerPosterValide();
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        /* Récupération des posters valides*/
        List<Poster> postersValide = new ArrayList<>(this.posterDAO.listerPosterValide());

        /* Zip de tous les posters validés pour une option */
        if (!postersValide.isEmpty()) {
            String cheminPosters = formatageCheminPoster();


            /* Zip */
            File directory = new File(cheminPosters);
            byte[] zip = IOUtilitaire.zipFiles(directory, postersValide);
            ServletOutputStream sos = response.getOutputStream();
            response.setContentType("application/zip");
            response.setHeader("Content-Disposition", "attachment; filename=\"posters.zip\"");
            sos.write(zip);
            sos.flush();
        } else {
            /* Ré-affichage du formulaire avec notification d'erreur */
            CallbackUtilitaire.setCallback(request, new Callback(CallbackType.ERROR,
                    "Aucun poster validé !"));
            this.getServletContext().getRequestDispatcher(VUE_FORM).forward(request, response);
        }
    }

    private String formatageCheminPoster() {
        /* Formatage du chemin vers les posters */
        SimpleDateFormat formatAnnee = new SimpleDateFormat("yyyy");
        String annee = formatAnnee.format(new Date());
        return CHEMIN_POSTER + "/" + annee + "/";
    }
}


