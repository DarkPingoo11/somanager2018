package fr.eseo.ld.servlets;

import fr.eseo.ld.beans.*;
import fr.eseo.ld.dao.*;
import fr.eseo.ld.objets.exportexcel.*;
import fr.eseo.ld.pgl.beans.Sprint;
import fr.eseo.ld.utils.CallbackUtilitaire;
import fr.eseo.ld.utils.ParamUtilitaire;
import fr.eseo.ld.utils.PixelUtilitaire;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFColor;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Servlet permettant d'exporter les notes de I3
 */
@WebServlet("/ExporterNotes")
@MultipartConfig
public class ExporterNotes extends HttpServlet {

    private static final String PARAM_ANNEEEXPORTTYPE = "exportAnneeChoisi";
    private ModeleExcelDAO modeleDAO;
    private ParametreDAO parametreDAO;
    private OptionESEODAO optionESEODAO;
    private AnneeScolaireDAO anneeScolaireDAO;
    private UtilisateurDAO utilisateurDAO;


    private static final String VUE_FORM = "/WEB-INF/formulaires/exporterNotes.jsp";

    private static final String MSG_ERREUR_IOMODELE = "Impossible d'accéder au modèle stocké sur le serveur";
    private static final String MSG_ERREUR_FORMATMODELE = "Le modèle stocké est au mauvais format";

    private static final String PATH_STOCKAGE = "/home/etudiant/stockage";

    private static final String ATT_ONGLET_CHOISI	= "ongletChoisi";
    private static final String ATT_MODELE_CONTENU	= "modeleContenu";
    private static final String ATT_MODELE_DONNEES	= "modeleDonnees";
    private static final String ATT_MODELESDISPO	= "modelesDispo";

    private static final String ATT_MODELESSELECT	= "modeleSelect";
    private static final String ATT_CHOIXMODELE 	= "choixModele";

    private static final String ATT_ANNEESSCOLAIRES = "anneesScolaires";
    private static final String ATT_OPTIONSESEO     = "optionsESEO";

    private static final String PARAM_MODELESEL 	= "I3_ModeleNoteJury";
    private static final String PARAM_LIGNEHEADER	= "ligneHeader";
    private static final String PARAM_COLID			= "colId";
    private static final String PARAM_COLNOM		= "colNom";
    private static final String PARAM_COLNOTE	    = "colNote";
    private static final String PARAM_COLCOM	    = "colCom";
    private static final String PARAM_COLMOYENNE	= "colMoyenne";
    private static final String PARAM_COLGRADE		= "colGrade";

    private static final String PARAM_ANNEESCOLAIRE = "anneeChoisi";
    private static final String PARAM_OPTION        = "optionChoisi";
    private static final String PARAM_EXPORTTYPE    = "exportChoisi";
    private static final String PARAM_NOMFICHIER    = "nomFichier";

    private static final String ONGLET_CONFIG       = "configuration";

    private static Logger logger = Logger.getLogger(ExporterNotes.class.getName());

    @Override
    public void init() throws ServletException {
        /* Récupération d'instances de DAO */
        DAOFactory daoFactory = (DAOFactory) this.getServletContext().getAttribute(ServletUtilitaire.CONF_DAO_FACTORY);
        this.modeleDAO = daoFactory.getModeleExcelDAO();
        this.parametreDAO = daoFactory.getParametreDAO();
        this.optionESEODAO = daoFactory.getOptionESEODAO();
        this.anneeScolaireDAO = daoFactory.getAnneeScolaireDAO();
        this.utilisateurDAO = daoFactory.getUtilisateurDao();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        //Récupérer le modèle sélectionné
        Parametre modeleSelectionne = new Parametre();
        modeleSelectionne.setNomParametre(PARAM_MODELESEL);
        List<Parametre> params = parametreDAO.trouver(modeleSelectionne);

        //Un modèle est sélectionné - L'afficher
        if(params.size() == 1) {
            modeleSelectionne = params.get(0);
            String nomFichier = modeleSelectionne.getValeur();
            File f = getModeleFile(nomFichier);
            //Afficher le modèle selectionné, si présent
            if(f.exists()) {
                this.afficherModele(request, f);
            } else {
                //Si le modèle n'existe pas physiquement, on le supprime de la base
                this.supprimerModele(nomFichier);
                CallbackUtilitaire.addCallback(request, new Callback("Le modèle choisi n'existe pas ou plus"));
            }

            //Définir le modèle selectionné
            request.setAttribute(ATT_MODELESSELECT, nomFichier);
        }


        //Afficher la liste des modèles disponibles
        List<ModeleExcel> modeles = modeleDAO.lister();
        request.setAttribute(ATT_MODELESDISPO, modeles);

        //Charger la liste des options et des années scolaires
        request.setAttribute(ATT_ANNEESSCOLAIRES,  this.anneeScolaireDAO.lister());
        request.setAttribute(ATT_OPTIONSESEO, this.optionESEODAO.lister());

        this.getServletContext().getRequestDispatcher(VUE_FORM).forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        /* Récupération de l'ID de l'utilisateur en session */
        String onglet = "general";

        if ("envoyerModele".equalsIgnoreCase(request.getParameter(ServletUtilitaire.CHAMP_FORMULAIRE))) {
            this.doEnvoyerModele(request);
            onglet = ONGLET_CONFIG;
        } else if ("exporterNotes".equalsIgnoreCase(request.getParameter(ServletUtilitaire.CHAMP_FORMULAIRE))) {
            this.doExporterNotes(request, response);
            return;
        } else if ("choisirModele".equalsIgnoreCase(request.getParameter(ServletUtilitaire.CHAMP_FORMULAIRE))) {
            this.doChangerModele(request);
            onglet = ONGLET_CONFIG;
        } else if ("parametrerModele".equalsIgnoreCase(request.getParameter(ServletUtilitaire.CHAMP_FORMULAIRE))) {
            this.doParametrerModele(request);
            onglet = ONGLET_CONFIG;
        } else if ("supprimerModele".equalsIgnoreCase(request.getParameter(ServletUtilitaire.CHAMP_FORMULAIRE))) {
            this.doSupprimerModele(request);
            onglet = ONGLET_CONFIG;
        }


        request.setAttribute(ATT_ONGLET_CHOISI, onglet);

        doGet(request, response);
    }


    /**
     * Action de changement des paramètres du modèle
     * @param request HTTPServletRequest
     */
    private void doParametrerModele(HttpServletRequest request) {
        //Récupération des paramètres
        Integer ligneHeader = ParamUtilitaire.getInt(request.getParameter(PARAM_LIGNEHEADER));
        Integer colId = ParamUtilitaire.getInt(request.getParameter(PARAM_COLID));
        Integer colNom = ParamUtilitaire.getInt(request.getParameter(PARAM_COLNOM));
        Integer colNote = ParamUtilitaire.getInt(request.getParameter(PARAM_COLNOTE));
        Integer colCom = ParamUtilitaire.getInt(request.getParameter(PARAM_COLCOM));
        Integer colMoyenne = ParamUtilitaire.getInt(request.getParameter(PARAM_COLMOYENNE));
        Integer colGrade = ParamUtilitaire.getInt(request.getParameter(PARAM_COLGRADE));
        String modele = request.getParameter(ATT_CHOIXMODELE);

        if(modele != null && modele.length() > 0) {
            ModeleExcel modeleExcel = new ModeleExcel();
            modeleExcel.setNomFichier(modele);
            modeleExcel.setLigneHeader(ligneHeader);
            modeleExcel.setColId(colId);
            modeleExcel.setColNom(colNom);
            modeleExcel.setColNote(colNote);
            modeleExcel.setColCom(colCom);
            modeleExcel.setColMoyenne(colMoyenne);
            modeleExcel.setColGrade(colGrade);

            modeleDAO.modifier(modeleExcel);

            CallbackUtilitaire.setCallback(request, new Callback(CallbackType.SUCCESS,
                    "Les paramètres ont bien été enregistrés"));
        } else {
            CallbackUtilitaire.setCallback(request, new Callback("Aucun modèle n'est sélectionné pour la sauvegarde"));
        }
    }

    /**
     * Action de choix du modèle
     * @param request HTTPServletRequest
     */
    private void doChangerModele(HttpServletRequest request) {
        //Récupération des paramètres
        String modeleChoisi = request.getParameter(ATT_CHOIXMODELE);

        if(modeleChoisi != null && modeleChoisi.length() > 0) {
            changerModele(modeleChoisi);
            CallbackUtilitaire.setCallback(request, new Callback(CallbackType.SUCCESS,
                    "Le nouveau modèle est le suivant : " + modeleChoisi));
        } else {
            CallbackUtilitaire.setCallback(request, new Callback("Le modèle choisi n'existe pas : " + modeleChoisi));
        }
    }

    /**
     * Envoies le modèle sur le serveur
     * @param request HttpServletRequest
     * @throws ServletException ServletException
     */
    private void doEnvoyerModele(HttpServletRequest request) throws ServletException {
        Part filePart;

        try {
            filePart = request.getPart("file_selected");
        } catch (IOException e) {
            CallbackUtilitaire.setCallback(request, new Callback("Impossible de charger le modèle"));
            logger.log(Level.WARN, "Impossible de charger le fichier envoyé" , e);
            return;
        }

        try(InputStream fileContent = filePart.getInputStream()) {
            String nomFichier = Paths.get(filePart.getSubmittedFileName()).getFileName().toString();
            //Sauvegarder le fichier
            this.sauvegarderModeleSurServeur(fileContent, nomFichier);

            //Ajouter le fichier a la base
            ModeleExcel modele = new ModeleExcel();
            modele.setNomFichier(nomFichier);
            modeleDAO.creer(modele);

            //Changer le modèle sur le nouveau modèle ajouté
            this.changerModele(nomFichier);

            CallbackUtilitaire.setCallback(request, new Callback(CallbackType.SUCCESS,
                    "Le modèle envoyé à bien été ajouté"));
        } catch (IOException e) {
            logger.log(Level.WARN, "Impossible d'enregistrer le modèle sur le serveur" , e);
            CallbackUtilitaire.setCallback(request, new Callback("Impossible d'enregistrer le modèle"));
        }
    }

    /**
     * Supprime le modèle du serveur
     * @param request HTTPServletRequest
     */
    private void doSupprimerModele(HttpServletRequest request) {
        //Récupération des requetes
        String nomFichier = request.getParameter(ATT_CHOIXMODELE);

        if(nomFichier != null && nomFichier.length() > 0) {
            //Suppression du modèle
            if(this.supprimerModele(nomFichier)) {
                CallbackUtilitaire.setCallback(request,
                        new Callback(CallbackType.SUCCESS,
                                "Le modèle '" + nomFichier + "' a été supprimé !"));
            } else {
                CallbackUtilitaire.setCallback(request,
                        new Callback(CallbackType.ERROR,
                                "Le modèle '" + nomFichier + "' a mal été supprimé du système de fichier"));
            }

            //Choix d'un autre modèle si il en existe un
            List<ModeleExcel> listeModeles = modeleDAO.lister();
            if(!listeModeles.isEmpty()) {
                this.changerModele(listeModeles.get(0).getNomFichier());
            }
        } else {
            CallbackUtilitaire.setCallback(request, new Callback("Aucun modèle n'a été choisi"));
        }
    }

    /**
     * Exporter les notes en fonction des paramètres choisis
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     */
    private void doExporterNotes(HttpServletRequest request, HttpServletResponse response) {
        //Récupération des parametres
        Integer anneeP      = ParamUtilitaire.getInt(request.getParameter(PARAM_ANNEESCOLAIRE));
        Integer exportTypeP = ParamUtilitaire.getInt(request.getParameter(PARAM_EXPORTTYPE));
        Integer optionP      = ParamUtilitaire.getInt(request.getParameter(PARAM_OPTION));
        String nomFichierP  = request.getParameter(PARAM_NOMFICHIER);
        String anneeTypeStr = request.getParameter(PARAM_ANNEEEXPORTTYPE);
        ExcelNotesJury.ExportAnnee exportAnnee = anneeTypeStr.equalsIgnoreCase("pfe") ?
                ExcelNotesJury.ExportAnnee.PFE :
                ExcelNotesJury.ExportAnnee.PGL;

        //Si les paramètres ne sont pas null
        if(ParamUtilitaire.notNull(anneeP, exportTypeP, optionP, nomFichierP)) {
            ExportType exportType = ExportType.getExportFromId(exportTypeP);

            //Rechercher la liste des eleves avec leurs notes pour l'option donnée
            optionP = optionP > 0 ? optionP : null;
            Map<Utilisateur, ENJNotes> notes;
            if(exportAnnee.equals(ExcelNotesJury.ExportAnnee.PFE)) {
                notes = utilisateurDAO.recupererNotesUtilisateursPFE(optionP, anneeP);
            } else {
                notes = DAOFactory.getInstance().getPglNoteDAO().recupererNotesMoyennesPglUtilisateurParSprint(ParamUtilitaire.getAnneeCourante());
            }

            //Exporter les notes
            if(exportType != null) {
                try {
                    this.exporterNotes(request,response,exportType, notes, nomFichierP, exportAnnee);
                } catch (IOException e) {
                    logger.log(Level.WARN, "Erreur d'ecriture du flux de sortie", e);
                    CallbackUtilitaire.setCallback(request, new Callback("Erreur d'ecriture du flux de sortie"));
                }
            }
        } else {
            CallbackUtilitaire.setCallback(request, new Callback("La requête est erronée"));
        }
    }

    /**
     * Affiche le modèle sur la page
     * @param request HttpServletRequest
     */
    private void afficherModele(HttpServletRequest request, File f) {
        //Récupérer les informations du modèle
        ModeleExcel modele = new ModeleExcel();
        modele.setNomFichier(f.getName());
        List<ModeleExcel> modeles = modeleDAO.trouver(modele);

        //On créer un modèle si il n'existe pas
        if(modeles.size() != 1) {
            modeleDAO.creer(modele);
        } else {
            modele = modeles.get(0);
        }

        Workbook workbook = null;
        try (FileInputStream fileContent = FileUtils.openInputStream(f)){
            workbook = WorkbookFactory.create(fileContent);
        } catch (IOException e) {
            CallbackUtilitaire.setCallback(request, new Callback(MSG_ERREUR_IOMODELE));
            logger.log(Level.WARN, MSG_ERREUR_IOMODELE, e);
        } catch (InvalidFormatException e) {
            CallbackUtilitaire.setCallback(request, new Callback(MSG_ERREUR_FORMATMODELE));
            logger.log(Level.WARN, MSG_ERREUR_FORMATMODELE, e);
            this.supprimerModele(f.getName());
        }

        //Si une erreur s'est produite
        if(workbook == null) {
            return;
        }

        ExcelNotesJury enj = new ExcelNotesJury(workbook);

        //Afficher le fichier
        final Sheet excelSheet = enj.getSheet();
        List<List<CelluleExcel>> lignes = new ArrayList<>();
        for(int i = 0; i < excelSheet.getLastRowNum()-1; i++) {
            List<CelluleExcel> cols = new ArrayList<>();
            Row r = excelSheet.getRow(i);

            for(int j = 0; r != null && j < r.getLastCellNum()-1; j++) {
                Cell c = r.getCell(j);
                if(c == null) {
                    continue;
                }

                //Récupération de la couleur de la cellule (Foreground fonctionne mieux)
                CellStyle cellStyle = c.getCellStyle();
                Color colorB = cellStyle.getFillForegroundColorColor();

                int height  = PixelUtilitaire.heightUnits2Pixel(r.getHeight());
                int width   = PixelUtilitaire.widthUnits2Pixel((short) excelSheet.getColumnWidth(j));
                CelluleExcel cellule = new CelluleExcel();
                cellule.setDonnees(this.getValeurCellule(c));
                cellule.setHexColorBackground(getColorFromPOI(colorB));
                cellule.setWidth(excelSheet.isColumnHidden(j) ? 1 : width);
                cellule.setHeight(height);

                cols.add(cellule);
            }
            lignes.add(cols);
        }

        //Définition des attributs
        request.setAttribute(ATT_MODELE_DONNEES, modele);
        request.setAttribute(ATT_MODELE_CONTENU, lignes);
    }

    /**
     * Supprime physiquement le modèle du serveur et en base de données
     * @param nomFichier Nom du modele a supprimer
     * @return Vrai si il a bien été supprimé, faux sinon
     */
    private boolean supprimerModele(String nomFichier) {
        ModeleExcel modele = new ModeleExcel();
        modele.setNomFichier(nomFichier);
        modeleDAO.supprimer(modele);

        //On supprime le fichier physiquement
        File cible = getModeleFile(nomFichier);
        if(cible.exists()) {
            boolean success = false;
            try {
                Files.delete(cible.toPath());
                success = true;
            } catch (Exception e) {
                logger.log(Level.WARN, "Impossible de supprimer le fichier", e);
            }
            return success;
        }

        //On choisi un nouveau parametre par défaut
        List<ModeleExcel> listeModeles = modeleDAO.lister();

        if(!listeModeles.isEmpty()) {
            this.changerModele(listeModeles.get(0).getNomFichier());
        } else {
            //On supprime le parametre
            Parametre paramModele = new Parametre();
            paramModele.setNomParametre(PARAM_MODELESEL);
            parametreDAO.supprimer(paramModele);
        }

        return true;
    }

    /**
     * Change le modele par défaut en base de données
     * @param modeleChoisi nom du modele
     */
    private void changerModele(String modeleChoisi) {
        Parametre modeleParam = new Parametre();
        modeleParam.setNomParametre(PARAM_MODELESEL);

        if(parametreDAO.trouver(modeleParam).isEmpty()) {
            //Création du paramètre dans la base
            modeleParam.setValeur(modeleChoisi);
            parametreDAO.creer(modeleParam);
        } else {
            //On modifie l'entrée existante
            modeleParam.setValeur(modeleChoisi);
            parametreDAO.modifier(modeleParam);
        }
    }

    /**
     * Enregistre le modèle sur le serveur
     * @param inputStream InputStream
     * @throws IOException En cas d'erreur
     */
    private void sauvegarderModeleSurServeur(InputStream inputStream, String nomFichier) throws IOException {
        File cible = getModeleFile(nomFichier);
        FileUtils.copyInputStreamToFile(inputStream, cible);
    }

    /**
     * Récupère la valeur d'une cellule
     * @param c Cellule
     * @return Valeur de la cellule
     */
    private String getValeurCellule(Cell c) {
        String valeur = "";
        switch(c.getCellTypeEnum()) {
            case _NONE:
                break;
            case NUMERIC:
                valeur = c.getNumericCellValue() + "";
                break;
            case STRING:
                valeur = c.getStringCellValue();
                break;
            case FORMULA:
                valeur = "<formule>";
                break;
            case BLANK:
                break;
            case BOOLEAN:
                valeur = c.getBooleanCellValue() ? "VRAI" : "FAUX";
                break;
            case ERROR:
                break;
            default:
                break;
        }
        return valeur;
    }


    private void exporterNotes(HttpServletRequest request, HttpServletResponse response, ExportType type,
                               Map<Utilisateur, ENJNotes> notes, String nomFichier,
                               ExcelNotesJury.ExportAnnee exportAnnee) throws IOException {
        //Ouvrir le modèle
        File file = getModeleFile(nomFichier);

        //Si le fichier n'existe pas, lever une erreur
        if(!file.exists()) {
            CallbackUtilitaire.setCallback(request, new Callback("Le modèle d'export n'existe pas"));
            return;
        }

        //Ouverture du workbook
        final Workbook workbook;
        try {
            workbook = WorkbookFactory.create(file);
        } catch (InvalidFormatException | IOException e ) {
            logger.log(Level.WARN, "Impossible de créer le workbook, le fichier est au mauvais format", e);
            CallbackUtilitaire.setCallback(request, new Callback("Impossible de créer le workbook, le fichier est au mauvais format"));
            return;
        }

        //Définition des paramètres de l'objet d'export
        ModeleExcel modeleExcel = new ModeleExcel();
        modeleExcel.setNomFichier(nomFichier);
        final List<ModeleExcel> modeles = modeleDAO.trouver(modeleExcel);
        //Rédéfinition du modèle
        modeleExcel = modeles.size() == 1 ? modeles.get(0) : modeleExcel;

        //On vérifie la configuration du modèle
        if(modeleExcel.getLigneHeader() == null) {
            CallbackUtilitaire.setCallback(request, new Callback("Le modèle n'est pas configuré"));
            //Close workbook
            workbook.close();
            return;
        }

        //Définition des paramètres du modèle pour l'objet d'export
        HashMap<ENJParams, Integer> params = new HashMap<>();
        params.put(ENJParams.COL_ID_UTILISATEUR, modeleExcel.getColId());
        params.put(ENJParams.COL_NOM_UTILISATEUR, modeleExcel.getColNom());
        params.put(ENJParams.COL_NOTE, modeleExcel.getColNote());
        params.put(ENJParams.COL_COMMENTAIRE, modeleExcel.getColCom());
        params.put(ENJParams.COL_MOYENNE, modeleExcel.getColMoyenne());
        params.put(ENJParams.COL_GRADE, modeleExcel.getColGrade());

        //Création de l'objet d'export
        ExcelNotesJury enj = new ExcelNotesJury(workbook, params, modeleExcel.getLigneHeader());

        //Exportation des notes
        List<ENJMatiere> matieres = new ArrayList<>();



        if(exportAnnee.equals(ExcelNotesJury.ExportAnnee.PFE)) {
            matieres.add(new ENJMatiere("Poster", 40f));
            matieres.add(new ENJMatiere("Soutenance", 60f));
        } else {
            final List<Sprint> sprints = DAOFactory.getInstance().getSprintDAO().recupererSprintAnnee(ParamUtilitaire.getAnneeCourante());
            for(Sprint sprint : sprints) {
                matieres.add(new ENJMatiere("Sprint " + sprint.getNumero(), sprint.getCoefficient()));
            }
        }

        enj.insererNotes(notes, matieres, type);

        //Définition de la réponse -> Téléchargement du fichier
        response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        response.setHeader("Content-Disposition", "attachment; filename=\"notesI3.xlsx\"");
        enj.enregistrerFichier(response.getOutputStream());

        //Close workbook
        workbook.close();
    }


    /**
     * Récupère une couleur a partir d'apachePOI
     * @param color couleur apachePOI
     * @return Couleur #hex
     */
    private static String getColorFromPOI(Color color){
        if (color != null) {
            if (color instanceof XSSFColor) {
                return "#" + ((XSSFColor)color).getARGBHex().substring(2);
            } else if (color instanceof HSSFColor) {
                return "#" + ((HSSFColor)color).getHexString();
            }
        }
        return "#FFFFFF";
    }

    /**
     * Récupère le fichier modèle stocké sur le serveur
     * @param nomFichier nom du modèle
     * @return Fichier modèle
     */
    private static File getModeleFile(String nomFichier) {
        StringBuilder builder = new StringBuilder();
        builder.append(PATH_STOCKAGE).append("/").append(nomFichier);
        return new File(builder.toString());
    }



}
