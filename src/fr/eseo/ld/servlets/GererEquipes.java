package fr.eseo.ld.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.eseo.ld.beans.AnneeScolaire;
import fr.eseo.ld.beans.Equipe;
import fr.eseo.ld.beans.EtatSujet;
import fr.eseo.ld.beans.Etudiant;
import fr.eseo.ld.beans.EtudiantEquipe;
import fr.eseo.ld.beans.OptionESEO;
import fr.eseo.ld.beans.OptionSujet;
import fr.eseo.ld.beans.ProfesseurSujet;
import fr.eseo.ld.beans.Role;
import fr.eseo.ld.beans.RoleUtilisateur;
import fr.eseo.ld.beans.Sujet;
import fr.eseo.ld.beans.Utilisateur;
import fr.eseo.ld.dao.AnneeScolaireDAO;
import fr.eseo.ld.dao.DAOFactory;
import fr.eseo.ld.dao.EquipeDAO;
import fr.eseo.ld.dao.EtudiantDAO;
import fr.eseo.ld.dao.NotificationDAO;
import fr.eseo.ld.dao.OptionESEODAO;
import fr.eseo.ld.dao.RoleDAO;
import fr.eseo.ld.dao.SujetDAO;
import fr.eseo.ld.dao.UtilisateurDAO;

/**
 * Servlet permettant la gestion des équipes.
 */
@WebServlet("/GererEquipes")
public class GererEquipes extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private static final String VUE_FORM = "/WEB-INF/formulaires/gererEquipes.jsp";

	private UtilisateurDAO utilisateurDAO;
	private OptionESEODAO optionESEODAO;
	private SujetDAO sujetDAO;
	private EquipeDAO equipeDAO;
	private EtudiantDAO etudiantDAO;
	private RoleDAO roleDAO;
	private AnneeScolaireDAO anneeScolaireDAO;
	private NotificationDAO notificationDAO;

	@Override
	public void init() throws ServletException {
		/* Récupération d'instances de DAO */
		DAOFactory daoFactory = (DAOFactory) this.getServletContext().getAttribute(ServletUtilitaire.CONF_DAO_FACTORY);
		this.utilisateurDAO = daoFactory.getUtilisateurDao();
		this.optionESEODAO = daoFactory.getOptionESEODAO();
		this.sujetDAO = daoFactory.getSujetDao();
		this.equipeDAO = daoFactory.getEquipeDAO();
		this.etudiantDAO = daoFactory.getEtudiantDao();
		this.roleDAO = daoFactory.getRoleDAO();
		this.anneeScolaireDAO = daoFactory.getAnneeScolaireDAO();
		this.notificationDAO = daoFactory.getNotificationDao();
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		/* --PARTIE 1-- */
		/* Liste de tous les etudiants disponibles, c'est-à-dire non attribues */
		List<Utilisateur> listeEtudiantsDispos = this.utilisateurDAO.listerEtudiantsDisponibles();
		/*
		 * Liste de toutes les options des étudiants non attribues pour pouvoir les
		 * afficher par option
		 */
		List<String> options = new ArrayList<>();
		List<String> optionsEtudiant = new ArrayList<>();
		peuplerOptionsUtilisateur(listeEtudiantsDispos, options, optionsEtudiant);
		/* Liste pour avoir l'attribut contratPro */
		Etudiant etudiantI3 = new Etudiant();
		AnneeScolaire anneeScolaire = new AnneeScolaire();
		anneeScolaire.setIdOption((long) 1);
		int annee = this.anneeScolaireDAO.trouver(anneeScolaire).get(0).getAnneeDebut();
		etudiantI3.setAnnee(annee);
		List<Etudiant> listeEtudiantsI3 = this.etudiantDAO.trouver(etudiantI3);

		/* --PARTIE 2-- */
		/* Liste les sujets publiés encore sans équipe */
		Sujet sujetPublie = new Sujet();
		EtatSujet etatSujet = EtatSujet.PUBLIE;
		sujetPublie.setEtat(etatSujet);
		List<Sujet> listeSujetsPublies = this.sujetDAO.trouver(sujetPublie);
		/*
		 * Liste de toutes les options des sujets non attribues pour pouvoir les
		 * afficher par option
		 */
		List<String> optionsSujet = new ArrayList<>();
		peuplerOptionsSujet(options, listeSujetsPublies, optionsSujet);
		/* Liste des référents des sujets */

		List<ProfesseurSujet> referents = this.utilisateurDAO.listerProfesseursSujetsFonction("référent");
		List<Utilisateur> referentsUtilisateurs = new ArrayList<>();
		List<Long> idReferentsUtilisateurs = new ArrayList<>();
		for (ProfesseurSujet referent : referents) {
			Utilisateur utilisateur = new Utilisateur();
			utilisateur.setIdUtilisateur(referent.getIdProfesseur());
			List<Utilisateur> utilisateursTrouves = this.utilisateurDAO.trouver(utilisateur);
			for (Utilisateur utilisateurTrouve : utilisateursTrouves) {
				if (!idReferentsUtilisateurs.contains(utilisateurTrouve.getIdUtilisateur())) {
					referentsUtilisateurs.add(utilisateurTrouve);
					idReferentsUtilisateurs.add(utilisateurTrouve.getIdUtilisateur());
				}
			}
		}

		/* --PARTIE 3-- */
		/*
		 * Liste de toutes les équipes de l'année en cours avec leur taille et la
		 * validation
		 */
		Equipe equipeCree = new Equipe();
		equipeCree.setAnnee(annee);
		List<Equipe> listeEquipes = this.equipeDAO.trouver(equipeCree);

		/* Pour chaque équipe, je veux son sujet */
		List<Sujet> listeSujetsEquipes = this.listeSujetParEquipe(listeEquipes);

		/* Pour chaque sujet attribué, je veux ses options */
		List<OptionSujet> listeOptionsSujetsEquipes = new ArrayList<>();
		for (Sujet sujet : listeSujetsEquipes) {
			List<OptionESEO> optionsDuSujet = this.optionESEODAO.trouverOptionSujet(sujet);
			for (OptionESEO option : optionsDuSujet) {
				OptionSujet optionSujets = new OptionSujet();
				optionSujets.setIdOption(option.getIdOption());
				optionSujets.setIdSujet(sujet.getIdSujet());
				listeOptionsSujetsEquipes.add(optionSujets);
			}
		}

		/* Liste de toutes les options pour pouvoir comparer et afficher leur nom */
		List<OptionESEO> optionsESEO = this.optionESEODAO.lister();

		/*
		 * Pour chaque équipe, je veux ses membres listeEtudiantsEquipes contient les
		 * membres d'équipes de l'annee en cours ce qui pour nous aujourd'hui semble
		 * comme la liste complete d'etudiantEquipe mais plus tard pour l'historique ce
		 * sera utile de ne pas avoir affichées les années passées
		 */
		List<EtudiantEquipe> listeEtudiantsEquipes = new ArrayList<>();
		Etudiant etudiant = new Etudiant();
		etudiant.setAnnee(annee);
		List<Etudiant> etudiantsAnneeActuelle = this.etudiantDAO.trouver(etudiant);
		for (Etudiant etudiantTrouve : etudiantsAnneeActuelle) {
			EtudiantEquipe etudiantEquipe = new EtudiantEquipe();
			etudiantEquipe.setIdEtudiant(etudiantTrouve.getIdEtudiant());
			List<EtudiantEquipe> cetEtudiantEquipe = this.equipeDAO.trouverEtudiantEquipe(etudiantEquipe);
			for (EtudiantEquipe etudiantEquipeTrouve : cetEtudiantEquipe) {
				listeEtudiantsEquipes.add(etudiantEquipeTrouve);
			}
		}

		/* Pour chaque membre d'équipe, je veux son nom et son prénom */
		List<Utilisateur> listeUtilisateursEquipes = this.utilisateurDAO.listerEtudiantEquipe();
		/* Je veux aussi son option */
		Role roleEtudiant = new Role();
		roleEtudiant.setIdRole((long) 1);
		List<RoleUtilisateur> optionsEtudiants = this.roleDAO.listerRoleUtilisateur(roleEtudiant);

		/* --ENVOI DES ATTRIBUTS-- */
		request.setAttribute(ServletUtilitaire.ATT_OPTIONS_ETUDIANT, optionsEtudiant);
		request.setAttribute(ServletUtilitaire.ATT_OPTIONS_SUJET, optionsSujet);
		request.setAttribute(ServletUtilitaire.ATT_LISTE_OPTIONS, options);
		request.setAttribute(ServletUtilitaire.ATT_LISTE_SUJETS, listeSujetsPublies);
		request.setAttribute(ServletUtilitaire.ATT_LISTE_ETUDIANTS_DISPOS, listeEtudiantsDispos);
		request.setAttribute(ServletUtilitaire.ATT_LISTE_EQUIPES, listeEquipes);
		request.setAttribute(ServletUtilitaire.ATT_LISTE_SUJETS_EQUIPES, listeSujetsEquipes);
		request.setAttribute(ServletUtilitaire.ATT_LISTE_OPTIONS_SUJETS, listeOptionsSujetsEquipes);
		request.setAttribute(ServletUtilitaire.ATT_LISTE_OPTIONS_ESEO, optionsESEO);
		request.setAttribute(ServletUtilitaire.ATT_LISTE_ETUDIANT_EQUIPE, listeEtudiantsEquipes);
		request.setAttribute(ServletUtilitaire.ATT_LISTE_UTILISATEURS, listeUtilisateursEquipes);
		request.setAttribute(ServletUtilitaire.ATT_LISTE_ROLE_UTILISATEUR, optionsEtudiants);
		request.setAttribute(ServletUtilitaire.ATT_LISTE_ETUDIANTS_I3, listeEtudiantsI3);
		request.setAttribute(ServletUtilitaire.ATT_LISTE_REFERENTS, referents);
		request.setAttribute(ServletUtilitaire.ATT_LISTE_UTILISATEURS_REFERENTS, referentsUtilisateurs);

		this.getServletContext().getRequestDispatcher(VUE_FORM).forward(request, response);
	}

	private void peuplerOptionsSujet(List<String> options, List<Sujet> listeSujetsPublies, List<String> optionsSujet) {
		for (Sujet sujet : listeSujetsPublies) {
			List<OptionESEO> recupOptions = this.optionESEODAO.trouverOptionSujet(sujet);
			StringBuilder optionSujet = new StringBuilder();
			for (OptionESEO option : recupOptions) {
				optionSujet.append(option.getNomOption()).append(" ");
				if (!options.contains(option.getNomOption())) {
					options.add(option.getNomOption());
				}
			}
			optionsSujet.add(optionSujet.toString());
		}
	}

	private void peuplerOptionsUtilisateur(List<Utilisateur> listeEtudiantsDispos, List<String> options, List<String> optionsEtudiant) {
		for (Utilisateur utilisateur : listeEtudiantsDispos) {
			List<OptionESEO> recupOptions = this.optionESEODAO.trouverOptionUtilisateur(utilisateur);
			StringBuilder optionUtilisateur = new StringBuilder();
			for (OptionESEO option : recupOptions) {
				optionUtilisateur.append(option.getNomOption()).append(" ");
				if (!options.contains(option.getNomOption())) {
					options.add(option.getNomOption());
				}
			}
			optionsEtudiant.add(optionUtilisateur.toString());
		}
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		/* Récupération de l'ID de l'utilisateur en session */
		HttpSession session = request.getSession();
		Utilisateur utilisateur = (Utilisateur) session.getAttribute(ServletUtilitaire.ATT_SESSION_USER);
		Long idUtilisateur = utilisateur.getIdUtilisateur();

		/* On regarde sur quel type de bouton l'utilisateur a appuyé */
		if ("validerEquipe".equalsIgnoreCase(request.getParameter(ServletUtilitaire.CHAMP_FORMULAIRE))) {
			/* On modifie l'équipe dans la BDD en la passant validée */
			String idEquipe = request.getParameter(ServletUtilitaire.CHAMP_ID_EQUIPE);
			Equipe equipe = new Equipe();
			equipe.setIdEquipe(idEquipe);
			List<Equipe> equipeAValider = this.equipeDAO.trouver(equipe);
			for (Equipe equipeTrouvee : equipeAValider) {
				equipeTrouvee.setValide("oui");
				this.equipeDAO.modifier(equipeTrouvee);
			}

			/* Notification à chaque membre de l'équipe */
			List<EtudiantEquipe> etudiantsEquipes = this.equipeDAO.listerEtudiantEquipe();
			for (EtudiantEquipe membre : etudiantsEquipes) {
				if (idEquipe.equals(membre.getIdEquipe())) {
					this.notificationDAO.ajouterNotification(membre.getIdEtudiant(), "Votre équipe a été validée.");
				}
			}

			/* Rechargement de la liste des équipes */
			response.sendRedirect(request.getHeader(ServletUtilitaire.HEADER));

		} else if ("refuserEquipe".equalsIgnoreCase(request.getParameter(ServletUtilitaire.CHAMP_FORMULAIRE))) {
			doRefuserEquipe(request, response);
		} else if ("supprimerMembre".equalsIgnoreCase(request.getParameter(ServletUtilitaire.CHAMP_FORMULAIRE))) {
			doSupprimerMembre(request, response, idUtilisateur);
		} else if ("ajouterMembre".equalsIgnoreCase(request.getParameter(ServletUtilitaire.CHAMP_FORMULAIRE))) {
			doAjouterMembre(request, response, idUtilisateur);

		}
	}

	private void doAjouterMembre(HttpServletRequest request, HttpServletResponse response, Long idUtilisateur) throws IOException {
		/*
		 * On vérifie les informations de l'équipe et du sujet pour voir si l'ajout est
		 * conforme
		 */
		String idEquipe = request.getParameter(ServletUtilitaire.CHAMP_ID_EQUIPE);
		int tailleEquipe = 0;
		Long idSujet = null;
		int nbrMaxEleves = 0;
		String contratProSujet = "";
		Long idEtudiant = Long.parseLong(request.getParameter(ServletUtilitaire.CHAMP_ID_MEMBRE));
		String contratProEtudiant = "";

		/* Tous les paramètres de l'équipe */
		Equipe equipe = new Equipe();
		equipe.setIdEquipe(idEquipe);
		List<Equipe> equipes = this.equipeDAO.trouver(equipe);
		for (Equipe equipeTrouvee : equipes) {
			tailleEquipe = equipeTrouvee.getTaille();
			idSujet = equipeTrouvee.getIdSujet();
		}

		/* Tous les paramètres du sujet */
		Sujet sujet = new Sujet();
		sujet.setIdSujet(idSujet);
		List<Sujet> sujets = this.sujetDAO.trouver(sujet);
		for (Sujet sujetTrouve : sujets) {
			nbrMaxEleves = sujetTrouve.getNbrMaxEleves();
			contratProSujet = sujetTrouve.getContratPro() ? "oui" : "non";
		}
		List<OptionESEO> optionsDuSujet = this.optionESEODAO.trouverOptionSujet(sujet);

		/* Tous les paramètres de l'étudiant */
		Etudiant etudiant = new Etudiant();
		etudiant.setIdEtudiant(idEtudiant);
		List<Etudiant> etudiants = this.etudiantDAO.trouver(etudiant);
		for (Etudiant etudiantTrouve : etudiants) {
			contratProEtudiant = etudiantTrouve.getContratPro();
		}
		Long optionEtudiant = 20L;
		Role roleEtudiant = new Role();
		roleEtudiant.setIdRole((long) 1);
		List<RoleUtilisateur> rolesEtudiant = this.roleDAO.listerRoleUtilisateur(roleEtudiant);
		for (RoleUtilisateur role : rolesEtudiant) {
			if (role.getIdUtilisateur().equals(idEtudiant)) {
				optionEtudiant = role.getIdOption();
			}
		}

		/* Comparaison des critères */
		boolean erreur = false;
		if (tailleEquipe == nbrMaxEleves) {
			this.notificationDAO.ajouterNotification(idUtilisateur,
					"Ajout impossible car la taille maximale de l'équipe a été atteinte.");
			erreur = true;
		}
		if (!contratProSujet.equals(contratProEtudiant)) {
			this.notificationDAO.ajouterNotification(idUtilisateur,
					"Ajout impossible car le sujet est en contrat pro et pas l'étudiant.");
			erreur = true;
		}
		boolean correspondanceOption = false;
		for (OptionESEO option : optionsDuSujet) {
			if (optionEtudiant.equals(option.getIdOption())) {
				correspondanceOption = true;
			}
		}
		if (!correspondanceOption) {
			this.notificationDAO.ajouterNotification(idUtilisateur,
					"Ajout impossible car les options ne correspondent pas.");
			erreur = true;
		}

		/* Ajout de l'étudiant dans l'équipe s'il n'y a pas d'erreurs */
		if (!erreur) {
			EtudiantEquipe ajout = new EtudiantEquipe();
			ajout.setIdEquipe(idEquipe);
			ajout.setIdEtudiant(idEtudiant);
			this.equipeDAO.attribuerEtudiantEquipe(ajout);
			int nouvelleTailleEquipe = tailleEquipe + 1;
			equipe.setTaille(nouvelleTailleEquipe);
			this.equipeDAO.modifier(equipe);
			this.notificationDAO.ajouterNotification(idEtudiant, "Vous avez été ajouté dans une équipe !");
		}

		/* Rechargement de la liste des équipes */
		response.sendRedirect(request.getHeader(ServletUtilitaire.HEADER));
	}

	private void doSupprimerMembre(HttpServletRequest request, HttpServletResponse response, Long idUtilisateur) throws IOException {
		/* On regarde la taille de l'équipe et nbrMinEleves */
		int tailleEquipe = Integer.parseInt(request.getParameter(ServletUtilitaire.CHAMP_TAILLE_EQUIPE));
		int nbrMinEleves = Integer.parseInt(request.getParameter(ServletUtilitaire.CHAMP_NBR_MIN_ELEVES));
		if (tailleEquipe == nbrMinEleves) {
			this.notificationDAO.ajouterNotification(idUtilisateur,
					"Suppression impossible car la taille minimale de l'équipe a été atteinte.");
		} else {
			EtudiantEquipe etudiantEquipe = new EtudiantEquipe();
			Long idMembre = Long.parseLong(request.getParameter(ServletUtilitaire.CHAMP_ID_MEMBRE));
			etudiantEquipe.setIdEtudiant(idMembre);
			this.equipeDAO.supprimerEtudiantEquipe(etudiantEquipe);
			this.notificationDAO.ajouterNotification(idMembre, "Vous avez été supprimé de votre équipe.");
		}

		/* Rechargement de la liste des équipes */
		response.sendRedirect(request.getHeader(ServletUtilitaire.HEADER));
	}

	private void doRefuserEquipe(HttpServletRequest request, HttpServletResponse response) throws IOException {
		/* On supprime l'équipe dans la BDD */
		String idEquipe = request.getParameter(ServletUtilitaire.CHAMP_ID_EQUIPE);
		List<EtudiantEquipe> etudiantsEquipes = this.equipeDAO.listerEtudiantEquipe();
		for (EtudiantEquipe membre : etudiantsEquipes) {
			if (idEquipe.equals(membre.getIdEquipe())) {
				this.notificationDAO.ajouterNotification(membre.getIdEtudiant(), "Votre équipe a été supprimée.");
			}
		}
		Equipe equipe = new Equipe();
		equipe.setIdEquipe(idEquipe);
		this.equipeDAO.supprimer(equipe);

		/* Rechargement de la liste des équipes */
		response.sendRedirect(request.getHeader(ServletUtilitaire.HEADER));
	}

	private List<Sujet> listeSujetParEquipe(List<Equipe> listeEquipes) {
		List<Sujet> listeSujetsEquipes = new ArrayList<>();
		for (Equipe equipe : listeEquipes) {
			Sujet sujet = new Sujet();
			sujet.setIdSujet(equipe.getIdSujet());
			List<Sujet> sujetEquipe = this.sujetDAO.trouver(sujet);
			for (Sujet sujetTrouve : sujetEquipe) {
				listeSujetsEquipes.add(sujetTrouve);
			}
		}
		return listeSujetsEquipes;
	}

}