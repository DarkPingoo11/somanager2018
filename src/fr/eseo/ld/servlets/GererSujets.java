package fr.eseo.ld.servlets;

import fr.eseo.ld.beans.*;
import fr.eseo.ld.dao.*;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.*;

/**
 * Servlet permettant la gestion des sujets.
 */
@WebServlet("/GererSujets")
@MultipartConfig
public class GererSujets extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private static final String VUE_FORM = "/WEB-INF/formulaires/gererSujets.jsp";

	private UtilisateurDAO utilisateurDAO;
	private SujetDAO sujetDAO;
	private OptionESEODAO optionESEODAO;
	private NotificationDAO notificationDAO;
	private CommentaireDAO commentaireDAO;

	@Override
	public void init() throws ServletException {
		/* Récupération d'instances de DAO */
		DAOFactory daoFactory = (DAOFactory) this.getServletContext().getAttribute(ServletUtilitaire.CONF_DAO_FACTORY);
		this.utilisateurDAO = daoFactory.getUtilisateurDao();
		this.sujetDAO = daoFactory.getSujetDao();
		this.optionESEODAO = daoFactory.getOptionESEODAO();
		this.notificationDAO = daoFactory.getNotificationDao();
		this.commentaireDAO = daoFactory.getCommentaireDao();
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		/* Récupération de la session depuis la requête */
		HttpSession session = request.getSession();

		// création de la liste de noms d'options de l'utilisateur:
		List<String> listNomsOptionUtilisateur = new ArrayList<>();

		/* Récupération des options de l'Utilisateur en session */
		Utilisateur utilisateur = (Utilisateur) session.getAttribute(ServletUtilitaire.ATT_SESSION_USER);
		List<OptionESEO> listOptionUtilisateur = null;
		if (utilisateur != null) {
			Long idUtilisateur = utilisateur.getIdUtilisateur();
			Utilisateur util = new Utilisateur();
			util.setIdUtilisateur(idUtilisateur);
			List<Utilisateur> listUtil = this.utilisateurDAO.trouver(util);
			listOptionUtilisateur = this.optionESEODAO.trouverOptionUtilisateur(listUtil.get(0));
		}

		// recherche des rôles de l'utilisateur:
		List<Role> rolesUtilisateur = this.utilisateurDAO.trouverRole(utilisateur);

		// attribution de ou des options pour lesquelles le telechargement de posters
		// est possible:
		// (option pour lequel le prof est responsable ou toutes les options si
		// l'utilisateurest le service com.
		List<OptionESEO> optionConcernee = new ArrayList<>();

		List<Commentaire> commentaires = new ArrayList<>();
		List<Commentaire> commentairesSecondaires = new ArrayList<>();

		// les seuls rôles permettant le telechargement de plusieurs posters sont
		// profResponsable et service com.
		optionConcernee = getOptionsConcernees(utilisateur, rolesUtilisateur, optionConcernee);

		// recupération de la liste de noms d'options de l'utilisateur:
		for (OptionESEO option : optionConcernee) {
			listNomsOptionUtilisateur.add(option.getNomOption());
		}

		/* Recherche de tous les sujets via sujetDao */
		List<Sujet> listeSujets = this.sujetDAO.lister();

		/*
		 * Changement du format des listes pour pouvoir appliquer des filtres de
		 * sélection dans les JSP
		 */
		request.setAttribute(ServletUtilitaire.ATT_LISTE_SUJETS, listeSujets);

		/* Jointure de Utilisateur et Sujet via idUtilisateur */
		Map<Long, String> nomsPorteursSujets = new HashMap<>();
		Map<Long, Long> idPorteursSujets = new HashMap<>();
		Map<Long, String> nomsExpediteursCommentaires = new HashMap<>();

		List<String> options = new ArrayList<>();
		List<String> optionsSujet = new ArrayList<>();

		/* Recuperation du role de l'utilisateur */
		boolean profResp = ServletUtilitaire.testeRoleResponsable(rolesUtilisateur);

		for (Iterator<Sujet> iter = listeSujets.listIterator(); iter.hasNext();) {
			Sujet sujet = iter.next();
			if (isEtatEtRoleOK(profResp, sujet)) {
				/*
				 * Si le sujet est en contrat pro, seul le responsable d'option peut le
				 * publier/refuser
				 */
				if (isSujetProOUResp(profResp, sujet)) {
					/* Recherche de l'utilisateur qui a déposé ce sujet */
					Long idUtilisateurSujet = getIdUtilisateurPorteurSujet(sujet, null);
					Utilisateur utilisateurSujet = new Utilisateur();
					utilisateurSujet.setIdUtilisateur(idUtilisateurSujet);
					List<Utilisateur> utilisateursTrouves = this.utilisateurDAO.trouver(utilisateurSujet);

					/* Correspondance entre l'ID d'un utilisateur et son nom */
					String nom;
					Long id = -1L;
					if (!utilisateursTrouves.isEmpty()) { // sécurité dans le cas où la BDD ait mal été remplie
						nom = utilisateursTrouves.get(0).getNom() + " " + utilisateursTrouves.get(0).getPrenom();
						id = utilisateursTrouves.get(0).getIdUtilisateur();
					} else {
						nom = "";
					}
					nomsPorteursSujets.put(sujet.getIdSujet(), nom);
					idPorteursSujets.put(sujet.getIdSujet(), id);

					/* Recherche des commentaires du sujet */
					Commentaire commentaireSujet = new Commentaire();
					commentaireSujet.setIdSujet(sujet.getIdSujet());
					List<Commentaire> commentairesTrouves = this.commentaireDAO.trouver(commentaireSujet);

					recupererCommentaires(commentaires, commentairesSecondaires, nomsExpediteursCommentaires, commentairesTrouves);

					/* Recherche des options du sujet */
					List<OptionESEO> recupOptions = this.optionESEODAO.trouverOptionSujet(sujet);
					StringBuilder optionSujet = new StringBuilder();
					boolean optionOK = false;
					optionOK = isOptionOK(listOptionUtilisateur, options, recupOptions, optionSujet, optionOK);

					/* on retire le sujet de la liste s'il n'a pas le droit de le voir */
					retirerSujetListeSiDroit(optionsSujet, iter, optionSujet, optionOK);

				} else { // Sujet en contrat pro et l'utilisateur est un professeur d'option
					iter.remove();
				}
			} else { // Sujet ni validé, ni refusé, ni déposé
				iter.remove();
			}
		}

		request.setAttribute(ServletUtilitaire.ATT_NOMS_PORTEURS_SUJETS, nomsPorteursSujets);
		request.setAttribute("idPorteursSujets", idPorteursSujets);
		request.setAttribute("optionSujet", optionsSujet);
		request.setAttribute(ServletUtilitaire.ATT_LISTE_OPTIONS, options);
		request.setAttribute("listNomsOptionUtilisateur", listNomsOptionUtilisateur);
		request.setAttribute(ServletUtilitaire.ATT_NOMS_EXPEDITEURS_COMMENTAIRES, nomsExpediteursCommentaires);
		request.setAttribute(ServletUtilitaire.ATT_COMMENTAIRES, commentaires);
		request.setAttribute(ServletUtilitaire.ATT_COMMENTAIRES_SECONDAIRES, commentairesSecondaires);
		request.setAttribute(ServletUtilitaire.ATT_ID_PORTEURS_SUJETS, idPorteursSujets);
		request.setAttribute("etatDepose", EtatSujet.DEPOSE);
		
		/* Affichage du formulaire */
		this.getServletContext().getRequestDispatcher(VUE_FORM).forward(request, response);
	}

	private boolean isSujetProOUResp(boolean profResp, Sujet sujet) {
		return sujet.getContratProInt() == 0 || profResp;
	}

	private Long getIdUtilisateurPorteurSujet(Sujet sujet, Long idUtilisateurSujet) {
		if(utilisateurDAO.trouverPorteurSujet(sujet) == null) {
			idUtilisateurSujet = utilisateurDAO.trouverPorteurSujet(sujet).get(0).getIdUtilisateur();
		}
		return idUtilisateurSujet;
	}

	private void retirerSujetListeSiDroit(List<String> optionsSujet, Iterator<Sujet> iter, StringBuilder optionSujet, boolean optionOK) {
		if (!optionOK) {
			iter.remove();
		} else {
			optionsSujet.add(optionSujet.toString());
		}
	}

	private boolean isEtatEtRoleOK(boolean profResp, Sujet sujet) {
		return (sujet.getEtat().equals(EtatSujet.VALIDE) && profResp) || sujet.getEtat().equals(EtatSujet.DEPOSE)
				|| sujet.getEtat().equals(EtatSujet.REFUSE);
	}

	private boolean isOptionOK(List<OptionESEO> listOptionUtilisateur, List<String> options, List<OptionESEO> recupOptions, StringBuilder optionSujet, boolean optionOK) {
		for (OptionESEO option : recupOptions) {
			optionSujet.append(option.getNomOption()).append(" ");
			if (!options.contains(option.getNomOption())) {
				options.add(option.getNomOption());
			}
			for (OptionESEO optionU : listOptionUtilisateur) {
				if (optionU.getIdOption().equals(option.getIdOption())) { // Pour savoir si le sujet est
																			// proposé dans l'option de
																			// l'utilisateur
					optionOK = true;
				}
			}
		}
		return optionOK;
	}

	private void recupererCommentaires(List<Commentaire> commentaires, List<Commentaire> commentairesSecondaires, Map<Long, String> nomsExpediteursCommentaires, List<Commentaire> commentairesTrouves) {
		List<Utilisateur> utilisateursTrouves;
		String nom;
		for (Commentaire commentaire : commentairesTrouves) {

			/* Recherche des utilisateurs ayant commenté ce sujet */
			Long idUtilisateurCommentaire = commentaire.getIdUtilisateur();
			Utilisateur utilisateurCommentaire = new Utilisateur();
			utilisateurCommentaire.setIdUtilisateur(idUtilisateurCommentaire);
			utilisateursTrouves = this.utilisateurDAO.trouver(utilisateurCommentaire);

			// Correspondance entre l'ID d'un utilisateur et son nom
			if (!utilisateursTrouves.isEmpty()) { // sécurité dans le cas où la BDD a mal été remplie
				nom = utilisateursTrouves.get(0).getPrenom() + " " + utilisateursTrouves.get(0).getNom();
			} else {
				nom = "";
			}
			nomsExpediteursCommentaires.put(idUtilisateurCommentaire, nom);

			// Ajoute un commentaire primaire ou secondaire
			if (commentaire.getIdObservation() != -1) { // s'il s'agit d'un commentaire secondaire
				commentairesSecondaires.add(commentaire);
			} else {
				commentaires.add(commentaire);
			}
		}
	}

	private List<OptionESEO> getOptionsConcernees(Utilisateur utilisateur, List<Role> rolesUtilisateur, List<OptionESEO> optionConcernee) {
		for (Role role : rolesUtilisateur) {
			// cas du profResponsable:
			if (("profResponsable").equals(role.getNomRole())) {
				optionConcernee = this.optionESEODAO.trouverOptionRoleUtilisateur(utilisateur.getIdUtilisateur(),
						role.getIdRole());
			} else if (("serviceCom").equals(role.getNomRole())) {
				optionConcernee = this.optionESEODAO.lister();
			}
		}
		return optionConcernee;
	}

	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		/* Récupération de la session depuis la requête */
		HttpSession session = request.getSession();
		/* Récupération de l'ID de l'Utilisateur en session */
		Utilisateur utilisateur = (Utilisateur) session.getAttribute(ServletUtilitaire.ATT_SESSION_USER);
		Long idUtilisateur = utilisateur.getIdUtilisateur();

		/* Récupération de l'id du Sujet choisi */
		Long idSujet = Long.parseLong(request.getParameter(ServletUtilitaire.CHAMP_ID_SUJET));

		/* Récuperation du sujet */
		Sujet sujetAModifier = new Sujet();
		sujetAModifier.setIdSujet(idSujet);
		List<Sujet> sujetsAModifier = this.sujetDAO.trouver(sujetAModifier);
		StringBuilder texteNotification = new StringBuilder("Le sujet ");
		for (Sujet sujetTrouve : sujetsAModifier) { // 1 seul sujet
			texteNotification.append(sujetTrouve.getTitre());

			/* Modification du sujet selectionné */
			if (sujetTrouve.getEtat() == EtatSujet.DEPOSE) {

				/* 2 actions possibles sur un sujet déposer : */
				String action = request.getParameter(ServletUtilitaire.ATT_ACTION_GERER_SUJET);
				if ("refuser".equals(action)) {
					/* Refus */
					sujetTrouve.setEtat(EtatSujet.REFUSE);
					texteNotification.append(" a bien été refusé.");
				} else {
					/* Validation */
					sujetTrouve.setEtat(EtatSujet.VALIDE);
					texteNotification.append(" a bien été validé.");
				}
			} else {
				/* Publication */
				sujetTrouve.setEtat(EtatSujet.PUBLIE);
				texteNotification.append(" a bien été publié.");
			}
			this.sujetDAO.modifier(sujetTrouve);
		}

		/* On notifie l'utilisateur que son équipe a été créée */
		this.notificationDAO.ajouterNotification(idUtilisateur, texteNotification.toString());

		/* Rechargement de la page */
		response.sendRedirect(request.getHeader(ServletUtilitaire.HEADER));

	}

}