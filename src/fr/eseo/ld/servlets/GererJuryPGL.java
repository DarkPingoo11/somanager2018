package fr.eseo.ld.servlets;

import fr.eseo.ld.beans.Callback;
import fr.eseo.ld.beans.CallbackType;
import fr.eseo.ld.beans.Utilisateur;
import fr.eseo.ld.dao.*;
import fr.eseo.ld.pgl.beans.CompositionJury;
import fr.eseo.ld.pgl.beans.Soutenance;
import fr.eseo.ld.pgl.beans.Sprint;
import fr.eseo.ld.utils.CallbackUtilitaire;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@WebServlet("/GererJuryPGL")
public class GererJuryPGL extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private static final String VUE_FORM = "/WEB-INF/formulaires/gererJurysPGL.jsp";
	private static final String ATT_ONGLET_ACTIF = "onglet";
	private static final String ATT_SPRINT_DEMO = "SprintDemo";
	private static final String ATT_SPRINT_REVIEW = "SprintReview";
	private static final String ATT_DATE_POPUP = "datePopup";
	private static final String ATT_NUM_SOUTENANCE = "numSoutenance";

	private ProfesseurDAO professeurDAO;
	private CompositionJuryDAO compositionJuryDAO;
	private UtilisateurDAO utilisateurDAO;
	private PglSoutenanceDAO pglSoutenanceDAO;
	private OptionESEODAO optionESEODAO;
	private SprintDAO sprintDAO;
	private NotificationDAO notificationDAO;

	@Override
	public void init() throws ServletException {
		DAOFactory daoFactory = (DAOFactory) this.getServletContext().getAttribute(ServletUtilitaire.CONF_DAO_FACTORY);
		this.professeurDAO = daoFactory.getProfesseurDAO();
		this.compositionJuryDAO = daoFactory.getCompositionJuryDAO();
		this.utilisateurDAO = daoFactory.getUtilisateurDao();
		this.pglSoutenanceDAO = daoFactory.getPGLSoutenanceDAO();
		this.optionESEODAO = daoFactory.getOptionESEODAO();
		this.sprintDAO = daoFactory.getSprintDAO();
		this.notificationDAO = daoFactory.getNotificationDao();
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		List<CompositionJury> listCompositionJury;
		List<Utilisateur> listeUtilisateur;
		List<Soutenance> listeSoutenanceReview = new ArrayList<>();
		List<Soutenance> listeSoutenanceDemo = new ArrayList<>();
		List<Utilisateur> listeProfesseursOptionsLD;
		List<Sprint> listeSprint;

		// Remplissage de la liste de toutes les compositions de Jury
		listCompositionJury = this.compositionJuryDAO.lister();
		// Remplissage de la liste de tous les utilisateurs
		listeUtilisateur = this.utilisateurDAO.lister();
		// Remplissage de la liste des Soutenance de sprint Review
		for (int i = 0; i < this.pglSoutenanceDAO.lister().size(); i++) {
			if (this.pglSoutenanceDAO.lister().get(i).getLibelle().equalsIgnoreCase(ATT_SPRINT_REVIEW)) {
				listeSoutenanceReview.add(this.pglSoutenanceDAO.lister().get(i));
			}
		}
		// Remplissage de la liste des Soutenance de sprint Demo
		for (int i = 0; i < this.pglSoutenanceDAO.lister().size(); i++) {
			if (this.pglSoutenanceDAO.lister().get(i).getLibelle().equalsIgnoreCase(ATT_SPRINT_DEMO)) {
				listeSoutenanceDemo.add(this.pglSoutenanceDAO.lister().get(i));
			}
		}
		// Remplissage de la liste des professeurs de l'option LD
		listeProfesseursOptionsLD = listerProfesseurLD();
		// Remplissage des sprints
		listeSprint = this.sprintDAO.lister();

		request.setAttribute("listCompositionJury", listCompositionJury);
		request.setAttribute("listeUtilisateur", listeUtilisateur);
		request.setAttribute("listeSoutenanceReview", listeSoutenanceReview);
		request.setAttribute("listeSoutenanceDemo", listeSoutenanceDemo);
		request.setAttribute("listeProfesseursOptionsLD", listeProfesseursOptionsLD);
		request.setAttribute("listeSprint", listeSprint);
		this.getServletContext().getRequestDispatcher(VUE_FORM).forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		List<Long> idParticipants;
		List<Long> idNonParticipants;
		Long numSoutenance = null;

		// Modifie le Jury de sprint Review
		if ("modifierReview".equals(request.getParameter("modifierJury"))) {
			idParticipants = idParticipants(request);
			idNonParticipants = idNonParticipants(request);
			numSoutenance = Long.parseLong(request.getParameter(ATT_NUM_SOUTENANCE));
			modifierJurySprintReview(idParticipants, idNonParticipants, request.getParameter(ATT_DATE_POPUP),
					numSoutenance, ATT_SPRINT_REVIEW);
			CallbackUtilitaire.setCallback(request, new Callback(CallbackType.SUCCESS,
					"La modification du jury de sprint review a été prise en compte"));
		}
		// Modifie le Jury de sprint Demo
		if ("modifierDemo".equals(request.getParameter("modifierJury2"))) {
			idParticipants = idParticipants(request);
			idNonParticipants = idNonParticipants(request);
			numSoutenance = Long.parseLong(request.getParameter(ATT_NUM_SOUTENANCE));
			modifierJurySprintReview(idParticipants, idNonParticipants, request.getParameter(ATT_DATE_POPUP),
					numSoutenance, ATT_SPRINT_DEMO);
			CallbackUtilitaire.setCallback(request, new Callback(CallbackType.SUCCESS,
					"La modification du jury de sprint de démonstration a été prise en compte"));
		}
		// Supprime une soutenance et les jurys associés
		if ("supprimer".equals(request.getParameter("delSout"))) {
			numSoutenance = Long.parseLong(request.getParameter(ATT_NUM_SOUTENANCE));
			supprimerJury(numSoutenance);
			CallbackUtilitaire.setCallback(request, new Callback(CallbackType.SUCCESS, "Le jury a été supprimé"));
		}
		// Créer la soutenance de sprint review et les jury associés
		if ("creerJurySprintReview".equals(request.getParameter("creerSoutenance"))) {
			idParticipants = idParticipants(request);
			creerJurySprint(idParticipants, request.getParameter(ATT_DATE_POPUP), ATT_SPRINT_REVIEW);
			CallbackUtilitaire.setCallback(request,
					new Callback(CallbackType.SUCCESS, "Le jury de sprint Review a été créé"));
		}
		// Créer la soutenance de sprint demo et les jury associés
		if ("creerJurySprintDemo".equals(request.getParameter("creerSoutenance2"))) {
			idParticipants = idParticipants(request);
			creerJurySprint(idParticipants, request.getParameter(ATT_DATE_POPUP), ATT_SPRINT_DEMO);
			CallbackUtilitaire.setCallback(request,
					new Callback(CallbackType.SUCCESS, "Le jury de sprint Demo a été créé"));
		}
		if ("jurySprintDem".equals(request.getParameter("ongletJuryDemo"))) {
			request.setAttribute(ATT_ONGLET_ACTIF, "jurySprintDemo");
		}

		doGet(request, response);
	}

	/**
	 * 
	 * @param request
	 * @return liste des professeurs participants au Jury
	 */
	protected List<Long> idParticipants(HttpServletRequest request) {
		List<Long> idParticipants = new ArrayList<>();

		for (int i = 0; i < listerProfesseurLD().size(); i++) {
			if (request.getParameter("checkbox-" + i) != null) {
				idParticipants.add(Long.parseLong(request.getParameter("checkbox-" + i)));
			}
		}
		return idParticipants;
	}

	/**
	 * 
	 * @param request
	 * @return liste des professeurs ne participants pas au Jury
	 */
	protected List<Long> idNonParticipants(HttpServletRequest request) {
		List<Long> idNonParticipants = new ArrayList<>();
		List<Long> idParticipants = idParticipants(request);
		List<Utilisateur> listeProfesseursOptionsLD = listerProfesseurLD();

		for (int i = 0; i < listeProfesseursOptionsLD.size(); i++) {
			if (!idParticipants.contains(listeProfesseursOptionsLD.get(i).getIdUtilisateur())) {
				idNonParticipants.add(listeProfesseursOptionsLD.get(i).getIdUtilisateur());
			}
		}
		return idNonParticipants;
	}

	/**
	 * Liste tous les proffesseur de l'option LD
	 * 
	 * @return liste des professeur de l'option LD
	 */
	protected List<Utilisateur> listerProfesseurLD() {
		List<Utilisateur> listeProfesseursOptionsLD = new ArrayList<>();
		for (int i = 0; i < this.optionESEODAO.lister().size(); i++) {
			if (this.optionESEODAO.lister().get(i).getNomOption().equalsIgnoreCase("LD")) {
				listeProfesseursOptionsLD = this.professeurDAO
						.listerProfesseursOption(this.optionESEODAO.lister().get(i));
			}
		}
		return listeProfesseursOptionsLD;
	}

	/**
	 * Modifie le jury de sprint Review ou Démonstration sélectionné avec les
	 * nouveaux paramètres
	 * 
	 * @param idsUtilisateurs
	 * @param nonParticipants
	 * @param date
	 * @param numSoutenance
	 * @param libelle
	 */
	protected void modifierJurySprintReview(List<Long> idsUtilisateurs, List<Long> nonParticipants, String date,
			Long numSoutenance, String libelle) {
		CompositionJury compoJury = new CompositionJury();
		Soutenance soutenance = new Soutenance();
		soutenance.setIdSoutenance(numSoutenance);
		soutenance.setDate(date);
		soutenance.setLibelle(libelle);

		this.pglSoutenanceDAO.modifier(soutenance);

		for (int i = 0; i < idsUtilisateurs.size(); i++) {
			compoJury.setRefSoutenance(numSoutenance);
			compoJury.setRefProfesseur(idsUtilisateurs.get(i));
			this.compositionJuryDAO.creer(compoJury);
			this.notificationDAO.ajouterNotification(idsUtilisateurs.get(i),
					"Vous avez été affecté à un jury de sprint");
		}
		for (int i = 0; i < nonParticipants.size(); i++) {
			compoJury.setRefProfesseur(nonParticipants.get(i));
			compoJury.setRefSoutenance(numSoutenance);
			this.compositionJuryDAO.supprimer(compoJury);
		}

	}

	/**
	 * Supprime le Jury sélectionné
	 * 
	 * @param numSoutenance
	 */
	protected void supprimerJury(Long numSoutenance) {
		CompositionJury compoJury = new CompositionJury();
		Soutenance soutenance = new Soutenance();

		compoJury.setRefSoutenance(numSoutenance);
		soutenance.setIdSoutenance(numSoutenance);

		this.compositionJuryDAO.supprimer(compoJury);
		this.pglSoutenanceDAO.supprimer(soutenance);
	}

	/**
	 * Creer un jury de sprint Review ou Démonstration
	 * 
	 * @param idsUtilisateurs
	 * @param date
	 * @param libelle
	 */
	protected void creerJurySprint(List<Long> idsUtilisateurs, String date, String libelle) {
		CompositionJury compoJury = new CompositionJury();
		Soutenance soutenance = new Soutenance();

		soutenance.setDate(date);
		soutenance.setLibelle(libelle);

		this.pglSoutenanceDAO.creer(soutenance);
		for (int i = 0; i < idsUtilisateurs.size(); i++) {
			compoJury.setRefSoutenance(
					this.pglSoutenanceDAO.lister().get(this.pglSoutenanceDAO.lister().size() - 1).getIdSoutenance());
			compoJury.setRefProfesseur(idsUtilisateurs.get(i));
			this.compositionJuryDAO.creer(compoJury);
			this.notificationDAO.ajouterNotification(idsUtilisateurs.get(i),
					"Vous avez été affecté à un jury de sprint");
		}
	}

}
