package fr.eseo.ld.servlets;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.mindrot.jbcrypt.BCrypt;

import fr.eseo.ld.beans.EtatSujet;
import fr.eseo.ld.beans.EtudiantEquipe;
import fr.eseo.ld.beans.OptionESEO;
import fr.eseo.ld.beans.Role;
import fr.eseo.ld.beans.Utilisateur;
import fr.eseo.ld.dao.DAOFactory;
import fr.eseo.ld.dao.EquipeDAO;
import fr.eseo.ld.dao.NotificationDAO;
import fr.eseo.ld.dao.UtilisateurDAO;
import fr.eseo.ld.ldap.AuthentificationAD;

/**
 * Servlet permettant la connexion d'un utilisateur.
 * 
 * BCrypt : Copyright (c) 2002 Johnny Shelley All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the author nor any contributors may be used to endorse
 * or promote products derived from this software without specific prior written
 * permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
@WebServlet("/Connexion")
public class Connexion extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private static final String ERREUR_SAISIE = "Identifiant ou Mot de passe incorrect.";
	private static final String VUE_FORM = "/WEB-INF/formulaires/connexion.jsp";

	private static Logger logger = Logger.getLogger(Connexion.class.getName());

	private UtilisateurDAO utilisateurDAO;
	private EquipeDAO equipeDAO;
	private NotificationDAO notificationDAO;

	@Override
	public void init() throws ServletException {
		/* Récupération d'instances de DAO */
		DAOFactory daoFactory = (DAOFactory) this.getServletContext().getAttribute(ServletUtilitaire.CONF_DAO_FACTORY);
		this.utilisateurDAO = daoFactory.getUtilisateurDao();
		this.equipeDAO = daoFactory.getEquipeDAO();
		this.notificationDAO = daoFactory.getNotificationDao();
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		/* A la réception d'une requête GET, simple affichage du formulaire */
		this.getServletContext().getRequestDispatcher(VUE_FORM).forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		/* Récupération des données saisies */
		String identifiant = request.getParameter(ServletUtilitaire.CHAMP_IDENTIFIANT);
		String motDePasse = request.getParameter(ServletUtilitaire.CHAMP_MOT_DE_PASSE);

		/*
		 * Utilisation d'UtilisateurDAO pour chercher un utilisateur via son identifiant
		 */
		Utilisateur utilisateur = new Utilisateur();
		utilisateur.setIdentifiant(identifiant);
		List<Utilisateur> utilisateurs = this.utilisateurDAO.trouver(utilisateur);

		/* Validation des champs */
		Map<String, String> erreurs = this.validerChamps(utilisateurs, identifiant, motDePasse);

		request.setAttribute(ServletUtilitaire.CHAMP_ETAT_DEPOSE, EtatSujet.DEPOSE);
		/*
		 * Si aucune erreur de validation n'a eu lieu, alors ajout du bean Utilisateur à
		 * la session, sinon ré-affichage du formulaire.
		 */
		if (erreurs.isEmpty()) {
			/* Chargement des roles de l'utilisateur */
			List<Role> roles = this.utilisateurDAO.trouverRole(utilisateurs.get(0));
			/* Chargement des options de l'utilisateur */
			List<OptionESEO> options = this.utilisateurDAO.trouverOption(utilisateurs.get(0));

			/*
			 * Chargement du boolean indiquant si l'utilisateur appartient ou non à une
			 * équipe
			 */
			boolean dansEquipe = false;
			List<EtudiantEquipe> etudiantEquipes = this.equipeDAO.listerEtudiantEquipe();
			for (EtudiantEquipe etudiantEquipe : etudiantEquipes) {
				if (etudiantEquipe.getIdEtudiant().equals(utilisateurs.get(0).getIdUtilisateur())) {
					dansEquipe = true;
				}
			}

			/* Récupération de la session depuis la requête */
			HttpSession session = request.getSession();
			session.setAttribute(ServletUtilitaire.ATT_SESSION_USER, utilisateurs.get(0));
			session.setAttribute(ServletUtilitaire.ATT_SESSION_ROLES, roles);
			session.setAttribute(ServletUtilitaire.ATT_SESSION_OPTIONS, options);
			session.setAttribute(ServletUtilitaire.ATT_SESSION_ETUDIANT_EQUIPE, dansEquipe);
			/* Ajout d'une notification */
			this.notificationDAO.ajouterNotification(utilisateurs.get(0).getIdUtilisateur(), "Connexion réussie !");
			/* Redirection vers la page d'accueil */
			response.sendRedirect(request.getContextPath() + ServletUtilitaire.VUE_DASHBOARD);
		} else {
			String resultat = "Échec de la connexion. ";
			logger.log(Level.WARN, resultat + erreurs.get(ServletUtilitaire.CHAMP_MOT_DE_PASSE));
			request.setAttribute(ServletUtilitaire.ATT_RESULTAT, resultat);
			request.setAttribute(ServletUtilitaire.ATT_ERREURS, erreurs);
			/* Ré-affichage du formulaire avec les erreurs */
			this.getServletContext().getRequestDispatcher(VUE_FORM).forward(request, response);
		}
	}

	/**
	 * Valide les champs saisis dans le formulaire de connexion.
	 * 
	 * @param utilisateurs
	 *            la liste des utilisateurs trouvés via un identifiant.
	 * @param identifiant
	 *            l'identifiant saisi dans le formulaire.
	 * @param motDePasse
	 *            le mot de passe saisi dans le formulaire.
	 * 
	 * @return erreurs la map des erreurs occasionnées par la saisie d'un mauvais
	 *         identifiant et/ou mot de passe.
	 */
	private Map<String, String> validerChamps(List<Utilisateur> utilisateurs, String identifiant, String motDePasse) {
		Map<String, String> erreurs = new HashMap<>();
		if (utilisateurs.isEmpty()) { // si aucun utilisateur ne possède cet identifiant
			erreurs.put(ServletUtilitaire.CHAMP_MOT_DE_PASSE, ERREUR_SAISIE);
		} else if ("activeDirectory".equals(utilisateurs.get(0).getHash())) { // si l'utilisateur appartient à l'AD
			AuthentificationAD autentification = new AuthentificationAD();
			String nomCommun = autentification.recupererNomCommun(identifiant);
			if (autentification.connexionAD(nomCommun, motDePasse) == null) {
				erreurs.put(ServletUtilitaire.CHAMP_MOT_DE_PASSE, ERREUR_SAISIE);
			}
			if ("non".equals(utilisateurs.get(0).getValide())) {
				erreurs.put(ServletUtilitaire.CHAMP_MOT_DE_PASSE, "Compte non validé par l'administrateur.");
			}
		} else { // si l'utilisateur n'appartient pas à l'AD
			if (!BCrypt.checkpw(motDePasse, utilisateurs.get(0).getHash())) {
				erreurs.put(ServletUtilitaire.CHAMP_MOT_DE_PASSE, ERREUR_SAISIE);
			}
			if ("non".equals(utilisateurs.get(0).getValide())) {
				erreurs.put(ServletUtilitaire.CHAMP_MOT_DE_PASSE, "Compte non validé par l'administrateur.");
			}
		}
		return erreurs;
	}

}