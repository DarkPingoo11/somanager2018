package fr.eseo.ld.servlets;

import fr.eseo.ld.beans.Notification;
import fr.eseo.ld.beans.Utilisateur;
import fr.eseo.ld.dao.DAOFactory;
import fr.eseo.ld.dao.NotificationDAO;
import fr.eseo.ld.utils.ParamUtilitaire;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

/**
 * Servlet permettant de notifier un utilisateur.
 * 
 * <p>
 * Utilisation de Server-Sent Events.
 * </p>
 * 
 * @author Hugo MENARD
 */
@WebServlet("/Notifier")
public class Notifier extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private static final String BUTTON = "</a>";

	private static final String BUTTON_TYPE_VALUE = "<a role=\"button\" value=\"";

	private static Logger logger = Logger.getLogger(Notifier.class.getName());

	private NotificationDAO notificationDAO;

	@Override
	public void init() throws ServletException {
		/* Récupération d'une instance de notre DAO Notification */
		this.notificationDAO = ((DAOFactory) this.getServletContext().getAttribute(ServletUtilitaire.CONF_DAO_FACTORY))
				.getNotificationDao();
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		/* Récupération de l'Utilisateur en session */
		HttpSession session = request.getSession();
		Utilisateur utilisateur = (Utilisateur) session.getAttribute(ServletUtilitaire.ATT_SESSION_USER);

		/* Si l'utilisateur n'est pas connecté */
		if (utilisateur == null) {
			/* Arrêt de la méthode */
			return;
		}

		/* Recherche des notifications reçues par l'utilisateur en session */
		Notification notification = new Notification();
		notification.setIdUtilisateur(utilisateur.getIdUtilisateur());
		List<Notification> listeNotifications = this.notificationDAO.trouver(notification);

		/* Traitement de la réponse */
		response.setContentType("text/event-stream");
		response.setCharacterEncoding("UTF-8"); // l'encoding doit être en UTF-8

		/* Ecriture des data */
		PrintWriter writer = response.getWriter();
		StringBuilder notifs = new StringBuilder("data: ");
		for (Notification notif : listeNotifications) {
			if (notif.getVue() == 0) {
				notifs.append(BUTTON_TYPE_VALUE + notif.getCommentaire() + "//" + notif.getIdNotification() + "//" + notif.getLien()
						+ "\" id=\"submit\" name=\"submit\" class=\"notificationLien\" style='background-color: #d3e9ff'>"
						+ notif.getCommentaire() + BUTTON);
				StringBuilder nouvelleNotif = new StringBuilder("data: " + notif.getCommentaire() + "/" + notif.getLien());
				nouvelleNotif.append("\n\n");
				writer.write("event:new_notifs\n");
				writer.write(String.valueOf(nouvelleNotif));

				notification.setVue(1);
				notification.setIdNotification(notif.getIdNotification());
                notification.setIdUtilisateur(notif.getIdUtilisateur());

				this.notificationDAO.modifier(notification);

				notification = new Notification();
				notification.setIdUtilisateur(utilisateur.getIdUtilisateur());
			} else if (notif.getVue() == 1) {
				notifs.append(BUTTON_TYPE_VALUE + notif.getCommentaire() + "//" + notif.getIdNotification() + "//" + notif.getLien() + "\""
						+ "\" id=\"submit\" name=\"submit\" class=\"notificationLien\" style='background-color: #d3e9ff'>"
						+ notif.getCommentaire() + BUTTON);
			}
		}
		notifs.append("\n\n");
		writer.write("event:toute_notifs\n");
		writer.write(String.valueOf(notifs));
		writer.flush();
		writer.checkError();

		/* Mise en pause de la méthode qui va être rappelée */
		try {
			Thread.sleep(15000);
		} catch (InterruptedException e) {
			logger.log(Level.WARN, "Notifications interrompues.", e);
			Thread.currentThread().interrupt();
		}
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		/* Récupération de l'Utilisateur en session */
		HttpSession session = request.getSession();
		Utilisateur utilisateur = (Utilisateur) session.getAttribute(ServletUtilitaire.ATT_SESSION_USER);

		/* Récupération des données */
		String data = request.getParameter("data");
		String[] donnees;
		donnees = data.split("//");
		donnees[2] = request.getContextPath() + donnees[2];

		/* Modification de la notification dans la BDD */
		Notification notification = new Notification();
		notification.setIdUtilisateur(utilisateur.getIdUtilisateur());
		notification.setVue(2);
		Integer id = ParamUtilitaire.getInt(donnees[1]);
		if(ParamUtilitaire.notNull(id)) {
            notification.setIdNotification((long)id);
            this.notificationDAO.modifier(notification);
        }

		/* Redirection vers l'adresse communiquée dans les données */
		response.sendRedirect(donnees[2]);
	}

}