package fr.eseo.ld.servlets;

import fr.eseo.ld.beans.*;
import fr.eseo.ld.dao.*;
import org.mindrot.jbcrypt.BCrypt;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;
import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Classe Utilitaire pour les servlets.
 *
 * <p>
 * Cette classe centralise des attributs et des méthodes utilisés par les
 * servlets.
 * </p>
 *
 * @author Hugo MENARD et Maxime LENORMAND
 */
public final class ServletUtilitaire {

    private static final String OPTIONS = "options";
    public static final String CONF_DAO_FACTORY = "daoFactory";

    public static final String ATT_SESSION_USER = "utilisateur";
    public static final String ATT_SESSION_ROLES = "roles";
    public static final String ATT_SESSION_OPTIONS = OPTIONS;
    public static final String ATT_SESSION_ETUDIANT_EQUIPE = "etudiantEquipe";

    public static final String ATT_RESULTAT = "resultat";
    public static final String ATT_ERREURS = "erreurs";
    public static final String ATT_UTILISATEURS = "utilisateurs";
    public static final String ATT_LISTE_EQUIPES = "listeEquipes";
    public static final String ATT_LISTE_ETUDIANTS = "listeEtudiants";
    public static final String ATT_LISTE_ETUDIANTS_DISPOS = "listeEtudiantsDispos";
    public static final String ATT_LISTE_ETUDIANTS_I2_DISPOS = "listeEtudiantsI2Dispos";
    public static final String ATT_LISTE_ETUDIANT_DISPOS_CONTRATPRO = "etudiantsDisponiblesContratPro";
    public static final String ATT_LISTE_ETUDIANT_EQUIPE = "listeEtudiantsEquipes";
    public static final String ATT_LISTE_ETUDIANTS_I3 = "listeEtudiantsI3";
    public static final String ATT_LISTE_SUJETS = "listeSujets";
    public static final String ATT_LISTE_SUJETS_EQUIPES = "listeSujetsEquipes";
    public static final String ATT_ID_PORTEURS_SUJETS = "idPorteursSujets";
    public static final String ATT_OPTIONS_ETUDIANT = "optionEtudiant";
    public static final String ATT_OPTIONS_SUJET = "optionSujet";
    public static final String ATT_LISTE_OPTIONS = "listeOptions";
    public static final String ATT_LISTE_OPTIONS_I2 = "listeOptionsI2";
    public static final String ATT_LISTE_OPTIONS_SUJETS = "listeOptionsSujets";
    public static final String ATT_LISTE_OPTIONS_ESEO = "listeOptionsESEO";
    public static final String ATT_LISTE_ROLE_UTILISATEUR = "listeRoleUtilisateur";
    public static final String ATT_LISTE_ETATS = "listeEtats";
    public static final String ATT_LISTE_PROFESSEURS_ATTIBUES_A_UN_SUJET = "listeProfesseursAttribuesAUnSujet";
    public static final String ATT_LISTE_PROFESSEURS = "listeProfesseurs";
    public static final String ATT_LISTE_REFERENTS = "referents";
    public static final String ATT_FONCTION_UTILISATEUR_SESSION = "fonctionsUtilisateurSession";
    public static final String ATT_LISTE_UTILISATEURS_REFERENTS = "referentsUtilisateurs";
    public static final String ATT_LISTE_SOUTENANCES = "listeSoutenances";
    public static final String ATT_LISTE_UTILISATEURS = "listeUtilisateurs";
    public static final String ATT_COMMENTAIRES = "commentaire";
    public static final String ATT_COMMENTAIRES_SECONDAIRES = "commentairesSecondaires";
    public static final String ATT_NOMS_PORTEURS_SUJETS = "nomsPorteursSujets";
    public static final String ATT_NOMS_CREATEUR_MEETING = "nomCreateurMeeting";
    public static final String ATT_NOMS_EXPEDITEURS_COMMENTAIRES = "nomsExpediteursCommentaires";
    public static final String ATT_MON_SUJET = "monSujet";
    public static final String ATT_MES_SUJETS = "mesSujets";
    public static final String ATT_MON_EQUIPE = "monEquipe";
    public static final String ATT_SUJET_SUIVI = "sujetSuivi";
    public static final String ATT_SUJET_A_PUBLIER = "sujetPublication";
    public static final String ATT_SUJET_INTERRESSANT = "sujetInterressant";
    public static final String ATT_SUJET_A_VALIDER = "sujetValidation";
    public static final String ATT_EQUIPE_A_VALIDER = "equipeValider";
    public static final String ATT_PROF_A_VALIDER = "professeurValider";
    public static final String ATT_COMPTE_A_VALIDER = "compteValider";
    public static final String ATT_PROCHAINE_SOUTENANCE = "prochaineSoutenance";
    public static final String ATT_DEADLINE_POSTER = "deadlinePoster";
    public static final String ATT_ACTION_GERER_SUJET = "actionValiderRefuser";
    public static final String ATT_ACTION_REFUSER_SUJET = "refuser";
    public static final String ATT_ANNEES_PLANIFIEES = "anneesPlanifiees";
    public static final String ATT_ANNEES_PGL_PLANIFIEES = "anneesPGLPlanifiees";
    public static final String ATT_OPTION_ANNEES = "optionAnnees";
    public static final String ATT_DISPO_NOTES_INTERMEDIAIRE = "dispoNotesIntermediaire";
    public static final String ATT_DISPO_NOTES_PROJET = "dispoNotesProjet";
    public static final String ATT_DISPO_NOTES_POSTER = "dispoNotesPoster";
    public static final String ATT_DISPO_NOTES_SOUTENANCE = "dispoNotesSoutenance";
    public static final String ATT_TOUTES_NOTES_INTERMEDIAIRE = "toutesNotesIntermediaire";
    public static final String ATT_TOUTES_NOTES_PROJET = "toutesNotesProjet";
    public static final String ATT_TOUTES_NOTES_POSTER = "toutesNotesPoster";
    public static final String ATT_TOUTES_NOTES_SOUTENANCE = "toutesNotesSoutenance";
    public static final String ATT_SUJETS_REFERENT = "sujetsReferent";
    public static final String ATT_SUJETS_POSTER = "sujetsPoster";
    public static final String ATT_SUJETS_SOUTENANCE = "sujetsSoutenance";
    public static final String ATT_MANQUE_NOTE_SUIVI = "manqueNoteSuivi";
    public static final String ATT_MANQUE_NOTE_POSTER = "manqueNotePoster";
    public static final String ATT_MANQUE_NOTE_SOUTENANCE = "manqueNoteSoutenance";
    public static final String ATT_DATE_POSTER = "datePoster";
    public static final String ATT_DATE_SOUTENANCE = "dateSoutenance";
    public static final String ATT_LISTE_MEETING = "listeMeeting";
    public static final String ATT_LISTE_POSTERS_VALIDES = "listePostersValides";
    public static final String ATT_NOMS_INVITES_MEETINGS = "nomsInvitesMeetings";
    public static final String ATT_OPTION_LD = "optionLD";
    public static final String ATT_LISTE_SPRINTS = "listeSprints";
    public static final String ATT_EQUIPES_I2_ETU = "listeEtuDisp";
    public static final String ATT_EQUIPES_I2_LISTE_EQUIPE = "listeEquipe";
    public static final String ATT_EQUIPES_I2_LISTE_USERS = "listeUtilisateur";
    public static final String ATT_EQUIPES_I2_LISTE_ANNEE_SCOLAIRE = "listeAnneeScolaire";
    public static final String ATT_EQUIPES_I2_LISTE_PROJET = "listeProjetEquipe";
    public static final String ATT_EQUIPES_I2_LISTE_SPRINT = "listeSprintDAO";
    public static final String ATT_EQUIPES_I2_LISTE_ROLES = "listeRolesEquipe";
    public static final String ATT_EQUIPES_I2_LISTE_PROF = "listeProf";
    public static final String ATT_EQUIPES_I2_LISTE_COMPO_EQUIPE = "listeCompositionEquipe";
    public static final String ATT_EQUIPES_I2_LISTE_SPRINT_ACTUEL = "sprintActuel";
    public static final String ATT_LISTE_MATIERES = "listeMatieres";

    public static final String ROLE_PROF_RESPONSABLE = "profResponsable";

    public static final String CHAMP_NOM = "nom";
    public static final String CHAMP_PRENOM = "prenom";
    public static final String CHAMP_IDENTIFIANT = "identifiant";
    public static final String CHAMP_ANCIEN_MOT_DE_PASSE = "ancienMotDePasse";
    public static final String CHAMP_MOT_DE_PASSE = "motDePasse";
    public static final String CHAMP_NOUVEAU_MOT_DE_PASSE = "nouveauMDP2";
    public static final String CHAMP_EMAIL = "email";
    public static final String CHAMP_TITRE = "titre";
    public static final String CHAMP_DESCRIPTION = "description";
    public static final String CHAMP_AUTRES = "autres";
    public static final String CHAMP_LIENS = "liens";
    public static final String CHAMP_ID_UTILISATEUR = "idUtilisateur";
    public static final String CHAMP_ID_SUJET = "idSujet";
    public static final String CHAMP_ID_OBSERVATION = "idObservation";
    public static final String CHAMP_ID_EQUIPE = "idEquipe";
    public static final String CHAMP_CONTENU = "contenu";
    public static final String CHAMP_TITRE_SUJET = "nomSujet";
    public static final String CHAMP_NBR_MIN_ELEVES = "nbrMinEleves";
    public static final String CHAMP_NBR_MAX_ELEVES = "nbrMaxEleves";
    public static final String CHAMP_CONTRAT_PRO = "contratPro";
    public static final String CHAMP_CONFIDENTIALITE = "confidentialite";
    public static final String CHAMP_OPTIONS = OPTIONS;
    public static final String CHAMP_ETAT_SUJET = "etatSujet";
    public static final String CHAMP_SUJET = "sujet";
    public static final String CHAMP_TAILLE_EQUIPE = "tailleEquipe";
    public static final String CHAMP_MEMBRE = "membre";
    public static final String CHAMP_FONCTION_PROF = "fonction";
    public static final String CHAMP_ID_MEMBRE = "idMembre";
    public static final String CHAMP_ID_MEMBRE_1 = "idMembre1";
    public static final String CHAMP_ANNEE_DEBUT = "anneeDebut";
    public static final String CHAMP_ANNEE_PGL_DEBUT = "anneePGLDebut";
    public static final String CHAMP_ANNEE_FIN = "anneeFin";
    public static final String CHAMP_ANNEE_PGL_FIN = "anneePGLFin";
    public static final String CHAMP_DATE_DEBUT_PROJET = "dateDebutProjet";
    public static final String CHAMP_DATE_MI_AVANCEMENT = "dateMiAvancement";
    public static final String CHAMP_DATE_DEPOT_POSTER = "dateDepotPoster";
    public static final String CHAMP_DATE_SOUTENANCE_FINALE = "dateSoutenanceFinale";
    public static final String CHAMP_NOTES_INTERMEDIAIRE = "notesIntermediaire";
    public static final String CHAMP_NOTES_POSTER = "notesPoster";
    public static final String CHAMP_NOTES_PROJET = "notesProjet";
    public static final String CHAMP_NOTES_SOUTENANCE = "notesSoutenance";
    public static final String CHAMP_FORMULAIRE = "formulaire";
    public static final String CHAMP_ENDROIT = "lieu";
    public static final String CHAMP_DATE_DEBUT_MEETING = "dateDebut";
    public static final String CHAMP_HEURE_RENDU_MEETING = "heureDebut";
    public static final String CHAMP_COMPTE_RENDU_MEETING = "compteRendu";
    public static final String CHAMP_DATE_HEURE_MEETING = "dateHeure";
    public static final String CHAMP_ID_MEETING = "idReunion";
    public static final String CHAMP_ID_CREATEUR_MEETING = "idCreateurMeeting";
    public static final String CHAMP_NBR_SPRINT = "nombreSprint";
    public static final String CHAMP_ID_SPRINT = "idSprintSelect";
    public static final String CHAMP_DATE_DEBUT_SPRINT = "idDateDebut";
    public static final String CHAMP_DATE_FIN_SPRINT = "idDateFin";
    public static final String CHAMP_COEFFICIENT_SPRINT = "idCoefficient";
    public static final String CHAMP_NOMBRE_HEURES_SPRINT = "idNombreHeures";
    public static final String CHAMP_REF_ANNEE_SPRINT = "refIdAnnee";
    public static final String CHAMP_NUMERO_SPRINT = "sprint";
    public static final String CHAMP_ERREUR_DATE_DEBUT_SPRINT = "dateDebutErreur";
    public static final String CHAMP_ERREUR_DATE_FIN_SPRINT = "dateFinErreur";
    public static final String CHAMP_ERREUR_NBR_HEURES_SPRINT = "nbrHErreur";
    public static final String CHAMP_ERREUR_NBR_COEFFICIENT_SPRINT = "coeffErreur";
    public static final String CHAMP_NOM_EQUIPE = "nomEquipe";
    public static final String CHAMP_PROJET_EQUIPE = "projetEquipe";
    public static final String CHAMP_LISTE_EQUIPE = "equipeEquipe";
    public static final String CHAMP_ANNEE_SCOLAIRE_EQUIPE = "anneeScolaireEquipe";
    public static final String CHAMP_ID_EQUIPE_PGL_DELETE = "idEquipeDelete";

    public static final String ATT_LISTE_SOUTENANCES_JURY_MANQUANT = "listeSoutenancesJuryManquant";
    public static final String ATT_LISTE_SUJETS_JURY_MANQUANT = "listeSujetsJuryManquant";
    public static final String CHAMP_ID_PROFESSEUR = "idProf";
    public static final String CHAMP_ID_SOUTENANCE = "idSoutenance";
    public static final String CHAMP_COMMENTAIRE = ATT_COMMENTAIRES;
    public static final String ATT_LISTE_SUJETS_CHOISIS = "listeSujetsChoisis";
    public static final String ATT_LISTE_SUJETS_CHOISIS_STRING = "listeSujetsChoisisString";
    public static final String ATT_LISTE_SOUTENANCES_CHOISIES = "listeSoutenancesChoisies";
    public static final String ATT_LISTE_EQUIPES_CHOISIES = "listeEquipesChoisies";
    public static final String ATT_LISTE_JURYS_CHOISIS = "listeJurysChoisis";
    public static final String ATT_LISTE_PROFESSEURS_CHOISIS = "listeProfesseursChoisis";
    public static final String ATT_LISTE_PROFESSEURS_CHOISIS_STRING = "listeProfesseursChoisisString";
    public static final String ATT_LISTE_PROFESSEURS_CHOISIS1 = "listeProfesseursChoisis1";
    public static final String ATT_LISTE_PROFESSEURS_CHOISIS2 = "listeProfesseursChoisis2";

    protected static final String CHAMP_ETAT_VALIDE = "etatValide";
    protected static final String CHAMP_ETAT_REFUSE = "etatRefuse";
    protected static final String CHAMP_ETAT_ATTIBUE = "etatAttribue";
    protected static final String CHAMP_ETAT_PUBLIE = "etatPublie";
    protected static final String CHAMP_ETAT_DEPOSE = "etatDepose";

    public static final String HEADER = "referer";
    public static final String VUE_CREER_EQUIPE = "/CreerEquipe";
    public static final String VUE_DASHBOARD = "/Dashboard";
    public static final String VUE_INDEX = "/index.jsp";
    public static final String VUE_INSCRIPTION = "/inscriptionProfSujet.jsp";


    /**
     * Constructeur caché par défaut (car c'est une classe finale utilitaire,
     * contenant uniquement des méthodes appelées de manière statique).
     */
    private ServletUtilitaire() {

    }

    /**
     * Récupère les informations choisies par l'utilisateur sous forme de String.
     *
     * @param request    la requête possédant les paramètres à récupérer.
     * @param listeInfos la liste de toutes les informations possibles.
     * @return infosChoisies les informations choisies par l'utilisateur sous forme
     * de String.
     */
    protected static String getInfosChoisies(HttpServletRequest request, String[] listeInfos) {
        StringBuilder infosChoisies = new StringBuilder();
        for (String choix : listeInfos) {
            if (request.getParameter(choix) != null) {
                infosChoisies.append(choix).append(";");
            }
        }
        infosChoisies.append("Autres;");
        infosChoisies.append(request.getParameter(CHAMP_AUTRES));
        return infosChoisies.toString();
    }

    /**
     * Change le format de la liste des options et des etats dans la requête pour
     * pouvoir appliquer des filtres de sélection dans les JSP.
     *
     * @param request     la requête possédant les paramètres à récupérer.
     * @param listeSujets la liste des sujets que l'on souhaite traiter.
     */
    protected static void setOptionsEtats(HttpServletRequest request, List<Sujet> listeSujets) {
        List<String> listeOptions = new ArrayList<>();
        List<EtatSujet> listeEtats = new ArrayList<>();

        request.setAttribute(ATT_LISTE_SUJETS, listeSujets);
        request.setAttribute(ATT_LISTE_OPTIONS, listeOptions);
        request.setAttribute(ATT_LISTE_ETATS, listeEtats);
    }

    protected static Utilisateur rechercheUtilisateurAutoCompletion(String professeurRecherche,
                                                                    UtilisateurDAO utilisateurDAO) {
        // Le nom du professeur qui est renvoyé est sous la forme <NOM PRENOM -
        // identifiant>
        // En splitant cette chaine de caractère on peut récuperer chacun des attributs
        String[] listeAttributs = professeurRecherche.split(" ");
        // Cependant si cette chaine de caractère splité est inférieur à 4 ou que le
        // champ du professeurRecherche est vide on génère un message
        if (professeurRecherche.length() == 0 || listeAttributs.length < 4) {
            return null;
        }

        // Sinon on peut lancer la recherche du professeur :
        Utilisateur utilisateur = new Utilisateur();
        utilisateur.setIdentifiant(listeAttributs[3].toLowerCase());
        List<Utilisateur> utilisateurs = utilisateurDAO.trouver(utilisateur);
        // Si le professeur n'exsite pas, on génère un message
        if ((utilisateurs.isEmpty()) || (utilisateurs.size() > 1)) {
            return null;
        } else {
            return utilisateurs.get(0);
        }
    }

    protected static boolean testeRoleResponsable(List<Role> rolesUtilisateur) {
        boolean result = false;
        for (Role role : rolesUtilisateur) {
            if (role.getNomRole().equals(ROLE_PROF_RESPONSABLE)) {
                result = true;
            }
        }
        return result;
    }

    /**
     * Permet d'avoir le nom du fichier passer en paramètre d'une requete doPost
     *
     * @param part, l'objet de type Part contenant le fichier
     * @return le nom du fichier
     */
    protected static String extractFileName(Part part) {
        String contentDisp = part.getHeader("content-disposition");
        String[] items = contentDisp.split(";");
        for (String s : items) {
            if (s.trim().startsWith("filename")) {
                return s.substring(s.indexOf('=') + 2, s.length() - 1);
            }
        }
        return "";
    }

    /**
     * Retourne extension d'un fichier
     *
     * @param nomFichier nom du fichier
     * @return l'extension
     */
    protected static String avoirExtension(String nomFichier) {
        int i = nomFichier.lastIndexOf('.');
        if (i > 0 && i < nomFichier.length() - 1) {
            return nomFichier.substring(i + 1).toLowerCase();
        } else {
            return null;
        }
    }

    /**
     * Fait la correspondance (le mapping) entre les paramètres récupérés de la
     * requête et un bean Utilisateur.
     *
     * @param request     la requête possédant les paramètres à récupérer.
     * @param identifiant l'identifiant déjà récupéré.
     * @param email       l'email déjà récupéré.
     * @param valide      le String indiquant si l'utilisateur est validé ou non.
     * @return utilisateur le bean dont on souhaite faire la correspondance.
     */
    protected static Utilisateur mapInscription(HttpServletRequest request, String identifiant, String email,
                                                String valide) {
        /* Récupération des données saisies */
        String nom = request.getParameter(CHAMP_NOM);
        String prenom = request.getParameter(CHAMP_PRENOM);
        String motDePasse = request.getParameter(CHAMP_MOT_DE_PASSE);

        /* Hashage du mot de passe */
        String hash = BCrypt.hashpw(motDePasse, BCrypt.gensalt());

        /* Insertion des données récupérées dans un bean */
        Utilisateur utilisateur = new Utilisateur();
        utilisateur.setNom(nom);
        utilisateur.setPrenom(prenom);
        utilisateur.setIdentifiant(identifiant);
        utilisateur.setHash(hash);
        utilisateur.setEmail(email);
        utilisateur.setValide(valide);
        return utilisateur;
    }

    /**
     * Valide les champs d'un formulaire d'inscription.
     *
     * @param utilisateurDAO le DAO Utilisateur.
     * @param identifiant    l'identifiant saisi par l'utilisateur que l'on souhaite valider.
     * @param email          l'email saisi par l'utilisateur que l'on souhaite valider.
     * @return erreurs les erreurs de validation.
     */
    protected static Map<String, String> validerChampsInscription(UtilisateurDAO utilisateurDAO, String identifiant,
                                                                  String email) {
        /*
         * Utilisation d'UtilisateurDAO pour chercher un utilisateur via son identifiant
         */
        Utilisateur utilisateurIdentifiant = new Utilisateur();
        utilisateurIdentifiant.setIdentifiant(identifiant);
        List<Utilisateur> utilisateursIdentifiant = utilisateurDAO.trouver(utilisateurIdentifiant);

        /* Utilisation d'UtilisateurDAO pour chercher un utilisateur via son email */
        Utilisateur utilisateurEmail = new Utilisateur();
        utilisateurEmail.setEmail(email);
        List<Utilisateur> utilisateursEmail = utilisateurDAO.trouver(utilisateurEmail);

        /* Validation des champs */
        Map<String, String> erreurs = new HashMap<>();
        if (!utilisateursIdentifiant.isEmpty()) { // si l'identifiant est déjà utilisé
            erreurs.put(ServletUtilitaire.CHAMP_IDENTIFIANT, "Identifiant déjà utilisé. ");
        }
        if (!utilisateursEmail.isEmpty()) { // si l'email est déjà utilisé
            erreurs.put(ServletUtilitaire.CHAMP_EMAIL, "Email déjà utilisé. ");
        }

        return erreurs;
    }

    /**
     * Notifie une liste d'utilisateurs avec le message rentré en paramètre
     *
     * @param listeUtilisateur la liste d'Utilisateur
     * @param message          message
     * @param destination,     le servlet de destination
     */
    protected static void notifierUtilisateur(List<Utilisateur> listeUtilisateur, String message, String destination,
                                              NotificationDAO notificationDAO) {
        for (Utilisateur utilisateur : listeUtilisateur) {
            notificationDAO.ajouterNotification(utilisateur.getIdUtilisateur(), message, destination);
        }
    }

    /**
     * Remplie deux maps : mapIdProfesseur(prof.id,prof) et mapOptionsProfesseur(prof.id, String de toutes les options du prof séparées par un espace)
     *
     * @param mapOptionsProfesseur
     * @param mapIdProfesseur
     * @param allOptions
     * @param listeProfesseurs
     * @param optionESEODAO
     */
    protected static void trouverProfesseur(Map<Long, String> mapOptionsProfesseur,
                                            Map<Long, Utilisateur> mapIdProfesseur, List<String> allOptions, List<Utilisateur> listeProfesseurs,
                                            OptionESEODAO optionESEODAO) {
        for (Utilisateur professeur : listeProfesseurs) {
            List<OptionESEO> optionsProfesseur = optionESEODAO.trouverOptionUtilisateur(professeur);
            StringBuilder optionsDuProf = new StringBuilder();
            for (OptionESEO option : optionsProfesseur) {
                optionsDuProf.append(option.getNomOption()).append(" ");
                if (!allOptions.contains(option.getNomOption())) {
                    allOptions.add(option.getNomOption());
                }
            }
            mapIdProfesseur.put(professeur.getIdUtilisateur(), professeur);
            mapOptionsProfesseur.put(professeur.getIdUtilisateur(), optionsDuProf.toString());
        }
    }

    /**
     * Charge dans une SimpleEntry tous les utilisateurs de l'équipe d'un sujet en
     * paramètre. L'objet renvoyé est sous la forme : <idSujet,List<Utilisateur>>
     *
     * @param sujet dont on veut trouver les membres de l'équipe
     * @return le SimpleEntry
     */
    protected static SimpleEntry<Long, List<Utilisateur>> trouverUtilisateurEquipe(Sujet sujet, EquipeDAO equipeDAO,
                                                                                   UtilisateurDAO utilisateurDAO) {
        List<Equipe> listeEquipe = equipeDAO.listerEquipeSujet(sujet.getIdSujet());
        List<Utilisateur> utilisateursEquipe = new ArrayList<>();
        // Si la fonction renvoie une liste vide alors l'équipe n'est pas créée.
        if (!listeEquipe.isEmpty()) {

            /* Sinon on peut récupérer la liste des étudiants de l'équipe */

            Equipe equipe = listeEquipe.get(0);
            EtudiantEquipe etudiantEquipe = new EtudiantEquipe();
            etudiantEquipe.setIdEquipe(equipe.getIdEquipe());

            List<EtudiantEquipe> listeEtudiantEquipe = equipeDAO.trouverEtudiantEquipe(etudiantEquipe);

            /* On récupère chaque EtudiantEquipe pour les avoir en utilisateur */
            for (EtudiantEquipe etudiant : listeEtudiantEquipe) {
                Utilisateur utilisateur = new Utilisateur();
                utilisateur.setIdUtilisateur(etudiant.getIdEtudiant());
                utilisateur = utilisateurDAO.trouver(utilisateur).get(0);
                utilisateur.setNom(utilisateur.getNom().toUpperCase());
                utilisateursEquipe.add(utilisateur);
            }

            return new SimpleEntry<>(sujet.getIdSujet(), utilisateursEquipe);
        } else {
            return new SimpleEntry<>(sujet.getIdSujet(), null);
        }
    }

    /**
     * Charge dans une SimpleEntry tous les professeurs de l'équipe d'un sujet en
     * paramètre. L'objet renvoyé est sous la forme : <idSujet,List<String>>
     *
     * @param sujet dont on veut trouver les professeurs interessés
     * @return le SimpleEntry
     */
    protected static SimpleEntry<Long, List<String>> trouverProfesseurConcerne(Sujet sujet,
                                                                               UtilisateurDAO utilisateurDAO) {

        // On recupère les professeurs concernés par le sujet
        List<ProfesseurSujet> listeProfesseurConcerne = utilisateurDAO.trouverProfesseursAttribuesAUnSujet(sujet);

        List<String> listeString = new ArrayList<>();

        /*
         * On cherche les utilisateurs Professeur concernés par le sujet, ayant comme
         * attribution, une attribution valide
         */
        for (ProfesseurSujet profSujet : listeProfesseurConcerne) {
            if ("oui".equals(profSujet.getValide())) {
                Utilisateur utilisateurProf = new Utilisateur();
                utilisateurProf.setIdUtilisateur(profSujet.getIdProfesseur());
                utilisateurProf = utilisateurDAO.trouver(utilisateurProf).get(0);
                listeString.add(utilisateurProf.getNom().toUpperCase() + " " + utilisateurProf.getPrenom() + " : "
                        + profSujet.getFonction());
            }
        }

        return new SimpleEntry<>(sujet.getIdSujet(), listeString);
    }

    /**
     * Charge dans une SimpleEntry le poster d'un sujet en paramètre. L'objet
     * renvoyé est sous la forme : <idSujet,Poster>
     *
     * @param sujet dont on veut trouver le poster
     * @return le SimpleEntry
     */
    protected static SimpleEntry<Long, Poster> trouverPoster(Sujet sujet, PosterDAO posterDAO) {
        Poster poster = posterDAO.trouverPoster(sujet.getIdSujet());
        return new SimpleEntry<>(sujet.getIdSujet(), poster);
    }

    /**
     * Charge dans une SimpleEntry une liste de String contenant toutes informations
     * pour les évaluations du sujet
     *
     * @param sujet dont on veut trouver le poster
     * @return le SimpleEntry
     */
    protected static SimpleEntry<Long, List<String>> trouverInformationsJurySoutenance(Sujet sujet,
                                                                                       SoutenanceDAO soutenanceDAO, JurySoutenanceDAO jurySoutenanceDAO, UtilisateurDAO utilisateurDAO) {

        List<String> listeString = new ArrayList<>();

        // On récupère la soutenance si elle existe
        Soutenance soutenance = soutenanceDAO.trouverSoutenanceSujet(sujet);

        String attribut1 = "";
        String attribut2 = "";
        String attribut3 = "";

        // Si la soutenance est défini, on met dans la variable message toutes les
        // informations nécessaires
        if (soutenance.getDateSoutenance() != null) {
            attribut1 = soutenance.getDateSoutenance().toString();
            if (soutenance.getIdJurySoutenance() != null) {
                JurySoutenance jury = new JurySoutenance();
                jury.setIdJurySoutenance(soutenance.getIdJurySoutenance());
                jury = jurySoutenanceDAO.trouver(jury).get(0);

                Utilisateur user1 = trouverUtilisateurAssocieID(jury.getIdProf1(), utilisateurDAO);
                if (user1 != null) {
                    attribut2 = user1.getNom().toUpperCase() + " " + user1.getPrenom();
                }

                Utilisateur user2 = trouverUtilisateurAssocieID(jury.getIdProf2(), utilisateurDAO);
                if (user2 != null) {
                    attribut3 = user2.getNom().toUpperCase() + " " + user2.getPrenom();
                }
            }
        }

        listeString.add(attribut1);
        listeString.add(attribut2);
        listeString.add(attribut3);

        return new SimpleEntry<>(sujet.getIdSujet(), listeString);
    }

    /**
     * Charge dans une SimpleEntry une liste de String contenant toutes informations
     * pour les évaluations du sujet
     *
     * @param sujet  dont on veut trouver le poster
     * @param poster contenant les informations sur l'équipe
     * @return le SimpleEntry
     */
    protected static SimpleEntry<Long, List<String>> trouverInformationsJuryPoster(Poster poster, Sujet sujet,
                                                                                   JuryPosterDAO juryPosterDAO, UtilisateurDAO utilisateurDAO) {
        if (poster != null) {
            List<String> listeString = new ArrayList<>();

            JuryPoster jury = new JuryPoster();
            jury.setIdEquipe(poster.getIdEquipe());
            List<JuryPoster> listeJuryPoster = juryPosterDAO.trouver(jury);

            String attribut1 = "";
            String attribut2 = "";
            String attribut3 = "";

            // Si le jury existe, on complete les valeurs de attributs
            if (listeJuryPoster.size() == 1) {
                Utilisateur user1 = trouverUtilisateurAssocieID(listeJuryPoster.get(0).getIdProf1(), utilisateurDAO);
                if (user1 != null) {
                    attribut1 = user1.getNom().toUpperCase() + " " + user1.getPrenom();
                }

                Utilisateur user2 = trouverUtilisateurAssocieID(listeJuryPoster.get(0).getIdProf2(), utilisateurDAO);
                if (user2 != null) {
                    attribut2 = user2.getNom().toUpperCase() + " " + user2.getPrenom();
                }

                Utilisateur user3 = trouverUtilisateurAssocieID(listeJuryPoster.get(0).getIdProf3(), utilisateurDAO);
                if (user3 != null) {
                    attribut3 = user3.getNom().toUpperCase() + " " + user3.getPrenom();
                }

            }

            listeString.add(attribut1);
            listeString.add(attribut2);
            listeString.add(attribut3);

            return new SimpleEntry<>(sujet.getIdSujet(), listeString);

        } else {
            return new SimpleEntry<>(sujet.getIdSujet(), null);
        }
    }

    /**
     * Charge dans une SimpleEntry une liste de String contenant toutes informations
     * concernant les dates de l'année scolaire
     *
     * @param sujet dont on veut trouver le poster
     * @return le SimpleEntry
     */
    protected static SimpleEntry<Long, List<AnneeScolaire>> trouverInformationsAnneeScolaire(Sujet sujet,
                                                                                             AnneeScolaireDAO anneeScolaireDAO, OptionESEODAO optionESEODAO) {
        List<AnneeScolaire> listeARetourner = new ArrayList<>();
        List<AnneeScolaire> listeAnneeScolaire = anneeScolaireDAO.lister();
        List<OptionESEO> listeOptionsSujet = optionESEODAO.trouverOptionSujet(sujet);

        for (OptionESEO optionSujet : listeOptionsSujet) {

            AnneeScolaire anneeTrouve = new AnneeScolaire();

            for (AnneeScolaire annee : listeAnneeScolaire) {
                if (annee.getIdOption().equals(optionSujet.getIdOption())) {
                    anneeTrouve = annee;
                    break;
                }
            }
            listeARetourner.add(anneeTrouve);
        }

        return new SimpleEntry<>(sujet.getIdSujet(), listeARetourner);
    }

    /**
     * Renvoie le bean complet de l'utilisateur
     *
     * @param idUtilisateur  idUtilisateur
     * @param utilisateurDAO dao
     * @return le bean de l'utilisateur
     */
    protected static Utilisateur trouverUtilisateurAssocieID(Long idUtilisateur, UtilisateurDAO utilisateurDAO) {
        Utilisateur user = new Utilisateur();
        user.setIdUtilisateur(idUtilisateur);
        List<Utilisateur> listeUtilisateur = utilisateurDAO.trouver(user);
        if (listeUtilisateur.isEmpty()) {
            return null;
        } else {
            return listeUtilisateur.get(0);
        }
    }

    /**
     * Méthode vérifiant que le fichier rentré correspond bien à un csv
     *
     * @param nomDuFichier nomDuFichier
     */
    protected static boolean verificationFichier(String nomDuFichier) {
        return ("csv".equals(ServletUtilitaire.avoirExtension(nomDuFichier)));
    }

    /**
     * Converit un InputStream en un String
     *
     * @param is, l'InputStram
     * @return la chaine de caractère
     */
    protected static String convertirStreamEnString(java.io.InputStream is) {
        java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
        return s.hasNext() ? s.next() : "";
    }

}