package fr.eseo.ld.servlets;

import fr.eseo.ld.beans.*;
import fr.eseo.ld.dao.*;
import fr.eseo.ld.pgl.beans.Matiere;
import fr.eseo.ld.pgl.beans.Sprint;
import fr.eseo.ld.utils.CallbackUtilitaire;
import fr.eseo.ld.utils.ParamUtilitaire;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Servlet permettant l'affichage des sprints.
 */
@WebServlet("/GestionProjetsEtPFE")
public class GestionProjetsEtPFE extends HttpServlet {

    private static final long serialVersionUID = 1L;
    private static final String ATT_FORMULAIRE = "formulaire";
    private static final String VUE_FORM = "/WEB-INF/formulaires/gestionProjetsEtPFE.jsp";
    private static final String ATT_ONGLET_ACTIF = "onglet";
    private static final String NON_VALIDE = "Non valide";
    private static final String NON_VIDE = "Ne peut être vide";
    private static final String FORMAT_DATE = "\\d{4}-\\d{2}-\\d{2}";
    private static final String FORMAT_NUM = "[0-9]+";
    private static final String FORMAT_NUM_DOT = "[0-9.]+";
    private static final String CONFIGURER_SPRINT = "configurerSprints";
    private static Logger logger = Logger.getLogger(GestionProjetsEtPFE.class.getName());
    private SprintDAO sprintDAO;
    private MatiereDAO matiereDAO;
    private AnneeScolaireDAO anneeScolaireDAO;
    private OptionESEODAO optionESEODAO;
    private int anneeDebut;
    private int anneeFin;
    private Date dateDebutProjet;
    private Date dateMiAvancement;
    private Date dateDepotPoster;
    private Date dateSoutenanceFinale;



    /*Comprend la gestion annee scolaire*/

    @Override
    public void init() throws ServletException {
        /* Récupération d'instances de DAO */
        DAOFactory daoFactory = (DAOFactory) this.getServletContext().getAttribute(ServletUtilitaire.CONF_DAO_FACTORY);
        this.anneeScolaireDAO = daoFactory.getAnneeScolaireDAO();
        this.optionESEODAO = daoFactory.getOptionESEODAO();
        this.sprintDAO = daoFactory.getSprintDAO();
        this.matiereDAO = daoFactory.getMatiereDAO();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        /* Récupération des années scolaires et des options */
        recuperationAnneesScolairesEtOptions(request);
        recuperationAnneesLD(request);
        listeSprints(request);
        listeMatieres(request);
        this.getServletContext().getRequestDispatcher(VUE_FORM).forward(request, response);
    }


    /**
     * Methode permettant de recuperer l'année scolaire de l'option LD.
     */
    private void recuperationAnneesLD(HttpServletRequest request) {
        /* Récupération des années */
        OptionESEO optionESEO = new OptionESEO();
        optionESEO.setNomOption("LD");
        OptionESEO optionLD = this.optionESEODAO.trouver(optionESEO).get(0);
        Long idOptionLD = optionLD.getIdOption();

        AnneeScolaire anneeLD = new AnneeScolaire();
        anneeLD.setIdOption(idOptionLD);
        List<AnneeScolaire> annees = this.anneeScolaireDAO.trouver(anneeLD);
        request.setAttribute(ServletUtilitaire.ATT_OPTION_LD, optionESEO.getNomOption());
        request.setAttribute(ServletUtilitaire.ATT_ANNEES_PGL_PLANIFIEES, annees);
        request.setAttribute("refAnneeLdEnCours", annees.size());
    }

    /**
     * Methode permettant de recuperer toutes les années scolaires des différentes options.
     */
    private void recuperationAnneesScolairesEtOptions(HttpServletRequest request) {
        /* Récupération des années scolaires et des options */
        List<AnneeScolaire> anneesScolaires = this.anneeScolaireDAO.lister();
        List<OptionESEO> options = this.optionESEODAO.lister();
        String[] optionAnnees = new String[anneesScolaires.size()];
        int compteur = 0;
        for (AnneeScolaire annee : anneesScolaires) {
            for (OptionESEO option : options) {
                if (option.getIdOption().compareTo(annee.getIdOption()) == 0) {
                    optionAnnees[compteur] = option.getNomOption();
                    compteur++;
                }
            }
        }
        request.setAttribute(ServletUtilitaire.ATT_ANNEES_PLANIFIEES, anneesScolaires);
        request.setAttribute(ServletUtilitaire.ATT_OPTION_ANNEES, optionAnnees);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        /* Recupere les valeurs des champs et création de l'année */
        if ("ajouterAnneeScolaire".equalsIgnoreCase(request.getParameter(ATT_FORMULAIRE))) {
            planificationAnnee(request);
        }
        if ("selectNbrSprint".equalsIgnoreCase(request.getParameter(ATT_FORMULAIRE))) {
            selectNbrSprint(request);
            request.setAttribute(ATT_ONGLET_ACTIF, CONFIGURER_SPRINT);
        }
        if ("modifierSprint".equalsIgnoreCase(request.getParameter(ATT_FORMULAIRE))) {
            modificationSprint(request);
            request.setAttribute(ATT_ONGLET_ACTIF, CONFIGURER_SPRINT);
        }
        if("modifierCoefficientIntra".equalsIgnoreCase(request.getParameter(ATT_FORMULAIRE))) {
        	modificationCoeffIntra(request);
        	ajoutMatiere(request);
        	request.setAttribute(ATT_ONGLET_ACTIF, CONFIGURER_SPRINT);
        }

    	
        try {
            /* Ré-affichage du formulaire */
            doGet(request, response);
        } catch (Exception e) {
            logger.log(Level.WARN, "Échec de la mise à jour de l'objet, aucune ligne modifiée dans la table.", e);
        }
    }

    /**
     * Methode permettant de planifier des années.
     */
    private void planificationAnnee(HttpServletRequest request) {
        /* Création de la liste des erreurs */
        Map<String, String> erreurs = new HashMap<>();
        Callback callback;

        /* Validation des données saisies */
        this.validation(request, erreurs);

        // vérification de la cohérence de l'année de début et de fin
        if (this.anneeFin - this.anneeDebut != 1) {
            erreurs.put(ServletUtilitaire.CHAMP_ANNEE_FIN, NON_VALIDE);
            erreurs.put(ServletUtilitaire.CHAMP_ANNEE_DEBUT, NON_VALIDE);
        }

        // vérification de la sélection d'au moins une option
        List<OptionESEO> options = this.optionESEODAO.lister();
        List<OptionESEO> optionsSujet = new ArrayList<>();
        for (OptionESEO option : options) {
            if (request.getParameter(option.getNomOption()) != null) {
                optionsSujet.add(option);        
            }
        }
        if (optionsSujet.isEmpty()) { // si aucune option n'a été cochée
            erreurs.put(ServletUtilitaire.CHAMP_OPTIONS, "Aucune option sélectionnée. ");
        }

        /* Vérification de la cohérence des dates */
        if (erreurs.isEmpty()) {
            this.coherenceDates(erreurs);
        }

        List<AnneeScolaire> annees = this.anneeScolaireDAO.lister();

        /*
         * Si aucune erreur de validation n'a eu lieu, alors insertion de l'année dans
         * la BDD, sinon ré-affichage du formulaire.
         */
        if (erreurs.isEmpty()) {
            for (OptionESEO option : optionsSujet) {
                AnneeScolaire anneeScolaire = new AnneeScolaire();
                anneeScolaire.setAnneeDebut(this.anneeDebut);
                anneeScolaire.setAnneeFin(this.anneeFin);
                anneeScolaire.setDateDebutProjet(this.dateDebutProjet);
                anneeScolaire.setDateMiAvancement(this.dateMiAvancement);
                anneeScolaire.setDateDepotPoster(this.dateDepotPoster);
                anneeScolaire.setDateSoutenanceFinale(this.dateSoutenanceFinale);
                anneeScolaire.setIdOption(option.getIdOption());
                this.ajoutModifAnneeScolaire(anneeScolaire, option, annees);
                callback = new Callback(CallbackType.SUCCESS,
                        "Création/Modification de l'année");
                CallbackUtilitaire.setCallback(request, callback);
            }

        } else {
            /*
             * Sinon on redirige l'utilisateur vers son formulaire pour qu'il modifie les
             * champs non valides
             */
            String resultat = "Échec de la Planification. ";
            request.setAttribute(ServletUtilitaire.ATT_RESULTAT, resultat);
            request.setAttribute(ServletUtilitaire.ATT_ERREURS, erreurs);
            request.setAttribute(ServletUtilitaire.CHAMP_ANNEE_DEBUT, this.anneeDebut);
            request.setAttribute(ServletUtilitaire.CHAMP_ANNEE_FIN, this.anneeFin);
            request.setAttribute(ServletUtilitaire.CHAMP_DATE_DEBUT_PROJET, this.dateDebutProjet);
            request.setAttribute(ServletUtilitaire.CHAMP_DATE_MI_AVANCEMENT, this.dateMiAvancement);
            request.setAttribute(ServletUtilitaire.CHAMP_DATE_DEPOT_POSTER, this.dateDepotPoster);
            request.setAttribute(ServletUtilitaire.CHAMP_DATE_SOUTENANCE_FINALE, this.dateSoutenanceFinale);
            callback = new Callback(CallbackType.ERROR,
                    "Echec de la création de l'année.");
            CallbackUtilitaire.setCallback(request, callback);
        }
    }

    /**
     * Vérifie si tous les champs saisis sont corrects.
     *
     * @param request la requête possédant les paramètres à récupérer.
     * @param erreurs les erreurs rencontrées.
     */
    private void validation(HttpServletRequest request, Map<String, String> erreurs) {
        String param = request.getParameter(ServletUtilitaire.CHAMP_ANNEE_DEBUT);
        if (anneeValide(param)) {
            this.anneeDebut = Integer.valueOf(request.getParameter(ServletUtilitaire.CHAMP_ANNEE_DEBUT));
        } else {
            erreurs.put(ServletUtilitaire.CHAMP_ANNEE_DEBUT, NON_VIDE);
        }
        param = request.getParameter(ServletUtilitaire.CHAMP_ANNEE_FIN);
        if (anneeValide(param)) {
            this.anneeFin = Integer.valueOf(request.getParameter(ServletUtilitaire.CHAMP_ANNEE_FIN));
        } else {
            erreurs.put(ServletUtilitaire.CHAMP_ANNEE_FIN, NON_VIDE);
        }
        param = request.getParameter(ServletUtilitaire.CHAMP_DATE_DEBUT_PROJET);
        if (dateValide(param)) {
            this.dateDebutProjet = Date.valueOf(request.getParameter(ServletUtilitaire.CHAMP_DATE_DEBUT_PROJET));
        } else {
            erreurs.put(ServletUtilitaire.CHAMP_DATE_DEBUT_PROJET, NON_VIDE);
        }
        param = request.getParameter(ServletUtilitaire.CHAMP_DATE_MI_AVANCEMENT);
        if (dateValide(param)) {
            this.dateMiAvancement = Date.valueOf(request.getParameter(ServletUtilitaire.CHAMP_DATE_MI_AVANCEMENT));
        } else {
            erreurs.put(ServletUtilitaire.CHAMP_DATE_MI_AVANCEMENT, NON_VIDE);
        }
        param = request.getParameter(ServletUtilitaire.CHAMP_DATE_DEPOT_POSTER);
        if (dateValide(param)) {
            this.dateDepotPoster = Date.valueOf(request.getParameter(ServletUtilitaire.CHAMP_DATE_DEPOT_POSTER));
        } else {
            erreurs.put(ServletUtilitaire.CHAMP_DATE_DEPOT_POSTER, NON_VIDE);
        }
        param = request.getParameter(ServletUtilitaire.CHAMP_DATE_SOUTENANCE_FINALE);
        if (dateValide(param)) {
            this.dateSoutenanceFinale = Date
                    .valueOf(request.getParameter(ServletUtilitaire.CHAMP_DATE_SOUTENANCE_FINALE));
        } else {
            erreurs.put(ServletUtilitaire.CHAMP_DATE_SOUTENANCE_FINALE, NON_VIDE);
        }
    }

    /**
     * Vérifie si l'année passée en paramètre est valide.
     *
     * @param annee l'année concernée.
     * @return valide le booleen indiquant si l'année est valide ou non.
     */
    private boolean anneeValide(String annee) {
        boolean valide = false;
        if (!annee.isEmpty() && annee.matches("\\d{4}")) {
            valide = true;
        }
        return valide;
    }

    /**
     * Vérifie si la date passée en paramètre est valide.
     *
     * @param date la date concernée.
     * @return valide le booleen indiquant si la date est valide ou non.
     */
    private boolean dateValide(String date) {
        boolean valide = false;
        if (!date.isEmpty() && date.matches(FORMAT_DATE)) {
            valide = true;
        }
        return valide;
    }

    /**
     * Vérifie si les deadlines sont bien comprisent entre l'année de début et
     * l'année de fin.
     *
     * @param erreurs les erreurs rencontrées.
     */
    private void coherenceDates(Map<String, String> erreurs) {
        try {
            Date dateDebut = Date.valueOf(this.anneeDebut + "-01-01");
            Date dateFin = Date.valueOf(this.anneeFin + "-12-31");

            if (!(this.dateDebutProjet.after(dateDebut) && this.dateDebutProjet.before(dateFin))) {
                erreurs.put(ServletUtilitaire.CHAMP_DATE_DEBUT_PROJET, NON_VALIDE);
            }
            if (!(this.dateMiAvancement.after(dateDebut) && this.dateMiAvancement.before(dateFin))) {
                erreurs.put(ServletUtilitaire.CHAMP_DATE_MI_AVANCEMENT, NON_VALIDE);
            }
            if (!(this.dateDepotPoster.after(dateDebut) && this.dateDepotPoster.before(dateFin))) {
                erreurs.put(ServletUtilitaire.CHAMP_DATE_DEPOT_POSTER, NON_VALIDE);
            }
            if (!(this.dateSoutenanceFinale.after(dateDebut) && this.dateSoutenanceFinale.before(dateFin))) {
                erreurs.put(ServletUtilitaire.CHAMP_DATE_SOUTENANCE_FINALE, NON_VALIDE);
            }
        } catch (NullPointerException e) {
            logger.log(Level.WARN, "Format de la date non valide", e);
        }
    }

    /**
     * Crée l'année scolaire si elle n'existe pas pour une option ou la remplace
     * sinon.
     *
     * @param anneeScolaire l'année scolaire à créer / modifier.
     * @param option        l'option concernée.
     * @param annees        la liste des années scolaires de la BDD.
     */
    private void ajoutModifAnneeScolaire(AnneeScolaire anneeScolaire, OptionESEO option, List<AnneeScolaire> annees) {
        boolean existe = false;
        for (AnneeScolaire annee : annees) {
            if (annee.getIdOption().compareTo(option.getIdOption()) == 0) {
                anneeScolaire.setIdAnneeScolaire(annee.getIdAnneeScolaire());
                existe = true;
            }
        }
        if (existe) {
            this.anneeScolaireDAO.modifier(anneeScolaire);
        } else {
            this.anneeScolaireDAO.creer(anneeScolaire);
        }
    }

    /**
     * Generation du nombre de sprint en fonction du choix de l'utilisateur
     */

    private void selectNbrSprint(HttpServletRequest req) {
        /* Récupération des données saisies */
        String selectAnnee = req.getParameter("selectAnnee");
        Callback callback;

        Long anneeDcp = Long.valueOf(selectAnnee.substring(selectAnnee.indexOf('=') + 1, selectAnnee.indexOf(',')));

        AnneeScolaire annee = new AnneeScolaire();
        annee.setIdAnneeScolaire(anneeDcp);
        AnneeScolaire anneeSelectionnee = this.anneeScolaireDAO.trouver(annee).get(0);

        Integer nombreSprint = Integer.valueOf((req.getParameter(ServletUtilitaire.CHAMP_NBR_SPRINT)));

        /*On supprime les sprints de l'année choisie*/
        Sprint sprint = new Sprint();
        sprint.setRefAnnee(anneeDcp);
        List<Sprint> listeSprintDelete = this.sprintDAO.trouver(sprint);
        for (Sprint sprintDel : listeSprintDelete){
            /*Si des matières sans notes sont associées au sprint */
            /*On supprime les matières liées au sprint */
            Matiere matiere = new Matiere();
            matiere.setRefSprint(sprintDel.getIdSprint());
            this.matiereDAO.supprimer(matiere);

            /*On supprime les sprints */
            this.sprintDAO.supprimer(sprintDel);
        }

        /*On créée les sprints */
        int nbrSprintNonDelete = this.sprintDAO.trouver(sprint).size();
        sprint.setDateDebut(String.valueOf(anneeSelectionnee.getAnneeDebut()) + "-09-01");
        sprint.setDateFin(String.valueOf(anneeSelectionnee.getAnneeFin()) + "-07-04 ");
        for (int i = 0; i < nombreSprint; i++) {
            sprint.setNumero(nbrSprintNonDelete+(i+1));
            this.sprintDAO.creer(sprint);
        }
        callback = new Callback(CallbackType.SUCCESS,
                "Création des sprints.");
        CallbackUtilitaire.setCallback(req, callback);
    }

    /**
     * Affichage de la liste des sprints
     */
    private void listeSprints(HttpServletRequest request) {
        /* Récupération des données saisies */
        request.setAttribute(ServletUtilitaire.ATT_LISTE_SPRINTS, this.sprintDAO.lister());
    }
    
    /**
     * Affichage de la liste des matieres
     */
    private void listeMatieres(HttpServletRequest request) {
        /* Récupération des données saisies */
        request.setAttribute(ServletUtilitaire.ATT_LISTE_MATIERES, this.matiereDAO.lister());
    }
    

    /**
     * Methode permettant la modification d'un sprint
     */
    private void modificationSprint(HttpServletRequest request) {
        Map<String, String> erreurs = new HashMap<>();
        Callback callback;

        /* Récupération des données saisies */
        Long idSprint = Long.parseLong(request.getParameter(ServletUtilitaire.CHAMP_ID_SPRINT));
        String dateDebut = request.getParameter(ServletUtilitaire.CHAMP_DATE_DEBUT_SPRINT);
        String dateFin = request.getParameter(ServletUtilitaire.CHAMP_DATE_FIN_SPRINT);
        String nombreHeures = String.valueOf(request.getParameter(ServletUtilitaire.CHAMP_NOMBRE_HEURES_SPRINT));
        Float coefficient = Float.valueOf(request.getParameter(ServletUtilitaire.CHAMP_COEFFICIENT_SPRINT));
        Integer numero = Integer.valueOf(request.getParameter(ServletUtilitaire.CHAMP_NUMERO_SPRINT));
        String refAnnee = request.getParameter(ServletUtilitaire.CHAMP_REF_ANNEE_SPRINT);

        verificationAnneeSprint(erreurs, dateDebut, dateFin, refAnnee);

        verificationNum(erreurs, nombreHeures);
        verificationNumDot(erreurs, String.valueOf(coefficient));


        if (erreurs.isEmpty()) {
            Sprint sprint = new Sprint();
            sprint.setIdSprint(idSprint);
            sprint.setNumero(numero);
            sprint.setDateDebut(dateDebut);
            sprint.setDateFin(dateFin);
            sprint.setNbrHeures(Integer.valueOf(nombreHeures));
            sprint.setCoefficient(coefficient);
            sprint.setRefAnnee(Long.valueOf(refAnnee));
            this.sprintDAO.modifier(sprint);

            /* Ajout d'une notification */
            callback = new Callback(CallbackType.SUCCESS,
                    "Modification du sprint.");
            CallbackUtilitaire.setCallback(request, callback);
            /* Rechargement de la page*/
        } else {
            String resultat = "Échec de la modification du sprint, des erreurs sont présentes. ";
            callback = new Callback(CallbackType.ERROR,
                    "Échec de la modification du sprint, des erreurs sont présentes.");
            CallbackUtilitaire.setCallback(request, callback);
            request.setAttribute(ServletUtilitaire.ATT_RESULTAT, resultat);
            request.setAttribute(ServletUtilitaire.ATT_ERREURS, erreurs);
            /* Ré-affichage du formulaire avec les erreurs */
        }
    }

    private void verificationNum(Map<String, String> erreurs, String nombreHeures) {
        if (!(nombreHeures.matches(FORMAT_NUM))) {
            erreurs.put(ServletUtilitaire.CHAMP_ERREUR_NBR_HEURES_SPRINT, NON_VALIDE);
        }
    }

    private void verificationNumDot(Map<String, String> erreurs, String nbrATester) {
        if (!(nbrATester.matches(FORMAT_NUM_DOT))) {
            erreurs.put(ServletUtilitaire.CHAMP_ERREUR_NBR_COEFFICIENT_SPRINT, NON_VALIDE);
        }
    }
    
    
    private void modificationCoeffIntra(HttpServletRequest request) {
    	Map<String, String> erreurs = new HashMap<>();
        Callback callback;
    	ArrayList<String> coefficients = new ArrayList<>();
        ArrayList<String> libelles = new ArrayList<>();
        ArrayList<String> identifiants = new ArrayList<>();
        Long idSprint = Long.valueOf(request.getParameter("idSprint"));
        
        if(idSprint != null) {
        	ArrayList<Matiere> listeDesMatieres = listeMatieresPourUnSprint(idSprint);
        	for(int compteur = 0; compteur<listeDesMatieres.size(); compteur++) {
        		String matiereLibelleString = ("matiereLibelle"+(listeDesMatieres.get(compteur).getIdMatiere())).toString();
            	String matiereCoefficientString = ("matiereCoefficient"+(listeDesMatieres.get(compteur).getIdMatiere())).toString();
            	String matiereIdentifiantString = ("matiereIdentifiant"+(listeDesMatieres.get(compteur).getIdMatiere())).toString();

            	String libelle = request.getParameter(matiereLibelleString);
            	libelles.add(libelle);
            	String coefficient = request.getParameter(matiereCoefficientString);

            	if(ParamUtilitaire.notNull(coefficient)) {
            		verificationNumDot(erreurs, coefficient);
            	}
            	coefficients.add(coefficient);
            	String identifiant = request.getParameter(matiereIdentifiantString);
            	
            	identifiants.add(identifiant);
        	}
            if (erreurs.isEmpty()) {
            	modificationCoeffient(listeDesMatieres,coefficients, libelles, identifiants,  idSprint);
                /* Ajout d'une notification */
                callback = new Callback(CallbackType.SUCCESS,
                        "Modification des coefficients des notes.");
                CallbackUtilitaire.setCallback(request, callback);
                /* Rechargement de la page*/
            } else {
                String resultat = "Échec de la modification des coefficients du sprint, des erreurs sont présentes. ";
                callback = new Callback(CallbackType.ERROR,
                        "Échec de la modification des coefficients des notes, des erreurs sont présentes.");
                CallbackUtilitaire.setCallback(request, callback);
                request.setAttribute(ServletUtilitaire.ATT_RESULTAT, resultat);
                request.setAttribute(ServletUtilitaire.ATT_ERREURS, erreurs);
                /* Ré-affichage du formulaire avec les erreurs */
            }
        }
                
    }
    
    
    private void ajoutMatiere(HttpServletRequest request) {
    	Map<String, String> erreurs = new HashMap<>();
        Callback callback;
        ArrayList<String> coefficientsMatieres = new ArrayList<>();
        ArrayList<String> libellesMatieres = new ArrayList<>();
    	Long idSprint = Long.valueOf(request.getParameter("idSprint"));
    	String matierePresenteAbsente = request.getParameter("presentOuNon");
    	if(matierePresenteAbsente.equalsIgnoreCase("present")) {
    		int compteurMatiere = ParamUtilitaire.getInt(request.getParameter("inputCompteur"));
    		for(int i = 1; i<=compteurMatiere;i++) {
        		String coefficientLibelle = ("coefficient"+i).toString();
        		String libelleLibelle = ("libelle"+i).toString();
        		String coefficient = request.getParameter(coefficientLibelle);
        		String libelle = request.getParameter(libelleLibelle);
        		if(!coefficient.isEmpty()) {
        			verificationNumDot(erreurs, coefficient);
        		}
        		if(coefficient.isEmpty() || libelle.isEmpty()) {
        			erreurs.put(ServletUtilitaire.CHAMP_ERREUR_NBR_COEFFICIENT_SPRINT, NON_VALIDE);
        		}
        		libellesMatieres.add(libelle);
        		coefficientsMatieres.add(coefficient);
        	}
        	if (erreurs.isEmpty()) {
        		creerMatiere(coefficientsMatieres, libellesMatieres, idSprint);
                /* Ajout d'une notification */
                callback = new Callback(CallbackType.SUCCESS,
                        "Ajout des matieres effectué.");
                CallbackUtilitaire.setCallback(request, callback);
                /* Rechargement de la page*/
            } else {
                String resultat = "Échec de l'ajout des matieres. ";
                callback = new Callback(CallbackType.ERROR,
                        "Échec de l'ajout des matieres.");
                CallbackUtilitaire.setCallback(request, callback);
                request.setAttribute(ServletUtilitaire.ATT_RESULTAT, resultat);
                request.setAttribute(ServletUtilitaire.ATT_ERREURS, erreurs);
                /* Ré-affichage du formulaire avec les erreurs */
            }
    	}
    }
    
    private void verificationAnneeSprint(Map<String, String> erreurs, String dateDebut, String dateFin, String refAnnee) {
        /* Verif de l'année */
        AnneeScolaire annee = new AnneeScolaire();
        annee.setIdAnneeScolaire(Long.valueOf(refAnnee));
        AnneeScolaire anneeSelectionnee = this.anneeScolaireDAO.trouver(annee).get(0);

        Date anneeDebutSprint = Date.valueOf(dateDebut);
        Date anneeFinSprint = Date.valueOf(dateFin);

        Date anneeDebutTest = Date.valueOf(anneeSelectionnee.getAnneeDebut() + "-01-01");
        Date anneeFinTest = Date.valueOf(anneeSelectionnee.getAnneeFin() + "-12-31");

        if (!(anneeDebutSprint.after(anneeDebutTest) && anneeFinSprint.before(anneeFinTest))) {
            erreurs.put(ServletUtilitaire.CHAMP_ERREUR_DATE_DEBUT_SPRINT, NON_VALIDE);
        }
        if (!(anneeFinSprint.after(anneeDebutTest) && anneeFinSprint.before(anneeFinTest))) {
            erreurs.put(ServletUtilitaire.CHAMP_ERREUR_DATE_FIN_SPRINT, NON_VALIDE);
        }

    }
    
    
    private ArrayList<Matiere> listeMatieresPourUnSprint(Long idSprint){
    	OptionESEO optionESEO = new OptionESEO();
        optionESEO.setNomOption("LD");
        OptionESEO optionLD = this.optionESEODAO.trouver(optionESEO).get(0);
        Long idOptionLD = optionLD.getIdOption();

        AnneeScolaire anneeLD = new AnneeScolaire();
        anneeLD.setIdOption(idOptionLD);
        List<AnneeScolaire> annees = this.anneeScolaireDAO.trouver(anneeLD);
    	ArrayList<Matiere> listeDesMatieres = new ArrayList<>(); 
    	for(int compteur = 0; compteur < this.anneeScolaireDAO.lister().size(); compteur++) {
    		for(int i = 0; i < this.sprintDAO.lister().size(); i++) {
            	for(int j = 0; j < this.matiereDAO.lister().size(); j ++) {
    	        	if(this.anneeScolaireDAO.lister().get(compteur).getIdAnneeScolaire() ==  annees.size() && this.sprintDAO.lister().get(i).getIdSprint() == this.matiereDAO.lister().get(j).getRefSprint()
    	        			&& this.matiereDAO.lister().get(j).getRefSprint() == idSprint) {
    	        		listeDesMatieres.add(this.matiereDAO.lister().get(j));
    	        	}
            	}
            }
    	}
    	return listeDesMatieres;
    }
    
    private void modificationCoeffient(ArrayList<Matiere> listeDesMatieres, ArrayList<String> coefficients, ArrayList<String> libelles, ArrayList<String> identifiants, Long idSprint) {
		for(int i = 0; i < listeDesMatieres.size(); i++){
        	Matiere matiereModifiee = new Matiere();
        	matiereModifiee.setIdMatiere(Long.valueOf(identifiants.get(i)));
        	
        	matiereModifiee.setRefSprint(idSprint);
        	matiereModifiee.setLibelle(libelles.get(i));
        	if(ParamUtilitaire.notNull(coefficients.get(i))) {
        		matiereModifiee.setCoefficient(Float.valueOf(coefficients.get(i)));
        	}
        	matiereDAO.modifier(matiereModifiee);
		}
	}
    
    private void creerMatiere(ArrayList<String> coefficientsMatieres, ArrayList<String> libellesMatieres, Long idSprint) {
		for(int i = 0; i < coefficientsMatieres.size(); i++){
        	Matiere matiere = new Matiere();
        	if(!coefficientsMatieres.get(i).isEmpty() && !libellesMatieres.get(i).isEmpty()) {
        		matiere.setCoefficient(Float.valueOf(coefficientsMatieres.get(i)));
        		matiere.setLibelle(libellesMatieres.get(i));
        	}
    		matiere.setRefSprint(idSprint);        	
        	matiereDAO.creer(matiere);
        }
	}

}