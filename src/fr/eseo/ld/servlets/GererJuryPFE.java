package fr.eseo.ld.servlets;

import fr.eseo.ld.beans.*;
import fr.eseo.ld.dao.*;
import fr.eseo.ld.utils.CallbackUtilitaire;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.List;

/**
 * Servlet implementation class GererJury
 */
@WebServlet("/GererJuryPFE")
public class GererJuryPFE extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private static final String VUE_FORM = "/WEB-INF/formulaires/gererJurysPFE.jsp";
	private static final String ATT_NUM_JURY_POSTER = "numJuryDePoster";
	private static final String ATT_ACTION = "actionButton";
	private static final String CHEMIN_FICHIER = "/images/planAffichagePoster/planAffichage2.PNG";
	private static final String CHEMIN_OUT_FICHIER = "/images/planAffichagePoster/planAffichageComplet.png";
	private static final String ATT_ONGLET_ACTIF = "onglet";
	private static final String ATT_FORMULAIRE = "formulaire";

	private SoutenanceDAO soutenanceDAO;
	private JurySoutenanceDAO jurySoutenanceDAO;
	private SujetDAO sujetDAO;
	private PosterDAO posterDAO;
	private OptionESEODAO optionESEODAO;
	private ProfesseurDAO professeurDAO;
	private JuryPosterDAO juryPosterDAO;
	private UtilisateurDAO utilisateurDAO;
	private ProfesseurSujetDAO professeurSujetDAO;
	private EquipeDAO equipeDAO;

	Boolean resultatExceptionGenererPlanAffichage = false;
	private long idSujet = -1;
	private Soutenance soutenance = new Soutenance();

	private static Logger logger = Logger.getLogger(GererJuryPFE.class.getName());

	@Override
	public void init() throws ServletException {
		DAOFactory daoFactory = (DAOFactory) this.getServletContext().getAttribute(ServletUtilitaire.CONF_DAO_FACTORY);
		this.sujetDAO = daoFactory.getSujetDao();
		this.optionESEODAO = daoFactory.getOptionESEODAO();
		this.professeurDAO = daoFactory.getProfesseurDAO();
		this.juryPosterDAO = daoFactory.getJuryPosterDAO();
		this.soutenanceDAO = daoFactory.getSoutenanceDAO();
		this.jurySoutenanceDAO = daoFactory.getJurySoutenanceDAO();
		this.posterDAO = daoFactory.getPosterDao();
		this.utilisateurDAO = daoFactory.getUtilisateurDao();
		this.professeurSujetDAO = daoFactory.getProfesseurSujetDAO();
		this.equipeDAO = daoFactory.getEquipeDAO();
	}

	@Override
	@SuppressWarnings("unchecked")
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		/* Récupération de la session depuis la requête */
		/* Récupération d'attributs de l'Utilisateur en session */
		/* Recherche de la liste des sujets de l'option du ProfesseurResponsable */
		/* Correspondance de l'ID du Professeur avec toutes ses options en un string */
		HttpSession session = request.getSession();
		List<Role> roles = (ArrayList<Role>) session.getAttribute(ServletUtilitaire.ATT_SESSION_ROLES);
		List<OptionESEO> options = (ArrayList<OptionESEO>) session.getAttribute(ServletUtilitaire.ATT_SESSION_OPTIONS);
		List<String> allOptions = new ArrayList<>();
		/* Partie commune : Jury de soutenance et de poster */
		List<Sujet> listeSujets;
		List<Sujet> listeSujetsOptions = new ArrayList<>();
		List<Utilisateur> listeProfesseurs;
		List<Utilisateur> listeUtilisateurs;
		/* Partie Jury de Soutenance */
		Map<Long, String> mapOptionsProfesseur = new HashMap<>();
		Map<Long, Utilisateur> mapIdProfesseur = new HashMap<>();
		List<Soutenance> listeSoutenance = new ArrayList<>();
		Map<Long, String> nomsJurys = new HashMap<>();
		Map<String, JurySoutenance> jurysSoutenances = new HashMap<>();
		List<ProfesseurSujet> referentSujets = new ArrayList<>();
		String optionPrincipale = "";
		/* Partie Jury de Poster */
		List<JuryPoster> listeJurysPoster;
		List<Poster> listePosters;

		/* Remplissage des listes */
		listeProfesseurs = this.professeurDAO.listerProfesseursDisponibles();
		listeSujets = this.sujetDAO.lister();
		listeJurysPoster = this.juryPosterDAO.lister();
		listeUtilisateurs = this.utilisateurDAO.lister();
		listePosters = this.posterDAO.lister();

		/* Complete les listes options principales et listeSujetOptions */
		for (int i = 0; i < roles.size(); i++) {
			if (roles.get(i).getNomRole().contentEquals("profResponsable")) {
				optionPrincipale = options.get(i).getNomOption();
				listeSujetsOptions.addAll(this.sujetDAO.trouverSujetOption(options.get(i)));
			}
		}

		creerListeSujetsOptions(listeSujets, listeSujetsOptions, referentSujets);

		ServletUtilitaire.trouverProfesseur(mapOptionsProfesseur, mapIdProfesseur, allOptions, listeProfesseurs,
				this.optionESEODAO);

		for (Sujet sujet : listeSujets) {
			Soutenance soutenanceSujet = soutenanceDAO.trouverSoutenanceSujet(sujet);
			listeSoutenance.add(soutenanceSujet);
			if (soutenanceSujet.getIdJurySoutenance() != null) {
				JurySoutenance jurySoutenance = new JurySoutenance();
				jurySoutenance.setIdJurySoutenance(soutenanceSujet.getIdJurySoutenance());
				List<JurySoutenance> jurySoutenanceTrouves = this.jurySoutenanceDAO.trouver(jurySoutenance);

				jurysSoutenances.put(soutenanceSujet.getIdJurySoutenance(), jurySoutenanceTrouves.get(0));
				if ((!jurySoutenanceTrouves.isEmpty()) && (jurySoutenanceTrouves.get(0).getIdProf1() != null)) {
					nomsJurys = getNomJury(jurySoutenanceTrouves.get(0).getIdProf1(), nomsJurys);
				}
				if ((!jurySoutenanceTrouves.isEmpty()) && (jurySoutenanceTrouves.get(0).getIdProf2() != null)) {
					nomsJurys = getNomJury(jurySoutenanceTrouves.get(0).getIdProf2(), nomsJurys);
				}
			}

		}

		request.setAttribute(ServletUtilitaire.ATT_LISTE_SUJETS, listeSujets);
		request.setAttribute(ServletUtilitaire.ATT_LISTE_PROFESSEURS, listeProfesseurs);
		request.setAttribute(ServletUtilitaire.ATT_LISTE_UTILISATEURS, listeUtilisateurs);
		request.setAttribute("listePosters", listePosters);
		request.setAttribute("listeJurysPoster", listeJurysPoster);
		request.setAttribute(ServletUtilitaire.ATT_LISTE_SOUTENANCES, listeSoutenance);
		request.setAttribute("optionPrincipal", optionPrincipale);
		request.setAttribute("mapOptionsProf", mapOptionsProfesseur);
		request.setAttribute("jurysSoutenances", jurysSoutenances);
		request.setAttribute("nomjury", nomsJurys);
		request.setAttribute("allOptions", allOptions);
		request.setAttribute("mapIdProfesseur", mapIdProfesseur);
		request.setAttribute("referentSujets", referentSujets);
		this.getServletContext().getRequestDispatcher(VUE_FORM).forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		List<Long> idParticipants;
		Date date;
		jurySoutenanceRequest(request, response);
		try {
			if ("modifierLeJuryPoster".equals(request.getParameter("modifierJuryDePoster"))) {
				idParticipants = idParticipants(request);
				date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm").parse(request.getParameter("datePopup"));
				if (idParticipants.size() == 3) {
					modifierJuryPoster(request, request.getParameter(ATT_NUM_JURY_POSTER), idParticipants, date);
				} else {
					CallbackUtilitaire.setCallback(request,
							new Callback(CallbackType.ERROR, "Vous n'avez pas sélectionné 3 professeurs"));
				}

			}
			if ("SupprimerJuryPoster".equals(request.getParameter("delJuryDePoster"))) {
				JuryPoster juryPoster = new JuryPoster();
				juryPoster.setIdJuryPoster(request.getParameter(ATT_NUM_JURY_POSTER));
				this.juryPosterDAO.supprimer(juryPoster);
				CallbackUtilitaire.setCallback(request,
						new Callback(CallbackType.SUCCESS, "Le jury de Poster a eté supprimé"));
			}
			if ("creerJuryPoster".equals(request.getParameter(ATT_ACTION))) {
				date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm").parse("2018-02-01T08:00");
				genererLesJurysAlea(this.professeurDAO.listerProfesseursDisponibles(), date);
				CallbackUtilitaire.setCallback(request,
						new Callback(CallbackType.SUCCESS, "La génération des jurys de posters a été effectué"));
			}
		} catch (Exception e) {
			logger.log(Level.WARN, "Erreur de generation", e);
		}

		if ("PlanAffichagePoster".equals(request.getParameter(ATT_ACTION))) {
			try {
				genererPlanAffichage();
				/* Formatage de la response */
				response.setContentType("text/html");
				PrintWriter out = response.getWriter();
				String nomImage = "Plan d'Affichage.png";
				String cheminImage = getServletContext().getRealPath(CHEMIN_OUT_FICHIER);

				response.setContentType("APPLICATION/PNG");
				response.setHeader("Content-Disposition", "inline; filename=\"" + nomImage + "\"");

				/* Téléchargement de l'image */
				try (FileInputStream fileInputStream = new FileInputStream(cheminImage)) {
					int i;
					while ((i = fileInputStream.read()) != -1) {
						out.write(i);
					}

					out.close();
				}

			} catch (Exception e) {
				resultatExceptionGenererPlanAffichage = true;
				logger.log(Level.WARN, "Erreur de generation", e);
			}
		}
		if (!"PlanAffichagePoster".equals(request.getParameter(ATT_ACTION))) {
			request.setAttribute(ATT_ONGLET_ACTIF, "juryPoster");
			doGet(request, response);
		}
	}

	private void jurySoutenanceRequest(HttpServletRequest request, HttpServletResponse response) throws IOException {
		if ("jurySoutenance".equals(request.getParameter(ATT_FORMULAIRE))) {
			/* Mapping de la soutenance si une seule soutenance est choisie */
			String numero = request.getParameter("numDivForm");
			if (!numero.contentEquals("")) {
				this.mapSoutenance(request, numero);
				/* Redirection vers la page prÃ©cÃ©dente */
				response.sendRedirect(request.getHeader(ServletUtilitaire.HEADER));
				return;
			}

			// RÃ©cupÃ©ration des paramÃ¨tres saisis
			boucleParametre(request);

			if (this.idSujet != -1) {
				this.creerSoutenance();
			}

			/* Redirection vers la page prÃ©cÃ©dente */
			response.sendRedirect(request.getHeader(ServletUtilitaire.HEADER));
		}
	}

	private void creerListeSujetsOptions(List<Sujet> listeSujets, List<Sujet> listeSujetsOptions,
			List<ProfesseurSujet> referentSujets) {
		List<ProfesseurSujet> professeurSujetsAuxiliaire;
		for (Sujet sujet : listeSujetsOptions) {
			if (sujet.getEtat() == EtatSujet.ATTRIBUE) {
				listeSujets.add(sujet);
				professeurSujetsAuxiliaire = utilisateurDAO.trouverProfesseursAttribuesAUnSujet(sujet);
				referentSujets.add(new ProfesseurSujet());
				for (ProfesseurSujet professeur : professeurSujetsAuxiliaire) {
					if (professeur.getFonction() == FonctionProfesseurSujet.REFERENT) {
						referentSujets.set(referentSujets.size() - 1, professeur);
					}
				}
			}
		}

	}

	/**
	 * Lie le nom d'un Jury avec son ID.
	 * 
	 * @param idJurySoutenance
	 *            l'ID du JurySoutenance.
	 * @param nomsJurys
	 *            la Map des ID des Jurys liés avec leurs noms.
	 * @return nomsJurys la Map des ID des Jurys liés avec leurs noms.
	 */
	private Map<Long, String> getNomJury(long idJurySoutenance, Map<Long, String> nomsJurys) {
		Utilisateur profJury = new Utilisateur();
		profJury.setIdUtilisateur(idJurySoutenance);
		List<Utilisateur> utilisateursTrouves = this.utilisateurDAO.trouver(profJury);
		String nom;
		if (!utilisateursTrouves.isEmpty()) { // sécurité dans le cas où la BDD ait mal été remplie
			nom = utilisateursTrouves.get(0).getPrenom() + " " + utilisateursTrouves.get(0).getNom();
		} else {
			nom = "";
		}
		nomsJurys.put(idJurySoutenance, nom);

		return nomsJurys;
	}

	/**
	 * 
	 * @param request
	 * @return liste des professeurs participants au Jury
	 */
	protected List<Long> idParticipants(HttpServletRequest request) {
		List<Long> idParticipants = new ArrayList<>();

		for (int i = 0; i < this.professeurDAO.lister().size(); i++) {
			if (request.getParameter("checkbox-" + i) != null) {
				idParticipants.add(Long.parseLong(request.getParameter("checkbox-" + i)));
			}
		}
		return idParticipants;
	}

	/**
	 * Sélectionne un professeur aléatoire parmi tous les professeurs disponibles.
	 * 
	 * @param listerProfesseursDisponibles
	 * 
	 * @return Professeur
	 * 
	 */
	private Professeur professeurRandom(List<Utilisateur> listerProfesseursDisponibles) {
		Random r = new Random();
		Long idProf;
		Professeur prof = new Professeur();

		int random = r.nextInt(listerProfesseursDisponibles.size());
		idProf = listerProfesseursDisponibles.get(random).getIdUtilisateur();
		prof.setIdProfesseur(idProf);

		return prof;
	}

	/**
	 * Compare 3 proffesseur pour savoir si ils sont différents
	 * 
	 * @param prof1,
	 *            prof2, prof3
	 * 
	 * @return true si ils sont différents, false sinon.
	 * 
	 */
	private boolean testTrinome(Professeur prof1, Professeur prof2, Professeur prof3) {
		return prof1.getIdProfesseur() != prof2.getIdProfesseur() && prof1.getIdProfesseur() != prof3.getIdProfesseur()
				&& prof2.getIdProfesseur() != prof3.getIdProfesseur();
	}

	/**
	 * Vérification si le proffesseur peut être Jury du Poster.
	 * 
	 * @param prof
	 * @param poster
	 * 
	 * @return renvoie true si il n'encadre pas le poster et false sinon.
	 */
	private boolean testProfPeutEtreJury(Professeur prof, Poster poster) {
		for (int i = 0; i < this.professeurSujetDAO.lister().size(); i++) {
			if (this.professeurSujetDAO.lister().get(i).getIdProfesseur() == prof.getIdProfesseur()
					&& this.professeurSujetDAO.lister().get(i).getIdSujet() == poster.getIdSujet()) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Liste les Posters qui ne sont pas associés a une JuryPoster
	 * 
	 * @param
	 * 
	 * @return List<Poster>
	 */
	private List<Poster> listerPosterSansJuryPoster() {
		List<Poster> listePosterDAOAvecJuryPoster = new ArrayList<>();
		List<Poster> listerPosterSansJuryPoster;

		listerPosterSansJuryPoster = this.posterDAO.lister();

		for (int i = 0; i < this.juryPosterDAO.lister().size(); i++) {
			for (int j = 0; j < this.posterDAO.lister().size(); j++) {
				if (isJuryPosterEquipe(i, j)) {
					listePosterDAOAvecJuryPoster.add(this.posterDAO.lister().get(j));
				}
			}
		}

		for (int i = 0; i < listePosterDAOAvecJuryPoster.size(); i++) {
			for (int j = 0; j < listerPosterSansJuryPoster.size(); j++) {
				if (listePosterDAOAvecJuryPoster.get(i).getIdEquipe()
						.equalsIgnoreCase(listerPosterSansJuryPoster.get(j).getIdEquipe())) {
					listerPosterSansJuryPoster.remove(j);
				}
			}
		}
		return listerPosterSansJuryPoster;
	}

	private boolean isJuryPosterEquipe(int i, int j) {
		return isJuryEquipe(i, j);
	}

	private List<Poster> listerSujetPourTrinome(Professeur prof1, Professeur prof2, Professeur prof3) {
		List<Poster> listePosters = new ArrayList<>();
		for (int i = 0; i < listerPosterSansJuryPoster().size(); i++) {
			if (testProfPeutEtreJury(prof1, listerPosterSansJuryPoster().get(i))
					&& testProfPeutEtreJury(prof2, listerPosterSansJuryPoster().get(i))
					&& testProfPeutEtreJury(prof3, listerPosterSansJuryPoster().get(i))) {
				listePosters.add(listerPosterSansJuryPoster().get(i));
			}
		}
		return listePosters;
	}

	/**
	 * Modifie le jury de poster
	 * 
	 * @param numJuryPoster
	 * @param request
	 * @param idsUtilisateurs
	 */
	private void modifierJuryPoster(HttpServletRequest request, String numJuryPoster, List<Long> idsUtilisateurs,
			Date date) {
		JuryPoster juryPoster = new JuryPoster();
		Professeur prof1 = new Professeur();
		Professeur prof2 = new Professeur();
		Professeur prof3 = new Professeur();

		prof1.setIdProfesseur(idsUtilisateurs.get(0));
		prof2.setIdProfesseur(idsUtilisateurs.get(1));
		prof3.setIdProfesseur(idsUtilisateurs.get(2));

		if (testTrinome(prof1, prof2, prof3)) {
			for (int i = 0; i < this.juryPosterDAO.lister().size(); i++) {
				if (this.juryPosterDAO.lister().get(i).getIdJuryPoster().equalsIgnoreCase(numJuryPoster)) {
					juryPoster = this.juryPosterDAO.lister().get(i);
				}
			}
		} else {
			CallbackUtilitaire.setCallback(request,
					new Callback(CallbackType.ERROR, "Un des professeurs sélectionnés est référent du projet"));
		}
		juryPoster.setIdProf1(prof1.getIdProfesseur());
		juryPoster.setIdProf2(prof2.getIdProfesseur());
		juryPoster.setIdProf3(prof3.getIdProfesseur());
		juryPoster.setDate(date);
		this.juryPosterDAO.modifier(juryPoster);
	}

	/**
	 * Génere un Jury de Poster (3 professeurs pour 1 équipe, elle même associée à 1
	 * Poster)
	 * 
	 * @param listerProfesseursDisponibles
	 * 
	 * @return JuryPoster
	 */
	private void genererUnJuryAlea(List<Utilisateur> listerProfesseursDisponibles, Date date) {
		Professeur prof1 = new Professeur();
		Professeur prof2 = new Professeur();
		Professeur prof3 = new Professeur();
		JuryPoster juryposter = new JuryPoster();
		int conteur = listerSujetPourTrinome(prof1, prof2, prof3).size();
		int nombreJury = 5;

		while (!testTrinome(prof1, prof2, prof3)) {
			prof1 = professeurRandom(listerProfesseursDisponibles);
			prof2 = professeurRandom(listerProfesseursDisponibles);
			prof3 = professeurRandom(listerProfesseursDisponibles);
		}

		while (!testProfPeutEtreJury(prof1, listerPosterSansJuryPoster().get(0))
				|| !testProfPeutEtreJury(prof2, listerPosterSansJuryPoster().get(0))
				|| !testProfPeutEtreJury(prof3, listerPosterSansJuryPoster().get(0))
				|| !testTrinome(prof1, prof2, prof3)) {
			prof1 = professeurRandom(listerProfesseursDisponibles);
			prof2 = professeurRandom(listerProfesseursDisponibles);
			prof3 = professeurRandom(listerProfesseursDisponibles);
		}

		for (int i = 0; i < conteur; i++) {
			if (nombreJury > 0) {
				juryposter.setIdProf1(prof1.getIdProfesseur());
				juryposter.setIdProf2(prof2.getIdProfesseur());
				juryposter.setIdProf3(prof3.getIdProfesseur());
				juryposter.setIdEquipe(listerSujetPourTrinome(prof1, prof2, prof3).get(0).getIdEquipe());
				juryposter.setDate(date);
				this.juryPosterDAO.creer(juryposter);
				nombreJury -= 1;
			}
		}

	}

	/**
	 * 
	 * 
	 * 
	 * @param listerProfesseursDisponibles,
	 *            conteur (= nombre de posters sans Jury)
	 * 
	 */
	private void genererLesJurysAlea(List<Utilisateur> listerProfesseursDisponibles, Date date) {
		int conteur = listerPosterSansJuryPoster().size();

		while (conteur > 0) {
			genererUnJuryAlea(listerProfesseursDisponibles, date);
			conteur -= 1;
		}
	}

	private void genererPlanAffichage() throws IOException {
		char[] alphabet = { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R',
				'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z' };
		String newLine = "\n\r";
		String legendePlanAffichage = "Jury         Place         Sujet" + newLine;
		String cheminIn = getServletContext().getRealPath(CHEMIN_FICHIER);
		String cheminOut = getServletContext().getRealPath(CHEMIN_OUT_FICHIER);
		BufferedImage planAffichage = ImageIO.read(new File(cheminIn));
		File outputfile = new File(cheminOut);
		int conteur = 0;

		Graphics2D g2d = (Graphics2D) planAffichage.getGraphics();
		g2d.setFont(new Font("Serif", Font.PLAIN, 20));
		g2d.setColor(Color.BLACK);
		g2d.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
		g2d.drawString(legendePlanAffichage, 350, 700);
		for (int i = 0; i < this.juryPosterDAO.lister().size(); i++) {
			for (int j = 0; j < this.posterDAO.lister().size(); j++) {
				for (int k = 0; k < this.sujetDAO.lister().size(); k++) {
					if (isJuryEquipe(i, j)
							&& this.posterDAO.lister().get(j).getIdSujet() == this.sujetDAO.lister().get(k)
									.getIdSujet()) {
						legendePlanAffichage = (this.juryPosterDAO.lister().get(i).getIdJuryPoster() + "     "
								+ alphabet[i] + "     " + this.sujetDAO.lister().get(k).getTitre());
						conteur += 1;
						drawLegende(legendePlanAffichage, conteur, g2d, i);
					}
				}
			}
		}
		g2d.dispose();
		ImageIO.write(planAffichage, "png", outputfile);
		planAffichage.flush();

	}

	private boolean isJuryEquipe(int i, int j) {
		return this.juryPosterDAO.lister().get(i).getIdEquipe()
				.equalsIgnoreCase(this.posterDAO.lister().get(j).getIdEquipe());
	}

	private void drawLegende(String legendePlanAffichage, int conteur, Graphics2D g2d, int i) {
		if (conteur <= 12) {
			g2d.drawString(legendePlanAffichage, 350, 700 + (i + 1) * 20);
		}
		if (conteur > 12) {
			g2d.drawString(legendePlanAffichage, 1100, 700 + (i + 1) * 20);
		}
	}

	/**
	 * Fait la correspondance (le mapping) entre les paramètres récupérés d'une
	 * requête et un bean Soutenance.
	 * 
	 * @param request
	 *            la requête possédant les paramètres à récupérer.
	 * @return numero le numero de la soutenance sélectionnée.
	 */
	private void mapSoutenance(HttpServletRequest request, String numero) {
		validerParametres(request, numero);

		JurySoutenance jury = new JurySoutenance();
		String parametres = request.getParameter("idJury-" + numero);
		if (parametres != null && !parametres.isEmpty()) {
			jury.setIdJurySoutenance(parametres);
		}
		Map<String, String[]> parameters = request.getParameterMap();

		for (String parameter : parameters.keySet()) {
			if (parameter.startsWith("jury-" + numero)) {
				parametres = request.getParameter(parameter);
				jury = this.setJury(jury, parametres);
			}
		}
		if (this.idSujet != -1) {
			this.creerSoutenance();
		}
	}

	private void validerParametres(HttpServletRequest request, String numero) {
		String parametres = request.getParameter("idSoutenance-" + numero);
		if (parametres != null && !parametres.isEmpty()) {
			this.soutenance.setIdSoutenance(Long.parseLong(parametres));
		}
		parametres = request.getParameter("idSujet-" + numero);
		if (parametres != null && !parametres.isEmpty()) {
			this.idSujet = Long.parseLong(parametres);
		}
		parametres = request.getParameter("idEquipe-" + numero);
		if (parametres != null && !parametres.isEmpty()) {
			this.soutenance.setIdEquipe(parametres);
		}
		parametres = request.getParameter("dateSoutenance-" + numero);
		if (parametres != null && !parametres.isEmpty()) {
			this.setDate(parametres);
		}

	}

	/**
	 * Formate la date donnée pour qu'elle soit en adéquation avec la BDD.
	 * 
	 * @param dateString
	 *            la date à formater.
	 */
	private void setDate(String dateString) {
		Date date = new Date();
		try {
			date = new SimpleDateFormat("yyyy-MM-dd hh:mm").parse(dateString);
		} catch (ParseException e) {
			logger.log(Level.WARN, "Format de la date non valide", e);
		}
		this.soutenance.setDateSoutenance(date);
	}
	
	/**
	 * Insère ou Modifie une Soutenance dans la BDD à partir des attributs de la
	 * classe.
	 */
	private void creerSoutenance() {
		if (this.soutenance.getIdSoutenance() == null) {
			if (this.soutenance.getIdEquipe() == null) {
				Equipe equipe = new Equipe();
				equipe.setIdSujet(this.idSujet);
				List<Equipe> equipes = this.equipeDAO.trouver(equipe);
				this.soutenance.setIdEquipe(equipes.get(0).getIdEquipe());
			}
			this.soutenanceDAO.creer(this.soutenance);
			this.idSujet = -1;
		} else {
			this.soutenanceDAO.modifier(this.soutenance);
			this.idSujet = -1;
		}
	}
	
	private void boucleParametre(HttpServletRequest request) {
		Map<String, String[]> parametres = request.getParameterMap();
		JurySoutenance jury = new JurySoutenance();
		for (String parametre : parametres.keySet()) {
			String valeursChamps = request.getParameter(parametre);
			// idSoutenance représente le début des paramètres d'une Soutenance dans la JSP
			if (parametre.startsWith("idSoutenance-")) {
				if (this.idSujet != -1) {
					this.creerSoutenance();
				}
				this.soutenance = new Soutenance();
				jury = new JurySoutenance();
			}

			/* Continuation de la modification des jurys */
			if (valeursChamps != null && !valeursChamps.isEmpty()) {
				switch (parametre.substring(0, 5)) {
				case "idSou":
					this.soutenance.setIdSoutenance(Long.parseLong(valeursChamps));
					break;
				case "idJur":
					jury.setIdJurySoutenance(valeursChamps);
					break;
				case "idEqu":
					this.soutenance.setIdEquipe(valeursChamps);
					break;
				case "idSuj":
					idSujet = Long.parseLong(valeursChamps);
					break;
				case "jury-":
					jury = this.setJury(jury, valeursChamps);
					break;
				case "dateS":
					this.setDate(valeursChamps);
					break;
				default:
					break;
				}
			}
		}
	}
	
	/**
	 * Récupère un ID provenant d'un "jury-" et l'insère dans un bean Jury.
	 * 
	 * @param jury
	 *            le jury dans lequel on insère un ID.
	 * @param id
	 *            l'ID inséré.
	 * @return juryBis le jury modifié.
	 */
	private JurySoutenance setJury(JurySoutenance jury, String id) {
		JurySoutenance juryBis = jury;
		Long idLong = Long.parseLong(id);
		if (jury.getIdProf1() == null) {
			juryBis.setIdProf1(idLong);
		} else if (jury.getIdProf2() == null) {
			jury.setIdProf2(idLong);
			juryBis = this.creerJury(jury);
			this.soutenance.setIdJurySoutenance(juryBis.getIdJurySoutenance());
			juryBis = new JurySoutenance();
		}
		return juryBis;
	}
	
	/**
	 * Insère ou Modifie un Jury dans la BDD.
	 * 
	 * @param jury
	 *            le JurySoutenance que l'on souhaite insèrer.
	 * @return juryBis le Jury inséré ou modifié.
	 */
	private JurySoutenance creerJury(JurySoutenance jury) {
		JurySoutenance juryBis;
		if ((jury.getIdJurySoutenance() == null) || (jury.getIdJurySoutenance().contentEquals(""))) {
			this.jurySoutenanceDAO.creer(jury);
			List<JurySoutenance> jurys = this.jurySoutenanceDAO.trouver(jury);
			juryBis = jurys.get(0);
		} else {
			juryBis = jury;
			this.jurySoutenanceDAO.modifier(jury);
		}
		return juryBis;
	}

}