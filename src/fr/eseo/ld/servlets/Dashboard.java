package fr.eseo.ld.servlets;

import fr.eseo.ld.beans.*;
import fr.eseo.ld.dao.*;
import fr.eseo.ld.pgl.beans.BonusMalus;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

/**
 * Servlet permettant l'affichage du Dashboard.
 */
@WebServlet("/Dashboard")
public class Dashboard extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private static Logger logger = Logger.getLogger(Dashboard.class.getName());

	private static final String VUE_FORM = "/WEB-INF/formulaires/dashboard.jsp";

	private static final String ATT_FORMULAIRE = "formulaire";
	private static final String PARAM_VALIDATION_BONUSMALUS = "validationDuBanusMalus";

	private Utilisateur utilisateurGlobal;
	private List<Role> roles;
	private List<OptionESEO> options;
	private List<Utilisateur> utilisateurs;
	private List<AnneeScolaire> annees;
	private List<Equipe> equipes;
	private List<Sujet> sujets;
	private List<Etudiant> etudiants;
	private List<EtudiantEquipe> etusEquipe;

	private SujetDAO sujetDAO;
	private UtilisateurDAO utilisateurDAO;
	private EquipeDAO equipeDAO;
	private OptionESEODAO optionDAO;
	private AnneeScolaireDAO anneeScolaireDAO;
	private EtudiantDAO etudiantDAO;
	private JurySoutenanceDAO jurySoutenanceDAO;
	private JuryPosterDAO juryPosterDAO;
	private SoutenanceDAO soutenanceDAO;
	private NotePosterDAO notePosterDAO;
	private NoteSoutenanceDAO noteSoutenanceDAO;
	private ProfesseurDAO professeurDAO;
	private PosterDAO posterDAO;
	private ProfesseurSujetDAO professeurSujetDAO;
	private PglEtudiantEquipeDAO etudiantEquipeDAO;
	private BonusMalusDAO bonusMalusDAO;
	private DemandeJuryDAO demandeJuryDAO;

	private JuryPoster juryPoster = new JuryPoster();
	Boolean resultatExceptionRemplacerJury = false;
	String idPosterJuryARemplacer;


	@Override
	public void init() throws ServletException {
		/* Récupération d'instances de DAO */
		DAOFactory daoFactory = (DAOFactory) this.getServletContext().getAttribute(ServletUtilitaire.CONF_DAO_FACTORY);
		this.sujetDAO = daoFactory.getSujetDao();
		this.utilisateurDAO = daoFactory.getUtilisateurDao();
		this.equipeDAO = daoFactory.getEquipeDAO();
		this.optionDAO = daoFactory.getOptionESEODAO();
		this.professeurDAO = daoFactory.getProfesseurDAO();
		this.anneeScolaireDAO = daoFactory.getAnneeScolaireDAO();
		this.etudiantDAO = daoFactory.getEtudiantDao();
		this.juryPosterDAO = daoFactory.getJuryPosterDAO();
		this.jurySoutenanceDAO = daoFactory.getJurySoutenanceDAO();
		this.soutenanceDAO = daoFactory.getSoutenanceDAO();
		this.notePosterDAO = daoFactory.getNotePosterDao();
		this.noteSoutenanceDAO = daoFactory.getNoteSoutenanceDAO();
		this.posterDAO = daoFactory.getPosterDao();
		this.professeurSujetDAO = daoFactory.getProfesseurSujetDAO();
		this.etudiantEquipeDAO = daoFactory.getPglEtudiantEquipeDAO();
		this.bonusMalusDAO = daoFactory.getBonusMalusDAO();
		this.demandeJuryDAO = daoFactory.getDemandeJuryDAO();
	}

	@Override
	@SuppressWarnings("unchecked")
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		/* Récupération de la session depuis la requête */
		HttpSession session = request.getSession();

		/* Récupération d'attributs de l'Utilisateur en session */
		this.roles = (ArrayList<Role>) session.getAttribute(ServletUtilitaire.ATT_SESSION_ROLES);
		this.options = (List<OptionESEO>) session.getAttribute(ServletUtilitaire.ATT_SESSION_OPTIONS);
		request.setAttribute(ServletUtilitaire.CHAMP_ETAT_DEPOSE, EtatSujet.DEPOSE);
		
		if (roles != null) { // si l'utilisateurGlobal est connecté, on commence la création du dashboard
			/* Création de la case Mes Sujets */
			List<String> roleString = this.toString(roles);
			this.utilisateurGlobal = (Utilisateur) session.getAttribute(ServletUtilitaire.ATT_SESSION_USER);
			List<Sujet> mesSujets = this.sujetDAO.trouverSujetsPortes(this.utilisateurGlobal);
			request.setAttribute(ServletUtilitaire.ATT_MES_SUJETS, mesSujets);
			/* Création des autres cases en fonction des rôles de l'utilisateurGlobal */
			for (String string : roleString) {
				if (string.contains("etudiant")) {
					this.creationEtudiant(request);
				} else if (string.contains("prof")) {
					this.creationProf(request, string);
				} else if (string.contains("admin")) {
					Utilisateur utilNonValide = new Utilisateur();
					utilNonValide.setValide("non");
					List<Utilisateur> utilsNonValide = this.utilisateurDAO.trouver(utilNonValide);
					request.setAttribute(ServletUtilitaire.ATT_COMPTE_A_VALIDER, utilsNonValide);
				}
			}
			
			accepterRefuserBonusMalus(request);
			bonusMalusAValider(request);
			
			/* Affichage du formulaire */
			this.getServletContext().getRequestDispatcher(VUE_FORM).forward(request, response);
		} else {
			/* Redirection vers l'index si l'utilisateurGlobal n'est pas connecté */
			this.getServletContext().getRequestDispatcher(ServletUtilitaire.VUE_INDEX).forward(request, response);
		}
	}


	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		if ("RemplacerJury".equals(request.getParameter(ATT_FORMULAIRE))){
			try {
				HttpSession session = request.getSession();
				Utilisateur utilisateur = (Utilisateur) session.getAttribute(ServletUtilitaire.ATT_SESSION_USER);
				equipes = equipeDAO.lister();
				idPosterJuryARemplacer = request.getParameter("idPoster");
				for(int i = 0; i < this.juryPosterDAO.lister().size(); i++) {
					String idEquipeJuryPoster = this.juryPosterDAO.lister().get(i).getIdEquipe();
					String idEquipePoster = this.posterDAO.trouverPoster(Long.parseLong(idPosterJuryARemplacer)).getIdEquipe();
					if(idEquipeJuryPoster.equalsIgnoreCase(idEquipePoster)) {
						this.juryPoster = this.juryPosterDAO.lister().get(i);
					}
				}
				remplacerProfesseur(utilisateur);
			} catch (Exception  e) {
				resultatExceptionRemplacerJury = true;
			}
		}
		
		/* Redirection vers la page précédente */
		response.sendRedirect(request.getHeader(ServletUtilitaire.HEADER));
	}


	/**
	 * Crée la case Etudiant en fonction des paramètres récupérés de la requête.
	 *
	 * @param request
	 *            la requête possédant les paramètres à récupérer.
	 */
	private void creationEtudiant(HttpServletRequest request) {
		List<Utilisateur> listeEquipe = new ArrayList<>();
		EtudiantEquipe etudiantEquipe = new EtudiantEquipe();
		etudiantEquipe.setIdEtudiant(this.utilisateurGlobal.getIdUtilisateur());

		List<EtudiantEquipe> test = this.equipeDAO.trouverEtudiantEquipe(etudiantEquipe);
		if (!test.isEmpty()) { // l'étudiant a une équipe
			/* Création de la case Equipe */
			EtudiantEquipe etudiantsEquipe = test.get(0);

			// on modifie l'objet etudiantEquipe en supprimant l'ID de l'étudiant pour
			// récupérer tous les membres de l'équipe
			etudiantsEquipe.setIdEtudiant(null);
			List<EtudiantEquipe> etudiantsEquip = this.equipeDAO.trouverEtudiantEquipe(etudiantsEquipe);
			for (EtudiantEquipe var : etudiantsEquip) {

				// pour chaque membre, on l'ajoute dans la liste qui sera montrée à
				// l'utilisateurGlobal
				Utilisateur partenaire = new Utilisateur();
				partenaire.setIdUtilisateur(var.getIdEtudiant());
				listeEquipe.add(this.utilisateurDAO.trouver(partenaire).get(0));
			}

			/* Création des cases spécifiques à l'étudiant */
			Equipe equipe = new Equipe();
			equipe.setIdEquipe(etudiantsEquipe.getIdEquipe());
			Sujet sujet = new Sujet();
			sujet.setIdSujet(this.equipeDAO.trouver(equipe).get(0).getIdSujet());
			sujet = this.sujetDAO.trouver(sujet).get(0);

			// on cherche l'année scolaire correspondant aux options du sujet pour signaler
			// les prochaines deadlines
			List<OptionESEO> optionsSujet = this.optionDAO.trouverOptionSujet(sujet);
			this.annees = this.anneeScolaireDAO.lister();
			AnneeScolaire annee = selectionAnneeSujet(optionsSujet, this.annees);

			request.setAttribute(ServletUtilitaire.ATT_MON_SUJET, sujet);
			request.setAttribute(ServletUtilitaire.ATT_MON_EQUIPE, listeEquipe);
			request.setAttribute(ServletUtilitaire.ATT_DEADLINE_POSTER, annee.getDateDepotPoster());
			request.setAttribute(ServletUtilitaire.ATT_PROCHAINE_SOUTENANCE, annee.getDateSoutenanceFinale());
		} else { // sinon, on lui propose des sujet qui pourraient l'intéresser
			List<Sujet> sujetsInteressant = getSujetsInteressants();
			request.setAttribute(ServletUtilitaire.ATT_SUJET_INTERRESSANT, sujetsInteressant);
		}
	}

	/**
	 * Crée la case Prof en fonction des paramètres récupérés de la requête.
	 *
	 * @param request
	 *            la requête possédant les paramètres à récupérer.
	 */
	private void creationProf(HttpServletRequest request, String string) {
		/* on créé le bean professeur correspondant à l'utilisateurGlobal */
		Professeur prof = new Professeur();
		prof.setIdProfesseur(this.utilisateurGlobal.getIdUtilisateur());
		List<Sujet> interetSujet = this.sujetDAO.trouverSujetsProfesseur(prof);
		this.annees = this.anneeScolaireDAO.lister();
		request.setAttribute(ServletUtilitaire.ATT_SUJET_SUIVI, interetSujet);

		if (interetSujet.isEmpty()) { // si le prof ne suis aucun sujet
			List<Sujet> sujetsInteressant = getSujetsInteressants();
			request.setAttribute(ServletUtilitaire.ATT_SUJET_INTERRESSANT, sujetsInteressant);
		}

		List<Sujet> sujetsPoster = new ArrayList<>();
		List<Sujet> sujetsSoutenance = new ArrayList<>();
		List<Date> datePoster = new ArrayList<>();
		List<Date> dateSoutenance = new ArrayList<>();
		List<String> manqueNotePoster = new ArrayList<>();
		List<String> manqueNoteSoutenance = new ArrayList<>();
		List<NotePoster> notesPoster = this.notePosterDAO.lister();
		List<NoteSoutenance> notesSoutenance = this.noteSoutenanceDAO.lister();
		this.etusEquipe = this.equipeDAO.listerEtudiantEquipe();
		this.etudiants = this.etudiantDAO.lister();
		this.equipes = this.equipeDAO.lister();
		this.sujets = this.sujetDAO.lister();
		List<JuryPoster> jurysPoster = this.juryPosterDAO.listerJurysUtilisateur(this.utilisateurGlobal);
		for (JuryPoster jury : jurysPoster) {
			if (jury.getIdEquipe().equals(jury.getIdEquipe())) {
				sujetsPoster.add(ajouterSujet(jury.getIdEquipe()));
				datePoster.add(jury.getDate());
				manqueNotePoster.add(manqueNotePoster(jury.getIdEquipe(), prof.getIdProfesseur(), notesPoster));
			}
		}
		List<JurySoutenance> jurysSoutenance = this.jurySoutenanceDAO.listerJurysUtilisateur(this.utilisateurGlobal);
		List<Soutenance> soutenances = this.soutenanceDAO.lister();
		peuplerListeSutenances(prof, sujetsSoutenance, dateSoutenance, manqueNoteSoutenance, notesSoutenance, jurysSoutenance, soutenances);

		request.setAttribute(ServletUtilitaire.ATT_SUJETS_POSTER, sujetsPoster);
		request.setAttribute(ServletUtilitaire.ATT_SUJETS_SOUTENANCE, sujetsSoutenance);
		request.setAttribute(ServletUtilitaire.ATT_DATE_POSTER, datePoster);
		request.setAttribute(ServletUtilitaire.ATT_DATE_SOUTENANCE, dateSoutenance);
		request.setAttribute(ServletUtilitaire.ATT_MANQUE_NOTE_POSTER, manqueNotePoster);
		request.setAttribute(ServletUtilitaire.ATT_MANQUE_NOTE_SOUTENANCE, manqueNoteSoutenance);

		// Si le prof est prof d'option, alors on charge les objets du dashboard
		// spécifiques au prof d'option et donc responsable aussi
		chargerContenuProfRespOption(request, string, prof);
		if (string.contains("Responsable")) { // si le prof est responsable
			List<Sujet> sujetValidation = new ArrayList<>();
			List<Sujet> sujetPublication = new ArrayList<>();
			List<String[]> equipeValidation = new ArrayList<>();
			List<String[]> professeurValidation = new ArrayList<>();
			this.utilisateurs = utilisateurDAO.listerProfesseur();
			chargerElementsProfesseur(sujetValidation, sujetPublication, equipeValidation, professeurValidation);
			request.setAttribute(ServletUtilitaire.ATT_SUJET_A_VALIDER, sujetValidation);
			request.setAttribute(ServletUtilitaire.ATT_SUJET_A_PUBLIER, sujetPublication);
			request.setAttribute(ServletUtilitaire.ATT_EQUIPE_A_VALIDER, equipeValidation);
			request.setAttribute(ServletUtilitaire.ATT_PROF_A_VALIDER, professeurValidation);
		}
		
		/* Recherche de la liste des sujets de l'option du ProfesseurResponsable */
		String optionPrincipale = "";
		optionPrincipale = getOptionPrincipale(optionPrincipale);

		/* Récupération d'attributs d'une demande de jury dans la BDD */
		DemandeJury demandeJuryBDD;
		ProfesseurDemande professeurDemandeBDD;
		demandeJuryBDD = this.demandeJuryDAO.trouver(optionPrincipale);
		if (demandeJuryBDD != null) {
			professeurDemandeBDD = this.demandeJuryDAO.trouverProfesseurDemande(String.valueOf(prof.getIdProfesseur()), String.valueOf(demandeJuryBDD.getIdDemande()));
			if(professeurDemandeBDD != null) {
				/* Récupération des sujets et jurys à afficher pour la demande d'inscription */
				List<Sujet> sujetsChoisis = new ArrayList<>();
				List<Soutenance> soutenancesChoisies = new ArrayList<>();
				List<Equipe> equipesChoisies = new ArrayList<>();
				List<JurySoutenance> jurysChoisis = new ArrayList<>();
				List<Utilisateur> professeursChoisis1 = new ArrayList<>();
				List<Utilisateur> professeursChoisis2 = new ArrayList<>();
				
				Sujet sujetChoisi;
				Soutenance soutenanceChoisie;
				Equipe equipeChoisie;
				JurySoutenance jury;
				
				/* Récupération des sujets */
				String sujetsChoisisString;
				sujetsChoisisString = demandeJuryBDD.getSujetsConcernes();
				for (String ligne : sujetsChoisisString.split(",")) {
					Long idSujetChoisi = null;
					
					try {
						idSujetChoisi = Long.valueOf(ligne);
					} catch (NumberFormatException e) {
						logger.log(Level.WARN, "Erreur de format pour l'id du sujet", e);
					}
					
					sujetChoisi = this.sujetDAO.trouver(idSujetChoisi);
					soutenanceChoisie = this.soutenanceDAO.trouverSoutenanceSujet(sujetChoisi);
					equipeChoisie = this.equipeDAO.listerEquipeSujet(idSujetChoisi).get(0);
					jury = this.jurySoutenanceDAO.trouverJurySoutenance(soutenanceChoisie);
					
					sujetsChoisis.add(sujetChoisi);
					soutenancesChoisies.add(soutenanceChoisie);
					equipesChoisies.add(equipeChoisie);
					jurysChoisis.add(jury);
					
				}
				
				/* Récupération des jurys */
				recuperationJury(jurysChoisis, professeursChoisis1, professeursChoisis2);

				/* Récupération du commentaire */
				String commentaire;
				commentaire = demandeJuryBDD.getCommentaire();
				
				request.setAttribute(ServletUtilitaire.ATT_LISTE_SUJETS_CHOISIS, sujetsChoisis);
				request.setAttribute(ServletUtilitaire.ATT_LISTE_SOUTENANCES_CHOISIES, soutenancesChoisies);
				request.setAttribute(ServletUtilitaire.ATT_LISTE_EQUIPES_CHOISIES, equipesChoisies);
				request.setAttribute(ServletUtilitaire.ATT_LISTE_JURYS_CHOISIS, jurysChoisis);
				request.setAttribute(ServletUtilitaire.ATT_LISTE_PROFESSEURS_CHOISIS1, professeursChoisis1);
				request.setAttribute(ServletUtilitaire.ATT_LISTE_PROFESSEURS_CHOISIS2, professeursChoisis2);
				request.setAttribute(ServletUtilitaire.CHAMP_COMMENTAIRE, commentaire);
				request.setAttribute("profDemandeJury", professeurDemandeBDD);
			}
		}
	}

	private void chargerContenuProfRespOption(HttpServletRequest request, String string, Professeur prof) {
		if (string.contains("Option") || (string.contains("Responsable"))) {
			List<Sujet> sujetsReferent = this.sujetDAO.trouverSujetsProfesseurReferent(prof);
			List<String> manqueNote = new ArrayList<>();
			for (Sujet sujet : sujetsReferent) {
				manqueNote.add(manqueNoteEquipe(sujet));
			}
			request.setAttribute(ServletUtilitaire.ATT_MANQUE_NOTE_SUIVI, manqueNote);
			request.setAttribute(ServletUtilitaire.ATT_SUJETS_REFERENT, sujetsReferent);
		}
	}

	private String getOptionPrincipale(String optionPrincipale) {
		for (int i = 0; i < roles.size(); i++) {
			if (roles.get(i).getNomRole().contentEquals("prof")) {
				optionPrincipale = options.get(i).getNomOption();
			}
		}
		return optionPrincipale;
	}

	private void recuperationJury(List<JurySoutenance> jurysChoisis, List<Utilisateur> professeursChoisis1, List<Utilisateur> professeursChoisis2) {
		for(JurySoutenance ju :jurysChoisis) {
			Utilisateur prof1 =new Utilisateur();
			Utilisateur prof2 =new Utilisateur();
			if(ju.getIdProf1() == null || ju.getIdProf1() == 0L) {
				professeursChoisis1.add(null);
			}
			else {
				prof1.setIdUtilisateur(ju.getIdProf1());
				prof1 = this.utilisateurDAO.trouver(prof1).get(0);
				professeursChoisis1.add(prof1);
			}

			if(ju.getIdProf2() == null || ju.getIdProf2() == 0L) {
				professeursChoisis2.add(null);
			}
			else {
				prof2.setIdUtilisateur(ju.getIdProf2());
				prof2 = this.utilisateurDAO.trouver(prof2).get(0);
				professeursChoisis2.add(prof2);
			}
		}
	}

	private void peuplerListeSutenances(Professeur prof, List<Sujet> sujetsSoutenance, List<Date> dateSoutenance, List<String> manqueNoteSoutenance, List<NoteSoutenance> notesSoutenance, List<JurySoutenance> jurysSoutenance, List<Soutenance> soutenances) {
		for (JurySoutenance jury : jurysSoutenance) {
			for (Soutenance soutenance : soutenances) {
				if (soutenance.getIdJurySoutenance() == null || soutenance.getDateSoutenance() == null) {
					continue;
				}
				if (soutenance.getIdJurySoutenance().equals(jury.getIdJurySoutenance())) {
					sujetsSoutenance.add(ajouterSujet(soutenance.getIdEquipe()));
					dateSoutenance.add(soutenance.getDateSoutenance());
					manqueNoteSoutenance.add(
							manqueNoteSoutenance(soutenance.getIdEquipe(), prof.getIdProfesseur(), notesSoutenance));
				}
			}
		}
	}

	private void chargerElementsProfesseur(List<Sujet> sujetValidation, List<Sujet> sujetPublication, List<String[]> equipeValidation, List<String[]> professeurValidation) {
		for (Sujet varSujet : sujets) { // on charge des éléments spécifiques à son rôle
			if (varSujet.getEtat().equals(EtatSujet.DEPOSE))
				sujetValidation.add(varSujet);
			if (varSujet.getEtat().equals(EtatSujet.VALIDE))
				sujetPublication.add(varSujet);
		}
		for (Equipe equipe : this.equipes) {
			if ("non".equals(equipe.getValide())) {
				Sujet sujet = this.ajouterSujet(equipe.getIdEquipe());
				String[] attributs = new String[] { sujet.getTitre(), String.valueOf(sujet.getNbrMinEleves()),
						String.valueOf(sujet.getNbrMaxEleves()), String.valueOf(equipe.getTaille()) };
				equipeValidation.add(attributs);
			}
		}
		List<ProfesseurSujet> professeursSujet = this.utilisateurDAO
				.listerProfesseursSujetsFonction(FonctionProfesseurSujet.REFERENT.toString());
		for (ProfesseurSujet profSujet : professeursSujet) {
			if ("non".equals(profSujet.getValide())) {
				Utilisateur utilisateurProf = this.ajouterProf(profSujet.getIdProfesseur());
				String[] attributs = new String[] { utilisateurProf.getNom(), utilisateurProf.getPrenom(),
						this.ajouterSujet(profSujet.getIdSujet()).getTitre(), profSujet.getFonction().toString() };
				professeurValidation.add(attributs);
			}
		}
	}

	private List<Sujet> getSujetsInteressants() {
		List<Sujet> sujetsInteressant = new ArrayList<>();
		for (OptionESEO option : this.options) {
			List<Sujet> sujetTrouve = this.sujetDAO.trouverSujetOption(option);
			for (Sujet suj : sujetTrouve) {
				sujetsInteressant.add(suj); // on lui en propose
			}
		}
		return sujetsInteressant;
	}

	/**
	 * Méthode qui retourne le prof via son id
	 *
	 * @param idProfesseur
	 *            l'id du prof
	 * @return utilisateurRetour le prof correspondant
	 */
	private Utilisateur ajouterProf(Long idProfesseur) {
		Utilisateur utilisateurRetour = new Utilisateur();
		for (Utilisateur util : this.utilisateurs) {
			if (util.getIdUtilisateur().compareTo(idProfesseur) == 0) {
				utilisateurRetour = util;
				break;
			}
		}
		return utilisateurRetour;
	}

	/**
	 * Méthode qui retourne le sujet via son id
	 *
	 * @param idSujet
	 *            l'id du sujet
	 * @return sujet le sujet correspondant
	 */
	private Sujet ajouterSujet(Long idSujet) {
		Sujet sujet = new Sujet();
		for (Sujet suj : this.sujets) {
			if (suj.getIdSujet().compareTo(idSujet) == 0) {
				sujet = suj;
				break;
			}
		}
		return sujet;
	}

	/**
	 * Méthode qui retourne le sujet sur le quel une équipe travail
	 *
	 * @param idEquipe
	 *            l'id de l'équipe
	 * @return sujet le sujet correspondant
	 */
	private Sujet ajouterSujet(String idEquipe) {
		Sujet sujet = new Sujet();
		Equipe equipe = trouverEquipe(idEquipe);
		for (Sujet suj : this.sujets) {
			if (suj.getIdSujet().compareTo(equipe.getIdSujet()) == 0) {
				sujet = suj;
				break;
			}
		}
		return sujet;
	}

	/**
	 * Méthode qui renvoie l'équipe correspondante à l'id
	 *
	 * @param idEquipe
	 *            l'id de l'équipe
	 * @return equipe l'équipe correspondante
	 */
	private Equipe trouverEquipe(String idEquipe) {
		Equipe equipe = new Equipe();
		for (Equipe equip : this.equipes) {
			if (equip.getIdEquipe().equals(idEquipe)) {
				equipe = equip;
				break;
			}
		}
		return equipe;
	}

	/**
	 * Méthode qui permet de savoir si un prof a noté ou non une équipe pour leur
	 * poster
	 *
	 * @param idEquipe
	 *            l'id de l'équipe a noter
	 * @param idProfesseur
	 *            l'id du professeur qui doit noter
	 * @return resultat null s'il a noté sinon un string
	 */
	private String manqueNotePoster(String idEquipe, Long idProfesseur, List<NotePoster> notes) {
		Equipe equipe = trouverEquipe(idEquipe);
		Integer compteur = 0;
		for (NotePoster note : notes) {
			if (note.getIdProfesseur().compareTo(idProfesseur) == 0) {
				compteur++;
			}
		}
		if (compteur.compareTo(equipe.getTaille()) == 0) {
			return null;
		}
		return "non";
	}

	/**
	 * Méthode qui permet de savoir si un prof a noté ou non une équipe pour leur
	 * soutenance
	 *
	 * @param idEquipe
	 *            l'id de l'équipe a noter
	 * @param idProfesseur
	 *            l'id du professeur qui doit noter
	 * @return resultat null s'il a noté sinon un string
	 */
	private String manqueNoteSoutenance(String idEquipe, Long idProfesseur, List<NoteSoutenance> notes) {
		Equipe equipe = trouverEquipe(idEquipe);
		Integer compteur = 0;
		for (NoteSoutenance note : notes) {
			if (note.getIdProfesseur().compareTo(idProfesseur) == 0) {
				compteur++;
			}
		}
		if (compteur.compareTo(equipe.getTaille()) == 0) {
			return null;
		}
		return "non";
	}

	/**
	 * Methode qui renvoie le type de note manquante
	 *
	 * @param sujet
	 *            le sujet dont on veut savoir s'il manque des notes
	 * @return toutes les notes, juste la note intermédiaire ou juste la note de
	 *         projet
	 */
	private String manqueNoteEquipe(Sujet sujet) {
		boolean inter = false;
		boolean projet = false;
		List<Equipe> equipesSujet = this.equipeDAO.listerEquipeSujet(sujet.getIdSujet());
		Equipe equipe = null;
		/* Récupération de l'équipe de l'année en cours */
		for (Equipe equip : equipesSujet) {
			if (equip.getAnnee().compareTo(this.annees.get(0).getAnneeDebut()) == 0) {
				equipe = equip;
				break;
			}
		}
		/* Récupération des deadlines pour l'attribution des notes */
		Date dateIntermediaire = this.annees.get(0).getDateMiAvancement();
		Date dateProjet = this.annees.get(0).getDateSoutenanceFinale();
		recuperationDeadline(dateIntermediaire, dateProjet);

		if (equipe != null) {
			/* récupération des étudiants de l'équipe */
			List<Etudiant> etudiantsEquipe = new ArrayList<>();
			recupererEtudiantsEquipe(equipe, etudiantsEquipe);
			for (Etudiant etudiant : etudiantsEquipe) {
				if (etudiant.getNoteIntermediaire() < 0) {
					inter = true;
				}
				if (etudiant.getNoteProjet() < 0) {
					projet = true;
				}
			}
		}

		/*
		 * si la date de dépot de note est passé et que les étudiants n'ont pas la note
		 * on le signale
		 */
		return getNoteManquanteType(inter, projet, equipe, dateIntermediaire, dateProjet);
	}

	private String getNoteManquanteType(boolean inter, boolean projet, Equipe equipe, Date dateIntermediaire, Date dateProjet) {
		if (inter && projet && dateIntermediaire.before(new Date()) && dateProjet.before(new Date())) {
			return "toutes";
		} else if (inter && dateIntermediaire.before(new Date())) {
			return "intermediaire";
		} else if (projet && dateProjet.before(new Date())) {
			return "projet";
		} else if ((dateIntermediaire.after(new Date()) && dateProjet.after(new Date())) || equipe == null) {
			return "rien";
		} else {
			return null;
		}
	}

	private void recuperationDeadline(Date dateIntermediaire, Date dateProjet) {
		for (AnneeScolaire annee : this.annees) {
			if (dateIntermediaire.after(annee.getDateMiAvancement())) {
				dateIntermediaire.setTime(annee.getDateMiAvancement().getTime());
			}
			if (dateProjet.after(annee.getDateSoutenanceFinale())) {
				dateProjet.setTime(annee.getDateSoutenanceFinale().getTime());
			}
		}
	}

	private void recupererEtudiantsEquipe(Equipe equipe, List<Etudiant> etudiantsEquipe) {
		for (EtudiantEquipe etuEquipe : this.etusEquipe) {
			if (etuEquipe.getIdEquipe().equals(equipe.getIdEquipe())) {
				etudiantsEquipe.add(ajouterEtudiant(etuEquipe.getIdEtudiant(), this.etudiants));
			}
		}
	}

	/**
	 * Méthode qui renvoie l'étudiant correspondant à l'id qu'on lui entre en param
	 *
	 * @param idEtudiant
	 *            l'id de l'étudiant que l'on veut
	 * @param etudiants
	 *            la liste de tout les etudiants
	 * @return l'étudiant correspondant
	 */
	private Etudiant ajouterEtudiant(Long idEtudiant, List<Etudiant> etudiants) {
		Etudiant etudiant = new Etudiant();
		for (Etudiant etu : etudiants) {
			if (etu.getIdEtudiant().compareTo(idEtudiant) == 0) {
				etudiant = etu;
				break;
			}
		}
		return etudiant;
	}

	/**
	 * Convertit une liste de Roles en une liste de Strings.
	 *
	 * @param roles
	 *            la liste des Roles à convertir en Strings.
	 * @return rolesString la liste des Roles convertis en Strings.
	 */
	private List<String> toString(List<Role> roles) {
		List<String> rolesString = new ArrayList<>();
		for (Role role : roles) {
			rolesString.add(role.getNomRole());
		}
		return rolesString;
	}

	/**
	 * Sélectionne l'année de deadline en fonction des options du sujet.
	 *
	 * @param options
	 *            la liste des options concernées par le sujet.
	 * @param annees
	 *            la liste des années scolaires dans la BDD.
	 *
	 * @return annee l'année scolaire sélectionnée.
	 */
	private AnneeScolaire selectionAnneeSujet(List<OptionESEO> options, List<AnneeScolaire> annees) {
		AnneeScolaire annee = new AnneeScolaire();
		List<AnneeScolaire> anneeSujets = new ArrayList<>();
		for (OptionESEO option : options) {
			for (AnneeScolaire anneeSujet : annees) {
				if (option.getIdOption().compareTo(anneeSujet.getIdOption()) == 0) {
					anneeSujets.add(anneeSujet);
				}
			}
		}
		for (AnneeScolaire anneeSujet : anneeSujets) {
			annee = getAnneeScolaireSujet(annee, anneeSujet);
		}
		return annee;
	}

	private AnneeScolaire getAnneeScolaireSujet(AnneeScolaire annee, AnneeScolaire anneeSujet) {
		if (annee.getDateDebutProjet() == null) {
			annee = anneeSujet;
		} else {
			if (anneeSujet.getDateDepotPoster().before(annee.getDateDepotPoster())) {
				annee.setDateDepotPoster(anneeSujet.getDateDepotPoster());
			}
			if (anneeSujet.getDateSoutenanceFinale().before(annee.getDateSoutenanceFinale())) {
				annee.setDateSoutenanceFinale(anneeSujet.getDateSoutenanceFinale());
			}
		}
		return annee;
	}

	/**
	 * Sélectionne un professeur aléatoire parmi tous les professeurs disponibles. 
	 *
	 * @param listerProfesseursDisponibles
	 *
	 * @return Professeur
	 *
	 */
	private Professeur professeurRandom(List<Utilisateur> listerProfesseursDisponibles) {
		Random r = new Random();
		Long idProf = null;
		Professeur prof = new Professeur();

		int random = r.nextInt(listerProfesseursDisponibles.size());
		idProf = listerProfesseursDisponibles.get(random).getIdUtilisateur();
		prof.setIdProfesseur(idProf);

		return prof;
	}
	/**
	 * Compare 3 proffesseur pour savoir si ils sont différents
	 *
	 * @param prof1, prof2, prof3
	 *
	 * @return true si ils sont différents, false sinon.
	 *
	 */
	private boolean testTrinome(Professeur prof1, Professeur prof2, Professeur prof3) {
		return prof1.getIdProfesseur() != prof2.getIdProfesseur()
				&& prof1.getIdProfesseur() != prof3.getIdProfesseur()
				&& prof2.getIdProfesseur() != prof3.getIdProfesseur();
	}
	/**
	 * Vérification si le proffesseur peut être Jury du Poster.
	 *
	 * @param prof Professeur
	 * @param poster POster
	 *
	 * @return renvoie true si il n'encadre pas le poster et false sinon.
	 */
	private boolean testProfPeutEtreJury(Professeur prof, Poster poster) {
		for(int i = 0; i < this.professeurSujetDAO.lister().size(); i++) {
			if(this.professeurSujetDAO.lister().get(i).getIdProfesseur() == prof.getIdProfesseur()
					&& this.professeurSujetDAO.lister().get(i).getIdSujet() == poster.getIdSujet()) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Si un professeur n'est pas disponible
	 * Remplace par un autre professeur
	 *
	 * @param utilisateur
	 *
	 */
	private void remplacerProfesseur(Utilisateur utilisateur) {
		List<Utilisateur> listerProfesseursDisponibles = this.professeurDAO.listerProfesseursDisponibles();
		Professeur profRemplace;
		Professeur prof1 = new Professeur();
		Professeur prof2 = new Professeur();
		Professeur prof3 = new Professeur();
		Poster poster;
		String valeurProf = null;

		profRemplace = professeurRandom(listerProfesseursDisponibles);
		poster = this.posterDAO.trouverPoster(Long.parseLong(this.idPosterJuryARemplacer));

		while(!testProfPeutEtreJury(profRemplace, poster)){
			profRemplace = professeurRandom(listerProfesseursDisponibles);
		}
		if(this.juryPoster.getIdProf1() == utilisateur.getIdUtilisateur()) {
			this.juryPoster.setIdProf1(profRemplace.getIdProfesseur());
			valeurProf = "Prof1";
		}
		if(this.juryPoster.getIdProf2() == utilisateur.getIdUtilisateur()) {
			this.juryPoster.setIdProf2(profRemplace.getIdProfesseur());
			valeurProf = "Prof2";
		}
		if(this.juryPoster.getIdProf3() == utilisateur.getIdUtilisateur()) {
			this.juryPoster.setIdProf3(profRemplace.getIdProfesseur());
			valeurProf = "Prof3";
		}

		for(int i = 0 ; i < this.professeurDAO.lister().size(); i++) {
			prof1 = tryToAddProfesseur(prof1, i, this.juryPoster.getIdProf1());
			prof2 = tryToAddProfesseur(prof2, i, this.juryPoster.getIdProf2());
			prof3 = tryToAddProfesseur(prof3, i, this.juryPoster.getIdProf3());
		}


		if(valeurProf.equals("Prof1")) {
			profRemplace = addProfSiPeutEtreJury(listerProfesseursDisponibles, profRemplace, poster, testTrinome(profRemplace, prof2, prof3));
			this.juryPoster.setIdProf1(profRemplace.getIdProfesseur());
		}
		if(valeurProf.equals("Prof2")) {
			profRemplace = addProfSiPeutEtreJury(listerProfesseursDisponibles, profRemplace, poster, testTrinome(prof1, profRemplace, prof3));
			this.juryPoster.setIdProf2(profRemplace.getIdProfesseur());
		}
		if(valeurProf.equals("Prof3")) {
			profRemplace = addProfSiPeutEtreJury(listerProfesseursDisponibles, profRemplace, poster, testTrinome(prof1, prof2, profRemplace));
			this.juryPoster.setIdProf3(profRemplace.getIdProfesseur());
		}

		this.juryPosterDAO.modifier(juryPoster);
	}

	private Professeur addProfSiPeutEtreJury(List<Utilisateur> listerProfesseursDisponibles, Professeur profRemplace, Poster poster, boolean b) {
		while (!testProfPeutEtreJury(profRemplace, poster) && !b) {
			profRemplace = professeurRandom(listerProfesseursDisponibles);
		}
		return profRemplace;
	}

	private Professeur tryToAddProfesseur(Professeur prof, int i, Long idProf) {
		if (idProf == this.professeurDAO.lister().get(i).getIdProfesseur()) {
			return this.professeurDAO.lister().get(i);
		}
		return prof;
	}

	private void bonusMalusAValider(HttpServletRequest request) {
		
		BonusMalus bonusMalusDeEtudiantConnecte;
		Boolean bonusMalusPresent;
		
		List<fr.eseo.ld.pgl.beans.EtudiantEquipe> etudiantEquipe = this.etudiantEquipeDAO.lister();
		List<BonusMalus> bonusMalus = this.bonusMalusDAO.lister();
		
		for(int i = 0; i < etudiantEquipe.size(); i++) {
			for(int j = 0; j < bonusMalus.size(); j++) {
				if(this.utilisateurGlobal.getIdUtilisateur() == etudiantEquipe.get(i).getIdEtudiant()
						&& this.utilisateurGlobal.getIdUtilisateur() == bonusMalus.get(j).getRefEtudiant()
						&& bonusMalus.get(j).getValidation() == "ATTENTE") {
					
					bonusMalusDeEtudiantConnecte = bonusMalus.get(j);
					bonusMalusPresent = true;
					
					request.setAttribute("bonusMalusPresent", bonusMalusPresent);
					request.setAttribute("justificationBonusMalus", bonusMalusDeEtudiantConnecte.getJustification());
					request.setAttribute("valeurBonusMalus", Math.round(bonusMalusDeEtudiantConnecte.getValeur()));
				}
			}
		}		
	}
	
	private void accepterRefuserBonusMalus(HttpServletRequest request) {
		
		BonusMalus bonusMalusModfier;
		
		List<fr.eseo.ld.pgl.beans.EtudiantEquipe> etudiantEquipe = this.etudiantEquipeDAO.lister();
		List<BonusMalus> bonusMalus = this.bonusMalusDAO.lister();
		for(int i = 0; i < etudiantEquipe.size(); i++) {
			for(int j = 0; j < bonusMalus.size(); j++) {
				if(this.utilisateurGlobal.getIdUtilisateur() == etudiantEquipe.get(i).getIdEtudiant()
						&& this.utilisateurGlobal.getIdUtilisateur() == bonusMalus.get(j).getRefEtudiant()) {
					
					bonusMalusModfier = bonusMalus.get(j);
					modifierStatutBonusMalus(request, bonusMalusModfier);
				}
			}
		}		
	}

	private void modifierStatutBonusMalus(HttpServletRequest request, BonusMalus bonusMalusModfier) {
		if(request.getParameter(PARAM_VALIDATION_BONUSMALUS) != null ) {
			if(request.getParameter(PARAM_VALIDATION_BONUSMALUS).equals("ACCEPTER")) {
				bonusMalusModfier.setValidation("ACCEPTER");
				this.bonusMalusDAO.modifier(bonusMalusModfier);
			}
			if(request.getParameter(PARAM_VALIDATION_BONUSMALUS).equals("REFUSER")) {
				bonusMalusModfier.setValidation("REFUSER");
				this.bonusMalusDAO.modifier(bonusMalusModfier);
			}
		}
	}
}