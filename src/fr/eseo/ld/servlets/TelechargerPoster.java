package fr.eseo.ld.servlets;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.eseo.ld.beans.Poster;
import fr.eseo.ld.dao.DAOFactory;
import fr.eseo.ld.dao.PosterDAO;

/**
 * Servlet permettant le téléchargement d'un poster.
 * 
 * @author Alexandre CLAMENS
 */
@WebServlet("/TelechargerPoster")
public class TelechargerPoster extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public static final int TAILLE_TAMPON = 10240; // 10 ko

	private PosterDAO posterDAO;

	@Override
	public void init() throws ServletException {
		/* Récupération d'instances de DAO */
		DAOFactory daoFactory = (DAOFactory) this.getServletContext().getAttribute(ServletUtilitaire.CONF_DAO_FACTORY);
		this.posterDAO = daoFactory.getPosterDao();
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		/* Récupération de l'ID du sujet concerné par le poster */

		Long idSujet = Long.parseLong(request.getParameter(ServletUtilitaire.CHAMP_ID_SUJET));
		String action = request.getParameter("action");

		/* Formatage de la response */
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		Poster posterExistant = this.posterDAO.trouverPoster(idSujet);
		String nomPoster = this.getNomPoster(posterExistant);
		String cheminPoster = posterExistant.getChemin();
		if ("afficher".equals(action)) {
			response.setContentType("APPLICATION/PDF");
			response.setHeader("Content-Disposition", "inline; filename=\"" + cheminPoster + "\"");
		} else {
			response.setContentType("APPLICATION/octet_stream");
			response.setHeader("Content-Disposition", "inline; filename=\"" + nomPoster + "\"");
		}

		/* Téléchargement du poster */
		try(FileInputStream fileInputStream = new FileInputStream(cheminPoster)){
		int i;
		while ((i = fileInputStream.read()) != -1) {
			out.write(i);
		}
		
		fileInputStream.close();
		out.close();
		}
	}

	/**
	 * Méthode utilitaire qui a pour unique but d'analyser l'en-tête
	 * "content-disposition", et de vérifier si le paramètre "filename" y est
	 * présent. Si oui, alors le champ traité est de type File et la méthode
	 * retourne son nom, sinon il s'agit d'un champ de formulaire classique et la
	 * méthode retourne null.
	 * 
	 * @param poster
	 *            le poster dont on souhaite récupérer le nom.
	 * @return nomPoster le nom du poster.
	 */
	public String getNomPoster(Poster poster) {
		String nomPoster = poster.getChemin();
		nomPoster = nomPoster.substring(nomPoster.lastIndexOf('/') + 1).substring(nomPoster.lastIndexOf('\\') + 1);
		return nomPoster;
	}

}