package fr.eseo.ld.servlets;

import fr.eseo.ld.beans.*;
import fr.eseo.ld.dao.*;
import fr.eseo.ld.pgl.beans.*;
import fr.eseo.ld.pgl.beans.Equipe;
import fr.eseo.ld.pgl.beans.EtudiantEquipe;
import fr.eseo.ld.utils.CallbackUtilitaire;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Servlet permettant la création et la gestion d'un équipe I2.
 */
@WebServlet("/CreerEquipesI2")
public class CreerEquipesI2 extends HttpServlet {
    private static final long serialVersionUID = 1L;

    private static final String VUE_FORM = "/WEB-INF/formulaires/creerEquipesI2.jsp";
    public static final String CHECKBOX = "checkbox-";
    public static final String ECHEC = "Échec de la création de l'objet.";
    public static final String SPRINT_SELECT = "sprintSelect";
    public static final String PROF = "prof";
    public static final String MESSAGE_ERREUR = "Erreur: Selection de l'équipe.";
    public static final String PATTERN = "yyyy-MM-dd";
    public static final String STRING_EXCEPTION = "• Pas de sprint en cours donc affichage du 1er sprint •";
    public static final String SUCCES_PO = "Ajout de(s) Product Owner(s) à l'équipe.";
    public static final String ERREUR_PO = "Echec: Ajout de(s) Product Owner(s) à l'équipe.";
    public static final String ERREUR_EQUIPE = "L'équipe n'a pas été ajouté.";
    public static final String SUCCES_DELETE = "L'équipe a été supprimée.";
    public static final String ERROR_DELETE = "L'équipe n'a pas été supprimée.";
    public static final String SUCCES_AJOUTER_ETU = "Ajout de(s) étudiant(s) à l'équipe.";
    public static final String ECHEC_AJOUT_ETU = "Echec de l'jout de(s) étudiant(s) à l'équipe.";
    public static final String SUCCES_SUPP_ETU = "Suppression de(s) étudiant(s) à l'équipe.";
    public static final String WARNING_TRANSFERT = "L'étudiant est déjà affecté a cette équipe pour ce sprint.";
    public static final String SUCCES_TRANSFERT = "L'étudiant à été affecté à l'équipe choisie pour le sprint selectionné.";
    public static final String PRODUCT_OWNER = "productOwner";
    public static final String PRODUCT_OWNER_PRESENT_DANS_L_EQUIPE = "Product Owner déjà présent dans l'équipe.";

    private static Logger logger = Logger.getLogger(CreerEquipe.class.getName());


    private UtilisateurDAO utilisateurDAO;
    private AnneeScolaireDAO anneeScolaireDAO;
    private RoleDAO roleDAO;
    /* Récupération DAO PGL */
    private PglEquipeDAO pglEquipeDAO;
    private PglEtudiantEquipeDAO pglEtudiantEquipeDAO;
    private PglProjetDAO pglProjetDAO;
    private SprintDAO sprintDAO;
    private PglRoleEquipeDAO pglRoleEquipeDAO;
    private OptionESEODAO optionESEODAO;
    private BonusMalusDAO bonusMalusDAO;

    @Override
    public void init() throws ServletException {
        /* Récupération d'instances de DAO */
        DAOFactory daoFactory = (DAOFactory) this.getServletContext().getAttribute(ServletUtilitaire.CONF_DAO_FACTORY);
        this.utilisateurDAO = daoFactory.getUtilisateurDao();
        this.anneeScolaireDAO = daoFactory.getAnneeScolaireDAO();
        this.pglEquipeDAO = daoFactory.getPglEquipeDAO();
        this.pglEtudiantEquipeDAO = daoFactory.getPglEtudiantEquipeDAO();
        this.pglProjetDAO = daoFactory.getPglProjetDAO();
        this.sprintDAO = daoFactory.getSprintDAO();
        this.pglRoleEquipeDAO = daoFactory.getPglRoleEquipeDAO();
        this.roleDAO = daoFactory.getRoleDAO();
        this.optionESEODAO = daoFactory.getOptionESEODAO();
        this.bonusMalusDAO = daoFactory.getBonusMalusDAO();
    }

    /**
     * Méthode doGet
     *
     * @param request
     * @param response
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        /* Liste des étudiants I2 disponibles pour créer une équipe */
        List<Utilisateur> listeEtudiantsI2Dispos = listerUtilisateursI2Dispo();

        /* Liste des étudiants I2 disponibles pour créer une équipe */
        List<Utilisateur> listeEtudiantsI2DisposAll = getUtilisateursI2();

        /* Liste des Professeurs disponibles */
        Role role = new Role();
        role.setNomRole(PROF);
        Role role1 = this.roleDAO.trouver(role).get(0);
        List<Utilisateur> listeProfesseurs = trouverUtilisateursByIdRole(role1);

        // Remplissage de la liste de toutes les compositions de Equipes
        List<fr.eseo.ld.pgl.beans.EtudiantEquipe> listeCompositionEquipe;
        fr.eseo.ld.pgl.beans.EtudiantEquipe etudiantEquipeTest = new fr.eseo.ld.pgl.beans.EtudiantEquipe();


        Date actuelle = new Date();
        DateFormat dateFormat = new SimpleDateFormat(PATTERN);
        String stringDateActuelle = dateFormat.format(actuelle);

        /* On affiche les membres de l'équipe en fonction du sprint actuel
         */
        List<Sprint> listeSprint = this.sprintDAO.lister();
        boolean premier = false;

        try {
            Date dateDateActuelle = dateFormat.parse(stringDateActuelle);
            for (Sprint sprint : listeSprint) {
                Date dateDebutSprint = dateFormat.parse(sprint.getDateDebut());
                Date dateFinSprint = dateFormat.parse(sprint.getDateFin());
                if (!premier) {
                    if ((dateDateActuelle.after(dateDebutSprint) && dateDateActuelle.before(dateFinSprint))) {
                        etudiantEquipeTest.setIdSprint(sprint.getIdSprint());
                        Integer sprintActuel = sprint.getNumero();
                        listeCompositionEquipe = this.pglEtudiantEquipeDAO.trouver(etudiantEquipeTest);
                        request.setAttribute(ServletUtilitaire.ATT_EQUIPES_I2_LISTE_COMPO_EQUIPE, listeCompositionEquipe);
                        request.setAttribute(ServletUtilitaire.ATT_EQUIPES_I2_LISTE_SPRINT_ACTUEL, sprintActuel);
                        premier = true;
                    } else {
                        etudiantEquipeTest.setIdSprint(1L);
                        listeCompositionEquipe = this.pglEtudiantEquipeDAO.trouver(etudiantEquipeTest);
                        request.setAttribute(ServletUtilitaire.ATT_EQUIPES_I2_LISTE_COMPO_EQUIPE, listeCompositionEquipe);
                        request.setAttribute(ServletUtilitaire.ATT_EQUIPES_I2_LISTE_SPRINT_ACTUEL, STRING_EXCEPTION);
                    }
                }
            }
        } catch (ParseException e) {
            logger.log(Level.WARN, "Échec.", e);
        }

        List<fr.eseo.ld.pgl.beans.Equipe> listeEquipe = new ArrayList<>();
        // Remplissage de la liste des Soutenance de sprint Review
        for (int i = 0; i < this.pglEquipeDAO.lister().size(); i++) {
            listeEquipe.add(this.pglEquipeDAO.lister().get(i));

        }
        // Remplissage de la liste de tous les utilisateurs
        List<Utilisateur> listeUtilisateur;
        listeUtilisateur = this.utilisateurDAO.lister();

        //Remplissage de la liste des années
        List<AnneeScolaire> listeAnneeScolaire;
        AnneeScolaire anneeScolaire = new AnneeScolaire();
        anneeScolaire.setIdOption(1L);

        listeAnneeScolaire = this.anneeScolaireDAO.trouver(anneeScolaire);

        // Remplissage des P.O des équipes
        RoleEquipe roleEquipe = new RoleEquipe();
        roleEquipe.setRefRole(12L);
        List<RoleEquipe> roleEquipes = this.pglRoleEquipeDAO.trouver(roleEquipe);

        //Remplissage des projets de l'équipe
        List<Projet> listeProjetEquipe;
        listeProjetEquipe = this.pglProjetDAO.lister();

        List<Utilisateur> listeEtudiantsI2 = this.utilisateurDAO.listerEtudiantsI2();
        
        request.setAttribute("listeEtudiantsI2", listeEtudiantsI2);
        request.setAttribute(ServletUtilitaire.ATT_LISTE_ETUDIANTS_I2_DISPOS, listeEtudiantsI2Dispos);
        request.setAttribute(ServletUtilitaire.ATT_EQUIPES_I2_ETU, listeEtudiantsI2DisposAll);
        request.setAttribute(ServletUtilitaire.ATT_EQUIPES_I2_LISTE_EQUIPE, listeEquipe);
        request.setAttribute(ServletUtilitaire.ATT_EQUIPES_I2_LISTE_USERS, listeUtilisateur);
        request.setAttribute(ServletUtilitaire.ATT_EQUIPES_I2_LISTE_ANNEE_SCOLAIRE, listeAnneeScolaire);
        request.setAttribute(ServletUtilitaire.ATT_EQUIPES_I2_LISTE_PROJET, listeProjetEquipe);
        request.setAttribute(ServletUtilitaire.ATT_EQUIPES_I2_LISTE_SPRINT, listeSprint);
        request.setAttribute(ServletUtilitaire.ATT_EQUIPES_I2_LISTE_ROLES, roleEquipes);
        request.setAttribute(ServletUtilitaire.ATT_EQUIPES_I2_LISTE_PROF, listeProfesseurs);

        this.getServletContext().getRequestDispatcher(VUE_FORM).forward(request, response);
    }


    /**
     * Méthode permettant de retourner la liste de tous les étudiants I2 (etudiantPGL+ role etudiant)
     * disponible pour la création
     * d'équipes (sans l'utilisateur qui crée l'équipe)
     */
    private List<Utilisateur> getUtilisateursI2() {
        return this.utilisateurDAO.listerEtudiantsI2Disponibles();
    }

    /**
     * Méthode permettant de retourner la liste de tous les étudiants I2 (etudiantPGL+ role etudiant)
     * disponible pour la création
     * d'équipes
     */
    protected List<Utilisateur> listerUtilisateursI2Dispo() {
        return this.utilisateurDAO.listerEtudiantsI2DisponiblesSansEquipes();
    }

    private List<Utilisateur> trouverUtilisateursByIdRole(Role idRole) {
        return this.utilisateurDAO.trouverUtilisateursByIdRole(idRole);
    }

    /**
     * @param request
     * @return liste des élèves choisis pour etre membre de l'équipe
     */
    protected List<Long> idMembresEquipesChoisisSansEquipe(HttpServletRequest request) {
        List<Long> idMembresEquipesChoisis = new ArrayList<>();

        for (int i = 0; i < listerUtilisateursI2Dispo().size(); i++) {
            if (request.getParameter(CHECKBOX + i) != null) {
                idMembresEquipesChoisis.add(Long.parseLong(request.getParameter(CHECKBOX + i)));
            }
        }
        return idMembresEquipesChoisis;
    }

    /**
     * @param request
     * @return liste des po choisis pour etre membre de l'équipe
     */
    protected List<Long> idProfSelect(HttpServletRequest request) {
        List<Long> idProductOwnersSelect = new ArrayList<>();

        Role role = new Role();
        role.setNomRole(PROF);

        Role role1 = this.roleDAO.trouver(role).get(0);
        for (int i = 0; i < trouverUtilisateursByIdRole(role1).size(); i++) {
            if (request.getParameter(CHECKBOX + i) != null) {
                idProductOwnersSelect.add(Long.parseLong(request.getParameter(CHECKBOX + i)));
            }
        }

        return idProductOwnersSelect;
    }

    /**
     * @param request
     * @return liste des élèves choisis pour etre membre de l'équipe
     */
    protected List<Long> idMembresEquipesI2SelectAll(HttpServletRequest request) {
        List<Long> idMembresEquipesChoisis = new ArrayList<>();

        for (int i = 0; i < getUtilisateursI2().size(); i++) {
            if (request.getParameter(CHECKBOX + i) != null) {
                idMembresEquipesChoisis.add(Long.parseLong(request.getParameter(CHECKBOX + i)));
            }
        }
        return idMembresEquipesChoisis;
    }

    /**
     * Méthode doPost
     *
     * @param request
     * @param response
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        switch (request.getParameter(ServletUtilitaire.CHAMP_FORMULAIRE).toLowerCase()) {
            case ("ajouterequipei2"):
                this.creationEquipeI2(request);
                break;
            case ("supprimerequipei2"):
                this.suppressionEquipeI2(request);
                break;
            case ("ajoutermembreequipei2"):
                this.ajouterMembreEquipeI2(request);
                break;
            case ("supprimermembreequipei2"):
                this.suppressionMembreEquipeI2(request);
                break;
            case ("modifiermembreequipei2"):
                this.transfererMembreEquipeI2(request);
                break;
            case ("affecterproductownerequipei2"):
                this.affecterProductOwnerEquipeI2(request);
                break;
            default:
                break;
        }
        try {
            doGet(request, response);
        } catch (Exception e) {
            logger.log(Level.WARN, "Echec chargement doGet", e);

        }
    }

    /**
     * Méthode permettant l'affectation des product Owners à une équipe
     *
     * @param request
     */
    private void affecterProductOwnerEquipeI2(HttpServletRequest request) {
        /* Récupération des données saisies */
        Long idEquipeDestination = Long.valueOf(request.getParameter(ServletUtilitaire.CHAMP_LISTE_EQUIPE));

        Callback callback;
        List<Long> idProfSelect = idProfSelect(request);

        fr.eseo.ld.pgl.beans.Equipe equipe = new fr.eseo.ld.pgl.beans.Equipe();
        equipe.setIdEquipe(idEquipeDestination);

        if (!pglEquipeDAO.trouver(equipe).isEmpty()) {
            Role role = new Role();
            role.setNomRole(PRODUCT_OWNER);

            Long idAnnee = this.pglEquipeDAO.trouver(equipe).get(0).getRefAnnee();
            AnneeScolaire scolaire = new AnneeScolaire();
            scolaire.setIdAnneeScolaire(idAnnee);

            AnneeScolaire anneeScolaire = this.anneeScolaireDAO.trouver(scolaire).get(0);
            Long idOption = anneeScolaire.getIdOption();
            OptionESEO optionEquipe = new OptionESEO();
            optionEquipe.setIdOption(idOption);
            OptionESEO optionDeEquipe = this.optionESEODAO.trouver(optionEquipe).get(0);

            Role role1 = this.roleDAO.trouver(role).get(0);

            RoleEquipe roleEquipe = new RoleEquipe();
            roleEquipe.setRefEquipe(idEquipeDestination);
            roleEquipe.setRefRole(role1.getIdRole());

            for (int i = 0; i < idProfSelect.size(); i++) {
                Utilisateur user = new Utilisateur();
                user.setIdUtilisateur(idProfSelect.get(i));

                Utilisateur prof = this.utilisateurDAO.trouver(user).get(i);

                this.utilisateurDAO.associerRoleOption(prof, role1, optionDeEquipe);


                roleEquipe.setRefUtilisateur(idProfSelect.get(i));
                if (this.pglRoleEquipeDAO.trouver(roleEquipe).isEmpty()) {
                    this.pglRoleEquipeDAO.creer(roleEquipe);
                    if (!this.pglRoleEquipeDAO.trouver(roleEquipe).isEmpty()) {
                        callback = new Callback(CallbackType.SUCCESS,
                                SUCCES_PO);
                        CallbackUtilitaire.setCallback(request, callback);
                    } else {
                        callback = new Callback(CallbackType.ERROR,
                                ERREUR_PO);
                        CallbackUtilitaire.setCallback(request, callback);
                    }
                } else {
                    callback = new Callback(CallbackType.WARNING,
                            PRODUCT_OWNER_PRESENT_DANS_L_EQUIPE);
                    CallbackUtilitaire.setCallback(request, callback);
                }
            }
        } else {
            callback = new Callback(CallbackType.WARNING,
                    MESSAGE_ERREUR);
            CallbackUtilitaire.setCallback(request, callback);
        }
    }


    /**
     * Méthode permettant la création d'une équipe I2
     *
     * @param request
     */
    private void creationEquipeI2(HttpServletRequest request) {
        /* Récupération des données saisies */
        String nomEquipe = request.getParameter(ServletUtilitaire.CHAMP_NOM_EQUIPE);
        Long idProjet = Long.valueOf(request.getParameter(ServletUtilitaire.CHAMP_PROJET_EQUIPE));
        Long idAnneeScolaire = Long.valueOf(request.getParameter(ServletUtilitaire.CHAMP_ANNEE_SCOLAIRE_EQUIPE));

        AnneeScolaire anneeScolaire = new AnneeScolaire();
        anneeScolaire.setIdAnneeScolaire(idAnneeScolaire);
        List<AnneeScolaire> anneeScolaireChoisie = this.anneeScolaireDAO.trouver(anneeScolaire);

        Callback callback;
        /* Récupération de l'ID de l'utilisateur en session */
        HttpSession session = request.getSession();
        Utilisateur utilisateurConnecte = (Utilisateur) session.getAttribute(ServletUtilitaire.ATT_SESSION_USER);
        Long idUtilisateurConnecte = utilisateurConnecte.getIdUtilisateur();

        if (!anneeScolaireChoisie.isEmpty()) {
            fr.eseo.ld.pgl.beans.Equipe equipe = new fr.eseo.ld.pgl.beans.Equipe();
            equipe.setNom(nomEquipe);
            equipe.setRefProjet(idProjet);
            equipe.setRefAnnee(anneeScolaireChoisie.get(0).getIdAnneeScolaire());
            this.pglEquipeDAO.creer(equipe);

            if (this.pglEquipeDAO.trouver(equipe).isEmpty()) {
                callback = new Callback(CallbackType.ERROR,
                        ERREUR_EQUIPE);
                CallbackUtilitaire.setCallback(request, callback);
            }

            Sprint sprint = new Sprint();
            sprint.setRefAnnee(anneeScolaireChoisie.get(0).getIdAnneeScolaire());
            Sprint sprintChoisi = this.sprintDAO.trouver(sprint).get(0);

            List<Long> idsMembres = idMembresEquipesI2SelectAll(request);
            for (int i = 0; i < idsMembres.size(); i++) {
                fr.eseo.ld.pgl.beans.EtudiantEquipe etudiantEquipe = new fr.eseo.ld.pgl.beans.EtudiantEquipe();
                fr.eseo.ld.pgl.beans.Equipe equipeCree = this.pglEquipeDAO.trouver(equipe).get(0);

                etudiantEquipe.setIdEquipe(equipeCree.getIdEquipe());
                etudiantEquipe.setIdEtudiant(idsMembres.get(i));
                etudiantEquipe.setIdSprint(sprintChoisi.getIdSprint());

                try {
                    this.pglEtudiantEquipeDAO.creer(etudiantEquipe);
                } catch (Exception e) {
                    logger.log(Level.WARN, ECHEC, e);
                }


                BonusMalus bonusMalus = new BonusMalus();
                bonusMalus.setValeur(0f);
                bonusMalus.setRefSprint(Math.toIntExact(sprintChoisi.getIdSprint()));
                bonusMalus.setRefEtudiant(Math.toIntExact(idsMembres.get(i)));
                bonusMalus.setRefEvaluateur(Math.toIntExact(idUtilisateurConnecte));
                bonusMalus.setJustification("attente");
                try {
                    this.bonusMalusDAO.creer(bonusMalus);
                } catch (Exception e) {
                    logger.log(Level.WARN, ECHEC, e);
                }
                if (this.pglEtudiantEquipeDAO.trouver(etudiantEquipe).isEmpty()) {
                    callback = new Callback(CallbackType.ERROR,
                            ERREUR_EQUIPE);
                    CallbackUtilitaire.setCallback(request, callback);
                }

            }
            callback = new Callback(CallbackType.SUCCESS,
                    "Equipe : " + equipe.getNom() + " a été ajouté.");
            CallbackUtilitaire.setCallback(request, callback);
        } else {
            callback = new Callback(CallbackType.ERROR,
                    ERREUR_EQUIPE);
            CallbackUtilitaire.setCallback(request, callback);
        }


    }


    /**
     * Méthode permettant la suppression d'une équipe
     *
     * @param request
     */
    private void suppressionEquipeI2(HttpServletRequest request) {
        Long idEquipe = Long.valueOf(request.getParameter(ServletUtilitaire.CHAMP_ID_EQUIPE_PGL_DELETE));
        fr.eseo.ld.pgl.beans.Equipe equipeToDelete = new fr.eseo.ld.pgl.beans.Equipe();
        equipeToDelete.setIdEquipe(idEquipe);
        EtudiantEquipe etudiantEquipeBonusMalus = new EtudiantEquipe();
        etudiantEquipeBonusMalus.setIdEquipe(idEquipe);
        etudiantEquipeBonusMalus.setIdSprint(1L);
        List<EtudiantEquipe> etudiantEquipe = this.pglEtudiantEquipeDAO.trouver(etudiantEquipeBonusMalus);
        for (EtudiantEquipe etu : etudiantEquipe) {
            BonusMalus bonusMalus = new BonusMalus();
            bonusMalus.setRefEtudiant(Math.toIntExact(etu.getIdEtudiant()));
            bonusMalus.setRefSprint(1);
            BonusMalus bonusMalus1 = this.bonusMalusDAO.trouverBonusMalusEtu(etu.getIdEtudiant());
            this.bonusMalusDAO.supprimer(bonusMalus1);
        }
        this.pglEquipeDAO.supprimer(equipeToDelete);
        Callback callback;
        if (pglEquipeDAO.trouver(equipeToDelete).isEmpty()) {
            callback = new Callback(CallbackType.SUCCESS,
                    SUCCES_DELETE);
            CallbackUtilitaire.setCallback(request, callback);
        } else {
            callback = new Callback(CallbackType.ERROR,
                    ERROR_DELETE);
            CallbackUtilitaire.setCallback(request, callback);
        }
    }


    /**
     * Méthode permettant l'ajout de membres à une équipe
     *
     * @param request
     */
    private void ajouterMembreEquipeI2(HttpServletRequest request) {
        Long idEquipeToModifier = Long.valueOf(request.getParameter(ServletUtilitaire.CHAMP_ID_EQUIPE));
        List<Long> idsMembresModifies = idMembresEquipesChoisisSansEquipe(request);

        Long idSprint = Long.valueOf(request.getParameter(SPRINT_SELECT));
        fr.eseo.ld.pgl.beans.Equipe equipeToModifier = new fr.eseo.ld.pgl.beans.Equipe();
        equipeToModifier.setIdEquipe(idEquipeToModifier);

        /* Récupération de l'ID de l'utilisateur en session */
        HttpSession session = request.getSession();
        Utilisateur utilisateurConnecte = (Utilisateur) session.getAttribute(ServletUtilitaire.ATT_SESSION_USER);
        Long idUtilisateurConnecte = utilisateurConnecte.getIdUtilisateur();


        Callback callback;
        if (!pglEquipeDAO.trouver(equipeToModifier).isEmpty()) {
            EtudiantEquipe etudiantEquipe = getEtudiantEquipe(idSprint, equipeToModifier);
            for (int i = 0; i < idsMembresModifies.size(); i++) {
                etudiantEquipe.setIdEtudiant(idsMembresModifies.get(i));
                this.pglEtudiantEquipeDAO.creer(etudiantEquipe);

                BonusMalus bonusMalus = new BonusMalus();
                bonusMalus.setValeur(0f);
                bonusMalus.setRefSprint(Math.toIntExact(idSprint));
                bonusMalus.setRefEtudiant(Math.toIntExact(idsMembresModifies.get(i)));
                bonusMalus.setRefEvaluateur(Math.toIntExact(idUtilisateurConnecte));
                bonusMalus.setJustification("attente");
                try {
                    this.bonusMalusDAO.creer(bonusMalus);
                } catch (Exception e) {
                    logger.log(Level.WARN, ECHEC, e);
                }
                if (!this.pglEtudiantEquipeDAO.trouver(etudiantEquipe).isEmpty()) {
                    callback = new Callback(CallbackType.SUCCESS,
                            SUCCES_AJOUTER_ETU);
                    CallbackUtilitaire.setCallback(request, callback);
                } else {
                    callback = new Callback(CallbackType.ERROR,
                            ECHEC_AJOUT_ETU);
                    CallbackUtilitaire.setCallback(request, callback);
                }
            }
        } else {
            callback = new Callback(CallbackType.WARNING,
                    MESSAGE_ERREUR);
            CallbackUtilitaire.setCallback(request, callback);
        }
    }

    private EtudiantEquipe getEtudiantEquipe(Long idSprint, Equipe equipeToModifier) {
        Equipe equipe = this.pglEquipeDAO.trouver(equipeToModifier).get(0);
        Long idEquipe = equipe.getIdEquipe();

        EtudiantEquipe etudiantEquipe = new EtudiantEquipe();
        etudiantEquipe.setIdEquipe(idEquipe);
        etudiantEquipe.setIdSprint(idSprint);
        return etudiantEquipe;
    }

    /**
     * Méthode permettant la suppression de membres à une équipe
     *
     * @param request
     */
    private void suppressionMembreEquipeI2(HttpServletRequest request) {
        Long idEquipeToModifier = Long.valueOf(request.getParameter(ServletUtilitaire.CHAMP_ID_EQUIPE));
        List<Long> listIdMembresModifies = idMembresEquipesI2SelectAll(request);

        Long idSprint = Long.valueOf(request.getParameter(SPRINT_SELECT));
        fr.eseo.ld.pgl.beans.Equipe equipeToModifier = new fr.eseo.ld.pgl.beans.Equipe();
        equipeToModifier.setIdEquipe(idEquipeToModifier);

        Callback callback;
        if (!pglEquipeDAO.trouver(equipeToModifier).isEmpty()) {
            EtudiantEquipe etudiantEquipe = getEtudiantEquipe(idSprint, equipeToModifier);

            this.pglEtudiantEquipeDAO.trouver(etudiantEquipe);

            List<EtudiantEquipe> etudiantEquipeActuelle = this.pglEtudiantEquipeDAO.trouver(etudiantEquipe);

            for (EtudiantEquipe etudiantEquipePresentActuellement : etudiantEquipeActuelle) {
                for (int i = 0; i < listIdMembresModifies.size(); i++) {
                    if (etudiantEquipePresentActuellement.getIdEtudiant().equals(listIdMembresModifies.get(i))) {

                        BonusMalus bonusMalus = new BonusMalus();
                        bonusMalus.setRefEtudiant(Math.toIntExact(etudiantEquipePresentActuellement.getIdEtudiant()));
                        bonusMalus.setRefSprint(1);
                        BonusMalus bonusMalus1 = this.bonusMalusDAO.trouverBonusMalusEtu(etudiantEquipePresentActuellement.getIdEtudiant());
                        this.bonusMalusDAO.supprimer(bonusMalus1);

                        this.pglEtudiantEquipeDAO.supprimer(etudiantEquipePresentActuellement);
                        callback = new Callback(CallbackType.SUCCESS,
                                SUCCES_SUPP_ETU);
                        CallbackUtilitaire.setCallback(request, callback);

                    }
                }
            }
        } else {
            callback = new Callback(CallbackType.WARNING,
                    MESSAGE_ERREUR);
            CallbackUtilitaire.setCallback(request, callback);
        }
    }

    /**
     * Méthode permettant de transferer des membres entre une équipe
     *
     * @param request
     */
    private void transfererMembreEquipeI2(HttpServletRequest request) {
        Long idEquipeDestination = Long.valueOf(request.getParameter(ServletUtilitaire.CHAMP_LISTE_EQUIPE));
        Long idSprint = Long.valueOf(request.getParameter(SPRINT_SELECT));
        List<Long> listeIdMembresModifies = idMembresEquipesI2SelectAll(request);
        Callback callback;
        for (int i = 0; i < listeIdMembresModifies.size(); i++) {
            EtudiantEquipe etudiantEquipeSelectionne = new EtudiantEquipe();
            etudiantEquipeSelectionne.setIdEtudiant(listeIdMembresModifies.get(i));

            EtudiantEquipe etudiantEquipeToChange = this.pglEtudiantEquipeDAO.trouver(etudiantEquipeSelectionne).get(0);
            etudiantEquipeToChange.setIdEquipe(idEquipeDestination);
            etudiantEquipeToChange.setIdSprint(idSprint);

            if (!this.pglEtudiantEquipeDAO.trouver(etudiantEquipeSelectionne).isEmpty()) {
                EtudiantEquipe etudiantEquipeToChange2 = this.pglEtudiantEquipeDAO.trouver(etudiantEquipeSelectionne).get(0);

                if (etudiantEquipeToChange2.getIdSprint().equals(idSprint) && etudiantEquipeToChange2.getIdEquipe().equals(idEquipeDestination)) {
                    callback = new Callback(CallbackType.WARNING,
                            WARNING_TRANSFERT);
                    CallbackUtilitaire.setCallback(request, callback);
                } else {
                    this.pglEtudiantEquipeDAO.modifier(etudiantEquipeToChange);
                    callback = new Callback(CallbackType.SUCCESS,
                            SUCCES_TRANSFERT);
                    CallbackUtilitaire.setCallback(request, callback);
                }
            }
        }
    }

}