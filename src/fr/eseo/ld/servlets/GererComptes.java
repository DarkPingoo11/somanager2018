package fr.eseo.ld.servlets;

import fr.eseo.ld.beans.*;
import fr.eseo.ld.dao.*;
import fr.eseo.ld.ldap.GestionUtilisateurLDAP;
import fr.eseo.ld.ldap.VerificationAnnuaire;
import fr.eseo.ld.utils.CallbackUtilitaire;
import fr.eseo.ld.utils.IconUtilitaire;
import fr.eseo.ld.utils.NomUtilitaire;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.mindrot.jbcrypt.BCrypt;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static fr.eseo.ld.servlets.ServletUtilitaire.validerChampsInscription;

/**
 * Servlet permettant la validation de l'inscription d'un utilisateur.
 */
@WebServlet("/GererComptes")
@MultipartConfig
public class GererComptes extends HttpServlet {
    private static final long serialVersionUID = 1L;

    private static final String VUE_FORM = "/WEB-INF/formulaires/gererComptes.jsp";

    private static final String ATT_NB_ECHEC = "nombreEchec";
    private static final String ATT_NB_INSERTION = "nombreInsertion";
    private static final String ATT_ERREURS_IMPORT = "erreursImportation";
    private static final String ATT_CHOIX_IMPORT = "choixImportCompte";

    private static final String CHAMP_ERREUR_EXTENSION = "extensionErreur";
    private static final String CHAMP_ERREUR = "erreurs";
    private static final String ATT_ANNEES = "annees";
    private static final String ATT_ROLES = "rolesDispo";
    private static final String ATT_STATUTVALIDE = "valider";

    // Choix compte LDAP ou compte sur base local
    private boolean ajoutSurLDAP = false;

    private UtilisateurDAO utilisateurDAO;
    private RoleDAO roleDAO;
    private AnneeScolaireDAO anneeScolaireDAO;
    private NotificationDAO notificationDAO;

    private static Logger logger = Logger.getLogger(GererComptes.class.getName());

    @Override
    public void init() throws ServletException {
        /* Récupération d'instances de DAO */
        DAOFactory daoFactory = (DAOFactory) this.getServletContext().getAttribute(ServletUtilitaire.CONF_DAO_FACTORY);
        this.utilisateurDAO = daoFactory.getUtilisateurDao();
        this.notificationDAO = daoFactory.getNotificationDao();
        this.anneeScolaireDAO = daoFactory.getAnneeScolaireDAO();
        this.roleDAO = daoFactory.getRoleDAO();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setAttribute(ATT_CHOIX_IMPORT, ajoutSurLDAP);
        request.setAttribute(ATT_ANNEES, this.chargerAnneesScolaires());
        request.setAttribute(ATT_ROLES, this.chargerRoles());

        this.getServletContext().getRequestDispatcher(VUE_FORM).forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        /* Récupération de l'utilisateur en session */
        HttpSession session = request.getSession();
        Utilisateur utilisateurSession = (Utilisateur) session.getAttribute(ServletUtilitaire.ATT_SESSION_USER);
        switch(request.getParameter(ServletUtilitaire.CHAMP_FORMULAIRE).toLowerCase()) {
            case("validercompte"):
                this.validerCompte(request, response);
                return;
            case("synchroniserbdd"):
                /* Synchronisation de la BDD */
                VerificationAnnuaire verification = new VerificationAnnuaire();
                verification.synchroniserBDD();
                this.notificationDAO.ajouterNotification(utilisateurSession.getIdUtilisateur(),
                        "La synchronisation a été réussie");
                break;
            case("suppressioncompte"):
                this.supprimerCompte(request, response, utilisateurSession);
                return;
            case("ajouterutilisateur"):
                this.ajouterUtilisateur(request, utilisateurSession);
                break;
            case("choixpreferenceajout"):
                String choix = request.getParameter("optradio");
                this.ajoutSurLDAP = Boolean.valueOf(choix);
                break;
            default:
                if (request.getContentType().contains("multipart/form-data;")) {
                    importerFichierCompte(request);
                }
                break;
        }



        // On recharge la page a la fin du traitement
        this.doGet(request, response);
    }


    private void ajouterUtilisateur(HttpServletRequest request, Utilisateur utilisateurSession) {
        /* Validation des champs */
        String identifiant = request.getParameter(ServletUtilitaire.CHAMP_IDENTIFIANT);
        String email = request.getParameter(ServletUtilitaire.CHAMP_EMAIL);
        Map<String, String> erreurs = validerChampsInscription(utilisateurDAO, identifiant, email);

        /*
         * Si aucune erreur de validation n'a eu lieu, alors insertion de l'Utilisateur
         * dans la BDD, sinon ré-affichage du formulaire.
         */
        String resultat;
        if (erreurs.isEmpty()) {
            Utilisateur utilisateur = ServletUtilitaire.mapInscription(request, identifiant, email, "oui");
            this.utilisateurDAO.creer(utilisateur);
            resultat = "Succès de l'inscription.";
            request.setAttribute(ServletUtilitaire.ATT_RESULTAT, resultat);
            this.notificationDAO.ajouterNotification(utilisateurSession.getIdUtilisateur(),
                    "L'utilisateur a bien été ajouté à la base de données");
            /* Ré-affichage du formulaire avec message de succès */
            CallbackUtilitaire.setCallback(request,
                    new Callback(CallbackType.SUCCESS, resultat));
        } else {
            resultat = "Échec de l'inscription.";
            request.setAttribute(ServletUtilitaire.ATT_RESULTAT, resultat);
            request.setAttribute(ServletUtilitaire.ATT_ERREURS, erreurs);
            CallbackUtilitaire.setCallback(request,
                    new Callback(CallbackType.ERROR, resultat));
        }
    }

    private void supprimerCompte(HttpServletRequest request, HttpServletResponse response, Utilisateur utilisateurS){
        // Cette fonction renvoies un message ajax
        // Recuperation de l'identifiant à supprimer
        String id = request.getParameter(ServletUtilitaire.CHAMP_ID_UTILISATEUR);

        /* Recherche de l'existence de l'utilisateur dans notre base d'utilisateurs */
        Utilisateur utilisateur = new Utilisateur();
        utilisateur.setIdUtilisateur(Long.valueOf(id));

        /* Suppression du compte si compte existant */
        List<Utilisateur> user = this.utilisateurDAO.trouver(utilisateur);
        Callback callback;

        if(!id.equalsIgnoreCase(utilisateurS.getIdUtilisateur().toString())) {
            if (!user.isEmpty()) {
                this.utilisateurDAO.supprimer(utilisateur);
                callback = new Callback(CallbackType.SUCCESS,
                        "L'utilisateur " + user.get(0).getNom() + " a été supprimé");
            } else {
                callback = new Callback(CallbackType.ERROR,
                        "L'utilisateur selectionné n'existe pas ou plus");
            }
        } else {
            callback = new Callback(CallbackType.ERROR,
                    "Vous ne pouvez pas supprimer votre compte");
        }

        //Renvoi de la reponse en JSON
        CallbackUtilitaire.sendJsonResponse(response, callback);
    }

    /**
     * Valide le compte (ou le suspend)
     * @param request requeteHTTP
     * @param response reponsehttp
     */
    private void validerCompte(HttpServletRequest request, HttpServletResponse response){
        //Requete utilisant AJAX
        /* Modification de l'utilisateur dans la base de données */
        Long idUtilisateur = Long.parseLong(request.getParameter(ServletUtilitaire.CHAMP_ID_UTILISATEUR));
        String validerCompte = request.getParameter(ATT_STATUTVALIDE);
        if(validerCompte == null || !validerCompte.equalsIgnoreCase("oui")) {
            validerCompte = "non";
        }

        Utilisateur utilisateur = new Utilisateur();
        utilisateur.setIdUtilisateur(idUtilisateur);
        utilisateur.setValide(validerCompte);
        this.utilisateurDAO.modifier(utilisateur);

        //Récupération des informations de l'utilisateur modifié
        Callback callback;
        List<Utilisateur> reponses = this.utilisateurDAO.trouver(utilisateur);
        if(reponses.size() == 1) {
            utilisateur = reponses.get(0);
            callback = new Callback(CallbackType.SUCCESS,
                    "Le compte de " +
                            utilisateur.getNom().toUpperCase() + " " + utilisateur.getPrenom() +
                            " a bien été " + (validerCompte.equalsIgnoreCase("oui") ? "validé" : "suspendu"));

            /* Envoi d'une notification à l'utilisateur */
            this.notificationDAO.ajouterNotification(utilisateur.getIdUtilisateur(),
                    "Votre compte a été "+
                            (validerCompte.equalsIgnoreCase("oui") ? "validé" : "suspendu")
                            +" par l\'administrateur !");
        } else {
            callback = new Callback(CallbackType.ERROR,
                    "Impossible de changer le statut du compte n°" + utilisateur.getIdUtilisateur());
        }

        //Renvoi de la reponse en JSON
        CallbackUtilitaire.sendJsonResponse(response, callback);
    }

    /**
     * Charge tous les rôles du système
     * @return rôles du système
     */
    private List<RoleAffichage> chargerRoles() {
        /* Recherche des roles */
        List<Role> roles = this.roleDAO.listerRolesApplicatifs();
        List<RoleAffichage> rolesList = new ArrayList<>();

        //Ajouter l'icone
        for(Role role : roles) {
            RoleAffichage roleA = new RoleAffichage();
            roleA.setIdRole(role.getIdRole());
            roleA.setNomRole(role.getNomRole());
            roleA.setNomComplet(NomUtilitaire.getNomCompletRole(role));
            roleA.setIcone(IconUtilitaire.getIconForRole(role));
            rolesList.add(roleA);
        }

        return rolesList;
    }

    /**
     * Charge toutes les années scolaires du système
     * @return années scolaires
     */
    private List<AnneeScolaire> chargerAnneesScolaires() {
        /* Recherche des utilisateurs semblables à l'utilisateur type */
        return this.anneeScolaireDAO.lister();
    }

    /**
     * Importe le fichier compte.
     *
     * @param request request
     */
    private void importerFichierCompte(HttpServletRequest request) {
        Map<String, String> erreursfichier = new HashMap<>();
        try {
            /* On parcourt toutes les images rentrées en paramètre de la requete */
            for (Part part : request.getParts()) {
                String fileName = ServletUtilitaire.extractFileName(part);
                fileName = new File(fileName).getName();
                // Si le fichier qu'on veut importer n'est pas de spécifié ou n'est pas un
                // fichier *.csv, on génère une erreur
                if (!("").equals(fileName) && ServletUtilitaire.verificationFichier(fileName)) {
                    InputStream inStream = part.getInputStream();
                    this.importFichierCompte(ServletUtilitaire.convertirStreamEnString(inStream), request);
                    erreursfichier.put(CHAMP_ERREUR_EXTENSION, "");
                } else {
                    erreursfichier.put(CHAMP_ERREUR_EXTENSION, "L'extension ne correspond pas à un fichier *.csv");
                }
            }
            request.setAttribute(CHAMP_ERREUR, erreursfichier);
            request.setAttribute(ATT_CHOIX_IMPORT, ajoutSurLDAP);
        } catch (IOException | ServletException e) {
            logger.log(Level.FATAL, "Impossible de modifier l'image ", e);
        }
    }

    /**
     *
     * @param texte texte
     * @param requete request
     */
    private void importFichierCompte(String texte, HttpServletRequest requete) {
        int nombreInsertion = 0;
        int compteur = 0;
        List<String> erreurs = new ArrayList<>();
        for (String ligne : texte.split("\n")) {
        	ligne = ligne.substring(1, ligne.length()-2);
            String[] attributs = ligne.split(";");

            // Si la ligne contient 5 attributs, alors il ne manque pas de données
            if (attributs.length == 5) {
                nombreInsertion += creerCompteUtilisateur(attributs, ligne, erreurs);
                // Sinon, la ligne est fausse et on ne peut pas la traiter
            } else {
                erreurs.add("Impossible lire la ligne, il manque des attributs : " + ligne);
            }
            compteur += 1;
        }
        requete.setAttribute(ATT_NB_ECHEC, compteur - nombreInsertion);
        requete.setAttribute(ATT_NB_INSERTION, nombreInsertion);
        requete.setAttribute(ATT_ERREURS_IMPORT, erreurs);
    }

    /**
     * Méthode traitant la création d'un role étudiant si la ligne importée à 5
     * attributs
     *
     * @param attributs
     *            le tableau String des différentes informations utilisateur
     * @param ligne
     *            la ligne brut du fichier décrivant le role à importer
     *
     * @return 1 si le role a été crée, 0 sinon
     */
    private int creerCompteUtilisateur(String[] attributs, String ligne, List<String> erreurs) {
        int resultat = 0;
        String identifiant = attributs[0];
        String email = attributs[1];
        String nom = attributs[2];
        String prenom = attributs[3];
        String motDePasse = attributs[4];

        /* On cherche si l'utilisateur existe déjà */
        Utilisateur utilisateur = new Utilisateur();
        utilisateur.setIdentifiant(identifiant);
        List<Utilisateur> utilisateurs = utilisateurDAO.trouver(utilisateur);

        // Vérification de la recherche des différents beans sur la BDD
        if (!utilisateurs.isEmpty()) {
            erreurs.add("Utilisateur déjà existant. Ligne : " + ligne);
        } else {

            /* Insertion des données récupérées dans un bean */
            //
            utilisateur = new Utilisateur();
            utilisateur.setNom(nom.toUpperCase());
            utilisateur.setPrenom(prenom);
            utilisateur.setIdentifiant(identifiant);
            utilisateur.setEmail(email);
            utilisateur.setValide("oui");

            String hash;
            if (!ajoutSurLDAP) {
                /* Hashage du mot de passe */
                hash = BCrypt.hashpw(motDePasse, BCrypt.gensalt());
                utilisateur.setHash(hash);
            } else {
                hash = "activeDirectory";
                utilisateur.setHash(hash);
                GestionUtilisateurLDAP gestionUtilisateur = new GestionUtilisateurLDAP();
                gestionUtilisateur.ajouterUtilisateur(identifiant, nom, prenom, email, motDePasse);
            }
            utilisateurDAO.creer(utilisateur);
            resultat = 1;
        }
        return resultat;
    }

}