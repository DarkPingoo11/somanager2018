package fr.eseo.ld.servlets;

import fr.eseo.ld.beans.*;
import fr.eseo.ld.dao.*;
import fr.eseo.ld.utils.CallbackUtilitaire;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.*;

/**
 * Servlet permettant l'affichage des réunions crées par l'utilisateur.
 */
@WebServlet("/GererMeetings")
public class GererMeetings extends HttpServlet {


    private static final String VUE_FORM = "/WEB-INF/formulaires/gererMeetings.jsp";

    private MeetingDAO meetingDAO;
    private InviteReunionDAO inviteReunionDAO;
    private NotificationDAO notificationDAO;
    private UtilisateurDAO utilisateurDAO;

    private static final String BTN_MODIFIER = "modifierMeeting";
    private static final String BTN_VALIDER_COMPTE_RENDU = "validerCompteRenduMeeting";
    private static final String BTN_DEL = "supprimerMeeting";

    private static final String ATT_FORMULAIRE = "formulaire";
    private static final String ATT_UTILISATEURS = "listeNomPrenomID";
    private static final String ATT_UTILISATEUR_RECHERCHE = "utilisateurRecherche";
    private static final String ATT_LISTE_MEETING_A = "listeMeeting_A";
    private static final String CHAMP_PROFESSEUR = "professeur";
    private static final String USER = "L'utilisateur ";
    private static final String ATT_GERER_MEETING = "GererMeeting";

    private static Logger logger = Logger.getLogger(GererMeetings.class.getName());

    private Map<String, Boolean> estValider = new HashMap<>();

    @Override
    public void init() throws ServletException {
        /* Récupération d'instances de DAO */
        DAOFactory daoFactory = (DAOFactory) this.getServletContext().getAttribute(ServletUtilitaire.CONF_DAO_FACTORY);
        this.meetingDAO = daoFactory.getMeetingDAO();
        this.notificationDAO = daoFactory.getNotificationDao();
        this.utilisateurDAO = daoFactory.getUtilisateurDao();
        this.inviteReunionDAO = daoFactory.getInviteReunionDAO();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        /* Récupération de l'ID de l'utilisateur en session */
        HttpSession session = req.getSession();
        HttpServletRequest requeteChargee = chargerListeUtilisateurs(req);

        /* Affichage de la liste des meetings  pour mes meetings*/
        this.afficherListeMeetingsCreesEtMeetingsInvites(req, session);

        /*Affichage de la liste des meetings pour ajouter un participant */
        List<Meeting> listeM = chargerListeMeeting(req);

        /* Recupere si l'utilisateur est un prof */
        List<Utilisateur> listeProfesseur = utilisateurDAO.listerProfesseur();

        Utilisateur user = (Utilisateur) session.getAttribute("utilisateur");
        Boolean estProf = false;

        for (int i = 0; i < listeProfesseur.size(); i++) {
            if (listeProfesseur.get(i).getIdUtilisateur() == user.getIdUtilisateur()) {
                estProf = true;
            }
        }

        req.setAttribute("estProf", estProf);
        req.setAttribute(ATT_LISTE_MEETING_A, this.chargerListeMeeting(listeM));
        req.setAttribute(ServletUtilitaire.ATT_LISTE_UTILISATEURS, this.chargerUtilisateursListe());


        this.getServletContext().getRequestDispatcher(VUE_FORM).forward(requeteChargee, resp);
    }


    private void afficherListeMeetingsCreesEtMeetingsInvites(HttpServletRequest req, HttpSession session) {
        Utilisateur utilisateurSession = (Utilisateur) session.getAttribute(ServletUtilitaire.ATT_SESSION_USER);
        Long idUtilisateurSession = utilisateurSession.getIdUtilisateur();

        /* Recherche des meetings de l'utilisateur via meetingDAO */
        Utilisateur utilisateur = new Utilisateur();
        utilisateur.setIdUtilisateur(idUtilisateurSession);

        Meeting meeting = new Meeting();
        meeting.setRefUtilisateur(idUtilisateurSession);

        List<Meeting> listeMeetings = this.meetingDAO.trouver(meeting);
        List<Meeting> listeMett = this.meetingDAO.trouverMeetingUtilisateur(utilisateur);
        listeMeetings.addAll(listeMett);

        /* Jointure de Utilisateur et Meeting via idUtilisateur */
        Map<Long, String> nomCreateurMeeting = new HashMap<>();
        Map<Long, Long> idCreateurMeeting = new HashMap<>();
        Map<Long, StringBuilder> nomsInvitesMeetings = new HashMap<>();

        intialiseVariableEstValider(req);

        /*Jointure de Utilisateur et Invite */
        for (Iterator<Meeting> iter = listeMeetings.listIterator(); iter.hasNext(); ) {
            Meeting meeting1 = iter.next();

            /* Recherche de l'utilisateur qui a déposé ce meeting */
            List<Utilisateur> listeCreateurMeeting = utilisateurDAO.trouverCreateurReunion(meeting1);
            /* Recherche des utilisateur ivnités au meeting */
            List<Utilisateur> listeInvitesMeeting = utilisateurDAO.trouverInvitesReunion(meeting1);

            /* Correspondance entre l'ID d'un utilisateur et son nom */
            String nom;
            Long id = -1L;
            if (!listeCreateurMeeting.isEmpty()) { // sécurité dans le cas où la BDD ait mal été remplie
                nom = listeCreateurMeeting.get(0).getNom() + " " + listeCreateurMeeting.get(0).getPrenom();
                id = listeCreateurMeeting.get(0).getIdUtilisateur();
            } else {
                /* Impossible mais au cas ou */
                nom = "Pas de créateur du meeting";
            }

            StringBuilder nomInvite = new StringBuilder();
            //Pour la liste des invites
            if (!listeInvitesMeeting.isEmpty()) {
                for (Utilisateur utilisa : listeInvitesMeeting) {
                    if (!listeInvitesMeeting.isEmpty()) { // sécurité dans le cas où la BDD ait mal été remplie
                        nomInvite.append(utilisa.getNom() + " " + utilisa.getPrenom() + " ; ");
                    } else {
                        /* Impossible mais au cas ou */
                        nomInvite.append("*");
                    }
                    nomsInvitesMeetings.put(meeting1.getIdReunion(), nomInvite);

                }
            } else {
                nomInvite.append("Il n'y a aucun participant à cet réunion.");
                nomsInvitesMeetings.put(meeting1.getIdReunion(), nomInvite);
            }
            nomCreateurMeeting.put(meeting1.getIdReunion(), nom);
            idCreateurMeeting.put(meeting1.getIdReunion(), id);
        }
        req.setAttribute(ServletUtilitaire.ATT_NOMS_CREATEUR_MEETING, nomCreateurMeeting);
        req.setAttribute(ServletUtilitaire.CHAMP_ID_CREATEUR_MEETING, idCreateurMeeting);

        req.setAttribute(ServletUtilitaire.ATT_NOMS_INVITES_MEETINGS, nomsInvitesMeetings);
        /* Affichage du formulaire */
        req.setAttribute(ServletUtilitaire.ATT_LISTE_MEETING, listeMeetings);
    }

    private void intialiseVariableEstValider(HttpServletRequest req) {
        /* initialise la variable estValider */
        List<Meeting> listMeeting = this.meetingDAO.lister();

        for (int i = 0; i < listMeeting.size(); i++) {
            if ((estValider != null) && (estValider.get(String.valueOf(listMeeting.get(i).getIdReunion())) == null)) {
                estValider.put(String.valueOf(listMeeting.get(i).getIdReunion()), false);
            }
        }
        req.setAttribute("hmapMeeting", estValider);
    }


    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        /* Récupération de l'utilisateur en session */
        HttpSession session = req.getSession();
        if (BTN_DEL.equalsIgnoreCase(req.getParameter(ServletUtilitaire.CHAMP_FORMULAIRE))) {
            try {
                this.supprimerMeeting(req, session);
            } catch (Exception e) {
                logger.log(Level.WARN, "Échec de la suppression de l'objet.", e);
            }
        } else if (BTN_VALIDER_COMPTE_RENDU.equalsIgnoreCase(req.getParameter(ServletUtilitaire.CHAMP_FORMULAIRE))) {
            try {
                this.validerCompteRenduMeeting(req, session);
            } catch (Exception e) {
                logger.log(Level.WARN, "Échec de la modification de l'objet.", e);
            }
        } else if (BTN_MODIFIER.equalsIgnoreCase(req.getParameter(ServletUtilitaire.CHAMP_FORMULAIRE))) {
            try {
                this.modifierMeeting(req, session);
            } catch (Exception e) {
                logger.log(Level.WARN, "Échec de la modification de l'objet.", e);
            }
        } else if ("ajouterMeeting".equalsIgnoreCase(req.getParameter(ATT_FORMULAIRE))) {
            ajouterMeeting(req, session);
        } else if ("ajouterInvite".equalsIgnoreCase(req.getParameter(ATT_FORMULAIRE))) {
            ajouterInvite(req);
        } else if ("supprimerInvite".equalsIgnoreCase(req.getParameter(ATT_FORMULAIRE))) {
            supprimerInvite(req);
        }
        try {
            doGet(req, resp);
        } catch (Exception e) {
            logger.log(Level.WARN, "Échec de la mise à jour de l'objet, aucune ligne modifiée dans la table.", e);

        }
    }

    private void modifierMeeting(HttpServletRequest req, HttpSession session) {
        /* Récupération des données saisies */
        Long idMeeting = Long.parseLong(req.getParameter(ServletUtilitaire.CHAMP_ID_MEETING));
        String titre = req.getParameter(ServletUtilitaire.CHAMP_TITRE);
        String description = req.getParameter(ServletUtilitaire.CHAMP_DESCRIPTION);
        String lieu = req.getParameter(ServletUtilitaire.CHAMP_ENDROIT);
        String dateDebut = req.getParameter(ServletUtilitaire.CHAMP_DATE_HEURE_MEETING);
        String compteRendu = req.getParameter(ServletUtilitaire.CHAMP_COMPTE_RENDU_MEETING);
        String idCreateurMeeting = (req.getParameter(ServletUtilitaire.CHAMP_ID_CREATEUR_MEETING));

        /* Validation des champs */
        Map<String, String> erreurs = new HashMap<>();
        Callback callback = null;

        Utilisateur utilisateur = (Utilisateur) session.getAttribute(ServletUtilitaire.ATT_SESSION_USER);
        Long idUtilisateur = utilisateur.getIdUtilisateur();

        /*
         * Si aucune erreur de validation n'a eu lieu, alors insertion du Meeting dans la
         * BDD, sinon ré-affichage du formulaire.
         */
        if (erreurs.isEmpty()) {
            /* Insertion du meeting dans la BDD via meetingDAO */
            Meeting meeting = new Meeting();
            meeting.setIdReunion(idMeeting);
            meeting.setTitre(titre);
            meeting.setDescription(description);
            meeting.setDate(dateDebut);
            meeting.setLieu(lieu);
            meeting.setCompteRendu(compteRendu);
            meeting.setRefUtilisateur(Long.valueOf(idCreateurMeeting));
            this.meetingDAO.modifier(meeting);
            /* Ajout d'une notification */
            this.notificationDAO.ajouterNotification(idUtilisateur, "Le meeting " + meeting.getTitre() +
                    " a été modifié !");
            callback = new Callback(CallbackType.SUCCESS,
                    "Meeting: " + meeting.getTitre() + " modifié.");
            CallbackUtilitaire.setCallback(req, callback);
            /* Rechargement de la page*/
        } else {
            String resultat = "Échec de la modification du meeting, des erreurs sont présentes. ";
            this.notificationDAO.ajouterNotification(idUtilisateur, "Le meeting n'a pas été modifié !", ATT_GERER_MEETING);
            callback = new Callback(CallbackType.ERROR,
                    "Meeting: non modifié.");
            CallbackUtilitaire.setCallback(req, callback);
            req.setAttribute(ServletUtilitaire.ATT_RESULTAT, resultat);
            req.setAttribute(ServletUtilitaire.ATT_ERREURS, erreurs);
            /* Ré-affichage du formulaire avec les erreurs */
        }
    }


    private void supprimerMeeting(HttpServletRequest request, HttpSession session) {
        // Cette fonction renvoies un message ajax
        // Recuperation de l'identifiant du meeting à supprimer
        String idMeeting = request.getParameter(ServletUtilitaire.CHAMP_ID_MEETING);

        Utilisateur utilisateur = (Utilisateur) session.getAttribute(ServletUtilitaire.ATT_SESSION_USER);
        Long idUtilisateur = utilisateur.getIdUtilisateur();

        /* Recherche de l'existence du meeting dans notre base de meeting */
        Meeting meeting = new Meeting();
        meeting.setIdReunion(Long.valueOf(idMeeting));

        /* Suppression du meeting si meeting existant */
        List<Meeting> meetings = this.meetingDAO.trouver(meeting);
        String titreMeeting = meetings.get(0).getTitre();
        Callback callback = null;

        Invite invite = new Invite();
        invite.setRefReunion(Long.valueOf(idMeeting));
        if (!meetings.isEmpty()) {
            List<Invite> inviteM = this.inviteReunionDAO.trouver(invite);
            if (!inviteM.isEmpty()) {
                this.meetingDAO.supprimerInviteMeeting(meeting);
            }
            this.meetingDAO.supprimer(meeting);
            /* Ajout d'une notification */
            this.notificationDAO.ajouterNotification(idUtilisateur, "Le meeting " + titreMeeting +
                    " est bien supprimé !");
            callback = new Callback(CallbackType.SUCCESS,
                    "Meeting: " + titreMeeting + " est bien suprimé.");
            /* Rechargement de la page*/
            CallbackUtilitaire.setCallback(request, callback);
        } else {
            callback = new Callback(CallbackType.ERROR,
                    "Le meeting selectionné n'existe pas ou plus.");
            CallbackUtilitaire.setCallback(request, callback);
        }
    }


    /**
     * Methode permettant de charger la liste de mes meetings à la bdd.
     */
    private List<Meeting> chargerListeMeeting(HttpServletRequest req) {
        /* Récupération de l'ID de l'utilisateur en session */
        HttpSession session = req.getSession();
        Utilisateur utilisateurSession = (Utilisateur) session.getAttribute(ServletUtilitaire.ATT_SESSION_USER);
        Long idUtilisateurSession = utilisateurSession.getIdUtilisateur();

        /* Recherche des meetings de l'utilisateur via meetingDAO */
        Utilisateur utilisateur = new Utilisateur();
        utilisateur.setIdUtilisateur(idUtilisateurSession);
        Meeting meeting = new Meeting();
        meeting.setRefUtilisateur(idUtilisateurSession);
        return this.meetingDAO.trouver(meeting);
    }


    private List<String> chargerListeMeeting(List<Meeting> listeMeeting) {
        List<String> listeMeetings = new ArrayList<>();
        for (int i = 0; i < listeMeeting.size(); i++) {
            listeMeetings.add(listeMeeting.get(i).getTitre() + " %/% " + listeMeeting.get(i).getIdReunion());
        }
        return listeMeetings;
    }

    /**
     * Methode permettant d'ajouter des invites à la bdd.
     */

    private void ajouterInvite(HttpServletRequest req) {
        /* Validation des champs */
        Map<String, String> erreurs = new HashMap<>();
        Callback callback;

        // On recupère l'utilisateur rentré en paramètre
        String utilisateurRecherche = req.getParameter(ATT_UTILISATEUR_RECHERCHE);

        // On utilise la méthode utilitaire qui permet de vérifier la cohérence de
        // l'utilisateur rentré et de vérifier qu'il existe
        // La fonction renvoie null si l'utilisateur n'existe pas
        Utilisateur utilisateurChoisi = ServletUtilitaire.rechercheUtilisateurAutoCompletion(utilisateurRecherche,
                this.utilisateurDAO);

        Meeting meetingChoisie = getMeeting(req, utilisateurChoisi);

        /* Créateur de la reunion ne peut etre ajouter aux invités */
        Long idCreateurMeeting;
        idCreateurMeeting = meetingChoisie.getRefUtilisateur();
        if (idCreateurMeeting.equals(utilisateurChoisi.getIdUtilisateur())) {
            erreurs.put(CHAMP_PROFESSEUR, "Cet utilisateur est le créateur de la réunion.");
            callback = new Callback(CallbackType.ERROR,
                    USER + utilisateurChoisi.getNom() + " " + utilisateurChoisi.getPrenom() + " est " +
                            "le créateur de la réunion, impossible de l'inviter.");
            CallbackUtilitaire.setCallback(req, callback);
        } else {


            Utilisateur utilisateurSession = (Utilisateur) req.getSession().getAttribute(ServletUtilitaire.ATT_SESSION_USER);

            Invite invite = new Invite();
            invite.setRefReunion(meetingChoisie.getIdReunion());
            invite.setRefUtilisateur(utilisateurChoisi.getIdUtilisateur());
            if (inviteReunionDAO.trouver(invite).isEmpty()) {
                inviteReunionDAO.creer(invite);
                // On signale à l'utilisateur2 qu'il est invité au meeting
                this.notificationDAO.ajouterNotification(utilisateurChoisi.getIdUtilisateur(),
                        utilisateurSession.getNom().toUpperCase() + " " + utilisateurSession.getPrenom()
                                + " vous a invité au meeting : " + meetingChoisie.getTitre() + ".");
                callback = new Callback(CallbackType.SUCCESS,
                        USER + utilisateurChoisi.getNom() + " " + utilisateurChoisi.getPrenom() + " a été ajouté" +
                                " au meeting.");
                CallbackUtilitaire.setCallback(req, callback);
            } else {
                callback = new Callback(CallbackType.ERROR,
                        USER + utilisateurChoisi.getNom() + " " + utilisateurChoisi.getPrenom() + " est déjà " +
                                "dans " +
                                "la liste des invités du meeting : " + meetingChoisie.getTitre() + ".");
                CallbackUtilitaire.setCallback(req, callback);
            }

        }
    }

    private Meeting getMeeting(HttpServletRequest req, Utilisateur utilisateurChoisi) {
        Meeting meetingExclure = new Meeting();
        meetingExclure.setRefUtilisateur(utilisateurChoisi.getIdUtilisateur());

        // On recupere le choix du meeting et de l'utilisateur
        String choix = req.getParameter("selectRole");

        String[] choixTab = choix.split("%/%");


        String idMeeting = choixTab[1];
        idMeeting = idMeeting.replaceAll(" ", "");

        Meeting meeting = new Meeting();
        meeting.setIdReunion(Long.valueOf(idMeeting));
        return meetingDAO.trouver(meeting).get(0);
    }

    /**
     * Methode permettant de supprimer des invites à la bdd.
     */
    private void supprimerInvite(HttpServletRequest req) {
        /* Validation des champs */
        Callback callback;

        // On recupère l'utilisateur rentré en paramètre
        String utilisateurRecherche = req.getParameter(ATT_UTILISATEUR_RECHERCHE);

        // On utilise la méthode utilitaire qui permet de vérifier la cohérence de
        // l'utilisateur rentré et de vérifier qu'il existe
        // La fonction renvoie null si l'utilisateur n'existe pas
        Utilisateur utilisateurChoisi = ServletUtilitaire.rechercheUtilisateurAutoCompletion(utilisateurRecherche,
                this.utilisateurDAO);

        Meeting meetingChoisie = getMeeting(req, utilisateurChoisi);

        Invite invite = new Invite();
        invite.setRefReunion(meetingChoisie.getIdReunion());
        invite.setRefUtilisateur(utilisateurChoisi.getIdUtilisateur());
        if (inviteReunionDAO.trouver(invite).isEmpty()) {
            callback = new Callback(CallbackType.ERROR,
                    USER + utilisateurChoisi.getNom() + " " + utilisateurChoisi.getPrenom() + " n'est pas " +
                            "dans " +
                            "la liste des invités du meeting : " + meetingChoisie.getTitre() + ".");
            CallbackUtilitaire.setCallback(req, callback);
        } else {
            inviteReunionDAO.supprimer(invite);
            callback = new Callback(CallbackType.SUCCESS,
                    USER + utilisateurChoisi.getNom() + " " + utilisateurChoisi.getPrenom() + " a été supprimé" +
                            " du meeting.");
            CallbackUtilitaire.setCallback(req, callback);
        }
    }

    /**
     * Methode permettant d'ajouter des meetings à la bdd.
     */

    private void ajouterMeeting(HttpServletRequest req, HttpSession session) {
        /* Récupération des données saisies */
        String titre = req.getParameter(ServletUtilitaire.CHAMP_TITRE);
        String description = req.getParameter(ServletUtilitaire.CHAMP_DESCRIPTION);
        String lieu = req.getParameter(ServletUtilitaire.CHAMP_ENDROIT);
        String dateDebut = req.getParameter(ServletUtilitaire.CHAMP_DATE_DEBUT_MEETING);
        String heureDebut = req.getParameter(ServletUtilitaire.CHAMP_HEURE_RENDU_MEETING);
        String compteRendu = req.getParameter(ServletUtilitaire.CHAMP_COMPTE_RENDU_MEETING);

        /* Concatenation de la date et l'heure */
        String dateMeeting = dateDebut + " " + heureDebut;
        /* Validation des champs */
        Map<String, String> erreurs = new HashMap<>();
        Callback callback;


        Utilisateur utilisateur = (Utilisateur) session.getAttribute(ServletUtilitaire.ATT_SESSION_USER);
        Long idUtilisateur = utilisateur.getIdUtilisateur();
        /*
         * Si aucune erreur de validation n'a eu lieu, alors insertion du Meeting dans la
         * BDD, sinon ré-affichage du formulaire.
         */
        if (erreurs.isEmpty()) {
            /* Insertion du meeting dans la BDD via meetingDAO */
            Meeting meeting = new Meeting();
            meeting.setTitre(titre);
            meeting.setDescription(description);
            meeting.setDate(dateMeeting);
            meeting.setLieu(lieu);
            meeting.setCompteRendu(compteRendu);
            meeting.setRefUtilisateur(idUtilisateur);
            this.meetingDAO.creer(meeting);
            /* Ajout d'une notification */
            this.notificationDAO.ajouterNotification(idUtilisateur, "Un meeting a été ajouté à votre " +
                    "liste !", "GererMeetings");
            callback = new Callback(CallbackType.SUCCESS,
                    "Meeting : " + meeting.getTitre() + " a été ajouté.");
            CallbackUtilitaire.setCallback(req, callback);
        } else {
            String resultat = "Échec de l'ajout du meeting, des erreurs sont présentes. ";
            /* Ajout d'une notification */
            this.notificationDAO.ajouterNotification(idUtilisateur, resultat);
            req.setAttribute(ServletUtilitaire.ATT_RESULTAT, resultat);
            req.setAttribute(ServletUtilitaire.ATT_ERREURS, erreurs);
        }
    }

    /**
     * Liste tous les utilisateurs.
     * (nécessaire pour le formulaire de recherche)
     *
     * @return la liste de tous les utilisateurs.
     */
    private List<Utilisateur> chargerUtilisateursListe() {
        /* Recherche des utilisateurs semblables à l'utilisateur type */
        return this.utilisateurDAO.lister();
    }


    /**
     * Charge la liste des utilisateurs dans une requête.
     *
     * @param requete la requête à charger.
     * @return requete la requête chargée.
     */
    private HttpServletRequest chargerListeUtilisateurs(HttpServletRequest requete) {
        /* Récupération de la liste des utilisateurs dans la BDD */
        List<Utilisateur> utilisateurs = utilisateurDAO.lister();

        /* Formatage de la liste des utilisateurs en String */
        StringBuilder liste = new StringBuilder();
        boolean premier = false;
        for (Utilisateur utilisateur : utilisateurs) {
            if (!premier) {
                liste.append(utilisateur.getNom().toUpperCase().replace(" ", "") + " "
                        + utilisateur.getPrenom().replace(" ", "") + " - "
                        + utilisateur.getIdentifiant().replace(" ", ""));
                premier = true;
            } else {
                liste.append("," + utilisateur.getNom().toUpperCase().replace(" ", "") + " "
                        + utilisateur.getPrenom().replace(" ", "") + " - "
                        + utilisateur.getIdentifiant().replace(" ", ""));
            }
        }
        /* Mise en place de la liste en session */
        requete.getSession().setAttribute(ATT_UTILISATEURS, liste.toString());
        return requete;
    }

    private void validerCompteRenduMeeting(HttpServletRequest req, HttpSession session) {

        /* Récupération des données saisies */
        Long idMeeting = Long.parseLong(req.getParameter(ServletUtilitaire.CHAMP_ID_MEETING));

        /* Validation des champs */
        Map<String, String> erreurs = new HashMap<>();
        Callback callback = null;

        Utilisateur utilisateur = (Utilisateur) session.getAttribute(ServletUtilitaire.ATT_SESSION_USER);
        Long idUtilisateur = utilisateur.getIdUtilisateur();

        /*
         * Si aucune erreur de validation n'a eu lieu, alors insertion du Meeting dans la
         * BDD, sinon ré-affichage du formulaire.
         */

        if (erreurs.isEmpty()) {
            estValider.replace(String.valueOf(idMeeting), true);
            req.setAttribute("hmapMeeting", estValider);

            CallbackUtilitaire.setCallback(req, callback);

        } else {
            String resultat = "Échec de la modification du meeting, des erreurs sont présentes. ";
            this.notificationDAO.ajouterNotification(idUtilisateur, "Le meeting n'a pas été modifié !", "GererMeetings");
            callback = new Callback(CallbackType.ERROR,
                    "Meeting: non modifié.");
            CallbackUtilitaire.setCallback(req, callback);
            req.setAttribute(ServletUtilitaire.ATT_RESULTAT, resultat);
            req.setAttribute(ServletUtilitaire.ATT_ERREURS, erreurs);
            /* Ré-affichage du formulaire avec les erreurs */
        }
    }

}
