package fr.eseo.ld.servlets;

import com.mysql.jdbc.Connection;
import fr.eseo.ld.beans.Callback;
import fr.eseo.ld.beans.CallbackType;
import fr.eseo.ld.utils.CallbackUtilitaire;
import fr.eseo.ld.utils.IOUtilitaire;
import fr.eseo.ld.objets.sql.SqlScriptRunner;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;

/**
 * Servlet permettant d'exporter les notes de I3
 * INFORMATION IMPORTANTE :
 * Si vous ne voulez pas configurer l'application à l'aide de l'assistant
 * Il suffit de placer un fichier 'installed.lock' à la racine de WebContent
 */
@WebServlet("/Installation")
public class Installation extends HttpServlet {


    private enum InstallationStatus { BDDPARAM, BDDSCRIPT, RECAP}

    private static final String SERVLET_ACCUEIL = "index.jsp";
    private static final String VUE_INSTALL = "/WEB-INF/formulaires/installation.jsp";
    private static final String PATH_INSTALLED_LOCK = "/installed.lock";
    private static final String PATH_DAO_PROPERTIESDEF = "/fr/eseo/ld/dao/daoDefaut.properties";
    private static final String PATH_DAO_PROPERTIES = "/fr/eseo/ld/dao/dao.properties";
    private static final String SCRIPT_CREATION_BDD = "/somanager.sql";

    private static final String MSG_ERROR_ECRITURE = "Impossible d'écrire dans le fichier dao.properties, verifiez que vous avez accordé les accès en écriture";
    private static final String MSG_ERROR_BDDCONNECT = "Impossible de se connecter à la base de données";
    private static final String MSG_ERROR_CREATIONBDD = "Une erreur est survenue lors de la création de la base de données. Merci d'executer le script manuellement";
    private static final String MSG_ERROR_BDDCREATION = "La base de données n'a pas été créée correctement";
    private static final String MSG_ERROR_FINALISER = "Le fichier 'installed.lock' n'a pas pu être crée, " +
            "merci de l'ajouter manuellement à la racine de WebContent";
    private static final String MSG_SUCCESS_CREATIONBDD = "La base de données à correctement été créée";

    private static final String ATT_HOTE = "hote";
    private static final String ATT_BDD = "bdd";
    private static final String ATT_USERNAME = "username";
    private static final String ATT_PW = "password";
    private static final String ATT_DRIVER = "driver";
    private static final String ATT_ETAT = "state";

    private static final String PROP_URL = "url";
    private static final String PROP_DRIVER = ATT_DRIVER;
    private static final String PROP_USERNAME = "nomUtilisateur";
    private static final String PROP_PW = "motDePasse";

    private static final String TEST_DATABASE = "testdatabase_2e9a8e90";

    private static Logger logger = Logger.getLogger(Installation.class.getName());

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        //Vérification du statut de l'application
        if (isSomanagerInstalled(this.getServletContext()) && request.getAttribute(ATT_ETAT) == null) {
            response.sendRedirect(SERVLET_ACCUEIL);
            return;
        }

        //Lire les properties
        if(request.getAttribute(ATT_ETAT) == null || request.getAttribute(ATT_ETAT).toString().equalsIgnoreCase(InstallationStatus.BDDPARAM.toString())) {
            Map<String, String> propDAO = IOUtilitaire.lireFichierProperties(PATH_DAO_PROPERTIESDEF);
            String url = propDAO.get(PROP_URL);
            request.setAttribute(PROP_DRIVER, propDAO.get(PROP_DRIVER));
            request.setAttribute(ATT_BDD, url.substring(url.lastIndexOf('/') + 1));
            request.setAttribute(ATT_HOTE, url.substring(url.indexOf("mysql://") + 8, url.lastIndexOf('/')));
            request.setAttribute(PROP_USERNAME, propDAO.get(PROP_USERNAME));
            request.setAttribute(PROP_PW, propDAO.get(PROP_PW));
        } else if(request.getAttribute(ATT_ETAT).toString().equalsIgnoreCase(InstallationStatus.BDDSCRIPT.toString())) {
            Map<String, String> propDAO = IOUtilitaire.lireFichierProperties(PATH_DAO_PROPERTIES);
            request.setAttribute(PROP_USERNAME, propDAO.get(PROP_USERNAME));
            request.setAttribute(PROP_PW, propDAO.get(PROP_PW));
        }


        //Si l'app n'est pas installée, on lance la configuration
        this.getServletContext().getRequestDispatcher(VUE_INSTALL).forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        /* Récupération de l'ID de l'utilisateur en session */
        switch(request.getParameter(ServletUtilitaire.CHAMP_FORMULAIRE).toLowerCase()) {
            case "validerbdd":
                doValiderBDD(request);
                break;
            case "creerbdd":
                doCreerBDD(request, response);
                break;
            case "finircreerbdd":
                doFinirCreerBdd(request);
                break;
            case "testerbdd":
                doTestConnexion(request, response);
                break;
            case "testeraccess":
                doTestAccess(request, response);
                break;
            default:
                break;
        }


        doGet(request, response);
    }

    /**
     * Effectue l'action de création de la base de données
     * @param request request
     * @param response response
     */
    private void doCreerBDD(HttpServletRequest request, HttpServletResponse response) {
        Map<String, String> properties = IOUtilitaire.lireFichierProperties(PATH_DAO_PROPERTIES);
        String url = properties.get(PROP_URL);
        url = url.substring(0, url.lastIndexOf('/')) + "?allowMultiQueries=true";
        String driver = properties.get(PROP_DRIVER);
        String username = request.getParameter(ATT_USERNAME);
        String password = request.getParameter(ATT_PW);

        if(IOUtilitaire.testConnexion(url, driver, username, password)) {
            try (Connection connection = (Connection) DriverManager.getConnection( url, username, password);InputStreamReader isr = new InputStreamReader(Installation.class.getResourceAsStream(SCRIPT_CREATION_BDD), "UTF8")){
                SqlScriptRunner script = new SqlScriptRunner(connection);
                if(script.runScript(isr)) {
                    CallbackUtilitaire.sendJsonResponse(response, new Callback(CallbackType.SUCCESS, MSG_SUCCESS_CREATIONBDD));
                } else {
                    CallbackUtilitaire.sendJsonResponse(response, new Callback(MSG_ERROR_CREATIONBDD));
                }
            } catch (SQLException | IOException e) {
                logger.log(Level.WARN, "Erreur lors de la création de la bdd", e);
                CallbackUtilitaire.sendJsonResponse(response, new Callback(MSG_ERROR_CREATIONBDD));
            }
        } else {
            CallbackUtilitaire.sendJsonResponse(response, new Callback(MSG_ERROR_BDDCONNECT));
        }
    }

    /**
     * Effecture l'action tester connexion
     * @param request request
     * @param response response
     */
    private void doTestConnexion(HttpServletRequest request, HttpServletResponse response) {
        String hote =  request.getParameter(ATT_HOTE);
        String url = "jdbc:mysql://" + hote;
        String username = request.getParameter(ATT_USERNAME);
        String password = request.getParameter(ATT_PW);
        String driver = request.getParameter(ATT_DRIVER);

        Callback c;
        if(IOUtilitaire.testConnexion(url, driver, username, password)) {
            c = new Callback(CallbackType.SUCCESS, "Connexion reussie !");
        } else {
            c = new Callback("Echec de connexion...");
        }
        CallbackUtilitaire.sendJsonResponse(response, c);
    }

    /**
     * Effectue l'action de fin de creation de la bdd
     * @param request request
     */
    private void doFinirCreerBdd(HttpServletRequest request) {
        Map<String, String> properties = IOUtilitaire.lireFichierProperties(PATH_DAO_PROPERTIES);
        String url = properties.get(PROP_URL);
        String driver = properties.get(PROP_DRIVER);
        String username = properties.get(PROP_USERNAME);
        String password = properties.get(PROP_PW);

        if(IOUtilitaire.testConnexion(url, driver, username, password)) {
            request.setAttribute(ATT_ETAT, InstallationStatus.RECAP);
            //Créer le fichier stipulant que l'installation est terminée
            File file = new File(this.getServletContext().getRealPath(PATH_INSTALLED_LOCK));
            try {
                if(!file.createNewFile()) {
                    CallbackUtilitaire.setCallback(request, new Callback(MSG_ERROR_FINALISER));
                }
            } catch (IOException e) {
                logger.log(Level.WARN, "Impossible de créer le fichier de finalisation de l'installation");
            }
        } else {
            request.setAttribute(ATT_ETAT, InstallationStatus.BDDSCRIPT);
            CallbackUtilitaire.setCallback(request, new Callback(MSG_ERROR_BDDCREATION));
        }
    }


    /**
     * Effectue l'action de test des droits d'accès
     * @param request request
     * @param response response
     */
    private void doTestAccess(HttpServletRequest request, HttpServletResponse response) {
        Map<String, String> properties = IOUtilitaire.lireFichierProperties(PATH_DAO_PROPERTIES);
        String url = properties.get(PROP_URL);
        url = url.substring(0, url.lastIndexOf('/'));
        String driver = properties.get(PROP_DRIVER);
        String username = request.getParameter(ATT_USERNAME);
        String password = request.getParameter(ATT_PW);

        Callback c;
        if(IOUtilitaire.testConnexion(url, driver, username, password)) {
            boolean hasRight = false;

            //Tentative de création d'une bdd fictive
            String sqlC = "CREATE DATABASE " + TEST_DATABASE;
            String sqlD = "DROP DATABASE IF EXISTS " + TEST_DATABASE;
            try (Connection connexion = (Connection) DriverManager.getConnection( url, username, password); Statement statement = connexion.createStatement()) {
                statement.execute(sqlD);
                statement.execute(sqlC);
                statement.execute(sqlD);
                hasRight = true;
            } catch ( SQLException e) {
                logger.log(Level.WARN, "Erreur lors de la connexion.", e);
            }

            if(hasRight) {
                c = new Callback(CallbackType.SUCCESS, "Accès autorisé !");
            } else {
                c = new Callback("Privilèges insuffisants");
            }
        } else {
            c = new Callback("Echec de connexion...");
        }
        CallbackUtilitaire.sendJsonResponse(response, c);
    }

    /**
     * Effecture l'action de validation des paramètres de BDD
     * @param request Request
     */
    private void doValiderBDD(HttpServletRequest request) {
        String hote =  request.getParameter(ATT_HOTE);
        String bdd =  request.getParameter(ATT_BDD);
        String url = "jdbc:mysql://" + hote;
        String urlfull = url + "/" + bdd;
        String username = request.getParameter(ATT_USERNAME);
        String password = request.getParameter(ATT_PW);
        String driver = request.getParameter(ATT_DRIVER);

        if(IOUtilitaire.testConnexion(url, driver, username, password)) {
            HashMap<String, String> properties = new HashMap<>();
            properties.put(PROP_URL, urlfull);
            properties.put(PROP_DRIVER, driver);
            properties.put(PROP_USERNAME, username);
            properties.put(PROP_PW, password);
            if(IOUtilitaire.ecrireFichierProperties(PATH_DAO_PROPERTIES, properties)) {
                request.setAttribute(ATT_ETAT, InstallationStatus.BDDSCRIPT);
            } else {
                request.setAttribute(ATT_ETAT, InstallationStatus.BDDPARAM);
                CallbackUtilitaire.setCallback(request, new Callback(MSG_ERROR_ECRITURE));
            }
        } else {
            CallbackUtilitaire.setCallback(request, new Callback(MSG_ERROR_BDDCONNECT));
        }
    }


    /**
     * Vérifie si somanager est installé & configuré
     * @param context ServletContext
     * @return true si installé, false sinon
     */
    public static boolean isSomanagerInstalled(ServletContext context) {
        //Vérifions si le fichier installed.lock est présent
        File file = new File(context.getRealPath(PATH_INSTALLED_LOCK));
        return file.exists();
    }

}