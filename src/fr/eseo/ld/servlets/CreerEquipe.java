package fr.eseo.ld.servlets;

import fr.eseo.ld.beans.*;
import fr.eseo.ld.dao.*;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.*;

/**
 * Servlet permettant la création d'un équipe.
 */
@WebServlet("/CreerEquipe")
public class CreerEquipe extends HttpServlet {
    private static final long serialVersionUID = 1L;

    private static final String VUE_FORM = "/WEB-INF/formulaires/creerEquipe.jsp";

    private static final String ECHEC = "Échec de la création de l'équipe. ";


    private static Logger logger = Logger.getLogger(CreerEquipe.class.getName());

    private SujetDAO sujetDAO;
    private OptionESEODAO optionESEODAO;
    private EquipeDAO equipeDAO;
    private NotificationDAO notificationDAO;
    private UtilisateurDAO utilisateurDAO;
    private EtudiantDAO etudiantDAO;
    private AnneeScolaireDAO anneeScolaireDAO;


    @Override
    public void init() throws ServletException {
        /* Récupération d'instances de DAO */
        DAOFactory daoFactory = (DAOFactory) this.getServletContext().getAttribute(ServletUtilitaire.CONF_DAO_FACTORY);
        this.sujetDAO = daoFactory.getSujetDao();
        this.optionESEODAO = daoFactory.getOptionESEODAO();
        this.equipeDAO = daoFactory.getEquipeDAO();
        this.notificationDAO = daoFactory.getNotificationDao();
        this.utilisateurDAO = daoFactory.getUtilisateurDao();
        this.etudiantDAO = daoFactory.getEtudiantDao();
        this.anneeScolaireDAO = daoFactory.getAnneeScolaireDAO();

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        /* Récupération de l'ID de l'utilisateur en session */
        HttpSession session = request.getSession();
        Utilisateur utilisateurConnecte = (Utilisateur) session.getAttribute(ServletUtilitaire.ATT_SESSION_USER);
        Long idUtilisateurConnecte = utilisateurConnecte.getIdUtilisateur();

        /* Liste des étudiants I3 disponibles pour créer une équipe */
        List<Utilisateur> listeEtudiantsI3Dispos = getUtilisateursI3(idUtilisateurConnecte);


        // Remplissage de la liste de tous les utilisateurs
        List<Utilisateur> listeUtilisateur;
        listeUtilisateur = this.utilisateurDAO.lister();

        //Remplissage de la liste des années
        List<AnneeScolaire> listeAnneeScolaire;
        listeAnneeScolaire = this.anneeScolaireDAO.lister();


        /* Recherche de tous les sujets et de leurs options */
        List<String> options = new ArrayList<>();
        List<Sujet> listeSujetsPublies = this.listerSujetsDisponibles();
        List<String> optionsSujet = this.listerOptionsSujets(listeSujetsPublies, options);

        Etudiant etudiant = new Etudiant();
        AnneeScolaire anneeScolaire = new AnneeScolaire();
        anneeScolaire.setIdOption((long) 1);
        int annee = this.anneeScolaireDAO.trouver(anneeScolaire).get(0).getAnneeDebut();
        etudiant.setAnnee(annee);
        List<Etudiant> listeEtudiantsI3 = this.etudiantDAO.trouver(etudiant);
        List<String> optionsEtudiant = this.listerOptionsEtudiants(listeEtudiantsI3Dispos, options);

        request.setAttribute(ServletUtilitaire.ATT_OPTIONS_ETUDIANT, optionsEtudiant);
        request.setAttribute(ServletUtilitaire.ATT_OPTIONS_SUJET, optionsSujet);
        request.setAttribute(ServletUtilitaire.ATT_LISTE_OPTIONS, options);
        request.setAttribute(ServletUtilitaire.ATT_LISTE_SUJETS, listeSujetsPublies);
        request.setAttribute(ServletUtilitaire.ATT_LISTE_ETUDIANTS_DISPOS, listeEtudiantsI3Dispos);
        request.setAttribute(ServletUtilitaire.ATT_LISTE_ETUDIANTS_I3, listeEtudiantsI3);
        request.setAttribute("listeUtilisateur", listeUtilisateur);
        request.setAttribute("listeAnneeScolaire", listeAnneeScolaire);

        this.getServletContext().getRequestDispatcher(VUE_FORM).forward(request, response);
    }

    /**
     * Méthode permettant de retourner la liste des étudiants I3 disponible pour la création
     * d'équipes (sans l'utilisateur qui crée l'équipe)
     */
    private List<Utilisateur> getUtilisateursI3(Long idUtilisateurConnecte) {
        /*
         * Recherche de tous les étudiants de I3 via etudiantDAO et leurs options et on
         * retire l'utilisateur connecté de la liste (s'il est dans la liste)
         */
        List<Utilisateur> listeEtudiantsDispos = this.utilisateurDAO.listerEtudiantsDisponibles();
        for (Iterator<Utilisateur> iterator = listeEtudiantsDispos.iterator(); iterator.hasNext(); ) {
            Utilisateur utilisateur = iterator.next();
            if (idUtilisateurConnecte.equals(utilisateur.getIdUtilisateur())) {
                iterator.remove();
            }
        }
        return listeEtudiantsDispos;
    }


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        /* On récupère l'année scolaire en cours */
        int annee = 0;
        AnneeScolaire anneeScolaire = new AnneeScolaire();
        anneeScolaire.setIdOption((long) 1);
        List<AnneeScolaire> anneesScolaires = this.anneeScolaireDAO.trouver(anneeScolaire);
        for (AnneeScolaire anneeScolaireTrouvee : anneesScolaires) {
            annee = anneeScolaireTrouvee.getAnneeDebut();
        }

        /* Récupération du sujet choisi dans la BDD via son ID */
        Long idSujet = Long.valueOf(request.getParameter(ServletUtilitaire.CHAMP_ID_SUJET));
        Sujet sujet = new Sujet();
        sujet.setIdSujet(idSujet);
        List<Sujet> sujetChoisi = this.sujetDAO.trouver(sujet);

        /*
         * Récupération de certains champs pour lever des erreurs : nbrMinEleves,
         * nbrMaxEleves, contratPro et les options
         */
        int nbrMinElevesSujetChoisi = 0;
        int nbrMaxElevesSujetChoisi = 0;
        boolean contratProSujetChoisi = false;
        for (Sujet sujetChoisiTrouve : sujetChoisi) {
            nbrMinElevesSujetChoisi = sujetChoisiTrouve.getNbrMinEleves();
            nbrMaxElevesSujetChoisi = sujetChoisiTrouve.getNbrMaxEleves();
            contratProSujetChoisi = sujetChoisiTrouve.getContratPro();
        }

        List<OptionESEO> optionsSujetChoisi = this.optionESEODAO.trouverOptionSujet(sujet);
        List<Long> idOptionsSujetChoisi = this.listerIdOptionsSujet(optionsSujetChoisi);

        /*
         * Récupération des champs des étudiants pour les vérifications et création de
         * annee pour l'annee pour la création d'équipe
         */
        int i = 1;
        int taille = 0;
        List<Long> idOptionsMembres = new ArrayList<>();
        List<String> contratProMembres = new ArrayList<>();

        try {
            while (request.getParameter(ServletUtilitaire.CHAMP_ID_MEMBRE + i) != null) {

                Long idMembre = Long.parseLong(request.getParameter(ServletUtilitaire.CHAMP_ID_MEMBRE + i));

                /* Récupération des id des options des membres */
                idOptionsMembres = this.optionsMembres(idOptionsMembres, idMembre);

                /* Récupération du champ contratPro du membre */
                contratProMembres = this.contratProMembres(contratProMembres, idMembre);

                i++;
                taille++;
            }

            /* Vérification des champs : taille, contratPro et options */

            Map<String, String> erreurs = this.verifierCorrespondanceTailleContratPro(taille,
                    nbrMinElevesSujetChoisi, nbrMaxElevesSujetChoisi, contratProSujetChoisi, contratProMembres);
            erreurs = this.verifierCorrespondanceOptions(erreurs, idOptionsMembres, idOptionsSujetChoisi,
                    optionsSujetChoisi);

            if (erreurs.isEmpty()) {
                /* Création de l'équipe et récupération de son ID */
                creerEquipeEtId(request, response, annee, idSujet, taille);

            } else {
                /* On crée le logger pour la console */
                creerLogger(request, response, annee, erreurs);
            }
        } catch (IndexOutOfBoundsException e) {
            genererAffichage(request, response, annee);
        }
    }

    private void creerEquipeEtId(HttpServletRequest request, HttpServletResponse response, int annee, Long idSujet, int taille) throws IOException {
        String resultat;
        Equipe equipe = new Equipe();
        equipe.setIdSujet(idSujet);
        equipe.setAnnee(annee);
        equipe.setValide("non");
        equipe.setTaille(taille);
        this.equipeDAO.creer(equipe);
        List<Equipe> equipes = this.equipeDAO.listerEquipeSujet(idSujet);
        String idEquipe = "";
        for (Equipe equipeTrouvee : equipes) {
            idEquipe = equipeTrouvee.getIdEquipe();
        }

        /* Insertion des étudiants dans la table EtudiantEquipe */
        int j = 1;
        while (request.getParameter(ServletUtilitaire.CHAMP_ID_MEMBRE + j) != null) {
            EtudiantEquipe etudiantEquipe = new EtudiantEquipe();
            Long idMembre = Long.parseLong(request.getParameter(ServletUtilitaire.CHAMP_ID_MEMBRE + j));
            etudiantEquipe.setIdEquipe(idEquipe);
            etudiantEquipe.setIdEtudiant(idMembre);
            this.equipeDAO.attribuerEtudiantEquipe(etudiantEquipe);

            /* On notifie les membres qu'ils ont été ajoutés dans une équipe */
            this.notificationDAO.ajouterNotification(idMembre, "Vous avez été ajouté dans une équipe !");

            j++;
        }

        /* Modification du sujet choisi pour passer son état à attribué */
        Sujet sujetAModifier = new Sujet();
        sujetAModifier.setIdSujet(idSujet);
        List<Sujet> sujetsAModifier = this.sujetDAO.trouver(sujetAModifier);
        for (Sujet sujetTrouve : sujetsAModifier) {
            sujetTrouve.setEtat(EtatSujet.ATTRIBUE);
            this.sujetDAO.modifier(sujetTrouve);
        }

        /* Logger avec le résultat de la création d'équipe */
        resultat = "Succès de la création d'équipe.";
        logger.log(Level.INFO, resultat);
        request.setAttribute(ServletUtilitaire.ATT_RESULTAT, resultat);

        /*
         * Modification des attributs en session (instanciés dans Connexion.java) pour
         * que l'utilisateur n'ait plus accès au lien "créer équipe" dans la navbar Si
         * c'est un prof qui a créé l'équipe, le changement ne se fait pas.
         */
        HttpSession session = request.getSession();
        boolean dansEquipe = verificationEtudiantOuProfesseur(session);
        session.setAttribute(ServletUtilitaire.ATT_SESSION_ETUDIANT_EQUIPE, dansEquipe);

        /* Redirection vers la page d'accueil */
        response.sendRedirect(request.getContextPath() + ServletUtilitaire.VUE_DASHBOARD);
    }

    private void creerLogger(HttpServletRequest request, HttpServletResponse response, int annee, Map<String, String> erreurs) throws ServletException, IOException {
        String resultat;
        resultat = ECHEC;
        logger.log(Level.WARN,
                resultat + erreurs.get(ServletUtilitaire.CHAMP_NBR_MIN_ELEVES)
                        + erreurs.get(ServletUtilitaire.CHAMP_NBR_MAX_ELEVES)
                        + erreurs.get(ServletUtilitaire.CHAMP_CONTRAT_PRO)
                        + erreurs.get(ServletUtilitaire.CHAMP_OPTIONS));
        request.setAttribute(ServletUtilitaire.ATT_RESULTAT, resultat);
        request.setAttribute(ServletUtilitaire.ATT_ERREURS, erreurs);

        /* On génère à nouveau les listes d'étudiants et de sujets avec leurs options */
        List<String> options = new ArrayList<>();
        List<Sujet> listeSujetsPublies = this.listerSujetsDisponibles();
        List<String> optionsSujet = this.listerOptionsSujets(listeSujetsPublies, options);
        List<Utilisateur> listeEtudiantsDispos = this.utilisateurDAO.listerEtudiantsDisponibles();
        List<String> optionsEtudiant = this.listerOptionsEtudiants(listeEtudiantsDispos, options);
        Etudiant etudiant = new Etudiant();
        etudiant.setAnnee(annee);
        List<Etudiant> listeEtudiantsI3 = this.etudiantDAO.trouver(etudiant);
        request.setAttribute(ServletUtilitaire.ATT_OPTIONS_ETUDIANT, optionsEtudiant);
        request.setAttribute(ServletUtilitaire.ATT_OPTIONS_SUJET, optionsSujet);
        request.setAttribute(ServletUtilitaire.ATT_LISTE_OPTIONS, options);
        request.setAttribute(ServletUtilitaire.ATT_LISTE_SUJETS, listeSujetsPublies);
        request.setAttribute(ServletUtilitaire.ATT_LISTE_ETUDIANTS_DISPOS, listeEtudiantsDispos);
        request.setAttribute(ServletUtilitaire.ATT_LISTE_ETUDIANTS_I3, listeEtudiantsI3);

        /* Ré-affichage du formulaire avec les erreurs */
        this.getServletContext().getRequestDispatcher(VUE_FORM).forward(request, response);
    }

    private void genererAffichage(HttpServletRequest request, HttpServletResponse response, int annee) throws ServletException, IOException {
        Map<String, String> erreurs = new HashMap<>();
        erreurs.put(ServletUtilitaire.CHAMP_MEMBRE, "Attention ! Il faut au moins un membre dans l'équipe !");
        String resultat = ECHEC;
        logger.log(Level.WARN, resultat + erreurs.get(ServletUtilitaire.CHAMP_MEMBRE));
        request.setAttribute(ServletUtilitaire.ATT_RESULTAT, resultat);
        request.setAttribute(ServletUtilitaire.ATT_ERREURS, erreurs);

        /* On génère à nouveau les listes d'étudiants et de sujets avec leurs options */
        List<Sujet> listeSujetsPublies = this.listerSujetsDisponibles();
        List<String> options = new ArrayList<>();
        List<String> optionsSujet = this.listerOptionsSujets(listeSujetsPublies, options);
        Etudiant etudiant = new Etudiant();
        etudiant.setAnnee(annee);
        List<Etudiant> listeEtudiantsI3 = this.etudiantDAO.trouver(etudiant);
        List<Utilisateur> listeEtudiantsDispos = this.utilisateurDAO.listerEtudiantsDisponibles();
        List<String> optionsEtudiant = this.listerOptionsEtudiants(listeEtudiantsDispos, options);
        request.setAttribute(ServletUtilitaire.ATT_LISTE_SUJETS, listeSujetsPublies);
        request.setAttribute(ServletUtilitaire.ATT_OPTIONS_ETUDIANT, optionsEtudiant);
        request.setAttribute(ServletUtilitaire.ATT_OPTIONS_SUJET, optionsSujet);
        request.setAttribute(ServletUtilitaire.ATT_LISTE_OPTIONS, options);
        request.setAttribute(ServletUtilitaire.ATT_LISTE_ETUDIANTS_DISPOS, listeEtudiantsDispos);
        request.setAttribute(ServletUtilitaire.ATT_LISTE_ETUDIANTS_I3, listeEtudiantsI3);

        /* Ré-affichage du formulaire avec les erreurs */
        this.getServletContext().getRequestDispatcher(VUE_FORM).forward(request, response);
    }


    /**
     * Liste tous les sujets disponibles pour le doGet et le doPost.
     *
     * @return
     */
    private List<Sujet> listerSujetsDisponibles() {
        /* Recherche de tous les sujets validés via sujetDao */
        Sujet sujetPublie = new Sujet();
        EtatSujet etatSujet = EtatSujet.PUBLIE;
        sujetPublie.setEtat(etatSujet);
        return this.sujetDAO.trouver(sujetPublie);
    }

    /**
     * Liste toutes les options des sujets disponibles pour le doGet et le doPost.
     *
     * @param listeSujetsPublies
     * @param options
     * @return
     */
    private List<String> listerOptionsSujets(List<Sujet> listeSujetsPublies, List<String> options) {
        /* Recherche des options du sujet */
        List<String> optionsSujet = new ArrayList<>();
        for (Sujet sujet : listeSujetsPublies) {
            List<OptionESEO> recupOptions = this.optionESEODAO.trouverOptionSujet(sujet);
            StringBuilder optionSujet = new StringBuilder();
            for (OptionESEO option : recupOptions) {
                optionSujet.append(option.getNomOption()).append(" ");
                if (!options.contains(option.getNomOption())) {
                    options.add(option.getNomOption());
                }
            }
            optionsSujet.add(optionSujet.toString());
        }
        return optionsSujet;
    }

    /**
     * Liste toutes les options des étudiants disponibles pour le doGet et le
     * doPost.
     *
     * @param listeEtudiantsDispos
     * @param options
     * @return
     */
    private List<String> listerOptionsEtudiants(List<Utilisateur> listeEtudiantsDispos, List<String> options) {
        /* Recherche des options des étudiants */
        List<String> optionsEtudiant = new ArrayList<>();
        for (Utilisateur utilisateur : listeEtudiantsDispos) {
            List<OptionESEO> recupOptions = this.optionESEODAO.trouverOptionUtilisateur(utilisateur);
            StringBuilder optionUtilisateur = new StringBuilder();
            for (OptionESEO option : recupOptions) {
                optionUtilisateur.append(option.getNomOption()).append(" ");
                if (!options.contains(option.getNomOption())) {
                    options.add(option.getNomOption());
                }
            }
            optionsEtudiant.add(optionUtilisateur.toString());
        }
        return optionsEtudiant;
    }

    /**
     * Liste les id des options du sujet passé en paramètre pour le doPost
     *
     * @param optionsSujetChoisi
     * @return
     */
    private List<Long> listerIdOptionsSujet(List<OptionESEO> optionsSujetChoisi) {
        List<Long> idOptionsSujetChoisi = new ArrayList<>();
        for (OptionESEO optionsSujetChoisiTrouve : optionsSujetChoisi) {
            idOptionsSujetChoisi.add(optionsSujetChoisiTrouve.getIdOption());
        }
        return idOptionsSujetChoisi;
    }

    /**
     * Ajoute l'option du membre à la liste des options passée en paramètre
     *
     * @param idOptionsMembres
     * @param idMembre
     * @return
     */
    private List<Long> optionsMembres(List<Long> idOptionsMembres, Long idMembre) {
        Utilisateur utilisateurMembre = new Utilisateur();
        utilisateurMembre.setIdUtilisateur(idMembre);
        List<OptionESEO> optionDuMembre = this.optionESEODAO.trouverOptionUtilisateur(utilisateurMembre);
        for (OptionESEO optionUtilisateur : optionDuMembre) {
            idOptionsMembres.add(optionUtilisateur.getIdOption());
        }
        return idOptionsMembres;
    }

    /**
     * Ajoute l'attribut contrat pro du membre à la liste des attributs
     *
     * @param contratProMembres
     * @param idMembre
     * @return
     */
    private List<String> contratProMembres(List<String> contratProMembres, Long idMembre) {
        Etudiant etudiantMembre = new Etudiant();
        etudiantMembre.setIdEtudiant(idMembre);
        List<Etudiant> etudiantsMembre = this.etudiantDAO.trouver(etudiantMembre);
        for (Etudiant etu : etudiantsMembre) {
            contratProMembres.add(etu.getContratPro());
        }
        return contratProMembres;
    }

    /**
     * Vérifie la correspondance sujet / membres pour la taille de l'équipe et le
     * contrat pro
     *
     * @param taille
     * @param nbrMinElevesSujetChoisi
     * @param nbrMaxElevesSujetChoisi
     * @param contratProSujetChoisi
     * @param contratProMembres
     * @return
     */
    private Map<String, String> verifierCorrespondanceTailleContratPro(int taille, int nbrMinElevesSujetChoisi,
                                                                       int nbrMaxElevesSujetChoisi, boolean contratProSujetChoisi, List<String> contratProMembres) {
        Map<String, String> erreurs = new HashMap<>();
        if (taille < nbrMinElevesSujetChoisi) {
            erreurs.put(ServletUtilitaire.CHAMP_NBR_MIN_ELEVES,
                    "Ce sujet nécessite un minimum de " + nbrMinElevesSujetChoisi + " étudiants. ");
        }
        if (taille > nbrMaxElevesSujetChoisi) {
            erreurs.put(ServletUtilitaire.CHAMP_NBR_MAX_ELEVES,
                    "Le nombre maximum d'étudiants autorisés sur ce sujet est de " + nbrMaxElevesSujetChoisi + ". ");
        }

        if (contratProSujetChoisi) {
            boolean correspondanceContratPro = true;
            for (String contratPro : contratProMembres) {
                if ("non".equals(contratPro)) {
                    correspondanceContratPro = false;
                }
            }
            if (!correspondanceContratPro) {
                erreurs.put(ServletUtilitaire.CHAMP_CONTRAT_PRO,
                        "Certains élèves de l'équipe créée ne sont pas en contrat pro. ");
            }
        }
        return erreurs;
    }

    /**
     * Vérifie la correspondance sujet / membres pour les options
     *
     * @param erreurs
     * @param idOptionsMembres
     * @param idOptionsSujetChoisi
     * @param optionsSujetChoisi
     * @return
     */
    private Map<String, String> verifierCorrespondanceOptions(Map<String, String> erreurs, List<Long> idOptionsMembres,
                                                              List<Long> idOptionsSujetChoisi, List<OptionESEO> optionsSujetChoisi) {
        boolean correspondanceOptions = true;
        for (Long optionDuMembre : idOptionsMembres) {
            boolean present = false;
            for (Long idOptionSujet : idOptionsSujetChoisi) {
                if (optionDuMembre.equals(idOptionSujet)) {
                    present = true;
                }
            }
            if (!present) {
                correspondanceOptions = false;
            }
        }
        if (!correspondanceOptions) {
            StringBuilder erreur = new StringBuilder("Les options des étudiants choisis ne correspondent pas aux options du sujet choisi qui sont : ");
            int nombreOptions = 1;
            for (OptionESEO nomOptionSujet : optionsSujetChoisi) {
                if (nombreOptions > 1) {
                    erreur.append(", ");
                }
                erreur.append(nomOptionSujet.getNomOption());
                nombreOptions++;
            }
            erreur.append(".");
            erreurs.put(ServletUtilitaire.CHAMP_OPTIONS, erreur.toString());
        }
        return erreurs;
    }

    private boolean verificationEtudiantOuProfesseur(HttpSession session) {
        boolean dansEquipe = false;
        Long idRoleEtudiant = 1L;
        List<Role> rolesUtilisateur = (List<Role>) session.getAttribute(ServletUtilitaire.ATT_SESSION_ROLES);
        for (Role role : rolesUtilisateur) {
            if (idRoleEtudiant.equals(role.getIdRole())) {
                dansEquipe = true;
            }
        }
        return dansEquipe;
    }

}