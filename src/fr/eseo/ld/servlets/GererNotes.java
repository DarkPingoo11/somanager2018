package fr.eseo.ld.servlets;

import fr.eseo.ld.beans.*;
import fr.eseo.ld.dao.*;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Servlet implementation class GererNotes
 */
@WebServlet("/GererNotes")
public class GererNotes extends HttpServlet {

	private static final long serialVersionUID = 1L;

	private static final String VUE_FORM = "/WEB-INF/formulaires/gererNotes.jsp";
	private static final String ATT_ETUDIANT_MANQ = "etuManq";
	private static final String ATT_NOTE_MANQ = "noteManq";
	private static final String ATT_PROF_MANQ = "profManq";

	private static Logger logger = Logger.getLogger(GererNotes.class.getName());

	private EtudiantDAO etudiantDAO;
	private UtilisateurDAO utilisateurDAO;
	private EquipeDAO equipeDAO;
	private JuryPosterDAO juryPosterDAO;
	private JurySoutenanceDAO jurySoutenanceDAO;
	private NotePosterDAO notePosterDAO;
	private SoutenanceDAO soutenanceDAO;
	private NoteSoutenanceDAO noteSoutenanceDAO;
	private List<Etudiant> etudiants;
	private List<Utilisateur> utilisateurs;
	private List<NotePoster> notesPoster;
	private List<NoteSoutenance> notesSoutenance;

	@Override
	public void init() throws ServletException {
		/* Récupération d'instances de DAO */
		DAOFactory daoFactory = (DAOFactory) this.getServletContext().getAttribute(ServletUtilitaire.CONF_DAO_FACTORY);
		this.etudiantDAO = daoFactory.getEtudiantDao();
		this.utilisateurDAO = daoFactory.getUtilisateurDao();
		this.equipeDAO = daoFactory.getEquipeDAO();
		this.juryPosterDAO = daoFactory.getJuryPosterDAO();
		this.notePosterDAO = daoFactory.getNotePosterDao();
		this.jurySoutenanceDAO = daoFactory.getJurySoutenanceDAO();
		this.soutenanceDAO = daoFactory.getSoutenanceDAO();
		this.noteSoutenanceDAO = daoFactory.getNoteSoutenanceDAO();
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		this.etudiants = this.etudiantDAO.lister();
		this.utilisateurs = this.utilisateurDAO.lister();
		this.notesPoster = this.notePosterDAO.lister();
		this.notesSoutenance = this.noteSoutenanceDAO.lister();
		rechercheNotes(request);

		trouverNotesManquantes(request);

		this.getServletContext().getRequestDispatcher(VUE_FORM).forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		this.etudiants = this.etudiantDAO.lister();
		this.utilisateurs = this.utilisateurDAO.lister();

		String stringCSV = creationCSV(request);

		response.setContentType("text/csv;charset=UTF-8");
		response.setCharacterEncoding("UTF-8");
		response.setHeader("Content-Disposition", "attachment; filename=\"notes_Etudiants_PFE.csv\"; charset=UTF-8");

		try {
			OutputStream outputStream = response.getOutputStream();
			outputStream.write(stringCSV.getBytes());
			outputStream.flush();
			outputStream.close();
		} catch (Exception e) {
			logger.log(Level.FATAL, "Impossible de créer le fichier d'export de notes", e);
		}
	}

	/**
	 * Methode qui permet de débloquer les checkBox du jsp si au moins un étudiant
	 * possède une des 4 notes
	 * 
	 * @param request
	 *            la requete pour modifier les différents champs d'affichage du jsp
	 */
	private void rechercheNotes(HttpServletRequest request) {

		/*
		 * Pour chaque étudiant on check s'il a été noté ou pas. S'il a été noté, alors
		 * on remplit l'attribut DISPO_NOTES qui permettra au prof de cocher la
		 * checkBox. Sinon, on remplit l'attribut TOUTES_NOTES pour spécifier au prof
		 * que s'il exporte les notes, il en manque pour certains.
		 */
		for (Etudiant etudiant : this.etudiants) {
			if (etudiant.getNoteIntermediaire() != -1) {
				request.setAttribute(ServletUtilitaire.ATT_DISPO_NOTES_INTERMEDIAIRE, "oui");
			} else {
				request.setAttribute(ServletUtilitaire.ATT_TOUTES_NOTES_INTERMEDIAIRE, "non");
			}
			if (etudiant.getNoteProjet() != -1) {
				request.setAttribute(ServletUtilitaire.ATT_DISPO_NOTES_PROJET, "oui");
			} else {
				request.setAttribute(ServletUtilitaire.ATT_TOUTES_NOTES_PROJET, "non");
			}
			if (etudiant.getNotePoster() != -1) {
				request.setAttribute(ServletUtilitaire.ATT_DISPO_NOTES_POSTER, "oui");
			} else {
				request.setAttribute(ServletUtilitaire.ATT_TOUTES_NOTES_POSTER, "non");
			}
			if (etudiant.getNoteSoutenance() != -1) {
				request.setAttribute(ServletUtilitaire.ATT_DISPO_NOTES_SOUTENANCE, "oui");
			} else {
				request.setAttribute(ServletUtilitaire.ATT_TOUTES_NOTES_SOUTENANCE, "non");
			}
		}
	}

	/**
	 * Méthode qui va charger tous les étudiants pour lesquels la note est manquante
	 * 
	 * @param request
	 *            la requete pour charger les attributs
	 */
	private void trouverNotesManquantes(HttpServletRequest request) {

		List<Utilisateur> etuNote = new ArrayList<>();
		List<String> typeNote = new ArrayList<>();
		List<Utilisateur> profNote = new ArrayList<>();

		for (Etudiant etudiant : this.etudiants) {
			/*
			 * Si un certain type de note à été entré pour un étudiant, alors pour tous les
			 * autres étudiants on va aller chercher le professeur qui n'a pas entré sa note
			 */
			if ("non".equals(request.getAttribute(ServletUtilitaire.ATT_TOUTES_NOTES_INTERMEDIAIRE))
					&& "oui".equals(request.getAttribute(ServletUtilitaire.ATT_DISPO_NOTES_INTERMEDIAIRE))
					&& etudiant.getNoteIntermediaire() < 0) {
				this.mapRequest(etuNote, profNote, typeNote, etudiant,
						trouverProfesseurReferent(etudiant.getIdEtudiant()), "Note Intermédiaire");
			}
			if ("non".equals(request.getAttribute(ServletUtilitaire.ATT_TOUTES_NOTES_PROJET))
					&& "oui".equals(request.getAttribute(ServletUtilitaire.ATT_DISPO_NOTES_PROJET))
					&& etudiant.getNoteProjet() < 0) {
				this.mapRequest(etuNote, profNote, typeNote, etudiant,
						trouverProfesseurReferent(etudiant.getIdEtudiant()), "Note Projet");
			}
			if ("non".equals(request.getAttribute(ServletUtilitaire.ATT_TOUTES_NOTES_POSTER))
					&& "oui".equals(request.getAttribute(ServletUtilitaire.ATT_DISPO_NOTES_POSTER))
					&& etudiant.getNotePoster() < 0) {
				this.mapRequest(etuNote, profNote, typeNote, etudiant,
						trouverProfesseurPoster(etudiant.getIdEtudiant()), "Note Poster");
			}
			if ("non".equals(request.getAttribute(ServletUtilitaire.ATT_TOUTES_NOTES_SOUTENANCE))
					&& "oui".equals(request.getAttribute(ServletUtilitaire.ATT_DISPO_NOTES_SOUTENANCE))
					&& etudiant.getNoteSoutenance() < 0) {
				this.mapRequest(etuNote, profNote, typeNote, etudiant,
						trouverProfesseurSoutenance(etudiant.getIdEtudiant()), "Note Soutenance");
			}
		}
		request.setAttribute(ATT_ETUDIANT_MANQ, etuNote);
		request.setAttribute(ATT_NOTE_MANQ, typeNote);
		request.setAttribute(ATT_PROF_MANQ, profNote);
	}

	/**
	 * Méthode qui ajoute aux listes que l'on va envoyer au jsp, ce qu'il faut pour
	 * un prof correspondant
	 * 
	 * @param etuNote
	 *            la liste d'étudiants qui n'ont pas encore de note
	 * @param profNote
	 *            la liste de profs qui n'ont pas entré leur note
	 * @param typeNote
	 *            la liste de types de note manquante
	 * @param etudiant
	 *            l'étudiant pour le quel on veut ajouter une note manquante
	 * @param profs
	 *            la liste des profs qui n'ont pas noté l'étudiant
	 * @param type
	 *            le type de note manquante
	 */
	private void mapRequest(List<Utilisateur> etuNote, List<Utilisateur> profNote, List<String> typeNote,
			Etudiant etudiant, List<Utilisateur> profs, String type) {
		for (Utilisateur prof : profs) {
			etuNote.add(ajouterUtilisateur(etudiant.getIdEtudiant()));
			typeNote.add(type);
			profNote.add(prof);
		}
	}

	/**
	 * Methode qui va créer le String qui va être utiliser pour générer la fichier
	 * .csv
	 * 
	 * @param request
	 *            la requete pour savoir les notes que l'utilisateur veut exporter
	 * @return resultat un string qui contient tout le fichier csv
	 */
	private String creationCSV(HttpServletRequest request) {
		/*
		 * On initialise le string, en ajoutant les notes que l'utilisateur veut
		 */
		StringBuilder resultat = new StringBuilder("Nom; Prenom");

		if (request.getParameter(ServletUtilitaire.CHAMP_NOTES_INTERMEDIAIRE) != null) {
			resultat.append("; Note Intermédiaire");
		}
		if (request.getParameter(ServletUtilitaire.CHAMP_NOTES_POSTER) != null) {
			resultat.append("; Note Poster");
		}
		if (request.getParameter(ServletUtilitaire.CHAMP_NOTES_PROJET) != null) {
			resultat.append("; Note Projet");

		}
		if (request.getParameter(ServletUtilitaire.CHAMP_NOTES_SOUTENANCE) != null) {
			resultat.append("; Note Soutenance");

		}
		resultat.append("\n");
		/*
		 * pour chaque étudiant on va ajouter la ligne correspondante à notre fichier
		 * csv avec ses notes
		 */
		for (Etudiant etudiant : this.etudiants) {
			for (Utilisateur utilisateur : this.utilisateurs) {
				if (etudiant.getIdEtudiant().compareTo(utilisateur.getIdUtilisateur()) == 0) {
					resultat.append(this.ajouterLigneEtudiant(request, etudiant, utilisateur));
				}
			}
		}
		return String.valueOf(resultat);
	}

	/**
	 * Methode qui créée une ligne csv correspondant à un étudiant donné
	 * 
	 * @param request
	 *            la requete pour savoir les notes que l'utilisateur veut exporter
	 * @param etudiant
	 *            l'étudiant pour le quel nous voulons inscrire les notes
	 * @param utilisateur
	 *            l'utilisateur correspondant à l'étudiant pour récupérer nom,
	 *            prenom
	 * @return resultat la ligne csv correspondante à l'étudiant et ses notes
	 */
	private String ajouterLigneEtudiant(HttpServletRequest request, Etudiant etudiant, Utilisateur utilisateur) {

		String resultat = utilisateur.getNom() + "; " + utilisateur.getPrenom();
		/*
		 * On va ajouter les notes correspondantes à l'étudiant suivant ce que
		 * l'utilisateur a choisit d'exporter
		 */
		if (request.getParameter(ServletUtilitaire.CHAMP_NOTES_INTERMEDIAIRE) != null) {
			resultat += "; " + etudiant.getNoteIntermediaire();
		}
		if (request.getParameter(ServletUtilitaire.CHAMP_NOTES_POSTER) != null) {
			resultat += "; " + etudiant.getNotePoster();
		}
		if (request.getParameter(ServletUtilitaire.CHAMP_NOTES_PROJET) != null) {
			resultat += "; " + etudiant.getNoteProjet();
		}
		if (request.getParameter(ServletUtilitaire.CHAMP_NOTES_SOUTENANCE) != null) {
			resultat += "; " + etudiant.getNoteSoutenance();
		}
		resultat += "\n";

		return resultat;
	}

	/**
	 * Méthode qui retourne l'utilisateur correspondant à l'id que l'on entre en
	 * paramètre
	 * 
	 * @param id
	 *            l'id de l'utilisteur que l'on veut ajouter
	 * @return utilisateur l'utilisateur correspondant
	 */
	private Utilisateur ajouterUtilisateur(Long id) {
		Utilisateur utilisateur = new Utilisateur();
		for (Utilisateur user : this.utilisateurs) {
			if (user.getIdUtilisateur().compareTo(id) == 0) {
				utilisateur = user;
				break;
			}
		}
		return utilisateur;
	}

	/**
	 * Methode qui permet de trouver les prof qui font partie du jury poster de
	 * l'étudiant
	 * 
	 * @param idEtudiant
	 *            l'id de l'étudiant
	 * @return profs la liste des profs du jury
	 */
	private List<Utilisateur> trouverProfesseurPoster(Long idEtudiant) {
		List<Utilisateur> profs = new ArrayList<>();
		/*
		 * On récupère l'équipe de l'étudiant
		 */
		Equipe equipe = this.equipeDAO.trouver(idEtudiant);
		/*
		 * Si on cherche le ou les profs qui n'ont pas encore rentré leur note poster
		 * pour l'étudiant
		 */
		if (equipe != null) {
			/*
			 * On crée le jury qui note l'équipe de l'étudiant pour voir quel prof n'a pas
			 * entré sa ou ses notes
			 */
			JuryPoster jury = new JuryPoster();
			jury.setIdEquipe(equipe.getIdEquipe());
			List<JuryPoster> jurys = this.juryPosterDAO.trouver(jury);
			/*
			 * Si le jury n'éxiste pas alors on ne fait rien, sinon on va chercher quel prof
			 * n'a pas noté
			 */
			if (!jurys.isEmpty()) {
				jury = jurys.get(0);
				boolean note1 = false;
				boolean note2 = false;
				boolean note3 = false;
				/*
				 * Pour chaque note existante on regarde si elle correspond à un des profs
				 */
				for (NotePoster note : this.notesPoster) {
					note1 = compareNoteJury(note.getIdProfesseur(), jury.getIdProf1());
					note2 = compareNoteJury(note.getIdProfesseur(), jury.getIdProf2());
					note3 = compareNoteJury(note.getIdProfesseur(), jury.getIdProf3());
				}
				/*
				 * Si le prof n'a pas entré sa note on l'ajoute à la liste
				 */
				if (!note1) {
					profs.add(this.ajouterUtilisateur(jury.getIdProf1()));
				}
				if (!note2) {
					profs.add(this.ajouterUtilisateur(jury.getIdProf2()));
				}
				if (!note3) {
					profs.add(this.ajouterUtilisateur(jury.getIdProf3()));
				}
			}
		}
		return profs;
	}

	/**
	 * Methode qui permet de trouver les prof qui font partie du jury de soutenance
	 * de l'étudiant
	 * 
	 * @param idEtudiant
	 *            l'id de l'étudiant
	 * @return profs la liste des profs du jury
	 */
	private List<Utilisateur> trouverProfesseurSoutenance(Long idEtudiant) {
		List<Utilisateur> profs = new ArrayList<>();
		/*
		 * On récupère l'équipe de l'étudiant
		 */
		Equipe equipe = this.equipeDAO.trouver(idEtudiant);
		/*
		 * Si on cherche le ou les profs qui n'ont pas encore rentré leur note
		 * soutenance pour l'étudiant
		 */
		if (equipe != null) {
			/*
			 * On crée le jury qui note l'équipe de l'étudiant pour voir quel prof n'a pas
			 * entré sa ou ses notes
			 */
			Sujet sujet = new Sujet();
			sujet.setIdSujet(equipe.getIdSujet());
			Soutenance soutenance = this.soutenanceDAO.trouverSoutenanceSujet(sujet);
			JurySoutenance jury = new JurySoutenance();
			jury.setIdJurySoutenance(soutenance.getIdJurySoutenance());
			List<JurySoutenance> jurys = this.jurySoutenanceDAO.trouver(jury);
			/*
			 * Si le jury n'éxiste pas alors on ne fait rien, sinon on va chercher quel prof
			 * n'a pas noté
			 */
			if (!jurys.isEmpty()) {
				jury = jurys.get(0);
				boolean note1 = false;
				boolean note2 = false;
				/*
				 * Pour chaque note existante on regarde si elle correspond à un des profs
				 */
				for (NoteSoutenance note : this.notesSoutenance) {
					note1 = compareNoteJury(note.getIdProfesseur(), jury.getIdProf1());
					note2 = compareNoteJury(note.getIdProfesseur(), jury.getIdProf2());
				}
				/*
				 * Si le prof n'a pas entré sa note on l'ajoute à la liste
				 */
				if (!note1) {
					profs.add(this.ajouterUtilisateur(jury.getIdProf1()));
				}
				if (!note2) {
					profs.add(this.ajouterUtilisateur(jury.getIdProf2()));
				}
			}
		}
		return profs;
	}

	/**
	 * Methode qui compare l'id d'un prof de jury et d'un prof, renvoie true si
	 * elles correspondent sinon renvoie false
	 * 
	 * @param idProf
	 *            l'id du prof que l'ont veut comparer
	 * @param idProfJury
	 *            l'id du prof dans le jury
	 * @return correspond le booléen
	 */
	private boolean compareNoteJury(Long idProf, Long idProfJury) {
		boolean correspond = false;
		if (idProf.compareTo(idProfJury) == 0) {
			correspond = true;
		}
		return correspond;
	}

	/**
	 * Methode qui renvoie le professeur référent d'un sujet
	 * 
	 * @param idEtudiant
	 * @return
	 */
	private List<Utilisateur> trouverProfesseurReferent(Long idEtudiant) {
		List<Utilisateur> profs = new ArrayList<>();
		/*
		 * On récupère l'équipe de l'étudiant
		 */
		Equipe equipe = this.equipeDAO.trouver(idEtudiant);
		/*
		 * Si on cherche le ou les profs qui n'ont pas encore rentré leur note
		 * intermédiaire ou de projet pour l'étudiant
		 */
		if (equipe != null) {
			Sujet sujet = new Sujet();
			sujet.setIdSujet(equipe.getIdSujet());
			/*
			 * On récupère tous les profs qui sont en lien avec le sujet
			 */
			List<ProfesseurSujet> profsSujet = this.utilisateurDAO.trouverProfesseursAttribuesAUnSujet(sujet);
			for (ProfesseurSujet profSujet : profsSujet) {
				/*
				 * Pour chaque prof, si il est référent on retourne le bean utilisateur
				 * correspondant à son id
				 */
				if (profSujet.getFonction().compareTo(FonctionProfesseurSujet.REFERENT) == 0) {
					profs.add(this.ajouterUtilisateur(profSujet.getIdProfesseur()));
				}
			}
		}
		return profs;
	}

}