package fr.eseo.ld.servlets;

import fr.eseo.ld.beans.*;
import fr.eseo.ld.dao.*;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * Servlet implementation class Trombinoscope
 */
@WebServlet("/Trombinoscope")
@MultipartConfig
public class Trombinoscope extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private static final String VUE_FORM = "/WEB-INF/formulaires/trombinoscope.jsp";
	private static final String CHEMIN_FICHIER = "/images/users";
	private static final String CHAMP_ERREUR_EXTENSION = "extensionErreur";
	private static final String CHAMP_ERREUR = "erreurs";


	private UtilisateurDAO utilisateurDAO;
	private OptionESEODAO optionESEODAO;
	private EquipeDAO equipeDAO;
	private PglEquipeDAO pglEquipeDAO;
	private AnneeScolaireDAO anneeScolaireDAO;

	private static Logger logger = Logger.getLogger(Trombinoscope.class.getName());

	@Override
	public void init() throws ServletException {
		/* Recuperation d'instances de DAO */
		DAOFactory daoFactory = (DAOFactory) this.getServletContext().getAttribute(ServletUtilitaire.CONF_DAO_FACTORY);
		this.utilisateurDAO = daoFactory.getUtilisateurDao();
		this.optionESEODAO = daoFactory.getOptionESEODAO();
		this.equipeDAO = daoFactory.getEquipeDAO();
		this.pglEquipeDAO = daoFactory.getPglEquipeDAO();
		this.anneeScolaireDAO = daoFactory.getAnneeScolaireDAO();
	}

	public UtilisateurDAO getUtilisateurDAO() {
		return this.utilisateurDAO;
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		/* Liste de tous les étudiants disponibles */
		List<Utilisateur> listeEtudiants = getUtilisateurDAO().listerEtudiants();
		List<Utilisateur> listeEtudiantsI2 = getUtilisateurDAO().listerEtudiantsI2();

		/* Récupération de la session depuis la requête */
		HttpSession session = request.getSession();

		/* Récupération de l'ID de l'Utilisateur en session */
		Utilisateur utilisateur = (Utilisateur) session.getAttribute(ServletUtilitaire.ATT_SESSION_USER);

		/* Liste de toutes les options des étudiants */
		List<String> options = new ArrayList<>();
		List<String> optionsEtudiant = new ArrayList<>();

		List<RoleUtilisateur> optionsEtIds = new ArrayList<>();
		List<String> optionsEtIdsString = new ArrayList<>();
		List<String> equipesEtIdsString = new ArrayList<>();

		/* Liste de toutes les options des étudiants I2 */
		List<String> optionsI2 = new ArrayList<>();
		List<String> optionsEtudiantI2 = new ArrayList<>();

		List<RoleUtilisateur> optionsEtIdsI2 = new ArrayList<>();
		List<String> optionsEtIdsStringI2 = new ArrayList<>();
		List<String> equipesEtIdsStringI2 = new ArrayList<>();

		for (Utilisateur util : listeEtudiants) {
			ajouterEtudiantsI3(options, optionsEtudiant, optionsEtIds, optionsEtIdsString, equipesEtIdsString, util);
		}

		for (Utilisateur utilI2 : listeEtudiantsI2) {
			ajouterEtudiantsPGL(optionsI2, optionsEtudiantI2, optionsEtIdsI2, optionsEtIdsStringI2, equipesEtIdsStringI2, utilI2);
		}

		/* Liste de toutes les equipes de l'annee en cours */
		Equipe equipeCree = new Equipe();
		
		AnneeScolaire anneeScolaire = new AnneeScolaire();
		anneeScolaire.setIdOption((long) 1);

		if (!this.anneeScolaireDAO.trouver(anneeScolaire).isEmpty()) {
			int annee = this.anneeScolaireDAO.trouver(anneeScolaire).get(0).getAnneeDebut();
			equipeCree.setAnnee(annee);
		}
		List<Equipe> listeEquipes = this.equipeDAO.trouver(equipeCree);
        List<fr.eseo.ld.pgl.beans.Equipe> listeEquipesI2 = new ArrayList<>();
        // Remplissage de la liste des Soutenance de sprint Review
        for (int i = 0; i < this.pglEquipeDAO.lister().size(); i++) {
            listeEquipesI2.add(this.pglEquipeDAO.lister().get(i));
        }

		/* Envoi des attributs */
		request.setAttribute(ServletUtilitaire.ATT_LISTE_OPTIONS, options);
		request.setAttribute(ServletUtilitaire.ATT_LISTE_OPTIONS_I2, optionsI2);
		request.setAttribute(ServletUtilitaire.ATT_LISTE_ETUDIANTS, listeEtudiants);
		request.setAttribute("listeEtudiantsI2", listeEtudiantsI2);
		request.setAttribute(ServletUtilitaire.ATT_LISTE_EQUIPES, listeEquipes);
		request.setAttribute("listeEquipesI2", listeEquipesI2);
		request.setAttribute("optionsEtIds", optionsEtIds);
		request.setAttribute("optionsEtIdsString", optionsEtIdsString);
		request.setAttribute("equipesEtIdsString", equipesEtIdsString);
		request.setAttribute("optionsEtIdsI2", optionsEtIdsI2);
		request.setAttribute("optionsEtIdsStringI2", optionsEtIdsStringI2);
		request.setAttribute("equipesEtIdsStringI2", equipesEtIdsStringI2);
		request.setAttribute("utilisateur", utilisateur);

		this.getServletContext().getRequestDispatcher(VUE_FORM).forward(request, response);
	}

	private void ajouterEtudiantsI3(List<String> options, List<String> optionsEtudiant, List<RoleUtilisateur> optionsEtIds, List<String> optionsEtIdsString, List<String> equipesEtIdsString, Utilisateur util) {
		RoleUtilisateur optionEtId = new RoleUtilisateur();
		String optionEtIdString = "";
		String equipeEtIdString = "";

		Equipe equipeEtu = this.equipeDAO.trouver(util.getIdUtilisateur());
		if (equipeEtu != null) {
			equipeEtIdString += equipeEtu.getIdEquipe();
		}

		List<OptionESEO> recupOptions = this.optionESEODAO.trouverOptionUtilisateur(util);
		StringBuilder stringBuilder = new StringBuilder();
		String optionUtilisateur = "";
		for (OptionESEO option : recupOptions) {
			stringBuilder.append(option.getNomOption() + " ");
			optionUtilisateur = stringBuilder.toString();
			if (!options.contains(option.getNomOption())) {
				options.add(option.getNomOption());
			}
			optionEtId.setIdOption(option.getIdOption());
			optionEtId.setIdUtilisateur(util.getIdUtilisateur());
			optionEtIdString = option.getNomOption();
		}
		optionsEtudiant.add(optionUtilisateur);
		optionsEtIds.add(optionEtId);
		optionsEtIdsString.add(optionEtIdString);
		equipesEtIdsString.add(equipeEtIdString);
	}

	private void ajouterEtudiantsPGL(List<String> optionsI2, List<String> optionsEtudiantI2, List<RoleUtilisateur> optionsEtIdsI2, List<String> optionsEtIdsStringI2, List<String> equipesEtIdsStringI2, Utilisateur utilI2) {
		RoleUtilisateur optionEtIdI2 = new RoleUtilisateur();
		String optionEtIdStringI2 = "";
		String equipeEtIdStringI2 = "";

		fr.eseo.ld.pgl.beans.Equipe equipeEtuI2 = this.pglEquipeDAO.trouverEqpEtu(utilI2.getIdUtilisateur());
		if (equipeEtuI2 != null) {
			equipeEtIdStringI2 += equipeEtuI2.getIdEquipe();
		}

		List<OptionESEO> recupOptionsI2 = this.optionESEODAO.trouverOptionUtilisateur(utilI2);
		StringBuilder stringBuilderI2 = new StringBuilder();
		String optionUtilisateurI2 = "";
		for (OptionESEO optionI2 : recupOptionsI2) {
			stringBuilderI2.append(optionI2.getNomOption() + " ");
			optionUtilisateurI2 = stringBuilderI2.toString();
			if (!optionsI2.contains(optionI2.getNomOption())) {
				optionsI2.add(optionI2.getNomOption());
			}
			optionEtIdI2.setIdOption(optionI2.getIdOption());
			optionEtIdI2.setIdUtilisateur(utilI2.getIdUtilisateur());
			optionEtIdStringI2 = optionI2.getNomOption();
		}
		optionsEtudiantI2.add(optionUtilisateurI2);
		optionsEtIdsI2.add(optionEtIdI2);
		optionsEtIdsStringI2.add(optionEtIdStringI2);
		equipesEtIdsStringI2.add(equipeEtIdStringI2);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		Map<String, String> erreurs = new HashMap<>();
		try {
			for (Part part : request.getParts()) {
				String fileName = ServletUtilitaire.extractFileName(part);
				fileName = new File(fileName).getName();
				if (!("").equals(fileName) && ("zip".equals(ServletUtilitaire.avoirExtension(fileName)))) {
					unzipFile(erreurs, part);
					// Si le fichier n'est pas un *.zip, génération d'une erreur
				} else {
					erreurs.put(CHAMP_ERREUR_EXTENSION, "Echec de l'importation : l'extension ne correspond pas à un fichier *.zip");
				}
			}
			request.setAttribute(CHAMP_ERREUR, erreurs);
			this.doGet(request, response);
		} catch (IOException | ServletException e) {
			logger.log(Level.FATAL, "Impossible de modifier l'image ", e);
		}
	}

	private void unzipFile(Map<String, String> erreurs, Part part) throws IOException {
		try (ZipInputStream flux = new ZipInputStream(part.getInputStream())) {
			erreurs.put(CHAMP_ERREUR_EXTENSION, "Réussite de l'importation.");
			byte[] buffer = new byte[1024];
			ZipEntry element = flux.getNextEntry();
			while (element != null) {
				String chemin = getServletContext().getRealPath(CHEMIN_FICHIER);
				String photoIdentifiant = recupererIdentifiant(element.getName());
				if (photoIdentifiant != null) {
					File photoUtilisateur = new File(chemin, photoIdentifiant + ".jpg");
					try (FileOutputStream fos = new FileOutputStream(photoUtilisateur)) {
						int len;
						while ((len = flux.read(buffer)) > 0) {
							fos.write(buffer, 0, len);
						}
					}
				}
				element = flux.getNextEntry();
			}
		}
	}

	protected String recupererIdentifiant(String nomFichier) {
		int i = nomFichier.lastIndexOf('.');
		if (i > 0 && i < nomFichier.length() - 1) {
			return this.utilisateurDAO.recupererIdSQL("SELECT idUtilisateur FROM Utilisateur WHERE identifiant='"+nomFichier.substring(0,i).toLowerCase()+"';").toString();
		} else {
			return null;
		}
	}

}