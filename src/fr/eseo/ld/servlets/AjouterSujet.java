package fr.eseo.ld.servlets;

import fr.eseo.ld.beans.*;
import fr.eseo.ld.dao.*;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static fr.eseo.ld.servlets.ServletUtilitaire.getInfosChoisies;

/**
 * Servlet permettant l'ajout d'un sujet par un utilisateur.
 */
@WebServlet("/AjouterSujet")
public class AjouterSujet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    private static final String VUE_FORM = "/WEB-INF/formulaires/ajouterSujet.jsp";
    private static final String[] CHAMP_CENTRES_INTERETS = {"JPO", "GP"};
    private static final String ATT_LIEN = "lienForm";

    private SujetDAO sujetDAO;
    private OptionESEODAO optionESEODAO;
    private NotificationDAO notificationDAO;
    private ProfesseurDAO professeurDAO;
    private UtilisateurDAO utilisateurDAO;

    @Override
    public void init() throws ServletException {
        /* Récupération d'instances de DAO */
        DAOFactory daoFactory = (DAOFactory) this.getServletContext().getAttribute(ServletUtilitaire.CONF_DAO_FACTORY);
        this.sujetDAO = daoFactory.getSujetDao();
        this.optionESEODAO = daoFactory.getOptionESEODAO();
        this.notificationDAO = daoFactory.getNotificationDao();
        this.professeurDAO = daoFactory.getProfesseurDAO();
        this.utilisateurDAO = daoFactory.getUtilisateurDao();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        /* A la réception d'une requête GET, simple affichage du formulaire */

        //Affichage de lien google
        Parametre paramLien = DepotGoogle.recupererLienFormulaireInDatabase();
        request.setAttribute(ATT_LIEN, paramLien != null ? paramLien.getValeur() : null);

        this.getServletContext().getRequestDispatcher(VUE_FORM).forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        /* Récupération de la session depuis la requête */
        HttpSession session = request.getSession();

        /* Récupération des données saisies */
        String titre = request.getParameter(ServletUtilitaire.CHAMP_TITRE);
        String description = request.getParameter(ServletUtilitaire.CHAMP_DESCRIPTION);
        String liens = request.getParameter(ServletUtilitaire.CHAMP_LIENS);
        Integer nbrMinEleves = Integer.valueOf(request.getParameter(ServletUtilitaire.CHAMP_NBR_MIN_ELEVES));
        Integer nbrMaxEleves = Integer.valueOf(request.getParameter(ServletUtilitaire.CHAMP_NBR_MAX_ELEVES));
        Boolean contratPro = Boolean.valueOf(request.getParameter(ServletUtilitaire.CHAMP_CONTRAT_PRO));
        Boolean confidentialite = Boolean.valueOf(request.getParameter(ServletUtilitaire.CHAMP_CONFIDENTIALITE));

        /* Validation des champs */
        Map<String, String> erreurs = new HashMap<>();
        if (nbrMinEleves > nbrMaxEleves) { // si le nombre d'élèves minimum est supérieur au nombre d'élèves maximum
            erreurs.put(ServletUtilitaire.CHAMP_NBR_MIN_ELEVES,
                    "Nombre minimum d'élèves supérieur au Nombre maximum d'élèves. ");
        }

        List<OptionESEO> options = this.optionESEODAO.lister();
        List<OptionESEO> optionsSujet = new ArrayList<>();
        for (OptionESEO option : options) {
            if (request.getParameter(option.getNomOption()) != null) {
                optionsSujet.add(option);
            }
        }
        if (optionsSujet.isEmpty()) { // si aucune option n'a été cochée
            erreurs.put(ServletUtilitaire.CHAMP_OPTIONS, "Aucune option sélectionnée. ");
        }

        /*
         * Si aucune erreur de validation n'a eu lieu, alors insertion du Sujet dans la
         * BDD, sinon ré-affichage du formulaire.
         */
        if (erreurs.isEmpty()) {
            /*
             * Concaténation des paramètres des boutons radio cochés pour les stocker dans
             * une seule variable
             */
            String centresInterets = getInfosChoisies(request, CHAMP_CENTRES_INTERETS);
            /* Insertion du sujet dans la BDD via sujetDAO */
            Sujet sujet = new Sujet();
            sujet.setTitre(titre);
            sujet.setDescription(description);
            sujet.setNbrMinEleves(nbrMinEleves);
            sujet.setNbrMaxEleves(nbrMaxEleves);
            sujet.setContratPro(contratPro);
            sujet.setConfidentialite(confidentialite);
            sujet.setEtat(EtatSujet.DEPOSE);
            sujet.setLiens(verificationLiens(liens));
            sujet.setInterets(centresInterets);
            this.sujetDAO.creer(sujet);

            /* Récupération du sujet avec l'ID auto-généré */
            sujet = sujetDAO.trouver(sujet).get(0);

            /* Insertion du porteur du sujet */
            Utilisateur utilisateur = (Utilisateur) session.getAttribute(ServletUtilitaire.ATT_SESSION_USER);
            Long idUtilisateur = utilisateur.getIdUtilisateur();
            this.sujetDAO.creerPorteurSujet(utilisateur, sujet);

            /* Attribution des options relatives au sujet */
            this.sujetDAO.attribuerOptionsSujet(sujet, optionsSujet);

            /*
             * Si le porteur du sujet est une entreprise exterieur, elle doit pouvoir avoir
             * une vision sur son sujet
             */
            List<Role> listeRole = this.utilisateurDAO.trouverRole(utilisateur);
            if (listeRole.size() == 1 && ("entrepriseExt").equals(listeRole.get(0).getNomRole())) {
                this.ajoutEntrepriseInteresse(sujet, utilisateur);
            }

            /* Ajout d'une notification */
            this.notificationDAO.ajouterNotification(idUtilisateur, "Un sujet a été ajouté à votre liste",
                    "VoirMesSujets");

            /* Redirection vers la page d'accueil */
            response.sendRedirect(request.getContextPath() + ServletUtilitaire.VUE_DASHBOARD);
        } else {
            String resultat = "Échec de l'ajout du sujet. ";
            request.setAttribute(ServletUtilitaire.ATT_RESULTAT, resultat);
            request.setAttribute(ServletUtilitaire.ATT_ERREURS, erreurs);
            /* Ré-affichage du formulaire avec les erreurs */
            this.getServletContext().getRequestDispatcher(VUE_FORM).forward(request, response);
        }
    }

    /**
     * Crée une entreprise exterieure intéresée : si la personne qui a rentré le
     * sujet est une entreprise exterieure, alors on doit la faire accéder à son
     * sujet en lui donnant le role "interesse". Elle pourra ainsi valider son
     * poster le moment venu.
     *
     * @param sujet       le sujet que l'entreprise vient de publier.
     * @param utilisateur le bean de l'entreprise ext.
     */
    private void ajoutEntrepriseInteresse(Sujet sujet, Utilisateur utilisateur) {
        Professeur prof = new Professeur();
        prof.setIdProfesseur(utilisateur.getIdUtilisateur());
        this.professeurDAO.creer(prof);
        prof = this.professeurDAO.trouver(prof).get(0);
        this.sujetDAO.creerProfesseurSujet(prof, sujet, FonctionProfesseurSujet.INTERESSE.getFonction(), "oui");
    }

    /**
     * Met les différents liens rentrés en paramètre pour être lisible par la base
     * de données
     *
     * @param liens
     * @return
     */
    private String verificationLiens(String liens) {
        String[] tableauDeLien = liens.split(";");
        StringBuilder resultat = new StringBuilder();
        for (String lien : tableauDeLien) {
            if (!lien.isEmpty()) {
                resultat.append(lien + ";");
            }
        }
        return String.valueOf(resultat);
    }

}