package fr.eseo.ld.servlets;

import fr.eseo.ld.beans.OptionESEO;
import fr.eseo.ld.beans.Role;
import fr.eseo.ld.beans.RoleAffichage;
import fr.eseo.ld.beans.Utilisateur;
import fr.eseo.ld.utils.IconUtilitaire;
import fr.eseo.ld.utils.NomUtilitaire;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Servlet permettant l'affichage de la barre de menu
 */
@WebServlet("/Navbar")
@MultipartConfig
public class Navbar extends HttpServlet {

    private static final String VUE_FORM = "/inc/sidebar.jsp";
    private static final String ATT_ROLES_AFFICHAGE = "rolesAffichage";

    @Override
    public void init() throws ServletException {
        /* Récupération d'instances de DAO */
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        HttpSession session = request.getSession();
        Utilisateur utilisateur = (Utilisateur) session.getAttribute(ServletUtilitaire.ATT_SESSION_USER);


        //Générer les variables associées aux rôles
        if(utilisateur != null) {
            List<Role> roles = (List<Role>) session.getAttribute(ServletUtilitaire.ATT_SESSION_ROLES);
            List<OptionESEO> options = (List<OptionESEO>) session.getAttribute(ServletUtilitaire.ATT_SESSION_OPTIONS);

            this.definirVariablesRoles(request, roles);
            this.definirAffichageRoles(request, roles, options);
        }

        request.getRequestDispatcher(VUE_FORM).include(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request, response);
    }
    /**
     * Défini la liste des rôles de l'utilisateur pour l'affichage
     * @param request HttpServletRequest
     * @param roles Roles de l'utilisateur
     * @param options Options de l'utilisateur
     */
    private void definirAffichageRoles(HttpServletRequest request, List<Role> roles, List<OptionESEO> options) {
        //Définition de l'objet d'affichage
        List<RoleAffichage> rolesAffichage = new ArrayList<>();

        for(int i = 0; i < roles.size(); i++) {
            OptionESEO option = options.get(i);
            Role role = roles.get(i);
            RoleAffichage roleAffichage = new RoleAffichage();

            //Définition des attributs
            roleAffichage.setNomRole(role.getNomRole());
            roleAffichage.setIdRole(role.getIdRole());
            roleAffichage.setIcone(IconUtilitaire.getIconForRole(role));
            roleAffichage.setNomComplet(NomUtilitaire.getNomCompletRole(role, option));

            rolesAffichage.add(roleAffichage);
        }

        //Définition de l'attribut
        request.setAttribute(ATT_ROLES_AFFICHAGE, rolesAffichage);
    }

    /**
     * Défini les variables de rôle
     * @param request HttpServletRequest
     * @param roles Roles de l'utilisateur
     */
    private void definirVariablesRoles(HttpServletRequest request, List<Role> roles) {
        //On défini un attribut du nom du role pour les accès
        for(Role r : roles) {
            request.setAttribute(r.getNomRole(), true);
        }
    }

}
