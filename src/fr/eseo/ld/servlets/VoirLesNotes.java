package fr.eseo.ld.servlets;

import fr.eseo.ld.beans.Utilisateur;
import fr.eseo.ld.dao.*;
import fr.eseo.ld.objets.exportexcel.ENJNotes;
import fr.eseo.ld.pgl.beans.Equipe;
import fr.eseo.ld.pgl.beans.Matiere;
import fr.eseo.ld.pgl.beans.Note;
import fr.eseo.ld.pgl.beans.Sprint;
import fr.eseo.ld.utils.ParamUtilitaire;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * Servlet implementation class VoirLesNotes
 */
@WebServlet("/VoirLesNotes")
public class VoirLesNotes extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final String VUE_FORM = "/WEB-INF/formulaires/voirLesNotes.jsp";

    private static final String ATT_MATIERE ="matieres";
    private static final String ATT_SPRINT_ANNEE = "sprintsAnnee";


    private static final String ATT_SPRINT_S = "sprintS";
    
    private static final String PARAM_SPRINT = "sprint";

    private PglEquipeDAO pglEquipeDAO;
    private SprintDAO sprintDAO;
    private PglNoteDAO noteDAO;
    private MatiereDAO matiereDAO;

    @Override
    public void init() throws ServletException {
        DAOFactory daoFactory = (DAOFactory) this.getServletContext().getAttribute(ServletUtilitaire.CONF_DAO_FACTORY);
        this.matiereDAO = daoFactory.getMatiereDAO();
        this.pglEquipeDAO = daoFactory.getPglEquipeDAO();
        this.sprintDAO = daoFactory.getSprintDAO();
        this.noteDAO = daoFactory.getPglNoteDAO();
    }

    
    /**
     * @see HttpServlet#HttpServlet()
     */
    public VoirLesNotes() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession session = request.getSession();
        Utilisateur utilisateur = (Utilisateur) session.getAttribute(ServletUtilitaire.ATT_SESSION_USER);
		//Afficher les sprints de l'année courante
        request.setAttribute(ATT_SPRINT_ANNEE, this.sprintDAO.recupererSprintAnnee(ParamUtilitaire.getAnneeCourante()));
		
        List<Matiere> matieres = this.matiereDAO.lister();
        request.setAttribute(ATT_MATIERE, matieres);
        List<Equipe> equipes = this.pglEquipeDAO.lister();
        request.setAttribute("equipes", equipes);
        
		 
		//RECUPERE L'ATTRIBUT 
		String sprint = request.getParameter(PARAM_SPRINT);
		
		// si le sprint est non null
		if(ParamUtilitaire.notNull(sprint)) {
			request.setAttribute(ATT_SPRINT_S, sprint);
			
			//recupere la liste de notes en fonction du filtre sprint spécifié
			List<Note> notes;
			Sprint sprintO = new Sprint();
            sprintO.setIdSprint(Long.valueOf(ParamUtilitaire.getInt(sprint)));
            Sprint sprintTrouve = this.sprintDAO.trouver(sprintO.getIdSprint());
            //si les notes sont publiées l'etudiant peut voir ses notes finales
			if(sprintTrouve.getNotesPubliees()) {
				notes = this.noteDAO.recupererNotesMoyennesPglUtilisateur(utilisateur, sprintO);
				request.setAttribute("notes", notes);
			}
		
		}
		//Voir les notes moyennes des autres équipes
		Map<Equipe, ENJNotes> map = DAOFactory.getInstance().getPglNoteDAO().recupererNotesMoyenneEquipeSprints(ParamUtilitaire.getAnneeCourante());
				
		request.setAttribute("map", map);
		request.setAttribute("listeSprint", sprint);
		
		this.getServletContext().getRequestDispatcher(VUE_FORM).forward(request, response);
	}

	

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
    @Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
