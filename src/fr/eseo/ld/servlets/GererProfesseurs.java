package fr.eseo.ld.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.eseo.ld.beans.Professeur;
import fr.eseo.ld.beans.ProfesseurSujet;
import fr.eseo.ld.beans.Sujet;
import fr.eseo.ld.beans.Utilisateur;
import fr.eseo.ld.dao.DAOFactory;
import fr.eseo.ld.dao.NotificationDAO;
import fr.eseo.ld.dao.SujetDAO;
import fr.eseo.ld.dao.UtilisateurDAO;

/**
 * Servlet permettant la gestion des professeurs.
 */
@WebServlet("/GererProfesseurs")
public class GererProfesseurs extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private static final String VUE_FORM = "/WEB-INF/formulaires/gererProfesseurs.jsp";

	private static final String ATT_ATTRIBUTION_REFRENT = "erreursAttributionReferents";
	private static final String ATT_BOUTON_RADIO = "optradio-";

	private static final String REFERENT = "REFERENT";

	private UtilisateurDAO utilisateurDAO;
	private SujetDAO sujetDAO;
	private NotificationDAO notificationDAO;

	private List<String> erreurs;
	private List<String> attributions;

	@Override
	public void init() throws ServletException {
		/* Récupération d'instances de DAO */
		DAOFactory daoFactory = (DAOFactory) this.getServletContext().getAttribute(ServletUtilitaire.CONF_DAO_FACTORY);
		this.utilisateurDAO = daoFactory.getUtilisateurDao();
		this.sujetDAO = daoFactory.getSujetDao();
		this.notificationDAO = daoFactory.getNotificationDao();
		/* Initialisation des autres éléments */
		this.erreurs = new ArrayList<>();
		this.attributions = new ArrayList<>();
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		this.erreurs = new ArrayList<>();
		this.attributions = new ArrayList<>();

		/* Chargement des formulaires de la page */
		this.chargerGererProfesseur(request);

		/* Affichage du formulaire */
		this.getServletContext().getRequestDispatcher(VUE_FORM).forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		this.erreurs = new ArrayList<>();
		this.attributions = new ArrayList<>();

		/* Récupération de tous les parmètres */
		Map<String, String[]> parameters = request.getParameterMap();

		/* Recherche des sujets sans référent */
		List<Sujet> sujetsSansReferent = this.sujetDAO.listerSujetsSansReferent();

		/*
		 * Rechercher des professeurs à valider avec en parallèle : - la liste des
		 * sujets des référents à valider - la liste du nombre d'occurrences comme
		 * "référent validé" pour chaque professeur
		 */

		List<Utilisateur> professeursReferentsAValider = this.utilisateurDAO.trouverProfesseurAValider();
		List<Sujet> sujetsDesReferentsAValider = this.sujets(professeursReferentsAValider);

		// Fabrication de la liste contenant le nombre de fois où un utilisateur
		// est référent :

		// Liste de tous les professeurs sujets
		List<ProfesseurSujet> professeurSujets = this.utilisateurDAO.listerProfesseurSujet();
		// Suppression des référents en attente de validation
		List<ProfesseurSujet> professeurSujetsSansReferentNonValide = new ArrayList<>();
		for (ProfesseurSujet ps : professeurSujets) {
			if (!((REFERENT).equals(ps.getFonction().name()) && ("non").equals(ps.getValide()))) {
				professeurSujetsSansReferentNonValide.add(ps);
			}
		}

		List<Utilisateur> professeurs = new ArrayList<>();
		List<Sujet> sujets = new ArrayList<>();

		listSujetProf(professeurSujetsSansReferentNonValide, professeurs, sujets);

		/*
		 * Exécution des 3 différents formulaires présents sur la page GererProfesseur
		 */
		// Attribution de un ou plusieurs professeur(s) sur des sujets sans
		// référents
		if ("attribuerReferent".equalsIgnoreCase(request.getParameter(ServletUtilitaire.CHAMP_FORMULAIRE))) {
			this.attribuerReferent(parameters, request, sujetsSansReferent);
			// Validation des professeurs qui ont postulé comme référent
		} else if ("validerReferent".equalsIgnoreCase(request.getParameter(ServletUtilitaire.CHAMP_FORMULAIRE))) {
			this.validerReferent(parameters, request, professeursReferentsAValider, sujetsDesReferentsAValider);
			// Suppression d'une ou des fonction(s) d'un ou de professeur(s) sur
			// un ou des sujet(s)
		} else if ("supprimerFonction".equalsIgnoreCase(request.getParameter(ServletUtilitaire.CHAMP_FORMULAIRE))) {
			this.supprimerFonction(parameters, request, professeurs, sujets);
		}

		int nombreEchecs = erreurs.size();
		int nombreAttribution = attributions.size();

		request.setAttribute(ATT_ATTRIBUTION_REFRENT, attributions);
		request.setAttribute(ATT_ATTRIBUTION_REFRENT, erreurs);
		request.setAttribute("nombreEchecs", nombreEchecs);
		request.setAttribute("nombreAttributions", nombreAttribution);

		/* Affichage du formulaire */
		this.chargerGererProfesseur(request);
		this.getServletContext().getRequestDispatcher(VUE_FORM).forward(request, response);
	}

	/**
	 * Attribution de référents
	 * 
	 * @param Map<String,String[]>
	 *            parameters, tous les paramètres de la pageGererProfesseur
	 * @param HttpServletResques
	 *            request la requête
	 * @param List<Sujet>
	 *            sujetsSansReferent, liste des sujets sans référents
	 * 
	 */
	private void attribuerReferent(Map<String, String[]> parameters, HttpServletRequest request,
			List<Sujet> sujetsSansReferent) {
		/*
		 * Récupération des données saisies pour l'attribution des profOptions dans les
		 * sujets sans référent
		 */

		for (String parameter : parameters.keySet()) {
			String values = request.getParameter(parameter);

			if (parameter.startsWith("idReferent-")) {
				String idSujet = parameter.substring("idReferent-".length());
				Long idLongSujet = 0L;
				for (Sujet sujet : sujetsSansReferent) {
					if (sujet.getIdSujet().equals(Long.valueOf(idSujet))) {
						idLongSujet = sujet.getIdSujet();
					}
				}
				Long idProfesseur = Long.parseLong(values);
				Professeur professeur = new Professeur();
				professeur.setIdProfesseur(idProfesseur);
				Sujet sujet = new Sujet();
				sujet.setIdSujet(idLongSujet);

				erreurProfesseurSujetNonCree(professeurSujetPossedeDejaUneFonction(professeur, sujet), idProfesseur,
						idLongSujet);

			}
		}
	}

	/**
	 * Validation des référents
	 * 
	 * @param Map<String,String[]>
	 *            parameters, tous les paramètres de la page GererProfesseur
	 * @param HttpServletResquest
	 *            request la requête
	 * @param List<Utilisateur>
	 *            professeursReferentsAValider, liste des référents à valider
	 * @param List<Sujet>
	 *            sujetsDesReferentsAValider, liste des sujets sur lesquels des
	 *            référents sont à valider
	 * 
	 */

	private void validerReferent(Map<String, String[]> parameters, HttpServletRequest request,
			List<Utilisateur> professeursReferentsAValider, List<Sujet> sujetsDesReferentsAValider) {
		/* Récupération des données saisies pour la validation */
		for (String parameter : parameters.keySet()) {
			String values = request.getParameter(parameter);

			if (parameter.startsWith(ATT_BOUTON_RADIO) && ("true").equals(values)) {
				String idSujet = parameter.substring(ATT_BOUTON_RADIO.length());
				Long idLongSujet = sujetsDesReferentsAValider.get(Integer.parseInt(idSujet)).getIdSujet();
				Long idProfesseur = professeursReferentsAValider.get(Integer.parseInt(idSujet)).getIdUtilisateur();
				Professeur professeur = new Professeur();
				professeur.setIdProfesseur(idProfesseur);
				Sujet sujet = new Sujet();
				sujet.setIdSujet(idLongSujet);
				this.sujetDAO.supprimerProfesseurSujet(sujet, professeur);
				this.sujetDAO.creerProfesseurSujet(professeur, sujet, "référent", "oui");

				sujet = this.sujetDAO.trouver(idLongSujet);

				/*
				 * On notifie le professeur de la validation de sa demande
				 */
				this.notificationDAO.ajouterNotification(idProfesseur,
						"Votre demande de référent sur le sujet " + sujet.getTitre() + " a été acceptée !");

			} else if (parameter.startsWith(ATT_BOUTON_RADIO) && ("false").equals(values)) {
				String idSujet = parameter.substring(ATT_BOUTON_RADIO.length());
				Long idLongSujet = sujetsDesReferentsAValider.get(Integer.parseInt(idSujet)).getIdSujet();
				Long idProfesseur = professeursReferentsAValider.get(Integer.parseInt(idSujet)).getIdUtilisateur();
				Professeur professeur = new Professeur();
				professeur.setIdProfesseur(idProfesseur);
				Sujet sujet = new Sujet();
				sujet.setIdSujet(idLongSujet);
				this.sujetDAO.supprimerProfesseurSujet(sujet, professeur);

				sujet = this.sujetDAO.trouver(idLongSujet);

				/*
				 * On notifie le professeur de la non validation de sa demande
				 */
				this.notificationDAO.ajouterNotification(idProfesseur,
						"Votre demande de référent sur le sujet " + sujet.getTitre() + " n\'a pas été acceptée !");
			}
		}
	}

	/**
	 * Suppression des fonctions d'un professeur
	 * 
	 * @param Map<String,String[]>
	 *            parameters, tous les paramètres de la page GererProfesseur
	 * @param HttpServletResquest
	 *            request la requête
	 * @param List<Utilisateur>
	 *            professeurs, liste des professeurs dont ont souhaite supprimer la
	 *            fonction
	 * @param List<Sujet>
	 *            sujets, liste des sujets dont on souhaite supprimer la fonction
	 * 
	 */

	private void supprimerFonction(Map<String, String[]> parameters, HttpServletRequest request,
			List<Utilisateur> professeurs, List<Sujet> sujets) {
		/*
		 * Récupération des données saisies pour la suppression de la fonction liant un
		 * professeur à un sujet
		 */
		for (String parameter : parameters.keySet()) {
			String values = request.getParameter(parameter);

			if (parameter.startsWith(ATT_BOUTON_RADIO) && ("true").equals(values)) {
				String idSujet = parameter.substring(ATT_BOUTON_RADIO.length());
				Long idLongSujet = sujets.get(Integer.parseInt(idSujet)).getIdSujet();
				Long idProfesseur = professeurs.get(Integer.parseInt(idSujet)).getIdUtilisateur();
				Professeur professeur = new Professeur();
				professeur.setIdProfesseur(idProfesseur);
				Sujet sujet = new Sujet();
				sujet.setIdSujet(idLongSujet);
				this.sujetDAO.supprimerProfesseurSujet(sujet, professeur);

				sujet = this.sujetDAO.trouver(idLongSujet);

				/*
				 * On notifie le professeur de la suppression de sa fonction sur le sujet
				 * concerné
				 */
				this.notificationDAO.ajouterNotification(idProfesseur,
						"Vous avez été retiré du sujet suivant : " + sujet.getTitre());
			}
		}
	}

	/**
	 * Charge toutes les listes nécessaires de la page GererProfesseur.
	 * 
	 * @param request
	 *            la requête à charger.
	 * @return request la requête chargée.
	 */
	private HttpServletRequest chargerGererProfesseur(HttpServletRequest request) {

		/* Recherche des sujets sans référent */
		List<Sujet> sujetsSansReferent = this.sujetDAO.listerSujetsSansReferent();

		/*
		 * Recherche des professeurs à valider avec en parallèle : - la liste des sujets
		 * des référents à valider - la liste du nombre d'occurrences comme
		 * "référent validé" pour chaque professeur
		 */

		List<Utilisateur> professeursReferentsAValider = this.utilisateurDAO.trouverProfesseurAValider();
		List<Sujet> sujetsDesReferentsAValider = this.sujets(professeursReferentsAValider);

		/* Recherche des professeurs à attribuer */
		List<Utilisateur> professeursAAttribuer = this.utilisateurDAO.trouverProfesseurSansSujet();

		// Liste de tous les professeurs sujets
		List<ProfesseurSujet> professeurSujets = this.utilisateurDAO.listerProfesseurSujet();
		// Suppression des référents en attente de validation
		List<ProfesseurSujet> professeurSujetsSansReferentNonValide = new ArrayList<>();
		for (ProfesseurSujet ps : professeurSujets) {
			if (!((REFERENT).equals(ps.getFonction().name()) && ("non").equals(ps.getValide()))) {
				professeurSujetsSansReferentNonValide.add(ps);
			}
		}

		List<Utilisateur> professeurs = new ArrayList<>();
		List<Sujet> sujets = new ArrayList<>();

		listSujetProf(professeurSujetsSansReferentNonValide, professeurs, sujets);

		/*
		 * Initialisation de la liste pour stocker le nombre de fois où un utilisateur
		 * est référent
		 */
		List<Integer> nombreDeFoisReferent = new ArrayList<>();

		for (Utilisateur u : professeursAAttribuer) {
			int cpt = 0;
			for (ProfesseurSujet ps : professeurSujets) {
				if (u.getIdUtilisateur().equals(ps.getIdProfesseur()) && (REFERENT).equals(ps.getFonction().name())
						&& ("oui").equals(ps.getValide())) {
					cpt++;
				}
			}
			nombreDeFoisReferent.add(cpt);

		}

		request.setAttribute("professeursReferentsAValider", professeursReferentsAValider);
		request.setAttribute("sujetsDesReferentsAValider", sujetsDesReferentsAValider);
		request.setAttribute("professeursAAttribuer", professeursAAttribuer);
		request.setAttribute("nombreDeFoisReferent", nombreDeFoisReferent);
		request.setAttribute("sujetsSansReferent", sujetsSansReferent);
		request.setAttribute("listeProfesseurSujets", professeurSujets);
		request.setAttribute("professeurs", professeurs);
		request.setAttribute("sujets", sujets);

		return request;
	}

	private void listSujetProf(List<ProfesseurSujet> professeurSujetsSansReferentNonValide,
			List<Utilisateur> professeurs, List<Sujet> sujets) {
		for (ProfesseurSujet ps : professeurSujetsSansReferentNonValide) {
			Utilisateur utilisateur = new Utilisateur();
			utilisateur.setIdUtilisateur(ps.getIdProfesseur());

			Utilisateur u = this.utilisateurDAO.trouver(utilisateur).get(0);
			Sujet s = this.sujetDAO.trouver(ps.getIdSujet());

			professeurs.add(u);
			sujets.add(s);
		}
	}

	/**
	 * Méthode renvoyant l'erreur si un professeurSujet n'a pas pu être créé sur le
	 * sujet choisi
	 * 
	 * @param Booléen
	 *            estCree
	 * @param Long
	 *            idProfesseur l'identifiant d'un professeur qui est attribué au
	 *            sujet
	 * @param Long
	 *            idSujet l'identifiant du sujet sur lequel le professeur est
	 *            attribué
	 * 
	 */
	protected void erreurProfesseurSujetNonCree(boolean estCree, Long idProfesseur, Long idSujet) {

		/* Création de l'utilisateur */
		Utilisateur utilisateur = new Utilisateur();
		utilisateur.setIdUtilisateur(idProfesseur);

		/*
		 * Initialisation des différents paramètres qui permettront d'indiquer quelle
		 * attribution n'a pas pu être effectuée
		 */
		/* - titre du sujet */
		/* - nom du professeur que l'on souhaite attribuer à un sujet */
		/* - prenom du professeur que l'on souhaite attribuer à un sujet */
		String titreSujet = this.sujetDAO.trouver(idSujet).getTitre();
		String nomProfesseur = this.utilisateurDAO.trouver(utilisateur).get(0).getNom();
		String prenomProfesseur = this.utilisateurDAO.trouver(utilisateur).get(0).getPrenom();

		/*
		 * Chargement de la liste de toutes les fonctions du professeur que l'on
		 * souhaite attibuer à un sujet
		 */
		List<ProfesseurSujet> professeurSujets = this.utilisateurDAO.trouverProfesseursSujet(utilisateur);
		String fonction = "";
		for (ProfesseurSujet ps : professeurSujets) {
			if (ps.getIdProfesseur().equals(idProfesseur) && ps.getIdSujet().equals(idSujet)) {
				fonction = ps.getFonction().name();
			}
		}

		/*
		 * Cas où le professeur que l'on cherche à attribuer comme référent sur un sujet
		 * possède déjà une fonction sur ce même sujet
		 */
		if (estCree) {

			// Ajout de l'erreur dans la liste des erreurs
			String erreur = nomProfesseur + " " + prenomProfesseur
					+ " n\'a pu être ajouté(e) comme référent sur le sujet " + titreSujet + " car il est déjà "
					+ fonction + " sur celui-ci";
			erreurs.add(erreur);

			/* Cas où le professeur peut être attribué au sujet souhaité */
		} else {
			// Création du professeurSujet
			Professeur professeur = new Professeur();
			professeur.setIdProfesseur(idProfesseur);
			Sujet sujet = new Sujet();
			sujet.setIdSujet(idSujet);
			this.sujetDAO.creerProfesseurSujet(professeur, sujet, "référent", "oui");

			// Ajout de l'attribution dans la liste des attributions
			String attribution = nomProfesseur + " " + prenomProfesseur
					+ " a été ajouté(e) comme référent sur le sujet " + titreSujet;
			attributions.add(attribution);

			/*
			 * On notifie le professeur de son attribution comme référent sur le sujet
			 * concerné
			 */
			sujet = this.sujetDAO.trouver(idSujet);
			this.notificationDAO.ajouterNotification(idProfesseur,
					"Vous avez été ajouté comme référent sur le sujet suivant : " + sujet.getTitre());
		}
	}

	/**
	 * Méthode créant une liste de sujets en fonction d'une liste d'utilisateurs qui
	 * ont postulé comme référent dessus
	 * 
	 * @param utilisateurs
	 *            une liste d'Utilisateurs
	 * 
	 * @return List<Sujet> sujetsDesReferentsAValider
	 */
	protected List<Sujet> sujets(List<Utilisateur> utilisateurs) {
		List<Sujet> sujetsDesReferentsAValider = new ArrayList<>();
		for (Utilisateur utilisateur : utilisateurs) {
			Professeur professeur = new Professeur();
			professeur.setIdProfesseur(utilisateur.getIdUtilisateur());
			List<Sujet> professeurSujet = this.sujetDAO.trouverSujetsProfesseurReferentNonValide(professeur);
			for (Sujet s : professeurSujet) {
				if (!sujetPresent(sujetsDesReferentsAValider, s)) {
					sujetsDesReferentsAValider.add(this.sujetDAO.trouver(s.getIdSujet()));
				}
			}
		}
		return sujetsDesReferentsAValider;
	}

	/**
	 * Méthode vérifiant si un professeurSujet a été créé ou non
	 * 
	 * @param Professeur
	 *            professeur, le professeur pour lequel on vérifie s'il possède déjà
	 *            une fonction sur un sujet donné
	 * @param Sujet
	 *            sujet, le sujet sur lequel on vérifie si un professeur possède
	 *            déjà une fonction
	 * 
	 * @return boolean cree
	 */
	protected boolean professeurSujetPossedeDejaUneFonction(Professeur professeur, Sujet sujet) {
		boolean cree = false;

		Utilisateur utilisateur = new Utilisateur();
		utilisateur.setIdUtilisateur(professeur.getIdProfesseur());

		List<ProfesseurSujet> professeurSujets = this.utilisateurDAO.trouverProfesseursSujet(utilisateur);
		for (ProfesseurSujet ps : professeurSujets) {
			if (ps.getIdProfesseur().equals(professeur.getIdProfesseur())
					&& ps.getIdSujet().equals(sujet.getIdSujet())) {
				cree = true;
			}
		}
		return cree;
	}

	/**
	 * Cette méthode privée permet de savoir si un sujet est présent dans une liste
	 * de sujets
	 * 
	 * @param List<Sujet>
	 *            sujets
	 * @param Sujet
	 *            sujet
	 * 
	 * @return boolean present
	 */
	private static boolean sujetPresent(List<Sujet> sujets, Sujet sujet) {
		boolean present = false;
		for (Sujet s : sujets) {
			if (sujet.getIdSujet() == s.getIdSujet()) {
				present = true;
			}
		}
		return present;
	}

}