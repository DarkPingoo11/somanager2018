package fr.eseo.ld.servlets;

import fr.eseo.ld.beans.Equipe;
import fr.eseo.ld.beans.Poster;
import fr.eseo.ld.beans.Utilisateur;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Servlet permettant la modification d'un poster.
 */
@WebServlet("/ModifierPoster")
@MultipartConfig
public class ModifierPoster extends DeposerPoster {
	private static final long serialVersionUID = 1L;

	private static Logger logger = Logger.getLogger(GererRoles.class.getName());

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		/* Récupération de la session depuis la requête */
		HttpSession session = request.getSession();

		/* Récupération de l'ID de l'Utilisateur en session */
		Utilisateur utilisateurSession = (Utilisateur) session.getAttribute(ServletUtilitaire.ATT_SESSION_USER);
		Long idUtilisateurSession = utilisateurSession.getIdUtilisateur();

		/* Recupération de l'équipe de l'utilisateur */

		Equipe equipePFE = this.equipeDAO.trouver(idUtilisateurSession);

		/*
		 * Les données reçues sont multipart, on doit donc utiliser la méthode getPart()
		 * pour traiter le champ d'envoi de fichiers.
		 */
		Part part = request.getPart("newPoster");

		/*
		 * création/récupération des attributs qui seront attribués au nouveau poster
		 * dans la BDD
		 */
		Long idSujet = equipePFE.getIdSujet();
		SimpleDateFormat formatDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		SimpleDateFormat formatAnnee = new SimpleDateFormat("yyyy");
		String date = formatDate.format(new Date());
		String annee = formatAnnee.format(new Date());
		String valide = "non";

		/*
		 * Il faut déterminer s'il s'agit d'un champ classique ou d'un champ de type
		 * fichier : on délègue cette opération à la méthode utilitaire getNomPoster().
		 */
		String nomPoster = getNomPoster(part, String.valueOf(idSujet));

		Poster newPoster = new Poster();

		/* Récupération de l'ID de l'Utilisateur en session */
		Utilisateur utilisateur = (Utilisateur) session.getAttribute(ServletUtilitaire.ATT_SESSION_USER);
		Long idUtilisateur = utilisateur.getIdUtilisateur();

		// création du fichier de stockage annuel s'il n'existe pas encore (cas du
		// premier poster de l'année)
		StringBuilder sb = new StringBuilder();
		sb.append(cheminEnregPoster).append("/").append(annee).append("/");
		File file = new File(sb.toString());
		if (!file.exists()) {
			file.mkdirs();
		}

		/*
		 * suppression de l'ancien poster et ecriture du poster sur le disque si le nom
		 * n'est pas null et au bon format
		 */
		if (nomPoster != null && !nomPoster.isEmpty() && bonFormatPoster(nomPoster)) {

			// création des attributs du nouveau poster
			newPoster.setDatePoster(date);
			newPoster.setIdSujet(idSujet);
			newPoster.setChemin(cheminEnregPoster + "/" + annee + "/" + nomPoster);
			newPoster.setValide(valide);
			newPoster.setIdEquipe(equipePFE.getIdEquipe());
			/* suppression de l'ancien poster sur le disque: */
			String cheminAncienPoster = this.posterDAO.trouverPoster(newPoster.getIdSujet()).getChemin();
			try {
				Files.delete(Paths.get(cheminAncienPoster));
			}catch(Exception e) {
				logger.log(Level.WARN, "Erreur lors de l'effacement du fichier", e);
			}

			/* suppression de la ligne correspodante dans la bdd */
			Poster ancienPoster = this.posterDAO.trouverPoster(newPoster.getIdSujet());
			this.posterDAO.supprimer(ancienPoster);

			/* Écriture du nouveau poster sur le disque */
			ecrirePoster(part, nomPoster, cheminEnregPoster + "/" + annee);

			/* Insertion du nouveau poster dans la BDD via posterDAO */
			this.posterDAO.creer(newPoster);

			/* Ajout d'une notification */
			this.notificationDAO.ajouterNotification(idUtilisateur, "Votre poster a bien été modifié");

		} else {
			/* Ajout d'une notification */
			this.notificationDAO.ajouterNotification(idUtilisateur, "Impossible de modifier votre poster");
		}
		/* Redirection vers la page d'accueil */
		response.sendRedirect(request.getHeader(ServletUtilitaire.HEADER));
	}

}