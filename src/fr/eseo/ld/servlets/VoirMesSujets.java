package fr.eseo.ld.servlets;

import static fr.eseo.ld.servlets.ServletUtilitaire.getInfosChoisies;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.eseo.ld.beans.Commentaire;
import fr.eseo.ld.beans.EtatSujet;
import fr.eseo.ld.beans.OptionESEO;
import fr.eseo.ld.beans.Sujet;
import fr.eseo.ld.beans.Utilisateur;
import fr.eseo.ld.dao.CommentaireDAO;
import fr.eseo.ld.dao.DAOFactory;
import fr.eseo.ld.dao.NotificationDAO;
import fr.eseo.ld.dao.OptionESEODAO;
import fr.eseo.ld.dao.SujetDAO;
import fr.eseo.ld.dao.UtilisateurDAO;

/**
 * Servlet permettant l'affichage des sujets déposés par l'utilisateur.
 */
@WebServlet("/VoirMesSujets")
public class VoirMesSujets extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private static final String[] CENTRES_INTERETS = { "JPO", "GP", "Autres" };

	private static final String VUE_FORM = "/WEB-INF/formulaires/voirMesSujets.jsp";

	private UtilisateurDAO utilisateurDAO;
	private SujetDAO sujetDAO;
	private NotificationDAO notificationDAO;
	private CommentaireDAO commentaireDAO;
	private OptionESEODAO optionESEODAO;

	@Override
	public void init() throws ServletException {
		/* Récupération d'instances de DAO */
		DAOFactory daoFactory = (DAOFactory) this.getServletContext().getAttribute(ServletUtilitaire.CONF_DAO_FACTORY);
		this.utilisateurDAO = daoFactory.getUtilisateurDao();
		this.sujetDAO = daoFactory.getSujetDao();
		this.notificationDAO = daoFactory.getNotificationDao();
		this.commentaireDAO = daoFactory.getCommentaireDao();
		this.optionESEODAO = daoFactory.getOptionESEODAO();
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		/* Récupération de l'ID de l'utilisateur en session */
		HttpSession session = request.getSession();
		Utilisateur utilisateurSession = (Utilisateur) session.getAttribute(ServletUtilitaire.ATT_SESSION_USER);
		Long idUtilisateurSession = utilisateurSession.getIdUtilisateur();

		/* Recherche des sujets de l'utilisateur via sujetDao */
		Utilisateur utilisateur = new Utilisateur();
		utilisateur.setIdUtilisateur(idUtilisateurSession);
		List<Sujet> sujetsTrouves = this.sujetDAO.trouverSujetsPortes(utilisateur);

		/* Jointure de Utilisateur et Commentaire via idUtilisateur */
		Map<Long, String> nomsExpediteursCommentaires = new HashMap<>();
		Map<Long, Long> idPorteursSujets = new HashMap<>();
		List<Commentaire> commentaires = new ArrayList<>();
		List<Commentaire> commentairesSecondaires = new ArrayList<>();
		List<String> optionsSujet = new ArrayList<>();

		for (Sujet sujet : sujetsTrouves) {
			/* Recherche des commentaires du sujet */
			Commentaire commentaireSujet = new Commentaire();
			commentaireSujet.setIdSujet(sujet.getIdSujet());
			List<Commentaire> commentairesTrouves = this.commentaireDAO.trouver(commentaireSujet);

			/* Recherche des options du sujet */
			List<OptionESEO> optionsTrouvees = this.optionESEODAO.trouverOptionSujet(sujet);
			StringBuilder optionSujet = new StringBuilder();
			idPorteursSujets.put(sujet.getIdSujet(), idUtilisateurSession);
			for (OptionESEO option : optionsTrouvees) {
				optionSujet.append(option.getNomOption() + " ");
			}
			optionsSujet.add(String.valueOf(optionSujet));

			/*
			 * Recherche des commentaires avec implémentation des différentes listes et
			 * hashmaps les concernant
			 */
			rechercheCommentaires(commentairesTrouves, nomsExpediteursCommentaires, commentairesSecondaires,
					commentaires);
		}
		/* Affichage du formulaire */
		request.setAttribute(ServletUtilitaire.ATT_LISTE_SUJETS, sujetsTrouves);
		request.setAttribute(ServletUtilitaire.ATT_ID_PORTEURS_SUJETS, idPorteursSujets);
		request.setAttribute(ServletUtilitaire.ATT_OPTIONS_SUJET, optionsSujet);
		request.setAttribute(ServletUtilitaire.ATT_COMMENTAIRES, commentaires);
		request.setAttribute(ServletUtilitaire.ATT_NOMS_EXPEDITEURS_COMMENTAIRES, nomsExpediteursCommentaires);
		request.setAttribute(ServletUtilitaire.ATT_COMMENTAIRES_SECONDAIRES, commentairesSecondaires);
		this.getServletContext().getRequestDispatcher(VUE_FORM).forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		/* Récupération de l'ID de l'utilisateur en session */
		HttpSession session = request.getSession();
		Utilisateur utilisateur = (Utilisateur) session.getAttribute(ServletUtilitaire.ATT_SESSION_USER);
		Long idUtilisateur = utilisateur.getIdUtilisateur();

		/* Récupération des données saisies */
		Long idSujet = Long.parseLong(request.getParameter(ServletUtilitaire.CHAMP_ID_SUJET));
		String titre = request.getParameter(ServletUtilitaire.CHAMP_TITRE);
		String description = request.getParameter(ServletUtilitaire.CHAMP_DESCRIPTION);
		Integer nbrMinEleves = Integer.valueOf(request.getParameter(ServletUtilitaire.CHAMP_NBR_MIN_ELEVES));
		Integer nbrMaxEleves = Integer.valueOf(request.getParameter(ServletUtilitaire.CHAMP_NBR_MAX_ELEVES));
		Boolean contratPro = Boolean.valueOf(request.getParameter(ServletUtilitaire.CHAMP_CONTRAT_PRO));
		Boolean confidentialite = Boolean.valueOf(request.getParameter(ServletUtilitaire.CHAMP_CONFIDENTIALITE));
		String liens = request.getParameter(ServletUtilitaire.CHAMP_LIENS);

		List<OptionESEO> options = this.optionESEODAO.lister();
		List<OptionESEO> optionsModifiees = new ArrayList<>();
		for (OptionESEO option : options) {
			if (request.getParameter(option.getNomOption()) != null) {
				optionsModifiees.add(option);
			}
		}

		/*
		 * Concaténation des paramètres des boutons radio cochés pour les stocker dans
		 * une seule variable
		 */
		String centresInteretsChoisis = getInfosChoisies(request, CENTRES_INTERETS);

		/* Modification du sujet dans la BDD via sujetDAO */
		Sujet sujet = new Sujet();
		sujet.setIdSujet(idSujet);
		sujet.setTitre(titre);
		sujet.setDescription(description);
		sujet.setNbrMinEleves(nbrMinEleves);
		sujet.setNbrMaxEleves(nbrMaxEleves);
		sujet.setContratPro(contratPro);
		sujet.setConfidentialite(confidentialite);
		sujet.setEtat(EtatSujet.DEPOSE);
		sujet.setInterets(centresInteretsChoisis);
		sujet.setLiens(liens);
		this.sujetDAO.modifier(sujet);

		/* Modification des options relatives au sujet */
		List<OptionESEO> optionsSupprimees = this.optionESEODAO.trouverOptionSujet(sujet);
		for (OptionESEO option : optionsSupprimees) {
			this.sujetDAO.supprimerOptionSujet(sujet, option);
		}
		this.sujetDAO.attribuerOptionsSujet(sujet, optionsModifiees);

		/* Redirection vers la page d'accueil avec notification */
		this.notificationDAO.ajouterNotification(idUtilisateur, "Un sujet de votre liste a été modifié",
				"VoirMesSujets");
		response.sendRedirect(request.getContextPath() + ServletUtilitaire.VUE_DASHBOARD);
	}

	private void rechercheCommentaires(List<Commentaire> commentairesTrouves,
			Map<Long, String> nomsExpediteursCommentaires, List<Commentaire> commentairesSecondaires,
			List<Commentaire> commentaires) {
		for (Commentaire commentaire : commentairesTrouves) {
			/* Recherche des utilisateurs ayant commenté ce sujet */
			Long idUtilisateurCommentaire = commentaire.getIdUtilisateur();
			Utilisateur utilisateurCommentaire = new Utilisateur();
			utilisateurCommentaire.setIdUtilisateur(idUtilisateurCommentaire);
			List<Utilisateur> utilisateursTrouves = this.utilisateurDAO.trouver(utilisateurCommentaire);

			/* Correspondance entre l'ID d'un utilisateur et son nom */
			String nom;
			if (!utilisateursTrouves.isEmpty()) { // sécurité dans le cas où la BDD ait mal été remplie
				nom = utilisateursTrouves.get(0).getPrenom() + " " + utilisateursTrouves.get(0).getNom();
			} else {
				nom = "";
			}
			nomsExpediteursCommentaires.put(idUtilisateurCommentaire, nom);
			/* Ajoute un commentaire primaire ou secondaire */
			if (commentaire.getIdObservation() != -1) { // s'il s'agit d'un commentaire secondaire
				commentairesSecondaires.add(commentaire);
			} else {
				commentaires.add(commentaire);
			}
		}
	}

}