package fr.eseo.ld.servlets;

import com.google.common.base.Charsets;
import fr.eseo.ld.beans.*;
import fr.eseo.ld.dao.DAOFactory;
import fr.eseo.ld.dao.ParametreDAO;
import fr.eseo.ld.dao.SujetDAO;
import fr.eseo.ld.utils.CallbackUtilitaire;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Servlet permettant l'affichage du lien vers le dépot Google Form
 */
@WebServlet("/DepotGoogle")
@MultipartConfig
public class DepotGoogle extends HttpServlet {

	private SujetDAO sujetDAO;
	private ParametreDAO parametreDAO;
	private static final String VUE_FORM = "/WEB-INF/formulaires/depotGoogle.jsp";

	private static final String NOM_PORTEUR = "nomPorteur";
	private static final String TITRE_SUJET = "titreSujet";
	private static final String DESC_SUJET = "descSujet";
	private static final String NB_MIN = "nbMin";
	private static final String NB_MAX = "nbMax";
	private static final String CONTRAT_PRO = "pro";
	private static final String CONFIDENTIEL = "confidentiel";
	private static final String OPTIONS_C = "options";
	private static final String INTERET = "interet";
	private static final String LIENS = "liens";

	private static final String LIEN_DEFAUT = "";
	private static final String ATT_LIEN = "lienFormulaireGoogle";

	private static final String[] HEADER = { "timestamp", NOM_PORTEUR, TITRE_SUJET, DESC_SUJET, NB_MIN, NB_MAX,
			CONTRAT_PRO, CONFIDENTIEL, OPTIONS_C, INTERET, LIENS };

	private static Logger logger = Logger.getLogger(DepotGoogle.class.getName());

	@Override
	public void init() throws ServletException {
		/* Récupération d'instances de DAO */
		DAOFactory daoFactory = (DAOFactory) this.getServletContext().getAttribute(ServletUtilitaire.CONF_DAO_FACTORY);
		this.sujetDAO = daoFactory.getSujetDao();
		this.parametreDAO = daoFactory.getParametreDAO();
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		/* Récupération de l'ID de l'utilisateur en session */

		if ("synchroniserSujets".equalsIgnoreCase(request.getParameter(ServletUtilitaire.CHAMP_FORMULAIRE))) {
			this.doSynchroniserSujets(request);
		} else if ("changerLien".equalsIgnoreCase(request.getParameter(ServletUtilitaire.CHAMP_FORMULAIRE))) {
			this.doChangerLien(request);
		}

		doGet(request, response);
	}

	/**
	 * Effectue l'action de changer de lien
	 * @param request HttpServletRequest
	 */
	private void doChangerLien(HttpServletRequest request) {
		Callback callback;
		// Changement du lien en base de données
		String lien = request.getParameter("nouveauLien");

		if (isLienValide(lien)) {
			Parametre parametreLien = new Parametre();
			parametreLien.setNomParametre(ParametreDAO.IDENTIFIANT_LIEN_FORM_SUJET);

			// On cherche le parametre dans la base
			List<Parametre> params = this.parametreDAO.trouver(parametreLien);
			if (params.size() == 1) {
				// Si il existe, on update
				parametreLien.setValeur(lien);
				this.parametreDAO.modifier(parametreLien);
			} else {
				parametreLien.setValeur(lien);
				this.parametreDAO.creer(parametreLien);
			}
			callback = new Callback(CallbackType.SUCCESS,
					"Le lien du formulaire de dépot de sujet a bien été mis à jour");
		} else {
			callback = new Callback("Merci de rentrer un lien valide");
		}
		CallbackUtilitaire.setCallback(request, callback);
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// Récupération du lien si il existe
		Parametre lien = recupererLienFormulaireInDatabase();
		request.setAttribute(ATT_LIEN, lien == null ? LIEN_DEFAUT : lien.getValeur());

		this.getServletContext().getRequestDispatcher(VUE_FORM).forward(request, response);
	}

	/**
	 * Effectuer l'action de synchroniser un sujet
	 * @param request HttpServletRequest
	 * @throws ServletException En cas de fichier incorrect
	 */
	private void doSynchroniserSujets(HttpServletRequest request) throws ServletException {
		Callback callback		= null;
		Callback callbackErrors = null;
		InputStream fileContent = null;

		try {
			Part filePart = request.getPart("file_selected");
			fileContent = filePart.getInputStream();
		} catch (IOException e) {
			callback = new Callback("Merci d'indiquer un fichier.csv");
		}

		//Si le fileContent n'est pas null
		Integer[] resultat = null;
		if(fileContent != null) {
			try {
				//[0] 		=> Nombre de sujets importés
				//[1----N] 	=> Lignes erronés
				resultat = synchroniserSujets(fileContent);
			} catch (IOException e2) {
				logger.warn("Le fichier n'est pas dans le bon format", e2);
				callback = new Callback("Le fichier n'est pas dans le bon format");
			}
		}
		//Si on a un résultat
		if(resultat != null) {
			int nbrSujets 	= resultat[0];
			if (nbrSujets > 0) {
				callback = new Callback(CallbackType.SUCCESS,
						nbrSujets + " sujet" + pluriel(nbrSujets, "s ont"," à") + " été synchronisé"
								+ pluriel(nbrSujets, "s", "") + " depuis le formulaire");
				if(resultat.length > 1) {
					//Mise en forme du message d'erreur
					StringBuilder msgB = new StringBuilder();
					for(int i = 1; i < resultat.length; i++) {
						msgB.append(", ").append(resultat[i]);
					}

					String msgErreur = msgB.toString().substring(2);

					callbackErrors = new Callback(CallbackType.ERROR,
							"Les lignes n°" + msgErreur + " sont au mauvais format et n'ont pas été importées");
				}
			} else {
				callback = new Callback("Le fichier est vide");
			}
		}

		CallbackUtilitaire.setCallback(request, callback, callbackErrors);
	}

	private String pluriel(int l, String pluriel, String singulier) {
		return l > 1 ? pluriel : singulier;
	}

	/**
	 * Recupere la liste des parametres de Parametres
	 */
	public static Parametre recupererLienFormulaireInDatabase() {
		Parametre parametre = new Parametre();
		parametre.setNomParametre(ParametreDAO.IDENTIFIANT_LIEN_FORM_SUJET);
		List<Parametre> params = DAOFactory.getInstance().getParametreDAO().trouver(parametre);

		return params.size() == 1 ? params.get(0) : null;
	}

	/**
	 * Synchronise les sujets depuis le formulaire. Importe les nouveaux sujets, met
	 * à jour les existants.
	 *
	 * @param stream
	 *            inputstream du Fichier.csv | Le fichier doit respecter le format
	 *            fournis dans la documentation
	 * @throws IOException
	 *             Fichier inconnu
	 * @return Nombre de sujets ajoutés/synchronisés [0] et le nombre d'erreurs [1]
	 */
	private Integer[] synchroniserSujets(InputStream stream) throws IOException {
		// Déclaration des outils pour lire un csv
		CSVParser csvFileParser;
		ArrayList<Integer> errors = new ArrayList<>();

		// Format par défaut
		CSVFormat csvFileFormat = CSVFormat.DEFAULT.withHeader(HEADER);

		// Définition du parseur
		csvFileParser = CSVParser.parse(stream, Charsets.UTF_8, csvFileFormat);

		List<CSVRecord> csvRecords = csvFileParser.getRecords();
		List<Sujet> sujets = new ArrayList<>();

		// Récupération des sujets (On commence à 1 car les colonnes sont définies en 0)
		for (int i = 1; i < csvRecords.size(); i++) {
			try {
				CSVRecord record = csvRecords.get(i);

				if(!record.isConsistent()) {
					throw new IllegalStateException("La ligne " + i + " est erronée !");
				}

				// Ajout des données au bean
				Sujet sujet = new Sujet();
				sujet.setTitre(record.get(TITRE_SUJET));
				sujet.setDescription(record.get(DESC_SUJET));
				sujet.setNbrMinEleves(Integer.parseInt(record.get(NB_MIN)));
				sujet.setNbrMaxEleves(Integer.parseInt(record.get(NB_MAX)));
				sujet.setContratPro(record.get(CONTRAT_PRO).equalsIgnoreCase("oui"));
				sujet.setConfidentialite(record.get(CONFIDENTIEL).equalsIgnoreCase("oui"));
				sujet.setEtatString("depose");
				sujet.setLiens(record.get(LIENS));
				sujet.setInterets(record.get(INTERET));
				sujet.setIdForm(i);

				sujets.add(sujet);
			} catch(Exception e) {
				//Ajout du numéro réel de la ligne (1 à n) de l'erreur
				errors.add(i+1);
			}
		}

		// Insertion en base
		this.insertSujetsInDatabase(sujets);

		//génération de la réponse
		//Le premier element du tableau est le nombre de sujets reussis
		//Les n autres éléments sont les numéros des sujets erronés
		ArrayList<Integer> reponse = new ArrayList<>();
		reponse.add(sujets.size());
		reponse.addAll(errors);

		return reponse.toArray(new Integer[]{});
	}

	/**
	 * Ajoute ou met à jour les sujets dans la base de données depuis le formulaire
	 *
	 * @param sujets
	 *            Liste des sujets
	 */
	private void insertSujetsInDatabase(List<Sujet> sujets) {
		// Insertion - Mise à jour des sujets dans la base
		SujetDAO dao = this.sujetDAO;
		for (Sujet s : sujets) {

			// Si le sujet existe déjà
			Sujet sujetExistant = dao.trouverFromForm(s.getIdForm());
			if (sujetExistant == null) {
				dao.creer(s);
				logger.info("Création d'un nouveau sujet : " + s);
			} else if (sujetExistant.getEtat().equals(EtatSujet.DEPOSE)
					|| sujetExistant.getEtat().equals(EtatSujet.REFUSE)) {
				s.setIdSujet(sujetExistant.getIdSujet());
				dao.modifier(s);
				logger.info("Modification depuis formulaire du sujet existant n° " + s.getIdSujet());
			}
		}
	}

	/**
	 * Vérifie si le lien est bien de la forme http(s)://____.__/_
	 *
	 * @param lien
	 *            Lien a tester
	 * @return True si valide, false sinon
	 */
	public static boolean isLienValide(String lien) {
		String pattern = "^(https?:\\/\\/[a-zA-Z0-9\\.]*\\.[a-z]{2,4})\\/([\\w\\W]*)$";
		return lien != null && lien.length() > 0 && lien.matches(pattern);
	}
}
