package fr.eseo.ld.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.eseo.ld.beans.NoteInteretSujet;
import fr.eseo.ld.beans.NoteInteretTechno;
import fr.eseo.ld.beans.Sujet;
import fr.eseo.ld.beans.Utilisateur;
import fr.eseo.ld.dao.DAOFactory;
import fr.eseo.ld.dao.NoteInteretSujetDAO;
import fr.eseo.ld.dao.NoteInteretTechnoDAO;
import fr.eseo.ld.dao.SujetDAO;

/**
 * Servlet permettant l'évaluation de l'intérêt d'un sujet.
 */
@WebServlet("/EvaluerInteret")
public class EvaluerInteret extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private static final String NOM_NOTE = "nameNote";
	private static final String NOM_NOTE_TECHNO = "techno";
	private static final String NOM_NOTE_SUJET = "sujet";

	private SujetDAO sujetDAO;
	private NoteInteretTechnoDAO noteInteretTechnoDAO;
	private NoteInteretSujetDAO noteInteretSujetDAO;

	@Override
	public void init() throws ServletException {
		/* Récupération d'instances de DAO */
		DAOFactory daoFactory = (DAOFactory) this.getServletContext().getAttribute(ServletUtilitaire.CONF_DAO_FACTORY);
		this.sujetDAO = daoFactory.getSujetDao();
		this.noteInteretTechnoDAO = daoFactory.getNoteInteretTechnoDAO();
		this.noteInteretSujetDAO = daoFactory.getNoteInteretSujetDAO();
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		/* Récupération des paramètres de la requête */
		Long idSujet = Long.parseLong(request.getParameter(ServletUtilitaire.CHAMP_ID_SUJET));
		String nomNote = request.getParameter(NOM_NOTE);

		/* Recherche de la note du sujet */
		Sujet sujet = new Sujet();
		sujet.setIdSujet(idSujet);
		List<Sujet> sujets = this.sujetDAO.trouver(sujet);
		Float note = (float) 0;
		if (nomNote.contentEquals(NOM_NOTE_TECHNO)) {
			note = sujets.get(0).getNoteInteretTechno();
		} else if (nomNote.contentEquals(NOM_NOTE_SUJET)) {
			note = sujets.get(0).getNoteInteretSujet();
		}

		/* Ecriture de la réponse en JSON */
		response.setCharacterEncoding("UTF-8");
		response.getWriter().write("" + note);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// Récuperer toute les notes faire la moyenne, envoyer
		/* Récupération de la session depuis la requête */
		HttpSession session = request.getSession();
		Utilisateur utilisateur = (Utilisateur) session.getAttribute(ServletUtilitaire.ATT_SESSION_USER);
		Long idUtilisateur = utilisateur.getIdUtilisateur();

		/* Récupération de l'id du Sujet choisi */
		Long idSujet = Long.parseLong(request.getParameter("idSujet"));
		int score = Math.round(Float.parseFloat(request.getParameter("score")));
		String name = request.getParameter(NOM_NOTE);

		Sujet sujet = new Sujet();
		sujet.setIdSujet(idSujet);

		int nombreDeNoteInteret;
		Float somme = (float) score;
		if (name.contentEquals(NOM_NOTE_TECHNO)) {
			sujet.setNoteInteretTechno(definitNoteInteretTechno(idUtilisateur, idSujet, score, somme));
		}
		if (name.contentEquals(NOM_NOTE_SUJET)) {
			NoteInteretSujet noteInteret = new NoteInteretSujet();
			noteInteret.setIdSujet(idSujet);
			List<NoteInteretSujet> notesInteretTrouves = noteInteretSujetDAO.trouver(noteInteret);
			nombreDeNoteInteret = notesInteretTrouves.size() + 1;
			noteInteret.setIdProfesseur(idUtilisateur);
			noteInteret.setNote(score);
			boolean existeDeja = false;
			for (NoteInteretSujet noteInteretSujet : notesInteretTrouves) {

				if ((noteInteretSujet.getIdSujet().equals(idSujet))
						&& (noteInteretSujet.getIdProfesseur().equals(idUtilisateur))) {
					existeDeja = true;
					noteInteretSujetDAO.modifier(noteInteret);
					nombreDeNoteInteret--;
				} else {
					somme += noteInteretSujet.getNote();
				}

			}
			ifExisteDejaSujet(existeDeja, noteInteret);
			sujet.setNoteInteretSujet(somme / nombreDeNoteInteret);
		}
		sujetDAO.modifier(sujet);
	}

	private Float definitNoteInteretTechno(Long idUtilisateur, Long idSujet, int score, Float somme) {
		int nombreDeNoteInteret;
		NoteInteretTechno noteInteret = new NoteInteretTechno();
		noteInteret.setIdSujet(idSujet);
		List<NoteInteretTechno> notesInteretTrouves = noteInteretTechnoDAO.trouver(noteInteret);
		nombreDeNoteInteret = notesInteretTrouves.size() + 1;

		noteInteret.setIdProfesseur(idUtilisateur);
		noteInteret.setNote(score);
		boolean existeDeja = false;
		for (NoteInteretTechno noteInteretTechno : notesInteretTrouves) {
			if ((noteInteretTechno.getIdSujet().equals(idSujet))
					&& (noteInteretTechno.getIdProfesseur().equals(idUtilisateur))) {
				existeDeja = true;

				noteInteretTechnoDAO.modifier(noteInteret);
				nombreDeNoteInteret--;
			} else {
				somme += noteInteretTechno.getNote();
			}
		}
		ifExisteDejaTechno(existeDeja, noteInteret);
		return somme / nombreDeNoteInteret;
	}

	private void ifExisteDejaSujet(boolean existeDeja, NoteInteretSujet noteInteret) {
		if (!existeDeja) {
			noteInteretSujetDAO.creer(noteInteret);
		}
	}

	private void ifExisteDejaTechno(boolean existeDeja, NoteInteretTechno noteInteret) {
		if (!existeDeja) {
			noteInteretTechnoDAO.creer(noteInteret);
		}
	}

}