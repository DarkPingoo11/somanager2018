package fr.eseo.ld.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.eseo.ld.beans.EtatSujet;
import fr.eseo.ld.beans.OptionESEO;
import fr.eseo.ld.beans.Professeur;
import fr.eseo.ld.beans.ProfesseurSujet;
import fr.eseo.ld.beans.Sujet;
import fr.eseo.ld.beans.Utilisateur;
import fr.eseo.ld.dao.DAOFactory;
import fr.eseo.ld.dao.NotificationDAO;
import fr.eseo.ld.dao.OptionESEODAO;
import fr.eseo.ld.dao.SujetDAO;
import fr.eseo.ld.dao.UtilisateurDAO;

/**
 * Servlet implementation class InscriptionProfSujet
 */
@WebServlet("/InscriptionProfSujet")
public class InscriptionProfSujet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private static final String VUE_FORM = "/WEB-INF/formulaires/inscriptionProfSujet.jsp";

	private static final String SUR_LE_SUJET = " sur le sujet ";

	private UtilisateurDAO utilisateurDAO;
	private SujetDAO sujetDAO;
	private OptionESEODAO optionESEODAO;
	private NotificationDAO notificationDAO;

	@Override
	public void init() throws ServletException {
		/* Récupération d'instances de DAO */
		DAOFactory daoFactory = (DAOFactory) this.getServletContext().getAttribute(ServletUtilitaire.CONF_DAO_FACTORY);
		this.utilisateurDAO = daoFactory.getUtilisateurDao();
		this.sujetDAO = daoFactory.getSujetDao();
		this.optionESEODAO = daoFactory.getOptionESEODAO();
		this.notificationDAO = daoFactory.getNotificationDao();
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		/* Récupération de la session depuis la requête */
		HttpSession session = request.getSession();

		/* Récupération de l'ID de l'Utilisateur en session */
		Utilisateur utilisateurSession = (Utilisateur) session.getAttribute(ServletUtilitaire.ATT_SESSION_USER);
		Long idUtilisateurSession = utilisateurSession.getIdUtilisateur();

		/* Recherche de tous les sujets publiés via sujetDao */
		Sujet sujetPublie = new Sujet();
		EtatSujet etatSujet = EtatSujet.PUBLIE;
		sujetPublie.setEtat(etatSujet);
		List<Sujet> listeSujetsPublies = this.sujetDAO.trouver(sujetPublie);

		/* Recherche de tous les suejts attribués vis sujetDAO */
		Sujet sujetAttribue = new Sujet();
		EtatSujet etatSujetAttribue = EtatSujet.ATTRIBUE;
		sujetAttribue.setEtat(etatSujetAttribue);
		List<Sujet> listeSujetsAttribues = this.sujetDAO.trouver(sujetAttribue);

		for (Sujet sujet : listeSujetsAttribues) {
			listeSujetsPublies.add(sujet);
		}

		// Tous les couples professeurs/sujet trouvés par sujet sont stockés
		// dans une même liste
		// On obtient ainsi une liste de professeurs/sujet
		List<ProfesseurSujet> professeursSujet = new ArrayList<>();

		for (Sujet sujet : listeSujetsPublies) {
			// Recherche des professeurs attribués à chaque sujet
			// Récupération des professeurs par sujet
			for (ProfesseurSujet profSujet : this.utilisateurDAO.trouverProfesseursAttribuesAUnSujet(sujet)) {
				// Stockage des professeursSujet (obtention de la fonction et du
				// validé)
				professeursSujet.add(profSujet);
			}
		}

		/*
		 * Liste de tous les utilisateurs pour récupérer le nom et le prénom des
		 * professeurs
		 */
		List<Utilisateur> utilisateurs = this.utilisateurDAO.lister();

		/*
		 * Fabrication de la liste des fonctions de l'utilisateur en session
		 */
		List<String> fonctionsUtilisateurSession = this.listerFonctionsUtilisateurSession(listeSujetsPublies,
				professeursSujet, idUtilisateurSession);

		/*
		 * Fabrication d'une liste de référents en parallèle de la liste des sujets
		 * publiés
		 */
		List<String> referents = this.listerReferents(listeSujetsPublies, professeursSujet);

		/* Traitement des options */
		List<String> options = new ArrayList<>();
		List<String> optionsSujet = new ArrayList<>();

		// Recherche des options du sujet
		for (Sujet sujet : listeSujetsPublies) {
			List<OptionESEO> recupOptions = this.optionESEODAO.trouverOptionSujet(sujet);

			StringBuilder optionSujet = new StringBuilder();
			for (OptionESEO option : recupOptions) {
				optionSujet.append(option.getNomOption() + " ");
				if (!options.contains(option.getNomOption())) {
					options.add(option.getNomOption());
				}
			}
			optionsSujet.add(String.valueOf(optionSujet));
		}

		request.setAttribute("optionSujet", optionsSujet);
		request.setAttribute("fonctionsUtilisateurSession", fonctionsUtilisateurSession);
		request.setAttribute("referents", referents);
		request.setAttribute(ServletUtilitaire.ATT_LISTE_OPTIONS, options);
		request.setAttribute(ServletUtilitaire.ATT_LISTE_SUJETS, listeSujetsPublies);
		request.setAttribute(ServletUtilitaire.ATT_LISTE_PROFESSEURS_ATTIBUES_A_UN_SUJET, professeursSujet);
		request.setAttribute(ServletUtilitaire.ATT_UTILISATEURS, utilisateurs);

		this.getServletContext().getRequestDispatcher(VUE_FORM).forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		/* Récupération de la session depuis la requête */
		HttpSession session = request.getSession();

		/* Récupération des données saisies */
		String fonction = request.getParameter(ServletUtilitaire.CHAMP_FONCTION_PROF);
		Long idSujet = Long.parseLong(request.getParameter(ServletUtilitaire.CHAMP_ID_SUJET));

		/* Récupération de l'ID de l'Utilisateur en session */
		Utilisateur utilisateur = (Utilisateur) session.getAttribute(ServletUtilitaire.ATT_SESSION_USER);
		Long idUtilisateur = utilisateur.getIdUtilisateur();

		Sujet sujet = new Sujet();
		sujet.setIdSujet(idSujet);
		List<Sujet> sujets = this.sujetDAO.trouver(sujet);

		/*
		 * Insertion du professeurSujet (sa fonction par rapport à un sujet) dans la BDD
		 * via
		 */
		Professeur professeur = new Professeur();
		professeur.setIdProfesseur(idUtilisateur);

		// Liste des fonctions de l'utilisateur en session
		List<ProfesseurSujet> professeurSujetUtilisateurSession = this.utilisateurDAO
				.trouverProfesseursSujet(utilisateur);
		List<ProfesseurSujet> professeurSujet = new ArrayList<>();

		for (ProfesseurSujet ps : professeurSujetUtilisateurSession) {
			if (ps.getIdSujet().equals(sujets.get(0).getIdSujet())) {
				professeurSujet.add(ps);
			}
		}

		if (professeurSujet.isEmpty()) {
			this.sujetDAO.creerProfesseurSujet(professeur, sujets.get(0), fonction, "non");

			/* On notifie le professeur de sa candidature */
			this.notificationDAO.ajouterNotification(professeur.getIdProfesseur(),
					"Vous avez été ajouté comme " + fonction + SUR_LE_SUJET + sujets.get(0).getTitre());
		} else {
			if (fonction != professeurSujet.get(0).getFonction().name()) {
				this.sujetDAO.supprimerProfesseurSujet(sujets.get(0), professeur);
				this.sujetDAO.creerProfesseurSujet(professeur, sujets.get(0), fonction, "non");

				/* On notifie le professeur du changement */
				this.notificationDAO.ajouterNotification(professeur.getIdProfesseur(),
						"Vous êtes passé de " + professeurSujet.get(0).getFonction().name() + " à " + fonction
								+ SUR_LE_SUJET + sujets.get(0).getTitre());
			} else {
				this.sujetDAO.creerProfesseurSujet(professeur, sujets.get(0), fonction, "non");

				/* On notifie le professeur de sa candidature */
				this.notificationDAO.ajouterNotification(professeur.getIdProfesseur(),
						"Vous avez été ajouté comme " + fonction + SUR_LE_SUJET + sujets.get(0).getTitre());
			}
		}

		/* Redirection vers la page d'accueil */
		response.sendRedirect(request.getHeader(ServletUtilitaire.HEADER));
	}

	/**
	 * Retourne une liste des fonction de l'utilisateur
	 * 
	 * @param List<Sujet>
	 *            sujets
	 * @param List<ProfesseurSujet>
	 *            professeurSujets
	 * @param Long
	 *            idUtilisateurSession, l'identifiant de l'utilisateur en session
	 * 
	 * @return List<String> la liste des référents
	 * 
	 */
	private List<String> listerFonctionsUtilisateurSession(List<Sujet> sujets, List<ProfesseurSujet> professeurSujets,
			Long idUtilisateurSession) {
		// Liste des fonctions de l'utilisateur en session
		List<ProfesseurSujet> professeurSujetUtilisateurSession = new ArrayList<>();
		for (ProfesseurSujet ps : professeurSujets) {
			if (ps.getIdProfesseur().equals(idUtilisateurSession)) {
				professeurSujetUtilisateurSession.add(ps);
			}
		}

		List<String> fonctionsUtilisateurSession = new ArrayList<>();
		// Fabrication d'une liste de même taille que listeSujetsPublies (sujets) qui
		// pour chaque sujet indiquera
		// si l'utilisateur en session possède une fonction pour ce sujet

		for (Sujet sujet : sujets) {
			String fonctionUtilisateurSessionParDefaut = "";
			for (ProfesseurSujet ps : professeurSujetUtilisateurSession) {
				if (ps.getIdSujet().equals(sujet.getIdSujet())) {

					fonctionUtilisateurSessionParDefaut = retournerFonction(ps.getFonction().name());
				}
			}
			fonctionsUtilisateurSession.add(fonctionUtilisateurSessionParDefaut);
		}
		return fonctionsUtilisateurSession;
	}

	/**
	 * Retourne une liste de référents
	 * 
	 * @param List<Sujet>
	 *            sujets liste des sujets dans laquelle on cherche si un professeur
	 *            est référent dessus
	 * @param List<ProfesseurSujet>
	 *            professeurSujets
	 * 
	 * @return List<String> la liste des référents
	 * 
	 */
	private List<String> listerReferents(List<Sujet> sujets, List<ProfesseurSujet> professeurSujets) {
		List<String> referents = new ArrayList<>();
		for (Sujet sujet : sujets) {
			String referent = "";
			for (ProfesseurSujet ps : professeurSujets) {
				if (ps.getIdSujet().equals(sujet.getIdSujet()) && "REFERENT".equals(ps.getFonction().name())) {
					referent = "référent";
				}
			}
			referents.add(referent);
		}

		return referents;
	}

	/**
	 * Retourne la fonction d'un professeur
	 * 
	 * @param String
	 *            fonction, la fonction qe l'on souhaite connaître
	 * @return String fonctionParDefaut
	 */
	private static String retournerFonction(String fonction) {
		String fonctionParDefaut = "";
		switch (fonction) {
		case "COENCADRANT":
			fonctionParDefaut = "co-encadrant";
			break;
		case "CONSULTANT":
			fonctionParDefaut = "consultant";
			break;
		case "INTERESSE":
			fonctionParDefaut = "interessé";
			break;
		case "REFERENT":
			fonctionParDefaut = "référent";
			break;
		default:
			break;
		}

		return fonctionParDefaut;
	}

}