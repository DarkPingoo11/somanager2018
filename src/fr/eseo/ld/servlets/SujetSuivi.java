package fr.eseo.ld.servlets;

import fr.eseo.ld.beans.*;
import fr.eseo.ld.dao.*;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.AbstractMap.SimpleEntry;
import java.util.*;

/**
 * Servlet implementation class SujetSuivi
 */
@WebServlet("/SujetSuivi")
public class SujetSuivi extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private static final String ATT_LISTE_SUJETS = "listeSujets";
	private static final String ATT_LISTE_FONCTIONS = "listeFonctions";
	private static final String ATT_LISTE_UTILISATEUR_PAR_SUJET = "utilisateursSurLeSujet";
	private static final String ATT_LISTE_PROFESSEUR_PAR_SUJET = "professeursSurLeSujet";
	private static final String ATT_LISTE_POSTER_PAR_SUJET = "posterSurLeSujet";
	private static final String ATT_LISTE_INFO_SOUTENANCE = "informationsSoutenance";
	private static final String ATT_LISTE_INFO_JURY_POSTER = "informationsJuryPoster";
	private static final String ATT_LISTE_ANNEE_SCOLAIRE = "informationsAnneeScolaire";
	private static final String ATT_LISTE_OPTIONS_SUJET = "informationsOptionsSujet";
	private static final String ATT_NOM_ROLE_ETUDIANT = "etudiant";
	private static final String ATT_POSSEDE_POSTER = "possedePoster";
	private static final String ATT_DROIT_DEPOSE = "dateLimitePassee";

	private static final String VUE_FORM = "/WEB-INF/formulaires/sujetSuivi.jsp";

	private UtilisateurDAO utilisateurDAO;
	private SujetDAO sujetDAO;
	private EquipeDAO equipeDAO;
	private PosterDAO posterDAO;
	private NotificationDAO notificationDAO;
	private SoutenanceDAO soutenanceDAO;
	private JurySoutenanceDAO jurySoutenanceDAO;
	private JuryPosterDAO juryPosterDAO;
	private AnneeScolaireDAO anneeScolaireDAO;
	private OptionESEODAO optionESEODAO;

	// Initialisation du logger
	private static Logger logger = Logger.getLogger(SujetSuivi.class.getName());

	@Override
	public void init() throws ServletException {
		/* Récupération d'instances de DAO */
		DAOFactory daoFactory = (DAOFactory) this.getServletContext().getAttribute(ServletUtilitaire.CONF_DAO_FACTORY);
		this.utilisateurDAO = daoFactory.getUtilisateurDao();
		this.sujetDAO = daoFactory.getSujetDao();
		this.equipeDAO = daoFactory.getEquipeDAO();
		this.posterDAO = daoFactory.getPosterDao();
		this.notificationDAO = daoFactory.getNotificationDao();
		this.soutenanceDAO = daoFactory.getSoutenanceDAO();
		this.jurySoutenanceDAO = daoFactory.getJurySoutenanceDAO();
		this.juryPosterDAO = daoFactory.getJuryPosterDAO();
		this.anneeScolaireDAO = daoFactory.getAnneeScolaireDAO();
		this.optionESEODAO = daoFactory.getOptionESEODAO();
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		/* Récupération de l'utilisateur choisi */
		Utilisateur utilisateurSession = (Utilisateur) request.getSession()
				.getAttribute(ServletUtilitaire.ATT_SESSION_USER);

		/* En fonction de l'utilisateur, on ne charge pas les mêmes élements */

		// Si c'est un utilisateur avec le role étudiant
		if (utilisateurDAO.trouverRole(utilisateurSession).get(0).getNomRole().equals(ATT_NOM_ROLE_ETUDIANT)) {
			this.getServletContext().getRequestDispatcher(VUE_FORM)
					.forward(chargerElementsEtudiant(request, utilisateurSession), response);
		}
		// Sinon c'est un prof ou entreprise extérieur
		else {
			forwardProfesseur(request, response, utilisateurSession);
		}
	}

	private void forwardProfesseur(HttpServletRequest request, HttpServletResponse response, Utilisateur utilisateurSession) throws ServletException, IOException {
		this.getServletContext().getRequestDispatcher(VUE_FORM)
				.forward(chargerElementsProfesseur(request, utilisateurSession), response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		/* Récupération de l'utilisateur choisi */
		Utilisateur utilisateurSession = (Utilisateur) request.getSession()
				.getAttribute(ServletUtilitaire.ATT_SESSION_USER);

		// On récupère l'ID du poster à valider
		String idPoster = request.getParameter("idPosterChoisi");

		// On récupèr l'ID du sujet du poster
		String idSujet = request.getParameter("idSujetChoisi");

		/* On modifie son attribut valide dans la bdd */
		Poster poster = new Poster();
		poster.setIdPoster(Long.valueOf(idPoster));
		poster.setValide("oui");
		posterDAO.modifier(poster);

		/* On notifie les membres de l'équipe */
		Sujet sujet = new Sujet();
		sujet.setIdSujet(Long.valueOf(idSujet));
		SimpleEntry<Long, List<Utilisateur>> liste = ServletUtilitaire.trouverUtilisateurEquipe(sujet, equipeDAO,
				utilisateurDAO);
		ServletUtilitaire.notifierUtilisateur(liste.getValue(), "Votre poster a été validé par le professeur référent",
				"/VoirMonSujet", this.notificationDAO);

		forwardProfesseur(request, response, utilisateurSession);
	}

	/**
	 * Charge les sujets, les listes des étudiants par sujet, la fonction du
	 * professeur sur le sujet ainsi que tout les attributs présents sur la page
	 * 
	 * @param request
	 * @return request avec les différentes infos
	 */
	private HttpServletRequest chargerElementsProfesseur(HttpServletRequest request, Utilisateur utilisateurSession) {

		List<ProfesseurSujet> listeAttributions = this.utilisateurDAO.trouverProfesseursSujet(utilisateurSession);

		List<Sujet> listeSujet = new ArrayList<>();

		List<FonctionProfesseurSujet> fonctionSurLeSujet = new ArrayList<>();

		Map<Long, List<Utilisateur>> utilisateursSurLeSujet = new HashMap<>();
		Map<Long, List<String>> professeursSurLeSujet = new HashMap<>();
		Map<Long, Poster> posterSurLeSujet = new HashMap<>();
		Map<Long, List<String>> informationsSoutenance = new HashMap<>();
		Map<Long, List<String>> informationsJuryPoster = new HashMap<>();
		Map<Long, List<AnneeScolaire>> informationsAnneeScolaire = new HashMap<>();
		Map<Long, List<OptionESEO>> informationsOptionsSujet = new HashMap<>();

		for (ProfesseurSujet profsujet : listeAttributions) {
			if ("oui".equals(profsujet.getValide())) {

				// On récupère la fonction du professeur sur ce sujet
				fonctionSurLeSujet.add(profsujet.getFonction());

				/*
				 * On récupère le sujet sur lequel le professeur est attribué et on ajoute ce
				 * sujet à la liste des sujets du prof :
				 */
				Sujet sujet = sujetDAO.trouver(profsujet.getIdSujet());
				listeSujet.add(sujet);

				/*
				 * On récupère la liste des étudiants de l'équipe, si l'équipe a été créée
				 */
				SimpleEntry<Long, List<Utilisateur>> resultatUtilisateurEquipe = ServletUtilitaire
						.trouverUtilisateurEquipe(sujet, equipeDAO, utilisateurDAO);
				Long keyUtilisateur = resultatUtilisateurEquipe.getKey();
				List<Utilisateur> valueUtilisateur = resultatUtilisateurEquipe.getValue();
				utilisateursSurLeSujet.put(keyUtilisateur, valueUtilisateur);

				/*
				 * On recupère la liste des professeurs concernés par le sujet
				 */
				SimpleEntry<Long, List<String>> resultatProfesseurEquipe = ServletUtilitaire
						.trouverProfesseurConcerne(sujet, utilisateurDAO);
				Long keyProfesseur = resultatProfesseurEquipe.getKey();
				List<String> valueProfesseur = resultatProfesseurEquipe.getValue();
				professeursSurLeSujet.put(keyProfesseur, valueProfesseur);

				/* On récupère l'entrée pour le poster du sujet */
				SimpleEntry<Long, Poster> resultatTrouverPoster = ServletUtilitaire.trouverPoster(sujet, posterDAO);
				Long keyPoster = resultatTrouverPoster.getKey();
				Poster valuePoster = resultatTrouverPoster.getValue();
				posterSurLeSujet.put(keyPoster, valuePoster);

				/* On récupère les données concernant les soutenances */
				SimpleEntry<Long, List<String>> resultatInfoSoutenance = ServletUtilitaire
						.trouverInformationsJurySoutenance(sujet, soutenanceDAO, jurySoutenanceDAO, utilisateurDAO);
				Long keyInfoSoutenance = resultatInfoSoutenance.getKey();
				List<String> valueInfoSoutenance = resultatInfoSoutenance.getValue();
				informationsSoutenance.put(keyInfoSoutenance, valueInfoSoutenance);

				/* On récupère les données concernant les jurys poster */
				SimpleEntry<Long, List<String>> resultatInformationsJuryPoster = ServletUtilitaire
						.trouverInformationsJuryPoster(valuePoster, sujet, juryPosterDAO, utilisateurDAO);
				Long keynformationsJuryPoster = resultatInformationsJuryPoster.getKey();
				List<String> valueInormationsJuryPoster = resultatInformationsJuryPoster.getValue();
				informationsJuryPoster.put(keynformationsJuryPoster, valueInormationsJuryPoster);

				/*
				 * On récupère les données concernant l'année scolaire en cours
				 */
				SimpleEntry<Long, List<AnneeScolaire>> resultatAnneeScolaire = ServletUtilitaire
						.trouverInformationsAnneeScolaire(sujet, anneeScolaireDAO, optionESEODAO);
				Long keyAnneeScolaire = resultatAnneeScolaire.getKey();
				List<AnneeScolaire> valueAnneeScolaire = resultatAnneeScolaire.getValue();
				informationsAnneeScolaire.put(keyAnneeScolaire, valueAnneeScolaire);

				informationsOptionsSujet.put(sujet.getIdSujet(), this.optionESEODAO.trouverOptionSujet(sujet));
			}
		}

		request.setAttribute(ATT_LISTE_SUJETS, listeSujet);
		request.setAttribute(ATT_LISTE_FONCTIONS, fonctionSurLeSujet);
		request.setAttribute(ATT_LISTE_UTILISATEUR_PAR_SUJET, utilisateursSurLeSujet);
		request.setAttribute(ATT_LISTE_PROFESSEUR_PAR_SUJET, professeursSurLeSujet);
		request.setAttribute(ATT_LISTE_POSTER_PAR_SUJET, posterSurLeSujet);
		request.setAttribute(ATT_LISTE_INFO_SOUTENANCE, informationsSoutenance);
		request.setAttribute(ATT_LISTE_INFO_JURY_POSTER, informationsJuryPoster);
		request.setAttribute(ATT_LISTE_ANNEE_SCOLAIRE, informationsAnneeScolaire);
		request.setAttribute(ATT_LISTE_OPTIONS_SUJET, informationsOptionsSujet);

		return request;
	}

	/**
	 * Charge les sujets, les listes des étudiants par sujet, sa fonction sur le
	 * sujet.
	 * 
	 * @param request
	 * @return request avec les différentes infos
	 */
	private HttpServletRequest chargerElementsEtudiant(HttpServletRequest request, Utilisateur utilisateurSession) {

		Equipe equipe = equipeDAO.trouver(utilisateurSession.getIdUtilisateur());

		if (equipe != null && "oui".equals(equipe.getValide())) {
			Map<Long, List<Utilisateur>> utilisateursSurLeSujet = new HashMap<>();
			Map<Long, List<String>> professeursSurLeSujet = new HashMap<>();
			Map<Long, Poster> posterSurLeSujet = new HashMap<>();
			Map<Long, List<String>> informationsSoutenance = new HashMap<>();
			Map<Long, List<AnneeScolaire>> informationsAnneeScolaire = new HashMap<>();
			Map<Long, List<OptionESEO>> informationsOptionsSujet = new HashMap<>();

			Sujet sujet = new Sujet();
			sujet.setIdSujet(equipe.getIdSujet());
			sujet = sujetDAO.trouver(sujet).get(0);
			List<Sujet> listeSujet = new ArrayList<>();
			listeSujet.add(sujet);

			// verification si le sujet possède un poster
			boolean possedePoster = this.sujetDAO.possedePoster(sujet);

			/* On récupère la liste des étuidants de l'équipe si l'équipe a été créée */
			SimpleEntry<Long, List<Utilisateur>> resultatUtilisateurEquipe = ServletUtilitaire
					.trouverUtilisateurEquipe(sujet, equipeDAO, utilisateurDAO);
			Long keyUtilisateur = resultatUtilisateurEquipe.getKey();
			List<Utilisateur> valueUtilisateur = resultatUtilisateurEquipe.getValue();
			utilisateursSurLeSujet.put(keyUtilisateur, valueUtilisateur);

			/* On recupère la liste des professeurs concernés par le sujet */
			SimpleEntry<Long, List<String>> resultatProfesseurEquipe = ServletUtilitaire
					.trouverProfesseurConcerne(sujet, utilisateurDAO);
			Long keyProfesseur = resultatProfesseurEquipe.getKey();
			List<String> valueProfesseur = resultatProfesseurEquipe.getValue();
			professeursSurLeSujet.put(keyProfesseur, valueProfesseur);

			/* On récupère l'entrée pour le poster du sujet */
			SimpleEntry<Long, Poster> resultatTrouverPoster = ServletUtilitaire.trouverPoster(sujet, posterDAO);
			Long keyPoster = resultatTrouverPoster.getKey();
			Poster valuePoster = resultatTrouverPoster.getValue();
			posterSurLeSujet.put(keyPoster, valuePoster);

			/* On récupère les données concernant les soutenances */
			SimpleEntry<Long, List<String>> resultatInfoSoutenance = ServletUtilitaire
					.trouverInformationsJurySoutenance(sujet, soutenanceDAO, jurySoutenanceDAO, utilisateurDAO);
			Long keyInfoSoutenance = resultatInfoSoutenance.getKey();
			List<String> valueInfoSoutenance = resultatInfoSoutenance.getValue();
			informationsSoutenance.put(keyInfoSoutenance, valueInfoSoutenance);

			/* On récupère les données concernant l'année scolaire en cours */
			SimpleEntry<Long, List<AnneeScolaire>> resultatAnneeScolaire = ServletUtilitaire
					.trouverInformationsAnneeScolaire(sujet, anneeScolaireDAO, optionESEODAO);
			Long keyAnneeScolaire = resultatAnneeScolaire.getKey();
			List<AnneeScolaire> valueAnneeScolaire = resultatAnneeScolaire.getValue();
			informationsAnneeScolaire.put(keyAnneeScolaire, valueAnneeScolaire);

			informationsOptionsSujet.put(sujet.getIdSujet(), this.optionESEODAO.trouverOptionSujet(sujet));

			request.setAttribute(ATT_DROIT_DEPOSE, verificationDroitDepot(sujet));
			request.setAttribute(ATT_POSSEDE_POSTER, possedePoster);
			request.setAttribute(ATT_LISTE_SUJETS, listeSujet);
			request.setAttribute(ATT_LISTE_UTILISATEUR_PAR_SUJET, utilisateursSurLeSujet);
			request.setAttribute(ATT_LISTE_PROFESSEUR_PAR_SUJET, professeursSurLeSujet);
			request.setAttribute(ATT_LISTE_POSTER_PAR_SUJET, posterSurLeSujet);
			request.setAttribute(ATT_LISTE_INFO_SOUTENANCE, informationsSoutenance);
			request.setAttribute(ATT_LISTE_ANNEE_SCOLAIRE, informationsAnneeScolaire);
			request.setAttribute(ATT_LISTE_OPTIONS_SUJET, informationsOptionsSujet);

		}
		return request;
	}

	/**
	 * Vérification du droit de dépose de poster
	 * 
	 * @param sujet
	 *            ou on doit déposer de poster
	 * @return un boolean true si il a le droit
	 */
	private boolean verificationDroitDepot(Sujet sujet) {

		List<AnneeScolaire> anneesScolairesOption = new ArrayList<>();
		List<OptionESEO> recupOptions = this.optionESEODAO.trouverOptionSujet(sujet);

		// recuperation de l'anneeScolaire correspondant à l'année en cours pour les
		// options données
		for (OptionESEO option : recupOptions) {
			AnneeScolaire anneeScolaire = new AnneeScolaire();
			anneeScolaire.setIdOption(option.getIdOption());
			anneesScolairesOption.add(this.anneeScolaireDAO.trouver(anneeScolaire).get(0));
		}

		// recupération de la date limite du depot d'un poster, il s'agit de la
		// date la plus tot en cas de poster possedant plusieurs options:
		List<Date> datesDepotOption = new ArrayList<>();
		for (AnneeScolaire anneeScolaire : anneesScolairesOption) {
			datesDepotOption.add(anneeScolaire.getDateDepotPoster());
		}

		// recuperation de la date la plus tot parmis les dates de différentes options :
		Date dateLimiteDepot = null;
		Date datePrec = null;

		for (Date date : datesDepotOption) {
			if (dateLimiteDepot == null) {
				dateLimiteDepot = date;
				datePrec = date;
			}
			if (date.compareTo(datePrec) < 0) {
				dateLimiteDepot = date;
			}
			datePrec = date;
		}

		// boolean testant si la date limite de depot de poster est passée ou non:
		SimpleDateFormat formatDate = new SimpleDateFormat("yyyy-MM-dd");
		String dateDuJourString = formatDate.format(new Date());
		Date dateDuJour = new Date();
		try {
			dateDuJour = formatDate.parse(dateDuJourString);
		} catch (ParseException e) {
			logger.log(Level.FATAL, "erreur de parsing de date du jour", e);
		}

		// boolean qui est true si la date limite est dépassée
		return dateDuJour.compareTo(dateLimiteDepot) > 0;
	}

}