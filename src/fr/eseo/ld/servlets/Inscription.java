package fr.eseo.ld.servlets;

import static fr.eseo.ld.servlets.ServletUtilitaire.validerChampsInscription;

import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import fr.eseo.ld.beans.Utilisateur;
import fr.eseo.ld.dao.DAOFactory;
import fr.eseo.ld.dao.UtilisateurDAO;

/**
 * Servlet permettant l'inscription d'un utilisateur.
 * 
 * BCrypt : Copyright (c) 2002 Johnny Shelley All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the author nor any contributors may be used to endorse
 * or promote products derived from this software without specific prior written
 * permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
@WebServlet("/Inscription")
public class Inscription extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private static final String VUE_FORM = "/WEB-INF/formulaires/inscription.jsp";

	private static Logger logger = Logger.getLogger(Inscription.class.getName());

	private UtilisateurDAO utilisateurDAO;

	@Override
	public void init() throws ServletException {
		/* Récupération d'une instance de notre DAO Utilisateur */
		this.utilisateurDAO = ((DAOFactory) this.getServletContext().getAttribute(ServletUtilitaire.CONF_DAO_FACTORY))
				.getUtilisateurDao();
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		/* A la réception d'une requête GET, simple affichage du formulaire */
		this.getServletContext().getRequestDispatcher(VUE_FORM).forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		/* Validation des champs */
		String identifiant = request.getParameter(ServletUtilitaire.CHAMP_IDENTIFIANT);
		String email = request.getParameter(ServletUtilitaire.CHAMP_EMAIL);
		Map<String, String> erreurs = validerChampsInscription(utilisateurDAO, identifiant, email);

		/*
		 * Si aucune erreur de validation n'a eu lieu, alors insertion de l'Utilisateur
		 * dans la BDD, sinon ré-affichage du formulaire.
		 */
		String resultat;
		if (erreurs.isEmpty()) {
			Utilisateur utilisateur = ServletUtilitaire.mapInscription(request, identifiant, email, "non");
			this.utilisateurDAO.creer(utilisateur);
			resultat = "Succès de l'inscription. Veuillez attendre la validation de votre compte par l'administrateur.";
			logger.log(Level.INFO, resultat);
			request.setAttribute(ServletUtilitaire.ATT_RESULTAT, resultat);
			/* Ré-affichage du formulaire avec message de succès */
			this.getServletContext().getRequestDispatcher(VUE_FORM).forward(request, response);
		} else {
			resultat = "Échec de l'inscription. ";
			logger.log(Level.WARN, resultat);
			request.setAttribute(ServletUtilitaire.ATT_RESULTAT, resultat);
			request.setAttribute(ServletUtilitaire.ATT_ERREURS, erreurs);
			/* Ré-affichage du formulaire avec les erreurs */
			this.getServletContext().getRequestDispatcher(VUE_FORM).forward(request, response);
		}
	}

}