package fr.eseo.ld.servlets;

import fr.eseo.ld.beans.*;
import fr.eseo.ld.dao.*;
import fr.eseo.ld.utils.IOUtilitaire;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Servlet permettant le téléchargement de tous les posters d'une option.
 *
 * @author Alexandre CLAMENS
 */
@WebServlet("/TelechargerPostersOption")
public class TelechargerPostersOption extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public static final int TAILLE_TAMPON = 10240; // 10 ko

	private static final String CHEMIN_POSTER = "/home/etudiant/Posters";
	private static final String VUE_FORM = "/WEB-INF/formulaires/gererSujets.jsp";

	private PosterDAO posterDAO;
	private SujetDAO sujetDAO;
	private OptionESEODAO optionESEODAO;
	private UtilisateurDAO utilisateurDAO;
	private NotificationDAO notificationDAO;

	@Override
	public void init() throws ServletException {
		/* Récupération d'instances de DAO */
		DAOFactory daoFactory = (DAOFactory) this.getServletContext().getAttribute(ServletUtilitaire.CONF_DAO_FACTORY);
		this.posterDAO = daoFactory.getPosterDao();
		this.sujetDAO = daoFactory.getSujetDao();
		this.optionESEODAO = daoFactory.getOptionESEODAO();
		this.notificationDAO = daoFactory.getNotificationDao();
		this.utilisateurDAO = daoFactory.getUtilisateurDao();

	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		/* Récupération de l'ID de l'Utilisateur en session */
		HttpSession session = request.getSession();
		Utilisateur utilisateurSession = (Utilisateur) session.getAttribute(ServletUtilitaire.ATT_SESSION_USER);
		Long idUtilisateurSession = utilisateurSession.getIdUtilisateur();

		/* Recherche des options de l'utilisateur via optionESEODAO */
		Utilisateur utilisateur = new Utilisateur();
		utilisateur.setIdUtilisateur(idUtilisateurSession);

		/* Recherche des rôles de l'utilisateur */
		List<Role> rolesUtilisateur = this.utilisateurDAO.trouverRole(utilisateur);

		/*
		 * Attribution des options pour lesquelles le telechargement de posters doit
		 * s'effectuer (option à laquelle le prof est responsable ou toutes les options
		 * si l'utilisateur est le service com)
		 */
		List<OptionESEO> optionConcernee = new ArrayList<>();
		// les seuls rôles permettant le telechargement de plusieurs posters sont
		// profResponsable et service com
		for (Role role : rolesUtilisateur) {
			if (role.getIdRole() == 5) { // cas du prof responsable
				optionConcernee = this.optionESEODAO.trouverOptionRoleUtilisateur(utilisateur.getIdUtilisateur(),
						role.getIdRole());
			} else if (role.getIdRole() == 18) { // cas du service com
				optionConcernee = this.optionESEODAO.lister();
			}
		}

		/*
		 * Récupération des sujets correspondant aux posters validés de l'option
		 * concernée
		 */
		List<Sujet> sujetsValidesOption = this.sujetDAO.trouverSujetsPostersValidesOptions(optionConcernee);

		/*
		 * Récupération des posters correspondant aux sujets validés de l'option
		 * concernée
		 */
		List<Poster> postersValideOption = new ArrayList<>();
		for (Sujet sujet : sujetsValidesOption) {
			Poster poster = this.posterDAO.trouverPoster(sujet.getIdSujet());
			postersValideOption.add(poster);
		}

		/* Zip de tous les posters validés pour une option */
		if (!postersValideOption.isEmpty()) {
			/* Formatage du chemin vers les posters */
			SimpleDateFormat formatAnnee = new SimpleDateFormat("yyyy");
			String annee = formatAnnee.format(new Date());
			String cheminPosters = CHEMIN_POSTER + "/" + annee + "/";

			/* Zip */
			File directory = new File(cheminPosters);
			byte[] zip = IOUtilitaire.zipFiles(directory, postersValideOption);
			ServletOutputStream sos = response.getOutputStream();
			response.setContentType("application/zip");
			response.setHeader("Content-Disposition", "attachment; filename=\"posters.zip\"");
			sos.write(zip);
			sos.flush();
		} else {
			/* Ré-affichage du formulaire avec notification d'erreur */
			this.notificationDAO.ajouterNotification(idUtilisateurSession, "Aucun poster validé pour ces options !");
			this.getServletContext().getRequestDispatcher(VUE_FORM).forward(request, response);
		}
	}


}