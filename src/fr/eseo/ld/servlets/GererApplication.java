package fr.eseo.ld.servlets;

import com.mysql.jdbc.Connection;
import fr.eseo.ld.beans.Callback;
import fr.eseo.ld.beans.CallbackType;
import fr.eseo.ld.beans.Utilisateur;
import fr.eseo.ld.config.FichierProprieteStockage;
import fr.eseo.ld.dao.DAOFactory;
import fr.eseo.ld.dao.NotificationDAO;
import fr.eseo.ld.ldap.FichierPropriete;
import fr.eseo.ld.ldap.LDAP;
import fr.eseo.ld.utils.CallbackUtilitaire;
import fr.eseo.ld.utils.IOUtilitaire;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.*;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Servlet permettant de modifier les paramètres du serveur LDAP et de les
 * enregistrer dans l'application.
 *
 * @author Thomas MENARD
 */
@WebServlet("/GererApplication")
@MultipartConfig(fileSizeThreshold = 1024 * 1024 * 5, maxFileSize = 1024 * 1024 * 10, maxRequestSize = 1024 * 1024 * 10)
public class GererApplication extends HttpServlet {
    private static final long serialVersionUID = 1L;

    private static final String ATT_FORMULAIRE = "formulaire";
    private static final String ATT_LDAP = "ldap";
    private static final String ATT_BDD = "bdd";
    private static final String ATT_STOCKAGE = "stockage";
    private static final String ATT_CONNEXION_LDAP = "ldapConnexion";
    private static final String ATT_CONNEXION_BDD = "bddConnexion";
    private static final String ATT_CONTENU_STYLE_CSS = "contenuStyleCss";
    private static final String ATT_CONTENU_DOSSIER_IMG = "imagesDansImages";

    private static final String ATT_FICHIER_CSS = "/css/style.css";
    private static final String ATT_FICHIER_CSS_DEFAUT = "/css/styleDefaut.css";
    private static final String ATT_CHEMIN_DOSSIER_IMG = "/images";

    private static final String VUE_FORM = "/WEB-INF/formulaires/gererApplication.jsp";
    private static final String VUE_DECO = "/Deconnexion";
    private static final String CHAMP_ERREUR_EXTENSION = "extensionErreur";
    private static final String CHAMP_ERREUR = "erreurs";

    private static final String MSG_CONF_STOCKAGE = "Les paramètres de stockage ont été mis à jour";

    private static final int TAILLE_TAMPON = 10240;

    private static Logger logger = Logger.getLogger(GererApplication.class.getName());

    // Fichier contenant les paramètres utilisés par l'application, on l'initialise
    // et donc on lui donne les paramètres par défaut, contenus dans
    // annuaire.properties
    private FichierPropriete fichierEnCours = new FichierPropriete();
    // Fichier contenant les paramètres de stockage utilisés par l'application, on
    // l'initialise
    // et donc on lui donne les paramètres par défaut, contenus dans
    // stockage.properties
    private FichierProprieteStockage fichierStockageEnCours = new FichierProprieteStockage();

    private DAOFactory bddProperties = DAOFactory.getInstance();

    private NotificationDAO notificationDAO;

    @Override
    public void init() throws ServletException {
        /* Récupération d'instances de DAO */
        DAOFactory daoFactory = (DAOFactory) this.getServletContext().getAttribute(ServletUtilitaire.CONF_DAO_FACTORY);
        this.notificationDAO = daoFactory.getNotificationDao();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        HttpServletRequest nouvelleRequete = chargerElementsPage(request);
        this.getServletContext().getRequestDispatcher(VUE_FORM).forward(nouvelleRequete, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        /* Récupération de l'utilisateur en session */
        HttpSession session = request.getSession();
        Utilisateur utilisateurSession = (Utilisateur) session.getAttribute(ServletUtilitaire.ATT_SESSION_USER);
        // Si l'utilisateur choisit de modifier les paramètres du serveur LDAP
        if ("modifierConfLDAP".equals(request.getParameter(ATT_FORMULAIRE))) {
            modifierConfLDAP(request, response, utilisateurSession);
        }
        // Si l'utilisateur choisit de remettre par défaut les paramètres du LDAP
        if ("defautConfLDAP".equals(request.getParameter(ATT_FORMULAIRE))) {
            defautConfLDAP(request, response, utilisateurSession);
        }
        // Si l'utilisateur choisit de modifier le style de l'application
        if ("modifierCss".equals(request.getParameter(ATT_FORMULAIRE))) {
            modifierCss(request, response, utilisateurSession);
        }
        // Si l'utilisateur choisit de remettre par défaut le style de l'application
        if ("defautStyleCss".equals(request.getParameter(ATT_FORMULAIRE))) {
            defautStyleCss(request, response, utilisateurSession);
        }
        // Si l'utilisateur choisit de modifier les paramètres en BDD
        if ("modifierConfBdd".equals(request.getParameter(ATT_FORMULAIRE))) {
            if(verificationConnexionBDD(request)) {
                modifierConfBdd(request, response, utilisateurSession);
            } else {
                request.getRequestDispatcher(VUE_FORM).forward(chargerElementsPage(request),response);
                // On teste la connexion anonyme au serveur (seule la connexion est testé)
                request.setAttribute(ATT_CONNEXION_BDD, verificationConnexionBDD(request));
            }
        }
        // Si l'utilisateur choisit de remettre par défaut les paramètres du BDD
        if ("defautConfBDD".equals(request.getParameter(ATT_FORMULAIRE))) {
            defautConfBDD(request, response, utilisateurSession);
        }
        // Si l'utilisateur choisit de modifier les paramètres de stockage
        if ("modifierConfStockage".equals(request.getParameter(ATT_FORMULAIRE))) {
            modifierConfStockage(request, response, utilisateurSession);
        }
        // Si l'utilisateur choisit de remettre par défaut les paramètres de stockage
        if ("defaultConfStockage".equals(request.getParameter(ATT_FORMULAIRE))) {
            defaultConfStockage(request, response, utilisateurSession);
        }
        // Si l'utilisateur choisit de modifier une ou des images
        if (request.getContentType().contains("multipart/form-data;")) {
            Map<String, String> erreurs = new HashMap<>();
            /* On parcourt toutes les images rentrées en paramètre de la requete */
            changerImageRequete(request, utilisateurSession, erreurs);
            HttpServletRequest nouvelleRequest = chargerElementsPage(request);
            nouvelleRequest.setAttribute(CHAMP_ERREUR, erreurs);
            this.getServletContext().getRequestDispatcher(VUE_FORM).forward(nouvelleRequest, response);
        }
    }

    /**
     * Test la connexion à la BDD et défini un callback
     * @param request request
     */
    private void doTestConnexionBdd(HttpServletRequest request) {
        DAOFactory df = this.bddProperties;
        if(IOUtilitaire.testConnexion(df.getUrlBdd(), df.getDriver(), df.getUserBdd(), df.getMdpBdd())) {
            request.setAttribute(ATT_CONNEXION_BDD, "Succès de connexion à la base de données");
        } else {
            request.setAttribute(ATT_CONNEXION_BDD, "Les paramètres de base de données sont erronés");
        }
    }

    /**
     * Effecture la requete de changement d'image
     * @param request Request
     * @param utilisateurSession Utilisateur courant
     * @param erreurs Erreurs
     * @throws ServletException En cas d'erreur avec le servlet
     */
    private void changerImageRequete(HttpServletRequest request, Utilisateur utilisateurSession, Map<String, String> erreurs)
            throws ServletException {
        try {
            for (Part part : request.getParts()) {
                String fileName = ServletUtilitaire.extractFileName(part);
                fileName = new File(fileName).getName();
                // Si l'image qu'on souhaite changer n'a pas de nouveaux attributs, on ne
                // modifie pas l'image
                if (!("").equals(fileName) && verificationFichier(fileName)) {
                    String path = this.getServletContext().getRealPath(ATT_CHEMIN_DOSSIER_IMG).concat("/")
                            .concat(part.getName());
                    ecrireFichier(part, path);
                    erreurs.put(CHAMP_ERREUR_EXTENSION, "");
                    this.notificationDAO.ajouterNotification(utilisateurSession.getIdUtilisateur(),
                            "Les images de l\'application ont été mises à jour");
                } else {
                    erreurs.put(CHAMP_ERREUR_EXTENSION,
                            "L'extension ne correspond pas à une image (*.jpg - *.png - *.gif - *.ico)");
                }
            }
        } catch (IOException e) {
            logger.log(Level.FATAL, "Impossible de modifier l'image ", e);
        }
    }

    /**
     * Méthode appelée lorsque l'utilisateur choisi de remettre par défaut le style
     * de l'application
     *
     * @param request
     * @param response
     * @param utilisateurSession
     * @throws ServletException
     * @throws IOException
     */
    private void defautStyleCss(HttpServletRequest request, HttpServletResponse response,
                                Utilisateur utilisateurSession) throws ServletException, IOException {
        ecrireDansFichier(ATT_FICHIER_CSS, lireContenuFichier(ATT_FICHIER_CSS_DEFAUT));
        this.notificationDAO.ajouterNotification(utilisateurSession.getIdUtilisateur(),
                "La style css de l\'application a été mis à jour");
        HttpServletRequest nouvelleRequest = chargerElementsPage(request);
        this.getServletContext().getRequestDispatcher(VUE_FORM).forward(nouvelleRequest, response);

    }

    /**
     * Méthode appelé lorsque l'utilisateur choisit de modifier le style de
     * l'application
     *
     * @param request
     * @param response
     * @param utilisateurSession
     * @throws ServletException
     * @throws IOException
     */
    private void modifierCss(HttpServletRequest request, HttpServletResponse response, Utilisateur utilisateurSession)
            throws ServletException, IOException {
        /* Récupération du style css modifié */
        String nouveauTexte = request.getParameter("texteCss");

        /* Remplacement des caractères spéciaux */
        nouveauTexte = nouveauTexte.replace("¿", "\n");
        nouveauTexte = nouveauTexte.replace("'", "\"");
        ecrireDansFichier(ATT_FICHIER_CSS, nouveauTexte);

        this.notificationDAO.ajouterNotification(utilisateurSession.getIdUtilisateur(),
                "La style css de l\'application a été mis à jour");

        HttpServletRequest nouvelleRequest = chargerElementsPage(request);
        this.getServletContext().getRequestDispatcher(VUE_FORM).forward(nouvelleRequest, response);
    }

    /**
     * Méthode appelée lorsque que l'utilisateur choisit de remettre par défaut les
     * paramètres d'accès au LDAP
     *
     * @param request
     * @param response
     * @param utilisateurSession
     * @throws IOException
     */
    private void defautConfLDAP(HttpServletRequest request, HttpServletResponse response,
                                Utilisateur utilisateurSession) throws IOException {
        // On lit directement le fichier annuaireDefaut.properties et on renvoie les
        // paramètres dans une liste
        List<String> listePropriete = fichierEnCours.lireFichierProprieteParDefaut();

        String[] nouveauxAttributs = { listePropriete.get(0), listePropriete.get(1), listePropriete.get(2),
                listePropriete.get(3), listePropriete.get(4), listePropriete.get(5), listePropriete.get(6),
                listePropriete.get(7), listePropriete.get(8), listePropriete.get(9), listePropriete.get(10),
                listePropriete.get(11) };

        // On modifie les paramètres
        fichierEnCours.modifierFichier(nouveauxAttributs);

        // On réinitialise le fichierEnCours pour qu'il ait les bons paramètres
        this.fichierEnCours = new FichierPropriete();

        this.notificationDAO.ajouterNotification(utilisateurSession.getIdUtilisateur(),
                "La configuration de l\'accès au serveur LDAP à été mise à jour");

        // On déconnecte l'utilisateur et on le renvoie à la page d'accueil
        response.sendRedirect(request.getContextPath() + VUE_DECO);
    }

    /**
     * Méthode appelée lorsque l'utilisateur choisi de modifier la configuration
     * d'accès au LDAP
     *
     * @param request request
     * @param response response
     * @param utilisateurSession utilisateur
     * @throws IOException exception
     */
    private void modifierConfLDAP(HttpServletRequest request, HttpServletResponse response,
                                  Utilisateur utilisateurSession) throws IOException {
        /*
         * On récupère les différents attributs en requete et on utilise la fonction qui
         * modifie les paramètres.
         */
        String[] nouveauxAttributs = { (String) request.getParameter("contexteDN"),
                (String) request.getParameter("serveur"), (String) request.getParameter("port"),
                (String) request.getParameter("nom"), (String) request.getParameter("prenom"),
                (String) request.getParameter("mail"), (String) request.getParameter("identifiant"),
                (String) request.getParameter("contexteDNUser"), (String) request.getParameter("compteUtilisateur"),
                (String) request.getParameter("mdpUtilisateur"), (String) request.getParameter("compteAdministrateur"),
                (String) request.getParameter("mdpAdministrateur") };

        fichierEnCours.modifierFichier(nouveauxAttributs);

        // On réinitialise le fichierEnCours pour qu'il ait les bons paramètres
        this.fichierEnCours = new FichierPropriete();

        this.notificationDAO.ajouterNotification(utilisateurSession.getIdUtilisateur(),
                "La configuration de l'accès au serveur LDAP à été mise à jour");

        // On déconnecte l'utilisateur et on le renvoie à la page d'accueil
        response.sendRedirect(request.getContextPath() + VUE_DECO);
    }

    /**
     * Charge les éléments sur la page
     * @param request request
     * @return request
     */
    private HttpServletRequest chargerElementsPage(HttpServletRequest request) {
        // On charge les paramètres du LDAP dans une List<String> et on le met dans la
        // requete
        request.setAttribute(ATT_LDAP, this.fichierEnCours.lireFichier());
        // On charge les paramètres du LDAP dans une List<String> et on le met dans la
        // requete
        request.setAttribute(ATT_STOCKAGE, this.fichierStockageEnCours.lireFichierStockage());
        // On teste la connexion anonyme au serveur (seule la connexion est testé)
        request.setAttribute(ATT_CONNEXION_LDAP, testConnexionLDAP());
        // On charge les paramètres de la BDD dans et on le met dans la
        // requete
        request.setAttribute(ATT_BDD, this.bddProperties.getProprietes());
        //Test connexion
        doTestConnexionBdd(request);
        //Lire contenu css
        request.setAttribute(ATT_CONTENU_STYLE_CSS, lireContenuFichier(ATT_FICHIER_CSS));
        // On charge toutes les images contenues dans le dossier image
        request.setAttribute(ATT_CONTENU_DOSSIER_IMG, listerRepertoire(ATT_CHEMIN_DOSSIER_IMG));

        return request;
    }


    /**
     * Méthode appelée lorsque que l'utilisateur choisit de remettre par défaut les
     * paramètres d'accès à la BDD
     *
     * @param request
     * @param response
     * @param utilisateurSession
     * @throws IOException
     */
    private void defautConfBDD(HttpServletRequest request, HttpServletResponse response,
                               Utilisateur utilisateurSession) throws IOException {

        // On modifie les parametres
        this.bddProperties.rechargerFichierProprieteParDefaut();

        this.notificationDAO.ajouterNotification(utilisateurSession.getIdUtilisateur(),
                "La configuration de l\'accès à la BDD à été mise à jour");

        // On déconnecte l'utilisateur et on le renvoie à la page d'accueil
        response.sendRedirect(request.getContextPath() + VUE_DECO);
    }


    /**
     * Méthode appelée lorsque l'utilisateur choisi de modifier la configuration
     * d'accès en BDD
     *
     * @param request request
     * @param response response
     * @param utilisateurSession utilisateur
     * @throws IOException exception
     */
    private void modifierConfBdd(HttpServletRequest request, HttpServletResponse response,
                                 Utilisateur utilisateurSession) throws IOException {
        //Récupération des parametre du formulaire pour modifier les informations de BDD
        String adresseBdd = request.getParameter("adresseBdd");
        String utilisateurBDD = request.getParameter("utilisateurBDD");
        String mdpBDD = request.getParameter("mdpBDD");

        this.bddProperties.changerProprietes(adresseBdd, utilisateurBDD, mdpBDD);

        this.notificationDAO.ajouterNotification(utilisateurSession.getIdUtilisateur(),
                "La configuration de la BDD à été mise à jour");

        // On déconnecte l'utilisateur et on le renvoie à la page d'accueil
        response.sendRedirect(request.getContextPath() + VUE_DECO);
    }



    /**
     * Méthode appelée lorsque que l'utilisateur choisit de remettre par défaut les
     * paramètres de stockage
     *
     * @param request
     * @param response
     * @param utilisateurSession
     * @throws IOException
     */
    private void defaultConfStockage(HttpServletRequest request, HttpServletResponse response,
                                     Utilisateur utilisateurSession) throws IOException {
        // On lit directement le fichier annuaireDefaut.properties et on renvoie les
        // paramètres dans une liste
        List<String> listePropriete = fichierStockageEnCours.lireFichierProprieteStockageParDefaut();

        String[] nouveauxAttributs = { listePropriete.get(0), listePropriete.get(1), listePropriete.get(2) };

        // On modifie les paramètres
        fichierStockageEnCours.modifierFichierStockage(nouveauxAttributs);

        // On réinitialise le fichierEnCours pour qu'il ait les bons paramètres
        this.fichierStockageEnCours = new FichierProprieteStockage();

        this.notificationDAO.ajouterNotification(utilisateurSession.getIdUtilisateur(),
                MSG_CONF_STOCKAGE);

        // On déconnecte l'utilisateur et on le renvoie à la page d'accueil
        response.sendRedirect(request.getContextPath() + VUE_DECO);

    }

    /**
     * Méthode appelée lorsque l'utilisateur choisi de modifier les paramètres de
     * stockage
     *
     * @param request Request
     * @param response Response
     * @param utilisateurSession Utilisateur courant
     * @throws IOException Si la moditication est impossible
     */
    private void modifierConfStockage(HttpServletRequest request, HttpServletResponse response,
                                      Utilisateur utilisateurSession) throws IOException {
        /*
         * On récupère les différents attributs en requete et on utilise la fonction qui
         * modifie les paramètres.
         */
        String[] nouveauxAttributs = { (String) request.getParameter("cheminEnregPosters"),
                (String) request.getParameter("cheminEnregSlides"),
                (String) request.getParameter("tailleTamponFichier") };

        fichierStockageEnCours.modifierFichierStockage(nouveauxAttributs);

        // On réinitialise le fichierEnCours pour qu'il ait les bons paramètres
        this.fichierStockageEnCours = new FichierProprieteStockage();

        this.notificationDAO.ajouterNotification(utilisateurSession.getIdUtilisateur(), MSG_CONF_STOCKAGE);

        // On actualise la page et on renvois un message de succès
        CallbackUtilitaire.setCallback(request,
                new Callback(CallbackType.SUCCESS, MSG_CONF_STOCKAGE));

        // On déconnecte l'utilisateur et on le renvoie à la page d'accueil
        response.sendRedirect(request.getContextPath() + VUE_DECO);
    }

    /**
     * Permet de tester la connexion au serveur LDAP en utilisant la fonction
     * contenue dans LDAP
     *
     * @return True si la connexion est valide, false sinon
     */
    private boolean testConnexionLDAP() {
        LDAP ldap = new LDAP();
        return ldap.testConnexion();
    }


    /**
     * Permet de tester la connexion au serveur BDD
     * @param request Objet httpServletRequest
     * @return true si la& connexion est valide, false sinon
     */
    private boolean verificationConnexionBDD(HttpServletRequest request) {
        boolean test = false;
        String adresseBdd = request.getParameter("adresseBdd");
        String utilisateurBDD = request.getParameter("utilisateurBDD");
        String mdpBDD = request.getParameter("mdpBDD");
        try (Connection connexion = (Connection) DriverManager.getConnection( adresseBdd, utilisateurBDD, mdpBDD )){
            test = connexion != null;
        } catch ( SQLException e) {
            logger.log(Level.WARN, "Erreur lors de la connexion.",e);
        }

        return test;

    }


    /**
     * Lit le contenu d'un fichier et retourne son contenu. Une contrainte avec
     * l'utilisation de Codemirror nous oblige à remplacer certains caractères
     *
     * @param chemin
     *            chemin vers ce fichier
     * @return le contenu du fichier
     */
    private String lireContenuFichier(String chemin) {
        File file = ouvrirFichier(chemin);
        StringBuilder contenu = new StringBuilder();
        try (FileReader in = new FileReader(file); BufferedReader br = new BufferedReader(in)) {
            String s;
            while ((s = br.readLine()) != null) {
                s = s.replace("\"", "'");
                contenu.append(s).append("¿");
            }
        } catch (IOException e) {
            logger.log(Level.FATAL, "Impossible de lire le fichier " + chemin, e);
        }

        return contenu.toString().length() == 0 ? null : contenu.toString();
    }

    /**
     * Ecrit et remplace dans un fichier ce qu'il contient
     *
     * @param chemin
     *            vers le fichier
     * @param texte
     *            que l'on veut mettre dans ce fichier
     */
    private void ecrireDansFichier(String chemin, String texte) {

        try (FileWriter writer = new FileWriter(ouvrirFichier(chemin))){
            writer.write(texte);
            writer.flush();
        } catch (IOException e) {
            logger.log(Level.FATAL, "Impossible de modifier le fichier " + chemin, e);
        }
    }

    /**
     * Ouvre un fichier/repertoire et renvoie un objet de type File
     *
     * @param chemin
     *            vers le fichier
     * @return le fichier de type File
     */
    private File ouvrirFichier(String chemin) {
        String path = this.getServletContext().getRealPath(chemin);
        return new File(path);
    }

    /**
     * Liste le contenu du repertoire dont le chemin est celui en paramètre
     *
     * @param chemin
     *            du repertoire à lister
     * @return un String[] avec tous les fichiers du repertoire
     */
    private String[] listerRepertoire(String chemin) {
        File repertoire = ouvrirFichier(chemin);
        return repertoire.list();
    }

    /**
     * Ecrit fichier désigné par "part" à l'emplacement "chemin"
     *
     * @param part
     *            le Part du fichier à crée
     * @param chemin
     *            de sauvegarde
     */
    private void ecrireFichier(Part part, String chemin){
        int read;
        byte[] bytes = new byte[TAILLE_TAMPON];
        File newFichier = new File(chemin);

        try(
                InputStream inStream = part.getInputStream();
                OutputStream out = new FileOutputStream(newFichier);
        ) {
            while ((read = inStream.read(bytes)) != -1) {
                out.write(bytes, 0, read);
            }
        } catch(IOException e) {
            logger.log(Level.WARN, "Erreur d'écriture de fichier", e);
        }
    }

    /**
     * Méthode vérifiant que le fichier rentré correspond bien à une image
     *
     * @param nomDuFichier
     *            fichier à vérifier
     * @return true si le fichier est une image, false sinon
     */
    private boolean verificationFichier(String nomDuFichier) {
        return ("png").equals(ServletUtilitaire.avoirExtension(nomDuFichier))
                || ("jpg").equals(ServletUtilitaire.avoirExtension(nomDuFichier))
                || ("gif").equals(ServletUtilitaire.avoirExtension(nomDuFichier))
                || ("ico").equals(ServletUtilitaire.avoirExtension(nomDuFichier));
    }

}