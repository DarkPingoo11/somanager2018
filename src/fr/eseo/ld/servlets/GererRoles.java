package fr.eseo.ld.servlets;

import fr.eseo.ld.beans.*;
import fr.eseo.ld.dao.*;
import fr.eseo.ld.utils.CallbackUtilitaire;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Servlet permettant la gestion des rôles par l'administrateur.
 *
 * @author Thomas MENARD
 */
@WebServlet("/GererRoles")
@MultipartConfig
public class GererRoles extends HttpServlet {
    private static final long serialVersionUID = 1L;

    private static final String ATT_UTILISATEURS = "listeNomPrenomID";
    private static final String ATT_ROLES = "listeRoles";
    private static final String ATT_ROLES_UTILISATEUR = "listeRolesUtilisateur";
    private static final String ATT_OPTIONS = "listeOptions";
    private static final String ATT_OPTIONS_UTILISATEUR = "listeOptionsUtilisateur";
    private static final String ATT_FORMULAIRE = "formulaire";
    private static final String ATT_UTILISATEUR_CHOISI = "utilisateurChoisi";
    private static final String ATT_UTILISATEUR_RECHERCHE = "utilisateurRecherche";
    private static final String ATT_CHOIX_VALIDATION = "choixValidation";
    private static final String ATT_RADIO_OPTION = "radioOption";
    private static final String ATT_RADIO_ROLE = "radioRole";
    private static final String ATT_OPTION_UTILISATEUR = "optionUtilisateur";
    private static final String ATT_ROLE_UTILISATEUR = "roleUtilisateur";
    private static final String ATT_NB_ECHEC = "nombreEchec";
    private static final String ATT_NB_INSERTION = "nombreInsertion";
    private static final String ATT_ERREURS_IMPORT = "erreursImportation";

    private static final String PROF_OPTION = "profOption";


    private static final String NOM_ROLE_ETUDIANT = "etudiant";
    private static final String NOM_ROLE_PROF = "prof";
    private static final String NOM_ROLE_PROF_OPTION = PROF_OPTION;
    private static final String NOM_ROLE_PROF_RESPONSABLE = PROF_OPTION;
    private static final String NOM_ROLE_ADMIN = "admin";
    private static final String NOM_ROLE_SERVICE_COM = "serviceCom";
    private static final String NOM_ROLE_ENTREPRISE_EXT = "entrepriseExt";

    private static final String CHAMP_ERREUR_RECHERCHE = "recherche";
    private static final String CHAMP_ERREUR_ROLE_OPTION = "erreurRole";

    private static final String CHAMP_ERREUR_EXTENSION = "extensionErreur";
    private static final String CHAMP_ERREUR = "erreurs";
    private static final String[] ROLE_NECESSITANT_OPTION = {NOM_ROLE_ETUDIANT, "prof", PROF_OPTION, "profResponsable",
            NOM_ROLE_ENTREPRISE_EXT};

    private static final String ERREUR_ROLE = "Impossible de trouver l'utilisateur et/ou le role et/ou l'option associée. Ligne : ";
    public static final String ROLE_DEJA_EXISTANT = "Rôle déja existant ou incohérent (ex : un étudiant ne peut pas devenir prof) ! Impossible de le rajouter. Ligne : ";

    private static final String VUE_FORM = "/WEB-INF/formulaires/gererRoles.jsp";

    private static Logger logger = Logger.getLogger(GererRoles.class.getName());

    private UtilisateurDAO utilisateurDAO;
    private EtudiantDAO etudiantDAO;
    private RoleDAO rolesDAO;
    private OptionESEODAO optionESEODAO;
    private NotificationDAO notificationDAO;
    private AnneeScolaireDAO anneeScolaireDAO;
    private PglEtudiantDAO pglEtudiantDAO;
    private List<String> erreursProjet;


    @Override
    public void init() throws ServletException {
        /* Récupération d'instances de DAO */
        DAOFactory daoFactory = (DAOFactory) this.getServletContext().getAttribute(ServletUtilitaire.CONF_DAO_FACTORY);
        this.utilisateurDAO = daoFactory.getUtilisateurDao();
        this.notificationDAO = daoFactory.getNotificationDao();
        this.rolesDAO = daoFactory.getRoleDAO();
        this.optionESEODAO = daoFactory.getOptionESEODAO();
        this.etudiantDAO = daoFactory.getEtudiantDao();
        this.anneeScolaireDAO = daoFactory.getAnneeScolaireDAO();
        this.pglEtudiantDAO = daoFactory.getPglEtudiantDAO();
        this.erreursProjet = new ArrayList<>();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        /* Chargement de la liste des utilisateurs */
        HttpServletRequest requeteChargee = chargerListeUtilisateurs(request);
        this.getServletContext().getRequestDispatcher(VUE_FORM).forward(requeteChargee, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        /* Récupération de l'utilisateur choisi */
        String idUtilisateurChoisi = request.getParameter(ATT_UTILISATEUR_CHOISI);
        Utilisateur utilisateurChoisi = new Utilisateur();

        if (idUtilisateurChoisi != null) {
            utilisateurChoisi.setIdUtilisateur(Long.valueOf(idUtilisateurChoisi));
            utilisateurChoisi = this.utilisateurDAO.trouver(utilisateurChoisi).get(0);
        }
        // si l'utilisateur utilise le formulaire de recherche
        if ("chercherUtilisateur".equalsIgnoreCase(request.getParameter(ATT_FORMULAIRE))) {
            try {
                chercherUtilisateur(request, response);
            } catch (Exception e) {
                logger.log(Level.WARN, "Échec de la recherche de l'utilisateur.", e);
            }
            // si l'administrateur choisit de suspendre le compte de l'utilisateurChoisi
        } else if ("suspendreCompte".equalsIgnoreCase(request.getParameter(ATT_FORMULAIRE))) {
            try {
                suspendreCompte(request, response, utilisateurChoisi);
            } catch (Exception e) {
                logger.log(Level.WARN, "Échec de la suspension du compte de l'utilisateur.", e);
            }
            // si l'administrateur choisit de supprmier une autorisation (role et option)
        } else if ("supprimerAutorisation".equalsIgnoreCase(request.getParameter(ATT_FORMULAIRE))) {
            try {
                supprimerAutorisation(request, response, utilisateurChoisi);
            } catch (Exception e) {
                logger.log(Level.WARN, "Échec de la supression de l'autorisation du compte de l'utilisateur.", e);
            }
            // si l'administrateur choisit d'ajouter une autorisation
        } else if ("ajouterAutorisation".equalsIgnoreCase(request.getParameter(ATT_FORMULAIRE))) {
            try {
                ajouterAutorisation(request, response, utilisateurChoisi);
            } catch (Exception e) {
                logger.log(Level.WARN, "Échec de l'ajout de l'autorisation du compte de l'utilisateur.", e);
            }
        } else if (request.getContentType().contains("multipart/form-data;")) {
            importerFichierRole(request, response);
        }
    }

    private void importerFichierRole(HttpServletRequest request, HttpServletResponse response) {
        Map<String, String> erreurs = new HashMap<>();
        try {

            /* On parcourt toutes les images rentrées en paramètre de la requete */
            for (Part part : request.getParts()) {
                String fileName = ServletUtilitaire.extractFileName(part);
                fileName = new File(fileName).getName();

                // Si le fichier qu'on veut importer n'est pas de spécifié ou n'est pas un
                // fichier *.csv, on génère une erreur
                if (!("").equals(fileName) && ServletUtilitaire.verificationFichier(fileName)) {
                    InputStream inStream = part.getInputStream();
                    creerRoleOption(ServletUtilitaire.convertirStreamEnString(inStream), request);
                    erreurs.put(CHAMP_ERREUR_EXTENSION, "");
                } else {
                    erreurs.put(CHAMP_ERREUR_EXTENSION, "L'extension ne correspond pas à un fichier *.csv");
                }
            }
            request.setAttribute(CHAMP_ERREUR, erreurs);
            this.getServletContext().getRequestDispatcher(VUE_FORM).forward(request, response);
        } catch (IOException | ServletException e) {
            logger.log(Level.FATAL, "Impossible de modifier l'image ", e);
        }
    }

    /**
     * Cherche un utilisateur et le met en requête. Est appelée lors de
     * l'utilisation du formulaire chercherUtilisateur.
     *
     * @param request  Request
     * @param response Response
     * @throws ServletException Exception
     * @throws IOException      Exception
     */
    private void chercherUtilisateur(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        boolean quitter = false;
        Callback callback;
        while (!quitter) {
            Map<String, String> erreurs = new HashMap<>();
            /*
             * Récupération de la valeur du champ de recherche sous la forme <NOM PRENOM -
             * identifiant>
             */
            String utilisateurRecherche = request.getParameter(ATT_UTILISATEUR_RECHERCHE);

            /* Vérification de la cohérence de l'utilisateur rentré et de son existence */
            Utilisateur utilisateurChoisi = ServletUtilitaire.rechercheUtilisateurAutoCompletion(utilisateurRecherche,
                    this.utilisateurDAO);

            // si l'utilisateur renvoyé est nul, on affiche l'erreur
            if (utilisateurChoisi == null) {
                erreurs.put(CHAMP_ERREUR_RECHERCHE, "Aucun utilisateur trouvé");
                callback = new Callback(CallbackType.ERROR,
                        "Aucun utilisateur trouvé");
                CallbackUtilitaire.setCallback(request, callback);
                HttpServletRequest nouvelleRequete = chargerListeUtilisateurs(request);
                nouvelleRequete.setAttribute(ServletUtilitaire.ATT_ERREURS, erreurs);
                this.getServletContext().getRequestDispatcher(VUE_FORM).forward(nouvelleRequete, response);
                quitter = true;
                continue;
            }

            request.setAttribute(ATT_UTILISATEUR_CHOISI, utilisateurChoisi);

            HttpServletRequest nouvelleRequete = chargerRoleOptionUtilisateur(request, utilisateurChoisi);
            this.getServletContext().getRequestDispatcher(VUE_FORM).forward(nouvelleRequete, response);
            quitter = true;
        }
    }

    /**
     * Suspend un compte utilisateur. Est appelée lors de l'utilisation du
     * formulaire suspendreCompte.
     *
     * @param request           Request
     * @param response          Response
     * @param utilisateurChoisi Utilisateur
     * @throws ServletException Exception
     * @throws IOException      Exception
     */
    private void suspendreCompte(HttpServletRequest request, HttpServletResponse response,
                                 Utilisateur utilisateurChoisi) throws ServletException, IOException {
        /* Récupération de la nouvelle valeur de Utilisateur.valide */
        String choix = request.getParameter(ATT_CHOIX_VALIDATION);
        utilisateurChoisi.setValide(choix);

        /* Modification dans la BDD */
        utilisateurDAO.modifier(utilisateurChoisi);
        HttpServletRequest nouvelleRequete = chargerRoleOptionUtilisateur(request, utilisateurChoisi);
        this.getServletContext().getRequestDispatcher(VUE_FORM).forward(nouvelleRequete, response);
    }

    /**
     * Supprime une autorisation d'un utilisateur. Est appelée lors de l'utilisation
     * du formulaire supprimerAutorisation.
     *
     * @param request           Request
     * @param response          Response
     * @param utilisateurChoisi Utilisateur
     * @throws ServletException Exception
     * @throws IOException      Exception
     */
    private void supprimerAutorisation(HttpServletRequest request, HttpServletResponse response,
                                       Utilisateur utilisateurChoisi) throws ServletException, IOException {
        /* Récupération du nom de l'option et du role à supprimer */
        String nomOption = request.getParameter(ATT_OPTION_UTILISATEUR);
        String nomRole = request.getParameter(ATT_ROLE_UTILISATEUR);

        /* Création des objets correspondants */
        OptionESEO optionSupprimee = new OptionESEO();
        optionSupprimee.setNomOption(nomOption);
        optionSupprimee = optionESEODAO.trouver(optionSupprimee).get(0);

        Role roleSupprimee = new Role();
        roleSupprimee.setNomRole(nomRole);
        roleSupprimee = rolesDAO.trouver(roleSupprimee).get(0);

        /* Suppression des objets dans la BDD */
        this.utilisateurDAO.supprimerRoleOption(utilisateurChoisi, roleSupprimee, optionSupprimee);
        HttpServletRequest nouvelleRequete = chargerRoleOptionUtilisateur(request, utilisateurChoisi);
        this.getServletContext().getRequestDispatcher(VUE_FORM).forward(nouvelleRequete, response);
    }

    /**
     * Ajoute une autorisation (role + option) à un utilisateur. Est appelée lors de
     * l'utilisation du formulaire ajouterAutorisation.
     *
     * @param request           Request
     * @param response          Response
     * @param utilisateurChoisi Utilisateur
     * @throws ServletException Exception
     * @throws IOException      Exception
     */
    private void ajouterAutorisation(HttpServletRequest request, HttpServletResponse response,
                                     Utilisateur utilisateurChoisi) throws ServletException, IOException {
        boolean quitter = false;
        while (!quitter) {
            String optionSelectionnee = request.getParameter(ATT_RADIO_OPTION);
            String roleSelectionne = request.getParameter(ATT_RADIO_ROLE);

            Callback callback;
            Map<String, String> erreurs = new HashMap<>();

            /* Vérification du role et de l'option rentrée */
            // si étudiant, prof, profOption... n'ont pas d'options associées, on renvoie
            // une erreur
            if ((this.contient(ROLE_NECESSITANT_OPTION, roleSelectionne)) && optionSelectionnee == null) {
                erreurs.put(CHAMP_ERREUR_ROLE_OPTION,
                        "Veuillez spécifier une option associée au rôle " + roleSelectionne);
                callback = new Callback(CallbackType.ERROR,
                        "Veuillez spécifier une option associée au rôle " + roleSelectionne);
                CallbackUtilitaire.setCallback(request, callback);
                HttpServletRequest nouvelleRequete = chargerRoleOptionUtilisateur(request, utilisateurChoisi);
                nouvelleRequete.setAttribute(ServletUtilitaire.ATT_ERREURS, erreurs);
                this.getServletContext().getRequestDispatcher(VUE_FORM).forward(nouvelleRequete, response);
                quitter = true;
                continue;
            }

            /* Vérification de l'option rentrée */
            OptionESEO optionESEO;
            // si l'option est pas spécifée ou que le role sélectionné n'a pas le droit à
            // une option
            if (optionSelectionnee == null || NOM_ROLE_ADMIN.equals(roleSelectionne) || NOM_ROLE_SERVICE_COM.equals(roleSelectionne)) {
                optionESEO = new OptionESEO();
                optionESEO.setNomOption("");
                optionESEO = optionESEODAO.trouver(optionESEO).get(0);

                // sinon l'option selectionnée est valide et on peut la chercher
            } else {
                optionESEO = new OptionESEO();
                optionESEO.setNomOption(optionSelectionnee);
                optionESEO = optionESEODAO.trouver(optionESEO).get(0);
            }

            Role role = new Role();
            role.setNomRole(roleSelectionne);
            role = rolesDAO.trouver(role).get(0);

            // si l'administrateur rajoute un role déjà existant ou qui ne peut être
            // dupliqué (ex : etudiant)
            if (verificationExistenceRole(role, optionESEO, utilisateurChoisi)) {
                erreurs.put(CHAMP_ERREUR_ROLE_OPTION, "Rôle déja existant ou incohérent ! Impossible de le rajouter.");
                callback = new Callback(CallbackType.ERROR,
                        "Rôle déja existant ou incohérent ! Impossible de le rajouter.");
                CallbackUtilitaire.setCallback(request, callback);
                HttpServletRequest nouvelleRequete = chargerRoleOptionUtilisateur(request, utilisateurChoisi);
                nouvelleRequete.setAttribute(ServletUtilitaire.ATT_ERREURS, erreurs);
                this.getServletContext().getRequestDispatcher(VUE_FORM).forward(nouvelleRequete, response);
                quitter = true;
                continue;
            }

            // sinon on peut ajouter l'association de role et d'option dans la BDD
            utilisateurDAO.associerRoleOption(utilisateurChoisi, role, optionESEO);

            // si le role choisi est un étudiant, on crée l'objet Etudiant dans la BDD
            if (NOM_ROLE_ETUDIANT.equals(role.getNomRole())) {
                String anneeEtudiant = request.getParameter("radioAnneeScolaire");

                AnneeScolaire anneeScolaire = new AnneeScolaire();
                anneeScolaire.setIdOption(optionESEO.getIdOption());
                Long annee = this.anneeScolaireDAO.trouver(anneeScolaire).get(0).getIdAnneeScolaire();
                int anneeEtu = this.anneeScolaireDAO.trouver(anneeScolaire).get(0).getAnneeDebut();

                //Création de l'étudiant en fonction du paramètre I2 ou I3
                creationEtudiantEnFonctionI2ouI3(request, utilisateurChoisi, anneeEtudiant, annee, anneeEtu);
            }

            /* Récupération de l'utilisateur en session et notification */
            Utilisateur utilisateurSession = (Utilisateur) request.getSession()
                    .getAttribute(ServletUtilitaire.ATT_SESSION_USER);
            notificationDAO.ajouterNotification(utilisateurSession.getIdUtilisateur(),
                    "Le rôle a bien été ajouté à " + utilisateurChoisi.getPrenom());
            callback = new Callback(CallbackType.SUCCESS,
                    "Le rôle a bien été ajouté à " + utilisateurChoisi.getPrenom());
            CallbackUtilitaire.setCallback(request, callback);
            HttpServletRequest nouvelleRequete = chargerRoleOptionUtilisateur(request, utilisateurChoisi);
            this.getServletContext().getRequestDispatcher(VUE_FORM).forward(nouvelleRequete, response);
            quitter = true;
        }
    }

    private void creationEtudiantEnFonctionI2ouI3(HttpServletRequest request, Utilisateur utilisateurChoisi, String anneeEtudiant, Long annee, int anneeEtu) {
        Callback callback;
        if (anneeEtudiant.equals("I2")) {
            fr.eseo.ld.pgl.beans.Etudiant etudiant = new fr.eseo.ld.pgl.beans.Etudiant();
            etudiant.setIdEtudiant(utilisateurChoisi.getIdUtilisateur());

            etudiant.setRefAnnee(annee);

            //On recherche dans la table Etudiant que l'utilisateur que l'on va ajouter n'est pas dans
            // l'autre table concernant les I3
            Etudiant etudiantDelete = new Etudiant();
            etudiantDelete.setIdEtudiant(utilisateurChoisi.getIdUtilisateur());
            //Si l'étudiant est déjà dans la table Etudiant on le supprime de celle-ci
            if (!this.etudiantDAO.trouver(etudiantDelete).isEmpty()) {
                this.etudiantDAO.supprimer(etudiantDelete);
            }

            this.pglEtudiantDAO.creer(etudiant);

            callback = new Callback(CallbackType.SUCCESS,
                    "Création du role de l'étudiant en " + anneeEtudiant + ".");
            CallbackUtilitaire.setCallback(request, callback);
        } else {
            String contratPro = request.getParameter("radioContratPro");

            //On recherche dans la table PglEtudiant que l'utilisateur que l'on va ajouter n'est pas dans
            // l'autre table concernant les I2
            fr.eseo.ld.pgl.beans.Etudiant etudiantDelete = new fr.eseo.ld.pgl.beans.Etudiant();
            etudiantDelete.setIdEtudiant(utilisateurChoisi.getIdUtilisateur());
            //Si l'étudiant est déjà dans la table PglEtudiant on le supprime de celle-ci
            if (!this.pglEtudiantDAO.trouver(etudiantDelete).isEmpty()) {
                this.pglEtudiantDAO.supprimer(etudiantDelete);
            }

            Etudiant etudiant = new Etudiant();
            etudiant.setIdEtudiant(utilisateurChoisi.getIdUtilisateur());
            etudiant.setAnnee(anneeEtu);
            etudiant.setContratPro(contratPro);
            etudiantDAO.creer(etudiant);
            callback = new Callback(CallbackType.SUCCESS,
                    "Création du role de l'étudiant en " + anneeEtudiant + ".");
            CallbackUtilitaire.setCallback(request, callback);
        }
    }

    /**
     * Evite la duplication des roles/options dans la BDD et permet de vérifier la
     * concordance des roles/options de l'utilisateur.
     *
     * @param role              le nouveau role voulu pour l'utilisateurChoisi.
     * @param optionESEO        la nouvelle option voulue pour l'utilisateurChoisi.
     * @param utilisateurChoisi l'utilisateur qui va bénéficier du nouveau role.
     * @return un boolean indiquant si l'utilisateur possède ou non ce role.
     */
    private boolean verificationExistenceRole(Role role, OptionESEO optionESEO, Utilisateur utilisateurChoisi) {
        /*
         * Récupération des roles et des options correspondant aux roles pour
         * l'utilisateurChoisi
         */
        List<Role> listeRolesUtilisateur = this.utilisateurDAO.trouverRole(utilisateurChoisi);
        List<OptionESEO> listeOptionsUtilisateur = this.utilisateurDAO.trouverOption(utilisateurChoisi);

        /* Vérification pour chaque RoleUtilisateur */
        for (int i = 0; i < listeRolesUtilisateur.size(); i++) {

            // si l'utilisateur possède déjà le role et l'option voulue
            if (listeOptionsUtilisateur.get(i).getIdOption().equals(optionESEO.getIdOption())
                    && listeRolesUtilisateur.get(i).getIdRole().equals(role.getIdRole())) {
                return true;
            }

            /*
             * si l'utilisateur est déjà un étudiant, on ne peut pas créer un nouveau
             * roleUtilisateur
             */

            String[] tab = {NOM_ROLE_ETUDIANT, NOM_ROLE_PROF, NOM_ROLE_PROF_OPTION, NOM_ROLE_PROF_RESPONSABLE,
                    NOM_ROLE_ADMIN, NOM_ROLE_SERVICE_COM, NOM_ROLE_ENTREPRISE_EXT};

            if (NOM_ROLE_ETUDIANT.equals(listeRolesUtilisateur.get(i).getNomRole())
                    && contient(tab, role.getNomRole())) {
                return true;
            }
            /*
             * si l'utilisateur est déjà un prof, on ne peut pas lui attribuer un role
             * différent de prof
             */

            String[] tab1 = {NOM_ROLE_ETUDIANT, NOM_ROLE_ADMIN, NOM_ROLE_SERVICE_COM, NOM_ROLE_ENTREPRISE_EXT};

            if ((NOM_ROLE_PROF.equals(listeRolesUtilisateur.get(i).getNomRole())
                    || NOM_ROLE_PROF_OPTION.equals(listeRolesUtilisateur.get(i).getNomRole())
                    || NOM_ROLE_PROF_RESPONSABLE.equals(listeRolesUtilisateur.get(i).getNomRole()))
                    && contient(tab1, role.getNomRole())) {
                return true;
            }

            /* si l'utilisateur est déja */

        }
        return false;
    }

    /**
     * Charge la liste de tous les rôles, la liste de toutes les options, la liste
     * des rôles associés à l'utilisateur et la liste des options associées à
     * l'utilisateur dans une requête.
     *
     * @param requete la requête à charger.
     * @return requete la requête chargée.
     */
    private HttpServletRequest chargerRoleOptionUtilisateur(HttpServletRequest requete, Utilisateur utilisateurChoisi) {
        requete.setAttribute(ATT_UTILISATEUR_CHOISI, utilisateurChoisi);
        requete.setAttribute(ATT_ROLES, rolesDAO.listerRolesApplicatifs());
        requete.setAttribute(ATT_OPTIONS, optionESEODAO.lister());
        requete.setAttribute(ATT_ROLES_UTILISATEUR, this.utilisateurDAO.trouverRole(utilisateurChoisi));
        requete.setAttribute(ATT_OPTIONS_UTILISATEUR, this.utilisateurDAO.trouverOption(utilisateurChoisi));
        return requete;
    }

    /**
     * Charge la liste des utilisateurs dans une requête.
     *
     * @param requete la requête à charger.
     * @return requete la requête chargée.
     */
    private HttpServletRequest chargerListeUtilisateurs(HttpServletRequest requete) {
        /* Récupération de la liste des utilisateurs dans la BDD */
        List<Utilisateur> utilisateurs = utilisateurDAO.lister();

        /* Formatage de la liste des utilisateurs en String */
        StringBuilder liste = new StringBuilder();
        boolean premier = false;
        for (Utilisateur utilisateur : utilisateurs) {
            if (!premier) {
                liste.append(utilisateur.getNom().toUpperCase().replace(" ", ""))
                        .append(" ").append(utilisateur.getPrenom().replace(" ", ""))
                        .append(" - ").append(utilisateur.getIdentifiant().replace(" ", ""));
                premier = true;
            } else {
                liste.append(",").append(utilisateur.getNom().toUpperCase().replace(" ", ""))
                        .append(" ").append(utilisateur.getPrenom().replace(" ", ""))
                        .append(" - ").append(utilisateur.getIdentifiant().replace(" ", ""));
            }
        }

        /* Mise en place de la liste en session */
        requete.getSession().setAttribute(ATT_UTILISATEURS, liste.toString());
        return requete;
    }

    /**
     * Teste si un tableau de Strings contient ou non une valeur spécifique.
     *
     * @param tab    le tableau de Strings.
     * @param valeur la valeur recherchée dans le tableau.
     * @return un booléen indiquant si le tableau contient ou non la valeur
     * recherchée.
     */
    private boolean contient(String[] tab, String valeur) {
        for (String test : tab) {
            if (test.equals(valeur)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Méthode générale permettant l'association, à partir d'une chaine de
     * caractère, d'un role avec une option
     *
     * @param texte   : la chaine de caractère décrivant les associations de roles et
     *                options
     * @param requete : la requete
     */
    private void creerRoleOption(String texte, HttpServletRequest requete) {
        int nombreInsertion = 0;
        int compteur = 0;
        for (String ligne : texte.split("\n")) {
            ligne = ligne.substring(1, ligne.length() - 2);

            String[] attributs = ligne.split(";");

            // Si la ligne contient 5 attributs, alors c'est un élève
            if ((attributs.length == 6) && (attributs[4].equals("I2"))) {

                nombreInsertion += importRoleEtudiantGPL(attributs, ligne, requete);

                // Sinon, si la ligne contient 6 attributs, alors c'est un professeur
            } else if ((attributs.length == 6) && (attributs[4].equals("I3"))) {

                nombreInsertion += importRoleEtudiant(attributs, ligne, requete);

                // Sinon, si la ligne contient 4 attributs, alors on crée un étudiant de I2 (PGL)
            } else if ((attributs.length == 7) && (attributs[4].equals("Enseignant"))) {

                nombreInsertion += importRoleProf(attributs, ligne, requete);

                // Sinon, si la ligne contient 4 attributs, alors on crée un étudiant de I2 (PGL)
            } else {
                erreursProjet.add("Impossible lire la ligne, il manque des attributs : " + ligne);
            }

            compteur += 1;
        }
        requete.setAttribute(ATT_NB_ECHEC, compteur - nombreInsertion);
        requete.setAttribute(ATT_NB_INSERTION, nombreInsertion);
        requete.setAttribute(ATT_ERREURS_IMPORT, erreursProjet);
    }

    /**
     * Méthode traitant la création d'un role professeur si la ligne importée à 6
     * attributs
     *
     * @param attributs le tableau String des différentes informations utilisateur,
     *                  nécessaire à la création des roles
     * @param ligne     la ligne brut du fichier décrivant le role à importer
     * @return 1 si le role a été crée, 0 sinon
     */
    private int importRoleProf(String[] attributs, String ligne, HttpServletRequest request) {
        int resultat = 0;
        String identifiant = attributs[0];
        String mail = attributs[1];
        String option = attributs[5];
        String roleChoisi = attributs[6];

        Callback callback;
        // Recherche de l'utilisateur
        List<Utilisateur> utilisateurs = getUtilisateurs(identifiant, mail);

        // Recherche de l'option
        List<OptionESEO> optionsESEO = getOptionESEOS(option);

        // Création du role
        Role role = new Role();
        role.setNomRole(roleChoisi);
        List<Role> roles = rolesDAO.trouver(role);

        // Vérification de la recherche des différents beans sur la BDD
        if (roles.size() != 1 || utilisateurs.size() != 1 || optionsESEO.size() != 1) {
            erreursProjet.add(ERREUR_ROLE + ligne);
            callback = new Callback(CallbackType.ERROR,
                    ERREUR_ROLE + ligne);
            CallbackUtilitaire.setCallback(request, callback);
            // Vérification que les roles ne sont pas déjà existants ou qu'ils respectent la
            // cohérence du programme (ex: un étudiant ne peut pas devenir prof)
        } else if (verificationExistenceRole(roles.get(0), optionsESEO.get(0), utilisateurs.get(0))) {
            erreursProjet.add(
                    ROLE_DEJA_EXISTANT
                            + ligne);
            callback = new Callback(CallbackType.ERROR,
                    ROLE_DEJA_EXISTANT + ligne);
            CallbackUtilitaire.setCallback(request, callback);

            // On peut ajoute l'association de role et d'option dans la BDD
        } else {
            utilisateurDAO.associerRoleOption(utilisateurs.get(0), roles.get(0), optionsESEO.get(0));
            callback = new Callback(CallbackType.SUCCESS,
                    "Association du role et de l'option");
            CallbackUtilitaire.setCallback(request, callback);
            resultat = 1;
        }
        return resultat;
    }

    /**
     * Méthode traitant la création d'un role étudiant si la ligne importée à 5
     * attributs
     *
     * @param attributs le tableau String des différentes informations utilisateur
     * @param ligne     la ligne brut du fichier décrivant le role à importer
     * @return 1 si le role a été crée, 0 sinon
     */
    private int importRoleEtudiant(String[] attributs, String ligne, HttpServletRequest request) {
        int resultat = 0;
        String identifiant = attributs[0];
        String mail = attributs[1];
        String option = attributs[5];
        Callback callback;
        List<Utilisateur> utilisateurs = getUtilisateurs(identifiant, mail);


        // Recherche de l'option
        List<OptionESEO> optionsESEO = getOptionESEOS(option);

        // Création du role
        Role role = new Role();
        role.setNomRole(NOM_ROLE_ETUDIANT);
        List<Role> roles = rolesDAO.trouver(role);
        // Vérification de la recherche des différents beans sur la BDD
        if (roles.size() != 1 || utilisateurs.size() != 1 || optionsESEO.size() != 1) {
            this.erreursProjet
                    .add(ERREUR_ROLE + ligne);

            callback = new Callback(CallbackType.ERROR,
                    ERREUR_ROLE + ligne);
            CallbackUtilitaire.setCallback(request, callback);
            // Vérification que les roles ne sont pas déjà existants ou qu'ils respectent la
            // cohérence du programme
            // (ex: un étudiant ne peut pas devenir prof)
        } else if (verificationExistenceRole(roles.get(0), optionsESEO.get(0), utilisateurs.get(0))) {
            this.erreursProjet.add(
                    ROLE_DEJA_EXISTANT + ligne);
            callback = new Callback(CallbackType.ERROR,
                    ROLE_DEJA_EXISTANT + ligne);
            CallbackUtilitaire.setCallback(request, callback);
        } else {
            // On ajoute l'association de role et d'option dans la BDD
            utilisateurDAO.associerRoleOption(utilisateurs.get(0), roles.get(0), optionsESEO.get(0));

            // si le role choisi est un étudiant, on crée l'objet Etudiant dans la BDD
            if (NOM_ROLE_ETUDIANT.equals(role.getNomRole())) {

                AnneeScolaire anneeScolaire = new AnneeScolaire();
                anneeScolaire.setIdOption(optionsESEO.get(0).getIdOption());
                int anneeSco = this.anneeScolaireDAO.trouver(anneeScolaire).get(0).getAnneeDebut();
                Etudiant etudiant = new Etudiant();
                etudiant.setIdEtudiant(utilisateurs.get(0).getIdUtilisateur());
                etudiant.setAnnee(anneeSco);
                etudiant.setContratPro("non");
                etudiantDAO.creer(etudiant);
                callback = new Callback(CallbackType.SUCCESS,
                        "Création du rôle de l'étudiant.");
                CallbackUtilitaire.setCallback(request, callback);

            }
            resultat = 1;
        }
        return resultat;
    }

    private List<Utilisateur> getUtilisateurs(String identifiant, String mail) {
        // Recherche de l'utilisateur
        Utilisateur utilisateur = new Utilisateur();
        utilisateur.setEmail(mail);
        utilisateur.setIdentifiant(identifiant);
        return utilisateurDAO.trouver(utilisateur);
    }


    /**
     * Méthode traitant la création d'un role étudiant PGL si la ligne importée à 4
     * attributs
     *
     * @param attributs le tableau String des différentes informations utilisateur
     * @param ligne     la ligne brut du fichier décrivant le role à importer
     * @return 1 si le role a été crée, 0 sinon
     */
    private int importRoleEtudiantGPL(String[] attributs, String ligne, HttpServletRequest request) {
        int resultat = 0;
        String identifiant = attributs[0];
        String mail = attributs[1];
        String option = attributs[5];

        Callback callback;

        // Recherche de l'utilisateur
        List<Utilisateur> utilisateurs = getUtilisateurs(identifiant, mail);
        List<OptionESEO> optionsESEO = getOptionESEOS(option);


        // Création du role
        Role role = new Role();
        role.setNomRole(NOM_ROLE_ETUDIANT);
        role.setType("applicatif"); //à changer si dans pgl_equipe
        List<Role> roles = rolesDAO.trouver(role);

        // Vérification de la recherche des différents beans sur la BDD
        if (roles.size() != 1 || utilisateurs.size() != 1) {
            this.erreursProjet
                    .add(ERREUR_ROLE + ligne);
            callback = new Callback(CallbackType.ERROR,
                    ERREUR_ROLE + ligne);
            CallbackUtilitaire.setCallback(request, callback);
        } else {
            // On ajoute l'association de role et d'option dans la BDD
            utilisateurDAO.associerRoleOption(utilisateurs.get(0), roles.get(0), optionsESEO.get(0));
            //On fait la création de l'étudiant PGL
            fr.eseo.ld.pgl.beans.Etudiant etudiant = new fr.eseo.ld.pgl.beans.Etudiant();
            etudiant.setIdEtudiant(utilisateurs.get(0).getIdUtilisateur());

            List<fr.eseo.ld.pgl.beans.Etudiant> etudiants = this.pglEtudiantDAO.trouver(etudiant);
            // Si des I2 sont déja en base, on modifie l'objet Etudiant dans la BDD
            if (!etudiants.isEmpty()) {
                this.pglEtudiantDAO.modifier(etudiant);
                callback = new Callback(CallbackType.SUCCESS,
                        "Modification du rôle de l'étudiant.");
                CallbackUtilitaire.setCallback(request, callback);
            } else {
                this.pglEtudiantDAO.creer(etudiant);
                callback = new Callback(CallbackType.SUCCESS,
                        "Importation du rôle de l'étudiant.");
                CallbackUtilitaire.setCallback(request, callback);
            }
            resultat = 1;
        }
        return resultat;
    }

    private List<OptionESEO> getOptionESEOS(String option) {
        // Recherche de l'option et attribution de l'option = VIDE
        OptionESEO optionESEO = new OptionESEO();
        optionESEO.setNomOption(option);
        return optionESEODAO.trouver(optionESEO);
    }
}