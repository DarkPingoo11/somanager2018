package fr.eseo.ld.servlets;

import fr.eseo.ld.beans.*;
import fr.eseo.ld.dao.*;
import fr.eseo.ld.pgl.beans.Matiere;
import fr.eseo.ld.pgl.beans.Note;
import fr.eseo.ld.pgl.beans.Sprint;
import fr.eseo.ld.utils.CallbackUtilitaire;
import fr.eseo.ld.utils.ParamUtilitaire;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Servlet permettant la notatation des élèves par les professeurs.
 */
@WebServlet("/Noter")
public class Noter extends HttpServlet {

    public enum RoleNotation{SCRUMMASTER, PRODUCTOWNER, PROFREFERENT, AUCUN }

    private static final long serialVersionUID = 1L;

    private static final String TYPE_SUIVI = "suivi";
    private static final String TYPE_INTERMEDIAIRE = "intermediaire";
    private static final String TYPE_PROJET = "projet";
    private static final String TYPE_POSTER = "poster";
    private static final String TYPE_SOUTENANCE = "soutenance";
    private static final String TYPE_PGL = "noterPGL";
    private static final String TYPE_PUBLIERNOTES = "publierNotes";
    private static final String VALEUR_SOUTENANCE = "Soutenance";

    private static final String CHAMP_TYPE = "type";
    private static final String CHAMP_NOTE_INTERMEDIAIRE = "noteIntermediaire";
    private static final String CHAMP_NOTE_PROJET = "noteProjet";
    private static final String CHAMP_NOTE_POSTER = "notePoster";
    private static final String CHAMP_NOTE_SOUTENANCE = "noteSoutenance";

    private static final String ATT_UTILISATEUR_SUIVIS = "utilisateurSuivis";
    private static final String ATT_ETUDIANT_SUIVIS = "etudiantSuivis";
    private static final String ATT_SUJET_SUIVIS = "sujetSuivis";
    private static final String ATT_UTILISATEUR_POSTERS = "utilisateurPosters";
    private static final String ATT_ETUDIANT_POSTERS = "etudiantPosters";
    private static final String ATT_SUJET_POSTERS = "sujetPosters";
    private static final String ATT_NOTE_POSTERS = "notePosters";
    private static final String ATT_UTILISATEUR_SOUTENANCES = "utilisateurSoutenances";
    private static final String ATT_ETUDIANT_SOUTENANCES = "etudiantSoutenances";
    private static final String ATT_SUJET_SOUTENANCES = "sujetSoutenances";
    private static final String ATT_NOTE_SOUTENANCES = "noteSoutenances";

    private static final String ATT_PGL_EQUIPE = "pglEquipes";
    private static final String ATT_ANNEESCOLAIRE = "anneeScolaires";
    private static final String ATT_SPRINT ="sprints";
    private static final String ATT_MATIERE ="matieres";
    private static final String ATT_ETUDIANTS ="pglEtudiants";
    private static final String ATT_UTILISATEURS ="listeUtilisateurs";


    private static final String ATT_ONGLETACTIF = "ongletActif";

    private static final String PARAM_ANNEE = "annee";
    private static final String PARAM_PAGE = "page";
    private static final String PARAM_MATIERE = "matiere";
    private static final String PARAM_SPRINT = "sprint";
    private static final String PARAM_EQUIPE = "equipe";

    private static final String ATT_ANNEE_S = "anneeS";
    private static final String ATT_MATIERE_S = "matiereS";
    private static final String ATT_SPRINT_S = "sprintS";
    private static final String ATT_EQUIPE_S = "equipeS";

    private static final String VUE_FORM = "/WEB-INF/formulaires/gererNotation.jsp";
    private static final String PARAM_IDUTILISATEUR = "idUtilisateur";
    private static final String PARAM_IDMATIERE = "idMatiere";
    private static final String PARAM_NOTE = "note";
    private static final String PARAM_IDSPRINT = "idSprint";
    private static final String ATT_SPRINT_ANNEE = "sprintsAnnee";

    private Utilisateur utilisateur;
    private List<EtudiantEquipe> etudiantEquipe;
    private List<Equipe> equipes;
    private List<Utilisateur> utilisateurs;
    private List<Etudiant> etudiants;
    private List<NotePoster> notesPoster;
    private List<NoteSoutenance> notesSoutenance;
    private List<Poster> posters;
    private List<Soutenance> soutenances;
    List<fr.eseo.ld.pgl.beans.Equipe> pglEquipes;
    List<AnneeScolaire> anneeScolaires;
    List<fr.eseo.ld.pgl.beans.Sprint> sprints;
    List<fr.eseo.ld.pgl.beans.Matiere> matieres;
    List<Utilisateur> listeUtilisateurs;
    List<Note> listeNotes;
    List<fr.eseo.ld.pgl.beans.Etudiant>pglEtudiants;
    List<fr.eseo.ld.pgl.beans.EtudiantEquipe> listeEtudiantEquipes;


    private UtilisateurDAO utilisateurDAO;
    private SujetDAO sujetDAO;
    private EtudiantDAO etudiantDAO;
    private EquipeDAO equipeDAO;
    private PosterDAO posterDAO;
    private NotePosterDAO notePosterDAO;
    private SoutenanceDAO soutenanceDAO;
    private NoteSoutenanceDAO noteSoutenanceDAO;

    private PglEtudiantDAO pglEtudiantDAO;
    private PglEquipeDAO pglEquipeDAO;
    private SprintDAO sprintDAO;
    private AnneeScolaireDAO anneeScolaireDAO;
    private PglNoteDAO noteDAO;
    private MatiereDAO matiereDAO;
    private PglEtudiantEquipeDAO etudiantEquipeDAO;




    @Override
    public void init() throws ServletException {
        DAOFactory daoFactory = (DAOFactory) this.getServletContext().getAttribute(ServletUtilitaire.CONF_DAO_FACTORY);
        this.equipeDAO = daoFactory.getEquipeDAO();
        this.utilisateurDAO = daoFactory.getUtilisateurDao();
        this.sujetDAO = daoFactory.getSujetDao();
        this.etudiantDAO = daoFactory.getEtudiantDao();
        this.notePosterDAO = daoFactory.getNotePosterDao();
        this.noteSoutenanceDAO = daoFactory.getNoteSoutenanceDAO();
        this.posterDAO = daoFactory.getPosterDao();
        this.soutenanceDAO = daoFactory.getSoutenanceDAO();
        this.matiereDAO = daoFactory.getMatiereDAO();

        this.pglEtudiantDAO = daoFactory.getPglEtudiantDAO();
        this.pglEquipeDAO = daoFactory.getPglEquipeDAO();
        this.sprintDAO = daoFactory.getSprintDAO();
        this.anneeScolaireDAO = daoFactory.getAnneeScolaireDAO();
        this.noteDAO = daoFactory.getPglNoteDAO();
        this.etudiantEquipeDAO = daoFactory.getPglEtudiantEquipeDAO();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        recupereNotePFE(request);

        /* Initialisation des listes dont nous aurons besoin pour la gestion des PGL */
        this.listeUtilisateurs = this.utilisateurDAO.lister();
        this.listeNotes = this.noteDAO.lister();
        this.pglEquipes = this.pglEquipeDAO.lister();
        AnneeScolaire anneeScolaire = new AnneeScolaire();
        anneeScolaire.setIdOption(1L);
        this.anneeScolaires = this.anneeScolaireDAO.trouver(anneeScolaire);
        this.sprints = this.sprintDAO.lister();
        this.matieres = this.matiereDAO.lister();
        this.pglEtudiants = this.pglEtudiantDAO.lister();
        this.listeEtudiantEquipes = this.etudiantEquipeDAO.lister();

        request.setAttribute(ATT_PGL_EQUIPE, pglEquipes);
        request.setAttribute(ATT_ANNEESCOLAIRE, anneeScolaires);
        request.setAttribute(ATT_SPRINT, sprints);
        request.setAttribute(ATT_MATIERE, matieres);
        request.setAttribute(ATT_ETUDIANTS, pglEtudiants );
        request.setAttribute(ATT_UTILISATEURS, listeUtilisateurs);
        request.setAttribute("listeEtudiantEquipes",listeEtudiantEquipes);

        //Définir onglet actif par défaut
        if(request.getAttribute(ATT_ONGLETACTIF) == null) {
            request.setAttribute(ATT_ONGLETACTIF, "PFE");
        }

        //Afficher les sprints de l'année courante
        request.setAttribute(ATT_SPRINT_ANNEE, this.sprintDAO.recupererSprintAnnee(ParamUtilitaire.getAnneeCourante()));

        final RoleNotation roleNotation = getRoleNotation(this.utilisateur.getIdUtilisateur().intValue());
        //Définir les attributs en fonction des notes
        if(isProductOrScrum(roleNotation)) {
            this.matieres.removeIf(m -> !m.getLibelle().contains( VALEUR_SOUTENANCE));
        }


        //Afficher le tableau
        if(request.getParameter(PARAM_PAGE) != null && request.getParameter(PARAM_PAGE).equalsIgnoreCase("PGL") ) {
            //Définir l'onglet
            request.setAttribute(ATT_ONGLETACTIF, "PGL");

            //Récupérer les attributs
            String annee = request.getParameter(PARAM_ANNEE);
            String sprint = request.getParameter(PARAM_SPRINT);
            String equipe = request.getParameter(PARAM_EQUIPE);
            String matiere = request.getParameter(PARAM_MATIERE);

            if(ParamUtilitaire.notNull(annee, sprint)) {
                request.setAttribute(ATT_ANNEE_S, annee);
                request.setAttribute(ATT_SPRINT_S, sprint);
                request.setAttribute(ATT_EQUIPE_S, equipe);
                request.setAttribute(ATT_MATIERE_S, matiere);

                Integer sprintID = ParamUtilitaire.getInt(sprint);
                //Récupérer les notes en fonction des filtres
                //Lorsqu'une valeur selectionée n'est pas nulle
                Map<Utilisateur, Map<Matiere, Note>> resultats = this.noteDAO.recupererMatieresANoterUtilisateur(
                        new Utilisateur(),
                        this.utilisateur, ParamUtilitaire.getInt(annee), sprintID,
                        ParamUtilitaire.getInt(equipe), ParamUtilitaire.getInt(matiere));

                if(isProductOrScrum(roleNotation)) {
                    for(Map.Entry<Utilisateur, Map<Matiere, Note>> e : resultats.entrySet()) {
                        e.getValue().entrySet().removeIf(es -> !es.getKey().getLibelle().contains(VALEUR_SOUTENANCE));
                    }
                }

                Sprint sObj = DAOFactory.getInstance().getSprintDAO().trouver(Long.valueOf(sprintID));
                request.setAttribute("estPublie", sObj.getNotesPubliees());

                request.setAttribute("resultats", resultats);
            }
        }
        this.getServletContext().getRequestDispatcher(VUE_FORM).forward(request, response);
    }

    private boolean isProductOrScrum(RoleNotation roleNotation) {
        return roleNotation.equals(RoleNotation.SCRUMMASTER) || roleNotation.equals(RoleNotation.PRODUCTOWNER);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        /*
         * Rechargement des objets dont nous avons besoin pour mener à bien l'ajout ou
         * la modification des notes PFE
         */
        HttpSession session = request.getSession();
        this.utilisateur = (Utilisateur) session.getAttribute(ServletUtilitaire.ATT_SESSION_USER);
        this.etudiants = this.etudiantDAO.lister();
        this.etudiantEquipe = this.equipeDAO.listerEtudiantEquipe();
        this.notesPoster = this.notePosterDAO.lister();
        this.notesSoutenance = this.noteSoutenanceDAO.lister();
        this.posters = this.posterDAO.lister();
        this.soutenances = this.soutenanceDAO.lister();


        //Prise en compte de l'action à réaliser
        switch(request.getParameter(CHAMP_TYPE)) {
            case TYPE_SUIVI:
                doNoterTypeSuivi(request);
                request.setAttribute(ATT_ONGLETACTIF, "PFE");
                break;
            case TYPE_POSTER:
                doNoterType(request, CHAMP_NOTE_POSTER, TYPE_POSTER);
                request.setAttribute(ATT_ONGLETACTIF, "PFE");
                break;
            case TYPE_SOUTENANCE:
                doNoterType(request, CHAMP_NOTE_SOUTENANCE, TYPE_SOUTENANCE);
                request.setAttribute(ATT_ONGLETACTIF, "PFE");
                break;
            case TYPE_PGL:
                //JSON
                doNoterPGL(request, response);
                return;
            case TYPE_PUBLIERNOTES:
                //JSON
                doVerouillerNotes(request, response);
                return;

            default:
                break;
        }

        doGet(request, response);
    }

    private void doVerouillerNotes(HttpServletRequest request, HttpServletResponse response) {
        Callback callback = new Callback("Une erreur s'est produite");
        RoleNotation roleNotation = getRoleNotation(this.utilisateur.getIdUtilisateur().intValue());

        // Le prof référent d'option peut vérouiller les notes
        if(roleNotation.equals(RoleNotation.PROFREFERENT) || true) {
            //Récupérer le sprint à vérrouiller
            Integer idSprint = ParamUtilitaire.getInt(request.getParameter(PARAM_IDSPRINT));

            if(ParamUtilitaire.notNull(idSprint)) {
                final SprintDAO sprintDAO2 = DAOFactory.getInstance().getSprintDAO();

                Sprint sprint = sprintDAO2.trouver((long)idSprint);

                if(ParamUtilitaire.notNull(sprint)) {
                    sprint.setNotesPubliees(true);
                    sprintDAO2.modifier(sprint);

                    callback.setType(CallbackType.SUCCESS.getValue());
                    callback.setMessage("Les notes du sprint numéro " + sprint.getNumero() + " ont bien été publiées");
                } else {
                    callback.setMessage("Le sprint n'a pas été trouvé");
                }
            } else {
                callback.setMessage("Le sprint n'a pas été trouvé");
            }
        } else {
            callback.setMessage("Vous n'avez pas les droits necessaires");
        }

        CallbackUtilitaire.sendJsonResponse(response, callback);
    }

    private void doNoterPGL(HttpServletRequest request, HttpServletResponse response) {
        Integer idUtilisateur = ParamUtilitaire.getInt(request.getParameter(PARAM_IDUTILISATEUR));
        Integer idMatiere = ParamUtilitaire.getInt(request.getParameter(PARAM_IDMATIERE));
        Float note = ParamUtilitaire.getFloat(request.getParameter(PARAM_NOTE));
        Integer idEvaluateur = this.utilisateur.getIdUtilisateur().intValue();

        Callback callback = new Callback("Une erreur s'est produite");

        if(note >= 0 && note <= 20) {
            RoleNotation role = getRoleNotation(idEvaluateur);
            if(ParamUtilitaire.notNull(idUtilisateur, idMatiere, note, idEvaluateur) && peutNoterMatiere(idMatiere, role)) {
                Note noteObj = new Note();
                noteObj.setNote(note);
                noteObj.setRefEtudiant((long)idUtilisateur);
                noteObj.setRefMatiere((long)idMatiere);
                noteObj.setRefEvaluateur((long)idEvaluateur);

                //Si la note existe
                Note aTrouver = new Note();
                aTrouver.setRefEtudiant((long)idUtilisateur);
                aTrouver.setRefMatiere((long)idMatiere);
                aTrouver.setRefEvaluateur((long)idEvaluateur);

                final PglNoteDAO pglNoteDAO = DAOFactory.getInstance().getPglNoteDAO();

                //Si la note existe, on update
                final List<Note> trouver = pglNoteDAO.trouver(aTrouver);
                if(trouver.size() == 1) {
                    noteObj.setIdNote(trouver.get(0).getIdNote());
                    pglNoteDAO.modifier(noteObj);
                    callback.setMessage("La note à bien été modifiée à " + note + "/20");
                } else {
                    //On la crée sinon
                    pglNoteDAO.creer(noteObj);
                    callback.setMessage("La note à bien été définie à " + note + "/20");
                }
                callback.setType(CallbackType.SUCCESS.getValue());
            } else {
                callback.setMessage("Erreur ! Vous n'avez pas les droits requis");
            }
        } else {
            callback.setMessage("Erreur ! La note doit être comprise entre 0 et 20");
        }

        CallbackUtilitaire.sendJsonResponse(response, callback);
    }

    private void doNoterType(HttpServletRequest request, String champNotePoster, String typePoster) {
        for (Etudiant etudiant : this.etudiants) {
            String note = request.getParameter(champNotePoster + etudiant.getIdEtudiant());
            if (note != null && !"".equals(note)) {
                float notePoster = Float.parseFloat(note);
                this.ajouterNote(etudiant, typePoster, notePoster);
            }
        }
    }

    private void doNoterTypeSuivi(HttpServletRequest request) {
        for (Etudiant etudiant : this.etudiants) {
            String noteInter = request.getParameter(CHAMP_NOTE_INTERMEDIAIRE + etudiant.getIdEtudiant());
            String notePro = request.getParameter(CHAMP_NOTE_PROJET + etudiant.getIdEtudiant());
            if (noteInter != null && !"".equals(noteInter)) {
                float noteIntermediaire = Float.parseFloat(noteInter);
                this.ajouterNote(etudiant, TYPE_INTERMEDIAIRE, noteIntermediaire);
            }
            if (notePro != null && !"".equals(notePro)) {
                float noteProjet = Float.parseFloat(notePro);
                this.ajouterNote(etudiant, TYPE_PROJET, noteProjet);
            }
        }
    }

    /**
     * Ajoute les utilisateurs correspondant aux utilisateurs qui sont dans l'équipe
     * qui traite le sujet entré en paramètre.
     *
     * @param sujet
     *            le sujet concerné.
     * @return utilisateurRetour la liste des utilisateurs ajoutés.
     */
    private Utilisateur[] ajouterUtilisateurSujet(Sujet sujet) {
        /* Récupération de l'équipe qui traite le sujet */
        Equipe equipe = new Equipe();
        for (Equipe equipeBoucle : this.equipes) {
            if (equipeBoucle.getIdSujet().compareTo(sujet.getIdSujet()) == 0) {
                equipe = equipeBoucle;
                break;
            }
        }
        /*
         * Pour chaque etudiantEquipe qui contient l'ID de l'équipe, on ajoute
         * l'utilisateur correspondant à l'ID dans la liste si l'équipe n'est pas nulle
         */
        Utilisateur[] utilisateursRetour = null;
        if (equipe.getTaille() != null) {
            utilisateursRetour = new Utilisateur[equipe.getTaille()];
            int compteur = 0;
            for (EtudiantEquipe etudiantEquip : this.etudiantEquipe) {
                if (equipe.getIdEquipe().equals(etudiantEquip.getIdEquipe())) {
                    compteur += mapUtilisateur(utilisateursRetour, etudiantEquip, compteur);
                }
            }
        }

        return utilisateursRetour;
    }

    /**
     * Ajoute un utilisateur à une liste si son id correspond à l'id de
     * l'étudiantEquipe. on incrémente alors le compteur pour placer le futur
     * utilisateur (s'il en reste) à la place n+1
     *
     * @param utilisateurs
     *            la liste des utilisateurs d'une equipe
     * @param etudiantEquip
     *            le bean EtudiantEquipe qui contient l'id de l'utilisateur que l'on
     *            souhaite ajouter
     * @param compteur
     *            la place à laquelle on veut ajouter l'utilisateur
     *
     * @return 1 qui permet d'incrémenter le compteur
     */
    private Integer mapUtilisateur(Utilisateur[] utilisateurs, EtudiantEquipe etudiantEquip, int compteur) {
        for (Utilisateur utilisateurAjout : this.utilisateurs) {
            if (utilisateurAjout.getIdUtilisateur().compareTo(etudiantEquip.getIdEtudiant()) == 0) {
                utilisateurs[compteur] = utilisateurAjout;
            }
        }
        return 1;
    }

    /**
     * Ajoute les étudiants correspondant aux étudiants qui sont dans l'équipe qui
     * traite le sujet entré en paramètre.
     *
     * @param sujet
     *            le sujet concerné.
     * @return etudiantsRetour la liste des étudiants ajoutés.
     */
    private Etudiant[] ajouterEtudiantSujet(Sujet sujet) {
        /* Récupération de l'équipe qui traite le sujet */
        Equipe equipe = new Equipe();
        for (Equipe equip : this.equipes) {
            if (equip.getIdSujet().compareTo(sujet.getIdSujet()) == 0) {
                equipe = equip;
                break;
            }
        }

        /*
         * Pour chaque etudiantEquipe qui contient l'ID de l'équipe, on ajoute
         * l'étudiant correspondant à l'ID dans la liste
         */
        Etudiant[] etudiantsRetour = null;
        if (equipe.getTaille() != null) {
            etudiantsRetour = new Etudiant[equipe.getTaille()];
            int compteur = 0;
            for (EtudiantEquipe etudiantEquip : this.etudiantEquipe) {
                if (equipe.getIdEquipe().equals(etudiantEquip.getIdEquipe())) {
                    compteur += mapEtudiant(etudiantsRetour, etudiantEquip, compteur);
                }
            }
        }
        return etudiantsRetour;
    }

    /**
     * Ajoute un etudiant à une liste si son id correspond à l'id de
     * l'étudiantEquipe. on incrémente alors le compteur pour placer le futur
     * etudiant (s'il en reste) à la place n+1
     *
     * @param etudiants
     *            la liste des etudiants d'une equipe
     * @param etudiantEquip
     *            le bean EtudiantEquipe qui contient l'id de l'étudiant que l'on
     *            souhaite ajouter
     * @param compteur
     *            la place à laquelle on veut ajouter l'étudiant
     *
     * @return 1 qui permet d'incrémenter le compteur
     */
    private Integer mapEtudiant(Etudiant[] etudiants, EtudiantEquipe etudiantEquip, int compteur) {
        for (Etudiant etudiant : this.etudiants) {
            if (etudiant.getIdEtudiant().compareTo(etudiantEquip.getIdEtudiant()) == 0) {
                etudiants[compteur] = etudiant;
            }
        }
        return 1;
    }

    /**
     * Ajoute la note de poster correspondant au prof et à l'étudiant. Si le prof
     * n'a toujours pas saisit sa note, on retourne null.
     *
     * @param etudiants
     *            le tableau d'étudiants concernés
     * @return notes le tableau de notes pour chaque étudiant.
     */
    private NotePoster[] ajouterNotesPoster(Etudiant[] etudiants) {
        NotePoster[] notes = new NotePoster[etudiants.length];
        int compteur = 0;
        for (Etudiant etudiant : etudiants) {
            boolean existe = false;
            for (NotePoster note : this.notesPoster) {
                if (etudiant.getIdEtudiant().compareTo(note.getIdEtudiant()) == 0) {
                    notes[compteur] = note;
                    existe = true;
                    compteur++;
                }
            }
            if (!existe) {
                notes[compteur] = null;
                compteur++;
            }
        }
        return notes;
    }

    /**
     * Ajoute la note de soutenance correspondant au prof et à l'étudiant. Si le
     * prof n'a toujours pas saisit sa note, on retourne null.
     *
     * @param etudiants
     *            le tableau d'étudiants concernés
     * @return notes le tableau de notes pour chaque étudiant.
     */
    private NoteSoutenance[] ajouterNoteSoutenance(Etudiant[] etudiants) {
        NoteSoutenance[] notes = new NoteSoutenance[etudiants.length];
        int compteur = 0;
        for (Etudiant etudiant : etudiants) {
            boolean existe = false;
            for (NoteSoutenance note : this.notesSoutenance) {
                if (etudiant.getIdEtudiant().compareTo(note.getIdEtudiant()) == 0) {
                    notes[compteur] = note;
                    existe = true;
                    compteur++;
                }
            }
            if (!existe) {
                notes[compteur] = null;
                compteur++;
            }
        }
        return notes;
    }

    /**
     * Modifie la note de l'étudiant si elle diffère de celle qu'il a déjà. S'il n'a
     * pas encore de note alors on la créée
     *
     * @param etudiant
     *            l'étudiant dont la note sera modifiée.
     * @param type
     *            le type de note.
     * @param note
     *            la note qui remplace la note précédente.
     */
    private void ajouterNote(Etudiant etudiant, String type, float note) {
        if (TYPE_INTERMEDIAIRE.equals(type) && note >= 0 && note <= 20) {
            etudiant.setNoteIntermediaire(note);
            this.etudiantDAO.modifier(etudiant);
        } else if (TYPE_PROJET.equals(type) && note >= 0 && note <= 20) {
            etudiant.setNoteProjet(note);
            this.etudiantDAO.modifier(etudiant);
        } else if (TYPE_POSTER.equals(type) && note >= 0 && note <= 20) {
            boolean existe = modifierNotePoster(etudiant, note);
            if (!existe) {
                creerNotePoster(etudiant, note);
            }
        } else if (TYPE_SOUTENANCE.equals(type) && note >= 0 && note <= 20) {
            boolean existe = modifierNoteSoutenance(etudiant, note);
            if (!existe) {
                this.creerNoteSoutenance(etudiant, note);
            }
        }
    }

    /**
     * Méthode qui va regarder si l'étudiant à déja eu une note soutenance par le
     * professeur, si tel est le cas on modifie cette note sinon on retourne un
     * boolean disant qu'il n'y a pas encore de note de créée afin de la créer
     * ensuite
     *
     * @param etudiant
     *            l'étudiant pour le quel on veut modifier la note
     * @param note
     *            la note quel'on veut modifier
     *
     * @return existe un boolean qui nous dis si la note existait et a été modifiée
     *         ou non
     */
    private boolean modifierNotePoster(Etudiant etudiant, Float note) {
        boolean existe = false;
        for (NotePoster notePoster : this.notesPoster) {
            if (notePoster.getIdEtudiant().compareTo(etudiant.getIdEtudiant()) == 0) {
                notePoster.setNote(note);
                this.notePosterDAO.modifier(notePoster);
                existe = true;
                break;
            }
        }
        return existe;
    }

    /**
     * Méthode qui va regarder si l'étudiant à déja eu une note soutenance par le
     * professeur, si tel est le cas on modifie cette note sinon on retourne un
     * boolean disant qu'il n'y a pas encore de note de créée afin de la créer
     * ensuite
     *
     * @param etudiant
     *            l'étudiant pour le quel on veut modifier la note
     * @param note
     *            la note quel'on veut modifier
     *
     * @return existe un boolean qui nous dis si la note existait et a été modifiée
     *         ou non
     */
    private boolean modifierNoteSoutenance(Etudiant etudiant, Float note) {
        boolean existe = false;
        for (NoteSoutenance noteSoutenance : this.notesSoutenance) {
            if (noteSoutenance.getIdEtudiant().compareTo(etudiant.getIdEtudiant()) == 0) {
                noteSoutenance.setNote(note);
                this.noteSoutenanceDAO.modifier(noteSoutenance);
                existe = true;
                break;
            }
        }
        return existe;
    }

    /**
     * Ajoute une note de poster à un etudiant après que le prof l'a rentrée s'il ne
     * l'avait pas déjà rentrée.
     *
     * @param etudiant
     *            l'étudiant concerné.
     * @param note
     *            la note rentrée.
     */
    public void creerNotePoster(Etudiant etudiant, Float note) {
        NotePoster notePoster = new NotePoster();
        notePoster.setIdEtudiant(etudiant.getIdEtudiant());
        notePoster.setIdProfesseur(this.utilisateur.getIdUtilisateur());
        notePoster.setNote(note);
        String idEquipe = "";
        for (EtudiantEquipe etuEquipe : this.etudiantEquipe) {
            if (etuEquipe.getIdEtudiant().compareTo(etudiant.getIdEtudiant()) == 0) {
                idEquipe = etuEquipe.getIdEquipe();
                break;
            }
        }
        for (Poster poster : this.posters) {
            if (poster.getIdEquipe().equals(idEquipe)) {
                notePoster.setIdPoster(poster.getIdPoster());
                break;
            }
        }
        this.notePosterDAO.creer(notePoster);
    }

    /**
     * Ajoute une note de soutenance à un etudiant après que le prof l'a rentrée
     * s'il ne l'avait pas déjà rentrée.
     *
     * @param etudiant
     *            l'étudiant concerné.
     * @param note
     *            la note rentrée.
     */
    public void creerNoteSoutenance(Etudiant etudiant, Float note) {
        NoteSoutenance noteSoutenance = new NoteSoutenance();
        noteSoutenance.setIdProfesseur(this.utilisateur.getIdUtilisateur());
        noteSoutenance.setIdEtudiant(etudiant.getIdEtudiant());
        noteSoutenance.setNote(note);
        String idEquipe = "";
        for (EtudiantEquipe etuEquipe : this.etudiantEquipe) {
            if (etuEquipe.getIdEtudiant().compareTo(etudiant.getIdEtudiant()) == 0) {
                idEquipe = etuEquipe.getIdEquipe();
                break;
            }
        }
        for (Soutenance soutenance : this.soutenances) {
            if (soutenance.getIdEquipe().equals(idEquipe)) {
                noteSoutenance.setIdSoutenance(soutenance.getIdSoutenance());
                break;
            }
        }
        this.noteSoutenanceDAO.creer(noteSoutenance);
    }

    private void recupereNotePFE(HttpServletRequest request) {
        /* Récupération de l'Utilisateur en session */
        HttpSession session = request.getSession();
        this.utilisateur = (Utilisateur) session.getAttribute(ServletUtilitaire.ATT_SESSION_USER);

        /* Initialisation des listes dont nous aurons besoin */
        this.equipes = this.equipeDAO.lister();
        this.etudiantEquipe = this.equipeDAO.listerEtudiantEquipe();
        this.utilisateurs = this.utilisateurDAO.listerEtudiants();
        this.etudiants = this.etudiantDAO.lister();

        /*
         * Récupération de tous les sujets que le prof souhaite noter dans les
         * différents jurys ou en tant que référent ainsi que les notes qu'il a déjà
         * attribuées
         */
        Professeur professeur = new Professeur();
        professeur.setIdProfesseur(this.utilisateur.getIdUtilisateur());
        NotePoster notePoster = new NotePoster();
        notePoster.setIdProfesseur(professeur.getIdProfesseur());
        NoteSoutenance noteSoutenance = new NoteSoutenance();
        noteSoutenance.setIdProfesseur(professeur.getIdProfesseur());

        List<Sujet> sujetPosters = this.sujetDAO.trouverSujetsJuryPoster(this.utilisateur);
        List<Sujet> sujetSoutenances = this.sujetDAO.trouverSujetsJurySoutenance(this.utilisateur);
        List<Sujet> sujetSuivi = this.sujetDAO.trouverSujetsProfesseurReferent(professeur);
        this.notesPoster = this.notePosterDAO.trouver(notePoster);
        this.notesSoutenance = this.noteSoutenanceDAO.trouver(noteSoutenance);

        /* Création des listes envoyées à la JSP */
        List<Utilisateur[]> utilisateurSuivi = new ArrayList<>();
        List<Utilisateur[]> utilisateurPosters = new ArrayList<>();
        List<Utilisateur[]> utilisateurSoutenances = new ArrayList<>();
        List<NoteSoutenance[]> noteSoutenances = new ArrayList<>();
        List<NotePoster[]> notePosters = new ArrayList<>();
        List<Etudiant[]> etudiantSuivi = new ArrayList<>();
        List<Etudiant[]> etudiantPosters = new ArrayList<>();
        List<Etudiant[]> etudiantSoutenances = new ArrayList<>();

        /*
         * Création, pour chaque sujet, de la liste d'utilisateurs et d'étudiants
         * correspondant à l'équipe sur le sujet afin de pouvoir bien les afficher dans
         * la JSP pour que le prof puisse rentrer sa note
         */
        for (Sujet sujet : sujetPosters) {
            utilisateurPosters.add(this.ajouterUtilisateurSujet(sujet));
            etudiantPosters.add(this.ajouterEtudiantSujet(sujet));
            if (etudiantPosters.get(etudiantPosters.size() - 1) != null) {
                notePosters
                        .add(this.ajouterNotesPoster(etudiantPosters.get(etudiantPosters.size() - 1)));
            } else {
                notePosters.add(null);
            }

        }
        for (Sujet sujet : sujetSoutenances) {
            utilisateurSoutenances.add(this.ajouterUtilisateurSujet(sujet));
            etudiantSoutenances.add(this.ajouterEtudiantSujet(sujet));
            if (etudiantSoutenances.get(etudiantSoutenances.size() - 1) != null) {
                noteSoutenances.add(
                        this.ajouterNoteSoutenance(etudiantSoutenances.get(etudiantSoutenances.size() - 1)));
            } else {
                noteSoutenances.add(null);
            }

        }
        for (Sujet sujet : sujetSuivi) {
            utilisateurSuivi.add(this.ajouterUtilisateurSujet(sujet));
            etudiantSuivi.add(this.ajouterEtudiantSujet(sujet));
        }

        request.setAttribute(ATT_UTILISATEUR_SUIVIS, utilisateurSuivi);
        request.setAttribute(ATT_ETUDIANT_SUIVIS, etudiantSuivi);
        request.setAttribute(ATT_SUJET_SUIVIS, sujetSuivi);
        request.setAttribute(ATT_UTILISATEUR_POSTERS, utilisateurPosters);
        request.setAttribute(ATT_ETUDIANT_POSTERS, etudiantPosters);
        request.setAttribute(ATT_SUJET_POSTERS, sujetPosters);
        request.setAttribute(ATT_NOTE_POSTERS, notePosters);
        request.setAttribute(ATT_UTILISATEUR_SOUTENANCES, utilisateurSoutenances);
        request.setAttribute(ATT_ETUDIANT_SOUTENANCES, etudiantSoutenances);
        request.setAttribute(ATT_SUJET_SOUTENANCES, sujetSoutenances);
        request.setAttribute(ATT_NOTE_SOUTENANCES, noteSoutenances);
    }



    public Integer convert(String s) {
        Integer result;
        try{
            result = Integer.parseInt(s);
        } catch(Exception ignored) {
            result = null;
        }
        return result;
    }


    /**
     * Récupère le role de notation
     * @param idUtilisateur utilisateur
     * @return role
     */
    private RoleNotation getRoleNotation(Integer idUtilisateur) {
        return DAOFactory.getInstance().getUtilisateurDao().checkRoleNotationUtilisateur(idUtilisateur);
    }

    /**
     * Vérifie si l'utilisateur peut noter la matière en fonctio nde son droit
     * @param idMatiere matière
     * @param role droit
     * @return oui ou non
     */
    private boolean peutNoterMatiere(Integer idMatiere, RoleNotation role) {
        Matiere mat = DAOFactory.getInstance().getMatiereDAO().trouver((long) idMatiere);

        if(ParamUtilitaire.notNull(mat)) {
            Sprint sprint = DAOFactory.getInstance().getSprintDAO().trouver(mat.getRefSprint());
            if(ParamUtilitaire.notNull(sprint)) {
                //Si le sprint est verouillé
                if(sprint.getNotesPubliees()) {
                    return false;
                }
                //On verifie si l'utilisateur peut notes les matières
                if ((isProductOrScrum(role) && mat.getLibelle().contains(VALEUR_SOUTENANCE)) || role.equals(RoleNotation.PROFREFERENT)) {
                    return true;
                }
            }
        }
        return false;
    }
}