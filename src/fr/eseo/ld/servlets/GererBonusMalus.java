package fr.eseo.ld.servlets;

import fr.eseo.ld.beans.AnneeScolaire;
import fr.eseo.ld.beans.Callback;
import fr.eseo.ld.beans.CallbackType;
import fr.eseo.ld.beans.Utilisateur;
import fr.eseo.ld.dao.*;
import fr.eseo.ld.pgl.beans.*;
import fr.eseo.ld.utils.CallbackUtilitaire;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Servlet implementation class BonusMalus
 */
@WebServlet("/BonusMalus")
public class GererBonusMalus extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private static final String VUE_FORM = "/WEB-INF/formulaires/bonusMalus.jsp";
	private static final String ETAT_ATTENTE = "ATTENTE";
	private static final String ETAT_RADIO = "radio-";

	private UtilisateurDAO utilisateurDAO;
	private PglEquipeDAO pglEquipeDAO;
	private BonusMalusDAO bonusMalusDAO;
	private SprintDAO sprintDAO;
	private PglEtudiantDAO pglEtudiantDAO;
	private AnneeScolaireDAO anneeScolaireDAO;
	private PglNoteDAO noteDAO;
	private MatiereDAO matiereDAO;
	private PglEtudiantEquipeDAO etudiantEquipeDAO;
	private NotificationDAO notificationDAO;

	private List<Equipe> listePglEquipe;
	private List<BonusMalus> listeBonusMalus;
	private List<EtudiantEquipe> listePglEtudiantEquipe;
	private List<BonusMalus> listeTemp = new ArrayList<>();

	private Boolean elementPresentDansTableau = false;
	private Boolean toutValide;
	private Boolean estPresent = false;
	private Boolean nonEnvoyer = true;

	@Override
	public void init() throws ServletException {
		/* Recuperation d'instances de DAO */
		DAOFactory daoFactory = (DAOFactory) this.getServletContext().getAttribute(ServletUtilitaire.CONF_DAO_FACTORY);
		this.bonusMalusDAO = daoFactory.getBonusMalusDAO();
		this.utilisateurDAO = daoFactory.getUtilisateurDao();
		this.pglEquipeDAO = daoFactory.getPglEquipeDAO();
		this.sprintDAO = daoFactory.getSprintDAO();
		this.pglEtudiantDAO = daoFactory.getPglEtudiantDAO();
		this.anneeScolaireDAO = daoFactory.getAnneeScolaireDAO();
		this.noteDAO = daoFactory.getPglNoteDAO();
		this.matiereDAO = daoFactory.getMatiereDAO();
		this.etudiantEquipeDAO = daoFactory.getPglEtudiantEquipeDAO();
		this.notificationDAO = daoFactory.getNotificationDao();
	}

    /**
     * @see HttpServlet#HttpServlet()
     */
    public GererBonusMalus() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List<Utilisateur> listeUtilisateurs = this.utilisateurDAO.lister();
		this.listePglEquipe = this.pglEquipeDAO.lister();
		this.listeBonusMalus = this.bonusMalusDAO.lister();
		List<Sprint> listeSprint = this.sprintDAO.lister();
		List<Etudiant> listePglEtudiant = this.pglEtudiantDAO.lister();
		AnneeScolaire anneeScolaire = new AnneeScolaire();
		anneeScolaire.setIdOption(1L);
		List<AnneeScolaire> listeAnneeScolaire = this.anneeScolaireDAO.trouver(anneeScolaire);
		List<Note> listeNoteEquipe = this.noteDAO.lister();
		List<Matiere> listeMatiere = this.matiereDAO.lister();
		this.listePglEtudiantEquipe = this.etudiantEquipeDAO.lister();

		/* Récupération de la session depuis la requête */
		HttpSession session = request.getSession();

		/* Récupération de l'ID de l'Utilisateur en session */
		Utilisateur utilisateur = (Utilisateur) session.getAttribute(ServletUtilitaire.ATT_SESSION_USER);

		//Récupération de l'equipe et du sprint
		String anneeSelect = request.getParameter("anneeSelect");
		String equipeSelect = request.getParameter("equipeSelect");
		String sprintSelect = request.getParameter("sprintSelect");

		equipeAToutValider(equipeSelect, anneeSelect, sprintSelect);

		/* Envoi des attributs */
		request.setAttribute("utilisateur", utilisateur);
		request.setAttribute("toutValide", toutValide);
		request.setAttribute("nonEnvoyer", nonEnvoyer);
		request.setAttribute("elementPresentDansTableau", elementPresentDansTableau);
		request.setAttribute("listeUtilisateurs", listeUtilisateurs);
		request.setAttribute("listePglEquipe", listePglEquipe);
		request.setAttribute("listeBonusMalus", listeTemp);
		request.setAttribute("listeSprintDAO", listeSprint);
		request.setAttribute("listePglEtudiant", listePglEtudiant);
		request.setAttribute("listeAnneeScolaire", listeAnneeScolaire);
		request.setAttribute("listeNoteEquipe", listeNoteEquipe);
		request.setAttribute("listeMatiere", listeMatiere);
		request.setAttribute("listePglEtudiantEquipe", listePglEtudiantEquipe);

		if(anneeSelect != null && equipeSelect != null && sprintSelect != null) {
			if(!anneeSelect.equals("none"))request.setAttribute("iDanneeSelect", Long.parseLong(anneeSelect));
			if(!equipeSelect.equals("none"))request.setAttribute("iDequipeSelect", Long.parseLong(equipeSelect));
			if(!sprintSelect.equals("none"))request.setAttribute("iDsprintSelect", Long.parseLong(sprintSelect));
		}

		this.getServletContext().getRequestDispatcher(VUE_FORM).forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List<BonusMalus> listeNouveauBonusMalus = new ArrayList<>();

		int valeurZero = 0;

		for(int i = 0; i < this.listeTemp.size(); i++) {
			if(request.getParameter(ETAT_RADIO+i) != "none" && request.getParameter(ETAT_RADIO+i) != null) {
				valeurZero = valeurZero + Integer.parseInt(request.getParameter(ETAT_RADIO+i));
			}
		}

		if (valeurZero != 0) {
			CallbackUtilitaire.setCallback(request,
					new Callback(CallbackType.ERROR, "Attention la somme des bonus/malus doit faire 0 !"));
		} else {
			CallbackUtilitaire.setCallback(request,
					new Callback(CallbackType.SUCCESS, "Les bonus et malus on été prit en compte !"));

			for(int i = 0; i < this.listeTemp.size(); i++) {
				BonusMalus nouveauBonusMalus = new BonusMalus();

				nouveauBonusMalus.setIdBonusMalus(Long.parseLong(request.getParameter("id-"+i)));
				nouveauBonusMalus.setValeur((float) Long.parseLong(request.getParameter(ETAT_RADIO + i)));
				nouveauBonusMalus.setValidation(setLaBonneValidation(Long.parseLong(request.getParameter("id-"+i)), request.getParameter("validation-"+i), Long.parseLong(request.getParameter(ETAT_RADIO+i))));
				nouveauBonusMalus.setJustification((String)request.getParameter("justification-"+i));
				nouveauBonusMalus.setRefSprint(Integer.parseInt(request.getParameter("refSprint-"+i)));
				nouveauBonusMalus.setRefEtudiant(Integer.parseInt(request.getParameter("etudiant-"+i)));
				nouveauBonusMalus.setRefEvaluateur(Integer.parseInt(request.getParameter("refEvaluateur-"+i)));

				listeNouveauBonusMalus.add(nouveauBonusMalus);
				this.notificationDAO.ajouterNotification(Long.parseLong(request.getParameter("etudiant-"+i)), "Un bonus/malus est à valider !");
			}
			for(int i = 0; i < listeNouveauBonusMalus.size(); i++) {
				this.bonusMalusDAO.modifier(listeNouveauBonusMalus.get(i));
			}

		}

		request.setAttribute("valeurEstZero", valeurZero);
		doGet(request, response);
	}

	private Boolean equipeAToutValider(String equipeSelect, String anneeSelect, String sprintSelect) {

		this.listeTemp.clear();

		/* Récupere la liste des bonus/malus selectionné */
		if (anneeSelect != null && !anneeSelect.equals("none")
				&& equipeSelect != null && !equipeSelect.equals("none")
				&& sprintSelect != null && !sprintSelect.equals("none")) {
			method1(equipeSelect, sprintSelect, anneeSelect);
		}

		/* toutValide est vrai si tout le monde a validé */
		this.toutValide = false;
		for(int i = 0; i < listeTemp.size(); i++) {
			if(this.listeTemp.get(i).getValidation() != null) {
				nonEnvoyer = false;
				if(this.listeTemp.get(i).getValidation().equals(ETAT_ATTENTE) ||
						this.listeTemp.get(i).getValidation().equals("REFUSER")) {
					toutValide = false;
					break;
				} else {
					toutValide = true;
				}
			} else {
				nonEnvoyer = true;
				break;
			}
		}
		return toutValide;
	}

	private Boolean idBonusMalusEstPresent(Long idMalusBonus) {

		/* Permet d'eviter les doublons dans la liste */
		for(int i = 0; i < this.listeTemp.size(); i++) {
			if(this.listeTemp.get(i).getIdBonusMalus() == idMalusBonus) {
				estPresent = true;
				break;
			} else {
				estPresent = false;
			}
		}
		return estPresent;
	}

	private String setLaBonneValidation(Long idMalusBonusActuel, String validation, Long valeurRadio) {
		String validationFinal = ETAT_ATTENTE;

		if(validation != null && !validation.equals("")) {
			for(int i = 0; i < this.listeTemp.size(); i++) {
				if(this.listeTemp.get(i).getIdBonusMalus() == idMalusBonusActuel) {
					if(!String.valueOf(this.listeTemp.get(i).getValeur()).equals(String.valueOf(Float.valueOf(valeurRadio)))) {
						validationFinal = ETAT_ATTENTE;
					} else {
						validationFinal = validation;
					}
				}
			}
		}
		return validationFinal;
	}

	private void method1(String equipeSelect, String sprintSelect, String anneeSelect) {
		for (int i = 0; i < this.listeBonusMalus.size(); i++) {
			for (int j = 0; j < this.listePglEtudiantEquipe.size(); j++) {
				for(int v = 0; v< this.listePglEquipe.size(); v++) {
					method2(equipeSelect, sprintSelect, anneeSelect, i, j, v);
				}
			}
		}

	}

	private void method2(String equipeSelect, String sprintSelect, String anneeSelect, int i, int j, int v) {
		if (this.listeBonusMalus.get(i).getRefEtudiant() == this.listePglEtudiantEquipe.get(j).getIdEtudiant()
					&& this.listePglEtudiantEquipe.get(j).getIdEquipe() == Long.parseLong(equipeSelect)
					&& this.listePglEtudiantEquipe.get(j).getIdSprint() == Long.parseLong(sprintSelect)
					&& this.listePglEtudiantEquipe.get(i).getIdEquipe() == this.listePglEquipe.get(v).getIdEquipe()
					&& this.listePglEquipe.get(v).getRefAnnee() == Long.parseLong(anneeSelect)
					&& (!idBonusMalusEstPresent(this.listeBonusMalus.get(i).getIdBonusMalus()))) {

					this.listeTemp.add(this.listeBonusMalus.get(i));
					this.elementPresentDansTableau = true;

		}
	}
}
