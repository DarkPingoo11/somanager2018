package fr.eseo.ld.servlets.ajax;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import fr.eseo.ld.beans.Callback;
import fr.eseo.ld.beans.CallbackType;
import fr.eseo.ld.beans.Utilisateur;
import fr.eseo.ld.dao.DAOFactory;
import fr.eseo.ld.dao.UtilisateurDAO;
import fr.eseo.ld.servlets.ServletUtilitaire;
import fr.eseo.ld.utils.CallbackUtilitaire;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

/**
 * Servlet permettant d'afficher les informatiosn d'un utilisateur
 */
@WebServlet("/InfosUtilisateur")
@MultipartConfig
public class InfosUtilisateur extends HttpServlet {

    public static final String ATT_ID_UTILISATEUR       = "idUtilisateur";

    private UtilisateurDAO utilisateurDAO;

    @Override
    public void init() throws ServletException {
        /* Récupération d'instances de DAO */
        DAOFactory daoFactory = (DAOFactory) this.getServletContext().getAttribute(ServletUtilitaire.CONF_DAO_FACTORY);
        this.utilisateurDAO = daoFactory.getUtilisateurDao();
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        /* Récupération de l'ID de l'utilisateur en session */
        HttpSession session     = request.getSession();
        Utilisateur utilisateur = (Utilisateur) session.getAttribute(ServletUtilitaire.ATT_SESSION_USER);
        Utilisateur reponse     = null;

        //Récupération des parametres
        String paramIDStr   = request.getParameter(ATT_ID_UTILISATEUR);

        //Définition des paramètres au bon format
        Integer paramID = null;
        try {
            paramID = Integer.parseInt(paramIDStr);
        } catch (Exception e) {
            CallbackUtilitaire.sendJsonResponse(response, new Callback(CallbackType.ERROR,
                    "Les paramètres sont au mauvais format (doit être un nombre)"));
            return;
        }

        if(!isNull(paramID)) {
            //Si on recherche ses propres informations
            if(paramID == -1) {
                reponse = utilisateur;
            } else {
                //On recherche les informations de l'utilisateur voulu
                Utilisateur recherche = new Utilisateur();
                recherche.setIdUtilisateur(Long.valueOf(paramID));

                List<Utilisateur> resultats = this.utilisateurDAO.trouver(recherche);
                reponse = resultats.size() == 1 ? resultats.get(0) : null;
            }
        } else {
            CallbackUtilitaire.sendJsonResponse(response, new Callback(CallbackType.ERROR,
                    "L'identifiant renseigné est null"));
        }

        //Mise en forme de la réponse
        Gson gson = new GsonBuilder().create();
        JsonObject json = new JsonObject();

        if(reponse != null) {
            JsonObject jUtilisateur = gson.toJsonTree(reponse).getAsJsonObject();
            jUtilisateur.addProperty("hash", "confidentiel");
            json.addProperty("type", "success");
            json.add("utilisateur", jUtilisateur);
        } else {
            CallbackUtilitaire.sendJsonResponse(response, new Callback(CallbackType.ERROR,
                    "L'utilisateur n'existe pas"));
        }

        //Renvoi
        CallbackUtilitaire.sendJsonResponse(response, json);
    }

    /**
     * Renvoies si un objet est considéré comme null
     * Un string vide sera considéré comme null
     * @param o Objet a tester
     * @return true si l'objet est null, false sinon
     */
    private boolean isNull(Object o) {
        if(o instanceof String) {
            return ((String) o).isEmpty() || ((String) o).length() == 0;
        } else {
            return o == null;
        }
    }

}
