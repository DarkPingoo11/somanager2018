package fr.eseo.ld.servlets.ajax;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import fr.eseo.ld.beans.FiltreUtilisateur;
import fr.eseo.ld.beans.ResultatFiltreUtilisateur;
import fr.eseo.ld.beans.Role;
import fr.eseo.ld.dao.DAOFactory;
import fr.eseo.ld.dao.UtilisateurDAO;
import fr.eseo.ld.servlets.ServletUtilitaire;
import fr.eseo.ld.utils.CallbackUtilitaire;
import fr.eseo.ld.utils.IconUtilitaire;
import fr.eseo.ld.utils.NomUtilitaire;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Servlet permettant l'affichage du lien vers le dépot Google Form
 */
@WebServlet("/ListeUtilisateurs")
@MultipartConfig
public class ListeUtilisateurs extends HttpServlet {

    public static final String ATT_NOM_UTILISATEUR      = "nomUtilisateur";
    public static final String ATT_ANNEE_UTILISATEUR    = "anneeUtilisateur";
    public static final String ATT_ROLE_UTILISATEUR     = "roleUtilisateur";
    public static final String ATT_NOMBRE_RESULTATS     = "nombre";
    public static final String ATT_PAGE                 = "pageCourante";
    public static final String ATT_COMPTES_VALIDES      = "compteValide";

    private UtilisateurDAO utilisateurDAO;

    @Override
    public void init() throws ServletException {
        /* Récupération d'instances de DAO */
        DAOFactory daoFactory = (DAOFactory) this.getServletContext().getAttribute(ServletUtilitaire.CONF_DAO_FACTORY);
        this.utilisateurDAO = daoFactory.getUtilisateurDao();
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        //Récupération des filtres
        String paramAnnee   = request.getParameter(ATT_ANNEE_UTILISATEUR);
        String paramNombre  = request.getParameter(ATT_NOMBRE_RESULTATS);
        String paramPage    = request.getParameter(ATT_PAGE);
        String paramRole    = request.getParameter(ATT_ROLE_UTILISATEUR);
        String paramValide  = request.getParameter(ATT_COMPTES_VALIDES);

        String filtreNom        = request.getParameter(ATT_NOM_UTILISATEUR);
        Integer filtreRole      = (isNull(paramRole) || paramRole.equals("-1")) ? null : Integer.parseInt(paramRole);
        Integer filtreAnnee     = (isNull(paramAnnee) || paramAnnee.equals("-1")) ? null : Integer.parseInt(paramAnnee);
        Integer filtreNombre    = isNull(paramNombre) ? -1 : Integer.parseInt(paramNombre);
        Integer filtrePage      = isNull(paramPage) ? 1 : Integer.parseInt(paramPage);
        Boolean filtreValide    = (isNull(paramValide) || paramValide.equals("-1")) ? null : paramValide.equalsIgnoreCase("oui");

        //Récupération de la liste des utilisateurs correspondants au nom/id/prenom
        ResultatFiltreUtilisateur reponseFiltre = this.utilisateurDAO.chercherUtilisateur(
                filtreNom, filtreRole, filtreAnnee, filtreNombre, filtrePage, filtreValide);

        //Mise en forme de la réponse
        Gson gson = new GsonBuilder().create();
        JsonObject json = new JsonObject();
        JsonArray jUtilisateurs = new JsonArray();

        //Creation de l'objet Utilisateur+Role+Annee par utilisateur
        List<FiltreUtilisateur> utilisateurs = reponseFiltre.getFiltreUtilisateurs();
        for(FiltreUtilisateur u : utilisateurs) {
            JsonObject jUtilisateur = gson.toJsonTree(u).getAsJsonObject();
            JsonArray jRoles;

            //Mise en forme de l'icone pour les rôles
            List<Role> roles = u.getRoles();
            jRoles = getRolesJson(roles);
            jUtilisateur.addProperty("annee", u.getAnnee() == 0 ? "" : u.getAnnee().toString());
            jUtilisateur.add("roles", jRoles);
            jUtilisateurs.add(jUtilisateur);
        }

        json.add("utilisateurs", jUtilisateurs);
        json.addProperty("resultats", reponseFiltre.getResultats());
        json.addProperty("page", reponseFiltre.getPage());
        json.addProperty("type", "success");

        //Renvoi
        CallbackUtilitaire.sendJsonResponse(response, json);
    }

    /**
     * Renvoies un array Json des rôles
     * @param roles roles de l'utilisateur
     * @return Array JSON
     */
    private JsonArray getRolesJson(List<Role> roles) {
        JsonArray jRoles = new JsonArray();
        if(roles != null) {
            for(Role role : roles) {
                JsonObject jRole = new JsonObject();
                jRole.addProperty("icone", IconUtilitaire.getIconForRole(role));
                jRole.addProperty("nom", NomUtilitaire.getNomCompletRole(role).replaceAll("'", "\'"));
                jRoles.add(jRole);
            }
        }
        return jRoles;
    }

    /**
     * Renvoies si un objet est considéré comme null
     * Un string vide sera considéré comme null
     * @param o Objet a tester
     * @return true si l'objet est null, false sinon
     */
    private boolean isNull(Object o) {
        if(o instanceof String) {
            return ((String) o).isEmpty() || ((String) o).length() == 0;
        } else {
            return o == null;
        }
    }

}
