package fr.eseo.ld.servlets;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.mindrot.jbcrypt.BCrypt;

import fr.eseo.ld.beans.Utilisateur;
import fr.eseo.ld.dao.DAOFactory;
import fr.eseo.ld.dao.NotificationDAO;
import fr.eseo.ld.dao.UtilisateurDAO;
import fr.eseo.ld.ldap.AuthentificationAD;
import fr.eseo.ld.ldap.GestionUtilisateurLDAP;

/**
 * Servlet permettant le changement de mot de passe.
 */
@WebServlet("/ChangerMotDePasse")
public class ChangerMotDePasse extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private static final String VUE_FORM = "/WEB-INF/formulaires/changerMotDePasse.jsp";
	private static final String VUE_DASHBOARD = "/WEB-INF/formulaires/dashboard.jsp";

	private static final String ATT_VERIF_UTILISATEUR = "utilisateur";
	private static final String ATT_VERIF_MOT_DE_PASSE = "motDePasse";
	private static final String MESSAGE_COMPTE_INEXISTANT = "Aucun utilisateur n'est attribué à cet identifiant et/ou mot de passe et/ou email";

	private UtilisateurDAO utilisateurDAO;
	private NotificationDAO notificationDAO;

	private static Logger logger = Logger.getLogger(ChangerMotDePasse.class.getName());

	@Override
	public void init() throws ServletException {
		/* Récupération d'instances de DAO */
		DAOFactory daoFactory = (DAOFactory) this.getServletContext().getAttribute(ServletUtilitaire.CONF_DAO_FACTORY);
		this.utilisateurDAO = daoFactory.getUtilisateurDao();
		this.notificationDAO = daoFactory.getNotificationDao();
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		/* A la réception d'une requête GET, simple affichage du formulaire */
		this.getServletContext().getRequestDispatcher(VUE_FORM).forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		/* Récupération les données du formulaire */
		String identifiant = request.getParameter(ServletUtilitaire.CHAMP_IDENTIFIANT);
		String email = request.getParameter(ServletUtilitaire.CHAMP_EMAIL);
		String ancienMotDePasse = request.getParameter(ServletUtilitaire.CHAMP_ANCIEN_MOT_DE_PASSE);
		String nouveauMotDePasse = request.getParameter(ServletUtilitaire.CHAMP_MOT_DE_PASSE);
		String nouveauMotDePasse2 = request.getParameter(ServletUtilitaire.CHAMP_NOUVEAU_MOT_DE_PASSE);

		/* Recherche de l'existence de l'utilisateur dans notre base d'utilisateurs */
		Utilisateur utilisateur = new Utilisateur();
		utilisateur.setIdentifiant(identifiant);
		utilisateur.setEmail(email);
		List<Utilisateur> utilisateurs = this.utilisateurDAO.trouver(utilisateur);

		Map<String, String> erreurs = verificationPrealable(nouveauMotDePasse, nouveauMotDePasse2, identifiant,
				ancienMotDePasse, utilisateurs);

		String resultat;

		// si on n'a aucune erreur, alors on peut changer le mot passe
		if (erreurs.isEmpty()) {
			changerMotDePasse(utilisateurs.get(0), request);

			this.notificationDAO.ajouterNotification(utilisateurs.get(0).getIdUtilisateur(),
					"Votre mot de passe a été changé avec succès");

			/* Ré-affichage du formulaire avec message de succès */
			this.getServletContext().getRequestDispatcher(VUE_DASHBOARD).forward(request, response);

			/* sinon, on annule l'opération de changement de mot de passe */
		} else {
			resultat = "Échec du changement de mot de passe. " + erreurs.get(ATT_VERIF_MOT_DE_PASSE);
			logger.log(Level.WARN, resultat);
			request.setAttribute(ServletUtilitaire.ATT_RESULTAT, resultat);
			request.setAttribute(ServletUtilitaire.ATT_ERREURS, erreurs);
			/* Ré-affichage du formulaire avec les erreurs */
			this.getServletContext().getRequestDispatcher(VUE_FORM).forward(request, response);
		}

	}

	/**
	 * Vérifie si le mot de passe rentré est correct ou si l'utilisateur existe.
	 * 
	 * @param motDePasse1
	 *            le nouveau mot de passe.
	 * @param motDePasse2
	 *            la confirmation du nouveau mot de passe.
	 * @param identifiant
	 *            l'identifiant de l'utilisateur.
	 * @param ancienMotDePasse
	 *            l'ancien mot de passe de l'utilisateur.
	 * @param utilisateurs
	 *            la liste des utilisateurs concernés par le même identifiant et le
	 *            même email.
	 * @return erreurs la liste d'erreurs retournées.
	 */
	private Map<String, String> verificationPrealable(String motDePasse1, String motDePasse2, String identifiant,
			String ancienMotDePasse, List<Utilisateur> utilisateurs) {
		Map<String, String> erreurs = verificationExistanceUtilisateur(utilisateurs, identifiant, ancienMotDePasse);
		if (erreurs.isEmpty()) {
			erreurs = concordanceMotDePasse(motDePasse1, motDePasse2, identifiant);
			return erreurs;
		} else {
			return erreurs;
		}
	}

	/**
	 * Vérifie que le nouveau mot de passe a une sécurité assez forte (mdp > 6
	 * caractères / au moins un chiffre).
	 * 
	 * @param mdp1
	 *            le nouveau mot de passe.
	 * @param mdp2
	 *            la confirmation du nouveau mot de passe.
	 * @param identifiant
	 *            l'identifiant de l'utilisateur.
	 * @return erreurs la liste d'erreurs retournées.
	 */
	private Map<String, String> concordanceMotDePasse(String mdp1, String mdp2, String identifiant) {
		Map<String, String> erreurs = new HashMap<>();
		if (mdp1.equals(mdp2)) {
			if (mdp1.length() > 30 || mdp1.length() < 6) {
				erreurs.put(ATT_VERIF_MOT_DE_PASSE, "Le mot de passe doit au moins faire une longueur de 8 caractères");
			}
			if (mdp1.indexOf(identifiant) > -1 && erreurs.isEmpty()) {
				erreurs.put(ATT_VERIF_MOT_DE_PASSE, "Le mot de passe doit être différent de l'identifiant");
			}
			String numbers = "(.*[0-9].*)";
			if (!mdp1.matches(numbers) && erreurs.isEmpty()) {
				erreurs.put(ATT_VERIF_MOT_DE_PASSE, "Le mot de passe doit au moins contenir un chiffre");
			}
		} else {
			erreurs.put(ATT_VERIF_MOT_DE_PASSE, "Les deux mots de passe sont différents");
		}
		return erreurs;
	}

	/**
	 * Vérifie que l'utilisateur existe et que son (ancien) mot de passe est valide.
	 * 
	 * @param utilisateurs
	 *            la liste des utilisateurs concernés par le même identifiant et le
	 *            même email.
	 * @param identifiant
	 *            l'identifiant de l'utilisateur.
	 * @param motDePasse
	 *            le mot de passe de l'utilisateur.
	 * @return erreurs la liste d'erreurs retournées.
	 */
	private Map<String, String> verificationExistanceUtilisateur(List<Utilisateur> utilisateurs, String identifiant,
			String motDePasse) {
		Map<String, String> erreurs = new HashMap<>();
		if (utilisateurs.isEmpty()) { // si aucun utilisateur ne possède cet identifiant
			erreurs.put(ATT_VERIF_UTILISATEUR, MESSAGE_COMPTE_INEXISTANT);
		} else if ("activeDirectory".equals(utilisateurs.get(0).getHash())) { // si l'utilisateur appartient à l'AD
			AuthentificationAD autentification = new AuthentificationAD();
			String nomCommun = autentification.recupererNomCommun(identifiant);
			if (autentification.connexionAD(nomCommun, motDePasse) == null) {
				erreurs.put(ATT_VERIF_UTILISATEUR, MESSAGE_COMPTE_INEXISTANT);
			}
			if ("non".equals(utilisateurs.get(0).getValide())) {
				erreurs.put(ATT_VERIF_UTILISATEUR, "Ce compte n'est pas valide");
			}
		} else { // si l'utilisateur n'appartient pas à l'AD
			if (!BCrypt.checkpw(motDePasse, utilisateurs.get(0).getHash())) {
				erreurs.put(ATT_VERIF_UTILISATEUR, MESSAGE_COMPTE_INEXISTANT);
			}
			if ("non".equals(utilisateurs.get(0).getValide())) {
				erreurs.put(ATT_VERIF_UTILISATEUR, "Ce compte n'est pas valide");
			}
		}
		return erreurs;
	}

	/**
	 * Change le mot de passe d'un utilisateur, sur la base local ou sur le serveur
	 * LDAP en fonction du choix de l'utilisateur.
	 * 
	 * @param utilisateur
	 *            l'utilisateur concerné.
	 * @param request
	 *            la requête possédant tous les paramètres à récupérer.
	 */
	private void changerMotDePasse(Utilisateur utilisateur, HttpServletRequest request) {
		if ("activeDirectory".equals(utilisateur.getHash())) {
			GestionUtilisateurLDAP gestionUtilisateur = new GestionUtilisateurLDAP();
			AuthentificationAD authentification = new AuthentificationAD();
			gestionUtilisateur.modifierMotDePasse(authentification.recupererNomCommun(utilisateur.getIdentifiant()),
					request.getParameter(ServletUtilitaire.CHAMP_MOT_DE_PASSE));
		} else {
			Utilisateur utilisateurMAJ = ServletUtilitaire.mapInscription(request, utilisateur.getIdentifiant(),
					utilisateur.getEmail(), "oui");
			utilisateurMAJ.setIdUtilisateur(utilisateur.getIdUtilisateur());
			this.utilisateurDAO.modifier(utilisateurMAJ);
		}
	}

}