package fr.eseo.ld.dao;

import fr.eseo.ld.beans.OptionESEO;
import fr.eseo.ld.beans.Professeur;
import fr.eseo.ld.beans.Utilisateur;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static fr.eseo.ld.dao.DAOUtilitaire.fermetures;
import static fr.eseo.ld.dao.DAOUtilitaire.initialisationRequetePreparee;

/**
 * Classe du DAO du bean Professeur.
 * 
 * <p>
 * Cette classe implémente toutes les méthodes CRUD définies dans sa classe mère
 * sauf la méthode modifier().
 * </p>
 * <p>
 * En effet, un Professeur de contient qu'un seul attribut. Etant sa clé
 * primaire, il ne peut être modifié.
 * </p>
 * 
 * @author Julie AVIZOU
 */
public class ProfesseurDAO extends DAO<Professeur> {

	private static final String NOM_ENTITE = "Professeur";
	protected static final String ATTRIBUT_ID_PROFESSEUR = "idProfesseur";
	protected static final String ATTRIBUT_ID_UTILISATEUR = "idUtilisateur";
	private static final String[] ATTRIBUTS_NOMS = { ATTRIBUT_ID_PROFESSEUR };
	private static final String ATTRIBUT_NOM = "nom";
	private static final String ATTRIBUT_PRENOM = "prenom";

	// tous les attributs du bean doivent être écrits dans la requête INSERT
	private static final String SQL_INSERT = "INSERT INTO Professeur (idProfesseur) VALUES (?)";
	private static final String SQL_SELECT_TOUT = "SELECT * FROM Professeur";

	private static final String SQL_SELECT_PROFESSEUR_OPTION_DISPONIBLES = "SELECT u.idUtilisateur, u.prenom, u.nom FROM Professeur e, RoleUtilisateur ru, OptionESEO o, Utilisateur u "
			+ "WHERE"
			+ " e.idProfesseur = ru.idUtilisateur AND e.idProfesseur = u.idUtilisateur AND ru.idOption = o.idOption AND o.nomOption = ?";
	private static final String SQL_SELECT_PROFESSEUR_OPTION = "SELECT u.idUtilisateur, u.prenom, u.nom FROM Professeur e, RoleUtilisateur ru, OptionESEO o, Utilisateur u "
			+ "WHERE"
			+ " e.idProfesseur = ru.idUtilisateur AND e.idProfesseur = u.idUtilisateur AND ru.idOption = o.idOption AND o.nomOption = ?";
	private static final String SQL_SELECT_PROFESSEUR = "SELECT u.* FROM Professeur p, Utilisateur u "
			+ "WHERE p.idProfesseur = u.idUtilisateur";

	private static Logger logger = Logger.getLogger(ProfesseurDAO.class.getName());

	/**
	 * Constructeur de DAO.
	 * 
	 * @param daoFactory
	 *            la Factory permettant la création d'une connexion à la BDD.
	 */
	public ProfesseurDAO(DAOFactory daoFactory) {
		super(daoFactory);
	}

	/**
	 * Insère un Professeur dans la BDD à partir des attributs spécifiés dans un
	 * bean Professeur.
	 * 
	 * @param professeur
	 *            le Professeur que l'on souhaite insérer dans la BDD à partir du
	 *            bean Professeur.
	 */
	@Override
	public void creer(Professeur professeur) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			// les "?" de la requête sont comblés par les attributs de l'objet en paramètre
			// ceux-ci peuvent être nuls à condition que la BDD accepte les données de type
			// "null"
			preparedStatement = initialisationRequetePreparee(connection, SQL_INSERT, true,
					professeur.getIdProfesseur());
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec de la création de l'objet, aucune ligne ajoutée dans la table.", e);
		} finally {
			// fermeture des ressources utilisées
			fermetures(preparedStatement, connection);
		}
	}

	/**
	 * Liste tous les Professeurs ayant pour attributs les mêmes que ceux spécifiés
	 * dans un bean Professeur.
	 * 
	 * @param professeur
	 *            le Professeur que l'on souhaite trouver dans la BDD.
	 * @return professeurs la liste des Professeurs trouvés dans la BDD.
	 */
	@Override
	public List<Professeur> trouver(Professeur professeur) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<Professeur> professeurs = new ArrayList<>();
		// tableau de Strings regroupant tous les noms des attributs d'un objet ainsi
		// que les valeurs correspondantes
		String[][] attributs = { ATTRIBUTS_NOMS, { String.valueOf(professeur.getIdProfesseur()) } };
		try {
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			// mise en forme de la requête SELECT en fonction des attributs de l'objet
			// professeur
			preparedStatement = DAOUtilitaire.initialisationRequetePreparee(connection, "SELECT", NOM_ENTITE, attributs,
					false);
			resultSet = preparedStatement.executeQuery();
			// récupération des valeurs des attributs de la BDD pour les mettre dans une
			// liste
			while (resultSet.next()) {
				professeurs.add(this.mapProfesseur(resultSet));
			}
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec de la recherche de l'objet.", e);
		} finally {
			// fermeture des ressources utilisées
			fermetures(resultSet, preparedStatement, connection);
		}
		return professeurs;
	}

	/**
	 * Modifie UN Professeur ayant pour attributs les mêmes que ceux spécifiés dans
	 * un bean Professeur et la même clé primaire. Cette clé primaire ne peut être
	 * modifiée.
	 * 
	 * @param professeur
	 *            le Professeur que l'on souhaite modifier dans la BDD.
	 */
	@Override
	public void modifier(Professeur professeur) {
		/**
		 * Méthode modifier() nécessaire pour la bonne création de DAO mais n'a aucune
		 * action car le seul attribut d'un Professeur est sa clé primaire.
		 */
	}

	/**
	 * Supprime tous les Professeurs ayant pour attributs les mêmes que ceux
	 * spécifiés dans un bean Professeur.
	 * 
	 * @param professeur
	 *            le Professeur que l'on souhaite supprimer dans la BDD.
	 */
	@Override
	public void supprimer(Professeur professeur) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		// tableau de Strings regroupant tous les noms des attributs d'un objet ainsi
		// que les valeurs correspondantes
		String[][] attributs = { ATTRIBUTS_NOMS, { String.valueOf(professeur.getIdProfesseur()) } };
		try {
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			preparedStatement = DAOUtilitaire.initialisationRequetePreparee(connection, "DELETE", NOM_ENTITE, attributs,
					false);
			preparedStatement.executeUpdate();
			// suppression du bean
			professeur.setIdProfesseur(null);
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec de la suppression de l'objet, aucune ligne supprimée de la table.", e);
		} finally {
			// fermeture des ressources utilisées
			fermetures(preparedStatement, connection);
		}
	}

	/**
	 * Liste tous les Professeurs présents dans la BDD.
	 * 
	 * @return professeur la liste des Professeurs présents dans la BDD.
	 */
	@Override
	public List<Professeur> lister() {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<Professeur> professeurs = new ArrayList<>();
		try {
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			preparedStatement = connection.prepareStatement(SQL_SELECT_TOUT);
			resultSet = preparedStatement.executeQuery();
			// récupération des valeurs des attributs de la BDD pour les mettre dans une
			// liste
			while (resultSet.next()) {
				professeurs.add(this.mapProfesseur(resultSet));
			}
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec du listage des objets.", e);
		} finally {
			fermetures(resultSet, preparedStatement, connection);
		}
		return professeurs;
	}

	/**
	 * Liste tous les Professeurs disponibles, c'est-à-dire ceux qui n'appartiennent
	 * à aucun jury de soutenance.
	 * 
	 * @return utilisateursDisponibles la liste des Professeurs disponibles.
	 */
	public List<Utilisateur> listerProfesseursDisponibles() {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<Utilisateur> utilisateursDisponibles = new ArrayList<>();
		try {
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			preparedStatement = connection.prepareStatement(SQL_SELECT_PROFESSEUR);
			resultSet = preparedStatement.executeQuery();
			// récupération des valeurs des attributs de la BDD pour les mettre dans une
			// liste
			while (resultSet.next()) {
				utilisateursDisponibles.add(this.mapUtilisateur(resultSet));
			}
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec du listage des professeurs disponibles.", e);
		} finally {
			fermetures(resultSet, preparedStatement, connection);
		}
		return utilisateursDisponibles;
	}

	/**
	 * Liste tous les Professeurs disponibles, c'est-à-dire ceux qui n'appartiennent
	 * à aucun jury de soutenance, pour une option particulière.
	 * 
	 * @param option
	 *            l'option dont nous voulons récupérer les Professeurs disponibles.
	 * @return utilisateursDisponiblesOption la liste des Professeurs disponibles
	 *         pour l'option concernée.
	 */
	public List<Utilisateur> listerProfesseursDisponiblesOption(OptionESEO option) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<Utilisateur> utilisateursDisponiblesOption = new ArrayList<>();
		try {
			// vérification que l'option rentrée n'est pas nulle
			if (option.getNomOption() == null) {
				throw new SQLException();
			}
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			String requete = SQL_SELECT_PROFESSEUR_OPTION_DISPONIBLES;
			preparedStatement = connection.prepareStatement(requete);
			preparedStatement.setString(1, option.getNomOption());
			resultSet = preparedStatement.executeQuery();
			// récupération des valeurs des attributs de la BDD pour les mettre dans une
			// liste
			while (resultSet.next()) {
				utilisateursDisponiblesOption.add(this.mapUtilisateur(resultSet));
			}
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec du listage des professeurs disponibles pour une option.", e);
		} finally {
			fermetures(resultSet, preparedStatement, connection);
		}
		return utilisateursDisponiblesOption;
	}
	
	/**
	 * Liste tous les Professeurs disponibles pour une option particulière.
	 * 
	 * @param option
	 *            l'option dont nous voulons récupérer les Professeurs disponibles.
	 * @return utilisateursDisponiblesOption la liste des Professeurs disponibles
	 *         pour l'option concernée.
	 */
	public List<Utilisateur> listerProfesseursOption(OptionESEO option) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<Utilisateur> utilisateursOption = new ArrayList<>();
		try {
			// vérification que l'option rentrée n'est pas nulle
			if (option.getNomOption() == null) {
				throw new SQLException();
			}
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			String requete = SQL_SELECT_PROFESSEUR_OPTION;
			preparedStatement = connection.prepareStatement(requete);
			preparedStatement.setString(1, option.getNomOption());
			resultSet = preparedStatement.executeQuery();
			// récupération des valeurs des attributs de la BDD pour les mettre dans une
			// liste
			while (resultSet.next()) {
				utilisateursOption.add(this.mapUtilisateur(resultSet));
			}
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec du listage des professeurs pour une option.", e);
		} finally {
			fermetures(resultSet, preparedStatement, connection);
		}
		return utilisateursOption;
	}

	/**
	 * Fait la correspondance (le mapping) entre une ligne issue de la table
	 * Professeur (un ResultSet) et un bean Professeur.
	 * 
	 * @param resultSet
	 *            la ligne issue de la table Professeur.
	 * @return professeur le bean dont on souhaite faire la correspondance.
	 * @throws SQLException
	 */
	private Professeur mapProfesseur(ResultSet resultSet) throws SQLException {
		Professeur professeur = new Professeur();
		professeur.setIdProfesseur(resultSet.getLong(ATTRIBUT_ID_PROFESSEUR));
		return professeur;
	}

	/**
	 * Fait la correspondance (le mapping) entre une ligne issue de la table
	 * Utilisateur (un ResultSet) et un bean Utilisateur.
	 * 
	 * @param resultSet
	 *            la ligne issue de la table Utilisateur.
	 * @return utilisateur le bean dont on souhaite faire la correspondance.
	 * @throws SQLException
	 */
	private Utilisateur mapUtilisateur(ResultSet resultSet) throws SQLException {
		Utilisateur utilisateur = new Utilisateur();
		utilisateur.setIdUtilisateur(resultSet.getLong(ATTRIBUT_ID_UTILISATEUR));
		utilisateur.setNom(resultSet.getString(ATTRIBUT_NOM));
		utilisateur.setPrenom(resultSet.getString(ATTRIBUT_PRENOM));
		return utilisateur;
	}

}