package fr.eseo.ld.dao;

import fr.eseo.ld.pgl.beans.EtudiantEquipe;
import fr.eseo.ld.pgl.beans.Sprint;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static fr.eseo.ld.dao.DAOUtilitaire.fermetures;
import static fr.eseo.ld.dao.DAOUtilitaire.initialisationRequetePreparee;

/**
 * Classe du DAO du bean EtudiantEquipe (Pgl).
 *
 * <p>
 * Cette classe implémente toutes les méthodes CRUD définies dans sa classe
 * mère.
 * </p>
 */
public class PglEtudiantEquipeDAO extends DAO<EtudiantEquipe> {

    private static final String NOM_ENTITE = "pgl_etudiantequipe";
    private static final String ATTRIBUT_ID_ETUDIANT = "idEtudiant";
    private static final String ATTRIBUT_ID_EQUIPE = "idEquipe";
    private static final String ATTRIBUT_ID_SPRINT = "idSprint";
    private static final String[] ATTRIBUTS_NOMS = {ATTRIBUT_ID_ETUDIANT, ATTRIBUT_ID_EQUIPE, ATTRIBUT_ID_SPRINT};

    // tous les attributs du bean doivent être écrits dans la requête INSERT
    private static final String SQL_SELECT_TOUT = "SELECT * FROM pgl_etudiantequipe";

    private static Logger logger = Logger.getLogger(PglEtudiantEquipeDAO.class.getName());

    /**
     * Constructeur de DAO.
     *
     * @param daoFactory la Factory permettant la création d'une connexion à la BDD.
     */
    public PglEtudiantEquipeDAO(DAOFactory daoFactory) {
        super(daoFactory);
    }

    /**
     * Insère un PglEtudiantEquipe dans la BDD à partir des attributs spécifiés dans un bean
     * EtudiantEquipe. Insère l'association pour tous les psrints suivants
     *
     * @param etudiantEquipe le PglEtudiantEquipe que l'on souhaite insérer dans la BDD à partir du bean
     *                       Etudiant.
     */
    @Override
    public void creer(EtudiantEquipe etudiantEquipe) {
        HashMap<String, Object> attributs = new HashMap<>();
        attributs.put(ATTRIBUT_ID_ETUDIANT, etudiantEquipe.getIdEtudiant());
        attributs.put(ATTRIBUT_ID_EQUIPE, etudiantEquipe.getIdEquipe());
        attributs.put(ATTRIBUT_ID_SPRINT, etudiantEquipe.getIdSprint());
        DAOUtilitaire.executeCreer(getDaoFactory(), NOM_ENTITE, attributs);

        SprintDAO sprintDAO = this.getDaoFactory().getSprintDAO();

        //Récupération du sprint courant
        Sprint sprint = sprintDAO.trouver(etudiantEquipe.getIdSprint());

        //Pour chaque sprint a venir
        for (Sprint s : sprintDAO.recupererFutursSprint(sprint)) {
            attributs.put(ATTRIBUT_ID_SPRINT, s.getIdSprint());
            DAOUtilitaire.executeCreer(getDaoFactory(), NOM_ENTITE, attributs);
        }

    }

    /**
     * Liste tous les Etudiants ayant pour attributs les mêmes que ceux spécifiés
     * dans un bean Etudiant.
     *
     * @param etudiant l'EtudiantEquipe que l'on souhaite trouver dans la BDD.
     * @return etudiants la liste des Etudiants trouvés dans la BDD.
     */
    @Override
    public List<EtudiantEquipe> trouver(EtudiantEquipe etudiant) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        List<EtudiantEquipe> etudiantEquipes = new ArrayList<>();
        // tableau de Strings regroupant tous les noms des attributs d'un objet ainsi
        // que les valeurs correspondantes
        String[][] attributs = {ATTRIBUTS_NOMS,
                {String.valueOf(etudiant.getIdEtudiant()), String.valueOf(etudiant.getIdEquipe()),
                        String.valueOf(etudiant.getIdSprint())}};
        try {
            // création d'une connexion grâce à la DAOFactory placée en attribut de la
            // classe
            connection = this.creerConnexion();
            // mise en forme de la requête SELECT en fonction des attributs de l'objet
            // etudiant
            preparedStatement = initialisationRequetePreparee(connection, "SELECT", NOM_ENTITE, attributs, false);
            resultSet = preparedStatement.executeQuery();
            // récupération des valeurs des attributs de la BDD pour les mettre dans une
            // liste
            while (resultSet.next()) {
                etudiantEquipes.add(map(resultSet));
            }
        } catch (SQLException e) {
            logger.log(Level.WARN, "Échec de la recherche de l'objet.", e);
        } finally {
            // fermeture des ressources utilisées
            fermetures(resultSet, preparedStatement, connection);
        }
        return etudiantEquipes;
    }


    /**
     * Modifie UN EtudiantEquipe ayant pour attributs les mêmes que ceux spécifiés dans un
     * bean EtudiantEquipe et la même clé primaire. Cette clé primaire ne peut être
     * modifiée. Modifie pour les sprints a venir
     *
     * @param etudiantEquipe l'EtudiantEquipe que l'on souhaite modifier dans la BDD.
     */
    @Override
    public void modifier(EtudiantEquipe etudiantEquipe) {
        //Update table SET <....>
        Map<String, Object> attributs = new HashMap<>();
        attributs.put(ATTRIBUT_ID_EQUIPE, etudiantEquipe.getIdEquipe());

        //Preparation de WHERE
        Map<String, Object> whereAtt = new HashMap<>();
        whereAtt.put(ATTRIBUT_ID_ETUDIANT, etudiantEquipe.getIdEtudiant());
        whereAtt.put(ATTRIBUT_ID_SPRINT, etudiantEquipe.getIdSprint());

        SprintDAO sprintDAO = this.getDaoFactory().getSprintDAO();

        //Modifier
        DAOUtilitaire.executeUpdate(this.getDaoFactory(), NOM_ENTITE, attributs, whereAtt);

        //Récupération du sprint courant
        Sprint sprint = sprintDAO.trouver(etudiantEquipe.getIdSprint());

        //Pour chaque sprint a venir
        for (Sprint s : sprintDAO.recupererFutursSprint(sprint)) {
            whereAtt.put(ATTRIBUT_ID_SPRINT, s.getIdSprint());
            DAOUtilitaire.executeUpdate(getDaoFactory(), NOM_ENTITE, attributs, whereAtt);
        }


    }


    /**
     * Supprime tous les Etudiants ayant pour attributs les mêmes que ceux spécifiés
     * dans un bean Etudiant.
     *
     * @param etudiant l'EtudiantEquipe que l'on souhaite supprimer dans la BDD.
     */
    @Override
    public void supprimer(EtudiantEquipe etudiant) {
        Map<String, Object> identifiants = new HashMap<>();
        identifiants.put(ATTRIBUT_ID_ETUDIANT, etudiant.getIdEtudiant());

        DAOUtilitaire.executeDelete(this.getDaoFactory(), NOM_ENTITE, identifiants);
    }

    /**
     * Liste tous les Etudiants présents dans la BDD.
     *
     * @return etudiants la liste des Etudiants présents dans la BDD.
     */
    @Override
    public List<EtudiantEquipe> lister() {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        List<EtudiantEquipe> etudiantEquipes = new ArrayList<>();
        try {
            // création d'une connexion grâce à la DAOFactory placée en attribut de la
            // classe
            connection = this.creerConnexion();
            preparedStatement = connection.prepareStatement(SQL_SELECT_TOUT);
            resultSet = preparedStatement.executeQuery();
            // récupération des valeurs des attributs de la BDD pour les mettre dans une
            // liste
            while (resultSet.next()) {
                etudiantEquipes.add(map(resultSet));
            }
        } catch (SQLException e) {
            logger.log(Level.WARN, "Échec du listage des objets.", e);
        } finally {
            fermetures(resultSet, preparedStatement, connection);
        }
        return etudiantEquipes;
    }

    /**
     * Map le beans Etudiant
     *
     * @param resultSet resultset
     * @return EtudiantEquipe
     * @throws SQLException exception
     */
    public static EtudiantEquipe map(ResultSet resultSet) throws SQLException {
        EtudiantEquipe etudiantEquipe = new EtudiantEquipe();
        etudiantEquipe.setIdEtudiant(resultSet.getLong(ATTRIBUT_ID_ETUDIANT));
        etudiantEquipe.setIdEquipe(resultSet.getLong(ATTRIBUT_ID_EQUIPE));
        etudiantEquipe.setIdSprint(resultSet.getLong(ATTRIBUT_ID_SPRINT));
        return etudiantEquipe;
    }
}
