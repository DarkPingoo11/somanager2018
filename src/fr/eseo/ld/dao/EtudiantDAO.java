package fr.eseo.ld.dao;

import fr.eseo.ld.beans.Etudiant;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static fr.eseo.ld.dao.DAOUtilitaire.*;

/**
 * Classe du DAO du bean Etudiant.
 *
 * <p>
 * Cette classe implémente toutes les méthodes CRUD définies dans sa classe
 * mère.
 * </p>
 */
public class EtudiantDAO extends DAO<Etudiant> {

    private static final String NOM_ENTITE = "Etudiant";
    private static final String ATTRIBUT_ID_ETUDIANT = "idEtudiant";
    private static final String ATTRIBUT_ANNEE = "annee";
    private static final String ATTRIBUT_CONTRAT_PRO = "contratPro";
    private static final String ATTRIBUT_NOTE_INTERMEDIAIRE = "noteIntermediaire";
    private static final String ATTRIBUT_NOTE_PROJET = "noteProjet";
    private static final String ATTRIBUT_NOTE_SOUTENANCE = "noteSoutenance";
    private static final String ATTRIBUT_NOTE_POSTER = "notePoster";
    private static final String[] ATTRIBUTS_NOMS = {ATTRIBUT_ID_ETUDIANT, ATTRIBUT_ANNEE, ATTRIBUT_CONTRAT_PRO,
            ATTRIBUT_NOTE_INTERMEDIAIRE, ATTRIBUT_NOTE_PROJET, ATTRIBUT_NOTE_SOUTENANCE, ATTRIBUT_NOTE_POSTER};
    private static final String[] ATTRIBUTS_NOMS_ID_FIN = {ATTRIBUT_ANNEE, ATTRIBUT_CONTRAT_PRO,
            ATTRIBUT_NOTE_INTERMEDIAIRE, ATTRIBUT_NOTE_PROJET, ATTRIBUT_NOTE_SOUTENANCE, ATTRIBUT_NOTE_POSTER,
            ATTRIBUT_ID_ETUDIANT};

    // tous les attributs du bean doivent être écrits dans la requête INSERT
    private static final String SQL_INSERT = "INSERT INTO Etudiant (idEtudiant, annee, contratPro, noteIntermediaire, noteProjet, noteSoutenance, notePoster) VALUES (?, ?, ?, ?, ?, ?, ?)";
    private static final String SQL_SELECT_TOUT = "SELECT * FROM Etudiant";
    private static final String SQL_SELECT_NOTEPOSTER = "SELECT DISTINCT e.* " +
            "FROM Etudiant e " +
            "JOIN EtudiantEquipe ee ON e.idEtudiant = ee.idEtudiant " +
            "JOIN Equipe equipe ON ee.idEquipe = equipe.idEquipe " +
            "WHERE equipe.idSujet = ?";

    private static Logger logger = Logger.getLogger(EtudiantDAO.class.getName());

    /**
     * Constructeur de DAO.
     *
     * @param daoFactory la Factory permettant la création d'une connexion à la BDD.
     */
    public EtudiantDAO(DAOFactory daoFactory) {
        super(daoFactory);
    }

    /**
     * Insère un Etudiant dans la BDD à partir des attributs spécifiés dans un bean
     * Etudiant.
     *
     * @param etudiant l'Etudiant que l'on souhaite insérer dans la BDD à partir du bean
     *                 Etudiant.
     */
    @Override
    public void creer(Etudiant etudiant) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            // création d'une connexion grâce à la DAOFactory placée en attribut de la
            // classe
            connection = this.creerConnexion();
            // les "?" de la requête sont comblés par les attributs de l'objet en paramètre
            // ceux-ci peuvent être nuls à condition que la BDD accepte les données de type
            // "null"
            preparedStatement = initialisationRequetePreparee(connection, SQL_INSERT, true, etudiant.getIdEtudiant(),
                    etudiant.getAnnee(), etudiant.getContratPro(), etudiant.getNoteIntermediaire(),
                    etudiant.getNoteProjet(), etudiant.getNoteSoutenance(), etudiant.getNotePoster());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            logger.log(Level.WARN, "Échec de la création de l'objet, aucune ligne ajoutée dans la table.", e);
        } finally {
            // fermeture des ressources utilisées
            fermetures(preparedStatement, connection);
        }
    }

    /**
     * Liste tous les Etudiants ayant pour attributs les mêmes que ceux spécifiés
     * dans un bean Etudiant.
     *
     * @param etudiant l'Etudiant que l'on souhaite trouver dans la BDD.
     * @return etudiants la liste des Etudiants trouvés dans la BDD.
     */
    @Override
    public List<Etudiant> trouver(Etudiant etudiant) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        List<Etudiant> etudiants = new ArrayList<>();
        // tableau de Strings regroupant tous les noms des attributs d'un objet ainsi
        // que les valeurs correspondantes
        String[][] attributs = {ATTRIBUTS_NOMS,
                {String.valueOf(etudiant.getIdEtudiant()), String.valueOf(etudiant.getAnnee()),
                        String.valueOf(etudiant.getContratPro()), String.valueOf(etudiant.getNoteIntermediaire()),
                        String.valueOf(etudiant.getNoteProjet()), String.valueOf(etudiant.getNoteSoutenance()),
                        String.valueOf(etudiant.getNotePoster())}};
        try {
            // création d'une connexion grâce à la DAOFactory placée en attribut de la
            // classe
            connection = this.creerConnexion();
            // mise en forme de la requête SELECT en fonction des attributs de l'objet
            // etudiant
            preparedStatement = initialisationRequetePreparee(connection, "SELECT", NOM_ENTITE, attributs, false);
            resultSet = preparedStatement.executeQuery();
            // récupération des valeurs des attributs de la BDD pour les mettre dans une liste
            while (resultSet.next()) {
                etudiants.add(map(resultSet));
            }
        } catch (SQLException e) {
            logger.log(Level.WARN, "Échec de la recherche de l'objet.", e);
        } finally {
            // fermeture des ressources utilisées
            fermetures(resultSet, preparedStatement, connection);
        }
        return etudiants;
    }

    public List<Etudiant> trouverNotePoster(Long idSujet) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        List<Etudiant> etudiants = new ArrayList<>();

        try {
            // création d'une connexion grâce à la DAOFactory placée en attribut de la
            // classe
            connection = this.getDaoFactory().getConnection();
            // mise en forme de la requête
            preparedStatement = connection.prepareStatement(SQL_SELECT_NOTEPOSTER, Statement.NO_GENERATED_KEYS);
            preparedStatement.setLong(1, idSujet);
            resultSet = preparedStatement.executeQuery();
            // récupération des valeurs des attributs de la BDD pour les mettre dans une
            // liste
            while (resultSet.next()) {
                etudiants.add(map(resultSet));
            }
        } catch (SQLException e) {
            logger.log(Level.FATAL, "Échec de la recherche de l'objet.", e);
        } finally {
            // fermeture des ressources utilisées
            fermetures(resultSet, preparedStatement, connection);
        }

        return etudiants;
    }

    /**
     * Modifie UN Etudiant ayant pour attributs les mêmes que ceux spécifiés dans un
     * bean Etudiant et la même clé primaire. Cette clé primaire ne peut être
     * modifiée.
     *
     * @param etudiant l'Etudiant que l'on souhaite modifier dans la BDD.
     */
    @Override
    public void modifier(Etudiant etudiant) {
        // tableau de Strings regroupant tous les noms des attributs d'un objet ainsi
        // que les valeurs correspondantes
        // /!\ bien placer la clé primaire et sa valeur à la FIN du tableau sinon erreur
        String[][] attributs = {ATTRIBUTS_NOMS_ID_FIN,
                {String.valueOf(etudiant.getAnnee()), String.valueOf(etudiant.getContratPro()),
                        String.valueOf(etudiant.getNoteIntermediaire()), String.valueOf(etudiant.getNoteProjet()),
                        String.valueOf(etudiant.getNoteSoutenance()), String.valueOf(etudiant.getNotePoster()),
                        String.valueOf(etudiant.getIdEtudiant())}};

        /* Traite la mise à jour de la BDD */
        traitementUpdate(this.getDaoFactory(), NOM_ENTITE, attributs, logger);
    }

    /**
     * Supprime tous les Etudiants ayant pour attributs les mêmes que ceux spécifiés
     * dans un bean Etudiant.
     *
     * @param etudiant l'Etudiant que l'on souhaite supprimer dans la BDD.
     */
    @Override
    public void supprimer(Etudiant etudiant) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        // tableau de Strings regroupant tous les noms des attributs d'un objet ainsi
        // que les valeurs correspondantes
        String[][] attributs = {ATTRIBUTS_NOMS,
                {String.valueOf(etudiant.getIdEtudiant()), String.valueOf(etudiant.getAnnee()),
                        String.valueOf(etudiant.getContratPro()), String.valueOf(etudiant.getNoteIntermediaire()),
                        String.valueOf(etudiant.getNoteProjet()), String.valueOf(etudiant.getNoteSoutenance()),
                        String.valueOf(etudiant.getNotePoster())}};
        try {
            // création d'une connexion grâce à la DAOFactory placée en attribut de la
            // classe
            connection = this.creerConnexion();
            preparedStatement = initialisationRequetePreparee(connection, "DELETE", NOM_ENTITE, attributs, false);
            int statut = preparedStatement.executeUpdate();
            if (statut == 0) {
                throw new SQLException();
            } else {
                // suppression du bean
                etudiant.setIdEtudiant(null);
            }
        } catch (SQLException e) {
            logger.log(Level.WARN, "Échec de la suppression de l'objet, aucune ligne supprimée de la table.", e);
        } finally {
            // fermeture des ressources utilisées
            fermetures(preparedStatement, connection);
        }
    }

    /**
     * Liste tous les Etudiants présents dans la BDD.
     *
     * @return etudiants la liste des Etudiants présents dans la BDD.
     */
    @Override
    public List<Etudiant> lister() {
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        List<Etudiant> etudiants = new ArrayList<>();
        Connection connection = null;
        try {
            // création d'une connexion grâce à la DAOFactory placée en attribut de la
            // classe
            connection = this.creerConnexion();
            preparedStatement = connection.prepareStatement(SQL_SELECT_TOUT);
            resultSet = preparedStatement.executeQuery();
            // récupération des valeurs des attributs de la BDD pour les mettre dans une
            // liste
            while (resultSet.next()) {
                etudiants.add(map(resultSet));
            }
        } catch (SQLException e) {
            logger.log(Level.WARN, "Échec du listage des objets.", e);
        } finally {
            fermetures(resultSet, preparedStatement, connection);
        }
        return etudiants;
    }


    /**
     * Fait la correspondance (le mapping) entre une ligne issue de la table
     * Etudiant (un ResultSet) et un bean Etudiant.
     *
     * @param resultSet la ligne issue de la table Etudiant.
     * @return etudiant le bean dont on souhaite faire la correspondance.
     * @throws SQLException
     */
    public Etudiant map(ResultSet resultSet) throws SQLException {
        Etudiant etudiant = new Etudiant();
        etudiant.setIdEtudiant(resultSet.getLong(ATTRIBUT_ID_ETUDIANT));
        etudiant.setAnnee(resultSet.getInt(ATTRIBUT_ANNEE));
        etudiant.setContratPro(resultSet.getString(ATTRIBUT_CONTRAT_PRO));
        etudiant.setNoteIntermediaire(resultSet.getFloat(ATTRIBUT_NOTE_INTERMEDIAIRE));
        etudiant.setNoteProjet(resultSet.getFloat(ATTRIBUT_NOTE_PROJET));
        etudiant.setNoteSoutenance(resultSet.getFloat(ATTRIBUT_NOTE_SOUTENANCE));
        etudiant.setNotePoster(resultSet.getFloat(ATTRIBUT_NOTE_POSTER));
        return etudiant;
    }

}