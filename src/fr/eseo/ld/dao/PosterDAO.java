package fr.eseo.ld.dao;

import fr.eseo.ld.beans.Poster;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static fr.eseo.ld.dao.DAOUtilitaire.*;

/**
 * Classe du DAO du bean Poster.
 * 
 * <p>
 * Cette classe implémente toutes les méthodes CRUD définies dans sa classe
 * mère.
 * </p>
 * 
 * @author Alexandre CLAMENS
 */
public class PosterDAO extends DAO<Poster> {

	private static final String NOM_ENTITE = "Poster";
	private static final String ATTRIBUT_ID_POSTER = "idPoster";
	private static final String ATTRIBUT_CHEMIN = "chemin";
	private static final String ATTRIBUT_DATE = "datePoster";
	private static final String ATTRIBUT_ID_SUJET = "idSujet";
	private static final String ATTRIBUT_VALIDE = "valide";
	private static final String ATTRIBUT_ID_EQUIPE = "idEquipe";
	private static final String[] ATTRIBUTS_NOMS = { ATTRIBUT_ID_POSTER, ATTRIBUT_ID_SUJET, ATTRIBUT_CHEMIN,
			ATTRIBUT_DATE, ATTRIBUT_VALIDE, ATTRIBUT_ID_EQUIPE };
	private static final String[] ATTRIBUTS_NOMS_ID_FIN = { ATTRIBUT_ID_SUJET, ATTRIBUT_CHEMIN, ATTRIBUT_DATE,
			ATTRIBUT_VALIDE, ATTRIBUT_ID_EQUIPE, ATTRIBUT_ID_POSTER };

	// tous les attributs du bean doivent être écrits dans la requête INSERT
	private static final String SQL_INSERT = "INSERT INTO Poster (chemin, datePoster, idSujet, valide, idEquipe) VALUES (?, ?, ?, ?, ?)";
	private static final String SQL_SELECT_TOUT = "SELECT * FROM Poster";
	
	//Recupére les posters VALIDES
	private static final String SQL_SELECT_VALIDE = "SELECT * FROM Poster WHERE valide ='oui'";

	// requete permetant de retrouver le chemin d'un poster dont l'idSujt est donné
	private static final String SQL_SELECT_ANCIEN_POSTER = "SELECT * FROM Poster WHERE idSujet=?";
	private static final String SQL_DELETE_ANCIEN_POSTER = "DELETE FROM Poster WHERE idSujet=?";

	private static Logger logger = Logger.getLogger(PosterDAO.class.getName());

	/**
	 * Constructeur de DAO.
	 * 
	 * @param daoFactory
	 *            la Factory permettant la création d'une connexion à la BDD.
	 */
	public PosterDAO(DAOFactory daoFactory) {
		super(daoFactory);
	}

	/**
	 * Insère un chemin de Poster dans la BDD à partir des attributs spécifiés dans
	 * un bean Poster.
	 * 
	 * @param poster
	 *            le Poster pour lequel on souhaite insérer le chemin de sauvegarde
	 *            dans la BDD à partir du bean Poster.
	 */
	@Override
	public void creer(Poster poster) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			// les "?" de la requête sont comblés par les attributs de l'objet en paramètre
			// ceux-ci peuvent être nuls à condition que la BDD accepte les données de type
			// "null"
			preparedStatement = initialisationRequetePreparee(connection, SQL_INSERT, true, poster.getChemin(),
					poster.getDatePoster(), poster.getIdSujet(), poster.getValide(), poster.getIdEquipe());
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec de la création de l'objet, aucune ligne ajoutée dans la table.", e);
		} finally {
			// fermeture des ressources utilisées
			fermetures(preparedStatement, connection);
		}
	}

	/**
	 * Liste tous les Posters ayant pour attributs les mêmes que ceux spécifiés dans
	 * un bean Poster.
	 * 
	 * @param poster
	 *            le Poster que l'on souhaite trouver dans la BDD.
	 * @return posters la liste des Posters trouvés dans la BDD.
	 */
	@Override
	public List<Poster> trouver(Poster poster) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<Poster> posters = new ArrayList<>();
		// tableau de Strings regroupant tous les noms des attributs d'un objet ainsi
		// que les valeurs correspondantes
		String[][] attributs = { ATTRIBUTS_NOMS,
				{ String.valueOf(poster.getIdPoster()), String.valueOf(poster.getIdSujet()), poster.getChemin(),
						poster.getDatePoster(), poster.getValide(), poster.getIdEquipe() } };
		try {
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			// mise en forme de la requête SELECT en fonction des attributs de l'objet
			// poster
			preparedStatement = initialisationRequetePreparee(connection, "SELECT", NOM_ENTITE, attributs, false);
			resultSet = preparedStatement.executeQuery();
			// récupération des valeurs des attributs de la BDD pour les mettre dans une
			// liste
			while (resultSet.next()) {
				posters.add(map(resultSet));
			}
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec de la recherche de l'objet.", e);
		} finally {
			// fermeture des ressources utilisées
			fermetures(resultSet, preparedStatement, connection);
		}
		return posters;
	}

	/**
	 * Modifie UN Poster ayant pour attributs les mêmes que ceux spécifiés dans un
	 * bean Poster et la même clé primaire. Cette clé primaire ne peut être
	 * modifiée.
	 * 
	 * @param poster
	 *            le Poster que l'on souhaite modifier dans la BDD.
	 */
	@Override
	public void modifier(Poster poster) {
		// tableau de Strings regroupant tous les noms des attributs d'un objet ainsi
		// que les valeurs correspondantes
		// /!\ bien placer la clé primaire et sa valeur à la FIN du tableau sinon erreur
		String[][] attributs = { ATTRIBUTS_NOMS_ID_FIN, { String.valueOf(poster.getIdSujet()), poster.getChemin(),
				poster.getDatePoster(), poster.getValide(), poster.getIdEquipe(), String.valueOf(poster.getIdPoster()) } };

		/* Traite la mise à jour de la BDD */
		traitementUpdate(this.getDaoFactory(), NOM_ENTITE, attributs, logger);
	}

	/**
	 * Supprime tous les Posters ayant pour idSujet le même que celui spécifié dans
	 * un bean Poster.
	 * 
	 * @param poster
	 *            le Poster que l'on souhaite supprimer dans la BDD.
	 */
	@Override
	public void supprimer(Poster poster) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			preparedStatement = initialisationRequetePreparee(connection, SQL_DELETE_ANCIEN_POSTER, true,
					String.valueOf(poster.getIdSujet()));
			int statut = preparedStatement.executeUpdate();
			if (statut == 0) {
				throw new SQLException();
			} else {
				// suppression du bean
				poster.setIdPoster(null);
			}
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec de la suppression de l'objet, aucune ligne supprimée de la table.", e);
		} finally {
			// fermeture des ressources utilisées
			fermetures(preparedStatement, connection);
		}
	}

	/**
	 * Liste tous les Posters présents dans la BDD.
	 * 
	 * @return posters la liste des Posters présents dans la BDD.
	 */
	@Override
	public List<Poster> lister() {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<Poster> posters = new ArrayList<>();
		try {
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			preparedStatement = connection.prepareStatement(SQL_SELECT_TOUT);
			resultSet = preparedStatement.executeQuery();
			// récupération des valeurs des attributs de la BDD pour les mettre dans une
			// liste
			while (resultSet.next()) {
				posters.add(map(resultSet));
			}
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec du listage des objets.", e);
		} finally {
			fermetures(resultSet, preparedStatement, connection);
		}
		return posters;
	}
	/**
	 * Liste tous les Posters valides et présents dans la BDD.
	 * 
	 * @return posters la liste des Posters valides et présents dans la BDD.
	 */
	public List<Poster> listerPosterValide() {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<Poster> posters = new ArrayList<>();
		try {
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			preparedStatement = connection.prepareStatement(SQL_SELECT_VALIDE);
			resultSet = preparedStatement.executeQuery();
			// récupération des valeurs des attributs de la BDD pour les mettre dans une
			// liste
			while (resultSet.next()) {
				posters.add(map(resultSet));
			}
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec du listage des objets.", e);
		} finally {
			fermetures(resultSet, preparedStatement, connection);
		}
		return posters;
	}

	/**
	 * Trouve le poster ayant pour idSujet celui spécifié en paramètre.
	 * 
	 * @param idSujet
	 *            l'ID du sujet associé au poster.
	 * @return poster le poster ayant pour idSujet celui spécifié en paramètre.
	 */
	public Poster trouverPoster(Long idSujet) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		Poster poster = null;
		try {
			// vérification que l'idSujet rentré n'est pas nul
			if (idSujet == null) {
				throw new SQLException();
			}
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			// mise en forme de la requête SELECT en fonction des attributs de l'objet
			preparedStatement = initialisationRequetePreparee(connection, SQL_SELECT_ANCIEN_POSTER, true,
					String.valueOf(idSujet));
			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				poster = map(resultSet);
			}
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec de la recherche d'un poster à partir de l'ID d'un sujet.", e);
		} finally {
			// fermeture des ressources utilisées
			fermetures(resultSet, preparedStatement, connection);
		}
		return poster;
	}

	/**
	 * Fait la correspondance (le mapping) entre une ligne issue de la table Poster
	 * (un ResultSet) et un bean Poster.
	 * 
	 * @param resultSet
	 *            la ligne issue de la table Poster.
	 * @return equipe le bean dont on souhaite faire la correspondance.
	 * @throws SQLException
	 */
	public static Poster map(ResultSet resultSet) throws SQLException {
		Poster poster = new Poster();
		poster.setIdPoster(resultSet.getLong(ATTRIBUT_ID_POSTER));
		poster.setChemin(resultSet.getString(ATTRIBUT_CHEMIN));
		poster.setDatePoster(resultSet.getString(ATTRIBUT_DATE));
		poster.setIdSujet(resultSet.getLong(ATTRIBUT_ID_SUJET));
		poster.setValide(resultSet.getString(ATTRIBUT_VALIDE));
		poster.setIdEquipe(resultSet.getString(ATTRIBUT_ID_EQUIPE));
		return poster;
	}

}