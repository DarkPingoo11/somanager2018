package fr.eseo.ld.dao;

import fr.eseo.ld.pgl.beans.Etudiant;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static fr.eseo.ld.dao.DAOUtilitaire.fermetures;
import static fr.eseo.ld.dao.DAOUtilitaire.initialisationRequetePreparee;

/**
 * Classe du DAO du bean Etudiant (Pgl).
 *
 * <p>
 * Cette classe implémente toutes les méthodes CRUD définies dans sa classe
 * mère.
 * </p>
 */
public class PglEtudiantDAO extends DAO<Etudiant> {

    private static final String NOM_ENTITE = "pgl_etudiant";
    private static final String ATTRIBUT_ID_ETUDIANT = "idEtudiant";
    private static final String ATTRIBUT_REF_ANNEE = "refAnnee";
    private static final String[] ATTRIBUTS_NOMS = {ATTRIBUT_ID_ETUDIANT, ATTRIBUT_REF_ANNEE};

    // tous les attributs du bean doivent être écrits dans la requête INSERT
    private static final String SQL_INSERT = "INSERT INTO pgl_etudiant (idEtudiant,refAnnee) VALUES (?, ?)";
    private static final String SQL_SELECT_TOUT = "SELECT * FROM pgl_etudiant";

    private static Logger logger = Logger.getLogger(PglEtudiantDAO.class.getName());

    /**
     * Constructeur de DAO.
     *
     * @param daoFactory la Factory permettant la création d'une connexion à la BDD.
     */
    public PglEtudiantDAO(DAOFactory daoFactory) {
        super(daoFactory);
    }

    /**
     * Insère un PglEtudiant dans la BDD à partir des attributs spécifiés dans un bean
     * Etudiant.
     *
     * @param etudiant le PglEtudiant que l'on souhaite insérer dans la BDD à partir du bean
     *                 Etudiant.
     */
    @Override
    public void creer(Etudiant etudiant) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            // création d'une connexion grâce à la DAOFactory placée en attribut de la
            // classe
            connection = this.creerConnexion();
            // les "?" de la requête sont comblés par les attributs de l'objet en paramètre
            // ceux-ci peuvent être nuls à condition que la BDD accepte les données de type
            // "null"
            preparedStatement = initialisationRequetePreparee(connection, SQL_INSERT, true, etudiant.getIdEtudiant(),
                    etudiant.getRefAnnee());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            logger.log(Level.WARN, "Échec de la création de l'objet, aucune ligne ajoutée dans la table.", e);
        } finally {
            // fermeture des ressources utilisées
            fermetures(preparedStatement, connection);
        }
    }

    /**
     * Liste tous les Etudiants ayant pour attributs les mêmes que ceux spécifiés
     * dans un bean Etudiant.
     *
     * @param etudiant l'Etudiant que l'on souhaite trouver dans la BDD.
     * @return etudiants la liste des Etudiants trouvés dans la BDD.
     */
    @Override
    public List<Etudiant> trouver(Etudiant etudiant) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        List<Etudiant> etudiants = new ArrayList<>();
        // tableau de Strings regroupant tous les noms des attributs d'un objet ainsi
        // que les valeurs correspondantes
        String[][] attributs = {ATTRIBUTS_NOMS,
                {String.valueOf(etudiant.getIdEtudiant()), String.valueOf(etudiant.getRefAnnee())}};
        try {
            // création d'une connexion grâce à la DAOFactory placée en attribut de la
            // classe
            connection = this.creerConnexion();
            // mise en forme de la requête SELECT en fonction des attributs de l'objet
            // etudiant
            preparedStatement = initialisationRequetePreparee(connection, "SELECT", NOM_ENTITE, attributs, false);
            resultSet = preparedStatement.executeQuery();
            // récupération des valeurs des attributs de la BDD pour les mettre dans une
            // liste
            while (resultSet.next()) {
                etudiants.add(map(resultSet));
            }
        } catch (SQLException e) {
            logger.log(Level.WARN, "Échec de la recherche de l'objet.", e);
        } finally {
            // fermeture des ressources utilisées
            fermetures(resultSet, preparedStatement, connection);
        }
        return etudiants;
    }


    /**
     * Modifie UN Etudiant ayant pour attributs les mêmes que ceux spécifiés dans un
     * bean Etudiant et la même clé primaire. Cette clé primaire ne peut être
     * modifiée.
     *
     * @param etudiant l'Etudiant que l'on souhaite modifier dans la BDD.
     */
    @Override
    public void modifier(Etudiant etudiant) {
        //Update table SET <....>
        Map<String, Object> attributs = new HashMap<>();
        attributs.put(ATTRIBUT_REF_ANNEE, etudiant.getRefAnnee());

        //Preparation de WHERE
        Map<String, Object> whereAtt = new HashMap<>();
        whereAtt.put(ATTRIBUT_ID_ETUDIANT, etudiant.getIdEtudiant());

        //Modifier
        DAOUtilitaire.executeUpdate(this.getDaoFactory(), NOM_ENTITE, attributs, whereAtt);
    }


    /**
     * Supprime tous les Etudiants ayant pour attributs les mêmes que ceux spécifiés
     * dans un bean Etudiant.
     *
     * @param etudiant l'Etudiant que l'on souhaite supprimer dans la BDD.
     */
    @Override
    public void supprimer(Etudiant etudiant) {
        Map<String, Object> identifiants = new HashMap<>();
        identifiants.put(ATTRIBUT_ID_ETUDIANT, etudiant.getIdEtudiant());

        DAOUtilitaire.executeDelete(this.getDaoFactory(), NOM_ENTITE, identifiants);
    }

    /**
     * Liste tous les Etudiants présents dans la BDD.
     *
     * @return etudiants la liste des Etudiants présents dans la BDD.
     */
    @Override
    public List<Etudiant> lister() {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        List<Etudiant> etudiants = new ArrayList<>();
        try {
            // création d'une connexion grâce à la DAOFactory placée en attribut de la
            // classe
            connection = this.creerConnexion();
            preparedStatement = connection.prepareStatement(SQL_SELECT_TOUT);
            resultSet = preparedStatement.executeQuery();
            // récupération des valeurs des attributs de la BDD pour les mettre dans une
            // liste
            while (resultSet.next()) {
                etudiants.add(map(resultSet));
            }
        } catch (SQLException e) {
            logger.log(Level.WARN, "Échec du listage des objets.", e);
        } finally {
            fermetures(resultSet, preparedStatement, connection);
        }
        return etudiants;
    }

    /**
     * Fait la correspondance (le mapping) entre une ligne issue de la table
     * Etudiant (un ResultSet) et un bean Etudiant.
     *
     * @param resultSet la ligne issue de la table Etudiant.
     * @return etudiant le bean dont on souhaite faire la correspondance.
     * @throws SQLException exception
     */
    public static Etudiant map(ResultSet resultSet) throws SQLException {
        Etudiant etudiant = new Etudiant();
        etudiant.setIdEtudiant(resultSet.getLong(ATTRIBUT_ID_ETUDIANT));
        etudiant.setRefAnnee(resultSet.getLong(ATTRIBUT_REF_ANNEE));
        return etudiant;
    }
}
