package fr.eseo.ld.dao;

import fr.eseo.ld.pgl.beans.Sprint;
import fr.eseo.ld.utils.ParamUtilitaire;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static fr.eseo.ld.dao.DAOUtilitaire.*;

/**
 * Classe du DAO du bean Sprint.
 *
 * <p>
 * Cette classe implémente toutes les méthodes CRUD définies dans sa classe
 * mère.
 * </p>
 */
public class SprintDAO extends DAO<Sprint> {

    /* Noms des entités */
    private static final String NOM_ENTITE = "pgl_sprint";

    /* Attributs de l'entité Sprint */
    private static final String ATTRIBUT_ID_SPRINT = "idSprint";
    private static final String ATTRIBUT_DATE_DEBUT = "dateDebut";
    private static final String ATTRIBUT_NUMERO = "numero";
    private static final String ATTRIBUT_DATE_FIN = "dateFin";
    private static final String ATTRIBUT_NBR_HEURES = "nbrHeures";
    private static final String ATTRIBUT_REF_ANNEE = "refAnnee";
    private static final String ATTRIBUT_COEFFICIENT = "coefficient";
    private static final String ATTRIBUT_NOTEPUBLIEE = "notesPubliees";


    private static final String[] ATTRIBUTS_NOMS = {ATTRIBUT_ID_SPRINT, ATTRIBUT_NUMERO, ATTRIBUT_DATE_DEBUT, ATTRIBUT_DATE_FIN, ATTRIBUT_NBR_HEURES,
            ATTRIBUT_COEFFICIENT, ATTRIBUT_NOTEPUBLIEE, ATTRIBUT_REF_ANNEE};

    private static Logger logger = Logger.getLogger(SprintDAO.class.getName());

    /* Requetes SQL Sprint */
    private static final String SQL_INSERT_SPRINT = "INSERT INTO `pgl_sprint`(`numero`,`dateDebut`, `dateFin`, `nbrHeures`, `coefficient`,`refAnnee`, `notesPubliees`) VALUES (?,?,?,?,?,?,?)";
    private static final String SQL_SELECT_TOUT = "SELECT * FROM pgl_sprint";

    /* Requetes SQL pour trouver un sprint à partir de son ID */
    private static final String SQL_SELECT_SPRINT_A_PARTIR_ID = "SELECT * FROM pgl_sprint WHERE idSprint=?";
    private static final String SQL_SELECT_SPRINT_ANNEE =
            "SELECT * FROM pgl_sprint s " +
                    "JOIN anneeScolaire a ON a.idAnneeScolaire = s.refAnnee " +
                    "WHERE a.anneeDebut = ?";

    /**
     * Constructeur de DAO.
     *
     * @param daoFactory la Factory permettant la création d'une connexion à la BDD.
     */
    SprintDAO(DAOFactory daoFactory) {
        super(daoFactory);
    }

    /**
     * Insère un Sprint dans la BDD à partir des attributs spécifiés dans un bean
     * Sprint.
     *
     * @param sprint le Sprint que l'on souhaite insérer dans la BDD à partir du bean
     *               Sprint.
     */
    @Override
    public void creer(Sprint sprint) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            // création d'une connexion grâce à la DAOFactory placée en attribut de la
            // classe
            connection = this.creerConnexion();
            // les "?" de la requête sont comblés par les attributs de l'objet en paramètre
            // ceux-ci peuvent être nuls à condition que la BDD accepte les données de type
            // "null"
            preparedStatement = initialisationRequetePreparee(connection, SQL_INSERT_SPRINT, true,
                    sprint.getNumero(), sprint.getDateDebut(), sprint.getDateFin(), sprint.getNbrHeures(), sprint.getCoefficient(), sprint.getRefAnnee(), sprint.getNotesPubliees());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            logger.log(Level.WARN, "Échec de la création de l'objet, aucune ligne ajoutée dans la table.", e);
        } finally {
            // fermeture des ressources utilisées
            fermetures(preparedStatement, connection);
        }
    }

    /**
     * Liste tous les Sprints ayant pour attributs les mêmes que ceux spécifiés dans
     * un bean Sprint.
     *
     * @param sprint le Sprint que l'on souhaite trouver dans la BDD.
     * @return sprint la liste des Sprints trouvés dans la BDD.
     */
    @Override
    public List<Sprint> trouver(Sprint sprint) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        List<Sprint> sprints = new ArrayList<>();
        // tableau de Strings regroupant tous les noms des attributs d'un objet ainsi
        // que les valeurs correspondantes
        String[][] attributs = {ATTRIBUTS_NOMS,
                {String.valueOf(sprint.getIdSprint()), String.valueOf(sprint.getNumero()), String.valueOf(sprint.getDateDebut()), String.valueOf(sprint.getDateFin()),
                        String.valueOf(sprint.getNbrHeures()),
                        String.valueOf(sprint.getCoefficient()),
                        String.valueOf(sprint.getNotesPubliees()),
                        String.valueOf(sprint.getRefAnnee())
                }};
        try {
            // création d'une connexion grâce à la DAOFactory placée en attribut de la classe
            connection = this.creerConnexion();
            // mise en forme de la requête SELECT en fonction des attributs de l'objet sujet

            preparedStatement = initialisationRequetePreparee(connection, "SELECT", NOM_ENTITE, attributs, true);
            resultSet = preparedStatement.executeQuery();
            // récupération des valeurs des attributs de la BDD pour les mettre dans une liste
            while (resultSet.next()) {
                sprints.add(map(resultSet));
            }
        } catch (SQLException e) {
            logger.log(Level.WARN, "Échec de la recherche de l'objet Sprint. ", e);
        } finally {
            fermetures(resultSet, preparedStatement, connection);
        }
        return sprints;
    }

    /**
     * Renvoie un Sprint à partir de son ID.
     *
     * @param idSprint l'ID du Meeting que l'on souhaite trouver dans la BDD.
     * @return meeting le meeting trouvé dans la BDD.
     */
    public Sprint trouver(Long idSprint) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Sprint sprint = null;
        try {
            // création d'une connexion grâce à la DAOFactory placée en attribut de la
            // classe
            connection = this.creerConnexion();
            preparedStatement = initialisationRequetePreparee(connection, SQL_SELECT_SPRINT_A_PARTIR_ID, true,
                    String.valueOf(idSprint));
            resultSet = preparedStatement.executeQuery();
            resultSet.next();
            sprint = map(resultSet);
        } catch (SQLException e) {
            logger.log(Level.WARN, "Échec de la recherche d'un sprint à partir de son ID.", e);
        } finally {
            // fermeture des ressources utilisées
            fermetures(resultSet, preparedStatement, connection);
        }
        return sprint;
    }


    /**
     * Modifie un Sprint ayant pour attributs les mêmes que ceux spécifiés dans un
     * bean Sprint et la même clé primaire. Cette clé primaire ne peut être
     * modifiée.
     *
     * @param sprint le Sprint que l'on souhaite modifier dans la BDD.
     */
    @Override
    public void modifier(Sprint sprint) {
        //Update table SET <....>
        Map<String, Object> attributs = new HashMap<>();
        attributs.put(ATTRIBUT_DATE_DEBUT, sprint.getDateDebut());
        attributs.put(ATTRIBUT_DATE_FIN, sprint.getDateFin());
        attributs.put(ATTRIBUT_NBR_HEURES, sprint.getNbrHeures());
        attributs.put(ATTRIBUT_COEFFICIENT, sprint.getCoefficient());
        attributs.put(ATTRIBUT_REF_ANNEE, sprint.getRefAnnee());
        attributs.put(ATTRIBUT_NUMERO, sprint.getNumero());
        attributs.put(ATTRIBUT_NOTEPUBLIEE, sprint.getNotesPubliees());

        //Preparation de WHERE
        Map<String, Object> whereAtt = new HashMap<>();
        whereAtt.put(ATTRIBUT_ID_SPRINT, sprint.getIdSprint());

        //Modifier
        DAOUtilitaire.executeUpdate(this.getDaoFactory(), NOM_ENTITE, attributs, whereAtt);
    }

    /**
     * Supprime tous les Sprint ayant pour attributs les mêmes que ceux spécifiés
     * dans un bean Sprint.
     *
     * @param sprint le Sprint que l'on souhaite supprimer dans la BDD.
     */
    @Override
    public void supprimer(Sprint sprint) {
        Map<String, Object> identifiants = new HashMap<>();
        identifiants.put(ATTRIBUT_ID_SPRINT, sprint.getIdSprint());

        DAOUtilitaire.executeDelete(this.getDaoFactory(), NOM_ENTITE, identifiants);
    }

    /**
     * Liste tous les Sprints présents dans la BDD.
     *
     * @return sprints la liste des Sprints présents dans la BDD.
     */
    @Override
    public List<Sprint> lister() {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        List<Sprint> sprints = new ArrayList<>();
        try {
            // création d'une connexion grâce à la DAOFactory placée en attribut de la
            // classe
            connection = this.creerConnexion();
            preparedStatement = connection.prepareStatement(SQL_SELECT_TOUT);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                sprints.add(map(resultSet));
            }
            // récupération des valeurs des attributs de la BDD pour les mettre dans une
            // liste
        } catch (SQLException e) {
            logger.log(Level.WARN, "Échec du listage des objets sprints. ", e);
        } finally {
            fermetures(resultSet, preparedStatement, connection);
        }
        return sprints;
    }

    /**
     * Récupère la liste des sprints postérieurs à la date donnée
     *
     * @param sprint sprint de début
     * @return liste des sprints
     */
    public List<Sprint> recupererFutursSprint(Sprint sprint) {
        List<Sprint> sprints = new ArrayList<>();
        String sql = "SELECT * FROM pgl_sprint WHERE dateDebut > ?";

        ResultSet resultSet = null;
        try (Connection connection = this.creerConnexion(); PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setString(1, sprint.getDateDebut());
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                sprints.add(map(resultSet));
            }
        } catch (SQLException e) {
            logger.log(Level.WARN, "Échec du listage des objets de futur sprints. ", e);
        } finally {
            fermeture(resultSet);
        }
        return sprints;
    }

    /**
     * Récupère les spritns de l'année courante
     *
     * @param debutAnnee Debut de l'année (Utiliser Paramutilitaire pour récupérer l'année courante)
     * @return Liste des sprints
     * @see ParamUtilitaire#getAnneeCourante()
     */
    public List<Sprint> recupererSprintAnnee(Integer debutAnnee) {
        List<Sprint> sprints = new ArrayList<>();
        ResultSet resultSet = null;
        try (Connection connection = this.creerConnexion(); PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_SPRINT_ANNEE)) {
            preparedStatement.setInt(1, debutAnnee);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                sprints.add(map(resultSet));
            }
        } catch (SQLException e) {
            logger.log(Level.WARN, "Échec du listage des sprints de l'année. ", e);
        } finally {
            fermeture(resultSet);
        }
        return sprints;
    }

    // #################################################
    // # Méthodes privées #
    // #################################################

    /**
     * Fait la correspondance (le mapping) entre une ligne issue de la table Sprint
     * (un ResultSet) et un bean Sprint.
     *
     * @param resultSet la ligne issue de la table Sprint.
     * @return sprint le bean dont on souhaite faire la correspondance.
     */
    public static Sprint map(ResultSet resultSet) throws SQLException {
        Sprint sprint = new Sprint();
        sprint.setIdSprint(resultSet.getLong(ATTRIBUT_ID_SPRINT));
        sprint.setDateDebut(resultSet.getString(ATTRIBUT_DATE_DEBUT));
        sprint.setDateFin(resultSet.getString(ATTRIBUT_DATE_FIN));
        sprint.setNbrHeures(resultSet.getInt(ATTRIBUT_NBR_HEURES));
        sprint.setRefAnnee(resultSet.getLong(ATTRIBUT_REF_ANNEE));
        sprint.setCoefficient(resultSet.getFloat(ATTRIBUT_COEFFICIENT));
        sprint.setNumero(resultSet.getInt(ATTRIBUT_NUMERO));
        sprint.setNotesPubliees(resultSet.getBoolean(ATTRIBUT_NOTEPUBLIEE));
        return sprint;
    }

}
