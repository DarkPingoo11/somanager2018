package fr.eseo.ld.dao;

import fr.eseo.ld.utils.ParamUtilitaire;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Classe Utilitaire pour les DAO.
 *
 * <p>
 * Cette classe centralise des attributs et des méthodes utilisés par les DAO.
 * </p>
 *
 * @author Thomas MENARD et Maxime LENORMAND
 */
public final class DAOUtilitaire {

    // attributs des NoteInteret
    protected static final String ATTRIBUT_ID_PROFESSEUR = "idProfesseur";
    protected static final String ATTRIBUT_ID_SUJET = "idSujet";
    protected static final String ATTRIBUT_NOTE = "note";
    protected static final String TRY_MESSAGE = "Échec de la requête.";

    private static final String SELECT = "SELECT";
    private static final String UPDATE = "UPDATE";
    private static final String DELETE = "DELETE";
    private static final String WHERE = " WHERE ";
    private static final String WHERE_ID = " WHERE id";
    private static final String AND = " AND ";
    private static final String AND_ID = " AND id";
    private static final String VALUES = " VALUES ";

    private static Logger logger = Logger.getLogger(DAOUtilitaire.class.getName());

    /**
     * Constructeur caché par défaut (car c'est une classe finale utilitaire,
     * contenant uniquement des méthodes appelées de manière statique).
     */
    private DAOUtilitaire() {

    }

    // ########################################################################################################
    // # Methodes pour la fermeture des ressources #
    // ########################################################################################################

    /**
     * Ferme le resultset.
     *
     * @param resultSet
     *            le resultSet à fermer.
     */
    protected static void fermeture(ResultSet resultSet) {
        if (resultSet != null) {
            try {
                resultSet.close();
            } catch (SQLException e) {
                logger.log(Level.WARN, "Echec de la fermeture du ResultSet : " + e.getMessage(), e);
            }
        }
    }

    /**
     * Ferme le statement.
     *
     * @param statement
     *            le statement à fermer.
     */
    protected static void fermeture(Statement statement) {
        if (statement != null) {
            try {
                statement.close();
            } catch (SQLException e) {
                logger.log(Level.WARN, "Echec de la fermeture du Statement : " + e.getMessage(), e);
            }
        }
    }



    /**
     * Ferme la connection.
     *
     * @param connection
     *            la connection à fermer.
     */
    protected static void fermeture(Connection connection) {
        if (connection != null) {
            try {
                connection.close();
            } catch (SQLException e) {
                logger.log(Level.WARN, "Echec de la fermeture de la connexion : " + e.getMessage(), e);
            }
        }
    }

    /**
     * Ferme le statement et la connection.
     *
     * @param statement
     *            le statement à fermer.
     * @param connection
     *            la connection à fermer.
     */
    protected static void fermetures(Statement statement, Connection connection) {
        fermeture(statement);
        fermeture(connection);
    }

    /**
     * Ferme le resultSet, le statement et la connection.
     *
     * @param resultSet
     *            le resultSet à fermer.
     * @param statement
     *            le statement à fermer.
     * @param connection
     *            la connection à fermer.
     */
    protected static void fermetures(ResultSet resultSet, Statement statement, Connection connection) {
        fermeture(resultSet);
        fermeture(statement);
        fermeture(connection);
    }

    // ########################################################################################################
    // # Methodes pour la création des requêtes préparées #
    // ########################################################################################################

    /**
     * Initialise une requête préparée.
     *
     * @param connection
     *            la connexion à la BDD.
     * @param sql
     *            la requête SQL.
     * @param returnGeneratedKeys
     *            le boolean qui permet de générer des ID ou pas.
     * @param objets
     *            la liste d'objets à insérer dans la requête.
     * @return preparedStatement la requête préparée initialisée.
     * @throws SQLException
     */
    protected static PreparedStatement initialisationRequetePreparee(Connection connection, String sql,
                                                                     boolean returnGeneratedKeys, Object... objets) throws SQLException {
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(sql,
                    returnGeneratedKeys ? Statement.RETURN_GENERATED_KEYS : Statement.NO_GENERATED_KEYS);
            for (int i = 0; i < objets.length; i++) {
                preparedStatement.setObject(i + 1, objets[i]);
            }
        } catch (SQLException e) {
            logger.log(Level.WARN, TRY_MESSAGE, e);
        }
        return preparedStatement;
    }

    /**
     * Initialise une requête préparée modulable.
     *
     * @param connection
     *            la connexion à la BDD.
     * @param choixCRUD
     *            le choix de la méthode CRUD à appliquer.
     * @param nomEntite
     *            le nom de l'entité de la BDD concernée.
     * @param attributs
     *            le tableau de Strings regroupant tous les noms des attributs d'un
     *            objet ainsi que les valeurs correspondantes.
     * @param returnGeneratedKeys
     *            le boolean qui permet de générer des ID ou pas.
     * @return preparedStatement la requête préparée initialisée.
     * @throws SQLException
     */
    protected static PreparedStatement initialisationRequetePreparee(Connection connection, String choixCRUD,
                                                                     String nomEntite, String[][] attributs, boolean returnGeneratedKeys) throws SQLException {
        // création de la requête préparée
        List<String> valeursAttributs = creationRequete(choixCRUD, nomEntite, attributs);
        String sql = valeursAttributs.remove(0);

        // changement de la structure de la requête si le choix de la méthode CRUD est
        // "UPDATE"
        if (UPDATE.equals(choixCRUD)) {

            sql = sql.replace(AND_ID + nomEntite + " = ?", "");
            sql = sql.replace("AND", ", ");
            sql += new StringBuilder().append(WHERE_ID).append(nomEntite).append(" = ?").toString();
        }

        // remplacement des "?" par les valeurs des attributs
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(sql,
                    returnGeneratedKeys ? Statement.RETURN_GENERATED_KEYS : Statement.NO_GENERATED_KEYS);
            for (int i = 0; i < valeursAttributs.size(); i++) {
                preparedStatement.setObject(i + 1, valeursAttributs.get(i));
            }
        } catch (SQLException e) {
            logger.log(Level.WARN, TRY_MESSAGE, e);
        }
        return preparedStatement;
    }

    /**
     * Initialise une requête préparée modulable.
     * <p>
     * Cas du traitement des ID mais aussi des Strings. Attention : nomColonne doit
     * posséder valeurColonne.
     * </p>
     *
     * @param connection
     *            la connexion à la BDD.
     * @param choixCRUD
     *            le choix de la méthode CRUD à appliquer.
     * @param nomEntite
     *            le nom de l'entité de la BDD concernée.
     * @param idEntite
     *            l'ID de l'entité.
     * @param nomColonne
     *            le nom de la colonne manipulée.
     * @param valeurColonne
     *            la valeur recherchée dans la colonne.
     * @param attributs
     *            le tableau de Strings regroupant tous les noms des attributs d'un
     *            objet ainsi que les valeurs correspondantes.
     * @return preparedStatement la requête préparée.
     * @throws SQLException
     */
    protected static PreparedStatement initialisationRequetePreparee(Connection connection, String choixCRUD,
                                                                     String nomEntite, String idEntite, String nomColonne,
                                                                     String valeurColonne, String[][] attributs) throws SQLException {
        // création de la requête préparée
        List<String> valeursAttributs = creationRequete(choixCRUD, nomEntite, attributs);
        String sql = valeursAttributs.remove(0);

        // changement de la structure de la requête si le choix de la méthode CRUD est
        // "UPDATE" ou "DELETE"
        StringBuilder sqlBuilder = new StringBuilder();
        if (UPDATE.equals(choixCRUD)) {
            sql = sql.replace("AND " + idEntite + " = ?", "");
            sql = sql.replace(AND, ", ");

            sqlBuilder.append(WHERE).append(idEntite).append(" = ?");
            sqlBuilder.append(AND).append(nomColonne).append(" LIKE '%").append(valeurColonne).append("%'");
        } else if (DELETE.equals(choixCRUD)) {
            sqlBuilder.append(AND).append(nomColonne).append(" LIKE '%").append(valeurColonne).append("%'");
        }
        sql += sqlBuilder.toString();

        // remplacement des "?" par les valeurs des attributs
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(sql, Statement.NO_GENERATED_KEYS);
            for (int i = 0; i < valeursAttributs.size(); i++) {
                preparedStatement.setObject(i + 1, valeursAttributs.get(i));
            }
        } catch (SQLException e) {
            logger.log(Level.WARN, TRY_MESSAGE, e);
        }
        return preparedStatement;
    }

    /**
     * Initialise une requête préparée modulable.
     * <p>
     * Cas de deux ID comme clés primaires.
     * </p>
     *
     * @param connection
     *            la connexion à la BDD.
     * @param choixCRUD
     *            le choix de la méthode CRUD à appliquer.
     * @param nomEntite
     *            le nom de l'entité de la BDD concernée.
     * @param attributs
     *            le tableau de Strings regroupant tous les noms des attributs d'un
     *            objet ainsi que les valeurs correspondantes.
     * @param nomEntiteId1
     *            nom de l'entité possédant la première clé primaire.
     * @param nomEntiteId2
     *            nom de l'entité possédant la seconde clé primaire.
     * @return preparedStatement la requête préparée initialisée.
     * @throws SQLException
     */
    protected static PreparedStatement initialisationRequetePreparee(Connection connection, String choixCRUD,
                                                                     String nomEntite, String[][] attributs, String nomEntiteId1, String nomEntiteId2,
                                                                     boolean returnGeneratedKeys) throws SQLException {
        // création de la requête préparée
        List<String> valeursAttributs = creationRequete(choixCRUD, nomEntite, attributs);
        String sql = valeursAttributs.remove(0);
        StringBuilder sqlBuilder = new StringBuilder();

        // changement de la structure de la requête si le choix de la méthode CRUD est
        // "UPDATE"
        if (UPDATE.equals(choixCRUD)) {
            sql = sql.replace(AND_ID + nomEntiteId1 + " = ?", "");
            sql = sql.replace(AND_ID + nomEntiteId2 + " = ?", "");
            sql = sql.replace(AND, ", ");
            sqlBuilder.append(WHERE_ID).append(nomEntiteId1).append(" = ?");
            sqlBuilder.append(AND_ID).append(nomEntiteId2).append(" = ?");
            sql += sqlBuilder.toString();
        }

        // remplacement des "?" par les valeurs des attributs
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(sql,
                    returnGeneratedKeys ? Statement.RETURN_GENERATED_KEYS : Statement.NO_GENERATED_KEYS);
            for (int i = 0; i < valeursAttributs.size(); i++) {
                preparedStatement.setObject(i + 1, valeursAttributs.get(i));
            }
        } catch (SQLException e) {
            logger.log(Level.WARN, TRY_MESSAGE, e);
        }
        return preparedStatement;
    }

    /**
     * Initialise une requête préparée modulable.
     * <p>
     * Cas de trois ID comme clés primaires.
     * </p>
     *
     * @param connection
     *            la connexion à la BDD.
     * @param choixCRUD
     *            le choix de la méthode CRUD à appliquer.
     * @param nomEntite
     *            le nom de l'entité de la BDD concernée.
     * @param attributs
     *            le tableau de Strings regroupant tous les noms des attributs d'un
     *            objet ainsi que les valeurs correspondantes.
     * @param nomEntiteIds
     *            noms de l'entité possédant la première clé primaire, la seconde et la dernière.
     * @return preparedStatement la requête préparée initialisée.
     */
    protected static PreparedStatement initialisationRequetePreparee(Connection connection, String choixCRUD,
                                                                     String nomEntite, String[][] attributs,
                                                                     String[] nomEntiteIds, boolean returnGeneratedKeys) throws SQLException {
        // création de la requête préparée
        List<String> valeursAttributs = creationRequete(choixCRUD, nomEntite, attributs);
        String sql = valeursAttributs.remove(0);
        StringBuilder sqlBuilder = new StringBuilder();

        if(nomEntiteIds.length == 3) {
            String nomEntiteId1 = nomEntiteIds[0];
            String nomEntiteId2 = nomEntiteIds[1];
            String nomEntiteId3 = nomEntiteIds[2];

            // changement de la structure de la requête si le choix de la méthode CRUD est
            // "UPDATE"
            if (UPDATE.equals(choixCRUD)) {
                sql = sql.replace(AND_ID + nomEntiteId1 + " = ?", "");
                sql = sql.replace(AND_ID + nomEntiteId2 + " = ?", "");
                sql = sql.replace(AND_ID + nomEntiteId3 + " = ?", "");
                sql = sql.replace(AND, ", ");
                sqlBuilder.append(WHERE_ID).append(nomEntiteId1).append(" = ?");
                sqlBuilder.append(AND_ID).append(nomEntiteId2).append(" = ?");
                sqlBuilder.append(AND_ID).append(nomEntiteId3).append(" = ?");

                sql += sqlBuilder.toString();
            }

            // remplacement des "?" par les valeurs des attributs
            PreparedStatement preparedStatement = null;
            try {
                preparedStatement = connection.prepareStatement(sql,
                        returnGeneratedKeys ? Statement.RETURN_GENERATED_KEYS : Statement.NO_GENERATED_KEYS);
                for (int i = 0; i < valeursAttributs.size(); i++) {
                    preparedStatement.setObject(i + 1, valeursAttributs.get(i));
                }
            } catch (SQLException e) {
                logger.log(Level.WARN, TRY_MESSAGE, e);
            }

            return preparedStatement;

        } else if (nomEntiteIds.length == 2){
            try {
                return initialisationRequetePreparee(connection, choixCRUD, nomEntite, attributs, nomEntiteIds[0], nomEntiteIds[1], returnGeneratedKeys);
            } catch(SQLException e) {
                logger.log(Level.WARN, TRY_MESSAGE, e);
            }
        } else {
            throw new SQLException("Il n'y a pas assez d'arguments");
        }

        return null;
    }

    /**
     * Traite la mise à jour de la BDD induite par la méthode void modifier(T
     * objet).
     *
     * @param daoFactory
     *            la DAOFactory qui permet de créer une connexion.
     * @param nomEntite
     *            le nom de l'entité à traiter.
     * @param attributs
     *            les attributs de l'entité à traiter.
     * @param logger
     *            le logger associé à l'entité à traiter.
     */
    protected static void traitementUpdate(DAOFactory daoFactory, String nomEntite, String[][] attributs,
                                           Logger logger) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            // création d'une connexion grâce à la DAOFactory placée en attribut de la
            // classe
            connection = daoFactory.getConnection();
            // mise en forme de la requête UPDATE en fonction des attributs de l'objet
            preparedStatement = initialisationRequetePreparee(connection, UPDATE, nomEntite, attributs, false);

            if(preparedStatement != null) {
                preparedStatement.executeUpdate();
            } else {
                logger.log(Level.WARN, "Impossible d'executer la mise a jour, la requête est nulle");
            }
        } catch (SQLException e) {
            logger.log(Level.WARN, "Échec de la mise à jour de l'objet, aucune ligne modifiée dans la table.", e);
        } finally {
            // fermeture des ressources utilisées
            fermetures(preparedStatement, connection);
        }
    }

    /**
     * Crée la requête préparée.
     *
     * @param choixCRUD
     *            le choix de la méthode CRUD à appliquer.
     * @param nomEntite
     *            le nom de l'entité de la BDD concernée.
     * @param attributs
     *            le tableau de Strings regroupant tous les noms des attributs d'un
     *            objet ainsi que les valeurs correspondantes.
     * @return valeursAttributs la requête préparée créée et les valeurs des
     *         attributs.
     */
    private static ArrayList<String> creationRequete(String choixCRUD, String nomEntite, String[][] attributs) {
        // début de la requête en fonction du choix de la méthode CRUD
        StringBuilder sqlbuilder = new StringBuilder();
        sqlbuilder.append(creationDebutRequete(choixCRUD, nomEntite));

        // création de la requête et stockage des valeurs des attributs
        ArrayList<String> valeursAttributs = new ArrayList<>();
        boolean dejaFait = false;
        for (int i = 0; i < attributs[0].length; i++) {
            // si la valeur de l'attribut courant n'est pas nulle
            if (attributs[1][i] != null && attributs[1][i] != "null") {
                // on la stocke dans une liste
                valeursAttributs.add(attributs[1][i]);
                // on continue la création de la requête
                if (!dejaFait) {
                    sqlbuilder.append(attributs[0][i]).append(" = ?");
                    dejaFait = true;
                } else {
                    sqlbuilder.append(AND).append(attributs[0][i]).append(" = ?");
                }
            }
        }
        valeursAttributs.add(0, sqlbuilder.toString());

        return valeursAttributs;
    }

    /**
     * Crée le début de la requête préparée.
     *
     * @param choixCRUD
     *            le choix de la méthode CRUD à appliquer.
     * @param nomEntite
     *            le nom de l'entité de la BDD concernée.
     * @return sql le début de la requête créée.
     */
    private static String creationDebutRequete(String choixCRUD, String nomEntite) {
        String sql;

        if (SELECT.equals(choixCRUD)) {
            sql = "SELECT * FROM " + nomEntite + WHERE;
        } else if (DELETE.equals(choixCRUD)) {
            sql = "DELETE FROM " + nomEntite + WHERE;
        } else if (UPDATE.equals(choixCRUD)) {
            sql = "UPDATE " + nomEntite + " SET ";
        } else {
            throw new DAOException("Erreur : choixCRUD ne peut être que 'SELECT', 'DELETE' ou 'UPDATE'");
        }

        return sql;
    }


    /*************************************/
    /** Méthodes contre fuites mémoires **/
    /*************************************/

    /**
     * Nouvelle methode contre les fuites mémoire, permettant plus de souplesse pour update une table
     * @param daoFactory daoFactory
     * @param nomTable Table sur laquelle effectuer l'update
     * @param attributs Attributs a modifier : SET key=value, key2=value2, etc
     * @param whereValues Attributs where : WHERE key=value AND key2=value2
     */
    public static void executeUpdate(DAOFactory daoFactory, String nomTable, Map<String, Object> attributs, Map<String, Object> whereValues) {
        //Définition de update table SET (a=b, c=d, ...)
        String set = preparerRequete(attributs, ", ");
        //Définition de la clause WHERE
        String where = preparerRequete(whereValues, AND);

        //Définition de la requete au complet
        StringBuilder sql = new StringBuilder();
        sql.append("UPDATE ").append(nomTable).append(" SET ").append(set).append(" ");
        sql.append(WHERE).append(" ").append(where);

        try (Connection connection = daoFactory.getConnection();PreparedStatement preparedStatement = connection.prepareStatement(sql.toString())){
            int index = 1;
            //Définition du SET
            for (Object o : attributs.values()) {
                if(ParamUtilitaire.notNull(o)) {
                    preparedStatement.setObject(index++, o);
                }
            }
            for (Object o : whereValues.values()) {
                if(ParamUtilitaire.notNull(o)) {
                    preparedStatement.setObject(index++, o);
                }
            }

            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            logger.log(Level.WARN, "Échec de la mise à jour de l'objet, aucune ligne modifiée dans la table", e);
        }
    }

    /**
     * Nouvelle methode contre les fuites mémoires, pour supprimer une ligne d'une table
     * @param daoFactory daoFactory
     * @param nomTable Table concernée
     * @param identifiants Identifiants de l'objet à supprimer : WHERE key=value, key2=value2
     */
    public static void executeDelete(DAOFactory daoFactory, String nomTable, Map<String, Object> identifiants) {
        //Définition de la clause WHERE
        String where = preparerRequete(identifiants, AND);
        //Définition de la requete au complet
        StringBuilder sql = new StringBuilder();
        sql.append("DELETE FROM ").append(nomTable).append(WHERE).append(where);
        try (Connection connection = daoFactory.getConnection();PreparedStatement preparedStatement = connection.prepareStatement(sql.toString())){
            int index = 1;
            //Définition du WHERE
            for (Object o : identifiants.values()) {
                if(ParamUtilitaire.notNull(o)) {
                    preparedStatement.setObject(index++, o);
                }
            }

            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            logger.log(Level.WARN, "Échec de la suppression de l'objet, aucune ligne modifiée dans la table", e);
        }
    }

    /**
     * Créer un objet dans la table indiquée
     * @param daoFactory daoFactory
     * @param nomTable Nom de la table
     * @param attributs Attributs avec valeurs de l'objet tels que : INSERT INTO (key1, key2,...) VALUES (val1, val2, ...)
     */
    public static void executeCreer(DAOFactory daoFactory, String nomTable, Map<String, Object> attributs) {
        getOnlyNotNullValues(attributs);

        String att = StringUtils.join(attributs.keySet(), ", ");
        StringBuilder toInsert = new StringBuilder();
        for(int i = 0; i < attributs.size(); i++) {
            toInsert.append(",?");
        }

        //Définition de la requete au complet
        StringBuilder sql = new StringBuilder();
        sql.append("INSERT INTO ").append(nomTable).append("(").append(att).append(")");
        sql.append(VALUES).append("(").append(toInsert.length() > 0 ? toInsert.substring(1) : toInsert).append(")");

        try (Connection connection = daoFactory.getConnection();PreparedStatement preparedStatement = connection.prepareStatement(sql.toString())){
            int index = 1;
            //Définition des valeurs
            for (Object o : attributs.values()) {
                if(ParamUtilitaire.notNull(o)) {
                    preparedStatement.setObject(index++, o);
                }
            }
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            logger.log(Level.WARN, "Échec de la création de l'objet, aucune ligne ajoutée dans la table.", e);
        }
    }


    /**
     * Prépare une requete préparée pour chaque attribut de la map
     * SET A=b, c=d, e=f
     * WHERE a=b AND c=d AND e=f
     * etc
     * @param values Map des attributs tel que nom=valeur
     * @param separator Séparateur
     * @return Requete de type a=? AND b=?
     */
    private static String preparerRequete(Map<String, Object> values, String separator) {
        StringBuilder requete = new StringBuilder();
        for (Map.Entry<String, Object> entry : values.entrySet()) {
            Object o = entry.getValue();
            if (o != null && !o.toString().equals("null")) {
                requete.append(separator).append(entry.getKey()).append("= ?");
            }
        }

        requete = new StringBuilder(requete.length() > 0 ? requete.substring(separator.length()) : requete.toString());
        return requete.toString();
    }

    /**
     * Supprime les valeurs null de la map
     * @param map Map
     * @return Map sans les valeurs nulles
     */
    private static void getOnlyNotNullValues(Map<String, Object> map) {
        map.entrySet().removeIf(entry -> entry.getValue() == null || entry.getValue().toString().equals("null"));
    }

}