package fr.eseo.ld.dao;

import fr.eseo.ld.beans.Notification;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static fr.eseo.ld.dao.DAOUtilitaire.fermetures;
import static fr.eseo.ld.dao.DAOUtilitaire.initialisationRequetePreparee;

/**
 * Classe du DAO du bean Notification.
 *
 * <p>
 * Cette classe implémente toutes les méthodes CRUD définies dans sa classe
 * mère.
 * </p>
 *
 * @author Hugo Menard
 */
public class NotificationDAO extends DAO<Notification> {

	private static final String NOM_ENTITE = "Notification";
	private static final String ATTRIBUT_ID_UTILISATEUR = "idUtilisateur";
	private static final String ATTRIBUT_COMMENTAIRE = "commentaire";
	private static final String ATTRIBUT_LIEN = "lien";
	private static final String ATTRIBUT_VUE = "vue";
	private static final String ATTRIBUT_ID_NOTIFICATION = "idNotification" ;
	private static final String[] ATTRIBUTS_NOMS = { ATTRIBUT_ID_UTILISATEUR, ATTRIBUT_COMMENTAIRE, ATTRIBUT_LIEN,
			ATTRIBUT_VUE };
	private static final String[] ATTRIBUTS_NOMS_SANS_COMMENTAIRE = { ATTRIBUT_ID_UTILISATEUR, ATTRIBUT_LIEN,
			ATTRIBUT_VUE };

	private static Logger logger = Logger.getLogger(NotificationDAO.class.getName());

	/**
	 * Constructeur de DAO.
	 *
	 * @param daoFactory
	 *            la Factory permettant la création d'une connexion à la BDD.
	 */
	public NotificationDAO(DAOFactory daoFactory) {
		super(daoFactory);
	}

	/**
	 * Insère une Notification dans la BDD à partir des attributs spécifiés dans un
	 * bean Notification.
	 *
	 * @param notification
	 *            la Notification que l'on souhaite insérer dans la BDD à partir du
	 *            bean Notification.
	 */
	@Override
	public void creer(Notification notification) {

		Map<String, Object> attributs = new HashMap<>();
		attributs.put(ATTRIBUT_COMMENTAIRE, notification.getCommentaire());
		attributs.put(ATTRIBUT_LIEN, notification.getLien());
		attributs.put(ATTRIBUT_VUE, notification.getVue());
		attributs.put(ATTRIBUT_ID_UTILISATEUR, notification.getIdUtilisateur());

		DAOUtilitaire.executeCreer(this.getDaoFactory(), NOM_ENTITE, attributs);
	}

	/**
	 * Liste toutes les Notifications ayant pour attributs les mêmes que ceux
	 * spécifiés dans un bean Notification.
	 *
	 * @param notification
	 *            la Notification que l'on souhaite trouver dans la BDD.
	 * @return notifications la liste des Notifications trouvées dans la BDD.
	 */
	@Override
	public List<Notification> trouver(Notification notification) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<Notification> notifications = new ArrayList<>();
		// tableau de Strings regroupant tous les noms des attributs d'un objet ainsi
		// que les valeurs correspondantes
		String[][] attributs = { ATTRIBUTS_NOMS, { String.valueOf(notification.getIdUtilisateur()),
				notification.getCommentaire(), notification.getLien(), String.valueOf(notification.getVue()) } };
		try {
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			// mise en forme de la requête SELECT en fonction des attributs de l'objet sujet
			preparedStatement = initialisationRequetePreparee(connection, "SELECT", NOM_ENTITE, attributs, false);
			resultSet = preparedStatement.executeQuery();
			// récupération des valeurs des attributs de la BDD pour les mettre dans une
			// liste
			while (resultSet.next()) {
				notifications.add(map(resultSet));
			}
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec de la recherche de l'objet.", e);
		} finally {
			// fermeture des ressources utilisées
			fermetures(resultSet, preparedStatement, connection);
		}
		return notifications;
	}

	/**
	 * Modifie des notificiations ayant pour attributs les mêmes que ceux spécifiés
	 * dans un bean Notification et le même idUtilisateur et commentaire.
	 *
	 * @param notification
	 *            la Notification que l'on souhaite modifier dans la BDD.
	 */
	@Override
	public void modifier(Notification notification) {
		Map<String, Object> attributs = new HashMap<>();
		attributs.put(ATTRIBUT_COMMENTAIRE, notification.getCommentaire());
		attributs.put(ATTRIBUT_LIEN, notification.getLien());
		attributs.put(ATTRIBUT_VUE, notification.getVue());

		Map<String, Object> where = new HashMap<>();
		where.put(ATTRIBUT_ID_NOTIFICATION, notification.getIdNotification());
		where.put(ATTRIBUT_ID_UTILISATEUR, notification.getIdUtilisateur());

		DAOUtilitaire.executeUpdate(this.getDaoFactory(), NOM_ENTITE, attributs, where);
	}

	/**
	 * Supprime toutes les Notifications ayant pour attributs les mêmes que ceux
	 * spécifiés dans un bean Notification.
	 *
	 * @param notification
	 *            la Notification que l'on souhaite supprimer dans la BDD.
	 */
	@Override
	public void supprimer(Notification notification) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		// tableau de Strings regroupant tous les noms des attributs d'un objet ainsi
		// que les valeurs correspondantes
		String[][] attributs = { ATTRIBUTS_NOMS_SANS_COMMENTAIRE, { String.valueOf(notification.getIdUtilisateur()),
				notification.getLien(), String.valueOf(notification.getVue()) } };
		try {
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			preparedStatement = initialisationRequetePreparee(connection, "DELETE", NOM_ENTITE, ATTRIBUT_ID_UTILISATEUR,
					ATTRIBUT_COMMENTAIRE, notification.getCommentaire(), attributs);
			int statut = preparedStatement.executeUpdate();
			if (statut == 0) {
				throw new SQLException();
			} else {
				// suppression du bean
				notification.setIdNotification(null);
			}
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec de la suppression de l'objet, aucune ligne supprimée de la table.", e);
		} finally {
			// fermeture des ressources utilisées
			fermetures(preparedStatement, connection);
		}
	}

	/**
	 * Non implémentée sur NotificationDAO. Pas nécessaire et peu sécurisée.
	 */
	@Override
	public List<Notification> lister() {
		return new ArrayList<>();
	}

	/**
	 * Ajoute une notification.
	 * <p>
	 * Cas d'une notification qui renvoie vers un lien donné.
	 * </p>
	 *
	 * @param idUtilisateur
	 *            l'ID de l'utilisateur notifié.
	 * @param commentaire
	 *            le commentaire de la notification.
	 * @param lien
	 *            le lien vers lequel l'utilisateur sera renvoyé.
	 */
	public void ajouterNotification(Long idUtilisateur, String commentaire, String lien) {
		/* Suppression des notifications qui possèdent ce commentaire */
		Notification notification = new Notification();
		notification.setIdUtilisateur(idUtilisateur);
		notification.setCommentaire(commentaire);
		try {
			this.supprimer(notification);
		} catch (Exception e) {
			logger.log(Level.WARN,
					"Échec de la suppression des notifications précédantes, aucune ligne supprimée de la table.", e);
		}

		/* Création de la nouvelle notification */
		notification.setCommentaire(commentaire);
		notification.setLien(lien);
		notification.setVue(0);
		this.creer(notification);
	}

	/**
	 * Ajoute une notification.
	 * <p>
	 * Cas d'une notification générique qui renvoie vers l'Accueil.
	 * </p>
	 *
	 * @param idUtilisateur
	 *            l'ID de l'utilisateur notifié.
	 * @param commentaire
	 *            le commentaire de la notification.
	 */
	public void ajouterNotification(Long idUtilisateur, String commentaire) {
		ajouterNotification(idUtilisateur, commentaire, "Dashboard");
	}

	/**
	 * Ajoute une notification.
	 * <p>
	 * Cas de la notification lors d'un commentaire de sujet.
	 * </p>
	 *
	 * @param idUtilisateur
	 *            l'ID de l'utilisateur notifié.
	 * @param prefixe
	 *            le prefixe du commentaire.
	 * @param commentaire
	 *            le commentaire de la notification.
	 * @param suffixe
	 *            le suffixe du commentaire.
	 */
	public void ajouterNotification(Long idUtilisateur, String prefixe, String commentaire, String suffixe) {
		/* Suppression des notifications qui possèdent ce commentaire */
		Notification notification = new Notification();
		notification.setIdUtilisateur(idUtilisateur);
		notification.setCommentaire(commentaire);
		try {
			this.supprimer(notification);
		} catch (Exception e) {
			logger.log(Level.ALL,
					"Échec de la suppression des notifications précédentes, aucune ligne supprimée de la table.", e);
		}

		/* Création de la nouvelle notification */
		notification.setCommentaire(prefixe + commentaire + suffixe);
		notification.setLien("VoirMesSujets");
		notification.setVue(0);
		this.creer(notification);
	}

	/**
	 * Fait la correspondance (le mapping) entre une ligne issue de la table
	 * Notification (un ResultSet) et un bean Notification.
	 *
	 * @param resultSet
	 *            la ligne issue de la table Notification.
	 * @return notification le bean dont on souhaite faire la correspondance.
	 * @throws SQLException
	 */
	public static Notification map(ResultSet resultSet) throws SQLException {
		Notification notification = new Notification();
		notification.setIdNotification(resultSet.getLong(ATTRIBUT_ID_NOTIFICATION));
		notification.setIdUtilisateur(resultSet.getLong(ATTRIBUT_ID_UTILISATEUR));
		notification.setCommentaire(resultSet.getString(ATTRIBUT_COMMENTAIRE));
		notification.setLien(resultSet.getString(ATTRIBUT_LIEN));
		notification.setVue(resultSet.getInt(ATTRIBUT_VUE));
		return notification;
	}

}