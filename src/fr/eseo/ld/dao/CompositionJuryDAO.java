package fr.eseo.ld.dao;

import fr.eseo.ld.pgl.beans.CompositionJury;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static fr.eseo.ld.dao.DAOUtilitaire.fermetures;
import static fr.eseo.ld.dao.DAOUtilitaire.initialisationRequetePreparee;

/**
 * Classe du DAO du bean CompositionJury.
 *
 * <p>
 * Cette classe implémente toutes les méthodes CRUD définies dans sa classe
 * mère.
 * </p>
 *
 * @author Pierre CESMAT
 */
public class CompositionJuryDAO extends DAO<CompositionJury> {
    private static final String NOM_ENTITE = "pgl_compositionjury";
    private static final String ATTRIBUT_ID_SOUTENANCE = "refSoutenance";
    private static final String ATTRIBUT_ID_PROF = "refProfesseur";
    private static final String[] ATTRIBUTS_NOMS = {ATTRIBUT_ID_SOUTENANCE, ATTRIBUT_ID_PROF};

    // tous les attributs du bean doivent être écrits dans la requête INSERT
    private static final String SQL_INSERT = "INSERT INTO pgl_compositionjury(refSoutenance, refProfesseur) VALUES (?, ?)";
    private static final String SQL_SELECT_TOUT = "SELECT * FROM pgl_compositionjury";

    private static Logger logger = Logger.getLogger(CompositionJuryDAO.class.getName());

    CompositionJuryDAO(DAOFactory daoFactory) {
        super(daoFactory);
    }

    /**
     * Insère une CompositionJury dans la BDD à partir des attributs spécifiés dans un
     * bean CompositionJury.
     *
     * @param compositionJury le CompositionJury que l'on souhaite insérer dans la BDD à partir du
     *                        bean CompositionJury.
     */
    @Override
    public void creer(CompositionJury compositionJury) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            // création d'une connexion grâce à la DAOFactory placée en attribut de la
            // classe
            connection = this.creerConnexion();
            // les "?" de la requête sont comblés par les attributs de l'objet en paramètre
            // ceux-ci peuvent être nuls à condition que la BDD accepte les données de type
            // "null"
            preparedStatement = initialisationRequetePreparee(connection, SQL_INSERT, true, compositionJury.getRefSoutenance(), compositionJury.getRefProfesseur());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            logger.log(Level.WARN, "Échec de la création de l'objet, aucune ligne ajoutée dans la table.", e);
        } finally {
            // fermeture des ressources utilisées
            fermetures(preparedStatement, connection);
        }
    }

    /**
     * Liste tous les CompositionJury ayant pour attributs les mêmes que ceux spécifiés
     * dans un bean CompositionJury.
     *
     * @param compositionJury le CompositionJury que l'on souhaite trouver dans la BDD.
     * @return compositionJury la liste des CompositionJury trouvés dans la BDD.
     */
    @Override
    public List<CompositionJury> trouver(CompositionJury compositionJury) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        List<CompositionJury> compositionJurys = new ArrayList<>();
        // tableau de Strings regroupant tous les noms des attributs d'un objet ainsi
        // que les valeurs correspondantes
        String[][] attributs = {ATTRIBUTS_NOMS, {String.valueOf(compositionJury.getRefSoutenance()),
                String.valueOf(compositionJury.getRefProfesseur())}};
        try {
            // création d'une connexion grâce à la DAOFactory placée en attribut de la
            // classe
            connection = this.creerConnexion();
            // mise en forme de la requête SELECT en fonction des attributs de l'objet
            // compositionJury
            preparedStatement = DAOUtilitaire.initialisationRequetePreparee(connection, "SELECT", NOM_ENTITE, attributs,
                    false);
            resultSet = preparedStatement.executeQuery();
            // récupération des valeurs des attributs de la BDD pour les mettre dans une
            // liste
            while (resultSet.next()) {
                compositionJurys.add(map(resultSet));
            }
        } catch (SQLException e) {
            logger.log(Level.WARN, "Échec de la recherche de l'objet.", e);
        } finally {
            // fermeture des ressources utilisées
            fermetures(resultSet, preparedStatement, connection);
        }
        return compositionJurys;
    }

    /**
     * Modifie UN CompositionJury ayant pour attributs les mêmes que ceux spécifiés dans
     * un bean CompositionJury et la même clé primaire. Cette clé primaire ne peut être
     * modifiée.
     *
     * @param compositionJury le CompositionJury que l'on souhaite modifier dans la BDD.
     */
    @Override
    public void modifier(CompositionJury compositionJury) {
        // tableau de Strings regroupant tous les noms des attributs d'un objet ainsi
        // que les valeurs correspondantes
        // /!\ bien placer la clé primaire et sa valeur à la FIN du tableau sinon erreur
        Map<String, Object> attriButs = new HashMap<>();
        attriButs.put(ATTRIBUT_ID_PROF, compositionJury.getRefProfesseur());
        Map<String, Object> whereAtt = new HashMap<>();
        whereAtt.put(ATTRIBUT_ID_SOUTENANCE, compositionJury.getRefSoutenance());
        whereAtt.put(ATTRIBUT_ID_PROF, compositionJury.getRefProfesseur());
        //Modifier
        DAOUtilitaire.executeUpdate(this.getDaoFactory(), NOM_ENTITE, whereAtt, attriButs);
    }

    /**
     * Supprime tous les CompositionJury ayant pour attributs les mêmes que ceux
     * spécifiés dans un bean CompositionJury.
     *
     * @param compositionJury le CompositionJury que l'on souhaite supprimer dans la BDD.
     */
    @Override
    public void supprimer(CompositionJury compositionJury) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        // tableau de Strings regroupant tous les noms des attributs d'un objet ainsi
        // que les valeurs correspondantes
        String[][] attributs = {ATTRIBUTS_NOMS,
                {String.valueOf(compositionJury.getRefSoutenance()),
                        String.valueOf(compositionJury.getRefProfesseur())}};
        try {
            // création d'une connexion grâce à la DAOFactory placée en attribut de la
            // classe
            connection = this.creerConnexion();
            preparedStatement = DAOUtilitaire.initialisationRequetePreparee(connection, "DELETE", NOM_ENTITE, attributs,
                    false);
            int statut = preparedStatement.executeUpdate();
            if (statut == 0) {
                throw new SQLException();
            } else {
                // suppression du bean
                compositionJury.setRefProfesseur(null);
                compositionJury.setRefSoutenance(null);
            }
        } catch (SQLException e) {
            logger.log(Level.WARN, "Échec de la suppression de l'objet, aucune ligne supprimée de la table.", e);
        } finally {
            // fermeture des ressources utilisées
            fermetures(preparedStatement, connection);
        }
    }

    /**
     * Liste tous les CcompositionJury présents dans la BDD.
     *
     * @return compositionJury la liste des CompositionJury présents dans la BDD.
     */
    @Override
    public List<CompositionJury> lister() {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        List<CompositionJury> compositionJury = new ArrayList<>();
        try {
            // création d'une connexion grâce à la DAOFactory placée en attribut de la
            // classe
            connection = this.creerConnexion();
            preparedStatement = connection.prepareStatement(SQL_SELECT_TOUT);
            resultSet = preparedStatement.executeQuery();
            // récupération des valeurs des attributs de la BDD pour les mettre dans une
            // liste
            while (resultSet.next()) {
                compositionJury.add(map(resultSet));
            }
        } catch (SQLException e) {
            logger.log(Level.WARN, "Échec du listage des objets.", e);
        } finally {
            fermetures(resultSet, preparedStatement, connection);
        }
        return compositionJury;
    }

    /**
     * Fait la correspondance (le mapping) entre une ligne issue de la table
     * CompositionJury (un ResultSet) et un bean CompositionJury.
     *
     * @param resultSet la ligne issue de la table CompositionJury.
     * @return compositionJury le bean dont on souhaite faire la correspondance.
     * @throws SQLException
     */
    public static CompositionJury map(ResultSet resultSet) throws SQLException {
        CompositionJury compositionJury = new CompositionJury();
        compositionJury.setRefProfesseur(resultSet.getLong(ATTRIBUT_ID_PROF));
        if (resultSet.wasNull()) {
            compositionJury.setRefProfesseur(null);
        }
        compositionJury.setRefSoutenance(resultSet.getLong(ATTRIBUT_ID_SOUTENANCE));
        return compositionJury;
    }

}