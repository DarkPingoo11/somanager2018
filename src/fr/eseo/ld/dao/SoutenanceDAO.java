package fr.eseo.ld.dao;

import fr.eseo.ld.beans.Soutenance;
import fr.eseo.ld.beans.Sujet;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import static fr.eseo.ld.dao.DAOUtilitaire.*;

/**
 * Classe du DAO du bean Soutenance.
 * 
 * <p>
 * Cette classe implémente toutes les méthodes CRUD définies dans sa classe
 * mère.
 * </p>
 * 
 * @author Hugo MENARD
 */
public class SoutenanceDAO extends DAO<Soutenance> {

	private static final String NOM_ENTITE = "Soutenance";
	private static final String ATTRIBUT_ID_SOUTENANCE = "idSoutenance";
	private static final String ATTRIBUT_DATE_SOUTENANCE = "dateSoutenance";
	private static final String ATTRIBUT_ID_JURY_SOUTENANCE = "idJurySoutenance";
	private static final String ATTRIBUT_ID_EQUIPE = "idEquipe";
	private static final String[] ATTRIBUTS_NOMS = { ATTRIBUT_ID_SOUTENANCE, ATTRIBUT_DATE_SOUTENANCE,
			ATTRIBUT_ID_JURY_SOUTENANCE, ATTRIBUT_ID_EQUIPE };
	private static final String[] ATTRIBUTS_NOMS_ID_FIN = { ATTRIBUT_DATE_SOUTENANCE, ATTRIBUT_ID_JURY_SOUTENANCE,
			ATTRIBUT_ID_EQUIPE, ATTRIBUT_ID_SOUTENANCE };

	// tous les attributs du bean doivent être écrits dans la requête INSERT
	private static final String SQL_INSERT = "INSERT INTO Soutenance (dateSoutenance, idJurySoutenance, idEquipe) VALUES (?, ?, ?)";
	private static final String SQL_SELECT_TOUT = "SELECT * FROM Soutenance";
	private static final String SQL_SELECT_SUJET_SOUTENANCE = "SELECT sou.* FROM Soutenance sou, Equipe eq, Sujet su WHERE su.idSujet=eq.idSujet and eq.idEquipe=sou.idEquipe and su.idSujet=?";
	private static final String SQL_SELECT_SOUTENANCE_JURY_MANQUANT = "SELECT DISTINCT sou.* FROM Soutenance sou, jurysoutenance ju WHERE sou.idJurySoutenance IS NULL OR sou.idJurySoutenance = ju.idJurySoutenance AND (ju.idProf1 IS NULL OR ju.idProf2 IS NULL)";

	private static final String SQL_FORMAT_DATE = "yyyy-MM-dd HH:mm:ss";

	private static Logger logger = Logger.getLogger(SoutenanceDAO.class.getName());

	/**
	 * Constructeur de DAO.
	 * 
	 * @param daoFactory
	 *            la Factory permettant la création d'une connexion à la BDD.
	 */
	SoutenanceDAO(DAOFactory daoFactory) {
		super(daoFactory);
	}

	/**
	 * Insère une Soutenance dans la BDD à partir des attributs spécifiés dans un
	 * bean Soutenance.
	 * 
	 * @param soutenance
	 *            la Soutenance que l'on souhaite insérer dans la BDD à partir du
	 *            bean Soutenance.
	 */
	@Override
	public void creer(Soutenance soutenance) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			// les "?" de la requête sont comblés par les attributs de l'objet en paramètre
			// ceux-ci peuvent être nuls à condition que la BDD accepte les données de type
			// "null"
			preparedStatement = initialisationRequetePreparee(connection, SQL_INSERT, true,
					soutenance.getDateSoutenance(), soutenance.getIdJurySoutenance(), soutenance.getIdEquipe());
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec de la création de l'objet, aucune ligne ajoutée dans la table.", e);
		} finally {
			// fermeture des ressources utilisées
			fermetures(preparedStatement, connection);
		}
	}

	/**
	 * Liste toutes les Soutenances ayant pour attributs les mêmes que ceux
	 * spécifiés dans un bean Soutenance.
	 * 
	 * @param soutenance
	 *            la Soutenance que l'on souhaite trouver dans la BDD.
	 * @return soutenances la liste des Soutenances trouvées dans la BDD.
	 */
	@Override
	public List<Soutenance> trouver(Soutenance soutenance) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<Soutenance> soutenances = new ArrayList<>();

		String date = null;
		if (soutenance.getDateSoutenance() != null) {
			date = new SimpleDateFormat(SQL_FORMAT_DATE).format(soutenance.getDateSoutenance());
		}

		// tableau de Strings regroupant tous les noms des attributs d'un objet ainsi
		// que les valeurs correspondantes
		String[][] attributs = { ATTRIBUTS_NOMS, { String.valueOf(soutenance.getIdSoutenance()), date,
				soutenance.getIdJurySoutenance(), soutenance.getIdEquipe() } };
		try {
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			// mise en forme de la requête SELECT en fonction des attributs de l'objet sujet
			preparedStatement = initialisationRequetePreparee(connection, "SELECT", NOM_ENTITE, attributs, false);
			resultSet = preparedStatement.executeQuery();
			// récupération des valeurs des attributs de la BDD pour les mettre dans une
			// liste
			while (resultSet.next()) {
				soutenances.add(map(resultSet));
			}
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec de la recherche de l'objet.", e);
		} finally {
			// fermeture des ressources utilisées
			fermetures(resultSet, preparedStatement, connection);
		}
		return soutenances;
	}

	/**
	 * Modifie UNE Soutenance ayant pour attributs les mêmes que ceux spécifiés dans
	 * un bean Soutenance et la même clé primaire. Cette clé primaire ne peut être
	 * modifiée.
	 * 
	 * @param soutenance
	 *            la Soutenance que l'on souhaite modifier dans la BDD.
	 */
	@Override
	public void modifier(Soutenance soutenance) {
		// tableau de Strings regroupant tous les noms des attributs d'un objet ainsi
		// que les valeurs correspondantes
		// /!\ bien placer la clé primaire et sa valeur à la FIN du tableau sinon erreur
		String date = null;
		if (soutenance.getDateSoutenance() != null) {
			date = new SimpleDateFormat(SQL_FORMAT_DATE).format(soutenance.getDateSoutenance());
		}
		String[][] attributs = { ATTRIBUTS_NOMS_ID_FIN, { date, soutenance.getIdJurySoutenance(), soutenance.getIdEquipe(),
				String.valueOf(soutenance.getIdSoutenance()) } };

		/* Traite la mise à jour de la BDD */
		traitementUpdate(this.getDaoFactory(), NOM_ENTITE, attributs, logger);
	}

	/**
	 * Supprime toutes les Soutenances ayant pour attributs les mêmes que ceux
	 * spécifiés dans un bean Soutenance.
	 * 
	 * @param soutenance
	 *            la Soutenance que l'on souhaite supprimer dans la BDD.
	 */
	@Override
	public void supprimer(Soutenance soutenance) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;

		String date = null;
		if (soutenance.getDateSoutenance() != null) {
			date = new SimpleDateFormat(SQL_FORMAT_DATE).format(soutenance.getDateSoutenance());
		}

		// tableau de Strings regroupant tous les noms des attributs d'un objet ainsi
		// que les valeurs correspondantes
		String[][] attributs = { ATTRIBUTS_NOMS, { String.valueOf(soutenance.getIdSoutenance()), date,
				soutenance.getIdJurySoutenance(), soutenance.getIdEquipe() } };
		try {
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			preparedStatement = initialisationRequetePreparee(connection, "DELETE", NOM_ENTITE, attributs, false);
			int statut = preparedStatement.executeUpdate();
			if (statut == 0) {
				throw new SQLException();
			} else {
				// suppression du bean role
				soutenance.setIdSoutenance(null);
			}
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec de la suppression de l'objet, aucune ligne supprimée de la table.", e);
		} finally {
			// fermeture des ressources utilisées
			fermetures(preparedStatement, connection);
		}
	}

	/**
	 * Liste toutes les Soutenances présentes dans la BDD.
	 * 
	 * @return soutenances la liste des Soutenances présentes dans la BDD.
	 */
	@Override
	public List<Soutenance> lister() {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<Soutenance> soutenances = new ArrayList<>();
		try {
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			preparedStatement = connection.prepareStatement(SQL_SELECT_TOUT);
			resultSet = preparedStatement.executeQuery();
			// récupération des valeurs des attributs de la BDD pour les mettre dans une
			// liste
			while (resultSet.next()) {
				soutenances.add(map(resultSet));
			}
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec du listage des objets.", e);
		} finally {
			fermetures(resultSet, preparedStatement, connection);
		}
		return soutenances;
	}

	/**
	 * Renvoie la soutenance associée à un Sujet donné.
	 * 
	 * @param sujet
	 *            le sujet dont on souhaite connaître la soutenance associée.
	 * @return soutenance la soutenance associée au sujet.
	 */
	public Soutenance trouverSoutenanceSujet(Sujet sujet) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		Soutenance soutenance = new Soutenance();
		try {
			// vérification que le sujet rentré n'est pas nul
			if (sujet.getIdSujet() == null) {
				throw new SQLException();
			}
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			preparedStatement = connection.prepareStatement(SQL_SELECT_SUJET_SOUTENANCE, Statement.NO_GENERATED_KEYS);
			preparedStatement.setLong(1, sujet.getIdSujet());
			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				soutenance = map(resultSet);
			}
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec de la recherche d'une soutenance à partir d'un sujet.", e);
		} finally {
			// fermeture des ressources utilisées
			fermetures(resultSet, preparedStatement, connection);
		}
		return soutenance;
	}
	
	/**
	 * Liste toutes les Soutenances présentes dans la BDD.
	 * 
	 * @return soutenances la liste des Soutenances présentes dans la BDD.
	 */
	public List<Soutenance> listerSoutenancesJuryManquant() {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<Soutenance> soutenances = new ArrayList<>();
		try {
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			preparedStatement = connection.prepareStatement(SQL_SELECT_SOUTENANCE_JURY_MANQUANT);
			resultSet = preparedStatement.executeQuery();
			// récupération des valeurs des attributs de la BDD pour les mettre dans une
			// liste
			while (resultSet.next()) {
				soutenances.add(map(resultSet));
			}
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec du listage des objets.", e);
		} finally {
			fermetures(resultSet, preparedStatement, connection);
		}
		return soutenances;
	}


	/**
	 * Fait la correspondance (le mapping) entre une ligne issue de la table
	 * Soutenance (un ResultSet) et un bean Soutenance.
	 * 
	 * @param resultSet
	 *            la ligne issue de la table Soutenance.
	 * @return soutenance le bean dont on souhaite faire la correspondance.
	 * @throws SQLException
	 */
	public static Soutenance map(ResultSet resultSet) throws SQLException {
		Soutenance soutenance = new Soutenance();
		soutenance.setIdSoutenance(resultSet.getLong(ATTRIBUT_ID_SOUTENANCE));
		soutenance.setDateSoutenance(resultSet.getTimestamp(ATTRIBUT_DATE_SOUTENANCE));
		soutenance.setIdJurySoutenance(resultSet.getString(ATTRIBUT_ID_JURY_SOUTENANCE));
		soutenance.setIdEquipe(resultSet.getString(ATTRIBUT_ID_EQUIPE));
		return soutenance;
	}

}