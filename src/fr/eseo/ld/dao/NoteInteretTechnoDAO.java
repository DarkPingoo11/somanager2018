package fr.eseo.ld.dao;

import fr.eseo.ld.beans.NoteInteretTechno;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static fr.eseo.ld.dao.DAOUtilitaire.fermetures;
import static fr.eseo.ld.dao.DAOUtilitaire.initialisationRequetePreparee;

/**
 * Classe du DAO du bean NoteInteretTechno.
 * 
 * <p>
 * Cette classe implémente toutes les méthodes CRUD définies dans sa classe
 * mère.
 * </p>
 * 
 * @author Maxime LENORMAND
 */
public class NoteInteretTechnoDAO extends DAO<NoteInteretTechno> {

	private static final String NOM_ENTITE = "NoteInteretTechno";
	private static final String[] ATTRIBUTS_NOMS = { DAOUtilitaire.ATTRIBUT_ID_PROFESSEUR,
			DAOUtilitaire.ATTRIBUT_ID_SUJET, DAOUtilitaire.ATTRIBUT_NOTE };
	private static final String[] ATTRIBUTS_NOMS_ID_FIN = { DAOUtilitaire.ATTRIBUT_NOTE,
			DAOUtilitaire.ATTRIBUT_ID_PROFESSEUR, DAOUtilitaire.ATTRIBUT_ID_SUJET };

	// tous les attributs du bean doivent être écrits dans la requête INSERT
	private static final String SQL_INSERT = "INSERT INTO NoteInteretTechno (idProfesseur, idSujet, note) VALUES (?, ?, ?)";
	private static final String SQL_SELECT_TOUT = "SELECT * FROM NoteInteretTechno";

	private static Logger logger = Logger.getLogger(NoteInteretTechnoDAO.class.getName());

	/**
	 * Constructeur de DAO.
	 * 
	 * @param daoFactory
	 *            la Factory permettant la création d'une connexion à la BDD.
	 */
	NoteInteretTechnoDAO(DAOFactory daoFactory) {
		super(daoFactory);
	}

	/**
	 * Insère une NoteInteretTechno dans la BDD à partir des attributs spécifiés
	 * dans un bean NoteInteretTechno.
	 * 
	 * @param noteInteretTechno
	 *            la NoteInteretTechno que l'on souhaite insérer dans la BDD à
	 *            partir du bean NoteInteretTechno.
	 */
	@Override
	public void creer(NoteInteretTechno noteInteretTechno) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			// les "?" de la requête sont comblés par les attributs de l'objet en paramètre
			// ceux-ci peuvent être nuls à condition que la BDD accepte les données de type
			// "null"
			preparedStatement = initialisationRequetePreparee(connection, SQL_INSERT, true,
					noteInteretTechno.getIdProfesseur(), noteInteretTechno.getIdSujet(), noteInteretTechno.getNote());
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec de la création de l'objet, aucune ligne ajoutée dans la table.", e);
		} finally {
			// fermeture des ressources utilisées
			fermetures(preparedStatement, connection);
		}
	}

	/**
	 * Liste toutes les NoteInteretTechno ayant pour attributs les mêmes que ceux
	 * spécifiés dans un bean NoteInteretTechno.
	 * 
	 * @param noteInteretTechno
	 *            la NoteInteretTechno que l'on souhaite trouver dans la BDD.
	 * @return noteInteretTechnos la liste des NoteInteretTechno trouvées dans la
	 *         BDD.
	 */
	@Override
	public List<NoteInteretTechno> trouver(NoteInteretTechno noteInteretTechno) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<NoteInteretTechno> notesInteretTechnos = new ArrayList<>();
		// tableau de Strings regroupant tous les noms des attributs d'un objet ainsi
		// que les valeurs correspondantes
		String[][] attributs = { ATTRIBUTS_NOMS, { String.valueOf(noteInteretTechno.getIdProfesseur()),
				String.valueOf(noteInteretTechno.getIdSujet()), String.valueOf(noteInteretTechno.getNote()) } };
		try {
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			// mise en forme de la requête SELECT en fonction des attributs de l'objet
			preparedStatement = initialisationRequetePreparee(connection, "SELECT", NOM_ENTITE, attributs, false);
			resultSet = preparedStatement.executeQuery();
			// récupération des valeurs des attributs de la BDD pour les mettre dans une
			// liste
			while (resultSet.next()) {
				notesInteretTechnos.add(map(resultSet));
			}
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec de la recherche de l'objet.", e);
		} finally {
			// fermeture des ressources utilisées
			fermetures(resultSet, preparedStatement, connection);
		}
		return notesInteretTechnos;
	}

	/**
	 * Modifie UNE NoteInteretTechno ayant pour attributs les mêmes que ceux
	 * spécifiés dans un bean NoteInteretTechno et la même clé primaire. Cette clé
	 * primaire ne peut être modifiée.
	 * 
	 * @param noteInteretTechno
	 *            la NoteInteretTechno que l'on souhaite modifier dans la BDD.
	 */
	@Override
	public void modifier(NoteInteretTechno noteInteretTechno) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		// tableau de Strings regroupant tous les noms des attributs d'un objet ainsi
		// que les valeurs correspondantes
		// /!\ bien placer la clé primaire et sa valeur à la FIN du tableau sinon erreur
		String[][] attributs = { ATTRIBUTS_NOMS_ID_FIN, { String.valueOf(noteInteretTechno.getNote()),
				String.valueOf(noteInteretTechno.getIdProfesseur()), String.valueOf(noteInteretTechno.getIdSujet()) } };
		try {
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			// mise en forme de la requête UPDATE en fonction des attributs de l'objet
			// noteInteretTechno
			preparedStatement = initialisationRequetePreparee(connection, "UPDATE", NOM_ENTITE, attributs, "Professeur",
					"Sujet", false);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec de la mise à jour de l'objet, aucune ligne modifée dans la table.", e);
		} finally {
			// fermeture des ressources utilisées
			fermetures(preparedStatement, connection);
		}
	}

	/**
	 * Supprime toutes les NoteInteretTechno ayant pour attributs les mêmes que ceux
	 * spécifiés dans un bean NoteInteretTechno.
	 * 
	 * @param noteInteretTechno
	 *            la NoteInteretTechno que l'on souhaite supprimer dans la BDD.
	 */
	@Override
	public void supprimer(NoteInteretTechno noteInteretTechno) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		// tableau de Strings regroupant tous les noms des attributs d'un objet ainsi
		// que les valeurs correspondantes
		String[][] attributs = { ATTRIBUTS_NOMS, { String.valueOf(noteInteretTechno.getIdProfesseur()),
				String.valueOf(noteInteretTechno.getIdSujet()), String.valueOf(noteInteretTechno.getNote()) } };
		try {
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			preparedStatement = initialisationRequetePreparee(connection, "DELETE", NOM_ENTITE, attributs, false);
			int statut = preparedStatement.executeUpdate();
			if (statut == 0) {
				throw new SQLException();
			} else {
				// suppression du bean
				noteInteretTechno.setIdProfesseur(null);
				noteInteretTechno.setIdSujet(null);
			}
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec de la suppression de l'objet, aucune ligne supprimée de la table.", e);
		} finally {
			// fermeture des ressources utilisées
			fermetures(preparedStatement, connection);
		}
	}

	/**
	 * Liste toutes les NoteInteretTechno présentes dans la BDD.
	 * 
	 * @return noteInteretTechnos la liste des NoteInteretTechnos présentes dans la
	 *         BDD.
	 */
	@Override
	public List<NoteInteretTechno> lister() {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<NoteInteretTechno> notesInteretTechnos = new ArrayList<>();
		try {
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			preparedStatement = connection.prepareStatement(SQL_SELECT_TOUT);
			resultSet = preparedStatement.executeQuery();
			// récupération des valeurs des attributs de la BDD pour les mettre dans une
			// liste
			while (resultSet.next()) {
				notesInteretTechnos.add(map(resultSet));
			}
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec du listage des objets.", e);
		} finally {
			fermetures(resultSet, preparedStatement, connection);
		}
		return notesInteretTechnos;
	}

	/**
	 * Fait la correspondance (le mapping) entre une ligne issue de la table
	 * NoteInteretTechno (un ResultSet) et un bean NoteInteretTechno.
	 * 
	 * @param resultSet
	 *            la ligne issue de la table NoteInteretTechno.
	 * @return noteInteretTechno le bean dont on souhaite faire la correspondance.
	 * @throws SQLException
	 */
	public static NoteInteretTechno map(ResultSet resultSet) throws SQLException {
		NoteInteretTechno noteInteretTechno = new NoteInteretTechno();
		noteInteretTechno.setIdProfesseur(resultSet.getLong(DAOUtilitaire.ATTRIBUT_ID_PROFESSEUR));
		noteInteretTechno.setIdSujet(resultSet.getLong(DAOUtilitaire.ATTRIBUT_ID_SUJET));
		noteInteretTechno.setNote(resultSet.getInt(DAOUtilitaire.ATTRIBUT_NOTE));
		return noteInteretTechno;
	}

}