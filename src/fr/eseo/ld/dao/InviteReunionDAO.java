package fr.eseo.ld.dao;

import fr.eseo.ld.beans.Invite;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static fr.eseo.ld.dao.DAOUtilitaire.fermetures;
import static fr.eseo.ld.dao.DAOUtilitaire.initialisationRequetePreparee;

/**
 * Classe du DAO du bean Invite.
 *
 * <p>
 * Cette classe implémente toutes les méthodes CRUD définies dans sa classe mère
 * </p>
 */
public class InviteReunionDAO extends DAO<Invite> {


    private static final String NOM_ENTITE = "Invite_Reunion";
    private static final String ATTRIBUT_ID_REUNION = "refReunion";
    private static final String ATTRIBUT_ID_ETUDIANT = "refUtilisateur";
    private static final String ATTRIBUT_PARTICIPE = "participe";
    private static final String[] ATTRIBUTS_NOMS = {ATTRIBUT_ID_REUNION, ATTRIBUT_ID_ETUDIANT,
            ATTRIBUT_PARTICIPE};


    // tous les attributs du bean doivent être écrits dans la requête INSERT
    private static final String SQL_INSERT = "INSERT INTO Invite_Reunion (participe, refReunion, refUtilisateur) VALUES (?, ?, ?)";
    private static final String SQL_SELECT_TOUT = "SELECT * FROM Invite_Reunion";

    private static Logger logger = Logger.getLogger(InviteReunionDAO.class.getName());


    /**
     * Constructeur de DAO.
     *
     * @param daoFactory la Factory permettant la création d'une connexion à la BDD.
     */
    InviteReunionDAO(DAOFactory daoFactory) {
        super(daoFactory);
    }


    /**
     * Insère un Invite dans la BDD à partir des attributs spécifiés dans
     * un bean invite.
     *
     * @param invite l'Invite l'on souhaite insérer dans la BDD à partir
     *               du bean Invite.
     */
    @Override
    public void creer(Invite invite) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            // création d'une connexion grâce à la DAOFactory placée en attribut de la
            // classe
            connection = this.creerConnexion();
            // les "?" de la requête sont comblés par les attributs de l'objet en paramètre
            // ceux-ci peuvent être nuls à condition que la BDD accepte les données de type
            // "null"
            preparedStatement = initialisationRequetePreparee(connection, SQL_INSERT, true,invite.getParticipe(),
                    invite.getRefReunion(), invite.getRefUtilisateur());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            logger.log(Level.WARN, "Échec de la création de l'objet, aucune ligne ajoutée dans la table.", e);
        } finally {
            // fermeture des ressources utilisées
            fermetures(preparedStatement, connection);
        }
    }

    /**
     * Liste tous les Invite ayant pour attributs les mêmes que ceux
     * spécifiés dans un bean Invite.
     *
     * @param invite l'invite que l'on souhaite trouver dans la BDD.
     * @return invites la liste des Invites trouvés dans la BDD.
     */
    @Override
    public List<Invite> trouver(Invite invite) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        List<Invite> invites = new ArrayList<>();
        // tableau de Strings regroupant tous les noms des attributs d'un objet ainsi
        // que les valeurs correspondantes
        String[][] attributs = {ATTRIBUTS_NOMS,
                {String.valueOf(invite.getRefReunion()), String.valueOf(invite.getRefUtilisateur()),
                        String.valueOf(invite.getParticipe())}};
        try {
            // création d'une connexion grâce à la DAOFactory placée en attribut de la
            // classe
            connection = this.creerConnexion();
            // mise en forme de la requête SELECT en fonction des attributs de l'objet
            preparedStatement = initialisationRequetePreparee(connection, "SELECT", NOM_ENTITE, attributs, false);
            resultSet = preparedStatement.executeQuery();
            // récupération des valeurs des attributs de la BDD pour les mettre dans une
            // liste
            while (resultSet.next()) {
                invites.add(map(resultSet));
            }
        } catch (SQLException e) {
            logger.log(Level.WARN, "Échec de la recherche de l'objet. ", e);
        } finally {
            // fermeture des ressources utilisées
            fermetures(resultSet, preparedStatement, connection);
        }
        return invites;
    }

    /**
     * Modifie un Invite ayant pour attributs les mêmes que ceux spécifiés
     * dans un bean Invite et la même clé primaire. Cette clé primaire ne
     * peut être modifiée.
     *
     * @param invite l' invite que l'on souhaite modifier dans la BDD.
     */
    @Override
    public void modifier(Invite invite) {
        //Update table SET <....>
        Map<String, Object> attributs = new HashMap<>();
        attributs.put(ATTRIBUT_PARTICIPE, invite.getParticipe());
        attributs.put(ATTRIBUT_ID_ETUDIANT, invite.getRefUtilisateur());

        //Preparation de WHERE
        Map<String, Object> whereAtt = new HashMap<>();
        whereAtt.put(ATTRIBUT_ID_REUNION, invite.getRefReunion());

        //Modifier
        DAOUtilitaire.executeUpdate(this.getDaoFactory(), NOM_ENTITE, attributs, whereAtt);
    }

    /**
     * Supprime tous les Invite ayant pour attributs les mêmes que ceux
     * spécifiés dans un bean NoteSoutenance.
     *
     * @param invite l'Invite que l'on souhaite supprimer dans la BDD.
     */
    @Override
    public void supprimer(Invite invite) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        // tableau de Strings regroupant tous les noms des attributs d'un objet ainsi
        // que les valeurs correspondantes
        String[][] attributs = {ATTRIBUTS_NOMS,
                {String.valueOf(invite.getRefReunion()), String.valueOf(invite.getRefUtilisateur()),
                        String.valueOf(invite.getParticipe())}};
        try {
            // création d'une connexion grâce à la DAOFactory placée en attribut de la
            // classe
            connection = this.creerConnexion();
            preparedStatement = initialisationRequetePreparee(connection, "DELETE", NOM_ENTITE, attributs, false);
            int statut = preparedStatement.executeUpdate();
            if (statut == 0) {
                throw new SQLException();
            } else {
                // suppression du bean
                invite.setRefReunion(null);
                invite.setRefUtilisateur(null);
                invite.setParticipe(null);
            }
        } catch (SQLException e) {
            logger.log(Level.WARN, "Échec de la suppression de l'objet, aucune ligne supprimée de la table.", e);
        } finally {
            // fermeture des ressources utilisées
            fermetures(preparedStatement, connection);
        }
    }

    /**
     * Liste tous les Invites présents dans la BDD.
     *
     * @return invites la liste des Invite présents dans la BDD.
     */
    @Override
    public List<Invite> lister() {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        List<Invite> invites = new ArrayList<>();
        try {
            // création d'une connexion grâce à la DAOFactory placée en attribut de la
            // classe
            connection = this.creerConnexion();
            preparedStatement = connection.prepareStatement(SQL_SELECT_TOUT);
            resultSet = preparedStatement.executeQuery();
            // récupération des valeurs des attributs de la BDD pour les mettre dans une
            // liste
            while (resultSet.next()) {
                invites.add(map(resultSet));
            }
        } catch (SQLException e) {
            logger.log(Level.WARN, "Échec du listage des invites.", e);
        } finally {
            fermetures(resultSet, preparedStatement, connection);
        }
        return invites;
    }




    // #################################################
    // # Méthodes privées #
    // #################################################

    /**
     * Fait la correspondance (le mapping) entre une ligne issue de la table
     * Invite (un ResultSet) et un bean Invite.
     *
     * @param resultSet la ligne issue de la table Invite.
     * @return invite le bean dont on souhaite faire la correspondance.
     * @throws SQLException
     */
    public static Invite map(ResultSet resultSet) throws SQLException {
        Invite invite = new Invite();
        invite.setRefReunion(resultSet.getLong(ATTRIBUT_ID_REUNION));
        invite.setRefUtilisateur(resultSet.getLong(ATTRIBUT_ID_ETUDIANT));
        invite.setParticipe(resultSet.getString(ATTRIBUT_PARTICIPE));
        return invite;
    }
}
