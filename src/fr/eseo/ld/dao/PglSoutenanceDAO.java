package fr.eseo.ld.dao;

import fr.eseo.ld.pgl.beans.Soutenance;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static fr.eseo.ld.dao.DAOUtilitaire.fermetures;
import static fr.eseo.ld.dao.DAOUtilitaire.initialisationRequetePreparee;

/**
 * Classe du DAO du bean Soutenance (Pgl).
 *
 * <p>
 * Cette classe implémente toutes les méthodes CRUD définies dans sa classe
 * mère.
 * </p>
 */
public class PglSoutenanceDAO extends DAO<Soutenance> {
    private static final String NOM_ENTITE = "pgl_soutenance";
    private static final String ATTRIBUT_ID_SOUTENANCE = "idSoutenance";
    private static final String ATTRIBUT_DATE_SOUTENANCE = "date";
    private static final String ATTRIBUT_LIBELLE = "libelle";
    private static final String[] ATTRIBUTS_NOMS = {ATTRIBUT_ID_SOUTENANCE, ATTRIBUT_DATE_SOUTENANCE, ATTRIBUT_LIBELLE};

    // Tous les attributs du bean doivent être écrits dans la requête INSERT
    private static final String SQL_INSERT = "INSERT INTO pgl_soutenance (date, libelle) VALUES (?, ?)";
    private static final String SQL_SELECT_TOUT = "SELECT * FROM pgl_soutenance";

    private static Logger logger = Logger.getLogger(PglSoutenanceDAO.class.getName());

    /**
     * Constructeur de DAO.
     *
     * @param daoFactory la Factory permettant la création d'une connexion à la BDD.
     */
    PglSoutenanceDAO(DAOFactory daoFactory) {
        super(daoFactory);
    }

    /**
     * Insère une Soutenance dans la BDD à partir des attributs spécifiés dans un
     * bean Soutenance.
     *
     * @param soutenance la Soutenance que l'on souhaite insérer dans la BDD à partir du
     *                   bean Soutenance.
     */
    @Override
    public void creer(Soutenance soutenance) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            // création d'une connexion grâce à la DAOFactory placée en attribut de la
            // classe
            connection = this.creerConnexion();
            // les "?" de la requête sont comblés par les attributs de l'objet en paramètre
            // ceux-ci peuvent être nuls à condition que la BDD accepte les données de type
            // "null"
            preparedStatement = initialisationRequetePreparee(connection, SQL_INSERT, true,
                    soutenance.getDate(), soutenance.getLibelle());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            logger.log(Level.WARN, "Échec de la création de l'objet, aucune ligne ajoutée dans la table.", e);
        } finally {
            // fermeture des ressources utilisées
            fermetures(preparedStatement, connection);
        }
    }

    /**
     * Liste toutes les Soutenances ayant pour attributs les mêmes que ceux
     * spécifiés dans un bean Soutenance.
     *
     * @param soutenance la Soutenance que l'on souhaite trouver dans la BDD.
     * @return soutenances la liste des Soutenances trouvées dans la BDD.
     */
    @Override
    public List<Soutenance> trouver(Soutenance soutenance) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        List<Soutenance> soutenances = new ArrayList<>();

        String date = soutenance.getDate();

        // tableau de Strings regroupant tous les noms des attributs d'un objet ainsi
        // que les valeurs correspondantes
        String[][] attributs = {ATTRIBUTS_NOMS, {String.valueOf(soutenance.getIdSoutenance()), date, soutenance.getLibelle()}};
        try {
            // création d'une connexion grâce à la DAOFactory placée en attribut de la
            // classe
            connection = this.creerConnexion();
            // mise en forme de la requête SELECT en fonction des attributs de l'objet sujet
            preparedStatement = initialisationRequetePreparee(connection, "SELECT", NOM_ENTITE, attributs, false);
            resultSet = preparedStatement.executeQuery();
            // récupération des valeurs des attributs de la BDD pour les mettre dans une
            // liste
            while (resultSet.next()) {
                soutenances.add(map(resultSet));
            }
        } catch (SQLException e) {
            logger.log(Level.WARN, "Échec de la recherche de l'objet.", e);
        } finally {
            // fermeture des ressources utilisées
            fermetures(resultSet, preparedStatement, connection);
        }
        return soutenances;
    }

    /**
     * Modifie UNE Soutenance ayant pour attributs les mêmes que ceux spécifiés dans
     * un bean Soutenance et la même clé primaire. Cette clé primaire ne peut être
     * modifiée.
     *
     * @param soutenance la Soutenance que l'on souhaite modifier dans la BDD.
     */
    @Override
    public void modifier(Soutenance soutenance) {
        //Update table SET <....>
        Map<String, Object> attributs = new HashMap<>();
        attributs.put(ATTRIBUT_DATE_SOUTENANCE, soutenance.getDate());
        attributs.put(ATTRIBUT_LIBELLE, soutenance.getLibelle());

        //Preparation de WHERE
        Map<String, Object> whereAtt = new HashMap<>();
        whereAtt.put(ATTRIBUT_ID_SOUTENANCE, soutenance.getIdSoutenance());

        //Modifier
        DAOUtilitaire.executeUpdate(this.getDaoFactory(), NOM_ENTITE, attributs, whereAtt);
    }

    /**
     * Supprime toutes les Soutenances ayant pour attributs les mêmes que ceux
     * spécifiés dans un bean Soutenance.
     *
     * @param soutenance la Soutenance que l'on souhaite supprimer dans la BDD.
     */
    @Override
    public void supprimer(Soutenance soutenance) {
        Map<String, Object> identifiants = new HashMap<>();
        identifiants.put(ATTRIBUT_ID_SOUTENANCE, soutenance.getIdSoutenance());

        DAOUtilitaire.executeDelete(this.getDaoFactory(), NOM_ENTITE, identifiants);
    }

    /**
     * Liste toutes les Soutenances présentes dans la BDD.
     *
     * @return soutenances la liste des Soutenances présentes dans la BDD.
     */
    @Override
    public List<Soutenance> lister() {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        List<Soutenance> soutenances = new ArrayList<>();
        try {
            // création d'une connexion grâce à la DAOFactory placée en attribut de la
            // classe
            connection = this.creerConnexion();
            preparedStatement = connection.prepareStatement(SQL_SELECT_TOUT);
            resultSet = preparedStatement.executeQuery();
            // récupération des valeurs des attributs de la BDD pour les mettre dans une
            // liste
            while (resultSet.next()) {
                soutenances.add(map(resultSet));
            }
        } catch (SQLException e) {
            logger.log(Level.WARN, "Échec du listage des objets.", e);
        } finally {
            fermetures(resultSet, preparedStatement, connection);
        }
        return soutenances;
    }


    /**
     * Fait la correspondance (le mapping) entre une ligne issue de la table
     * Soutenance (un ResultSet) et un bean Soutenance.
     *
     * @param resultSet la ligne issue de la table Soutenance.
     * @return soutenance le bean dont on souhaite faire la correspondance.
     * @throws SQLException
     */
    public static Soutenance map(ResultSet resultSet) throws SQLException {
        Soutenance soutenance = new Soutenance();
        soutenance.setIdSoutenance(resultSet.getLong(ATTRIBUT_ID_SOUTENANCE));
        soutenance.setDate(resultSet.getTimestamp(ATTRIBUT_DATE_SOUTENANCE).toString());
        soutenance.setLibelle(resultSet.getString(ATTRIBUT_LIBELLE));
        return soutenance;
    }


}
