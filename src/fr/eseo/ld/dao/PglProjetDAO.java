package fr.eseo.ld.dao;

import fr.eseo.ld.pgl.beans.Projet;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static fr.eseo.ld.dao.DAOUtilitaire.fermetures;
import static fr.eseo.ld.dao.DAOUtilitaire.initialisationRequetePreparee;

/**
 * Classe du DAO du bean Projet (Pgl).
 *
 * <p>
 * Cette classe implémente toutes les méthodes CRUD définies dans sa classe
 * mère.
 * </p>
 */
public class PglProjetDAO extends DAO<Projet> {
    /* Noms des entités */
    private static final String NOM_ENTITE = "pgl_projet";

    /* Attributs de l'entité Note */
    private static final String ATTRIBUT_ID_PROJET = "idProjet";
    private static final String ATTRIBUT_TITRE = "titre";
    private static final String ATTRIBUT_DESCRIPTION = "description";
    private static final String ATTRIBUT_ID_PROF_REFERENT = "idProfReferent";

    private static final String[] ATTRIBUTS_NOMS = {ATTRIBUT_ID_PROJET, ATTRIBUT_TITRE, ATTRIBUT_DESCRIPTION,
            ATTRIBUT_ID_PROF_REFERENT};

    private static final String SQL_SELECT_TOUT = "SELECT * FROM pgl_projet";

    private static Logger logger = Logger.getLogger(PglProjetDAO.class.getName());

    /**
     * Constructeur de DAO.
     *
     * @param daoFactory la Factory permettant la création d'une connexion à la BDD.
     */
    PglProjetDAO(DAOFactory daoFactory) {
        super(daoFactory);
    }

    /**
     * Insère un Projet dans la BDD à partir des attributs spécifiés dans un bean
     * Projet.
     *
     * @param projet le projet que l'on souhaite insérer dans la BDD à partir du bean
     *               projet.
     */
    @Override
    public void creer(Projet projet) {


        Map<String, Object> attributs = new HashMap<>();
        attributs.put(ATTRIBUT_TITRE, projet.getTitre());
        attributs.put(ATTRIBUT_DESCRIPTION, projet.getDescription());
        attributs.put(ATTRIBUT_ID_PROF_REFERENT, projet.getIdProfReferent());

        DAOUtilitaire.executeCreer(this.getDaoFactory(), NOM_ENTITE, attributs);
    }

    /**
     * Liste tous les Projets ayant pour attributs les mêmes que ceux spécifiés dans
     * un bean Projet.
     *
     * @param projet le projet que l'on souhaite trouver dans la BDD.
     * @return projet la liste des projets trouvés dans la BDD.
     */
    @Override
    public List<Projet> trouver(Projet projet) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        List<Projet> projets = new ArrayList<>();
        // tableau de Strings regroupant tous les noms des attributs d'un objet ainsi
        // que les valeurs correspondantes
        String[][] attributs = {ATTRIBUTS_NOMS,
                {String.valueOf(projet.getIdProjet()), projet.getTitre(), projet.getDescription(),
                        String.valueOf(projet.getIdProfReferent())
                }};
        try {
            // création d'une connexion grâce à la DAOFactory placée en attribut de la classe
            connection = this.creerConnexion();
            // mise en forme de la requête SELECT en fonction des attributs de l'objet sujet
            preparedStatement = initialisationRequetePreparee(connection, "SELECT", NOM_ENTITE, attributs, false);
            resultSet = preparedStatement.executeQuery();
            // récupération des valeurs des attributs de la BDD pour les mettre dans une liste
            while (resultSet.next()) {
                projets.add(map(resultSet));
            }
        } catch (SQLException e) {
            logger.log(Level.WARN, "Échec de la recherche de l'objet Projet. ", e);
        } finally {
            fermetures(resultSet, preparedStatement, connection);
        }
        return projets;
    }

    /**
     * Modifie un Projet ayant pour attributs les mêmes que ceux spécifiés dans un
     * bean Projet et la même clé primaire. Cette clé primaire ne peut être
     * modifiée.
     *
     * @param projet la Projet que l'on souhaite modifier dans la BDD.
     */
    @Override
    public void modifier(Projet projet) {
//Update table SET <....>
        Map<String, Object> attributs = new HashMap<>();
        attributs.put(ATTRIBUT_TITRE, projet.getTitre());
        attributs.put(ATTRIBUT_DESCRIPTION, projet.getDescription());
        attributs.put(ATTRIBUT_ID_PROF_REFERENT, projet.getIdProfReferent());

        //Preparation de WHERE
        Map<String, Object> whereAtt = new HashMap<>();
        whereAtt.put(ATTRIBUT_ID_PROJET, projet.getIdProjet());

        //Modifier
        DAOUtilitaire.executeUpdate(this.getDaoFactory(), NOM_ENTITE, attributs, whereAtt);
    }

    /**
     * Supprime tous les Projets ayant pour attributs les mêmes que ceux spécifiés
     * dans un bean Projet.
     *
     * @param projet le Projet que l'on souhaite supprimer dans la BDD.
     */
    @Override
    public void supprimer(Projet projet) {

        Map<String, Object> identifiants = new HashMap<>();
        identifiants.put(ATTRIBUT_ID_PROJET, projet.getIdProjet());

        DAOUtilitaire.executeDelete(this.getDaoFactory(), NOM_ENTITE, identifiants);
    }

    /**
     * Liste tous les Projets présents dans la BDD.
     *
     * @return projets la liste des Projets présents dans la BDD.
     */
    @Override
    public List<Projet> lister() {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        List<Projet> projets = new ArrayList<>();
        try {
            // création d'une connexion grâce à la DAOFactory placée en attribut de la
            // classe
            connection = this.creerConnexion();
            preparedStatement = connection.prepareStatement(SQL_SELECT_TOUT);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                projets.add(map(resultSet));
            }
            // récupération des valeurs des attributs de la BDD pour les mettre dans une
            // liste
        } catch (SQLException e) {
            logger.log(Level.WARN, "Échec du listage des objets projets. ", e);
        } finally {
            fermetures(resultSet, preparedStatement, connection);
        }
        return projets;
    }


    // #################################################
    // # Méthodes privées #
    // #################################################

    /**
     * Fait la correspondance (le mapping) entre une ligne issue de la table Projet
     * (un ResultSet) et un bean Projet.
     *
     * @param resultSet la ligne issue de la table Projet.
     * @return Projet le bean dont on souhaite faire la correspondance.
     */
    public static Projet map(ResultSet resultSet) throws SQLException {
        Projet projet = new Projet();
        projet.setIdProjet(resultSet.getLong(ATTRIBUT_ID_PROJET));
        projet.setTitre(resultSet.getString(ATTRIBUT_TITRE));
        projet.setDescription(resultSet.getString(ATTRIBUT_DESCRIPTION));
        projet.setIdProfReferent(resultSet.getLong(ATTRIBUT_ID_PROF_REFERENT));
        return projet;
    }

}
