package fr.eseo.ld.dao;

import fr.eseo.ld.beans.ModeleExcel;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static fr.eseo.ld.dao.DAOUtilitaire.fermetures;
import static fr.eseo.ld.dao.DAOUtilitaire.initialisationRequetePreparee;

/**
 * Classe du DAO du bean ModeleExcel.
 *
 * <p>
 * Cette classe implémente toutes les méthodes CRUD définies dans sa classe
 * mère.
 * </p>
 *
 * @author Tristan LE GACQUE
 */
public class ModeleExcelDAO extends DAO<ModeleExcel> {

	private static final String SQL_SELECT_TOUT = "SELECT * FROM ModeleExcel";
	private static final String SQL_INSERT_MODELE = "INSERT INTO modeleexcel(nomFichier, ligneHeader, colId, colNom, " +
			"colNote, colCom, colMoyenne, colGrade) " +
			"VALUES (?, ?, ?, ?, ?, ?, ?, ?)";

	private static Logger logger = Logger.getLogger(ModeleExcelDAO.class.getName());

	private static final String ATTRIBUT_NOM_FICHIER = "nomFichier";
	private static final String ATTRIBUT_LIGNEHEADER = "ligneHeader";
	private static final String ATTRIBUT_COLID = "colId";
	private static final String ATTRIBUT_COLNOM = "colNom";
	private static final String ATTRIBUT_COLNOTE = "colNote";
	private static final String ATTRIBUT_COLCOM = "colCom";
	private static final String ATTRIBUT_COLMOYENNE = "colMoyenne";
	private static final String ATTRIBUT_COLGRADE = "colGrade";
	private static final String NOM_ENTITE = "ModeleExcel";

	private static final String SQL_SELECT = "SELECT * FROM ModelEexcel WHERE nomFichier = ?";


	/**
	 * Constructeur de DAO.
	 *
	 * @param daoFactory
	 *            la Factory permettant la création d'une connexion à la BDD.
	 */
	public ModeleExcelDAO(DAOFactory daoFactory) {
		super(daoFactory);
	}
	// ########################################################################################################
	// # Méthodes CRUD concernant le bean Parametre #
	// ########################################################################################################

	/**
	 * Insère un ModeleExcel dans la BDD à partir des attributs spécifiés dans un bean
	 * Parametre.
	 *
	 * @param modele
	 *            le parametre que l'on souhaite insérer dans la BDD à partir du
	 *            bean ModeleExcel.
	 */
	@Override
	public void creer(ModeleExcel modele) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			// les "?" de la requête sont comblés par les attributs de l'objet en paramètre
			// ceux-ci peuvent être nuls à condition que la BDD accepte les données de type
			// "null"
			preparedStatement = initialisationRequetePreparee(connection, SQL_INSERT_MODELE, true, modele.getNomFichier(),
					modele.getLigneHeader(), modele.getColId(), modele.getColNom(), modele.getColNote(), modele.getColCom(),
					modele.getColMoyenne(), modele.getColGrade());
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec de la création de l'objet, aucune ligne ajoutée dans la table.", e);
		} finally {
			// fermeture des ressources utilisées
			fermetures(preparedStatement, connection);
		}
	}


	/**
	 * Liste tous les modelesExcel ayant pour attributs les mêmes que ceux spécifiés
	 * dans un bean ModeleExcel.
	 *
	 * @param modele
	 *            le modele que l'on souhaite trouver dans la BDD.
	 * @return parametres la liste des modeles trouvés dans la BDD.
	 */
	@Override
	public List<ModeleExcel> trouver(ModeleExcel modele) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<ModeleExcel> modeles = new ArrayList<>();

		try {
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.getDaoFactory().getConnection();
			// mise en forme de la requête
			preparedStatement = connection.prepareStatement(SQL_SELECT, Statement.NO_GENERATED_KEYS);
			preparedStatement.setString(1, modele.getNomFichier());
			resultSet = preparedStatement.executeQuery();
			// récupération des valeurs des attributs de la BDD pour les mettre dans une
			// liste
			while (resultSet.next()) {
				modeles.add(map(resultSet));
			}
		} catch (SQLException e) {
			logger.log(Level.FATAL, "Échec de la recherche de l'objet.", e);
		} finally {
			// fermeture des ressources utilisées
			fermetures(resultSet, preparedStatement, connection);
		}

		return modeles;
	}

	/**
	 * Modifie UN parametre ayant pour attributs les mêmes que ceux spécifiés dans
	 * un bean Parametre et la même clé primaire. Cette clé primaire ne peut être
	 * modifiée.
	 *
	 * @param modele
	 *            le Parametre que l'on souhaite modifier dans la BDD.
	 */
	@Override
	public void modifier(ModeleExcel modele) {
		/* Traite la mise à jour de la BDD */
        Map<String, Object> attributs = new HashMap<>();
		Map<String, Object> whereValues = new HashMap<>();

        attributs.put(ATTRIBUT_LIGNEHEADER, modele.getLigneHeader());
        attributs.put(ATTRIBUT_COLNOM, modele.getColNom());
        attributs.put(ATTRIBUT_COLID, modele.getColId());
        attributs.put(ATTRIBUT_COLNOTE, modele.getColNote());
        attributs.put(ATTRIBUT_COLCOM, modele.getColCom());
        attributs.put(ATTRIBUT_COLGRADE, modele.getColGrade());
        attributs.put(ATTRIBUT_COLMOYENNE, modele.getColMoyenne());

		whereValues.put(ATTRIBUT_NOM_FICHIER, modele.getNomFichier());

		DAOUtilitaire.executeUpdate(this.getDaoFactory(), NOM_ENTITE, attributs, whereValues);
	}

	/**
	 * Supprime tous les Modeles ayant pour attributs les mêmes que ceux
	 * spécifiés dans un bean Modele.
	 *
	 * @param modele
	 *            le modele que l'on souhaite supprimer dans la BDD.
	 */
	@Override
	public void supprimer(ModeleExcel modele) {
		Map<String, Object> identifiants = new HashMap<>();
		identifiants.put(ATTRIBUT_NOM_FICHIER, modele.getNomFichier());

		DAOUtilitaire.executeDelete(this.getDaoFactory(), NOM_ENTITE, identifiants);
	}

	/**
	 * Liste tous les Modèles présents dans la BDD.
	 *
	 * @return parametres la liste des Parametres présents dans la BDD.
	 */
	@Override
	public List<ModeleExcel> lister() {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<ModeleExcel> modeles = new ArrayList<>();
		try {
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			preparedStatement = connection.prepareStatement(SQL_SELECT_TOUT);
			resultSet = preparedStatement.executeQuery();
			// récupération des valeurs des attributs de la BDD pour les mettre dans une
			// liste
			while (resultSet.next()) {
				modeles.add(map(resultSet));
			}
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec du listage des objets.", e);
		} finally {
			fermetures(resultSet, preparedStatement, connection);
		}
		return modeles;
	}

	/**
	 * Fait la correspondance (le mapping) entre une ligne issue de la table
	 * ModeleExcel (un ResultSet) et un bean modele.
	 *
	 * @param resultSet
	 *            la ligne issue de la table ModeleExcel.
	 * @return modeleExcel le bean dont on souhaite faire la correspondance.
	 * @throws SQLException Exception sql
	 */
	public static ModeleExcel map(ResultSet resultSet) throws SQLException {
		ModeleExcel modele = new ModeleExcel();
		modele.setNomFichier(resultSet.getString(ATTRIBUT_NOM_FICHIER));
		modele.setLigneHeader(resultSet.getInt(ATTRIBUT_LIGNEHEADER));
		modele.setColId(resultSet.getInt(ATTRIBUT_COLID));
		modele.setColNom(resultSet.getInt(ATTRIBUT_COLNOM));
		modele.setColNote(resultSet.getInt(ATTRIBUT_COLNOTE));
		modele.setColCom(resultSet.getInt(ATTRIBUT_COLCOM));
		modele.setColMoyenne(resultSet.getInt(ATTRIBUT_COLMOYENNE));
		modele.setColGrade(resultSet.getInt(ATTRIBUT_COLGRADE));

		return modele;
	}


}
