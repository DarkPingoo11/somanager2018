package fr.eseo.ld.dao;

import fr.eseo.ld.beans.PartageRole;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static fr.eseo.ld.dao.DAOUtilitaire.*;

/**
 * Classe du DAO du bean PartageRole.
 * 
 * <p>
 * Cette classe implémente toutes les méthodes CRUD définies dans sa classe
 * mère.
 * </p>
 * 
 * @author Thomas MENARD
 */
public class PartageRoleDAO extends DAO<PartageRole> {

	private static final String NOM_ENTITE = "PartageRole";
	private static final String ATT_ID_PARTAGE = "idPartageRole";
	private static final String ATT_ID_UTILISATEUR1 = "idUtilisateur1";
	private static final String ATT_ID_UTILISATEUR2 = "idUtilisateur2";
	private static final String ATT_ID_ROLE = "idRole";
	private static final String ATT_ID_OPTION = "idOption";
	private static final String ATT_DATE_DEBUT = "dateDebut";
	private static final String ATT_DATE_FIN = "dateFin";
	// de préférence, ne pas l'instancier, la base de données gère cet attribut
	// automatiquement
	private static final String ATT_ACTIF = "actif";

	// tous les attributs du bean doivent être écrits dans la requête INSERT
	private static final String SQL_INSERT = "INSERT INTO `PartageRole`(`idUtilisateur1`, `idUtilisateur2`, `idRole`, `idOption`, `dateDebut`, `dateFin`, `actif`) VALUES (?,?,?,?,?,?,?)";
	private static final String SQL_SELECT_TOUT = "SELECT * FROM PartageRole";

	private static Logger logger = Logger.getLogger(PartageRoleDAO.class.getName());

	/**
	 * Constructeur de DAO.
	 * 
	 * @param daoFactory
	 *            la Factory permettant la création d'une connexion à la BDD.
	 */
	PartageRoleDAO(DAOFactory daoFactory) {
		super(daoFactory);
	}

	/**
	 * Insère un Partage de role dans la BDD à partir des attributs spécifiés dans
	 * un bean PartageRole.
	 * 
	 * @param partageRole
	 *            le PartageRole que l'on souhaite insérer dans la BDD.
	 */
	@Override
	public void creer(PartageRole partageRole) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet valeursAutoGenerees = null;
		try {
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			// les "?" de la requête sont comblés par les attributs de l'objet en paramètre
			// ceux-ci peuvent être nuls à condition que la BDD accepte les données de type
			// "null"
			preparedStatement = initialisationRequetePreparee(connection, SQL_INSERT, true,
					partageRole.getIdUtilisateur1(), partageRole.getIdUtilisateur2(), partageRole.getIdRole(),
					partageRole.getIdOption(), partageRole.getDateDebut(), partageRole.getDateFin(),
					partageRole.getActif());
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec de la création de l'objet, aucune ligne ajoutée dans la table.", e);
		} finally {
			// fermeture des ressources utilisées
			fermetures(valeursAutoGenerees, preparedStatement, connection);
		}
	}

	/**
	 * Liste tous les PartageRole ayant pour attributs les mêmes que ceux spécifiés
	 * dans un bean PartageRole.
	 * 
	 * @param partageRole
	 *            le PartageRole que l'on souhaite trouver dans la BDD.
	 * @return partageRoles la liste des PartageRole trouvés dans la BDD.
	 */
	@Override
	public List<PartageRole> trouver(PartageRole partageRole) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<PartageRole> partageRoles = new ArrayList<>();
		// tableau de Strings regroupant tous les noms des attributs d'un objet ainsi
		// que les valeurs correspondantes
		String[][] attributs = {
				{ ATT_ID_PARTAGE, ATT_ID_UTILISATEUR1, ATT_ID_UTILISATEUR2, ATT_ID_ROLE, ATT_ID_OPTION, ATT_DATE_DEBUT,
						ATT_DATE_FIN, ATT_ACTIF },
				{ String.valueOf(partageRole.getIdPartage()), String.valueOf(partageRole.getIdUtilisateur1()),
						String.valueOf(partageRole.getIdUtilisateur2()), String.valueOf(partageRole.getIdRole()),
						String.valueOf(partageRole.getIdOption()), String.valueOf(partageRole.getDateDebut()),
						String.valueOf(partageRole.getDateFin()), String.valueOf(partageRole.getActif()) } };
		try {
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			// mise en forme de la requête SELECT en fonction des attributs de l'objet sujet
			preparedStatement = initialisationRequetePreparee(connection, "SELECT", NOM_ENTITE, attributs, false);
			resultSet = preparedStatement.executeQuery();
			// récupération des valeurs des attributs de la BDD pour les mettre dans une
			// liste
			while (resultSet.next()) {
				partageRoles.add(map(resultSet));
			}
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec de la recherche de l'objet.", e);
		} finally {
			// fermeture des ressources utilisées
			fermetures(resultSet, preparedStatement, connection);
		}
		return partageRoles;
	}

	/**
	 * Modifie UN PartageRole ayant pour attributs les mêmes que ceux spécifiés dans
	 * un bean PartageRole et la même clé primaire. Cette clé primaire ne peut être
	 * modifiée.
	 * 
	 * @param partageRole
	 *            le PartageRole que l'on souhaite modifier dans la BDD.
	 */
	@Override
	public void modifier(PartageRole partageRole) {
		// tableau de Strings regroupant tous les noms des attributs d'un objet ainsi
		// que les valeurs correspondantes
		// /!\ bien placer la clé primaire et sa valeur à la FIN du tableau sinon erreur
		String[][] attributs = {
				{ ATT_ID_UTILISATEUR1, ATT_ID_UTILISATEUR2, ATT_ID_ROLE, ATT_ID_OPTION, ATT_DATE_DEBUT, ATT_DATE_FIN,
						ATT_ACTIF, ATT_ID_PARTAGE },
				{ String.valueOf(partageRole.getIdUtilisateur1()), String.valueOf(partageRole.getIdUtilisateur2()),
						String.valueOf(partageRole.getIdRole()), String.valueOf(partageRole.getIdOption()),
						String.valueOf(partageRole.getDateDebut()), String.valueOf(partageRole.getDateFin()),
						String.valueOf(partageRole.getActif()), String.valueOf(partageRole.getIdPartage()) } };

		/* Traite la mise à jour de la BDD */
		traitementUpdate(this.getDaoFactory(), NOM_ENTITE, attributs, logger);
	}

	/**
	 * Supprime tous les PartageRole ayant pour attributs les mêmes que ceux
	 * spécifiés dans un bean PartageRole.
	 * 
	 * @param partageRole
	 *            le PartageRole que l'on souhaite supprimer dans la BDD.
	 */
	@Override
	public void supprimer(PartageRole partageRole) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		// tableau de Strings regroupant tous les noms des attributs d'un objet ainsi
		// que les valeurs correspondantes
		String[][] attributs = {
				{ ATT_ID_PARTAGE, ATT_ID_UTILISATEUR1, ATT_ID_UTILISATEUR2, ATT_ID_ROLE, ATT_ID_OPTION, ATT_DATE_DEBUT,
						ATT_DATE_FIN, ATT_ACTIF },
				{ String.valueOf(partageRole.getIdPartage()), String.valueOf(partageRole.getIdUtilisateur1()),
						String.valueOf(partageRole.getIdUtilisateur2()), String.valueOf(partageRole.getIdRole()),
						String.valueOf(partageRole.getIdOption()), String.valueOf(partageRole.getDateDebut()),
						String.valueOf(partageRole.getDateFin()), String.valueOf(partageRole.getActif()) } };
		try {
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			// mise en forme de la requête DELETE en fonction des attributs de l'objet
			preparedStatement = initialisationRequetePreparee(connection, "DELETE", NOM_ENTITE, attributs, false);
			int statut = preparedStatement.executeUpdate();
			if (statut == 0) {
				throw new SQLException();
			} else {
				// suppression du bean
				partageRole.setIdPartage(null);
			}
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec de la suppression de l'objet, aucune ligne supprimée de la table.", e);
		} finally {
			// fermeture des ressources utilisées
			fermetures(preparedStatement, connection);
		}
	}

	/**
	 * Liste tous les PartageRole présents dans la BDD.
	 * 
	 * @return partageRoles la liste des PartageRole présents dans la BDD.
	 */
	@Override
	public List<PartageRole> lister() {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<PartageRole> partageRoles = new ArrayList<>();
		try {
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			preparedStatement = connection.prepareStatement(SQL_SELECT_TOUT);
			resultSet = preparedStatement.executeQuery();
			// récupération des valeurs des attributs de la BDD pour les mettre dans une
			// liste
			while (resultSet.next()) {
				partageRoles.add(map(resultSet));
			}
		} catch (Exception e) {
			logger.log(Level.WARN, "Échec du listage des objets.", e);
		} finally {
			fermetures(resultSet, preparedStatement, connection);
		}
		return partageRoles;
	}

	/**
	 * Fait la correspondance (le mapping) entre une ligne issue de la table
	 * PartageRole (un ResultSet) et un bean PartageRole.
	 * 
	 * @param resultSet
	 *            la ligne issue de la table PartageRole.
	 * @return partageRole le bean dont on souhaite faire la correspondance.
	 * @throws SQLException
	 */
	public static PartageRole map(ResultSet resultSet) throws SQLException {
		PartageRole partageRole = new PartageRole();
		partageRole.setIdPartage(resultSet.getLong(ATT_ID_PARTAGE));
		partageRole.setIdUtilisateur1(resultSet.getLong(ATT_ID_UTILISATEUR1));
		partageRole.setIdUtilisateur2(resultSet.getLong(ATT_ID_UTILISATEUR2));
		partageRole.setIdRole(resultSet.getLong(ATT_ID_ROLE));
		partageRole.setIdOption(resultSet.getLong(ATT_ID_OPTION));
		partageRole.setDateDebut(resultSet.getString(ATT_DATE_DEBUT));
		partageRole.setDateFin(resultSet.getString(ATT_DATE_FIN));
		partageRole.setActif(resultSet.getLong(ATT_ACTIF));
		return partageRole;
	}

}