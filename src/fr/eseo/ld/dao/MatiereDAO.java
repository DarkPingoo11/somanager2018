package fr.eseo.ld.dao;
import fr.eseo.ld.pgl.beans.Matiere;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static fr.eseo.ld.dao.DAOUtilitaire.fermetures;
import static fr.eseo.ld.dao.DAOUtilitaire.initialisationRequetePreparee;
/**
 * Classe du DAO du bean Matiere.
 *
 * <p>
 * Cette classe implémente toutes les méthodes CRUD définies dans sa classe
 * mère.
 * </p>
 *
 */
public class MatiereDAO extends DAO<Matiere>{

	    /* Noms des entités */
	    private static final String NOM_ENTITE = "pgl_matiere";

	    /* Attributs de l'entité Matiere */
	    private static final String ATTRIBUT_ID_MATIERE = "idMatiere";
	    public static final String ATTRIBUT_LIBELLE = "libelle";
	    public static final String ATTRIBUT_COEFFICIENT = "coefficient";
	    public static final String ATTRIBUT_REF_SPRINT = "refSprint";

	    private static final String[] ATTRIBUTS_NOMS = {ATTRIBUT_ID_MATIERE, ATTRIBUT_LIBELLE, ATTRIBUT_COEFFICIENT, ATTRIBUT_REF_SPRINT
	            };


	    /* Constantes pour éviter la duplication de code */
	    private static Logger logger = Logger.getLogger(MatiereDAO.class.getName());

	    /* Requetes SQL Sprint */
	    private static final String SQL_INSERT_MATIERE = "INSERT INTO `pgl_matiere`(`libelle`, `coefficient`, `refSprint`)  VALUES (?,?,?)";
	    private static final String SQL_SELECT_TOUT = "SELECT * FROM pgl_matiere";

	    /* Requetes SQL pour trouver un sprint à partir de son ID */
	    private static final String SQL_SELECT_MATIERE_A_PARTIR_ID = "SELECT * FROM pgl_matiere WHERE idMatiere=?";

	    /**
	     * Constructeur de DAO.
	     *
	     * @param daoFactory la Factory permettant la création d'une connexion à la BDD.
	     */
	    MatiereDAO(DAOFactory daoFactory) {
	        super(daoFactory);
	    }

	    /**
	     * Insère une Matiere dans la BDD à partir des attributs spécifiés dans un bean
	     * Matiere.
	     *
	     * @param matiere la matiere que l'on souhaite insérer dans la BDD à partir du bean
	     *               Matiere.
	     */
	    @Override
	    public void creer(Matiere matiere) {
	        Connection connection = null;
	        PreparedStatement preparedStatement = null;
	        try {
	            // création d'une connexion grâce à la DAOFactory placée en attribut de la
	            // classe
	            connection = this.creerConnexion();
	            // les "?" de la requête sont comblés par les attributs de l'objet en paramètre
	            // ceux-ci peuvent être nuls à condition que la BDD accepte les données de type
	            // "null"
	            preparedStatement = initialisationRequetePreparee(connection, SQL_INSERT_MATIERE, true,
	                    matiere.getLibelle(), matiere.getCoefficient(), matiere.getRefSprint());
	            preparedStatement.executeUpdate();
	        } catch (SQLException e) {
	            logger.log(Level.WARN, "Échec de la création de l'objet, aucune ligne ajoutée dans la table.", e);
	        } finally {
	            // fermeture des ressources utilisées
	            fermetures(preparedStatement, connection);
	        }
	    }

	    /**
	     * Liste tous les matieres ayant pour attributs les mêmes que ceux spécifiés dans
	     * un bean Matiere.
	     *
	     * @param matiere la matiere que l'on souhaite trouver dans la BDD.
	     * @return matiere la liste des Matieres trouvés dans la BDD.
	     */
	    @Override
	    public List<Matiere> trouver(Matiere matiere) {
	        Connection connection = null;
	        PreparedStatement preparedStatement = null;
	        ResultSet resultSet = null;
	        List<Matiere> matieres = new ArrayList<>();
	        // tableau de Strings regroupant tous les noms des attributs d'un objet ainsi
	        // que les valeurs correspondantes
	        String[][] attributs = {ATTRIBUTS_NOMS,
	                {String.valueOf(matiere.getIdMatiere()), String.valueOf(matiere.getLibelle()), String.valueOf(matiere.getCoefficient()),
	                        String.valueOf(matiere.getRefSprint()),
	                }};
	        try {
	            // création d'une connexion grâce à la DAOFactory placée en attribut de la classe
	            connection = this.creerConnexion();
	            // mise en forme de la requête SELECT en fonction des attributs de l'objet sujet

	            preparedStatement = initialisationRequetePreparee(connection, "SELECT", NOM_ENTITE, attributs, true);
	            resultSet = preparedStatement.executeQuery();
	            // récupération des valeurs des attributs de la BDD pour les mettre dans une liste
	            while (resultSet.next()) {
	                matieres.add(map(resultSet));
	            }
	        } catch (SQLException e) {
	            logger.log(Level.WARN, "Échec de la recherche de l'objet Sprint. ", e);
	        } finally {
	            fermetures(resultSet, preparedStatement, connection);
	        }
	        return matieres;
	    }

	    /**
	     * Renvoie une Matiere à partir de son ID.
	     *
	     * @param idMatiere l'ID de Matiere que l'on souhaite trouver dans la BDD.
	     * @return matiere la matiere trouvé dans la BDD.
	     */
	    public Matiere trouver(Long idMatiere) {
	        Connection connection = null;
	        PreparedStatement preparedStatement = null;
	        ResultSet resultSet = null;
	        Matiere matiere = null;
	        try {
	            // création d'une connexion grâce à la DAOFactory placée en attribut de la
	            // classe
	            connection = this.creerConnexion();
	            preparedStatement = initialisationRequetePreparee(connection, SQL_SELECT_MATIERE_A_PARTIR_ID, true,
	                    String.valueOf(idMatiere));
	            resultSet = preparedStatement.executeQuery();
	            resultSet.next();
	            matiere = map(resultSet);
	        } catch (SQLException e) {
	            logger.log(Level.WARN, "Échec de la recherche d'une matiere à partir de son ID.", e);
	        } finally {
	            // fermeture des ressources utilisées
	            fermetures(resultSet, preparedStatement, connection);
	        }
	        return matiere;
	    }


	    /**
	     * Modifie une Matiere ayant pour attributs les mêmes que ceux spécifiés dans un
	     * bean Matiere et la même clé primaire. Cette clé primaire ne peut être
	     * modifiée.
	     *
	     * @param matiere la matiere que l'on souhaite modifier dans la BDD.
	     */
	    @Override
	    public void modifier(Matiere matiere) {
	        //Update table SET <....>
	        Map<String, Object> attributs = new HashMap<>();
	        attributs.put(ATTRIBUT_LIBELLE, matiere.getLibelle());
	        attributs.put(ATTRIBUT_COEFFICIENT, matiere.getCoefficient());
	        attributs.put(ATTRIBUT_REF_SPRINT, matiere.getRefSprint());
	        
	        //Preparation de WHERE
	        Map<String, Object> whereAtt = new HashMap<>();
	        whereAtt.put(ATTRIBUT_ID_MATIERE, matiere.getIdMatiere());

	        //Modifier
	        DAOUtilitaire.executeUpdate(this.getDaoFactory(), NOM_ENTITE, attributs, whereAtt);
	    }

	    /**
	     * Supprime tous les Matieres ayant pour attributs les mêmes que ceux spécifiés
	     * dans un bean Matiere.
	     *
	     * @param matiere la matiere que l'on souhaite supprimer dans la BDD.
	     */
	    @Override
	    public void supprimer(Matiere matiere) {
			Map<String, Object> identifiants = new HashMap<>();
			identifiants.put(ATTRIBUT_ID_MATIERE, matiere.getIdMatiere());

			DAOUtilitaire.executeDelete(this.getDaoFactory(), NOM_ENTITE, identifiants);
	    }

	    /**
	     * Liste tous les Matieres présents dans la BDD.
	     *
	     * @return matieres la liste des matieres présents dans la BDD.
	     */
	    @Override
	    public List<Matiere> lister() {
	        Connection connection = null;
	        PreparedStatement preparedStatement = null;
	        ResultSet resultSet = null;
	        List<Matiere> matieres = new ArrayList<>();
	        try {
	            // création d'une connexion grâce à la DAOFactory placée en attribut de la
	            // classe
	            connection = this.creerConnexion();
	            preparedStatement = connection.prepareStatement(SQL_SELECT_TOUT);
	            resultSet = preparedStatement.executeQuery();
	            while (resultSet.next()) {
	                matieres.add(map(resultSet));
	            }
	            // récupération des valeurs des attributs de la BDD pour les mettre dans une
	            // liste
	        } catch (SQLException e) {
	            logger.log(Level.WARN, "Échec du listage des objets sprints. ", e);
	        } finally {
	            fermetures(resultSet, preparedStatement, connection);
	        }
	        return matieres;
	    }

	    // #################################################
	    // # Méthodes privées #
	    // #################################################

	    /**
	     * Fait la correspondance (le mapping) entre une ligne issue de la table Matiere
	     * (un ResultSet) et un bean Matiere.
	     *
	     * @param resultSet la ligne issue de la table Matiere.
	     * @return matiere le bean dont on souhaite faire la correspondance.
	     */
	    public static Matiere map(ResultSet resultSet) throws SQLException {
	        Matiere matiere = new Matiere();
	        matiere.setIdMatiere(resultSet.getLong(ATTRIBUT_ID_MATIERE));
	        matiere.setLibelle(resultSet.getString(ATTRIBUT_LIBELLE));
	        matiere.setCoefficient(resultSet.getFloat(ATTRIBUT_COEFFICIENT));
	        matiere.setRefSprint(resultSet.getLong(ATTRIBUT_REF_SPRINT));
	        return matiere;
	    }

	}



