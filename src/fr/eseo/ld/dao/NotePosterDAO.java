package fr.eseo.ld.dao;

import fr.eseo.ld.beans.NotePoster;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static fr.eseo.ld.dao.DAOUtilitaire.fermetures;
import static fr.eseo.ld.dao.DAOUtilitaire.initialisationRequetePreparee;

/**
 * Classe du DAO du bean NotePoster.
 *
 * <p>
 * Cette classe implémente toutes les méthodes CRUD définies dans sa classe
 * mère.
 * </p>
 *
 * @author Maxime LENORMAND
 */
public class NotePosterDAO extends DAO<NotePoster> {

	private static final String NOM_ENTITE = "NotePoster";
	private static final String ATTRIBUT_ID_PROFESSEUR = "idProfesseur";
	private static final String ATTRIBUT_ID_POSTER = "idPoster";
	private static final String ATTRIBUT_ID_ETUDIANT = "idEtudiant";
	private static final String ATTRIBUT_NOTE = "note";
	private static final String[] ATTRIBUTS_NOMS = { ATTRIBUT_ID_PROFESSEUR, ATTRIBUT_ID_ETUDIANT, ATTRIBUT_ID_POSTER,
			ATTRIBUT_NOTE };
	private static final String[] ATTRIBUTS_NOMS_ID_FIN = { ATTRIBUT_NOTE, ATTRIBUT_ID_PROFESSEUR, ATTRIBUT_ID_ETUDIANT,
			ATTRIBUT_ID_POSTER };
	// tous les attributs du bean doivent être écrits dans la requête INSERT
	private static final String SQL_INSERT = "INSERT INTO NotePoster (idProfesseur, idEtudiant, idPoster, note) VALUES (?, ?, ?, ?)";
	private static final String SQL_SELECT_TOUT = "SELECT * FROM NotePoster";

	private static Logger logger = Logger.getLogger(NotePosterDAO.class.getName());

	/**
	 * Constructeur de DAO.
	 *
	 * @param daoFactory
	 *            la Factory permettant la création d'une connexion à la BDD.
	 */
	NotePosterDAO(DAOFactory daoFactory) {
		super(daoFactory);
	}

	/**
	 * Insère une NotePoster dans la BDD à partir des attributs spécifiés dans un
	 * bean NotePoster.
	 *
	 * @param notePoster
	 *            la NotePoster que l'on souhaite insérer dans la BDD à partir du
	 *            bean NotePoster.
	 */
	@Override
	public void creer(NotePoster notePoster) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			// les "?" de la requête sont comblés par les attributs de l'objet en paramètre
			// ceux-ci peuvent être nuls à condition que la BDD accepte les données de type
			// "null"
			preparedStatement = initialisationRequetePreparee(connection, SQL_INSERT, true,
					notePoster.getIdProfesseur(), notePoster.getIdEtudiant(), notePoster.getIdPoster(),
					notePoster.getNote());
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec de la création de l'objet, aucune ligne ajoutée dans la table.", e);
		} finally {
			// fermeture des ressources utilisées
			fermetures(preparedStatement, connection);
		}
	}

	/**
	 * Liste toutes les NotePoster ayant pour attributs les mêmes que ceux spécifiés
	 * dans un bean NotePoster.
	 *
	 * @param notePoster
	 *            la NotePoster que l'on souhaite trouver dans la BDD.
	 * @return notePosters la liste des NotePoste trouvées dans la BDD.
	 */
	@Override
	public List<NotePoster> trouver(NotePoster notePoster) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<NotePoster> notePosters = new ArrayList<>();
		// tableau de Strings regroupant tous les noms des attributs d'un objet ainsi
		// que les valeurs correspondantes
		String[][] attributs = { ATTRIBUTS_NOMS,
				{ String.valueOf(notePoster.getIdProfesseur()), String.valueOf(notePoster.getIdEtudiant()),
						String.valueOf(notePoster.getIdPoster()), String.valueOf(notePoster.getNote()) } };
		try {
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			// mise en forme de la requête SELECT en fonction des attributs de l'objet
			preparedStatement = initialisationRequetePreparee(connection, "SELECT", NOM_ENTITE, attributs, false);
			resultSet = preparedStatement.executeQuery();
			// récupération des valeurs des attributs de la BDD pour les mettre dans une
			// liste
			while (resultSet.next()) {
				notePosters.add(map(resultSet));
			}
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec de la recherche de l'objet NotePoster.", e);
		} finally {
			// fermeture des ressources utilisées
			fermetures(resultSet, preparedStatement, connection);
		}
		return notePosters;
	}

	/**
	 * Modifie UNE NotePoster ayant pour attributs les mêmes que ceux spécifiés dans
	 * un bean NotePoster et la même clé primaire. Cette clé primaire ne peut être
	 * modifiée.
	 *
	 * @param notePoster
	 *            la NotePoster que l'on souhaite modifier dans la BDD.
	 */
	@Override
	public void modifier(NotePoster notePoster) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		// tableau de Strings regroupant tous les noms des attributs d'un objet ainsi
		// que les valeurs correspondantes
		// /!\ bien placer la clé primaire et sa valeur à la FIN du tableau sinon erreur
		String[][] attributs = { ATTRIBUTS_NOMS_ID_FIN,
				{ String.valueOf(notePoster.getNote()), String.valueOf(notePoster.getIdProfesseur()),
						String.valueOf(notePoster.getIdEtudiant()), String.valueOf(notePoster.getIdPoster()) } };
		try {
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			// mise en forme de la requête UPDATE en fonction des attributs de l'objet
			// notePoster
			preparedStatement = initialisationRequetePreparee(connection, "UPDATE", NOM_ENTITE, attributs,
					new String[]{"Professeur", "Etudiant", "Poster"}, false);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec de la mise à jour de l'objet, aucune ligne modifée dans la table.", e);
		} finally {
			// fermeture des ressources utilisées
			fermetures(preparedStatement, connection);
		}
	}

	/**
	 * Supprime toutes les NotePoster ayant pour attributs les mêmes que ceux
	 * spécifiés dans un bean NotePoster.
	 *
	 * @param notePoster
	 *            la NotePoster que l'on souhaite supprimer dans la BDD.
	 */
	@Override
	public void supprimer(NotePoster notePoster) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		// tableau de Strings regroupant tous les noms des attributs d'un objet ainsi
		// que les valeurs correspondantes
		String[][] attributs = { ATTRIBUTS_NOMS,
				{ String.valueOf(notePoster.getIdProfesseur()), String.valueOf(notePoster.getIdEtudiant()),
						String.valueOf(notePoster.getIdPoster()), String.valueOf(notePoster.getNote()) } };
		try {
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			preparedStatement = initialisationRequetePreparee(connection, "DELETE", NOM_ENTITE, attributs, false);
			int statut = preparedStatement.executeUpdate();
			if (statut == 0) {
				throw new SQLException();
			} else {
				// suppression du bean
				notePoster.setIdProfesseur(null);
				notePoster.setIdPoster(null);
			}
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec de la suppression de l'objet, aucune ligne supprimée de la table.", e);
		} finally {
			// fermeture des ressources utilisées
			fermetures(preparedStatement, connection);
		}
	}

	/**
	 * Liste toutes les NotePoster présentes dans la BDD.
	 *
	 * @return notePosters la liste des NotePoster présentes dans la BDD.
	 */
	@Override
	public List<NotePoster> lister() {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<NotePoster> notePosters = new ArrayList<>();
		try {
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			preparedStatement = connection.prepareStatement(SQL_SELECT_TOUT);
			resultSet = preparedStatement.executeQuery();
			// récupération des valeurs des attributs de la BDD pour les mettre dans une
			// liste
			while (resultSet.next()) {
				notePosters.add(map(resultSet));
			}
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec du listage des objets.", e);
		} finally {
			fermetures(resultSet, preparedStatement, connection);
		}
		return notePosters;
	}

	/**
	 * Fait la correspondance (le mapping) entre une ligne issue de la table
	 * NotePoster (un ResultSet) et un bean NotePoster.
	 *
	 * @param resultSet
	 *            la ligne issue de la table NotePoster.
	 * @return notePoster le bean dont on souhaite faire la correspondance.
	 * @throws SQLException
	 */
	public static NotePoster map(ResultSet resultSet) throws SQLException {
		NotePoster notePoster = new NotePoster();
		notePoster.setIdProfesseur(resultSet.getLong(ATTRIBUT_ID_PROFESSEUR));
		notePoster.setIdEtudiant(resultSet.getLong(ATTRIBUT_ID_ETUDIANT));
		notePoster.setIdPoster(resultSet.getLong(ATTRIBUT_ID_POSTER));
		notePoster.setNote(resultSet.getFloat(ATTRIBUT_NOTE));
		return notePoster;
	}

}