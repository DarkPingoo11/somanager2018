package fr.eseo.ld.dao;

import fr.eseo.ld.beans.*;
import fr.eseo.ld.objets.exportexcel.ENJMatiere;
import fr.eseo.ld.objets.exportexcel.ENJNotes;
import fr.eseo.ld.servlets.Noter;
import fr.eseo.ld.utils.UtilisateurNameComparator;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.*;

import static fr.eseo.ld.dao.DAOUtilitaire.*;

/**
 * <h1>Utilisateur DAO</h1>
 * <p>
 * Classe du DAO du bean Utilisateurs.
 * </p>
 * <h2>Méthodes incluses :</h2>
 * <ul style="list-style-type:circle">
 * <li>void creer(Utilisateur utilisateur)</li>
 * <li>List(Utilisateur) trouver(Utilisateur utilisateur)</li>
 * <li>void modifier(Utilisateur utilisateur)</li>
 * <li>void supprimer(Utilisateur utilisateur)</li>
 * <li>List(Utilisateur) lister()</li>
 * <li>List(Role) trouverRole(Utilisateur utilisateur)</li>
 * <li>dissocierRole(Utilisateur utilisateur, Role role)</li>
 * <li>Role mapRole(ResultSet resultSet)</li>
 * <li>List(OptionESEO) trouverOption(Utilisateur utilisateur)</li>
 * <li>void dissocierOption(Utilisateur utilisateur, OptionESEO option)</li>
 * <li>void associerRoleOption(Utilisateur utilisateur, Role role, OptionESEO
 * option)</li>
 * <li>List(Utilisateur) avoirPorteurSujet(Sujet sujet)</li>
 * <li>Utilisateur mapUtilisateur(ResultSet resultSet)</li>
 * </ul>
 *
 * @author Thomas MENARD et Maxime LENORMAND
 */
public class UtilisateurDAO extends DAO<Utilisateur> {

	/* Noms des entités */
	private static final String NOM_ENTITE = "Utilisateur";
	private static final String NOM_ENTITE_ROLE_UTILISATEUR = "RoleUtilisateur";

	/* Attributs de l'entité Utilisateur */
	private static final String ATTRIBUT_NOM = "nom";
	private static final String ATTRIBUT_PRENOM = "prenom";
	private static final String ATTRIBUT_IDENTIFIANT = "identifiant";
	private static final String ATTRIBUT_HASH = "hash";
	private static final String ATTRIBUT_EMAIL = "email";
	private static final String ATTRIBUT_VALIDE = "valide";
	private static final String ATTRIBUT_ROLES = "roles";
	private static final String ATTRIBUT_ANNEE = "annee";
	private static final String ATTRIBUT_ID_UTILISATEUR = "idUtilisateur";
	private static final String ATTRIBUT_NOTEPOSTER = "notePoster";
	private static final String ATTRIBUT_NOTESOUTENANCE = "noteSoutenance";

	private static final String[] ATTRIBUTS_NOMS = { ATTRIBUT_ID_UTILISATEUR, ATTRIBUT_NOM, ATTRIBUT_PRENOM, ATTRIBUT_IDENTIFIANT, ATTRIBUT_HASH, ATTRIBUT_EMAIL, ATTRIBUT_VALIDE };
	private static final String[] ATTRIBUTS_NOMS_ID_FIN = { ATTRIBUT_NOM, ATTRIBUT_PRENOM, ATTRIBUT_IDENTIFIANT, ATTRIBUT_HASH, ATTRIBUT_EMAIL, ATTRIBUT_VALIDE, ATTRIBUT_ID_UTILISATEUR };

	/* Requetes SQL pour Utilisateur */
	private static final String SQL_INSERT = "INSERT INTO Utilisateur (nom, prenom, identifiant, hash, email, valide) VALUES (?, ?, ?, ?, ?, ?)";
	private static final String SQL_SELECT_UTILISATEUR = "SELECT idUtilisateur, nom, prenom, identifiant, hash, email, valide FROM Utilisateur ORDER BY idUtilisateur";

	/* Requetes SQL pour RoleUtilisateur */
	private static final String SQL_INSERT_ROLE_UTILISATEUR = "INSERT INTO `RoleUtilisateur`(`idUtilisateur`, `idRole`, `idOption`) VALUES (?,?,?)";

	/* Requetes SQL pour Role */
	private static final String SQL_SELECT_ROLE = "SELECT r.* FROM Role r, RoleUtilisateur ru WHERE ru.idUtilisateur = ? AND ru.idRole = r.idRole";
	private static final String ATTRIBUT_ID_ROLE = "idRole";

	private static final String SQL_SELECT_ROLE_PRODUCT_BY_ID = "SELECT DISTINCT ru.idUtilisateur, ut.nom, ut.prenom  FROM pgl_roleequipe eq, Role r, RoleUtilisateur ru, Utilisateur ut WHERE ru.idUtilisateur = ut.idUtilisateur " + "AND ru.idRole = r.idRole AND r.idRole = ?";

	/* Requetes SQL pour OptionESEO */
	private static final String SQL_SELECT_OPTIONS = "SELECT opt.idOption, opt.nomOption FROM OptionESEO opt, RoleUtilisateur ru WHERE opt.idOption = ru.idOption and ru.idUtilisateur=?";
	private static final String ATTRIBUT_ID_OPTION = "idOption";

	/* Requetes SQL pour Etudiant */
	private static final String ATTRIBUT_ID_ETUDIANT = "idEtudiant";
	private static final String SQL_SELECT_ETUDIANTS_DISPONIBLES = "SELECT DISTINCT Etudiant.idEtudiant, Utilisateur.nom, Utilisateur.prenom FROM Etudiant, Utilisateur, AnneeScolaire, RoleUtilisateur" + " WHERE NOT EXISTS (SELECT idEtudiant FROM EtudiantEquipe WHERE EtudiantEquipe.idEtudiant = Etudiant.idEtudiant) AND Etudiant.idEtudiant = Utilisateur.idUtilisateur" + " AND Etudiant.annee = AnneeScolaire.anneeDebut AND AnneeScolaire.idOption = RoleUtilisateur.idOption AND RoleUtilisateur.idUtilisateur = Etudiant.idEtudiant";

	/* Requetes SQL pour Etudiant : Requete correcte */
	private static final String SQL_SELECT_ETUDIANTS_DISPONIBLES_I2_SANS_EQUIPE = "SELECT DISTINCT pgl_etudiant.idEtudiant, Utilisateur.nom, Utilisateur.prenom FROM pgl_etudiant, Utilisateur, AnneeScolaire, RoleUtilisateur" + " WHERE NOT EXISTS (SELECT idEtudiant FROM pgl_etudiantequipe WHERE pgl_etudiantequipe.idEtudiant = pgl_etudiant.idEtudiant) AND pgl_etudiant.idEtudiant = Utilisateur.idUtilisateur" + " AND pgl_etudiant.refAnnee = AnneeScolaire.idAnneeScolaire AND AnneeScolaire.idOption = RoleUtilisateur.idOption AND RoleUtilisateur.idUtilisateur = pgl_etudiant.idEtudiant";

	private static final String SQL_SELECT_ETUDIANTS_I2_DISPONIBLES = "SELECT DISTINCT pgl_etudiant.idEtudiant, Utilisateur.nom, Utilisateur.prenom FROM pgl_etudiant, Utilisateur, AnneeScolaire, RoleUtilisateur" + " WHERE pgl_etudiant.idEtudiant = Utilisateur.idUtilisateur" + " AND pgl_etudiant.refAnnee = AnneeScolaire.idAnneeScolaire AND AnneeScolaire.idOption = RoleUtilisateur.idOption AND RoleUtilisateur.idUtilisateur = pgl_etudiant.idEtudiant";

	private static final String SQL_LISTER_ETUDIANT_ANNEE_EN_COURS = "SELECT DISTINCT Utilisateur.idUtilisateur, Utilisateur.nom, Utilisateur.prenom FROM Utilisateur, Etudiant, AnneeScolaire, RoleUtilisateur " + "WHERE Utilisateur.idUtilisateur = Etudiant.idEtudiant " + "AND Etudiant.annee = AnneeScolaire.anneeDebut AND AnneeScolaire.idOption = RoleUtilisateur.idOption AND RoleUtilisateur.idUtilisateur = Etudiant.idEtudiant";

	private static final String SQL_LISTER_ETUDIANT_I2_ANNEE_EN_COURS = "SELECT DISTINCT Utilisateur.idUtilisateur, Utilisateur.nom, Utilisateur.prenom FROM Utilisateur, pgl_etudiant, AnneeScolaire, RoleUtilisateur " + "WHERE Utilisateur.idUtilisateur = pgl_etudiant.idEtudiant " + "AND pgl_etudiant.refAnnee = AnneeScolaire.idAnneeScolaire AND AnneeScolaire.idOption = RoleUtilisateur.idOption AND RoleUtilisateur.idUtilisateur = pgl_etudiant.idEtudiant";

	
	/* Requetes SQL pour EtudiantEquipe */
	private static final String SQL_LISTER_ETUDIANT_EQUIPE = "SELECT DISTINCT Utilisateur.idUtilisateur, Utilisateur.nom, Utilisateur.prenom FROM Utilisateur, EtudiantEquipe, Etudiant, AnneeScolaire, RoleUtilisateur " + "WHERE Utilisateur.idUtilisateur = EtudiantEquipe.idEtudiant AND EtudiantEquipe.idEtudiant = Etudiant.idEtudiant " + "AND Etudiant.annee = AnneeScolaire.anneeDebut AND AnneeScolaire.idOption = RoleUtilisateur.idOption AND RoleUtilisateur.idUtilisateur = Etudiant.idEtudiant";

	/* Requetes SQL pour Professeur */
	private static final String SQL_SELECT_PROFESSEUR = "SELECT u.* FROM Utilisateur u, Professeur p Where p.idProfesseur=u.idUtilisateur";

	private static final String SQL_SELECT_PROFESSEUR_SUJET = "SELECT ps.* FROM Utilisateur u, ProfesseurSujet ps WHERE ps.idProfesseur=u.idUtilisateur";
	private static final String SQL_SELECT_PROFESSEURS_ATTRIBUES_A_UN_SUJET = "SELECT ps.idProfesseur, ps.idSujet, ps.fonction, ps.valide FROM ProfesseurSujet ps, Professeur p, Utilisateur u WHERE ps.idProfesseur = p.idProfesseur and p.idProfesseur = u.idUtilisateur and ps.idSujet=?";
	private static final String SQL_SELECT_PROFESSEURS_SUJETS = "SELECT ps.idProfesseur, ps.idSujet, ps.fonction, ps.valide FROM ProfesseurSujet ps, Professeur p, Utilisateur u WHERE ps.idProfesseur = p.idProfesseur and p.idProfesseur = u.idUtilisateur and ps.idProfesseur=?";
	private static final String SQL_SELECT_PROFESSEURS_A_VALIDER = "SELECT u.* FROM Utilisateur u, ProfesseurSujet ps WHERE ps.idProfesseur = u.idUtilisateur and ps.fonction = 'référent' and ps.valide = 'non'";
	private static final String SQL_SELECT_PROFESSEURS_A_ATTRIBUER = "SELECT DISTINCT u.* FROM Utilisateur u, RoleUtilisateur ru WHERE  (ru.idRole = 4 OR ru.idRole = 5)  AND ru.idUtilisateur = u.idUtilisateur";
	private static final String SQL_SELECT_PROFESSEUR_FONCTION = "SELECT * FROM ProfesseurSujet WHERE ProfesseurSujet.fonction = ?";

	/*
	 * Requetes SQL pour trouver un utilisateur a partir du formulaire de recherche
	 */
	private static final String SQL_FILTER_SELECT_ID_ROLES = "SELECT idRole FROM roleutilisateur WHERE idUtilisateur = u.idUtilisateur";

	private static final String SQL_FILTER_FIND_UTILISATEUR = "SELECT u.idUtilisateur, u.nom, u.prenom, u.identifiant, u.email, u.valide, IFNULL(e.annee, a.anneeDebut) annee, " + "GROUP_CONCAT(CONCAT_WS('+', role.idRole, role.nomRole)) as roles " + "FROM Utilisateur u " + "LEFT JOIN etudiant e ON u.idUtilisateur = e.idEtudiant " + "LEFT JOIN pgl_etudiant pe ON u.idUtilisateur = pe.idEtudiant " + "LEFT JOIN anneescolaire a ON a.idAnneeScolaire = pe.refAnnee " + "LEFT JOIN roleutilisateur r ON u.idUtilisateur = r.idUtilisateur " + "LEFT JOIN role ON r.idRole = role.idRole " + "WHERE (LOWER(u.nom) LIKE ? OR LOWER(u.prenom) LIKE ? OR LOWER(u.identifiant) LIKE ? " + "OR LOWER(CONCAT(u.nom, ' ', u.prenom)) LIKE ? OR LOWER(CONCAT(u.prenom, ' ', u.nom)) LIKE ?) " + "AND (? IN(" + SQL_FILTER_SELECT_ID_ROLES + ") OR ? IS NULL) " + "AND ((e.annee = ? OR ? IS NULL) OR (a.anneeDebut = ? OR ? IS NULL)) " + "AND (u.valide = ? OR ? IS NULL) " + "GROUP BY u.idUtilisateur LIMIT ?, ?";

	private static final String SQL_FILTER_FIND_UTILISATEUR_COUNT = "SELECT COUNT(DISTINCT u.idUtilisateur) as nbr " + "FROM Utilisateur u " + "LEFT JOIN etudiant e ON u.idUtilisateur = e.idEtudiant " + "LEFT JOIN pgl_etudiant pe ON u.idUtilisateur = pe.idEtudiant " + "LEFT JOIN anneescolaire a ON a.idAnneeScolaire = pe.refAnnee " + "LEFT JOIN roleutilisateur r ON u.idUtilisateur = r.idUtilisateur " + "LEFT JOIN role ON r.idRole = role.idRole " + "WHERE (LOWER(u.nom) LIKE ? OR LOWER(u.prenom) LIKE ? OR LOWER(u.identifiant) LIKE ? " + "OR LOWER(CONCAT(u.nom, ' ', u.prenom)) LIKE ? OR LOWER(CONCAT(u.prenom, ' ', u.nom)) LIKE ?) " + "AND (? IN(" + SQL_FILTER_SELECT_ID_ROLES + ") OR ? IS NULL) " + "AND ((e.annee = ? OR ? IS NULL) OR (a.anneeDebut = ? OR ? IS NULL)) " + "AND (u.valide = ? OR ? IS NULL) ";

	private static final String SQL_TROUVER_NOTES_UTILISATEURS = "SELECT us.*, ns.note as noteSoutenance, np.note as notePoster " + "FROM Utilisateur us " + "JOIN roleutilisateur ru ON us.idUtilisateur = ru.idUtilisateur " + "JOIN Etudiant e ON e.idEtudiant = us.idUtilisateur " + "LEFT JOIN notesoutenance ns ON e.idEtudiant = ns.idEtudiant " + "LEFT JOIN noteposter np ON e.idEtudiant = np.idEtudiant " + "WHERE (ru.idOption = ? OR ? IS NULL) AND e.annee = ?";

	private static final String SQL_CHECKROLENOTATION = "SELECT u.idUtilisateur, IFNULL(re.refUtilisateur, 0) isScrumMaster,  " + "IFNULL(ru.idUtilisateur, 0) isProfResp, " + "IFNULL(re2.refUtilisateur, 0) isProductOwner  " + "FROM utilisateur u " + "LEFT JOIN pgl_roleequipe re ON re.refUtilisateur = u.idUtilisateur AND re.refRole = 11 " + "LEFT JOIN roleutilisateur ru ON ru.idUtilisateur = u.idUtilisateur AND ru.idRole = 5 " + "LEFT JOIN pgl_roleequipe re2 ON re2.refUtilisateur = u.idUtilisateur AND re2.refRole = 12 " + "WHERE u.idUtilisateur = ?";

	/* Requetes SQL pour PorteurSujet */
	private static final String SQL_SELECT_PORTEUR_SUJET = "SELECT user.* FROM Utilisateur user, PorteurSujet ps, Sujet su WHERE su.idSujet=ps.idSujet and ps.idUtilisateur=user.idUtilisateur and su.idSujet=?";

	/* Requetes SQL pour Meeting */
	private static final String SQL_SELECT_CREATEUR_MEETING = "SELECT user.* FROM Utilisateur user, Reunion reu WHERE  reu.refUtilisateur=user.idUtilisateur and reu.idReunion=?";

	/* Requetes SQL pour Invite */
	private static final String SQL_SELECT_INVITE_MEETING2 = "SELECT user.* FROM Utilisateur user, Invite_Reunion inv, Reunion reu WHERE inv.refUtilisateur=user.idUtilisateur and inv.refReunion=reu.idReunion and reu.idReunion=?";

	/* Constantes pour éviter la duplication de code */
	private static final String DELETE = "DELETE";
	public static final String MESSAGE = "Échec de la recherche des étudiants disponibles.";

	/* Logger */
	private static Logger logger = Logger.getLogger(UtilisateurDAO.class.getName());
	private static final String MESSAGE_ERREUR = "Échec de la recherche des utilisateurs attribués à un sujet.";

	/**
	 * Constructeur de DAO.
	 *
	 * @param daoFactory
	 *            la Factory permettant la création d'une connexion à la BDD.
	 */
	public UtilisateurDAO(DAOFactory daoFactory) {
		super(daoFactory);
	}

	// ########################################################################################################
	// # Méthodes CRUD concernant le bean Utilisateur #
	// ########################################################################################################

	/**
	 * Insère un Utilisateur dans la BDD à partir des attributs spécifiés dans un
	 * bean Utilisateur.
	 *
	 * @param utilisateur
	 *            l'Utilisateur que l'on souhaite insérer dans la BDD à partir du
	 *            bean Utilisateur.
	 */
	@Override
	public void creer(Utilisateur utilisateur) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			// les "?" de la requête sont comblés par les attributs de l'objet en paramètre
			// ceux-ci peuvent être nuls à condition que la BDD accepte les données de type
			// "null"
			preparedStatement = initialisationRequetePreparee(connection, SQL_INSERT, true, utilisateur.getNom(), utilisateur.getPrenom(), utilisateur.getIdentifiant(), utilisateur.getHash(), utilisateur.getEmail(), utilisateur.getValide());
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec de la création de l'objet, aucune ligne ajoutée dans la table.", e);
		} finally {
			// fermeture des ressources utilisées
			fermetures(preparedStatement, connection);
		}
	}

	/**
	 * Liste tous les Utilisateurs ayant pour attributs les mêmes que ceux spécifiés
	 * dans un bean Utilisateur.
	 *
	 * @param utilisateur
	 *            l'Utilisateur que l'on souhaite trouver dans la BDD.
	 * @return utilisateurs la liste des Utilisateurs trouvés dans la BDD.
	 */
	@Override
	public List<Utilisateur> trouver(Utilisateur utilisateur) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<Utilisateur> utilisateurs = new ArrayList<>();
		// tableau de Strings regroupant tous les noms des attributs d'un objet ainsi
		// que les valeurs correspondantes
		String[][] attributs = { ATTRIBUTS_NOMS, { String.valueOf(utilisateur.getIdUtilisateur()), utilisateur.getNom(), utilisateur.getPrenom(), utilisateur.getIdentifiant(), utilisateur.getHash(), utilisateur.getEmail(), String.valueOf(utilisateur.getValide()) } };
		try {
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			// mise en forme de la requête SELECT en fonction des attributs de l'objet
			// utilisateur
			preparedStatement = initialisationRequetePreparee(connection, "SELECT", NOM_ENTITE, attributs, false);
			resultSet = preparedStatement.executeQuery();
			// récupération des valeurs des attributs de la BDD pour les mettre dans une
			// liste
			while (resultSet.next()) {
				utilisateurs.add(mapUtilisateur(resultSet));
			}
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec de la recherche de l'objet.", e);
		} finally {
			// fermeture des ressources utilisées
			fermetures(resultSet, preparedStatement, connection);
		}
		return utilisateurs;
	}

	/**
	 * Modifie UN Utilisateur ayant pour attributs les mêmes que ceux spécifiés dans
	 * un bean Utilisateur et la même clé primaire. Cette clé primaire ne peut être
	 * modifiée.
	 *
	 * @param utilisateur
	 *            l'Utilisateur que l'on souhaite modifier dans la BDD.
	 */
	@Override
	public void modifier(Utilisateur utilisateur) {
		// tableau de Strings regroupant tous les noms des attributs d'un objet ainsi
		// que les valeurs correspondantes
		// /!\ bien placer la clé primaire et sa valeur à la FIN du tableau sinon erreur
		String[][] attributs = { ATTRIBUTS_NOMS_ID_FIN, { utilisateur.getNom(), utilisateur.getPrenom(), utilisateur.getIdentifiant(), utilisateur.getHash(), utilisateur.getEmail(), String.valueOf(utilisateur.getValide()), String.valueOf(utilisateur.getIdUtilisateur()) } };
		/* Traite la mise à jour de la BDD */
		traitementUpdate(this.getDaoFactory(), NOM_ENTITE, attributs, logger);
	}

	/**
	 * Supprime tous les Utilisateurs ayant pour attributs les mêmes que ceux
	 * spécifiés dans un bean Utilisateur.
	 *
	 * @param utilisateur
	 *            l'Utilisateur que l'on souhaite supprimer dans la BDD.
	 */
	@Override
	public void supprimer(Utilisateur utilisateur) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		// tableau de Strings regroupant tous les noms des attributs d'un objet ainsi
		// que les valeurs correspondantes
		String[][] attributs = { ATTRIBUTS_NOMS, { String.valueOf(utilisateur.getIdUtilisateur()), utilisateur.getNom(), utilisateur.getPrenom(), utilisateur.getIdentifiant(), utilisateur.getHash(), utilisateur.getEmail(), String.valueOf(utilisateur.getValide()) } };
		try {
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			// mise en forme de la requête DELETE en fonction des attributs de l'objet
			// utilisateur
			preparedStatement = initialisationRequetePreparee(connection, DELETE, NOM_ENTITE, attributs, false);
			int statut = preparedStatement.executeUpdate();
			if (statut == 0) {
				throw new SQLException();
			} else {
				// suppression du bean
				utilisateur.setIdUtilisateur(null);
			}
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec de la suppression de l'objet, aucune ligne supprimée de la table.", e);
		} finally {
			// fermeture des ressources utilisées
			fermetures(preparedStatement, connection);
		}
	}

	/**
	 * Liste tous les Utilisateurs présents dans la BDD.
	 *
	 * @return utilisateurs la liste des Utilisateurs présents dans la BDD.
	 */
	@Override
	public List<Utilisateur> lister() {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<Utilisateur> utilisateurs = new ArrayList<>();
		try {
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			preparedStatement = connection.prepareStatement(SQL_SELECT_UTILISATEUR);
			resultSet = preparedStatement.executeQuery();
			// récupération des valeurs des attributs de la BDD pour les mettre dans une
			// liste
			while (resultSet.next()) {
				utilisateurs.add(mapUtilisateur(resultSet));// Trier par ordre alpha
				Collections.sort(utilisateurs, new UtilisateurNameComparator());
			}
		} catch (SQLException e) {
			logger.log(Level.FATAL, "Échec du listage des objets.", e);
		} finally {
			// fermeture des ressources utilisées
			fermetures(resultSet, preparedStatement, connection);
		}
		return utilisateurs;
	}

	public Long recupererIdSQL(String request) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		Utilisateur utilisateur = new Utilisateur();
		try {
			connection = this.creerConnexion();
			preparedStatement = connection.prepareStatement(request);
			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				utilisateur.setIdUtilisateur(Long.parseLong(resultSet.getString(1)));
			}
		} catch (SQLException e) {
			logger.log(Level.FATAL, "Échec de la récupération de l'identifiant.", e);
		} finally {
			fermetures(resultSet, preparedStatement, connection);
		}
		return utilisateur.getIdUtilisateur();
	}

	// ###########################################################################################
	// # Méthodes concernant le bean RoleUtilisateur #
	// ###########################################################################################

	/**
	 * Crée l'association entre un Utilisateur, un Role et une Option dans la BDD à
	 * partir des attributs spécifiés dans les beans Utilisateur, Role et
	 * OptionESEO.
	 *
	 * @param utilisateur
	 *            l'Utilisateur dont on souhaite associer un Role et une Option.
	 * @param role
	 *            le Role que l'on souhaite ajouter à l'Utilisateur.
	 * @param option
	 *            l'Option que l'on souhaite ajouter à l'Utilisateur.
	 */
	public void associerRoleOption(Utilisateur utilisateur, Role role, OptionESEO option) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			// les "?" de la requête sont comblés par les attributs de l'objet en paramètre
			// ceux-ci peuvent être nuls à condition que la BDD accepte les données de type
			// "null"
			preparedStatement = initialisationRequetePreparee(connection, SQL_INSERT_ROLE_UTILISATEUR, false, utilisateur.getIdUtilisateur(), role.getIdRole(), option.getIdOption());
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec de l'association entre un utilisateur, un role et une option.", e);
		} finally {
			// fermeture des ressources utilisées
			fermetures(preparedStatement, connection);
		}
	}

	/**
	 * Supprime tous les RoleUtilisateur ayant pour attributs les mêmes que ceux
	 * spécifiés dans dans les beans Utilisateur, Role et OptionESEO.
	 *
	 * @param utilisateur
	 *            l'Utilisateur dont ou souhaite soustraire un Role et une Option.
	 * @param role
	 *            le Role que l'on souhaite retirer de l'Utilisateur
	 * @param option
	 *            l'Option que l'on souhaite retirer de l'Utilisateur
	 */
	public void supprimerRoleOption(Utilisateur utilisateur, Role role, OptionESEO option) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		// tableau de Strings regroupant tous les noms des attributs d'un objet ainsi
		// que les valeurs correspondantes
		String[][] attributs = { { ATTRIBUT_ID_UTILISATEUR, ATTRIBUT_ID_ROLE, ATTRIBUT_ID_OPTION }, { String.valueOf(utilisateur.getIdUtilisateur()), String.valueOf(role.getIdRole()), String.valueOf(option.getIdOption()) } };
		try {
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			// mise en forme de la requête DELETE en fonction des attributs de l'objet
			// utilisateur
			preparedStatement = DAOUtilitaire.initialisationRequetePreparee(connection, DELETE, NOM_ENTITE_ROLE_UTILISATEUR, attributs, false);
			int statut = preparedStatement.executeUpdate();
			if (statut == 0) {
				throw new SQLException();
			}
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec de la dissociation entre un utilisateur, un role et une option.", e);
		} finally {
			// fermeture des ressources utilisées
			fermetures(preparedStatement, connection);
		}
	}

	// #####################################################################################################
	// # Méthodes liant Utilisateur et Role à travers RoleUtilisateur #
	// #####################################################################################################

	/**
	 * Liste tous les Roles de l'Utilisateur ayant pour attribut le même ID que
	 * celui spécifié dans le bean Utilisateur.
	 *
	 * @param utilisateur
	 *            l'Utilisateur dont on souhaite récupérer les Roles.
	 * @return roles la liste des Roles trouvés dans la BDD correspondant à
	 *         l'Utilisateur.
	 */
	public List<Role> trouverRole(Utilisateur utilisateur) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<Role> roles = new ArrayList<>();
		try {
			// vérification que l'utilisateur rentré n'est pas nul
			if (utilisateur.getIdUtilisateur() == null) {
				throw new SQLException();
			}
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			// mise en forme de la requête SELECT en fonction des attributs de l'objet
			// utilisateur
			preparedStatement = connection.prepareStatement(SQL_SELECT_ROLE, Statement.NO_GENERATED_KEYS);
			preparedStatement.setLong(1, utilisateur.getIdUtilisateur());
			resultSet = preparedStatement.executeQuery();
			// récupération des valeurs des attributs de la BDD pour les mettre dans une
			// liste
			while (resultSet.next()) {
				roles.add(RoleDAO.map(resultSet));
			}
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec de la recherche des roles d'un utilisateur.", e);
		} finally {
			// fermeture des ressources utilisées
			fermetures(resultSet, preparedStatement, connection);
		}
		return roles;
	}

	public List<Utilisateur> trouverUtilisateursByIdRole(Role idRole) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<Utilisateur> utilisateurs = new ArrayList<>();
		try {
			// vérification que l'utilisateur rentré n'est pas nul
			if (idRole.getIdRole() == null) {
				throw new SQLException();
			}
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			// mise en forme de la requête SELECT en fonction des attributs de l'objet
			// utilisateur
			preparedStatement = connection.prepareStatement(SQL_SELECT_ROLE_PRODUCT_BY_ID, Statement.NO_GENERATED_KEYS);
			preparedStatement.setLong(1, idRole.getIdRole());
			resultSet = preparedStatement.executeQuery();
			// récupération des valeurs des attributs de la BDD pour les mettre dans une
			// liste
			while (resultSet.next()) {
				Utilisateur etu = new Utilisateur();
				etu.setIdUtilisateur(resultSet.getLong(ATTRIBUT_ID_UTILISATEUR));
				etu.setNom(resultSet.getString(ATTRIBUT_NOM));
				etu.setPrenom(resultSet.getString(ATTRIBUT_PRENOM));
				utilisateurs.add(etu);
			}
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec de la recherche des roles d'un utilisateur.", e);
		} finally {
			// fermeture des ressources utilisées
			fermetures(resultSet, preparedStatement, connection);
		}
		return utilisateurs;
	}

	// ####################################################################################################
	// # Méthodes Utilisateur et OptionESEO à travers RoleUtilisateur #
	// ####################################################################################################

	/**
	 * Liste toutes les Options de l'Utilisateur ayant pour attribut le même ID que
	 * celui spécifié dans le bean Utilisateur.
	 *
	 * @param utilisateur
	 *            l'Utilisateur dont on souhaite récupérer les Options.
	 * @return options la liste des OptionsESEO trouvées dans la BDD correspondant à
	 *         l'Utilisateur.
	 */
	public List<OptionESEO> trouverOption(Utilisateur utilisateur) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<OptionESEO> options = new ArrayList<>();
		try {
			// vérification que l'utilisateur rentré n'est pas nul
			if (utilisateur.getIdUtilisateur() == null) {
				throw new SQLException();
			}
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			// mise en forme de la requête SELECT en fonction des attributs de l'objet
			// utilisateur
			preparedStatement = connection.prepareStatement(SQL_SELECT_OPTIONS, Statement.NO_GENERATED_KEYS);
			preparedStatement.setLong(1, utilisateur.getIdUtilisateur());
			resultSet = preparedStatement.executeQuery();
			// récupération des valeurs des attributs de la BDD pour les mettre dans une
			// liste
			while (resultSet.next()) {
				options.add(OptionESEODAO.map(resultSet));
			}
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec de la recherche des options d'un utilisateur.", e);
		} finally {
			// fermeture des ressources utilisées
			fermetures(resultSet, preparedStatement, connection);
		}
		return options;
	}

	// ###########################################################################################################
	// # Méthodes liant Utilisateur et Sujet à travers PorteurSujet #
	// ###########################################################################################################

	/**
	 * Liste les Utilisateurs attribués à un Sujet donné.
	 *
	 * @param sujet
	 *            le Sujet dont on souhaite connaître les Utilisateurs qui y sont
	 *            attribués.
	 * @return utilisateurs la liste des Utilisateurs attribués au Sujet.
	 */
	public List<Utilisateur> trouverPorteurSujet(Sujet sujet) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<Utilisateur> utilisateurs = new ArrayList<>();
		try {
			// vérification que l'utilisateur rentré n'est pas nul
			if (sujet.getIdSujet() == null) {
				throw new SQLException();
			}
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			// mise en forme de la requête SELECT en fonction des attributs de l'objet sujet
			preparedStatement = initialisationRequetePreparee(connection, SQL_SELECT_PORTEUR_SUJET, false, sujet.getIdSujet());
			resultSet = preparedStatement.executeQuery();
			// récupération des valeurs des attributs de la BDD pour les mettre dans une
			// liste
			while (resultSet.next()) {
				utilisateurs.add(mapUtilisateur(resultSet));
			}
		} catch (SQLException e) {
			logger.log(Level.WARN, MESSAGE_ERREUR, e);
		} finally {
			// fermeture des ressources utilisées
			fermetures(resultSet, preparedStatement, connection);
		}
		return utilisateurs;
	}

	// ##################################################################################################
	// # Méthodes concernant les beans Etudiant et EtudiantEquipe #
	// ##################################################################################################

	/**
	 * Liste tous les Utilisateurs qui sont des Etudiants de l'année en cours.
	 *
	 * @return utilisateursEtudiants la liste des Etudiants de l'année en cours.
	 */
	public List<Utilisateur> listerEtudiants() {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<Utilisateur> utilisateursEtudiants = new ArrayList<>();
		try {
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			preparedStatement = connection.prepareStatement(SQL_LISTER_ETUDIANT_ANNEE_EN_COURS);
			resultSet = preparedStatement.executeQuery();
			// récupération des valeurs des attributs de la BDD pour les mettre dans une
			// liste
			while (resultSet.next()) {
				Utilisateur etu = new Utilisateur();
				etu.setIdUtilisateur(resultSet.getLong(ATTRIBUT_ID_UTILISATEUR));
				etu.setNom(resultSet.getString(ATTRIBUT_NOM));
				etu.setPrenom(resultSet.getString(ATTRIBUT_PRENOM));
				utilisateursEtudiants.add(etu);
			}
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec de la recherche des étudiants.", e);
		} finally {
			fermetures(resultSet, preparedStatement, connection);
		}
		return utilisateursEtudiants;
	}

	/**
	 * Liste tous les Etudiants disponibles, c'est-à-dire ceux qui n'appartiennent à
	 * aucune équipe.
	 *
	 * @return utilisateursDisponiblesOption la liste des Etudiants disponibles.
	 */
	public List<Utilisateur> listerEtudiantsDisponibles() {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<Utilisateur> etudiantsDisponibles = new ArrayList<>();
		try {
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			preparedStatement = connection.prepareStatement(SQL_SELECT_ETUDIANTS_DISPONIBLES);
			resultSet = preparedStatement.executeQuery();
			// récupération des valeurs des attributs de la BDD pour les mettre dans une
			// liste
			while (resultSet.next()) {
				Utilisateur etu = new Utilisateur();
				etu.setIdUtilisateur(resultSet.getLong(ATTRIBUT_ID_ETUDIANT));
				etu.setNom(resultSet.getString(ATTRIBUT_NOM));
				etu.setPrenom(resultSet.getString(ATTRIBUT_PRENOM));
				etudiantsDisponibles.add(etu);
			}
		} catch (SQLException e) {
			logger.log(Level.WARN, MESSAGE, e);
		} finally {
			fermetures(resultSet, preparedStatement, connection);
		}
		return etudiantsDisponibles;
	}

	/**
	 * Liste tous les Utilisateurs qui sont des Etudiants I2 de l'année en cours.
	 *
	 * @return utilisateursEtudiants la liste des Etudiants I2 de l'année en cours.
	 */
	public List<Utilisateur> listerEtudiantsI2() {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<Utilisateur> utilisateursEtudiants = new ArrayList<>();
		try {
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			preparedStatement = connection.prepareStatement(SQL_LISTER_ETUDIANT_I2_ANNEE_EN_COURS);
			resultSet = preparedStatement.executeQuery();
			// récupération des valeurs des attributs de la BDD pour les mettre dans une
			// liste
			while (resultSet.next()) {
				Utilisateur etu = new Utilisateur();
				etu.setIdUtilisateur(resultSet.getLong(ATTRIBUT_ID_UTILISATEUR));
				etu.setNom(resultSet.getString(ATTRIBUT_NOM));
				etu.setPrenom(resultSet.getString(ATTRIBUT_PRENOM));
				utilisateursEtudiants.add(etu);
			}
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec de la recherche des étudiants.", e);
		} finally {
			fermetures(resultSet, preparedStatement, connection);
		}
		return utilisateursEtudiants;
	}
	
	/**
	 * Liste tous les Etudiants I2 disponibles, c'est-à-dire ceux qui
	 * n'appartiennent à aucune équipe.
	 *
	 * @return utilisateursDisponiblesOption la liste des Etudiants disponibles.
	 */
	public List<Utilisateur> listerEtudiantsI2DisponiblesSansEquipes() {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<Utilisateur> etudiantsDisponibles = new ArrayList<>();
		try {
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			preparedStatement = connection.prepareStatement(SQL_SELECT_ETUDIANTS_DISPONIBLES_I2_SANS_EQUIPE);
			resultSet = preparedStatement.executeQuery();
			// récupération des valeurs des attributs de la BDD pour les mettre dans une
			// liste
			while (resultSet.next()) {
				Utilisateur etu = new Utilisateur();
				etu.setIdUtilisateur(resultSet.getLong(ATTRIBUT_ID_ETUDIANT));
				etu.setNom(resultSet.getString(ATTRIBUT_NOM));
				etu.setPrenom(resultSet.getString(ATTRIBUT_PRENOM));
				etudiantsDisponibles.add(etu);
			}
		} catch (SQLException e) {
			logger.log(Level.WARN, MESSAGE, e);
		} finally {
			fermetures(resultSet, preparedStatement, connection);
		}
		return etudiantsDisponibles;
	}

	/**
	 * Liste tous les Etudiants I2 disponibles, c'est-à-dire ceux qui
	 * n'appartiennent à aucune équipe.
	 *
	 * @return utilisateursDisponiblesOption la liste des Etudiants disponibles.
	 */
	public List<Utilisateur> listerEtudiantsI2Disponibles() {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<Utilisateur> etudiantsDisponibles = new ArrayList<>();
		try {
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			preparedStatement = connection.prepareStatement(SQL_SELECT_ETUDIANTS_I2_DISPONIBLES);
			resultSet = preparedStatement.executeQuery();
			// récupération des valeurs des attributs de la BDD pour les mettre dans une
			// liste
			while (resultSet.next()) {
				Utilisateur etu = new Utilisateur();
				etu.setIdUtilisateur(resultSet.getLong(ATTRIBUT_ID_ETUDIANT));
				etu.setNom(resultSet.getString(ATTRIBUT_NOM));
				etu.setPrenom(resultSet.getString(ATTRIBUT_PRENOM));
				etudiantsDisponibles.add(etu);
			}
		} catch (SQLException e) {
			logger.log(Level.WARN, MESSAGE, e);
		} finally {
			fermetures(resultSet, preparedStatement, connection);
		}
		return etudiantsDisponibles;
	}

	/**
	 * Liste tous les Utilisateurs qui sont des Etudiants de l'année en cours dans
	 * une équipe.
	 *
	 * @return utilisateursEtudiants la liste des Etudiants dans une équipe.
	 */
	public List<Utilisateur> listerEtudiantEquipe() {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<Utilisateur> utilisateursEtudiantsEquipes = new ArrayList<>();
		try {
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			preparedStatement = connection.prepareStatement(SQL_LISTER_ETUDIANT_EQUIPE);
			resultSet = preparedStatement.executeQuery();
			// récupération des valeurs des attributs de la BDD pour les mettre dans une
			// liste
			while (resultSet.next()) {
				Utilisateur etu = new Utilisateur();
				etu.setIdUtilisateur(resultSet.getLong(ATTRIBUT_ID_UTILISATEUR));
				etu.setNom(resultSet.getString(ATTRIBUT_NOM));
				etu.setPrenom(resultSet.getString(ATTRIBUT_PRENOM));
				utilisateursEtudiantsEquipes.add(etu);
			}
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec de la recherche des étudiants dans une équipe.", e);
		} finally {
			fermetures(resultSet, preparedStatement, connection);
		}
		return utilisateursEtudiantsEquipes;
	}

	// ###################################################################################
	// # Méthodes concernant le bean Professeur #
	// ###################################################################################

	/**
	 * Liste tous les Professeurs.
	 *
	 * @return utilisateurs la liste des Professeurs.
	 */
	public List<Utilisateur> listerProfesseur() {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<Utilisateur> utilisateurs = new ArrayList<>();
		// tableau de Strings regroupant tous les noms des attributs d'un objet ainsi
		// que les valeurs correspondantes
		try {
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			// mise en forme de la requête SELECT en fonction des attributs de l'objet
			preparedStatement = connection.prepareStatement(SQL_SELECT_PROFESSEUR);
			resultSet = preparedStatement.executeQuery();
			// récupération des valeurs des attributs de la BDD pour les mettre dans une
			// liste
			while (resultSet.next()) {
				utilisateurs.add(mapUtilisateur(resultSet));
			}
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec du listage des professeurs.", e);
		} finally {
			// fermeture des ressources utilisées
			fermetures(resultSet, preparedStatement, connection);
		}
		return utilisateurs;
	}

	// ############################################################################################
	// # Méthodes concernant le bean ProfesseurSujet #
	// ############################################################################################

	/**
	 * Liste tous les utilisateurs qui correspondent au nom / prenom / identifiant
	 * passé en paramètre. Il doit correspondre au role donnée ainsi qu'à l'année
	 *
	 * @param nom
	 *            nom/prenom/identifiant à rechercher
	 * @param role
	 *            Identifiant du role a rechercher
	 * @param annee
	 *            Annee année de recherche
	 * @param nombreR
	 *            nombre de résultats à afficher
	 * @param valide
	 *            si on cherche uniquement les comptes valides
	 * @return la liste des Utilisateurs trouvés dans la BDD correspondant à
	 *         l'Utilisateur, sous forme de Hashmap
	 */
	public ResultatFiltreUtilisateur chercherUtilisateur(String nom, Integer role, Integer annee, Integer nombreR, int page, Boolean valide) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		PreparedStatement preparedStatementCount = null;
		ResultSet resultSet = null;
		ResultSet resultSetCount = null;
		List<FiltreUtilisateur> utilisateurs = new ArrayList<>();
		ResultatFiltreUtilisateur reponse = new ResultatFiltreUtilisateur();

		try {

			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();

			// Création de la requête en fonction des paramètres

			// mise en forme de la requête SELECT en fonction des attributs de l'objet
			// utilisateur
			preparedStatement = connection.prepareStatement(SQL_FILTER_FIND_UTILISATEUR, Statement.NO_GENERATED_KEYS);
			preparedStatementCount = connection.prepareStatement(SQL_FILTER_FIND_UTILISATEUR_COUNT, Statement.NO_GENERATED_KEYS);

			// Définition des paramètres
			int parameterIndex = 1;
			String nomP = "%" + nom + "%";
			String valideP = null;
			if (valide != null) {
				valideP = valide ? "oui" : "non";
			}

			// Préparation de la requete
			parameterIndex = prepareStatement(nomP, 5, preparedStatement, preparedStatementCount, parameterIndex);
			parameterIndex = prepareStatement(role, 2, preparedStatement, preparedStatementCount, parameterIndex);
			parameterIndex = prepareStatement(annee, 4, preparedStatement, preparedStatementCount, parameterIndex);
			parameterIndex = prepareStatement(valideP, 2, preparedStatement, preparedStatementCount, parameterIndex);

			// Calcul de la limite
			page = page < 1 ? 1 : page;
			preparedStatement.setInt(parameterIndex++, nombreR == null ? 0 : nombreR * (page - 1));
			preparedStatement.setInt(parameterIndex, nombreR == null ? -1 : nombreR);

			// Execution
			resultSet = preparedStatement.executeQuery();
			resultSetCount = preparedStatementCount.executeQuery();

			// récupération des valeurs des attributs de la BDD pour les mettre dans une
			// liste
			while (resultSet.next()) {
				utilisateurs.add(mapFiltreUtilisateur(resultSet));
			}

			while (resultSetCount.next()) {
				reponse.setResultats(resultSetCount.getInt("nbr"));
			}
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec de la recherche des utilisateurs.", e);
		} finally {
			// fermeture des ressources utilisées
			fermetures(resultSet, preparedStatement, connection);
			fermetures(resultSetCount, preparedStatementCount, null);
		}

		// Mise en forme de la réponse
		reponse.setFiltreUtilisateurs(utilisateurs);
		reponse.setPage(page);
		return reponse;
	}

	/**
	 * Prépare les statements de la requete ChercherUtilisateur (String)
	 *
	 * @param value
	 *            valeur a ajouter
	 * @param times
	 *            nombre de fois consécutive à ajouter
	 * @param preparedStatement
	 *            Statement preparé de la requete
	 * @param preparedStatementCount
	 *            Statement préparé de la requete d'occurence
	 * @param parameterIndex
	 *            Index de préparation
	 * @return nouvel index de préparation
	 * @throws SQLException
	 *             Requete mal formulée
	 */
	private int prepareStatement(String value, int times, PreparedStatement preparedStatement, PreparedStatement preparedStatementCount, int parameterIndex) throws SQLException {
		return this.prepareStatement(String.class, value, times, preparedStatement, preparedStatementCount, parameterIndex);
	}

	/**
	 * Prépare les statements de la requete ChercherUtilisateur (Integer)
	 *
	 * @param value
	 *            valeur a ajouter
	 * @param times
	 *            nombre de fois consécutive à ajouter
	 * @param preparedStatement
	 *            Statement preparé de la requete
	 * @param preparedStatementCount
	 *            Statement préparé de la requete d'occurence
	 * @param parameterIndex
	 *            Index de préparation
	 * @return nouvel index de préparation
	 * @throws SQLException
	 *             Requete mal formulée
	 */
	private int prepareStatement(Integer value, int times, PreparedStatement preparedStatement, PreparedStatement preparedStatementCount, int parameterIndex) throws SQLException {
		return this.prepareStatement(Integer.class, value, times, preparedStatement, preparedStatementCount, parameterIndex);
	}

	/**
	 * Prépare les statements de la requete ChercherUtilisateur
	 *
	 * @param value
	 *            valeur a ajouter
	 * @param times
	 *            nombre de fois consécutive à ajouter
	 * @param type
	 *            type de valeur
	 * @param preparedStatement
	 *            Statement preparé de la requete
	 * @param preparedStatementCount
	 *            Statement préparé de la requete d'occurence
	 * @param parameterIndex
	 *            Index de préparation
	 * @return nouvel index de préparation
	 * @throws SQLException
	 *             Requete mal formulée
	 */
	private int prepareStatement(Class type, Object value, int times, PreparedStatement preparedStatement, PreparedStatement preparedStatementCount, int parameterIndex) throws SQLException {
		Integer typeNull = type.equals(Integer.class.getClass()) ? Types.INTEGER : Types.VARCHAR;
		for (int i = 0; i < times; i++) {
			if (value != null) {
				if (type.equals(String.class)) {
					preparedStatementCount.setString(parameterIndex, (String) value);
					preparedStatement.setString(parameterIndex++, (String) value);
				} else if (type.equals(Integer.class)) {
					preparedStatementCount.setInt(parameterIndex, (Integer) value);
					preparedStatement.setInt(parameterIndex++, (Integer) value);
				}
			} else {
				preparedStatementCount.setNull(parameterIndex, typeNull);
				preparedStatement.setNull(parameterIndex++, typeNull);
			}

		}
		return parameterIndex;
	}

	/**
	 * Liste tous les ProfesseurSujet.
	 *
	 * @return professeurSujets liste de tous les ProfesseurSujet.
	 */
	public List<ProfesseurSujet> listerProfesseurSujet() {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<ProfesseurSujet> professeurSujets = new ArrayList<>();
		// tableau de Strings regroupant tous les noms des attributs d'un objet ainsi
		// que les valeurs correspondantes
		try {
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			// mise en forme de la requête SELECT en fonction des attributs de l'objet
			preparedStatement = connection.prepareStatement(SQL_SELECT_PROFESSEUR_SUJET);
			resultSet = preparedStatement.executeQuery();
			// récupération des valeurs des attributs de la BDD pour les mettre dans une
			// liste
			while (resultSet.next()) {
				professeurSujets.add(ProfesseurSujetDAO.map(resultSet));
			}
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec du listage des professeurSujet.", e);
		} finally {
			// fermeture des ressources utilisées
			fermetures(resultSet, preparedStatement, connection);
		}
		return professeurSujets;
	}

	/**
	 * Liste les ProfesseurSujets qui sont attribués à un Sujet.
	 *
	 * @param sujet
	 *            le Sujet concerné.
	 */
	public List<ProfesseurSujet> trouverProfesseursAttribuesAUnSujet(Sujet sujet) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<ProfesseurSujet> professeurSujets = new ArrayList<>();
		// tableau de Strings regroupant tous les noms des attributs d'un objet ainsi
		// que les valeurs correspondantes
		try {
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			// mise en forme de la requête SELECT en fonction des attributs de l'objet
			preparedStatement = connection.prepareStatement(SQL_SELECT_PROFESSEURS_ATTRIBUES_A_UN_SUJET, Statement.NO_GENERATED_KEYS);
			preparedStatement.setLong(1, sujet.getIdSujet());
			resultSet = preparedStatement.executeQuery();
			// récupération des valeurs des attributs de la BDD pour les mettre dans une
			// liste
			while (resultSet.next()) {
				professeurSujets.add(ProfesseurSujetDAO.map(resultSet));
			}
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec de la recherche des professeurSujet attribués à un sujet.", e);
		} finally {
			// fermeture des ressources utilisées
			fermetures(resultSet, preparedStatement, connection);
		}
		return professeurSujets;
	}

	/**
	 * Liste les ProfesseurSujet qui sont attribués à un Utilisateur.
	 *
	 * @param utilisateur
	 *            l'utilisateur concerné.
	 */
	public List<ProfesseurSujet> trouverProfesseursSujet(Utilisateur utilisateur) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<ProfesseurSujet> professeurSujets = new ArrayList<>();
		// tableau de Strings regroupant tous les noms des attributs d'un objet ainsi
		// que les valeurs correspondantes
		try {
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			// mise en forme de la requête SELECT en fonction des attributs de l'objet
			preparedStatement = connection.prepareStatement(SQL_SELECT_PROFESSEURS_SUJETS, Statement.NO_GENERATED_KEYS);
			preparedStatement.setLong(1, utilisateur.getIdUtilisateur());
			resultSet = preparedStatement.executeQuery();
			// récupération des valeurs des attributs de la BDD pour les mettre dans une
			// liste
			while (resultSet.next()) {
				professeurSujets.add(ProfesseurSujetDAO.map(resultSet));
			}
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec de la recherche des professeurSujet attribués à un utilisateur.", e);
		} finally {
			// fermeture des ressources utilisées
			fermetures(resultSet, preparedStatement, connection);
		}
		return professeurSujets;
	}

	/**
	 * Liste les Professeurs d'option qui attenndent la validation pour être
	 * référent sur un projet.
	 *
	 * @return professeursAValider la liste des professeurs à valider.
	 */
	public List<Utilisateur> trouverProfesseurAValider() {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<Utilisateur> professeursAValider = new ArrayList<>();

		try {
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			// mise en forme de la requête SELECT en fonction des attributs de l'objet sujet
			preparedStatement = connection.prepareStatement(SQL_SELECT_PROFESSEURS_A_VALIDER, Statement.NO_GENERATED_KEYS);
			resultSet = preparedStatement.executeQuery();
			// récupération des valeurs des attributs de la BDD pour les mettre dans une
			// liste
			while (resultSet.next()) {
				professeursAValider.add(mapUtilisateur(resultSet));
			}
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec de la recherche des professeurs à valider.", e);
		} finally {
			// fermeture des ressources utilisées
			fermetures(resultSet, preparedStatement, connection);
		}
		return professeursAValider;
	}

	/**
	 * Liste les Professeurs d'option qui ne sont référent sur aucun sujet.
	 *
	 * @return professeursSansSujet la liste des professeurs sans sujet.
	 */
	public List<Utilisateur> trouverProfesseurSansSujet() {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<Utilisateur> professeursSansSujet = new ArrayList<>();

		try {
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			// mise en forme de la requête SELECT en fonction des attributs de l'objet
			preparedStatement = connection.prepareStatement(SQL_SELECT_PROFESSEURS_A_ATTRIBUER, Statement.NO_GENERATED_KEYS);
			resultSet = preparedStatement.executeQuery();
			// récupération des valeurs des attributs de la BDD pour les mettre dans une
			// liste
			while (resultSet.next()) {
				professeursSansSujet.add(mapUtilisateur(resultSet));
			}
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec de la recherche des professeurs sans sujet.", e);
		} finally {
			// fermeture des ressources utilisées
			fermetures(resultSet, preparedStatement, connection);
		}
		return professeursSansSujet;
	}

	/**
	 * Liste les ProfesseurSujet dont la fonction est spécifiée dans le
	 * professeurSujet passé en paramètre.
	 *
	 * @param fonction
	 *            la fonction du professeur
	 */
	public List<ProfesseurSujet> listerProfesseursSujetsFonction(String fonction) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<ProfesseurSujet> professeursFonction = new ArrayList<>();
		try {
			connection = this.getDaoFactory().getConnection();
			preparedStatement = connection.prepareStatement(SQL_SELECT_PROFESSEUR_FONCTION);
			preparedStatement.setString(1, fonction);
			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				professeursFonction.add(ProfesseurSujetDAO.map(resultSet));
			}
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec de la recherche des professeurs pour une fonction donnée.", e);
		} finally {
			// fermeture des ressources utilisées
			fermetures(resultSet, preparedStatement, connection);
		}
		return professeursFonction;
	}

	public Noter.RoleNotation checkRoleNotationUtilisateur(Integer idUtilisateur) {
		ResultSet resultSet = null;
		Noter.RoleNotation role = Noter.RoleNotation.AUCUN;

		try (Connection connection = this.getDaoFactory().getConnection(); PreparedStatement preparedStatement = connection.prepareStatement(SQL_CHECKROLENOTATION)) {
			preparedStatement.setInt(1, idUtilisateur);
			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				boolean isScrumMaster = resultSet.getInt("isScrumMaster") > 0;
				boolean isProfResp = resultSet.getInt("isProfResp") > 0;
				boolean isProductOwner = resultSet.getInt("isProductOwner") > 0;

				if (isScrumMaster) {
					role = Noter.RoleNotation.SCRUMMASTER;
				} else if (isProductOwner) {
					role = Noter.RoleNotation.PRODUCTOWNER;
				} else if (isProfResp) {
					role = Noter.RoleNotation.PROFREFERENT;
				}
			}
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec de la recherche des droits utilisateurs", e);
		} finally {
			// fermeture des ressources utilisées
			fermeture(resultSet);
		}

		return role;
	}

	public Map<Utilisateur, ENJNotes> recupererNotesUtilisateursPFE(Integer idOption, Integer anneeScolaire) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		Map<Utilisateur, ENJNotes> notesUtilisateurs = new HashMap<>();
		try {
			connection = this.getDaoFactory().getConnection();
			preparedStatement = connection.prepareStatement(SQL_TROUVER_NOTES_UTILISATEURS);
			if (idOption == null) {
				preparedStatement.setNull(1, Types.INTEGER);
				preparedStatement.setNull(2, Types.INTEGER);
			} else {
				preparedStatement.setInt(1, idOption);
				preparedStatement.setInt(2, idOption);
			}

			preparedStatement.setInt(3, anneeScolaire);
			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				ENJNotes note = new ENJNotes();
				Utilisateur u = mapUtilisateur(resultSet);

				note.addNote(new ENJMatiere("Poster", 0.40f), resultSet.getFloat(ATTRIBUT_NOTEPOSTER));
				note.addNote(new ENJMatiere("Soutenance", 0.60f), resultSet.getFloat(ATTRIBUT_NOTESOUTENANCE));

				notesUtilisateurs.put(u, note);
			}
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec de la recherche des notes des utilisateurs.", e);
		} finally {
			// fermeture des ressources utilisées
			fermetures(resultSet, preparedStatement, connection);
		}
		return notesUtilisateurs;
	}

	// ###########################################################################################################
	// # Méthodes liant Utilisateur et Meeting
	// ###########################################################################################################

	/**
	 * Liste les Utilisateurs attribués à un Sujet donné.
	 *
	 * @param meeting
	 *            le Meeting dont on souhaite connaître les Utilisateurs qui y sont
	 *            attribués.
	 * @return utilisateurs la liste des Utilisateurs attribués au Meeting.
	 */
	public List<Utilisateur> trouverCreateurReunion(Meeting meeting) {
		return getUtilisateurs(meeting, SQL_SELECT_CREATEUR_MEETING);
	}

	/**
	 * Liste les Utilisateurs attribués à un Meeting donné.
	 *
	 * @param meeting
	 *            le Meeting dont on souhaite connaître les Utilisateurs qui y sont
	 *            attribués.
	 * @return utilisateurs la liste des Utilisateurs attribués au Meeting.
	 */
	public List<Utilisateur> trouverInvitesReunion(Meeting meeting) {
		return getUtilisateurs(meeting, SQL_SELECT_INVITE_MEETING2);
	}

	private List<Utilisateur> getUtilisateurs(Meeting meeting, String sqlSelectInviteMeeting2) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<Utilisateur> utilisateurs = new ArrayList<>();
		try {
			// vérification que l'utilisateur rentré n'est pas nul
			if (meeting.getIdReunion() == null) {
				throw new SQLException();
			}
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			// mise en forme de la requête SELECT en fonction des attributs de l'objet sujet
			preparedStatement = initialisationRequetePreparee(connection, sqlSelectInviteMeeting2, false, meeting.getIdReunion());
			resultSet = preparedStatement.executeQuery();
			// récupération des valeurs des attributs de la BDD pour les mettre dans une
			// liste
			while (resultSet.next()) {
				utilisateurs.add(mapUtilisateur(resultSet));
			}
		} catch (SQLException e) {
			logger.log(Level.WARN, MESSAGE_ERREUR, e);
		} finally {
			// fermeture des ressources utilisées
			fermetures(resultSet, preparedStatement, connection);
		}
		return utilisateurs;
	}
	// #################################################
	// # Méthodes privées #
	// #################################################

	/**
	 * Fait la correspondance (le mapping) entre une ligne issue de la table
	 * Utilisateur (un ResultSet) et un bean Utilisateur.
	 *
	 * @param resultSet
	 *            la ligne issue de la table Utilisateur.
	 * @return utilisateur le bean dont on souhaite faire la correspondance.
	 * @throws SQLException
	 *             En cas d'erreur de requête
	 */
	public static Utilisateur mapUtilisateur(ResultSet resultSet) throws SQLException {
		Utilisateur utilisateur = new Utilisateur();
		utilisateur.setIdUtilisateur(resultSet.getLong(ATTRIBUT_ID_UTILISATEUR));
		utilisateur.setNom(resultSet.getString(ATTRIBUT_NOM));
		utilisateur.setPrenom(resultSet.getString(ATTRIBUT_PRENOM));
		utilisateur.setIdentifiant(resultSet.getString(ATTRIBUT_IDENTIFIANT));
		utilisateur.setHash(resultSet.getString(ATTRIBUT_HASH));
		utilisateur.setEmail(resultSet.getString(ATTRIBUT_EMAIL));
		utilisateur.setValide(resultSet.getString(ATTRIBUT_VALIDE));
		return utilisateur;
	}

	/**
	 * Fait la correspondance (le mapping) entre une ligne issue de la table
	 * Utilisateur (un ResultSet), de la table roleutilisateur et de la table
	 * etudiant et un bean Utilisateur.
	 *
	 * @param resultSet
	 *            la ligne issue des 3 tables.
	 * @return utilisateur le bean dont on souhaite faire la correspondance.
	 * @throws SQLException
	 *             En cas d'erreur de requête
	 */
	public static FiltreUtilisateur mapFiltreUtilisateur(ResultSet resultSet) throws SQLException {
		FiltreUtilisateur utilisateur = new FiltreUtilisateur();
		utilisateur.setIdUtilisateur(resultSet.getLong(ATTRIBUT_ID_UTILISATEUR));
		utilisateur.setNom(resultSet.getString(ATTRIBUT_NOM));
		utilisateur.setPrenom(resultSet.getString(ATTRIBUT_PRENOM));
		utilisateur.setIdentifiant(resultSet.getString(ATTRIBUT_IDENTIFIANT));
		utilisateur.setHash("none");
		utilisateur.setEmail(resultSet.getString(ATTRIBUT_EMAIL));
		utilisateur.setValide(resultSet.getString(ATTRIBUT_VALIDE));
		utilisateur.setAnnee(resultSet.getInt(ATTRIBUT_ANNEE));

		// Convertir id+nom id2+nom2 => Liste<Role>
		List<Role> roles = new ArrayList<>();
		String rolesStr = resultSet.getString(ATTRIBUT_ROLES);
		if (rolesStr != null && rolesStr.length() > 0) {
			String[] rolesId = rolesStr.split(",");
			for (String roleid : rolesId) {
				Role role = new Role();
				role.setIdRole(Long.parseLong(roleid.split("\\+")[0]));
				role.setNomRole(roleid.split("\\+")[1]);
				roles.add(role);
			}
			utilisateur.setRoles(roles);
		}
		return utilisateur;
	}

}