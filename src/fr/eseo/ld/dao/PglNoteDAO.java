package fr.eseo.ld.dao;

import fr.eseo.ld.beans.Utilisateur;
import fr.eseo.ld.objets.exportexcel.ENJMatiere;
import fr.eseo.ld.objets.exportexcel.ENJNotes;
import fr.eseo.ld.pgl.beans.Equipe;
import fr.eseo.ld.pgl.beans.Matiere;
import fr.eseo.ld.pgl.beans.Note;
import fr.eseo.ld.pgl.beans.Sprint;
import fr.eseo.ld.utils.ParamUtilitaire;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static fr.eseo.ld.dao.DAOUtilitaire.*;

/**
 * Classe du DAO du bean Note (Pgl).
 *
 * <p>
 * Cette classe implémente toutes les méthodes CRUD définies dans sa classe
 * mère.
 * </p>
 */
public class PglNoteDAO extends DAO<Note> {
    /* Noms des entités */
    private static final String NOM_ENTITE = "pgl_note";

    /* Attributs de l'entité Note */
    private static final String ATTRIBUT_ID_NOTEEQUIPE = "idNote";
    private static final String ATTRIBUT_NOTE = "note";
    private static final String ATTRIBUT_REF_MATIERE = "refMatiere";
    private static final String ATTRIBUT_REF_EVALUATEUR = "refEvaluateur";
    private static final String ATTRIBUT_REF_ETUDIANT = "refEtudiant";

    private static final String ATTRIBUT_ID_UTILISATEUR = "idUtilisateur";
    private static final String ATTRIBUT_ID_MATIERE = "idMatiere";
    private static final String ATTRIBUT_NOM = "nom";
    private static final String ATTRIBUT_PRENOM = "prenom";
    private static final String ATTRIBUT_LIBELLE = "libelle";
    private static final String ATTRIBUT_COEFF = "coefficient";
    private static final String ATTRIBUT_NOTE_ID_NOTE = ATTRIBUT_ID_NOTEEQUIPE;
    private static final String ATTRIBUT_REF_SPRINT = "idSprint";


    private static final String[] ATTRIBUTS_NOMS = {ATTRIBUT_ID_NOTEEQUIPE, ATTRIBUT_NOTE, ATTRIBUT_REF_MATIERE, ATTRIBUT_REF_ETUDIANT,
            ATTRIBUT_REF_EVALUATEUR};


    /* Constantes pour éviter la duplication de code */
    private static final String DELETE = "DELETE";
    private static final String ATTRIBUT_SPRINT_COEFFICIENT = ATTRIBUT_COEFF;
    private static final String ATTRIBUT_NOM_EQUIPE = "nomEquipe";
    private static final String ATTRIBUT_ID_EQUIPE = "idEquipe";


    private static Logger logger = Logger.getLogger(PglNoteDAO.class.getName());

    /* Requetes SQL */
    private static final String SQL_INSERT_SPRINT = "INSERT INTO `pgl_note`(`note`, `refMatiere`, `refEtudiant`, `refEvaluateur`) VALUES (?,?,?,?)";
    private static final String SQL_SELECT_TOUT = "SELECT * FROM pgl_note";

    private static final String SQL_NOTES_UTILISATEUR =
            "SELECT * FROM pgl_moyennesetudiantsmatieres " +
                    "WHERE refEtudiant = ? " +
                    "AND (anneeDebut = ? OR ? IS NULL) " +
                    "AND (idSprint = ? OR ? IS NULL)";

    private static final String SQL_NOTES_UTILISATEUR_PAR_SPRINT =
            "SELECT refEtudiant, idSprint, libelle, coefficient, numero, nom, prenom, note, bonusMalus " +
                    "FROM pgl_moyennesetudiantssprint " +
                    "WHERE anneeDebut = ? ";

    private static final String SQL_NOTES_EQUIPE_PAR_SPRINT =
            "SELECT e.idEquipe, e.nom nomEquipe, me.idSprint, me.libelle, me.coefficient, " +
                    "  AVG(me.note) note, COUNT(me.refEtudiant) nbMembres " +
                    " FROM pgl_moyennesetudiantssprint me " +
                    "  JOIN pgl_etudiantequipe ee ON ee.idEtudiant = me.refEtudiant " +
                    "  JOIN pgl_equipe e on ee.idEquipe = e.idEquipe " +
                    "  WHERE me.anneeDebut = ?" +
                    "  GROUP BY e.idEquipe, me.idSprint";

    private static final String SQL_NOTES_ETUDIANTSEQUIPE = "" +
            "SELECT idAnneeScolaire, idSprint, idEquipe, idUtilisateur, nom, prenom, NULL idNote, NULL note, idMatiere, libelle, coefficient " +
            "FROM pgl_matieresetudiantevaluees " +
            "WHERE (idUtilisateur = ? OR ? IS NULL) " +
            "      AND (idAnneeScolaire = ? OR ? IS NULL) " +
            "      AND (idSprint = ? OR ? IS NULL) " +
            "      AND (idEquipe = ? OR ? IS NULL) " +
            "      AND (idMatiere = ? OR ? IS NULL) " +
            "      AND (FIND_IN_SET(?, evaluateurs) = 0 OR FIND_IN_SET(?, evaluateurs) IS NULL) " +
            "UNION " +
            "SELECT idAnneeScolaire, idSprint, idEquipe, idUtilisateur, nom, prenom, idNote, note, idMatiere, libelle, coefficient " +
            "FROM pgl_notesetudiants " +
            "WHERE idEvaluateur = ? " +
            "      AND (idAnneeScolaire = ? OR ? IS NULL) " +
            "      AND (idSprint = ? OR ? IS NULL) " +
            "      AND (idEquipe = ? OR ? IS NULL) " +
            "      AND (idMatiere = ? OR ? IS NULL) " +
            "      AND (idUtilisateur = ? OR ? IS NULL) " +
            "ORDER BY idUtilisateur ASC";

    /* Requetes SQL pour trouver une note à partir de son ID */
    private static final String SQL_SELECT_NOTEEQUIPE_A_PARTIR_ID = "SELECT * FROM pgl_note WHERE idNote=?";


    /**
     * Constructeur de DAO.
     *
     * @param daoFactory la Factory permettant la création d'une connexion à la BDD.
     */
    PglNoteDAO(DAOFactory daoFactory) {
        super(daoFactory);
    }

    /**
     * Insère une Note dans la BDD à partir des attributs spécifiés dans un bean
     * NoteE.
     *
     * @param note la noteEquipe que l'on souhaite insérer dans la BDD à partir du bean
     *             noteEquipe.
     */
    @Override
    public void creer(Note note) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            // création d'une connexion grâce à la DAOFactory placée en attribut de la
            // classe
            connection = this.creerConnexion();
            // les "?" de la requête sont comblés par les attributs de l'objet en paramètre
            // ceux-ci peuvent être nuls à condition que la BDD accepte les données de type
            // "null"
            preparedStatement = initialisationRequetePreparee(connection, SQL_INSERT_SPRINT, true,
                    note.getNote(), note.getRefMatiere(), note.getRefEtudiant(), note.getRefEvaluateur());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            logger.log(Level.WARN, "Échec de la création de l'objet, aucune ligne ajoutée dans la table.", e);
        } finally {
            // fermeture des ressources utilisées
            fermetures(preparedStatement, connection);
        }
    }

    /**
     * Liste tous les Note ayant pour attributs les mêmes que ceux spécifiés dans
     * un bean Note.
     *
     * @param note le noteEquipe que l'on souhaite trouver dans la BDD.
     * @return noetEquipe la liste des noteEquipe trouvés dans la BDD.
     */
    @Override
    public List<Note> trouver(Note note) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        List<Note> noteEquipes = new ArrayList<>();
        // tableau de Strings regroupant tous les noms des attributs d'un objet ainsi
        // que les valeurs correspondantes
        String[][] attributs = {ATTRIBUTS_NOMS,
                {String.valueOf(note.getIdNote()), String.valueOf(note.getNote()), String.valueOf(note.getRefMatiere()),
                        String.valueOf(note.getRefEtudiant()),
                        String.valueOf(note.getRefEvaluateur())
                }};
        try {
            // création d'une connexion grâce à la DAOFactory placée en attribut de la classe
            connection = this.creerConnexion();
            // mise en forme de la requête SELECT en fonction des attributs de l'objet sujet

            preparedStatement = initialisationRequetePreparee(connection, "SELECT", NOM_ENTITE, attributs, true);
            resultSet = preparedStatement.executeQuery();
            // récupération des valeurs des attributs de la BDD pour les mettre dans une liste
            while (resultSet.next()) {
                noteEquipes.add(map(resultSet));
            }
        } catch (SQLException e) {
            logger.log(Level.WARN, "Échec de la recherche de l'objet Sprint. ", e);
        } finally {
            fermetures(resultSet, preparedStatement, connection);
        }
        return noteEquipes;
    }

    /**
     * Renvoie une Note à partir de son ID.
     *
     * @param idNote l'ID de la Note que l'on souhaite trouver dans la BDD.
     * @return noteEquipe la noteEquipe trouvé dans la BDD.
     */
    public Note trouver(Long idNote) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Note noteEquipe = null;
        try {
            // création d'une connexion grâce à la DAOFactory placée en attribut de la
            // classe
            connection = this.creerConnexion();
            preparedStatement = initialisationRequetePreparee(connection, SQL_SELECT_NOTEEQUIPE_A_PARTIR_ID, true,
                    String.valueOf(idNote));
            resultSet = preparedStatement.executeQuery();
            resultSet.next();
            noteEquipe = map(resultSet);
        } catch (SQLException e) {
            logger.log(Level.WARN, "Échec de la recherche d'un sprint à partir de son ID.", e);
        } finally {
            // fermeture des ressources utilisées
            fermetures(resultSet, preparedStatement, connection);
        }
        return noteEquipe;
    }


    /**
     * Modifie une Note ayant pour attributs les mêmes que ceux spécifiés dans un
     * bean Note et la même clé primaire. Cette clé primaire ne peut être
     * modifiée.
     *
     * @param noteEquipe la noteEquipe que l'on souhaite modifier dans la BDD.
     */
    @Override
    public void modifier(Note noteEquipe) {
        //Update table SET <....>
        Map<String, Object> attributs = new HashMap<>();
        attributs.put(ATTRIBUT_NOTE, noteEquipe.getNote());
        attributs.put(ATTRIBUT_REF_MATIERE, noteEquipe.getRefMatiere());
        attributs.put(ATTRIBUT_REF_ETUDIANT, noteEquipe.getRefEtudiant());
        attributs.put(ATTRIBUT_REF_EVALUATEUR, noteEquipe.getRefEvaluateur());

        //Preparation de WHERE
        Map<String, Object> whereAtt = new HashMap<>();
        whereAtt.put(ATTRIBUT_ID_NOTEEQUIPE, noteEquipe.getIdNote());

        //Modifier
        DAOUtilitaire.executeUpdate(this.getDaoFactory(), NOM_ENTITE, attributs, whereAtt);
    }

    /**
     * Supprime tous les Notes ayant pour attributs les mêmes que ceux spécifiés
     * dans un bean Note.
     *
     * @param noteEquipe la noteEquipe que l'on souhaite supprimer dans la BDD.
     */
    @Override
    public void supprimer(Note noteEquipe) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        // tableau de Strings regroupant tous les noms des attributs d'un objet ainsi
        // que les valeurs correspondantes
        String[][] attributs = {ATTRIBUTS_NOMS,
                {String.valueOf(noteEquipe.getIdNote()), String.valueOf(noteEquipe.getNote()), String.valueOf(noteEquipe.getRefMatiere()),
                        String.valueOf(noteEquipe.getRefEtudiant()),
                        String.valueOf(noteEquipe.getRefEvaluateur())}};
        try {
            // création d'une connexion grâce à la DAOFactory placée en attribut de la
            // classe
            connection = this.creerConnexion();
            // mise en forme de la requête DELETE en fonction des attributs de l'objet
            // utilisateur
            preparedStatement = initialisationRequetePreparee(connection, DELETE, NOM_ENTITE, attributs, false);
            int statut = preparedStatement.executeUpdate();
            if (statut == 0) {
                throw new SQLException();
            } else {
                //suppression du bean
                noteEquipe.setIdNote(null);
            }
        } catch (SQLException e) {
            logger.log(Level.WARN, "Échec de la suppression de l'objet Note, aucune ligne supprimée de la table pgl_Note.", e);
        } finally {
            // fermeture des ressources utilisées
            fermetures(preparedStatement, connection);
        }

    }

    /**
     * Récupère la moyenne de chaque note pour chaque matière de l'utilisateur passé en paramètre
     * pour le sprint donné
     *
     * @param utilisateur Utilisateur
     * @param sprint      Sprint
     * @return Liste des notes
     */
    public List<Note> recupererNotesMoyennesPglUtilisateur(Utilisateur utilisateur, Sprint sprint) {
        return recupererNotesMoyennesPglUtilisateur(utilisateur, sprint, null);
    }

    /**
     * Récupère la moyenne de chaque note pour chaque matière de l'utilisateur passé en paramètre
     * pour l'année courante
     *
     * @param utilisateur Utilisateur
     * @return Liste des notes
     */
    public List<Note> recupererNotesMoyennesPglUtilisateur(Utilisateur utilisateur) {
        return recupererNotesMoyennesPglUtilisateur(utilisateur, ParamUtilitaire.getAnneeCourante());
    }

    /**
     * Récupère la moyenne de chaque note pour chaque matière de l'utilisateur passé en paramètre
     * pour l'année choisie (Si on souhaite 2017-2018, il faut préciser 2017)
     *
     * @param utilisateur Utilisateur
     * @param anneeDebut  Année de début (pour 2017-2018, choisir 2017)
     * @return Liste des notes
     */
    public List<Note> recupererNotesMoyennesPglUtilisateur(Utilisateur utilisateur, Integer anneeDebut) {
        return recupererNotesMoyennesPglUtilisateur(utilisateur, null, anneeDebut);
    }

    /**
     * Récupère la moyenne de chaque note pour chaque matière de l'utilisateur passé en paramètre
     * pour l'année choisie (Si on souhaite 2017-2018, il faut préciser 2017)
     *
     * @param utilisateur Utilisateur
     * @param anneeDebut  Année de début (pour 2017-2018, choisir 2017)
     * @param sprint      Sprint
     * @return Liste des notes
     */
    private List<Note> recupererNotesMoyennesPglUtilisateur(Utilisateur utilisateur, Sprint sprint, Integer anneeDebut) {
        List<Note> notes = new ArrayList<>();
        ResultSet resultSet = null;
        try (Connection connection = getDaoFactory().getConnection(); PreparedStatement preparedStatement = connection.prepareStatement(SQL_NOTES_UTILISATEUR)) {
            int index = 1;
            preparedStatement.setLong(index++, utilisateur.getIdUtilisateur());
            //Définir année
            if (anneeDebut != null) {
                preparedStatement.setString(index++, "" + anneeDebut);
                preparedStatement.setString(index++, "" + anneeDebut);
            } else {
                preparedStatement.setNull(index++, Types.INTEGER);
                preparedStatement.setNull(index++, Types.INTEGER);
            }

            //Définir Sprint
            if (sprint != null) {
                preparedStatement.setLong(index++, sprint.getIdSprint());
                preparedStatement.setLong(index++, sprint.getIdSprint());
            } else {
                preparedStatement.setNull(index++, Types.VARCHAR);
                preparedStatement.setNull(index++, Types.VARCHAR);
            }


            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                notes.add(map(resultSet));
            }
        } catch (SQLException e) {
            logger.log(Level.WARN, "Impossible de récupérer les notes utilisateur", e);
        } finally {
            fermeture(resultSet);
        }

        return notes;
    }

    /**
     * Récupère la moyenne de chaque note pour chaque spritn de chaque étudiant
     * pour l'année choisie (Si on souhaite 2017-2018, il faut préciser 2017)
     *
     * @param anneeDebut Année de début (pour 2017-2018, choisir 2017)
     * @return Liste des notes
     */
    public Map<Utilisateur, ENJNotes> recupererNotesMoyennesPglUtilisateurParSprint(Integer anneeDebut) {
        Map<Integer, Utilisateur> notesU = new HashMap<>();
        Map<Integer, ENJNotes> notesN = new HashMap<>();
        Map<Utilisateur, ENJNotes> notes = new HashMap<>();


        ResultSet resultSet = null;
        try (Connection connection = getDaoFactory().getConnection(); PreparedStatement preparedStatement = connection.prepareStatement(SQL_NOTES_UTILISATEUR_PAR_SPRINT)) {
            preparedStatement.setInt(1, anneeDebut);

            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                ENJNotes enjNotes = new ENJNotes();
                Utilisateur utilisateur = new Utilisateur();
                utilisateur.setIdUtilisateur(resultSet.getLong(ATTRIBUT_REF_ETUDIANT));
                utilisateur.setNom(resultSet.getString(ATTRIBUT_NOM));
                utilisateur.setPrenom(resultSet.getString(ATTRIBUT_PRENOM));

                if (notesN.containsKey(utilisateur.getIdUtilisateur().intValue())) {
                    enjNotes = notesN.get(utilisateur.getIdUtilisateur().intValue());
                }

                enjNotes.addNote(new ENJMatiere(resultSet.getString(ATTRIBUT_LIBELLE),
                                resultSet.getFloat(ATTRIBUT_SPRINT_COEFFICIENT)),
                        resultSet.getFloat(ATTRIBUT_NOTE));

                notesU.put(utilisateur.getIdUtilisateur().intValue(), utilisateur);
                notesN.put(utilisateur.getIdUtilisateur().intValue(), enjNotes);
            }
        } catch (SQLException e) {
            logger.log(Level.WARN, "Impossible de récupérer les notes utilisateur de l'année par sprint", e);
        } finally {
            fermeture(resultSet);
        }

        for (Map.Entry<Integer, Utilisateur> entry : notesU.entrySet()) {
            notes.put(entry.getValue(), notesN.get(entry.getKey()));
        }

        return notes;
    }

    /**
     * Récupère la moyenne des note par sprint pour chaque équipe pour chaque sprint de l'année
     * @see ParamUtilitaire#getAnneeCourante()
     * @param anneeDebut Année de début
     * @return Notes par equipe par sprint
     */
    public Map<Equipe, ENJNotes> recupererNotesMoyenneEquipeSprints(Integer anneeDebut) {
        Map<Integer, Equipe> notesU = new HashMap<>();
        Map<Integer, ENJNotes> notesN = new HashMap<>();
        Map<Equipe, ENJNotes> notes = new HashMap<>();


        ResultSet resultSet = null;
        try (Connection connection = getDaoFactory().getConnection(); PreparedStatement preparedStatement = connection.prepareStatement(SQL_NOTES_EQUIPE_PAR_SPRINT)) {
            preparedStatement.setInt(1, anneeDebut);

            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                ENJNotes enjNotes = new ENJNotes();
                Equipe equipe = new Equipe();
                equipe.setNom(resultSet.getString(ATTRIBUT_NOM_EQUIPE));
                equipe.setIdEquipe(resultSet.getLong(ATTRIBUT_ID_EQUIPE));

                if (notesN.containsKey(equipe.getIdEquipe().intValue())) {
                    enjNotes = notesN.get(equipe.getIdEquipe().intValue());
                }

                enjNotes.addNote(new ENJMatiere(resultSet.getString(ATTRIBUT_LIBELLE),
                                resultSet.getFloat(ATTRIBUT_SPRINT_COEFFICIENT)),
                        resultSet.getFloat(ATTRIBUT_NOTE));

                notesU.put(equipe.getIdEquipe().intValue(), equipe);
                notesN.put(equipe.getIdEquipe().intValue(), enjNotes);
            }
        } catch (SQLException e) {
            logger.log(Level.WARN, "Impossible de récupérer les notes utilisateur de l'année par sprint", e);
        } finally {
            fermeture(resultSet);
        }

        for (Map.Entry<Integer, Equipe> entry : notesU.entrySet()) {
            notes.put(entry.getValue(), notesN.get(entry.getKey()));
        }

        return notes;
    }


    /**
     * Recupere la liste des notes à noter et notées pour un évaluateur donné
     *
     * @param utilisateur     Utilisateur spécifié (Peut être null)
     * @param evaluateur      Evaluateur
     * @param idAnneeScolaire idAnnee spécifiée (Peut être null)
     * @param idSprint        idSprint spécifié (Peut être null)
     * @param idEquipe        idEquipe spécifié (Peut être null)
     * @param idMatiere       idMatiere spécifiée (Peut être null)
     * @return [Utilisateur -> (Mat1 -> note1, Mat2-> note2, etc...), Utilisateur2....]
     */
    public Map<Utilisateur, Map<Matiere, Note>> recupererMatieresANoterUtilisateur(Utilisateur utilisateur, Utilisateur evaluateur, Integer idAnneeScolaire, Integer idSprint, Integer idEquipe, Integer idMatiere) {
        Map<Utilisateur, Map<Matiere, Note>> resultats = new HashMap<>();

        ResultSet resultSet = null;
        try (Connection connection = getDaoFactory().getConnection(); PreparedStatement preparedStatement = connection.prepareStatement(SQL_NOTES_ETUDIANTSEQUIPE)) {
            this.preparerRecupererNotesANoter(preparedStatement, utilisateur, evaluateur, idAnneeScolaire, idSprint, idEquipe, idMatiere);

            //Executer la requete
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                Utilisateur u = new Utilisateur();
                Note n = new Note();
                Matiere m = new Matiere();
                u.setIdUtilisateur(resultSet.getLong(ATTRIBUT_ID_UTILISATEUR));
                u.setNom(resultSet.getString(ATTRIBUT_NOM));
                u.setPrenom(resultSet.getString(ATTRIBUT_PRENOM));

                n.setIdNote(resultSet.getLong(ATTRIBUT_NOTE_ID_NOTE));
                n.setNote(resultSet.getFloat(ATTRIBUT_NOTE));
                n.setRefEtudiant(resultSet.getLong(ATTRIBUT_ID_UTILISATEUR));
                n.setRefMatiere(resultSet.getLong(ATTRIBUT_ID_MATIERE));

                m.setIdMatiere(resultSet.getLong(ATTRIBUT_ID_MATIERE));
                m.setRefSprint(resultSet.getLong(ATTRIBUT_REF_SPRINT));
                m.setLibelle(resultSet.getString(ATTRIBUT_LIBELLE));
                m.setCoefficient(resultSet.getFloat(ATTRIBUT_COEFF));

                //Ajout a la map
                ajouterDansMap(resultats, u, m, n);
            }
        } catch (SQLException e) {
            logger.log(Level.WARN, "Impossible de récupérer les notes utilisateur", e);
        } finally {
            fermeture(resultSet);
        }

        return resultats;
    }

    /**
     * Ajoute les résultats dans la map spécifique
     *
     * @param map         Map de resultat
     * @param utilisateur utilisateur concerné
     * @param matiere     matiere
     * @param note        note
     */
    private void ajouterDansMap(Map<Utilisateur, Map<Matiere, Note>> map, Utilisateur utilisateur, Matiere matiere, Note note) {
        //On verifie si l'utilisateur est dans la map
        Utilisateur key = null;
        for (Utilisateur u : map.keySet()) {
            //Si il est présent
            if (u.getIdUtilisateur().equals(utilisateur.getIdUtilisateur())) {
                key = u;
            }
        }
        Map<Matiere, Note> matiereNoteMap = key != null ? map.get(key) : new HashMap<>();
        key = key == null ? utilisateur : key;

        //On ajoute
        matiereNoteMap.put(matiere, note);
        map.put(key, matiereNoteMap);
    }

    private void prepareLong(PreparedStatement ps, int index, Long val) throws SQLException {
        if (val != null) {
            ps.setLong(index, val);
        } else {
            ps.setNull(index, Types.INTEGER);
        }
    }

    private void prepareInt(PreparedStatement ps, int index, Integer val) throws SQLException {
        if (val != null) {
            ps.setLong(index, val);
        } else {
            ps.setNull(index, Types.INTEGER);
        }
    }

    /**
     * Liste tous les Notes présents dans la BDD.
     *
     * @return noteEquipe la liste des Notes présents dans la BDD.
     */
    @Override
    public List<Note> lister() {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        List<Note> noteEquipes = new ArrayList<>();
        try {
            // création d'une connexion grâce à la DAOFactory placée en attribut de la
            // classe
            connection = this.creerConnexion();
            preparedStatement = connection.prepareStatement(SQL_SELECT_TOUT);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                noteEquipes.add(map(resultSet));
            }
            // récupération des valeurs des attributs de la BDD pour les mettre dans une
            // liste
        } catch (SQLException e) {
            logger.log(Level.WARN, "Échec du listage des objets noteEquipes. ", e);
        } finally {
            fermetures(resultSet, preparedStatement, connection);
        }
        return noteEquipes;
    }

    // #################################################
    // # Méthodes privées #
    // #################################################

    /**
     * Fait la correspondance (le mapping) entre une ligne issue de la table Note
     * (un ResultSet) et un bean Note.
     *
     * @param resultSet la ligne issue de la table Note.
     * @return noteEquipe le bean dont on souhaite faire la correspondance.
     */
    public static Note map(ResultSet resultSet) throws SQLException {
        Note noteEquipe = new Note();
        noteEquipe.setIdNote(resultSet.getLong(ATTRIBUT_ID_NOTEEQUIPE));
        noteEquipe.setNote(resultSet.getFloat(ATTRIBUT_NOTE));
        noteEquipe.setRefMatiere(resultSet.getLong(ATTRIBUT_REF_MATIERE));
        noteEquipe.setRefEtudiant(resultSet.getLong(ATTRIBUT_REF_ETUDIANT));
        noteEquipe.setRefEvaluateur(resultSet.getLong(ATTRIBUT_REF_EVALUATEUR));
        return noteEquipe;
    }

    private void preparerRecupererNotesANoter(PreparedStatement statement, Utilisateur utilisateur, Utilisateur evaluateur, Integer idAnneeScolaire, Integer idSprint, Integer idEquipe, Integer idMatiere) throws SQLException {
        prepareLong(statement, 1, utilisateur.getIdUtilisateur());
        prepareLong(statement, 2, utilisateur.getIdUtilisateur());
        prepareInt(statement, 3, idAnneeScolaire);
        prepareInt(statement, 4, idAnneeScolaire);
        prepareInt(statement, 5, idSprint);
        prepareInt(statement, 6, idSprint);
        prepareInt(statement, 7, idEquipe);
        prepareInt(statement, 8, idEquipe);
        prepareInt(statement, 9, idMatiere);
        prepareInt(statement, 10, idMatiere);
        prepareLong(statement, 11, evaluateur.getIdUtilisateur());
        prepareLong(statement, 12, evaluateur.getIdUtilisateur());

        prepareLong(statement, 13, evaluateur.getIdUtilisateur());
        prepareInt(statement, 14, idAnneeScolaire);
        prepareInt(statement, 15, idAnneeScolaire);
        prepareInt(statement, 16, idSprint);
        prepareInt(statement, 17, idSprint);
        prepareInt(statement, 18, idEquipe);
        prepareInt(statement, 19, idEquipe);
        prepareInt(statement, 20, idMatiere);
        prepareInt(statement, 21, idMatiere);
        prepareLong(statement, 22, utilisateur.getIdUtilisateur());
        prepareLong(statement, 23, utilisateur.getIdUtilisateur());
    }



}
