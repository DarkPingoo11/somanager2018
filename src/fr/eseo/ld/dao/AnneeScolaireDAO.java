package fr.eseo.ld.dao;

import fr.eseo.ld.beans.AnneeScolaire;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static fr.eseo.ld.dao.DAOUtilitaire.*;

/**
 * Classe du DAO du bean AnneeScolaire.
 * 
 * <p>
 * Cette classe implémente toutes les méthodes CRUD définies dans sa classe
 * mère.
 * </p>
 * 
 * @author Pierre CESMAT
 */
public class AnneeScolaireDAO extends DAO<AnneeScolaire> {

	private static final String NOM_ENTITE = "AnneeScolaire";
	private static final String ATTRIBUT_ID_ANNEE_SCOLAIRE = "idAnneeScolaire";
	private static final String ATTRIBUT_ANNEE_DEBUT = "anneeDebut";
	private static final String ATTRIBUT_ANNEE_FIN = "anneeFin";
	private static final String ATTRIBUT_DATE_DEBUT_PROJET = "dateDebutProjet";
	private static final String ATTRIBUT_DATE_MI_AVANCEMENT = "dateMiAvancement";
	private static final String ATTRIBUT_DATE_DEPOT_POSTER = "dateDepotPoster";
	private static final String ATTRIBUT_DATE_SOUTENANCE_FINALE = "dateSoutenanceFinale";
	private static final String ATTRIBUT_ID_OPTION = "idOption";
	private static final String[] ATTRIBUTS_NOMS = { ATTRIBUT_ID_ANNEE_SCOLAIRE, ATTRIBUT_ANNEE_DEBUT,
			ATTRIBUT_ANNEE_FIN, ATTRIBUT_DATE_DEBUT_PROJET, ATTRIBUT_DATE_MI_AVANCEMENT, ATTRIBUT_DATE_DEPOT_POSTER,
			ATTRIBUT_DATE_SOUTENANCE_FINALE, ATTRIBUT_ID_OPTION };
	private static final String[] ATTRIBUTS_NOMS_ID_FIN = { ATTRIBUT_ANNEE_DEBUT, ATTRIBUT_ANNEE_FIN,
			ATTRIBUT_DATE_DEBUT_PROJET, ATTRIBUT_DATE_MI_AVANCEMENT, ATTRIBUT_DATE_DEPOT_POSTER,
			ATTRIBUT_DATE_SOUTENANCE_FINALE, ATTRIBUT_ID_OPTION, ATTRIBUT_ID_ANNEE_SCOLAIRE };

	// tous les attributs du bean doivent être écrits dans la requête INSERT
	private static final String SQL_INSERT = "INSERT INTO AnneeScolaire (anneeDebut, anneeFin, dateDebutProjet, dateMiAvancement, dateDepotPoster, dateSoutenanceFinale, idOption) "
			+ "VALUES (?, ?, ?, ?, ?, ?, ?)";
	private static final String SQL_SELECT_TOUT = "SELECT * FROM AnneeScolaire";

	private static Logger logger = Logger.getLogger(AnneeScolaireDAO.class.getName());

	/**
	 * Constructeur de DAO.
	 * 
	 * @param daoFactory
	 *            la Factory permettant la création d'une connexion à la BDD.
	 */
	public AnneeScolaireDAO(DAOFactory daoFactory) {
		super(daoFactory);
	}

	/**
	 * Insère une AnneeScolaire dans la BDD à partir des attributs spécifiés dans un
	 * bean AnneeScolaire.
	 * 
	 * @param anneeScolaire
	 *            l'AnneeScolaire que l'on souhaite insérer dans la BDD à partir du
	 *            bean AnneeScolaire.
	 */
	@Override
	public void creer(AnneeScolaire anneeScolaire) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			// les "?" de la requête sont comblés par les attributs de l'objet en paramètre
			// ceux-ci peuvent être nuls à condition que la BDD accepte les données de type
			// "null"
			preparedStatement = initialisationRequetePreparee(connection, SQL_INSERT, true,
					anneeScolaire.getAnneeDebut(), anneeScolaire.getAnneeFin(), anneeScolaire.getDateDebutProjet(),
					anneeScolaire.getDateMiAvancement(), anneeScolaire.getDateDepotPoster(),
					anneeScolaire.getDateSoutenanceFinale(), anneeScolaire.getIdOption());
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec de la création de l'objet, aucune ligne ajoutée dans la table.", e);
		} finally {
			// fermeture des ressources utilisées
			fermetures(preparedStatement, connection);
		}
	}

	/**
	 * Liste toutes les AnneeScolaire ayant pour attributs les mêmes que ceux
	 * spécifiés dans un bean AnneeScolaire.
	 * 
	 * @param anneeScolaire
	 *            l'AnneeScolaire que l'on souhaite trouver dans la BDD.
	 * @return anneeScolaires la liste des AnneeScolaire trouvées dans la BDD.
	 */
	@Override
	public List<AnneeScolaire> trouver(AnneeScolaire anneeScolaire) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<AnneeScolaire> anneeScolaires = new ArrayList<>();
		// tableau de Strings regroupant tous les noms des attributs d'un objet ainsi
		// que les valeurs correspondantes
		String[][] attributs = { ATTRIBUTS_NOMS,
				{ String.valueOf(anneeScolaire.getIdAnneeScolaire()), String.valueOf(anneeScolaire.getAnneeDebut()),
						String.valueOf(anneeScolaire.getAnneeFin()), String.valueOf(anneeScolaire.getDateDebutProjet()),
						String.valueOf(anneeScolaire.getDateMiAvancement()),
						String.valueOf(anneeScolaire.getDateDepotPoster()),
						String.valueOf(anneeScolaire.getDateSoutenanceFinale()),
						String.valueOf(anneeScolaire.getIdOption()) } };
		try {
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			// mise en forme de la requête SELECT en fonction des attributs de l'objet
			// equipe
			preparedStatement = initialisationRequetePreparee(connection, "SELECT", NOM_ENTITE, attributs, false);
			resultSet = preparedStatement.executeQuery();
			// récupération des valeurs des attributs de la BDD pour les mettre dans une
			// liste
			while (resultSet.next()) {
				anneeScolaires.add(map(resultSet));
			}
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec de la recherche de l'objet.", e);
		} finally {
			// fermeture des ressources utilisées
			fermetures(resultSet, preparedStatement, connection);
		}
		return anneeScolaires;
	}

	/**
	 * Modifie UNE AnneeScolaire ayant pour attributs les mêmes que ceux spécifiés
	 * dans un bean AnneeScolaire et la même clé primaire. Cette clé primaire ne
	 * peut être modifiée.
	 * 
	 * @param anneeScolaire
	 *            l'AnneeScolaire que l'on souhaite modifier dans la BDD.
	 */
	@Override
	public void modifier(AnneeScolaire anneeScolaire) {
		// tableau de Strings regroupant tous les noms des attributs d'un objet ainsi
		// que les valeurs correspondantes
		// /!\ bien placer la clé primaire et sa valeur à la FIN du tableau sinon erreur
		String[][] attributs = { ATTRIBUTS_NOMS_ID_FIN, { String.valueOf(anneeScolaire.getAnneeDebut()),
				String.valueOf(anneeScolaire.getAnneeFin()), String.valueOf(anneeScolaire.getDateDebutProjet()),
				String.valueOf(anneeScolaire.getDateMiAvancement()), String.valueOf(anneeScolaire.getDateDepotPoster()),
				String.valueOf(anneeScolaire.getDateSoutenanceFinale()), String.valueOf(anneeScolaire.getIdOption()),
				String.valueOf(anneeScolaire.getIdAnneeScolaire()) } };

		/* Traite la mise à jour de la BDD */
		traitementUpdate(this.getDaoFactory(), NOM_ENTITE, attributs, logger);
	}

	/**
	 * Supprime toutes les AnneeScolaire ayant pour attributs les mêmes que ceux
	 * spécifiés dans un bean AnneeScolaire.
	 * 
	 * @param anneeScolaire
	 *            l'AnneeScolaire que l'on souhaite supprimer dans la BDD.
	 */
	@Override
	public void supprimer(AnneeScolaire anneeScolaire) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		// tableau de Strings regroupant tous les noms des attributs d'un objet ainsi
		// que les valeurs correspondantes
		String[][] attributs = { ATTRIBUTS_NOMS,
				{ String.valueOf(anneeScolaire.getIdAnneeScolaire()), String.valueOf(anneeScolaire.getAnneeDebut()),
						String.valueOf(anneeScolaire.getAnneeFin()), String.valueOf(anneeScolaire.getDateDebutProjet()),
						String.valueOf(anneeScolaire.getDateMiAvancement()),
						String.valueOf(anneeScolaire.getDateDepotPoster()),
						String.valueOf(anneeScolaire.getDateSoutenanceFinale()),
						String.valueOf(anneeScolaire.getIdOption()) } };
		try {
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			preparedStatement = initialisationRequetePreparee(connection, "DELETE", NOM_ENTITE, attributs, false);
			int statut = preparedStatement.executeUpdate();
			if (statut == 0) {
				throw new SQLException();
			} else {
				// suppression du bean
				anneeScolaire.setIdAnneeScolaire(null);
			}
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec de la suppression de l'objet, aucune ligne supprimée de la table.", e);
		} finally {
			// fermeture des ressources utilisées
			fermetures(preparedStatement, connection);
		}
	}

	/**
	 * Liste toutes les AnneeScolaire présentes dans la BDD.
	 * 
	 * @return anneeScolaires la liste des AnneeScolaires présentes dans la BDD.
	 */
	@Override
	public List<AnneeScolaire> lister() {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<AnneeScolaire> anneeScolaires = new ArrayList<>();
		try {
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			preparedStatement = connection.prepareStatement(SQL_SELECT_TOUT);
			resultSet = preparedStatement.executeQuery();
			// récupération des valeurs des attributs de la BDD pour les mettre dans une
			// liste
			while (resultSet.next()) {
				anneeScolaires.add(map(resultSet));
			}
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec du listage des objets.", e);
		} finally {
			fermetures(resultSet, preparedStatement, connection);
		}
		return anneeScolaires;
	}


	/**
	 * Fait la correspondance (le mapping) entre une ligne issue de la table
	 * AnneeScolaire (un ResultSet) et un bean AnneeScolaire.
	 * 
	 * @param resultSet
	 *            la ligne issue de la table AnneeScolaire.
	 * @return anneeScolaire le bean dont on souhaite faire la correspondance.
	 * @throws SQLException
	 */
	public static AnneeScolaire map(ResultSet resultSet) throws SQLException {
		AnneeScolaire anneeScolaire = new AnneeScolaire();
		anneeScolaire.setIdAnneeScolaire(resultSet.getLong(ATTRIBUT_ID_ANNEE_SCOLAIRE));
		anneeScolaire.setAnneeDebut(resultSet.getInt(ATTRIBUT_ANNEE_DEBUT));
		anneeScolaire.setAnneeFin(resultSet.getInt(ATTRIBUT_ANNEE_FIN));
		anneeScolaire.setDateDebutProjet(resultSet.getDate(ATTRIBUT_DATE_DEBUT_PROJET));
		anneeScolaire.setDateMiAvancement(resultSet.getDate(ATTRIBUT_DATE_MI_AVANCEMENT));
		anneeScolaire.setDateDepotPoster(resultSet.getDate(ATTRIBUT_DATE_DEPOT_POSTER));
		anneeScolaire.setDateSoutenanceFinale(resultSet.getDate(ATTRIBUT_DATE_SOUTENANCE_FINALE));
		anneeScolaire.setIdOption(resultSet.getLong(ATTRIBUT_ID_OPTION));
		return anneeScolaire;
	}

}