package fr.eseo.ld.dao;

import fr.eseo.ld.beans.NoteSoutenance;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static fr.eseo.ld.dao.DAOUtilitaire.fermetures;
import static fr.eseo.ld.dao.DAOUtilitaire.initialisationRequetePreparee;

/**
 * Classe du DAO du bean NoteSoutenance.
 * 
 * <p>
 * Cette classe implémente toutes les méthodes CRUD définies dans sa classe
 * mère.
 * </p>
 * 
 * @author Thomas MENARD
 */
public class NoteSoutenanceDAO extends DAO<NoteSoutenance> {

	private static final String NOM_ENTITE = "NoteSoutenance";
	private static final String ATTRIBUT_ID_PROFESSEUR = "idProfesseur";
	private static final String ATTRIBUT_ID_ETUDIANT = "idEtudiant";
	private static final String ATTRIBUT_ID_SOUTENANCE = "idSoutenance";
	private static final String ATTRIBUT_NOTE = "note";
	private static final String[] ATTRIBUTS_NOMS = { ATTRIBUT_ID_PROFESSEUR, ATTRIBUT_ID_ETUDIANT,
			ATTRIBUT_ID_SOUTENANCE, ATTRIBUT_NOTE };
	private static final String[] ATTRIBUTS_NOMS_ID_FIN = { ATTRIBUT_NOTE, ATTRIBUT_ID_PROFESSEUR, ATTRIBUT_ID_ETUDIANT,
			ATTRIBUT_ID_SOUTENANCE };

	// tous les attributs du bean doivent être écrits dans la requête INSERT
	private static final String SQL_INSERT = "INSERT INTO NoteSoutenance (idProfesseur, idEtudiant, idSoutenance, note) VALUES (?, ?, ?, ?)";
	private static final String SQL_SELECT_TOUT = "SELECT * FROM NoteSoutenance";

	private static Logger logger = Logger.getLogger(NoteSoutenanceDAO.class.getName());

	/**
	 * Constructeur de DAO.
	 * 
	 * @param daoFactory
	 *            la Factory permettant la création d'une connexion à la BDD.
	 */
	NoteSoutenanceDAO(DAOFactory daoFactory) {
		super(daoFactory);
	}

	/**
	 * Insère une NoteSoutenance dans la BDD à partir des attributs spécifiés dans
	 * un bean NoteSoutenance.
	 * 
	 * @param noteSoutenance
	 *            la NoteSoutenance que l'on souhaite insérer dans la BDD à partir
	 *            du bean NoteSoutenance.
	 */
	@Override
	public void creer(NoteSoutenance noteSoutenance) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			// les "?" de la requête sont comblés par les attributs de l'objet en paramètre
			// ceux-ci peuvent être nuls à condition que la BDD accepte les données de type
			// "null"
			preparedStatement = initialisationRequetePreparee(connection, SQL_INSERT, true,
					noteSoutenance.getIdProfesseur(), noteSoutenance.getIdEtudiant(), noteSoutenance.getIdSoutenance(),
					noteSoutenance.getNote());
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec de la création de l'objet, aucune ligne ajoutée dans la table.", e);
		} finally {
			// fermeture des ressources utilisées
			fermetures(preparedStatement, connection);
		}
	}

	/**
	 * Liste toutes les NoteSoutenance ayant pour attributs les mêmes que ceux
	 * spécifiés dans un bean NoteSoutenance.
	 * 
	 * @param noteSoutenance
	 *            la NoteSoutenance que l'on souhaite trouver dans la BDD.
	 * @return notePosters la liste des NoteSoutenance trouvées dans la BDD.
	 */
	@Override
	public List<NoteSoutenance> trouver(NoteSoutenance noteSoutenance) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<NoteSoutenance> noteSoutenances = new ArrayList<>();
		// tableau de Strings regroupant tous les noms des attributs d'un objet ainsi
		// que les valeurs correspondantes
		String[][] attributs = { ATTRIBUTS_NOMS,
				{ String.valueOf(noteSoutenance.getIdProfesseur()), String.valueOf(noteSoutenance.getIdEtudiant()),
						String.valueOf(noteSoutenance.getIdSoutenance()), String.valueOf(noteSoutenance.getNote()) } };
		try {
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			// mise en forme de la requête SELECT en fonction des attributs de l'objet
			preparedStatement = initialisationRequetePreparee(connection, "SELECT", NOM_ENTITE, attributs, false);
			resultSet = preparedStatement.executeQuery();
			// récupération des valeurs des attributs de la BDD pour les mettre dans une
			// liste
			while (resultSet.next()) {
				noteSoutenances.add(map(resultSet));
			}
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec de la recherche de l'objet NoteSoutenance.", e);
		} finally {
			// fermeture des ressources utilisées
			fermetures(resultSet, preparedStatement, connection);
		}
		return noteSoutenances;
	}

	/**
	 * Modifie UNE NoteSoutenance ayant pour attributs les mêmes que ceux spécifiés
	 * dans un bean NoteSoutenance et la même clé primaire. Cette clé primaire ne
	 * peut être modifiée.
	 * 
	 * @param noteSoutenance
	 *            la NoteSoutenance que l'on souhaite modifier dans la BDD.
	 */
	@Override
	public void modifier(NoteSoutenance noteSoutenance) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		// tableau de Strings regroupant tous les noms des attributs d'un objet ainsi
		// que les valeurs correspondantes
		// /!\ bien placer la clé primaire et sa valeur à la FIN du tableau sinon erreur
		String[][] attributs = { ATTRIBUTS_NOMS_ID_FIN,
				{ String.valueOf(noteSoutenance.getNote()), String.valueOf(noteSoutenance.getIdProfesseur()),
						String.valueOf(noteSoutenance.getIdEtudiant()),
						String.valueOf(noteSoutenance.getIdSoutenance()) } };
		try {
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			// mise en forme de la requête UPDATE en fonction des attributs de l'objet
			// notePoster
			preparedStatement = initialisationRequetePreparee(connection, "UPDATE", NOM_ENTITE, attributs,
					new String[]{"Professeur", "Etudiant", "Soutenance"}, false);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec de la mise à jour de l'objet, aucune ligne modifée dans la table.", e);
		} finally {
			// fermeture des ressources utilisées
			fermetures(preparedStatement, connection);
		}
	}

	/**
	 * Supprime toutes les NoteSoutenance ayant pour attributs les mêmes que ceux
	 * spécifiés dans un bean NoteSoutenance.
	 * 
	 * @param noteSoutenance
	 *            la NoteSoutenance que l'on souhaite supprimer dans la BDD.
	 */
	@Override
	public void supprimer(NoteSoutenance noteSoutenance) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		// tableau de Strings regroupant tous les noms des attributs d'un objet ainsi
		// que les valeurs correspondantes
		String[][] attributs = { ATTRIBUTS_NOMS,
				{ String.valueOf(noteSoutenance.getIdProfesseur()), String.valueOf(noteSoutenance.getIdEtudiant()),
						String.valueOf(noteSoutenance.getIdSoutenance()), String.valueOf(noteSoutenance.getNote()) } };
		try {
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			preparedStatement = initialisationRequetePreparee(connection, "DELETE", NOM_ENTITE, attributs, false);
			int statut = preparedStatement.executeUpdate();
			if (statut == 0) {
				throw new SQLException();
			} else {
				// suppression du bean
				noteSoutenance.setIdProfesseur(null);
				noteSoutenance.setIdEtudiant(null);
				noteSoutenance.setIdSoutenance(null);
			}
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec de la suppression de l'objet, aucune ligne supprimée de la table.", e);
		} finally {
			// fermeture des ressources utilisées
			fermetures(preparedStatement, connection);
		}
	}

	/**
	 * Liste toutes les NoteSoutenance présentes dans la BDD.
	 * 
	 * @return notePosters la liste des NoteSoutenance présentes dans la BDD.
	 */
	@Override
	public List<NoteSoutenance> lister() {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<NoteSoutenance> noteSoutenance = new ArrayList<>();
		try {
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			preparedStatement = connection.prepareStatement(SQL_SELECT_TOUT);
			resultSet = preparedStatement.executeQuery();
			// récupération des valeurs des attributs de la BDD pour les mettre dans une
			// liste
			while (resultSet.next()) {
				noteSoutenance.add(map(resultSet));
			}
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec du listage des objets.", e);
		} finally {
			fermetures(resultSet, preparedStatement, connection);
		}
		return noteSoutenance;
	}

	/**
	 * Fait la correspondance (le mapping) entre une ligne issue de la table
	 * NoteSoutenance (un ResultSet) et un bean NoteSoutenance.
	 * 
	 * @param resultSet
	 *            la ligne issue de la table NoteSoutenance.
	 * @return noteSoutenance le bean dont on souhaite faire la correspondance.
	 * @throws SQLException
	 */
	public static NoteSoutenance map(ResultSet resultSet) throws SQLException {
		NoteSoutenance noteSoutenance = new NoteSoutenance();
		noteSoutenance.setIdProfesseur(resultSet.getLong(ATTRIBUT_ID_PROFESSEUR));
		noteSoutenance.setIdEtudiant(resultSet.getLong(ATTRIBUT_ID_ETUDIANT));
		noteSoutenance.setIdSoutenance(resultSet.getLong(ATTRIBUT_ID_SOUTENANCE));
		noteSoutenance.setNote(resultSet.getFloat(ATTRIBUT_NOTE));
		return noteSoutenance;
	}

}