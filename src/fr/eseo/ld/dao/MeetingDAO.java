package fr.eseo.ld.dao;

import fr.eseo.ld.beans.Meeting;
import fr.eseo.ld.beans.Utilisateur;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static fr.eseo.ld.dao.DAOUtilitaire.*;
/**
 * Classe du DAO du bean Meeting.
 *
 * <p>
 * Cette classe implémente toutes les méthodes CRUD définies dans sa classe
 * mère.
 * </p>
 *
 */
public class MeetingDAO extends DAO<Meeting> {

    /* Noms des entités */
    private static final String NOM_ENTITE = "Reunion";
    private static final String NOM_ENTITE_INVITE_MEETING = "Invite_Reunion";

    /* Attributs de l'entité Meeting */
    private static final String ATTRIBUT_ID_MEETING = "idReunion";
    private static final String ATTRIBUT_ID_INVITE = "refReunion";
    public static final String ATTRIBUT_TITRE = "titre";
    public static final String ATTRIBUT_DESCRIPTION = "description";
    public static final String ATTRIBUT_LIEU = "lieu";
    public static final String ATTRIBUT_COMPTERENDU = "compteRendu";
    public static final String ATTRIBUT_DATE = "dateReunion";
    public static final String ATTRIBUT_REF_UTILISATEUR = "refUtilisateur";
    private static final String[] ATTRIBUTS_NOMS = {ATTRIBUT_ID_MEETING, ATTRIBUT_TITRE, ATTRIBUT_DESCRIPTION,
            ATTRIBUT_LIEU, ATTRIBUT_COMPTERENDU, ATTRIBUT_DATE, ATTRIBUT_REF_UTILISATEUR};
    private static final String[] ATTRIBUTS_NOMS_ID_FIN = {ATTRIBUT_TITRE, ATTRIBUT_DESCRIPTION, ATTRIBUT_LIEU,
            ATTRIBUT_COMPTERENDU, ATTRIBUT_DATE, ATTRIBUT_REF_UTILISATEUR, ATTRIBUT_ID_MEETING};

    /* Requetes SQL Sujet */
    private static final String SQL_INSERT_MEETING = "INSERT INTO `Reunion`(`dateReunion`, `compteRendu`, `titre`, `description`, `lieu`, `refUtilisateur`) VALUES (?,?,?,?,?,?)";
    private static final String SQL_SELECT_TOUT = "SELECT * FROM Reunion";

    /* Requetes SQL pour trouver un sujet à partir de son ID */
    private static final String SQL_SELECT_MEETING_A_PARTIR_ID = "SELECT * FROM Reunion WHERE idReunion=?";


    /* Requetes SQL Invite */
    private static final String SQL_SELECT_MEETING_A_PARTIR_ID_UTILISATEUR = "SELECT reu.* FROM Reunion reu, Invite_Reunion invi WHERE reu.idReunion=invi.refReunion " +
            "AND invi.refUtilisateur=? ";

    /* Constantes pour éviter la duplication de code */
    private static final String DELETE = "DELETE";

    /* Logger */
    private static Logger logger = Logger.getLogger(MeetingDAO.class.getName());

    /**
     * Constructeur de DAO.
     *
     * @param daoFactory la Factory permettant la création d'une connexion à la BDD.
     */
    public MeetingDAO(DAOFactory daoFactory) {
        super(daoFactory);
    }

    // ########################################################################################################
    // # Méthodes CRUD concernant le bean Meeting #
    // ########################################################################################################

    /**
     * Insère un Meeting dans la BDD à partir des attributs spécifiés dans un bean
     * Meeting.
     *
     * @param meeting le Meeting que l'on souhaite insérer dans la BDD à partir du bean
     *                Meeting.
     */
    @Override
    public void creer(Meeting meeting) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            // création d'une connexion grâce à la DAOFactory placée en attribut de la
            // classe
            connection = this.creerConnexion();
            // les "?" de la requête sont comblés par les attributs de l'objet en paramètre
            // ceux-ci peuvent être nuls à condition que la BDD accepte les données de type
            // "null"
            preparedStatement = initialisationRequetePreparee(connection, SQL_INSERT_MEETING, true,
                    meeting.getDate(), meeting.getCompteRendu(), meeting.getTitre(), meeting.getDescription(),
                    meeting.getLieu(), meeting.getRefUtilisateur());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            logger.log(Level.WARN, "Échec de la création de l'objet, aucune ligne ajoutée dans la table.", e);
        } finally {
            // fermeture des ressources utilisées
            fermetures(preparedStatement, connection);
        }
    }


    /**
     * Liste tous les Meetings ayant pour attributs les mêmes que ceux spécifiés dans
     * un bean Meeting.
     *
     * @param meeting le Meeting que l'on souhaite trouver dans la BDD.
     * @return meeting la liste des Sujets trouvés dans la BDD.
     */
    @Override
    public List<Meeting> trouver(Meeting meeting) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        List<Meeting> meetings = new ArrayList<>();
        // tableau de Strings regroupant tous les noms des attributs d'un objet ainsi
        // que les valeurs correspondantes
        String[][] attributs = {ATTRIBUTS_NOMS,
                {String.valueOf(meeting.getIdReunion()), meeting.getTitre(), meeting.getDescription(),
                        meeting.getLieu(), meeting.getCompteRendu(), String.valueOf(meeting.getDate()),
                        String.valueOf(meeting.getRefUtilisateur()),
                }};
        try {
            // création d'une connexion grâce à la DAOFactory placée en attribut de la classe
            connection = this.creerConnexion();
            // mise en forme de la requête SELECT en fonction des attributs de l'objet sujet
            preparedStatement = initialisationRequetePreparee(connection, "SELECT", NOM_ENTITE, attributs, true);
            resultSet = preparedStatement.executeQuery();
            // récupération des valeurs des attributs de la BDD pour les mettre dans une liste
            resultSet(resultSet, meetings);
        } catch (SQLException e) {
            logger.log(Level.WARN, "Échec de la recherche de l'objet. ", e);
        } finally {
            fermetures(resultSet, preparedStatement, connection);
        }
        return meetings;
    }

    private void resultSet(ResultSet resultSet, List<Meeting> meetings) {
        try {
            while (resultSet.next()) {
                meetings.add(map(resultSet));
            }
        } catch (Exception e) {
            logger.log(Level.WARN, "Échec de la recherche de l'objet. ", e);
        }
    }

    /**
     * Renvoie un Meeting à partir de son ID.
     *
     * @param idMeeting l'ID du Meeting que l'on souhaite trouver dans la BDD.
     * @return meeting le meeting trouvé dans la BDD.
     */
    public Meeting trouver(Long idMeeting) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Meeting meeting = null;
        try {
            // création d'une connexion grâce à la DAOFactory placée en attribut de la
            // classe
            connection = this.creerConnexion();
            preparedStatement = initialisationRequetePreparee(connection, SQL_SELECT_MEETING_A_PARTIR_ID, true,
                    String.valueOf(idMeeting));
            resultSet = preparedStatement.executeQuery();
            resultSet.next();
            meeting = map(resultSet);
        } catch (SQLException e) {
            logger.log(Level.WARN, "Échec de la recherche d'un meeting à partir de son ID.", e);
        } finally {
            // fermeture des ressources utilisées
            fermetures(resultSet, preparedStatement, connection);
        }
        return meeting;
    }

    /**
     * Modifie un Meeting ayant pour attributs les mêmes que ceux spécifiés dans un
     * bean Meeting et la même clé primaire. Cette clé primaire ne peut être
     * modifiée.
     *
     * @param meeting le Meeting que l'on souhaite modifier dans la BDD.
     */
    @Override
    public void modifier(Meeting meeting) {
        // tableau de Strings regroupant tous les noms des attributs d'un objet ainsi
        // que les valeurs correspondantes
        // /!\ bien placer la clé primaire et sa valeur à la FIN du tableau sinon erreur
        String[][] attributs = {ATTRIBUTS_NOMS_ID_FIN,
                {meeting.getTitre(), meeting.getDescription(), meeting.getLieu(), meeting.getCompteRendu(),
                        String.valueOf(meeting.getDate()), String.valueOf(meeting.getRefUtilisateur()),
                        String.valueOf(meeting.getIdReunion())}};
        /* Traite la mise à jour de la BDD */
        try {
            traitementUpdate(this.getDaoFactory(), NOM_ENTITE, attributs, logger);
        } catch (Exception e) {
            logger.log(Level.WARN, "Échec de la modification de l'objet. ", e);
        }
    }

    /**
     * Supprime tous les Meetings ayant pour attributs les mêmes que ceux spécifiés
     * dans un bean Meeting.
     *
     * @param meeting le Meeting que l'on souhaite supprimer dans la BDD.
     */
    @Override
    public void supprimer(Meeting meeting) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        // tableau de Strings regroupant tous les noms des attributs d'un objet ainsi
        // que les valeurs correspondantes
        String[][] attributs = {ATTRIBUTS_NOMS,
                {String.valueOf(meeting.getIdReunion()), meeting.getTitre(), meeting.getDescription(),
                        meeting.getLieu(), meeting.getCompteRendu(), String.valueOf(meeting.getDate()),
                        String.valueOf(meeting.getRefUtilisateur()),}};
        try {
            // création d'une connexion grâce à la DAOFactory placée en attribut de la
            // classe
            connection = this.creerConnexion();
            // mise en forme de la requête DELETE en fonction des attributs de l'objet
            // utilisateur
            preparedStatement = initialisationRequetePreparee(connection, DELETE, NOM_ENTITE, attributs, false);
            int statut = preparedStatement.executeUpdate();
            if (statut == 0) {
                throw new SQLException();
            } else {
                //suppression du bean
                meeting.setIdReunion(null);
            }
        } catch (SQLException e) {
            logger.log(Level.WARN, "Échec de la suppression de l'objet, aucune ligne supprimée de la table.", e);
        } finally {
            // fermeture des ressources utilisées
            fermetures(preparedStatement, connection);
        }
    }


    /**
     * Liste tous les Meetings présents dans la BDD.
     *
     * @return meetings la liste des Meetings présents dans la BDD.
     */
    @Override
    public List<Meeting> lister() {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        List<Meeting> meetings = new ArrayList<>();
        try {
            // création d'une connexion grâce à la DAOFactory placée en attribut de la
            // classe
            connection = this.creerConnexion();
            preparedStatement = connection.prepareStatement(SQL_SELECT_TOUT);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                meetings.add(map(resultSet));
            }
            // récupération des valeurs des attributs de la BDD pour les mettre dans une
            // liste
        } catch (SQLException e) {
            logger.log(Level.WARN, "Échec du listage des objets. ", e);
        } finally {
            fermetures(resultSet, preparedStatement, connection);
        }
        return meetings;
    }

    // ########################################################################################################
    // # Méthodes liant Meeting et Utilisateur à travers Invite (les participants
    // aux réunions) #
    // ########################################################################################################

    /**
     * Liste les Meetings attribués à un Utilisateur.
     *
     * @param utilisateur l'utilisateur associé au meeting.
     */
    public List<Meeting> trouverMeetingUtilisateur(Utilisateur utilisateur) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        List<Meeting> meetings = new ArrayList<>();
        try {
            // vérification que l'utilisateur rentré n'est pas nul
            if (utilisateur.getIdUtilisateur() == null) {
                throw new SQLException();
            }
            // création d'une connexion grâce à la DAOFactory placée en attribut de la
            // classe
            connection = this.creerConnexion();
            // mise en forme de la requête SELECT en fonction des attributs de l'objet

            preparedStatement = connection.prepareStatement(SQL_SELECT_MEETING_A_PARTIR_ID_UTILISATEUR, Statement.NO_GENERATED_KEYS);
            preparedStatement.setLong(1, utilisateur.getIdUtilisateur());
            resultSet = preparedStatement.executeQuery();

            // récupération des valeurs des attributs de la BDD pour les mettre dans une
            // liste
            assert resultSet != null;
            while (resultSet.next()) {
                meetings.add(map(resultSet));
            }
        } catch (SQLException e) {
            logger.log(Level.WARN, "Échec de la recherche des meetings invités par un utilisateur.", e);
        } finally {
            // fermeture des ressources utilisées
            fermetures(resultSet, preparedStatement, connection);
        }
        return meetings;
    }


    /**
     * Supprime la liaison entre un Meeting et un Invite. La suppression se
     * fait uniquement via les ID de ces deux objets.
     *
     * @param meeting le meeting que l'on souhaite dissocier de l'invite.
     */
    public void supprimerInviteMeeting(Meeting meeting) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        // tableau de Strings regroupant tous les noms des attributs d'un objet ainsi
        // que les valeurs correspondantes
        String[][] attributs = {{ATTRIBUT_ID_INVITE},
                {String.valueOf(meeting.getIdReunion())}};
        try {
            // création d'une connexion grâce à la DAOFactory placée en attribut de la
            // classe
            connection = this.creerConnexion();
            preparedStatement = initialisationRequetePreparee(connection, DELETE, NOM_ENTITE_INVITE_MEETING, attributs,
                    false);
            int statut = preparedStatement.executeUpdate();
            if (statut == 0) {
                throw new SQLException();
            }
        } catch (SQLException e) {
            logger.log(Level.WARN, "Échec de la suppression de invite, aucune ligne supprimée de la table.", e);
        } finally {
            // fermeture des ressources utilisées
            fermetures(preparedStatement, connection);
        }
    }

    // #################################################
    // # Méthodes privées #
    // #################################################

    /**
     * Fait la correspondance (le mapping) entre une ligne issue de la table Reunion
     * (un ResultSet) et un bean Meeting.
     *
     * @param resultSet la ligne issue de la table Reunion.
     * @return meeting le bean dont on souhaite faire la correspondance.
     */
    public static Meeting map(ResultSet resultSet) throws SQLException {
        Meeting meeting = new Meeting();
        meeting.setIdReunion(resultSet.getLong(ATTRIBUT_ID_MEETING));
        meeting.setDate(resultSet.getString(ATTRIBUT_DATE));
        meeting.setCompteRendu(resultSet.getString(ATTRIBUT_COMPTERENDU));
        meeting.setTitre(resultSet.getString(ATTRIBUT_TITRE));
        meeting.setDescription(resultSet.getString(ATTRIBUT_DESCRIPTION));
        meeting.setLieu(resultSet.getString(ATTRIBUT_LIEU));
        meeting.setRefUtilisateur(resultSet.getLong(ATTRIBUT_REF_UTILISATEUR));
        return meeting;
    }


}
