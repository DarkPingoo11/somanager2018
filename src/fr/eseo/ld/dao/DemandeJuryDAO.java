package fr.eseo.ld.dao;

import fr.eseo.ld.beans.DemandeJury;
import fr.eseo.ld.beans.ProfesseurDemande;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static fr.eseo.ld.dao.DAOUtilitaire.*;

/**
 * <h1>DemandeJury DAO</h1>
 * <p>
 * Classe du DAO du bean DemandeJury.
 * </p>
 * <h2>Méthodes incluses :</h2>
 * <ul style="list-style-type:circle">
 * <li>void creer(DemandeJury demandeJury)</li>
 * <li>List(DemandeJury) trouver(DemandeJury demandeJury)</li>
 * <li>DemandeJury trouver(Long idDemande)</li>
 * <li>void modifier(DemandeJury demandeJury)</li>
 * <li>void supprimer(DemandeJury demandeJury)</li>
 * <li>List(DemandeJury) lister()</li>
 * <li>DemandeJury map(ResultSet resultSet)</li>
 * <li>void creerProfesseurDemande(Professeur professeur, DemandeJury
 * demandeJury)</li>
 * </ul>
 *
 * @author Dimitri JARNEAU
 */
public class DemandeJuryDAO extends DAO<DemandeJury> {

	/* Noms des entités */
	private static final String NOM_ENTITE = "DemandeJury";
	private static final String NOM_ENTITE_PROFESSEUR_DEMANDE = "ProfesseurDemande";

	/* Attributs de l'entité Sujet */
	private static final String ATTRIBUT_ID_DEMANDE = "idDemande";
	private static final String ATTRIBUT_SUJETS_CONCERNES = "sujetsConcernes";
	private static final String ATTRIBUT_COMMENTAIRE = "commentaire";
	private static final String ATTRIBUT_NOM_OPTION = "nomOption";

	private static final String ATTRIBUT_ID_PROFESSEUR = "idProfesseur";

	private static final String[] ATTRIBUTS_NOMS = { ATTRIBUT_ID_DEMANDE, ATTRIBUT_SUJETS_CONCERNES,
			ATTRIBUT_COMMENTAIRE, ATTRIBUT_NOM_OPTION };
	private static final String[] ATTRIBUTS_NOMS_ID_FIN = { ATTRIBUT_SUJETS_CONCERNES, ATTRIBUT_COMMENTAIRE,
			ATTRIBUT_NOM_OPTION, ATTRIBUT_ID_DEMANDE };

	/* Requetes SQL DemandeJury */
	private static final String SQL_INSERT_DEMANDE = "INSERT INTO `DemandeJury`(`sujetsConcernes`, `commentaire`, `nomOption`) VALUES (?,?,?)";
	private static final String SQL_SELECT_TOUT = "SELECT * FROM DemandeJury";

	/* Requetes SQL pour trouver une demande à partir de son ID */
	private static final String SQL_SELECT_DEMANDE_A_PARTIR_ID = "SELECT * FROM DemandeJury WHERE idDemande= ? ";

	/* Requetes SQL pour trouver une demande à partir de son nom d'option */
	private static final String SQL_SELECT_DEMANDE_A_PARTIR_NOM_OPTION = "SELECT * FROM DemandeJury WHERE nomOption = ? ";

	/* Constantes pour éviter la duplication de code */
	private static final String DELETE = "DELETE";
	private static final String SQL_DELETE_BEFORE_INSERT = "DELETE FROM `demandejury` WHERE nomOption = ? ";

	/* Requetes SQL ProfesseurDemande */
	private static final String SQL_INSERT_PROFESSEUR_DEMANDE = "INSERT INTO `ProfesseurDemande`(`idProfesseur`, `idDemande`) VALUES (?,?)";
	private static final String[] ATTRIBUTS_NOMS_PROFESSEUR_DEMANDE = { ATTRIBUT_ID_PROFESSEUR, ATTRIBUT_ID_DEMANDE };

	/* Logger */
	private static Logger logger = Logger.getLogger(DemandeJuryDAO.class.getName());

	/**
	 * Constructeur de DAO.
	 *
	 * @param daoFactory
	 *            la Factory permettant la création d'une connexion à la BDD.
	 */
	public DemandeJuryDAO(DAOFactory daoFactory) {
		super(daoFactory);
	}

	// ########################################################################################################
	// # Méthodes CRUD concernant le bean Sujet #
	// ########################################################################################################

	/**
	 * Insère une demande dans la BDD à partir des attributs spécifiés dans un bean
	 * DemandeJury.
	 *
	 * @param demandeJury
	 *            la demande que l'on souhaite insérer dans la BDD à partir du bean
	 *            DemandeJury.
	 */
	@Override
	public void creer(DemandeJury demandeJury) {
		this.supprimerAvantCreer(demandeJury.getNomOption());
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			// les "?" de la requête sont comblés par les attributs de l'objet en paramètre
			// ceux-ci peuvent être nuls à condition que la BDD accepte les données de type
			// "null"
			preparedStatement = initialisationRequetePreparee(connection, SQL_INSERT_DEMANDE, true,
					demandeJury.getSujetsConcernes(), demandeJury.getCommentaire(), demandeJury.getNomOption());
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec de la création de l'objet, aucune ligne ajoutée dans la table.", e);
			
		} finally {
			// fermeture des ressources utilisées
			fermetures(preparedStatement, connection);
		}
	}

	/**
	 * Liste toutes les demandes ayant pour attributs les mêmes que ceux spécifiés
	 * dans un bean DemandeJury.
	 *
	 * @param demandeJury
	 *            la demande que l'on souhaite trouver dans la BDD.
	 * @return demandeJury la liste des demandes trouvées dans la BDD.
	 */
	@Override
	public List<DemandeJury> trouver(DemandeJury demandeJury) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<DemandeJury> demandesJury = new ArrayList<>();
		// tableau de Strings regroupant tous les noms des attributs d'un objet ainsi
		// que les valeurs correspondantes
		String[][] attributs = { ATTRIBUTS_NOMS, { String.valueOf(demandeJury.getIdDemande()),
				demandeJury.getSujetsConcernes(), demandeJury.getCommentaire(), demandeJury.getNomOption() } };
		try {
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			// mise en forme de la requête SELECT en fonction des attributs de l'objet sujet
			preparedStatement = initialisationRequetePreparee(connection, "SELECT", NOM_ENTITE, attributs, false);
			resultSet = preparedStatement.executeQuery();
			// récupération des valeurs des attributs de la BDD pour les mettre dans une
			// liste
			while (resultSet.next()) {
				demandesJury.add(this.map(resultSet));
			}
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec de la recherche de l'objet.", e);
		} finally {
			// fermeture des ressources utilisées
			fermetures(resultSet, preparedStatement, connection);
		}
		return demandesJury;
	}

	/**
	 * Renvoie une demande à partir de son ID.
	 *
	 * @param idDemande
	 *            l'ID de la demande que l'on souhaite trouver dans la BDD.
	 * @return demandeJury la demande trouvée dans la BDD.
	 */
	public DemandeJury trouver(Long idDemande) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		DemandeJury demandeJury = null;
		try {
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			preparedStatement = initialisationRequetePreparee(connection, SQL_SELECT_DEMANDE_A_PARTIR_ID, true,
					String.valueOf(idDemande));
			resultSet = preparedStatement.executeQuery();
			resultSet.next();
			demandeJury = this.map(resultSet);
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec de la recherche d'une demande à partir de son ID.", e);
		} finally {
			// fermeture des ressources utilisées
			fermetures(resultSet, preparedStatement, connection);
		}
		return demandeJury;
	}

	/**
	 * Renvoie une demande à partir de son option.
	 *
	 * @param nomOption
	 *            l'option de la demande que l'on souhaite trouver dans la BDD.
	 * @return demandeJury la demande trouvée dans la BDD.
	 */
	public DemandeJury trouver(String nomOption) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		DemandeJury demandeJury = null;
		try {
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			preparedStatement = initialisationRequetePreparee(connection, SQL_SELECT_DEMANDE_A_PARTIR_NOM_OPTION, true,
					nomOption);
			resultSet = preparedStatement.executeQuery();
			resultSet.next();
			demandeJury = this.map(resultSet);
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec de la recherche d'une demande à partir de son nom d'option.", e);
		} finally {
			// fermeture des ressources utilisées
			fermetures(resultSet, preparedStatement, connection);
		}
		return demandeJury;
	}

	/**
	 * Modifie une demande ayant pour attributs les mêmes que ceux spécifiés dans un
	 * bean DemandeJury et la même clé primaire. Cette clé primaire ne peut être
	 * modifiée.
	 *
	 * @param demandeJury
	 *            la demande que l'on souhaite modifier dans la BDD.
	 */
	@Override
	public void modifier(DemandeJury demandeJury) {
		Map<String, Object> attributs = new HashMap<>();
		attributs.put(ATTRIBUT_SUJETS_CONCERNES, demandeJury.getSujetsConcernes());
		attributs.put(ATTRIBUT_COMMENTAIRE, demandeJury.getCommentaire());
		attributs.put(ATTRIBUT_NOM_OPTION, demandeJury.getNomOption());

		Map<String, Object> whereValues = new HashMap<>();
		whereValues.put(ATTRIBUT_ID_DEMANDE, demandeJury.getIdDemande());

		DAOUtilitaire.executeUpdate(this.getDaoFactory(), NOM_ENTITE, attributs, whereValues);
	}

	/**
	 * Supprime toutes les demandes ayant pour attributs les mêmes que ceux
	 * spécifiés dans un bean DemandeJury.
	 *
	 * @param demandeJury
	 *            la demande que l'on souhaite supprimer dans la BDD.
	 */
	@Override
	public void supprimer(DemandeJury demandeJury) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		// tableau de Strings regroupant tous les noms des attributs d'un objet ainsi
		// que les valeurs correspondantes
		String[][] attributs = { ATTRIBUTS_NOMS, { String.valueOf(demandeJury.getIdDemande()),
				demandeJury.getSujetsConcernes(), demandeJury.getCommentaire(), demandeJury.getNomOption() } };
		try {
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			// mise en forme de la requête DELETE en fonction des attributs de l'objet
			// utilisateur
			preparedStatement = initialisationRequetePreparee(connection, DELETE, NOM_ENTITE, attributs, false);
			int statut = preparedStatement.executeUpdate();
			if (statut == 0) {
				throw new SQLException();
			} else {
				// suppression du bean
				demandeJury.setIdDemande(null);
			}
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec de la suppression de l'objet, aucune ligne supprimée de la table.", e);
		} finally {
			// fermeture des ressources utilisées
			fermetures(preparedStatement, connection);
		}
	}

	/**
	 * Liste toutes les demandes présentes dans la BDD.
	 *
	 * @return demandesJury la liste des demandes présentes dans la BDD.
	 */
	@Override
	public List<DemandeJury> lister() {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<DemandeJury> demandesJury = new ArrayList<>();
		try {
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			preparedStatement = connection.prepareStatement(SQL_SELECT_TOUT);
			resultSet = preparedStatement.executeQuery();
			// récupération des valeurs des attributs de la BDD pour les mettre dans une
			// liste
			while (resultSet.next()) {
				demandesJury.add(this.map(resultSet));
			}
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec du listage des objets.", e);
		} finally {
			fermetures(resultSet, preparedStatement, connection);
		}
		return demandesJury;
	}

	/**
	 * Attribue une Demande à un professeur. L'attribution se fait uniquement via
	 * les ID de ces deux objets.
	 *
	 * @param idProfesseur
	 *            l'ID du professeur à qui il faut attribuer la demande.
	 * 
	 * @param idDemande
	 *            l'ID de la demande à qui il faut attribuer le professeur.
	 */
	public void creerProfesseurDemande(String idProfesseur, String idDemande) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			// les "?" de la requête sont comblés par les attributs de l'objet en paramètre
			preparedStatement = initialisationRequetePreparee(connection, SQL_INSERT_PROFESSEUR_DEMANDE, true,
					idProfesseur, idDemande);

			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec de la création de ProfesseurDemande, aucune ligne ajoutée dans la table.", e);
		} finally {
			// fermeture des ressources utilisées
			fermetures(preparedStatement, connection);
		}
	}

	/**
	 * Renvoie un objet ProfesseurDemande liant un professeur à une demandeJury.
	 *
	 * @param idProfesseur
	 *            l'ID du professeur.
	 * 
	 * @param idDemande
	 *            l'ID de la demande.
	 */
	public ProfesseurDemande trouverProfesseurDemande(String idProfesseur, String idDemande) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		ProfesseurDemande professeurDemande = null;
		// tableau de Strings regroupant tous les noms des attributs d'un objet ainsi
		// que les valeurs correspondantes
		String[][] attributs = { ATTRIBUTS_NOMS_PROFESSEUR_DEMANDE, { idProfesseur, idDemande } };
		try {
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			// mise en forme de la requête SELECT en fonction des attributs de l'objet sujet
			preparedStatement = initialisationRequetePreparee(connection, "SELECT", NOM_ENTITE_PROFESSEUR_DEMANDE,
					attributs, false);
			resultSet = preparedStatement.executeQuery();
			resultSet.next();
			professeurDemande = this.mapProfesseurDemande(resultSet);
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec de la recherche de l'objet.", e);
		} finally {
			// fermeture des ressources utilisées
			fermetures(resultSet, preparedStatement, connection);
		}
		return professeurDemande;
	}

	// #################################################
	// # Méthodes privées #
	// #################################################

	private void supprimerAvantCreer(String nomOption) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			// les "?" de la requête sont comblés par les attributs de l'objet en paramètre
			// ceux-ci peuvent être nuls à condition que la BDD accepte les données de type
			// "null"
			preparedStatement = initialisationRequetePreparee(connection, SQL_DELETE_BEFORE_INSERT, true, nomOption);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec de la création de l'objet, aucune ligne ajoutée dans la table.", e);
		} finally {
			// fermeture des ressources utilisées
			fermetures(preparedStatement, connection);
		}
	}

	/**
	 * Fait la correspondance (le mapping) entre une ligne issue de la table
	 * DemandeJury (un ResultSet) et un bean DemandeJury.
	 *
	 * @param resultSet
	 *            la ligne issue de la table DemandeJury.
	 * @return demandeJury le bean dont on souhaite faire la correspondance.
	 * @throws SQLException
	 */
	private DemandeJury map(ResultSet resultSet) throws SQLException {
		DemandeJury demandeJury = new DemandeJury();
		demandeJury.setIdDemande(resultSet.getLong(ATTRIBUT_ID_DEMANDE));
		demandeJury.setSujetsConcernes(resultSet.getString(ATTRIBUT_SUJETS_CONCERNES));
		demandeJury.setCommentaire(resultSet.getString(ATTRIBUT_COMMENTAIRE));
		demandeJury.setNomOption(resultSet.getString(ATTRIBUT_NOM_OPTION));
		return demandeJury;
	}

	private ProfesseurDemande mapProfesseurDemande(ResultSet resultSet) throws SQLException {
		ProfesseurDemande professeurDemande = new ProfesseurDemande();
		professeurDemande.setIdProfesseur(resultSet.getLong(ATTRIBUT_ID_PROFESSEUR));
		professeurDemande.setIdDemande(resultSet.getLong(ATTRIBUT_ID_DEMANDE));
		return professeurDemande;
	}

}