package fr.eseo.ld.dao;

import fr.eseo.ld.beans.Role;
import fr.eseo.ld.beans.RoleUtilisateur;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static fr.eseo.ld.dao.DAOUtilitaire.*;

/**
 * Classe du DAO du bean Role.
 * 
 * <p>
 * Cette classe implémente toutes les méthodes CRUD définies dans sa classe
 * mère.
 * </p>
 * 
 * @author Pierre CESMAT
 */
public class RoleDAO extends DAO<Role> {

	private static final String NOM_ENTITE = "Role";
	private static final String ATTRIBUT_ID_ROLE = "idRole";
	private static final String ATTRIBUT_NOM_ROLE = "nomRole";
	private static final String ATTRIBUT_ID_UTILISATEUR = "idUtilisateur";
	private static final String ATTRIBUT_ID_OPTION = "idOption";
	private static final String ATTRIBUT_TYPE_ROLE = "type";
	private static final String[] ATTRIBUTS_NOMS = { ATTRIBUT_ID_ROLE, ATTRIBUT_NOM_ROLE, ATTRIBUT_TYPE_ROLE };
	private static final String[] ATTRIBUTS_NOMS_ID_FIN = { ATTRIBUT_NOM_ROLE, ATTRIBUT_ID_ROLE };

	// tous les attributs du bean doivent être écrits dans la requête INSERT
	private static final String SQL_INSERT = "INSERT INTO Role (nomRole) VALUES (?)";
	private static final String SQL_INSERT_TOUT = "INSERT INTO Role (idRole, nomRole, type) VALUES (?,?,?)";
	private static final String SQL_SELECT_TOUT = "SELECT * FROM Role";

	// Pour les RoleUtilisateur
	private static final String SQL_LISTER_ROLE_UTILISATEUR = "SELECT RoleUtilisateur.* FROM RoleUtilisateur, Role WHERE RoleUtilisateur.idRole = Role.idRole AND Role.idRole = ? AND Role.type = 'applicatif'";
	private static final String SQL_SELECT_APPLICATIF = "SELECT * FROM Role WHERE type = 'applicatif'";

	private static Logger logger = Logger.getLogger(RoleDAO.class.getName());

	/**
	 * Constructeur de DAO.
	 * 
	 * @param daoFactory
	 *            la Factory permettant la création d'une connexion à la BDD.
	 */
	RoleDAO(DAOFactory daoFactory) {
		super(daoFactory);
	}

	/**
	 * Insère un Role dans la BDD à partir d'un nomRole dans un bean
	 * Role.
	 * 
	 * @param role
	 *            le Role que l'on souhaite insérer dans la BDD à partir du bean
	 *            Role.
	 */
	@Override
	public void creer(Role role) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			// les "?" de la requête sont comblés par les attributs de l'objet en paramètre
			// ceux-ci peuvent être nuls à condition que la BDD accepte les données de type
			// "null"
			preparedStatement = initialisationRequetePreparee(connection, SQL_INSERT, true, role.getNomRole());
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec de la création de l'objet, aucune ligne ajoutée dans la table.", e);
		} finally {
			// fermeture des ressources utilisées
			fermetures(preparedStatement, connection);
		}
	}
	
	/**
	 * Insère un Role dans la BDD à partir de tous les attributs spécifiés dans un bean
	 * Role.
	 * 
	 * @param role
	 *            le Role que l'on souhaite insérer dans la BDD à partir du bean
	 *            Role.
	 */
	public void creerRole(Role role) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			// les "?" de la requête sont comblés par les attributs de l'objet en paramètre
			// ceux-ci peuvent être nuls à condition que la BDD accepte les données de type
			// "null"
			preparedStatement = initialisationRequetePreparee(connection, SQL_INSERT_TOUT, true, role.getIdRole(), role.getNomRole(), role.getType());
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec de la création de l'objet, aucune ligne ajoutée dans la table.", e);
		} finally {
			// fermeture des ressources utilisées
			fermetures(preparedStatement, connection);
		}
	}

	/**
	 * Liste tous les Roles ayant pour attributs les mêmes que ceux spécifiés dans
	 * un bean Role.
	 * 
	 * @param role
	 *            le Role que l'on souhaite trouver dans la BDD.
	 * @return roles la liste des Sujets trouvés dans la BDD.
	 */
	@Override
	public List<Role> trouver(Role role) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<Role> roles = new ArrayList<>();
		// tableau de Strings regroupant tous les noms des attributs d'un objet ainsi
		// que les valeurs correspondantes
		String[][] attributs = { ATTRIBUTS_NOMS, { String.valueOf(role.getIdRole()), role.getNomRole(), role.getType() } };
		try {
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			// mise en forme de la requête SELECT en fonction des attributs de l'objet sujet
			preparedStatement = initialisationRequetePreparee(connection, "SELECT", NOM_ENTITE, attributs, false);
			resultSet = preparedStatement.executeQuery();
			// récupération des valeurs des attributs de la BDD pour les mettre dans une
			// liste
			while (resultSet.next()) {
				roles.add(map(resultSet));
			}
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec de la recherche de l'objet.", e);
		} finally {
			// fermeture des ressources utilisées
			fermetures(resultSet, preparedStatement, connection);
		}
		return roles;
	}

	/**
	 * Modifie UN Role ayant pour attributs les mêmes que ceux spécifiés dans un
	 * bean Role et la même clé primaire. Cette clé primaire ne peut être modifiée.
	 * 
	 * @param role
	 *            le Role que l'on souhaite modifier dans la BDD.
	 */
	@Override
	public void modifier(Role role) {
		// tableau de Strings regroupant tous les noms des attributs d'un objet ainsi
		// que les valeurs correspondantes
		// /!\ bien placer la clé primaire et sa valeur à la FIN du tableau sinon erreur
		String[][] attributs = { ATTRIBUTS_NOMS_ID_FIN, { role.getNomRole(), String.valueOf(role.getIdRole()) } };

		/* Traite la mise à jour de la BDD */
		traitementUpdate(this.getDaoFactory(), NOM_ENTITE, attributs, logger);
	}

	/**
	 * Supprime tous les Roles ayant pour attributs les mêmes que ceux spécifiés
	 * dans un bean Role.
	 * 
	 * @param role
	 *            le Role que l'on souhaite supprimer dans la BDD.
	 */
	@Override
	public void supprimer(Role role) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		// tableau de Strings regroupant tous les noms des attributs d'un objet ainsi
		// que les valeurs correspondantes
		String[][] attributs = { ATTRIBUTS_NOMS, { String.valueOf(role.getIdRole()), role.getNomRole(), role.getType() } };
		try {
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			preparedStatement = initialisationRequetePreparee(connection, "DELETE", NOM_ENTITE, attributs, false);
			int statut = preparedStatement.executeUpdate();
			if (statut == 0) {
				throw new SQLException();
			} else {
				// suppression du bean
				role.setIdRole(null);
			}
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec de la suppression de l'objet, aucune ligne supprimée de la table.", e);
		} finally {
			// fermeture des ressources utilisées
			fermetures(preparedStatement, connection);
		}
	}

	/**
	 * Liste tous les Roles présents dans la BDD.
	 * 
	 * @return roles la liste des Roles présents dans la BDD.
	 */
	@Override
	public List<Role> lister() {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<Role> roles = new ArrayList<>();
		try {
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			preparedStatement = connection.prepareStatement(SQL_SELECT_TOUT);
			resultSet = preparedStatement.executeQuery();
			// récupération des valeurs des attributs de la BDD pour les mettre dans une
			// liste
			while (resultSet.next()) {
				roles.add(map(resultSet));
			}
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec du listage des objets.", e);
		} finally {
			fermetures(resultSet, preparedStatement, connection);
		}
		return roles;
	}

	public List<Role> listerRolesApplicatifs() {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<Role> roles = new ArrayList<>();
		try {
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			preparedStatement = connection.prepareStatement(SQL_SELECT_APPLICATIF);
			resultSet = preparedStatement.executeQuery();
			// récupération des valeurs des attributs de la BDD pour les mettre dans une
			// liste
			while (resultSet.next()) {
				roles.add(map(resultSet));
			}
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec du listage des objets.", e);
		} finally {
			fermetures(resultSet, preparedStatement, connection);
		}
		return roles;
	}

	/**
	 * Trouve tous les RoleUtilisateur pour un Role donné.
	 * 
	 * @return roleUtilisateurs la liste des RoleUtulisateur présents dans la BDD.
	 */
	public List<RoleUtilisateur> listerRoleUtilisateur(Role role) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<RoleUtilisateur> roleUtilisateurs = new ArrayList<>();
		try {
			// vérification que le role rentré n'est pas nul
			if (role.getIdRole() == null) {
				throw new SQLException();
			}
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			preparedStatement = initialisationRequetePreparee(connection, SQL_LISTER_ROLE_UTILISATEUR, true,
					role.getIdRole());
			resultSet = preparedStatement.executeQuery();
			// récupération des valeurs des attributs de la BDD pour les mettre dans une
			// liste
			while (resultSet.next()) {
				roleUtilisateurs.add(mapRoleUtilisateur(resultSet));
			}
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec du listage des roleUtilisateur.", e);
		} finally {
			fermetures(resultSet, preparedStatement, connection);
		}
		return roleUtilisateurs;
	}

	/**
	 * Fait la correspondance (le mapping) entre une ligne issue de la table Role
	 * (un ResultSet) et un bean Role.
	 * 
	 * @param resultSet
	 *            la ligne issue de la table Role.
	 * @return role le bean dont on souhaite faire la correspondance.
	 * @throws SQLException
	 */
	public static Role map(ResultSet resultSet) throws SQLException {
		Role role = new Role();
		role.setIdRole(resultSet.getLong(ATTRIBUT_ID_ROLE));
		role.setNomRole(resultSet.getString(ATTRIBUT_NOM_ROLE));
		role.setType(resultSet.getString(ATTRIBUT_TYPE_ROLE));
		return role;
	}

	/**
	 * Fait la correspondance (le mapping) entre une ligne issue de la table
	 * RoleUtilisateur (un ResultSet) et un bean RoleUtilisateur.
	 * 
	 * @param resultSet
	 *            la ligne issue de la table RoleUtilisateur.
	 * @return roleUtilisateur le bean dont on souhaite faire la correspondance.
	 * @throws SQLException
	 */
	public static RoleUtilisateur mapRoleUtilisateur(ResultSet resultSet) throws SQLException {
		RoleUtilisateur roleUtilisateur = new RoleUtilisateur();
		roleUtilisateur.setIdOption(resultSet.getLong(ATTRIBUT_ID_OPTION));
		roleUtilisateur.setIdRole(resultSet.getLong(ATTRIBUT_ID_ROLE));
		roleUtilisateur.setIdUtilisateur(resultSet.getLong(ATTRIBUT_ID_UTILISATEUR));
		return roleUtilisateur;
	}

}