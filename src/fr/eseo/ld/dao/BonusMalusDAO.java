package fr.eseo.ld.dao;

import fr.eseo.ld.pgl.beans.BonusMalus;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static fr.eseo.ld.dao.DAOUtilitaire.fermetures;
import static fr.eseo.ld.dao.DAOUtilitaire.initialisationRequetePreparee;

/**
 * Classe du DAO du bean BonusMalus.
 *
 * <p>
 * Cette classe implémente toutes les méthodes CRUD définies dans sa classe
 * mère.
 * </p>
 *
 */
public class BonusMalusDAO extends DAO<BonusMalus> {
	
	private static final String NOM_ENTITE = "pgl_bonusmalus";
	private static final String ATTRIBUT_ID_BONUS_MALUS = "idBonusMalus";
	private static final String ATTRIBUT_VALEUR = "valeur";
	private static final String ATTRIBUT_VALIDATION = "validation";
	private static final String ATTRIBUT_JUSTIFICATION = "justification";
	private static final String ATTRIBUT_REF_SPRINT = "refSprint";
	private static final String ATTRIBUT_REF_ETUDIANT = "refEtudiant";
	private static final String ATTRIBUT_REF_EVALUATEUR = "refEvaluateur";
	
	private static final String[] ATTRIBUTS_NOMS = { ATTRIBUT_ID_BONUS_MALUS, ATTRIBUT_VALEUR,
			ATTRIBUT_VALIDATION, ATTRIBUT_JUSTIFICATION, ATTRIBUT_REF_SPRINT, ATTRIBUT_REF_ETUDIANT, ATTRIBUT_REF_EVALUATEUR};

	// tous les attributs du bean doivent être écrits dans la requête INSERT
	private static final String SQL_INSERT = "INSERT INTO pgl_bonusmalus (valeur, validation, justification, refSprint, refEtudiant, refEvaluateur) "
			+ "VALUES (?, ?, ?, ?, ?, ?)";
	private static final String SQL_SELECT_TOUT = "SELECT * FROM pgl_bonusmalus";

    private static final String SQL_SELECT_BONUS_MALUS_ETU = "SELECT * FROM pgl_bonusmalus WHERE pgl_bonusmalus.refEtudiant= ? ";


    private static Logger logger = Logger.getLogger(BonusMalusDAO.class.getName());
	
	/**
	 * Constructeur de DAO.
	 * 
	 * @param daoFactory
	 *            la Factory permettant la création d'une connexion à la BDD.
	 */
	BonusMalusDAO(DAOFactory daoFactory) {	
		super(daoFactory);
	}

	/**
	 * Insère un BonusMalus dans la BDD à partir des attributs spécifiés dans un
	 * bean BonusMalus.
	 * 
	 * @param bonusMalus
	 *            le bonus/malus que l'on souhaite insérer dans la BDD à partir du
	 *            bean BonusMalus.
	 */
	
	@Override
	public void creer(BonusMalus bonusMalus) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			// les "?" de la requête sont comblés par les attributs de l'objet en paramètre
			// ceux-ci peuvent être nuls à condition que la BDD accepte les données de type
			// "null"
			preparedStatement = initialisationRequetePreparee(connection, SQL_INSERT, true,
					bonusMalus.getValeur(), 
					bonusMalus.getValidation(), 
					bonusMalus.getJustification(), 
					bonusMalus.getRefSprint(),
					bonusMalus.getRefEtudiant(),
					bonusMalus.getRefEvaluateur());
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec de la création de l'objet, aucune ligne ajoutée dans la table.", e);
		} finally {
			// fermeture des ressources utilisées
			fermetures(preparedStatement, connection);
		}
	}

	
	/**
	 * Liste toutes les BonusMalus ayant pour attributs les mêmes que ceux
	 * spécifiés dans un bean BonusMalus.
	 * 
	 * @param bonusMalus
	 *            le BonusMalus que l'on souhaite trouver dans la BDD.
	 * @return bonusMalus la liste des BonusMalus trouvées dans la BDD.
	 */
	@Override
	public List<BonusMalus> trouver(BonusMalus bonusMalus) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<BonusMalus> lesBonusMalus = new ArrayList<>();
		// tableau de Strings regroupant tous les noms des attributs d'un objet ainsi
		// que les valeurs correspondantes
		String[][] attributs = { ATTRIBUTS_NOMS,
				{ String.valueOf(bonusMalus.getIdBonusMalus()), 
						String.valueOf(bonusMalus.getValeur()),
						String.valueOf(bonusMalus.getValidation()),
						String.valueOf(bonusMalus.getJustification()), 
						String.valueOf(bonusMalus.getRefSprint()),
						String.valueOf(bonusMalus.getRefEtudiant()),
						String.valueOf(bonusMalus.getRefEvaluateur())} };
		try {
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			// mise en forme de la requête SELECT en fonction des attributs de l'objet
			// equipe
			preparedStatement = initialisationRequetePreparee(connection, "SELECT", NOM_ENTITE, attributs, false);
			resultSet = preparedStatement.executeQuery();
			// récupération des valeurs des attributs de la BDD pour les mettre dans une
			// liste
			while (resultSet.next()) {
				lesBonusMalus.add(map(resultSet));
			}
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec de la recherche de l'objet.", e);
		} finally {
			// fermeture des ressources utilisées
			fermetures(resultSet, preparedStatement, connection);
		}
		return lesBonusMalus;
	}

    /**
     * Renvoie l'Equipe d'un Etudiant.
     *
     * @param refEtudiant
     *            l'ID de l'etudiant.
     * @return equipe l'equipe de l'etudiant.
     */
    public BonusMalus trouverBonusMalusEtu(Long refEtudiant) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        BonusMalus bonusMalus = null;
        try {
            // vérification que l'ID de l'étudiant n'est pas nul
            if (refEtudiant == null) {
                throw new SQLException();
            }
            // création d'une connexion grâce à la DAOFactory placée en attribut de la
            // classe
            connection = this.creerConnexion();
            preparedStatement = initialisationRequetePreparee(connection, SQL_SELECT_BONUS_MALUS_ETU, true,
                    String.valueOf(refEtudiant));
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                bonusMalus = map(resultSet);
            }
        } catch (SQLException e) {
            logger.log(Level.WARN, "Échec de la recherche de l'équipe de l'etudiant.", e);
        } finally {
            // fermeture des ressources utilisées
            fermetures(resultSet, preparedStatement, connection);
        }
        return bonusMalus;
    }


    /**
	 * Modifie UN BonusMalus ayant pour attributs les mêmes que ceux spécifiés
	 * dans un bean BonusMalus et la même clé primaire. Cette clé primaire ne
	 * peut être modifiée.
	 * 
	 * @param bonusMalus
	 *            l'BonusMalus que l'on souhaite modifier dans la BDD.
	 */
	@Override
	public void modifier(BonusMalus bonusMalus) {
        //Update table SET <....>
        Map<String, Object> attributs = new HashMap<>();
        attributs.put(ATTRIBUT_VALEUR, bonusMalus.getValeur());
        attributs.put(ATTRIBUT_VALIDATION, bonusMalus.getValidation());
        attributs.put(ATTRIBUT_JUSTIFICATION, bonusMalus.getJustification());
        attributs.put(ATTRIBUT_REF_SPRINT, bonusMalus.getRefSprint());
        attributs.put(ATTRIBUT_REF_ETUDIANT, bonusMalus.getRefEtudiant());
        attributs.put(ATTRIBUT_REF_EVALUATEUR, bonusMalus.getRefEvaluateur());

        //Preparation de WHERE
        Map<String, Object> whereAtt = new HashMap<>();
        whereAtt.put(ATTRIBUT_ID_BONUS_MALUS, bonusMalus.getIdBonusMalus());

        //Modifier
        DAOUtilitaire.executeUpdate(this.getDaoFactory(), NOM_ENTITE, attributs, whereAtt);
	}
	
	/**
	 * Supprime tous les BonusMalus ayant pour attributs les mêmes que ceux
	 * spécifiés dans un bean BonusMalus.
	 * 
	 * @param bonusMalus
	 *            le BonusMalus que l'on souhaite supprimer dans la BDD.
	 */
	@Override
	public void supprimer(BonusMalus bonusMalus) {
        Map<String, Object> identifiants = new HashMap<>();
        identifiants.put(ATTRIBUT_ID_BONUS_MALUS, bonusMalus.getIdBonusMalus());

        DAOUtilitaire.executeDelete(this.getDaoFactory(), NOM_ENTITE, identifiants);
	}
	
	/**
	 * Liste toutes les BonusMalus présentes dans la BDD.
	 * 
	 * @return bonusMalus 
	 * 			la liste des BonusMalus présentes dans la BDD.
	 */
	@Override
	public List<BonusMalus> lister() {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<BonusMalus> lesBonusMalus = new ArrayList<>();
		try {
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			preparedStatement = connection.prepareStatement(SQL_SELECT_TOUT);
			resultSet = preparedStatement.executeQuery();
			// récupération des valeurs des attributs de la BDD pour les mettre dans une
			// liste
			while (resultSet.next()) {
				lesBonusMalus.add(map(resultSet));
			}
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec du listage des objets.", e);
		} finally {
			fermetures(resultSet, preparedStatement, connection);
		}
		return lesBonusMalus;
	}
	
	/**
	 * Fait la correspondance (le mapping) entre une ligne issue de la table
	 * BonusMalus (un ResultSet) et un bean BonusMalus.
	 * 
	 * @param resultSet
	 *            la ligne issue de la table BonusMalus.
	 * @return bonusMalus le bean dont on souhaite faire la correspondance.
	 * @throws SQLException exception
	 */
	public static BonusMalus map(ResultSet resultSet) throws SQLException {
		BonusMalus bonusMalus = new BonusMalus();
		bonusMalus.setIdBonusMalus(resultSet.getLong(ATTRIBUT_ID_BONUS_MALUS));
		bonusMalus.setValeur(resultSet.getFloat(ATTRIBUT_VALEUR));
		bonusMalus.setValidation(resultSet.getString(ATTRIBUT_VALIDATION));
		bonusMalus.setJustification(resultSet.getString(ATTRIBUT_JUSTIFICATION));
		bonusMalus.setRefSprint(resultSet.getInt(ATTRIBUT_REF_SPRINT));
		bonusMalus.setRefEtudiant(resultSet.getInt(ATTRIBUT_REF_ETUDIANT));
		bonusMalus.setRefEvaluateur(resultSet.getInt(ATTRIBUT_REF_EVALUATEUR));
		return bonusMalus;
	}
}
