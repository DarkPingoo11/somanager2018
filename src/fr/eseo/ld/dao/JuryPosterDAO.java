package fr.eseo.ld.dao;

import fr.eseo.ld.beans.JuryPoster;
import fr.eseo.ld.beans.Utilisateur;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static fr.eseo.ld.dao.DAOUtilitaire.*;

/**
 * Classe du DAO du bean JuryPoster.
 * 
 * <p>
 * Cette classe implémente toutes les méthodes CRUD définies dans sa classe
 * mère.
 * </p>
 * 
 * @author Julie AVIZOU
 */
public class JuryPosterDAO extends DAO<JuryPoster> {

	private static final String NOM_ENTITE = "JuryPoster";
	private static final String ATTRIBUT_ID_JURY_POSTER = "idJuryPoster";
	private static final String ATTRIBUT_ID_PROF1 = "idProf1";
	private static final String ATTRIBUT_ID_PROF2 = "idProf2";
	private static final String ATTRIBUT_ID_PROF3 = "idProf3";
	private static final String ATTRIBUT_DATE = "date";
	private static final String ATTRIBUT_ID_EQUIPE = "idEquipe";
	private static final String[] ATTRIBUTS_NOMS = { ATTRIBUT_ID_JURY_POSTER, ATTRIBUT_ID_PROF1, ATTRIBUT_ID_PROF2,
			ATTRIBUT_ID_PROF3, ATTRIBUT_DATE, ATTRIBUT_ID_EQUIPE };
	private static final String[] ATTRIBUTS_NOMS_ID_FIN = { ATTRIBUT_ID_PROF1, ATTRIBUT_ID_PROF2, ATTRIBUT_ID_PROF3,
			ATTRIBUT_DATE, ATTRIBUT_ID_EQUIPE, ATTRIBUT_ID_JURY_POSTER };

	// tous les attributs du bean doivent être écrits dans la requête INSERT
	private static final String SQL_INSERT = "INSERT INTO JuryPoster (idProf1, idProf2, idProf3, date, idEquipe) VALUES (?, ?, ?, ?, ?)";
	private static final String SQL_SELECT_TOUT = "SELECT * FROM JuryPoster";
	private static final String SQL_SELECT_JURYS_POSTER_PROF = "SELECT * FROM `JuryPoster` WHERE ? in (idProf1, idProf2, idProf3)";

	private static Logger logger = Logger.getLogger(JuryPosterDAO.class.getName());

	/**
	 * Constructeur de DAO.
	 * 
	 * @param daoFactory
	 *            la Factory permettant la création d'une connexion à la BDD.
	 */
	public JuryPosterDAO(DAOFactory daoFactory) {
		super(daoFactory);
	}

	/**
	 * Insère un JuryPoster dans la BDD à partir des attributs spécifiés dans un
	 * bean JuryPoster.
	 * 
	 * @param juryPoster
	 *            le JuryPoster que l'on souhaite insérer dans la BDD à partir du
	 *            bean JuryPoster.
	 */
	@Override
	public void creer(JuryPoster juryPoster) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			// les "?" de la requête sont comblés par les attributs de l'objet en paramètre
			// ceux-ci peuvent être nuls à condition que la BDD accepte les données de type
			// "null"
			preparedStatement = initialisationRequetePreparee(connection, SQL_INSERT, true, juryPoster.getIdProf1(),
					juryPoster.getIdProf2(), juryPoster.getIdProf3(), juryPoster.getDate(), juryPoster.getIdEquipe());
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec de la création de l'objet, aucune ligne ajoutée dans la table.", e);
		} finally {
			// fermeture des ressources utilisées
			fermetures(preparedStatement, connection);
		}
	}

	/**
	 * Liste tous les JuryPoster ayant pour attributs les mêmes que ceux spécifiés
	 * dans un bean JuryPoster.
	 * 
	 * @param juryPoster
	 *            le JuryPoster que l'on souhaite trouver dans la BDD.
	 * @return juryPosters la liste des JuryPoster trouvés dans la BDD.
	 */
	@Override
	public List<JuryPoster> trouver(JuryPoster juryPoster) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<JuryPoster> juryPosters = new ArrayList<>();
		// tableau de Strings regroupant tous les noms des attributs d'un objet ainsi
		// que les valeurs correspondantes
		String[][] attributs = { ATTRIBUTS_NOMS,
				{ juryPoster.getIdJuryPoster(), String.valueOf(juryPoster.getIdProf1()),
						String.valueOf(juryPoster.getIdProf2()), String.valueOf(juryPoster.getIdProf3()),
						String.valueOf(juryPoster.getDate()), juryPoster.getIdEquipe() } };
		try {
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			// mise en forme de la requête SELECT en fonction des attributs de l'objet
			// juryPoster
			preparedStatement = DAOUtilitaire.initialisationRequetePreparee(connection, "SELECT", NOM_ENTITE, attributs,
					false);
			resultSet = preparedStatement.executeQuery();
			// récupération des valeurs des attributs de la BDD pour les mettre dans une
			// liste
			while (resultSet.next()) {
				juryPosters.add(map(resultSet));
			}
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec de la recherche de l'objet.", e);
		} finally {
			// fermeture des ressources utilisées
			fermetures(resultSet, preparedStatement, connection);
		}
		return juryPosters;
	}

	/**
	 * Modifie UN JuryPoster ayant pour attributs les mêmes que ceux spécifiés dans
	 * un bean JuryPoster et la même clé primaire. Cette clé primaire ne peut être
	 * modifiée.
	 * 
	 * @param juryPoster
	 *            le JuryPoster que l'on souhaite modifier dans la BDD.
	 */
	@Override
	public void modifier(JuryPoster juryPoster) {
		// tableau de Strings regroupant tous les noms des attributs d'un objet ainsi
		// que les valeurs correspondantes
		// /!\ bien placer la clé primaire et sa valeur à la FIN du tableau sinon erreur
		Timestamp date = null;
		if (juryPoster.getDate() != null) {
			date = new Timestamp(juryPoster.getDate().getTime());
		}
		String[][] attributs = { ATTRIBUTS_NOMS_ID_FIN,
				{ String.valueOf(juryPoster.getIdProf1()), String.valueOf(juryPoster.getIdProf2()),
						String.valueOf(juryPoster.getIdProf3()), String.valueOf(date), juryPoster.getIdEquipe(),
						juryPoster.getIdJuryPoster() } };

		/* Traite la mise à jour de la BDD */
		traitementUpdate(this.getDaoFactory(), NOM_ENTITE, attributs, logger);
	}

	/**
	 * Supprime tous les JuryPoster ayant pour attributs les mêmes que ceux
	 * spécifiés dans un bean JuryPoster.
	 * 
	 * @param juryPoster
	 *            le JuryPoster que l'on souhaite supprimer dans la BDD.
	 */
	@Override
	public void supprimer(JuryPoster juryPoster) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		// tableau de Strings regroupant tous les noms des attributs d'un objet ainsi
		// que les valeurs correspondantes
		String[][] attributs = { ATTRIBUTS_NOMS,
				{ juryPoster.getIdJuryPoster(), String.valueOf(juryPoster.getIdProf1()),
						String.valueOf(juryPoster.getIdProf2()), String.valueOf(juryPoster.getIdProf3()),
						String.valueOf(juryPoster.getDate()), juryPoster.getIdEquipe() } };
		try {
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			preparedStatement = DAOUtilitaire.initialisationRequetePreparee(connection, "DELETE", NOM_ENTITE, attributs,
					false);
			int statut = preparedStatement.executeUpdate();
			if (statut == 0) {
				throw new SQLException();
			} else {
				// suppression du bean
				juryPoster.setIdJuryPoster(null);
			}
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec de la suppression de l'objet, aucune ligne supprimée de la table.", e);
		} finally {
			// fermeture des ressources utilisées
			fermetures(preparedStatement, connection);
		}
	}

	/**
	 * Liste tous les JuryPoster présents dans la BDD.
	 * 
	 * @return juryPosters la liste des JuryPoster présents dans la BDD.
	 */
	@Override
	public List<JuryPoster> lister() {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<JuryPoster> juryPosters = new ArrayList<>();
		try {
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			preparedStatement = connection.prepareStatement(SQL_SELECT_TOUT);
			resultSet = preparedStatement.executeQuery();
			// récupération des valeurs des attributs de la BDD pour les mettre dans une
			// liste
			while (resultSet.next()) {
				juryPosters.add(map(resultSet));
			}
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec du listage des objets.", e);
		} finally {
			fermetures(resultSet, preparedStatement, connection);
		}
		return juryPosters;
	}

	/**
	 * Liste tous les JuryPoster associés à un Utilisateur donné (ici un
	 * Professeur).
	 * 
	 * @param utilisateur
	 *            l'utilisateur associé aux JuryPoster.
	 * @return juryPosters la liste des JuryPoster associés à l'utilisateur.
	 */
	public List<JuryPoster> listerJurysUtilisateur(Utilisateur utilisateur) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<JuryPoster> juryPosters = new ArrayList<>();
		try {
			// vérification que l'utilisateur rentré n'est pas nul
			if (utilisateur.getIdUtilisateur() == null) {
				throw new SQLException();
			}
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			preparedStatement = initialisationRequetePreparee(connection, SQL_SELECT_JURYS_POSTER_PROF, true,
					utilisateur.getIdUtilisateur());
			resultSet = preparedStatement.executeQuery();
			// récupération des valeurs des attributs de la BDD pour les mettre dans une
			// liste
			while (resultSet.next()) {
				juryPosters.add(map(resultSet));
			}
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec du listage des juryPoster associés à un utilisateur.", e);
		} finally {
			fermetures(resultSet, preparedStatement, connection);
		}
		return juryPosters;
	}

	/**
	 * Fait la correspondance (le mapping) entre une ligne issue de la table
	 * JuryPoster (un ResultSet) et un bean JuryPoster.
	 * 
	 * @param resultSet
	 *            la ligne issue de la table JuryPoster.
	 * @return juryPoster le bean dont on souhaite faire la correspondance.
	 * @throws SQLException
	 */
	public static JuryPoster map(ResultSet resultSet) throws SQLException {
		JuryPoster juryPoster = new JuryPoster();
		juryPoster.setIdJuryPoster(resultSet.getString(ATTRIBUT_ID_JURY_POSTER));
		juryPoster.setIdProf1(resultSet.getLong(ATTRIBUT_ID_PROF1));
		if (resultSet.wasNull()) {
			juryPoster.setIdProf1(null);
		}
		juryPoster.setIdProf2(resultSet.getLong(ATTRIBUT_ID_PROF2));
		if (resultSet.wasNull()) {
			juryPoster.setIdProf2(null);
		}
		juryPoster.setIdProf3(resultSet.getLong(ATTRIBUT_ID_PROF3));
		if (resultSet.wasNull()) {
			juryPoster.setIdProf3(null);
		}
		juryPoster.setDate(resultSet.getTimestamp(ATTRIBUT_DATE));
		juryPoster.setIdEquipe(resultSet.getString(ATTRIBUT_ID_EQUIPE));
		return juryPoster;
	}

}