package fr.eseo.ld.dao;

import fr.eseo.ld.pgl.beans.RoleEquipe;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static fr.eseo.ld.dao.DAOUtilitaire.fermetures;
import static fr.eseo.ld.dao.DAOUtilitaire.initialisationRequetePreparee;

/**
 * Classe du DAO du bean RoleEquipe (Pgl).
 *
 * <p>
 * Cette classe implémente toutes les méthodes CRUD définies dans sa classe
 * mère.
 * </p>
 */
public class PglRoleEquipeDAO extends DAO<RoleEquipe> {

    private static final String NOM_ENTITE = "pgl_roleequipe";
    private static final String ATTRIBUT_REF_EQUIPE = "refEquipe";
    private static final String ATTRIBUT_REF_UTILISATEUR = "refUtilisateur";
    private static final String ATTRIBUT_REF_ROLE = "refRole";
    private static final String[] ATTRIBUTS_NOMS = {ATTRIBUT_REF_EQUIPE, ATTRIBUT_REF_UTILISATEUR, ATTRIBUT_REF_ROLE};

    // tous les attributs du bean doivent être écrits dans la requête INSERT
    private static final String SQL_INSERT = "INSERT INTO pgl_roleequipe (refEquipe,refUtilisateur,refRole) VALUES (?,?,?)";
    private static final String SQL_SELECT_TOUT = "SELECT * FROM pgl_roleequipe";

    private static Logger logger = Logger.getLogger(PglRoleEquipeDAO.class.getName());

    /**
     * Constructeur de DAO.
     *
     * @param daoFactory la Factory permettant la création d'une connexion à la BDD.
     */
    public PglRoleEquipeDAO(DAOFactory daoFactory) {
        super(daoFactory);
    }

    /**
     * Insère un RoleEquipe dans la BDD à partir des attributs spécifiés dans un bean
     * RoleEquipe.
     *
     * @param roleEquipe le RoleEquipe que l'on souhaite insérer dans la BDD à partir du bean
     *                   RoleEquipe.
     */
    @Override
    public void creer(RoleEquipe roleEquipe) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            // création d'une connexion grâce à la DAOFactory placée en attribut de la
            // classe
            connection = this.creerConnexion();
            // les "?" de la requête sont comblés par les attributs de l'objet en paramètre
            // ceux-ci peuvent être nuls à condition que la BDD accepte les données de type
            // "null"
            preparedStatement = initialisationRequetePreparee(connection, SQL_INSERT, true, roleEquipe.getRefEquipe(),
                    roleEquipe.getRefUtilisateur(), roleEquipe.getRefRole());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            logger.log(Level.WARN, "Échec de la création de l'objet, aucune ligne ajoutée dans la table.", e);
        } finally {
            // fermeture des ressources utilisées
            fermetures(preparedStatement, connection);
        }
    }

    /**
     * Liste tous les RoleEquipes ayant pour attributs les mêmes que ceux spécifiés
     * dans un bean RoleEquipe.
     *
     * @param roleEquipe lz RoleEquipe que l'on souhaite trouver dans la BDD.
     * @return roleEquipes la liste des RoleEquipes trouvés dans la BDD.
     */
    @Override
    public List<RoleEquipe> trouver(RoleEquipe roleEquipe) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        List<RoleEquipe> roleEquipes = new ArrayList<>();
        // tableau de Strings regroupant tous les noms des attributs d'un objet ainsi
        // que les valeurs correspondantes
        String[][] attributs = {ATTRIBUTS_NOMS,
                {String.valueOf(roleEquipe.getRefEquipe()), String.valueOf(roleEquipe.getRefUtilisateur()),
                        String.valueOf(roleEquipe.getRefRole())}};
        try {
            // création d'une connexion grâce à la DAOFactory placée en attribut de la
            // classe
            connection = this.creerConnexion();
            // mise en forme de la requête SELECT en fonction des attributs de l'objet
            // etudiant
            preparedStatement = initialisationRequetePreparee(connection, "SELECT", NOM_ENTITE, attributs, false);
            resultSet = preparedStatement.executeQuery();
            // récupération des valeurs des attributs de la BDD pour les mettre dans une
            // liste
            while (resultSet.next()) {
                roleEquipes.add(map(resultSet));
            }
        } catch (SQLException e) {
            logger.log(Level.WARN, "Échec de la recherche de l'objet.", e);
        } finally {
            // fermeture des ressources utilisées
            fermetures(resultSet, preparedStatement, connection);
        }
        return roleEquipes;
    }

    /**
     * Modifie UN RoleEquipe ayant pour attributs les mêmes que ceux spécifiés dans un
     * bean RoleEquipe et la même clé primaire. Cette clé primaire ne peut être
     * modifiée.
     *
     * @param roleEquipe le RoleEquipe que l'on souhaite modifier dans la BDD.
     */
    @Override
    public void modifier(RoleEquipe roleEquipe) {
        //Update table SET <....>
        Map<String, Object> attributs = new HashMap<>();
        attributs.put(ATTRIBUT_REF_ROLE, roleEquipe.getRefRole());
        attributs.put(ATTRIBUT_REF_UTILISATEUR, roleEquipe.getRefUtilisateur());

        //Preparation de WHERE
        Map<String, Object> whereAtt = new HashMap<>();
        whereAtt.put(ATTRIBUT_REF_EQUIPE, roleEquipe.getRefEquipe());

        //Modifier
        DAOUtilitaire.executeUpdate(this.getDaoFactory(), NOM_ENTITE, attributs, whereAtt);

    }

    /**
     * Supprime tous les RoleEquipes ayant pour attributs les mêmes que ceux spécifiés
     * dans un bean Etudiant.
     *
     * @param roleEquipe le RoleEquipe que l'on souhaite supprimer dans la BDD.
     */
    @Override
    public void supprimer(RoleEquipe roleEquipe) {
        Map<String, Object> identifiants = new HashMap<>();
        identifiants.put(ATTRIBUT_REF_EQUIPE, roleEquipe.getRefEquipe());

        DAOUtilitaire.executeDelete(this.getDaoFactory(), NOM_ENTITE, identifiants);
    }

    /**
     * Liste tous les RoleEquipes présents dans la BDD.
     *
     * @return roleEquipes la liste des RoleEquipes présents dans la BDD.
     */
    @Override
    public List<RoleEquipe> lister() {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        List<RoleEquipe> roleEquipes = new ArrayList<>();
        try {
            // création d'une connexion grâce à la DAOFactory placée en attribut de la
            // classe
            connection = this.creerConnexion();
            preparedStatement = connection.prepareStatement(SQL_SELECT_TOUT);
            resultSet = preparedStatement.executeQuery();
            // récupération des valeurs des attributs de la BDD pour les mettre dans une
            // liste
            while (resultSet.next()) {
                roleEquipes.add(map(resultSet));
            }
        } catch (SQLException e) {
            logger.log(Level.WARN, "Échec du listage des objets.", e);
        } finally {
            fermetures(resultSet, preparedStatement, connection);
        }
        return roleEquipes;
    }

    /**
     * Fait la correspondance (le mapping) entre une ligne issue de la table
     * RoleEquipe (un ResultSet) et un bean RoleEquipe.
     *
     * @param resultSet la ligne issue de la table RoleEquipe.
     * @return roleEquipe le bean dont on souhaite faire la correspondance.
     * @throws SQLException exception
     */
    public static RoleEquipe map(ResultSet resultSet) throws SQLException {
        RoleEquipe roleEquipe = new RoleEquipe();
        roleEquipe.setRefEquipe(resultSet.getLong(ATTRIBUT_REF_EQUIPE));
        roleEquipe.setRefUtilisateur(resultSet.getLong(ATTRIBUT_REF_UTILISATEUR));
        roleEquipe.setRefRole(resultSet.getLong(ATTRIBUT_REF_ROLE));
        return roleEquipe;
    }
}
