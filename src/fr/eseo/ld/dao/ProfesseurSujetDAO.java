package fr.eseo.ld.dao;

import fr.eseo.ld.beans.ProfesseurSujet;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static fr.eseo.ld.dao.DAOUtilitaire.fermetures;
import static fr.eseo.ld.dao.DAOUtilitaire.initialisationRequetePreparee;

/**
 * Classe du DAO du bean ProfesseurSujet.
 *
 * <p>
 * Cette classe implémente toutes les méthodes CRUD définies dans sa classe
 * mère.
 * </p>
 */
public class ProfesseurSujetDAO extends DAO<ProfesseurSujet> {

    private static final String NOM_ENTITE = "ProfesseurSujet";

    protected static final String ATTRIBUT_ID_PROFESSEUR = "idProfesseur";
    protected static final String ATTRIBUT_ID_SUJET = "idSujet";
    protected static final String ATTRIBUT_FONCTION = "fonction";
    protected static final String ATTRIBUT_VALIDE = "valide";

    private static final String SQL_SELECT_TOUT = "SELECT * FROM ProfesseurSujet";

	private static final String[] ATTRIBUTS_NOMS = {ATTRIBUT_ID_PROFESSEUR, ATTRIBUT_ID_SUJET, ATTRIBUT_FONCTION, ATTRIBUT_VALIDE};

	private static final String DELETE = "DELETE";

	private static final String SQL_INSERT = "INSERT INTO ProfesseurSujet (idProfesseur, idSujet, fonction, valide) VALUES (?,?,?,?)";

    private static Logger logger = Logger.getLogger(ProfesseurDAO.class.getName());

    /**
     * Constructeur de DAO.
     *
     * @param daoFactory la Factory permettant la création d'une connexion à la BDD.
     */
    public ProfesseurSujetDAO(DAOFactory daoFactory) {
        super(daoFactory);
    }


    @Override
    public void creer(ProfesseurSujet sujet) {
    	Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			// les "?" de la requête sont comblés par les attributs de l'objet en paramètre
			// ceux-ci peuvent être nuls à condition que la BDD accepte les données de type
			// "null"
			preparedStatement = initialisationRequetePreparee(connection, SQL_INSERT, true, sujet.getIdProfesseur(), sujet.getIdSujet(), sujet.getFonction().getFonction(),sujet.getValide());
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec de la création de l'objet, aucune ligne ajoutée dans la table.", e);
		} finally {
			// fermeture des ressources utilisées
			fermetures(preparedStatement, connection);
		}
    }

    @Override
    public List<ProfesseurSujet> trouver(ProfesseurSujet sujet) {
        return new ArrayList<>();
    }

    /**
     * Modifie l'association professeurSujet avec les paramètres spécifiés
     *
     * @param ancien  l'ancienne association concernée.
     * @param nouveau la nouvelle association
     */
    public void modifier(ProfesseurSujet nouveau, ProfesseurSujet ancien) {
        /* Traite la mise à jour de la BDD */
        Map<String, Object> attributs = new HashMap<>();
        Map<String, Object> whereValues = new HashMap<>();

        if (nouveau.getIdProfesseur() != null) {
            attributs.put(ATTRIBUT_ID_PROFESSEUR, nouveau.getIdProfesseur());
        }
        if (nouveau.getIdSujet() != null) {
            attributs.put(ATTRIBUT_ID_SUJET, nouveau.getIdSujet());
        }
        if (nouveau.getFonction() != null) {
            attributs.put(ATTRIBUT_FONCTION, nouveau.getFonction().getFonction());
        }
        if (nouveau.getValide() != null) {
            attributs.put(ATTRIBUT_VALIDE, nouveau.getValide());
        }

        whereValues.put(ATTRIBUT_ID_PROFESSEUR, ancien.getIdProfesseur());
        whereValues.put(ATTRIBUT_ID_SUJET, ancien.getIdSujet());
        whereValues.put(ATTRIBUT_FONCTION, ancien.getFonction().getFonction());

        DAOUtilitaire.executeUpdate(this.getDaoFactory(), NOM_ENTITE, attributs, whereValues);
    }

    /**
	 * Supprime le reférent spécifié au sujet spécifié
	 *
	 * @param professeurSujet
	 *            la correspondance que l'on souhaite supprimer dans la BDD.
	 */
    @Override
    public void supprimer(ProfesseurSujet professeurSujet) {
    	Connection connection = null;
    	PreparedStatement preparedStatement = null;
		// tableau de Strings regroupant tous les noms des attributs d'un objet ainsi
		// que les valeurs correspondantes
		String[][] attributs = { ATTRIBUTS_NOMS,
				{ String.valueOf(professeurSujet.getIdProfesseur()), String.valueOf(professeurSujet.getIdSujet()), professeurSujet.getFonction().getFonction(), professeurSujet.getValide() } };
		try {
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			// mise en forme de la requête DELETE en fonction des attributs de l'objet
			// utilisateur
			preparedStatement = initialisationRequetePreparee(connection, DELETE, NOM_ENTITE, attributs, false);
			int statut = preparedStatement.executeUpdate();
			if (statut == 0) {
				throw new SQLException();
			} else {
				// suppression du bean
				professeurSujet.setIdSujet(null);
			}
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec de la suppression de l'objet, aucune ligne supprimée de la table.", e);
		} finally {
			// fermeture des ressources utilisées
			fermetures(preparedStatement, connection);
		}
    }

    /**
     * Liste tous les ProfesseurSujets présents dans la BDD.
     *
     * @return professeurSujet la liste des ProfesseurSujets présents dans la BDD.
     */
    @Override
    public List<ProfesseurSujet> lister() {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        List<ProfesseurSujet> professeursSujet = new ArrayList<>();
        try {
            // création d'une connexion grâce à la DAOFactory placée en attribut de la
            // classe
            connection = this.creerConnexion();
            preparedStatement = connection.prepareStatement(SQL_SELECT_TOUT);
            resultSet = preparedStatement.executeQuery();
            // récupération des valeurs des attributs de la BDD pour les mettre dans une
            // liste
            while (resultSet.next()) {
                professeursSujet.add(map(resultSet));
            }
        } catch (SQLException e) {
            logger.log(Level.WARN, "Échec du listage des objets.", e);
        } finally {
            fermetures(resultSet, preparedStatement, connection);
        }
        return professeursSujet;
    }

    /**
     * Fait la correspondance (le mapping) entre une ligne issue de la table
     * ProfesseurSujet (un ResultSet) et un bean ProfesseurSujet.
     *
     * @param resultSet la ligne issue de la table ProfesseurSujet.
     * @return professeurSujet le bean dont on souhaite faire la correspondance.
     * @throws SQLException
     */
    public static ProfesseurSujet map(ResultSet resultSet) throws SQLException {
        ProfesseurSujet professeurSujet = new ProfesseurSujet();
        professeurSujet.setIdProfesseur(resultSet.getLong(ATTRIBUT_ID_PROFESSEUR));
        professeurSujet.setIdSujet(resultSet.getLong(ATTRIBUT_ID_SUJET));
        professeurSujet.setFonctionString(resultSet.getString(ATTRIBUT_FONCTION));
        professeurSujet.setValide(resultSet.getString(ATTRIBUT_VALIDE));
        return professeurSujet;
    }


    @Override
    public void modifier(ProfesseurSujet objet) {
        //Une modification => Un nouvel objet
    }
}