package fr.eseo.ld.dao;

import fr.eseo.ld.beans.JurySoutenance;
import fr.eseo.ld.beans.Soutenance;
import fr.eseo.ld.beans.Utilisateur;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static fr.eseo.ld.dao.DAOUtilitaire.*;

/**
 * Classe du DAO du bean JurySoutenance.
 * 
 * <p>
 * Cette classe implémente toutes les méthodes CRUD définies dans sa classe
 * mère.
 * </p>
 * 
 * @author Julie AVIZOU
 */
public class JurySoutenanceDAO extends DAO<JurySoutenance> {

	private static final String NOM_ENTITE = "JurySoutenance";
	private static final String ATTRIBUT_ID_JURY_SOUTENANCE = "idJurySoutenance";
	private static final String ATTRIBUT_ID_PROF1 = "idProf1";
	private static final String ATTRIBUT_ID_PROF2 = "idProf2";
	private static final String[] ATTRIBUTS_NOMS = { ATTRIBUT_ID_JURY_SOUTENANCE, ATTRIBUT_ID_PROF1,
			ATTRIBUT_ID_PROF2 };
	private static final String[] ATTRIBUTS_NOMS_ID_FIN = { ATTRIBUT_ID_PROF1, ATTRIBUT_ID_PROF2,
			ATTRIBUT_ID_JURY_SOUTENANCE };

	// tous les attributs du bean doivent être écrits dans la requête INSERT
	private static final String SQL_INSERT = "INSERT INTO JurySoutenance (idProf1, idProf2) VALUES (?,?)";
	private static final String SQL_SELECT_TOUT = "SELECT * FROM JurySoutenance";
	private static final String SQL_SELECT_JURYS_SOUTENANCE_PROF = "SELECT * FROM `JurySoutenance` WHERE ? in (idProf1,idProf2)";
	private static final String SQL_SELECT_JURY_SOUTENANCE = "SELECT jurysoutenance.* FROM `jurysoutenance`, soutenance WHERE soutenance.idJurySoutenance = jurysoutenance.idJurySoutenance and soutenance.idSoutenance = ?";

	private static Logger logger = Logger.getLogger(JurySoutenanceDAO.class.getName());

	/**
	 * Constructeur de DAO.
	 * 
	 * @param daoFactory
	 *            la Factory permettant la création d'une connexion à la BDD.
	 */
	public JurySoutenanceDAO(DAOFactory daoFactory) {
		super(daoFactory);
	}

	/**
	 * Insère un JurySoutenance dans la BDD à partir des attributs spécifiés dans un
	 * bean JurySoutenance.
	 * 
	 * @param jurySoutenance
	 *            le JurySoutenance que l'on souhaite insérer dans la BDD à partir
	 *            du bean JurySoutenance.
	 */
	@Override
	public void creer(JurySoutenance jurySoutenance) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			// les "?" de la requête sont comblés par les attributs de l'objet en paramètre
			// ceux-ci peuvent être nuls à condition que la BDD accepte les données de type
			// "null"
			preparedStatement = initialisationRequetePreparee(connection, SQL_INSERT, true, jurySoutenance.getIdProf1(),
					jurySoutenance.getIdProf2());
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec de la création de l'objet, aucune ligne ajoutée dans la table.", e);
		} finally {
			// fermeture des ressources utilisées
			fermetures(preparedStatement, connection);
		}
	}

	/**
	 * Liste tous les JurySoutenance ayant pour attributs les mêmes que ceux
	 * spécifiés dans un bean JurySoutenance.
	 * 
	 * @param jurySoutenance
	 *            le JurySoutenance que l'on souhaite trouver dans la BDD.
	 * @return jurySoutenances la liste des JurySoutenance trouvés dans la BDD.
	 */
	@Override
	public List<JurySoutenance> trouver(JurySoutenance jurySoutenance) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<JurySoutenance> jurySoutenances = new ArrayList<>();
		// tableau de Strings regroupant tous les noms des attributs d'un objet ainsi
		// que les valeurs correspondantes
		String[][] attributs = { ATTRIBUTS_NOMS, { jurySoutenance.getIdJurySoutenance(),
				String.valueOf(jurySoutenance.getIdProf1()), String.valueOf(jurySoutenance.getIdProf2()) } };
		try {
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			// mise en forme de la requête SELECT en fonction des attributs de l'objet
			// jurySoutenance
			preparedStatement = initialisationRequetePreparee(connection, "SELECT", NOM_ENTITE, attributs, false);
			resultSet = preparedStatement.executeQuery();
			// récupération des valeurs des attributs de la BDD pour les mettre dans une
			// liste
			while (resultSet.next()) {
				jurySoutenances.add(map(resultSet));
			}
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec de la recherche de l'objet.", e);
		} finally {
			// fermeture des ressources utilisées
			fermetures(resultSet, preparedStatement, connection);
		}
		return jurySoutenances;
	}

	/**
	 * Modifie UN JurySoutenance ayant pour attributs les mêmes que ceux spécifiés
	 * dans un bean JurySoutenance et la même clé primaire. Cette clé primaire ne
	 * peut être modifiée.
	 * 
	 * @param jurySoutenance
	 *            le JurySoutenance que l'on souhaite modifier dans la BDD.
	 */
	@Override
	public void modifier(JurySoutenance jurySoutenance) {
		// tableau de Strings regroupant tous les noms des attributs d'un objet ainsi
		// que les valeurs correspondantes
		// /!\ bien placer la clé primaire et sa valeur à la FIN du tableau sinon erreur
		String[][] attributs = { ATTRIBUTS_NOMS_ID_FIN, { String.valueOf(jurySoutenance.getIdProf1()),
				String.valueOf(jurySoutenance.getIdProf2()), jurySoutenance.getIdJurySoutenance() } };

		/* Traite la mise à jour de la BDD */
		traitementUpdate(this.getDaoFactory(), NOM_ENTITE, attributs, logger);
	}

	/**
	 * Supprime tous les JurySoutenance ayant pour attributs les mêmes que ceux
	 * spécifiés dans un bean JurySoutenance.
	 * 
	 * @param jurySoutenance
	 *            le JurySoutenance que l'on souhaite supprimer dans la BDD.
	 */
	@Override
	public void supprimer(JurySoutenance jurySoutenance) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		// tableau de Strings regroupant tous les noms des attributs d'un objet ainsi
		// que les valeurs correspondantes
		String[][] attributs = { ATTRIBUTS_NOMS, { jurySoutenance.getIdJurySoutenance(),
				String.valueOf(jurySoutenance.getIdProf1()), String.valueOf(jurySoutenance.getIdProf2()) } };
		try {
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			preparedStatement = initialisationRequetePreparee(connection, "DELETE", NOM_ENTITE, attributs, false);
			int statut = preparedStatement.executeUpdate();
			if (statut == 0) {
				throw new SQLException();
			} else {
				// suppression du bean
				jurySoutenance.setIdJurySoutenance(null);
			}
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec de la suppression de l'objet, aucune ligne supprimée de la table.", e);
		} finally {
			// fermeture des ressources utilisées
			fermetures(preparedStatement, connection);
		}
	}

	/**
	 * Liste tous les JurySoutenance présents dans la BDD.
	 * 
	 * @return jurySoutenances la liste des JurySoutenance présents dans la BDD.
	 */
	@Override
	public List<JurySoutenance> lister() {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<JurySoutenance> jurySoutenances = new ArrayList<>();
		try {
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			preparedStatement = connection.prepareStatement(SQL_SELECT_TOUT);
			resultSet = preparedStatement.executeQuery();
			// récupération des valeurs des attributs de la BDD pour les mettre dans une
			// liste
			while (resultSet.next()) {
				jurySoutenances.add(map(resultSet));
			}
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec du listage des objets.", e);
		} finally {
			fermetures(resultSet, preparedStatement, connection);
		}
		return jurySoutenances;
	}

	/**
	 * Liste tous les JurySoutenance associés à un Utilisateur donné (ici un
	 * Professeur).
	 * 
	 * @param utilisateur
	 *            l'utilisateur associé aux JurySoutenance.
	 * @return jurySoutenances la liste des JurySoutenance associés à l'utilisateur.
	 */
	public List<JurySoutenance> listerJurysUtilisateur(Utilisateur utilisateur) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<JurySoutenance> jurySoutenances = new ArrayList<>();
		try {
			// vérification que l'utilisateur rentré n'est pas nul
			if (utilisateur.getIdUtilisateur() == null) {
				throw new SQLException();
			}
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			preparedStatement = initialisationRequetePreparee(connection, SQL_SELECT_JURYS_SOUTENANCE_PROF, true,
					utilisateur.getIdUtilisateur());
			resultSet = preparedStatement.executeQuery();
			// récupération des valeurs des attributs de la BDD pour les mettre dans une
			// liste
			while (resultSet.next()) {
				jurySoutenances.add(map(resultSet));
			}
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec du listage des jurySoutenance associés à un utilisateur.", e);
		} finally {
			fermetures(resultSet, preparedStatement, connection);
		}
		return jurySoutenances;
	}
	
	/**
	 * Trouve le JurySoutenance associé à une soutenance donnée.
	 * 
	 * @param soutenanceChoisie
	 *            la soutenance associée au JurySoutenance.
	 * @return jurySoutenance la JurySoutenance associée.
	 */
	public JurySoutenance trouverJurySoutenance(Soutenance soutenanceChoisie) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		JurySoutenance jurySoutenance = new JurySoutenance();
		try {
			// vérification que la soutenance rentré n'est pas nulle
			if (soutenanceChoisie.getIdSoutenance() == null) {
				throw new SQLException();
			}
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			preparedStatement = initialisationRequetePreparee(connection, SQL_SELECT_JURY_SOUTENANCE, true,
					soutenanceChoisie.getIdSoutenance());
			resultSet = preparedStatement.executeQuery();
			// récupération des valeurs des attributs de la BDD pour les mettre dans une
			// liste
			while (resultSet.next()) {
				jurySoutenance = this.map(resultSet);
			}
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec du listage des jurySoutenance associés à un utilisateur.", e);
		} finally {
			fermetures(resultSet, preparedStatement, connection);
		}
		return jurySoutenance;
	}

	/**
	 * Fait la correspondance (le mapping) entre une ligne issue de la table
	 * JurySoutenance (un ResultSet) et un bean JurySoutenance.
	 * 
	 * @param resultSet
	 *            la ligne issue de la table JurySoutenance.
	 * @return jurySoutenance le bean dont on souhaite faire la correspondance.
	 * @throws SQLException
	 */
	public static JurySoutenance map(ResultSet resultSet) throws SQLException {
		JurySoutenance jurySoutenance = new JurySoutenance();
		jurySoutenance.setIdJurySoutenance(resultSet.getString(ATTRIBUT_ID_JURY_SOUTENANCE));
		/* vérification car quand la bd retourne nul resultSet.getLong retourne 0 */
		jurySoutenance.setIdProf1(resultSet.getObject(ATTRIBUT_ID_PROF1) != null ? resultSet.getLong(ATTRIBUT_ID_PROF1) : null);
		jurySoutenance.setIdProf2(resultSet.getObject(ATTRIBUT_ID_PROF2) != null ? resultSet.getLong(ATTRIBUT_ID_PROF2) : null);
		return jurySoutenance;
	}

}