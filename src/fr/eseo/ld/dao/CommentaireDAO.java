package fr.eseo.ld.dao;

import fr.eseo.ld.beans.Commentaire;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static fr.eseo.ld.dao.DAOUtilitaire.*;

/**
 * Classe du DAO du bean Commentaire.
 *
 * <p>
 * Cette classe implémente toutes les méthodes CRUD définies dans sa classe
 * mère.
 * </p>
 *
 * @author Hugo Menard
 */
public class CommentaireDAO extends DAO<Commentaire> {

    private static final String NOM_ENTITE = "Commentaire";
    private static final String ATTRIBUT_ID_COMMENTAIRE = "idCommentaire";
    private static final String ATTRIBUT_CONTENU = "contenu";
    private static final String ATTRIBUT_ID_UTILISATEUR = "idUtilisateur";
    private static final String ATTRIBUT_ID_SUJET = "idSujet";
    private static final String ATTRIBUT_ID_OBSERVATION = "idObservation";
    private static final String[] ATTRIBUTS_NOMS = {ATTRIBUT_ID_COMMENTAIRE, ATTRIBUT_CONTENU, ATTRIBUT_ID_UTILISATEUR,
            ATTRIBUT_ID_SUJET, ATTRIBUT_ID_OBSERVATION};
    private static final String[] ATTRIBUTS_NOMS_ID_FIN = {ATTRIBUT_CONTENU, ATTRIBUT_ID_UTILISATEUR,
            ATTRIBUT_ID_SUJET, ATTRIBUT_ID_OBSERVATION, ATTRIBUT_ID_COMMENTAIRE};

    // tous les attributs du bean doivent être écrits dans la requête INSERT
    private static final String SQL_INSERT = "INSERT INTO Commentaire (contenu, idUtilisateur, idSujet, idObservation) VALUES (?, ?, ?, ?)";

    private static Logger logger = Logger.getLogger(CommentaireDAO.class.getName());

    /**
     * Constructeur de DAO.
     *
     * @param daoFactory la Factory permettant la création d'une connexion à la BDD.
     */
    public CommentaireDAO(DAOFactory daoFactory) {
        super(daoFactory);
    }

    /**
     * Insère un Commentaire dans la BDD à partir des attributs spécifiés dans un
     * bean Commentaire.
     *
     * @param commentaire le commentaire que l'on souhaite insérer dans la BDD à partir du
     *                    bean Commentaire.
     */
    @Override
    public void creer(Commentaire commentaire) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            // création d'une connexion grâce à la DAOFactory placée en attribut de la
            // classe
            connection = this.creerConnexion();
            // les "?" de la requête sont comblés par les attributs de l'objet en paramètre
            // ceux-ci peuvent être nuls à condition que la BDD accepte les données de type
            // "null"
            preparedStatement = initialisationRequetePreparee(connection, SQL_INSERT, true, commentaire.getContenu(),
                    String.valueOf(commentaire.getIdUtilisateur()), String.valueOf(commentaire.getIdSujet()),
                    String.valueOf(commentaire.getIdObservation()));
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            logger.log(Level.WARN, "Échec de la création de l'objet, aucune ligne ajoutée dans la table.", e);
        } finally {
            // fermeture des ressources utilisées
            fermetures(preparedStatement, connection);
        }
    }

    /**
     * Liste tous les Commentaires ayant pour attributs les mêmes que ceux spécifiés
     * dans un bean Commentaire.
     *
     * @param commentaire le Commentaire que l'on souhaite trouver dans la BDD.
     * @return commentaire la liste des Commentaires trouvés dans la BDD.
     */
    @Override
    public List<Commentaire> trouver(Commentaire commentaire) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        List<Commentaire> commentaires = new ArrayList<>();
        // tableau de Strings regroupant tous les noms des attributs d'un objet ainsi
        // que les valeurs correspondantes
        String[][] attributs = {ATTRIBUTS_NOMS,
                {String.valueOf(commentaire.getIdCommentaire()), commentaire.getContenu(),
                        String.valueOf(commentaire.getIdUtilisateur()), String.valueOf(commentaire.getIdSujet()),
                        String.valueOf(commentaire.getIdObservation())}};
        try {
            // création d'une connexion grâce à la DAOFactory placée en attribut de la
            // classe
            connection = this.creerConnexion();
            // mise en forme de la requête SELECT en fonction des attributs de l'objet sujet
            preparedStatement = DAOUtilitaire.initialisationRequetePreparee(connection, "SELECT", NOM_ENTITE, attributs,
                    false);
            resultSet = preparedStatement.executeQuery();
            // récupération des valeurs des attributs de la BDD pour les mettre dans une
            // liste
            while (resultSet.next()) {
                commentaires.add(map(resultSet));
            }
        } catch (SQLException e) {
            logger.log(Level.WARN, "Échec de la recherche de l'objet.", e);
        } finally {
            // fermeture des ressources utilisées
            fermetures(resultSet, preparedStatement, connection);
        }
        return commentaires;
    }

    /**
     * Modifie UN Commentaire ayant pour attributs les mêmes que ceux spécifiés dans
     * un bean Commentaire et la même clé primaire. Cette clé primaire ne peut être
     * modifiée.
     *
     * @param commentaire le Commentaire que l'on souhaite modifier dans la BDD.
     */
    @Override
    public void modifier(Commentaire commentaire) {
        // tableau de Strings regroupant tous les noms des attributs d'un objet ainsi
        // que les valeurs correspondantes
        // /!\ bien placer la clé primaire et sa valeur à la FIN du tableau sinon erreur
        String[][] attributs = {ATTRIBUTS_NOMS_ID_FIN,
                {commentaire.getContenu(), String.valueOf(commentaire.getIdUtilisateur()),
                        String.valueOf(commentaire.getIdSujet()), String.valueOf(commentaire.getIdObservation()),
                        String.valueOf(commentaire.getIdCommentaire())}};

        /* Traite la mise à jour de la BDD */
        traitementUpdate(this.getDaoFactory(), NOM_ENTITE, attributs, logger);
    }

    /**
     * Supprime tous les Commentaires ayant pour attributs les mêmes que ceux
     * spécifiés dans un bean Commentaire.
     *
     * @param commentaire le Commentaire que l'on souhaite supprimer dans la BDD.
     */
    @Override
    public void supprimer(Commentaire commentaire) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        // tableau de Strings regroupant tous les noms des attributs d'un objet ainsi
        // que les valeurs correspondantes
        String[][] attributs = {ATTRIBUTS_NOMS,
                {String.valueOf(commentaire.getIdCommentaire()), commentaire.getContenu(),
                        String.valueOf(commentaire.getIdUtilisateur()), String.valueOf(commentaire.getIdSujet()),
                        String.valueOf(commentaire.getIdObservation())}};
        try {
            // création d'une connexion grâce à la DAOFactory placée en attribut de la
            // classe
            connection = this.creerConnexion();
            preparedStatement = initialisationRequetePreparee(connection, "DELETE", NOM_ENTITE, attributs, false);
            int statut = preparedStatement.executeUpdate();
            if (statut == 0) {
                throw new SQLException();
            } else {
                // suppression du bean
                commentaire.setIdCommentaire(null);
            }
        } catch (SQLException e) {
            logger.log(Level.WARN, "Échec de la suppression de l'objet, aucune ligne supprimée de la table.", e);
        } finally {
            // fermeture des ressources utilisées
            fermetures(preparedStatement, connection);
        }
    }

    /**
     * Non implémentée sur CommentaireDAO. Pas nécessaire et peu sécurisée.
     */
    @Override
    public List<Commentaire> lister() {
        return new ArrayList<>();
    }

    /**
     * Fait la correspondance (le mapping) entre une ligne issue de la table
     * Commentaire (un ResultSet) et un bean Commentaire.
     *
     * @param resultSet la ligne issue de la table Commentaire.
     * @return commentaire le bean dont on souhaite faire la correspondance.
     * @throws SQLException
     */
    public static Commentaire map(ResultSet resultSet) throws SQLException {
        Commentaire commentaire = new Commentaire();
        commentaire.setIdCommentaire(resultSet.getLong(ATTRIBUT_ID_COMMENTAIRE));
        commentaire.setContenu(resultSet.getString(ATTRIBUT_CONTENU));
        commentaire.setIdUtilisateur(resultSet.getLong(ATTRIBUT_ID_UTILISATEUR));
        commentaire.setIdSujet(resultSet.getLong(ATTRIBUT_ID_SUJET));
        commentaire.setIdObservation(resultSet.getLong(ATTRIBUT_ID_OBSERVATION));
        return commentaire;
    }

}