package fr.eseo.ld.dao;

import fr.eseo.ld.beans.NoteInteretSujet;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static fr.eseo.ld.dao.DAOUtilitaire.fermetures;
import static fr.eseo.ld.dao.DAOUtilitaire.initialisationRequetePreparee;

/**
 * Classe du DAO du bean NoteInteretSujet.
 * 
 * <p>
 * Cette classe implémente toutes les méthodes CRUD définies dans sa classe
 * mère.
 * </p>
 * 
 * @author Maxime LENORMAND
 */
public class NoteInteretSujetDAO extends DAO<NoteInteretSujet> {

	private static final String NOM_ENTITE = "NoteInteretSujet";
	private static final String[] ATTRIBUTS_NOMS = { DAOUtilitaire.ATTRIBUT_ID_PROFESSEUR,
			DAOUtilitaire.ATTRIBUT_ID_SUJET, DAOUtilitaire.ATTRIBUT_NOTE };
	private static final String[] ATTRIBUTS_NOMS_ID_FIN = { DAOUtilitaire.ATTRIBUT_NOTE,
			DAOUtilitaire.ATTRIBUT_ID_PROFESSEUR, DAOUtilitaire.ATTRIBUT_ID_SUJET };

	// tous les attributs du bean doivent être écrits dans la requête INSERT
	private static final String SQL_INSERT = "INSERT INTO NoteInteretSujet (idProfesseur, idSujet, note) VALUES (?, ?, ?)";
	private static final String SQL_SELECT_TOUT = "SELECT * FROM NoteInteretSujet";

	private static Logger logger = Logger.getLogger(NoteInteretSujetDAO.class.getName());

	/**
	 * Constructeur de DAO.
	 * 
	 * @param daoFactory
	 *            la Factory permettant la création d'une connexion à la BDD.
	 */
	NoteInteretSujetDAO(DAOFactory daoFactory) {
		super(daoFactory);
	}

	/**
	 * Insère une NoteInteretSujet dans la BDD à partir des attributs spécifiés dans
	 * un bean NoteInteretSujet.
	 * 
	 * @param noteInteretSujet
	 *            la NoteInteretSujet que l'on souhaite insérer dans la BDD à partir
	 *            du bean NoteInteretSujet.
	 */
	@Override
	public void creer(NoteInteretSujet noteInteretSujet) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			// les "?" de la requête sont comblés par les attributs de l'objet en paramètre
			// ceux-ci peuvent être nuls à condition que la BDD accepte les données de type
			// "null"
			preparedStatement = initialisationRequetePreparee(connection, SQL_INSERT, true,
					noteInteretSujet.getIdProfesseur(), noteInteretSujet.getIdSujet(), noteInteretSujet.getNote());
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec de la création de l'objet, aucune ligne ajoutée dans la table.", e);
		} finally {
			// fermeture des ressources utilisées
			fermetures(preparedStatement, connection);
		}
	}

	/**
	 * Liste toutes les NoteInteretSujets ayant pour attributs les mêmes que ceux
	 * spécifiés dans un bean NoteInteretSujet.
	 * 
	 * @param noteInteretSujet
	 *            la NoteInteretSujet que l'on souhaite trouver dans la BDD.
	 * @return noteInteretSujets la liste des NoteInteretSujet trouvées dans la BDD.
	 */
	@Override
	public List<NoteInteretSujet> trouver(NoteInteretSujet noteInteretSujet) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<NoteInteretSujet> noteInteretSujets = new ArrayList<>();
		// tableau de Strings regroupant tous les noms des attributs d'un objet ainsi
		// que les valeurs correspondantes
		String[][] attributs = { ATTRIBUTS_NOMS, { String.valueOf(noteInteretSujet.getIdProfesseur()),
				String.valueOf(noteInteretSujet.getIdSujet()), String.valueOf(noteInteretSujet.getNote()) } };
		try {
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			// mise en forme de la requête SELECT en fonction des attributs de l'objet
			preparedStatement = initialisationRequetePreparee(connection, "SELECT", NOM_ENTITE, attributs, false);
			resultSet = preparedStatement.executeQuery();
			// récupération des valeurs des attributs de la BDD pour les mettre dans une
			// liste
			while (resultSet.next()) {
				noteInteretSujets.add(map(resultSet));
			}
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec de la recherche de l'objet.", e);
		} finally {
			// fermeture des ressources utilisées
			fermetures(resultSet, preparedStatement, connection);
		}
		return noteInteretSujets;
	}

	/**
	 * Modifie UNE NoteInteretSujet ayant pour attributs les mêmes que ceux
	 * spécifiés dans un bean NoteInteretSujet et la même clé primaire. Cette clé
	 * primaire ne peut être modifiée.
	 * 
	 * @param noteInteretSujet
	 *            la NoteInteretSujet que l'on souhaite modifier dans la BDD.
	 */
	@Override
	public void modifier(NoteInteretSujet noteInteretSujet) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		// tableau de Strings regroupant tous les noms des attributs d'un objet ainsi
		// que les valeurs correspondantes
		// /!\ bien placer la clé primaire et sa valeur à la FIN du tableau sinon erreur
		String[][] attributs = { ATTRIBUTS_NOMS_ID_FIN, { String.valueOf(noteInteretSujet.getNote()),
				String.valueOf(noteInteretSujet.getIdProfesseur()), String.valueOf(noteInteretSujet.getIdSujet()) } };
		try {
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			// mise en forme de la requête UPDATE en fonction des attributs de l'objet
			// noteInteretSujet
			preparedStatement = initialisationRequetePreparee(connection, "UPDATE", NOM_ENTITE, attributs, "Professeur",
					"Sujet", false);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec de la mise à jour de l'objet, aucune ligne modifée dans la table.", e);
		} finally {
			// fermeture des ressources utilisées
			fermetures(preparedStatement, connection);
		}
	}

	/**
	 * Supprime toutes les NoteInteretSujet ayant pour attributs les mêmes que ceux
	 * spécifiés dans un bean NoteInteretSujet.
	 * 
	 * @param noteInteretSujet
	 *            la NoteInteretSujet que l'on souhaite supprimer dans la BDD.
	 */
	@Override
	public void supprimer(NoteInteretSujet noteInteretSujet) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		// tableau de Strings regroupant tous les noms des attributs d'un objet ainsi
		// que les valeurs correspondantes
		String[][] attributs = { ATTRIBUTS_NOMS, { String.valueOf(noteInteretSujet.getIdProfesseur()),
				String.valueOf(noteInteretSujet.getIdSujet()), String.valueOf(noteInteretSujet.getNote()) } };
		try {
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			preparedStatement = initialisationRequetePreparee(connection, "DELETE", NOM_ENTITE, attributs, false);
			int statut = preparedStatement.executeUpdate();
			if (statut == 0) {
				throw new SQLException();
			} else {
				// suppression du bean
				noteInteretSujet.setIdProfesseur(null);
				noteInteretSujet.setIdSujet(null);
			}
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec de la suppression de l'objet, aucune ligne supprimée de la table.", e);
		} finally {
			// fermeture des ressources utilisées
			fermetures(preparedStatement, connection);
		}
	}

	/**
	 * Liste toutes les NoteInteretSujet présentes dans la BDD.
	 * 
	 * @return noteInteretSujets la liste des NoteInteretSujet présentes dans la
	 *         BDD.
	 */
	@Override
	public List<NoteInteretSujet> lister() {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<NoteInteretSujet> noteInteretSujets = new ArrayList<>();
		try {
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			preparedStatement = connection.prepareStatement(SQL_SELECT_TOUT);
			resultSet = preparedStatement.executeQuery();
			// récupération des valeurs des attributs de la BDD pour les mettre dans une
			// liste
			while (resultSet.next()) {
				noteInteretSujets.add(map(resultSet));
			}
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec du listage des objets.", e);
		} finally {
			fermetures(resultSet, preparedStatement, connection);
		}
		return noteInteretSujets;
	}

	/**
	 * Fait la correspondance (le mapping) entre une ligne issue de la table
	 * NoteInteretSujet (un ResultSet) et un bean NoteInteretSujet.
	 * 
	 * @param resultSet
	 *            la ligne issue de la table NoteInteretSujet.
	 * @return noteInteretSujet le bean dont on souhaite faire la correspondance.
	 * @throws SQLException
	 */
	public static NoteInteretSujet map(ResultSet resultSet) throws SQLException {
		NoteInteretSujet noteInteretSujet = new NoteInteretSujet();
		noteInteretSujet.setIdProfesseur(resultSet.getLong(DAOUtilitaire.ATTRIBUT_ID_PROFESSEUR));
		noteInteretSujet.setIdSujet(resultSet.getLong(DAOUtilitaire.ATTRIBUT_ID_SUJET));
		noteInteretSujet.setNote(resultSet.getInt(DAOUtilitaire.ATTRIBUT_NOTE));
		return noteInteretSujet;
	}

}