package fr.eseo.ld.dao;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import fr.eseo.ld.utils.IOUtilitaire;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

/**
 * Classe d'instanciation d'une Factory de DAO.
 *
 * <p>
 * Cette classe ne sera instanciée que si les informations de configuration sont
 * correctes.
 * </p>
 *
 * @author Thomas MENARD et Maxime LENORMAND
 * @see fr.eseo.ld.config.InitialisationDAOFactory
 */
public class DAOFactory {

	private static final String FICHIER_PROPERTIES = "/fr/eseo/ld/dao/dao.properties";
	private static final String FICHIER_PROPERTIES_DEFAUT = "/fr/eseo/ld/dao/daoDefaut.properties";
	private static final String PROPERTY_URL = "url";
	private static final String PROPERTY_DRIVER = "driver";
	private static final String PROPERTY_NOM_UTILISATEUR = "nomUtilisateur";
	private static final String PROPERTY_MOT_DE_PASSE = "motDePasse";

	private static DAOFactory instance;

	private String urlBdd;
	private String driver;
	private String nomUtilisateur;
	private String motDePasse;

	private HikariDataSource dataSource;
	private static Logger logger = Logger.getLogger(DAOFactory.class.getName());

	/**
	 * Constructeur de la Factory permettant une simple connexion à la base de
	 * données.
	 *
	 * @param url url
	 * @param nomUtilisateur nom
	 * @param motDePasse pass
	 */
	public DAOFactory(String url, String nomUtilisateur, String motDePasse) {
		this.urlBdd = url;
		this.nomUtilisateur = nomUtilisateur;
		this.motDePasse = motDePasse;
	}

	private DAOFactory() {
		//Récupération des properties
		this.rechargerProprietesDepuisFichier(FICHIER_PROPERTIES);
	}

	/**
	 * Constructeur de la Factory permettant une connexion au pool de connexions.
	 *
	 * @param dataSource
	 *            la Factory permettant la création d'une connexion à la BDD.
	 */
	private DAOFactory(HikariDataSource dataSource) {
		this.dataSource = dataSource;
	}


	/**
	 * Charge les propriétés par défaut
	 */
	public void rechargerFichierProprieteParDefaut() {
		this.rechargerProprietesDepuisFichier(FICHIER_PROPERTIES_DEFAUT);
	}

	/**
	 * Récupère une instance de la Factory permettant une simple connexion à la base
	 * de données.
	 * Singleton
	 *
	 * @return l'instance de la Factory.
	 */
	public static DAOFactory getInstance() {
		if(instance == null) {
			instance = new DAOFactory();
		}

		return instance;
	}

	/**
	 * Récupère une instance de la Factory permettant une connexion au pool de
	 * connexions.
	 *
	 * @return l'instance de la Factory.
	 */
	public static DAOFactory getInstancePool() {
		/* Récupération des proprietes de connexion */
		Map<String, String> proprietes = IOUtilitaire.lireFichierProperties(FICHIER_PROPERTIES);
		String url = proprietes.get(PROPERTY_URL);
		String nomUtilisateur = proprietes.get(PROPERTY_NOM_UTILISATEUR);
        String motDePasse = proprietes.get(PROPERTY_MOT_DE_PASSE);
        String driver = proprietes.get(PROPERTY_DRIVER);

        try {
            Class.forName(driver);
        } catch (ClassNotFoundException e) {
            logger.log(Level.WARN, "Driver inconnu : " + driver, e);
        }

        /*
		 * Création d'une configuration de pool de connexions via l'objet HikariConfig
		 * et les différents setters associés
		 */
		HikariConfig config = new HikariConfig();
		/* Mise en place de l'URL, du nom et du mot de passe */
		config.setJdbcUrl(url);
		config.setUsername(nomUtilisateur);
		config.setPassword(motDePasse);
		config.setDriverClassName(driver);
		/* Paramétrage du pool */
		// nombre maximum de connexions dans la pool
		config.setMaximumPoolSize(1);
		// fixe la durée de vie des connexions
		config.setIdleTimeout(1380);
		config.setMaxLifetime(1380);
		// permet de mettre en cache les requetes preparees
		config.addDataSourceProperty("cachePrepStmts", "true");
		// fixe le nombre de requetes preparees que le Driver met en cache par connexion
		config.addDataSourceProperty("prepStmtCacheSize", "250");
		// fixe la taille maximale d'une requete preparee que le Driver met en cache
		config.addDataSourceProperty("prepStmtCacheSqlLimit", "2048");
		// permet au serveur d'executer des requetes preparees pour gagner en
		// performances
		config.addDataSourceProperty("useServerPrepStmts", "true");

		/*
		 * Création du pool à partir de la configuration, via l'objet HikariDataSource
		 */
		HikariDataSource dataSource = new HikariDataSource(config);

		/*
		 * Enregistrement du pool créé dans une variable d'instance via un appel au
		 * constructeur de DAOFactory
		 */
		return new DAOFactory(dataSource);
	}



	/**
	 * Charge les propriétés de connexion à la base de données.
	 * Change les informations en mémoire
	 */
	private void rechargerProprietesDepuisFichier(String path) {
		final Map<String, String> prop = IOUtilitaire.lireFichierProperties(path);
		this.urlBdd = prop.get(PROPERTY_URL);
		this.driver = prop.get(PROPERTY_DRIVER);
		this.nomUtilisateur = prop.get(PROPERTY_NOM_UTILISATEUR);
		this.motDePasse = prop.get(PROPERTY_MOT_DE_PASSE);

		//Inclusion du driver
		try {
			Class.forName(this.driver);
		} catch (ClassNotFoundException e) {
			logger.log(Level.WARN, "Le driver suivant est inconnu : " + this.driver, e);
		}
	}

	/**
	 * Change les propriétés de connexion à la base de données.
	 */
	public void changerProprietes(String url, String nomUtilisateur, String motDePasse) {
		Map<String, String> proprietes = new HashMap<>();
		proprietes.put(PROPERTY_URL, url);
		proprietes.put(PROPERTY_NOM_UTILISATEUR, nomUtilisateur);
		proprietes.put(PROPERTY_MOT_DE_PASSE, motDePasse);

		IOUtilitaire.ecrireFichierProperties(FICHIER_PROPERTIES, proprietes);
	}


	/* Getter & setter */
	/**
	 * Fournit une connexion à la base de données ou au pool de connexions.
	 *
	 * @return connection la connexion à la base de données ou au pool de
	 *         connexions.
	 * @throws SQLException exception
	 */
	public Connection getConnection() throws SQLException {
		// si on souhaite une simple connexion à la BDD
		if (this.urlBdd != null && this.nomUtilisateur != null && this.motDePasse != null) {
			return DriverManager.getConnection(this.urlBdd, this.nomUtilisateur, this.motDePasse);
		} else { // sinon, on souhaite une connexion au pool de connexions
			return this.dataSource.getConnection();
		}
	}

	public String getUrlBdd() {
		return this.urlBdd;
	}

	public String getUserBdd() {
		return this.nomUtilisateur;
	}

	public String getMdpBdd() {
		return this.motDePasse;
	}

	public String getDriver() {
		return this.driver;
	}

	public String[] getProprietes() {
		return new String[]{this.getUrlBdd(), this.getUserBdd(), this.getMdpBdd()};
	}

	/* Méthodes de récupération des différents DAO */
	public UtilisateurDAO getUtilisateurDao() {
		return new UtilisateurDAO(this);
	}

	public SujetDAO getSujetDao() {
		return new SujetDAO(this);
	}

	public PosterDAO getPosterDao() {
		return new PosterDAO(this);
	}

	public NotePosterDAO getNotePosterDao() {
		return new NotePosterDAO(this);
	}

	public NotificationDAO getNotificationDao() {
		return new NotificationDAO(this);
	}

	public CommentaireDAO getCommentaireDao() {
		return new CommentaireDAO(this);
	}

	public EtudiantDAO getEtudiantDao() {
		return new EtudiantDAO(this);
	}

	public RoleDAO getRoleDAO() { return new RoleDAO(this); }

	public OptionESEODAO getOptionESEODAO() {
		return new OptionESEODAO(this);
	}

	public ParametreDAO getParametreDAO() {
		return new ParametreDAO(this);
	}

	public ProfesseurDAO getProfesseurDAO() {
		return new ProfesseurDAO(this);
	}

	public EquipeDAO getEquipeDAO() {
		return new EquipeDAO(this);
	}

	public JurySoutenanceDAO getJurySoutenanceDAO() {
		return new JurySoutenanceDAO(this);
	}

	public JuryPosterDAO getJuryPosterDAO() {
		return new JuryPosterDAO(this);
	}
	public CompositionJuryDAO getCompositionJuryDAO() {
		return new CompositionJuryDAO(this);
	}
	
	public BonusMalusDAO getBonusMalusDAO() {
		return new BonusMalusDAO(this);
	}

	public SoutenanceDAO getSoutenanceDAO() {
		return new SoutenanceDAO(this);
	}
	public PglSoutenanceDAO getPGLSoutenanceDAO() {
		return new PglSoutenanceDAO(this);
	}

	public PartageRoleDAO getPartageDAO() {
		return new PartageRoleDAO(this);
	}

	public NoteInteretTechnoDAO getNoteInteretTechnoDAO() {
		return new NoteInteretTechnoDAO(this);
	}

	public NoteInteretSujetDAO getNoteInteretSujetDAO() {
		return new NoteInteretSujetDAO(this);
	}

	public AnneeScolaireDAO getAnneeScolaireDAO() {
		return new AnneeScolaireDAO(this);
	}

	public NoteSoutenanceDAO getNoteSoutenanceDAO() {
		return new NoteSoutenanceDAO(this);
	}

	public ProfesseurSujetDAO getProfesseurSujetDAO() {
		return new ProfesseurSujetDAO(this);
	}

	public MeetingDAO getMeetingDAO() {
		return new MeetingDAO(this);
	}
	
	public PglNoteDAO getPglNoteDAO() {
		return new PglNoteDAO(this);
	}
	
	public PglEquipeDAO getPglEquipeDAO() {
		return new PglEquipeDAO(this);
	}
	
	public MatiereDAO getMatiereDAO() {
		return new MatiereDAO(this);
	}

	public InviteReunionDAO getInviteReunionDAO() { return new InviteReunionDAO(this); }

	public ModeleExcelDAO getModeleExcelDAO() { return new ModeleExcelDAO(this); }

	public SprintDAO getSprintDAO() { return new SprintDAO(this); }

	public PglEtudiantDAO getPglEtudiantDAO(){ return new PglEtudiantDAO(this); }

	public PglEtudiantEquipeDAO getPglEtudiantEquipeDAO(){ return new PglEtudiantEquipeDAO(this); }

	public PglProjetDAO getPglProjetDAO() { return new PglProjetDAO(this); }

	public PglRoleEquipeDAO getPglRoleEquipeDAO() { return new PglRoleEquipeDAO(this); }
	
	public DemandeJuryDAO getDemandeJuryDAO() {
		return new DemandeJuryDAO(this);
	}

}