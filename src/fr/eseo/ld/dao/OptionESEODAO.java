package fr.eseo.ld.dao;

import fr.eseo.ld.beans.OptionESEO;
import fr.eseo.ld.beans.Sujet;
import fr.eseo.ld.beans.Utilisateur;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static fr.eseo.ld.dao.DAOUtilitaire.fermetures;
import static fr.eseo.ld.dao.DAOUtilitaire.initialisationRequetePreparee;

/**
 * Classe du DAO du bean OptionESEO.
 * 
 * <p>
 * Cette classe implémente toutes les méthodes CRUD définies dans sa classe
 * mère.
 * </p>
 * 
 * @author Thomas MENARD
 */
public class OptionESEODAO extends DAO<OptionESEO> {

	private static final String NOM_ENTITE = "OptionESEO";
	protected static final String ATTRIBUT_ID_OPTION = "idOption";
	private static final String ATTRIBUT_NOM_OPTION = "nomOption";

	// tous les attributs du bean doivent être écrits dans la requête INSERT
	private static final String SQL_INSERT = "INSERT INTO `OptionESEO`(`idOption`, `nomOption`) VALUES (?, ?)";
	private static final String SQL_SELECT_TOUT = "SELECT * FROM OptionESEO";
	private static final String SQL_SELECT_OPTIONS_SUJET = "SELECT opt.* FROM OptionESEO opt , OptionSujets optSujet, Sujet su WHERE su.idSujet=optSujet.idSujet and opt.idOption=optSujet.idOption and su.idSujet=?;";
	private static final String SQL_SELECT_OPTIONS_UTILISATEUR = "SELECT opt.* FROM OptionESEO opt , RoleUtilisateur roleUtilisateur, Utilisateur utilisateur WHERE utilisateur.idUtilisateur=roleUtilisateur.idUtilisateur and roleUtilisateur.idOption=opt.idOption and utilisateur.idUtilisateur=?;";

	private static final String SQL_SELECT_OPTION_ROLEUTILISATEUR = "SELECT OptionESEO.* FROM RoleUtilisateur, OptionESEO WHERE RoleUtilisateur.idUtilisateur=? AND RoleUtilisateur.idRole=? and OptionESEO.idOption=RoleUtilisateur.idOption;";

	private static Logger logger = Logger.getLogger(OptionESEODAO.class.getName());

	/**
	 * Constructeur de DAO.
	 * 
	 * @param daoFactory
	 *            la Factory permettant la création d'une connexion à la BDD.
	 */
	OptionESEODAO(DAOFactory daoFactory) {
		super(daoFactory);
	}

	/**
	 * Insère une OptionESEO dans la BDD à partir des attributs spécifiés dans un
	 * bean OptionESEO.
	 * 
	 * @param option
	 *            l'OptionESEO que l'on souhaite insérer dans la BDD à partir du
	 *            bean OptionESEO.
	 */
	@Override
	public void creer(OptionESEO option) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			// les "?" de la requête sont comblés par les attributs de l'objet en paramètre
			// ceux-ci peuvent être nuls à condition que la BDD accepte les données de type
			// "null"
			preparedStatement = initialisationRequetePreparee(connection, SQL_INSERT, true, option.getIdOption(),
					option.getNomOption());
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec de la création de l'objet, aucune ligne ajoutée dans la table.", e);
		} finally {
			// fermeture des ressources utilisées
			fermetures(preparedStatement, connection);
		}
	}

	/**
	 * Liste toutes les OptionESEO ayant pour attributs les mêmes que ceux spécifiés
	 * dans un bean OptionESEO.
	 * 
	 * @param option
	 *            l'OptionESEO que l'on souhaite trouver dans la BDD.
	 * @return options la liste des OptionESEO trouvés dans la BDD.
	 */
	@Override
	public List<OptionESEO> trouver(OptionESEO option) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<OptionESEO> options = new ArrayList<>();
		// tableau de Strings regroupant tous les noms des attributs d'un objet ainsi
		// que les valeurs correspondantes
		String[][] attributs = { { ATTRIBUT_ID_OPTION, ATTRIBUT_NOM_OPTION },
				{ String.valueOf(option.getIdOption()), option.getNomOption() } };
		try {
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			// mise en forme de la requête SELECT en fonction des attributs de l'objet
			preparedStatement = initialisationRequetePreparee(connection, "SELECT", NOM_ENTITE, attributs, false);
			resultSet = preparedStatement.executeQuery();
			// récupération des valeurs des attributs de la BDD pour les mettre dans une
			// liste
			while (resultSet.next()) {
				options.add(map(resultSet));
			}
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec de la recherche de l'objet.", e);
		} finally {
			// fermeture des ressources utilisées
			fermetures(resultSet, preparedStatement, connection);
		}
		return options;
	}

	/**
	 * Modifie UNE OptionESEO ayant pour attributs les mêmes que ceux spécifiés dans
	 * un bean OptionESEO et la même clé primaire. Cette clé primaire ne peut être
	 * modifiée.
	 * 
	 * @param option
	 *            l'OptionESEO que l'on souhaite modifier dans la BDD.
	 */
	@Override
	public void modifier(OptionESEO option) {
		/**
		 * Méthode modifier() nécessaire pour la bonne création de DAO mais n'a aucune
		 * action car on ne peut modifier une option.
		 */
	}

	/**
	 * Supprime toutes les OptionESEO ayant pour attributs les mêmes que ceux
	 * spécifiés dans un bean OptionESEO.
	 * 
	 * @param option
	 *            l'OptionESEO que l'on souhaite supprimer dans la BDD.
	 */
	@Override
	public void supprimer(OptionESEO option) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		// tableau de Strings regroupant tous les noms des attributs d'un objet ainsi
		// que les valeurs correspondantes
		String[][] attributs = { { ATTRIBUT_ID_OPTION, ATTRIBUT_NOM_OPTION },
				{ String.valueOf(option.getIdOption()), option.getNomOption() } };
		try {
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			// mise en forme de la requête DELETE en fonction des attributs de l'objet
			preparedStatement = DAOUtilitaire.initialisationRequetePreparee(connection, "DELETE", NOM_ENTITE, attributs,
					false);
			int statut = preparedStatement.executeUpdate();
			if (statut == 0) {
				throw new SQLException();
			} else {
				// suppression du bean utilisateur
				option.setIdOption(null);
			}
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec de la suppression de l'objet, aucune ligne supprimée de la table.", e);
		} finally {
			// fermeture des ressources utilisées
			fermetures(preparedStatement, connection);
		}
	}

	/**
	 * Liste toutes les OptionESEO présentes dans la BDD.
	 * 
	 * @return options la liste des OptionESEO présentes dans la BDD.
	 */
	@Override
	public List<OptionESEO> lister() {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<OptionESEO> options = new ArrayList<>();
		try {
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			preparedStatement = connection.prepareStatement(SQL_SELECT_TOUT);
			resultSet = preparedStatement.executeQuery();
			// récupération des valeurs des attributs de la BDD pour les mettre dans une
			// liste
			while (resultSet.next()) {
				options.add(map(resultSet));
			}
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec du listage des objets.", e);
		} finally {
			fermetures(resultSet, preparedStatement, connection);
		}
		return options;
	}

	/**
	 * Liste toutes les options attribuées à un sujet.
	 * 
	 * @param sujet
	 *            le sujet concerné.
	 * @return options la liste des options attribuées au sujet concerné.
	 */
	public List<OptionESEO> trouverOptionSujet(Sujet sujet) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<OptionESEO> options = new ArrayList<>();
		try {
			// vérification que le sujet rentré n'est pas nul
			if (sujet.getIdSujet() == null) {
				throw new SQLException();
			}
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			// mise en forme de la requête SELECT en fonction des attributs de l'objet
			preparedStatement = initialisationRequetePreparee(connection, SQL_SELECT_OPTIONS_SUJET, false,
					sujet.getIdSujet());
			resultSet = preparedStatement.executeQuery();
			// récupération des valeurs des attributs de la BDD pour les mettre dans une
			// liste
			while (resultSet.next()) {
				options.add(map(resultSet));
			}
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec de la recherche des options attribuées à un sujet.", e);
		} finally {
			// fermeture des ressources utilisées
			fermetures(resultSet, preparedStatement, connection);
		}
		return options;
	}

	/**
	 * Liste toutes les options attribuées à un utilisateur.
	 * 
	 * @param utilisateur
	 *            l'utilisateur concerné.
	 * @return options la liste des options attribuées à l'utilisateur concerné.
	 */
	public List<OptionESEO> trouverOptionUtilisateur(Utilisateur utilisateur) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<OptionESEO> options = new ArrayList<>();
		try {
			// vérification que l'utilisateur rentré n'est pas nul
			if (utilisateur.getIdUtilisateur() == null) {
				throw new SQLException();
			}
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			// mise en forme de la requête SELECT en fonction des attributs de l'objet
			preparedStatement = initialisationRequetePreparee(connection, SQL_SELECT_OPTIONS_UTILISATEUR, false,
					utilisateur.getIdUtilisateur());
			resultSet = preparedStatement.executeQuery();
			// récupération des valeurs des attributs de la BDD pour les mettre dans une
			// liste
			while (resultSet.next()) {
				options.add(map(resultSet));
			}
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec de la recherche des options attribuées à un utilisateur.", e);
		} finally {
			// fermeture des ressources utilisées
			fermetures(resultSet, preparedStatement, connection);
		}
		return options;
	}

	/**
	 * Liste toutes les options attribuées à un utilisateur pour un de ses roles.
	 * 
	 * @param idUtilisateur
	 *            l'id de l'utilisateur.
	 * @param idRole
	 *            l'id d'un role de l'utilisateur.
	 * @return options la liste des options attribuées à l'utilisateur pour un role
	 *         donné.
	 */
	public List<OptionESEO> trouverOptionRoleUtilisateur(Long idUtilisateur, Long idRole) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<OptionESEO> options = new ArrayList<>();
		try {
			// vérification que les paramètres ne sont pas nuls
			if (idUtilisateur == null || idRole == null) {
				throw new SQLException();
			}
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			// mise en forme de la requête SELECT en fonction des attributs de l'objet
			preparedStatement = initialisationRequetePreparee(connection, SQL_SELECT_OPTION_ROLEUTILISATEUR, false,
					idUtilisateur, idRole);
			resultSet = preparedStatement.executeQuery();
			// récupération des valeurs des attributs de la BDD pour les mettre dans une
			// liste
			while (resultSet.next()) {
				options.add(map(resultSet));
			}
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec de la recherche des options attribuées au role d'un utilisateur.", e);
		} finally {
			// fermeture des ressources utilisées
			fermetures(resultSet, preparedStatement, connection);
		}
		return options;
	}

	/**
	 * Fait la correspondance (le mapping) entre une ligne issue de la table
	 * OptionESEO (un ResultSet) et un bean OptionESEO.
	 * 
	 * @param resultSet
	 *            la ligne issue de la table OptionESEO.
	 * @return optionESEO le bean dont on souhaite faire la correspondance.
	 * @throws SQLException
	 */
	public static OptionESEO map(ResultSet resultSet) throws SQLException {
		OptionESEO optionESEO = new OptionESEO();
		optionESEO.setIdOption(resultSet.getLong(ATTRIBUT_ID_OPTION));
		optionESEO.setNomOption(resultSet.getString(ATTRIBUT_NOM_OPTION));
		return optionESEO;
	}

}