package fr.eseo.ld.dao;

import fr.eseo.ld.beans.*;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static fr.eseo.ld.dao.DAOUtilitaire.*;

/**
 * <h1>Sujet DAO</h1>
 * <p>
 * Classe du DAO du bean Sujet.
 * </p>
 * <h2>Méthodes incluses :</h2>
 * <ul style="list-style-type:circle">
 * <li>void creer(Sujet sujet)</li>
 * <li>List(Sujet) trouver(Sujet sujet)</li>
 * <li>Sujet trouver(Long idSujet)</li>
 * <li>void modifier(Sujet sujet)</li>
 * <li>void supprimer(Sujet sujet)</li>
 * <li>List(Sujet) lister()</li>
 * <li>void creerPorteurSujet(Utilisateur utilisateur, Sujet sujet)</li>
 * <li>List(Sujet) trouverSujetsPortes(Utilisateur utilisateur)</li>
 * <li>void supprimerPorteurSujet(Sujet sujet, Utilisateur utilisateur)</li>
 * <li>void creerProfesseurSujet(Professeur professeur, Sujet sujet, String
 * fonction, String valide)</li>
 * <li>List(Sujet) trouverSujetsProfesseur(Professeur professeur)</li>
 * <li>void supprimerProfesseurSujet(Sujet sujet, Professeur professeur)
 * <li>
 * <li>List(Sujet) listerSujetsSansReferent()
 * <li>
 * <li>void attribuerOptionsSujet(Sujet sujet, List(OptionESEO) options)</li>
 * <li>List(Sujet) trouverSujetOption(OptionESEO optionESEO)</li>
 * <li>void supprimerOptionSujet(Sujet sujet, OptionESEO option)</li>
 * <li>boolean possedePoster(Sujet sujet)</li>
 * <li>List(Sujet) trouverSujetsPostersValidesOptions(List(OptionESEO) options)
 * <li>
 * <li>List(Sujet) trouverSujetsJurySoutenance(Utilisateur utilisateur)</li>
 * <li>List(Sujet) trouverSujetsJuryPoster(Utilisateur utilisateur)</li>
 * <li>Sujet map(ResultSet resultSet)</li>
 * </ul>
 *
 * @author Thomas MENARD et Maxime LENORMAND
 */
public class SujetDAO extends DAO<Sujet> {

	/* Noms des entités */
	private static final String NOM_ENTITE = "Sujet";
	private static final String NOM_ENTITE_PORTEUR_SUJET = "PorteurSujet";
	private static final String NOM_ENTITE_OPTION_SUJET = "OptionSujets";
	private static final String NOM_ENTITE_PROFESSEUR_SUJET = "ProfesseurSujet";

	/* Attributs de l'entité Sujet */
	private static final String ATTRIBUT_ID_SUJET = "idSujet";
	private static final String ATTRIBUT_ID_FORM = "idForm";
	private static final String ATTRIBUT_TITRE = "titre";
	private static final String ATTRIBUT_DESCRIPTION = "description";
	private static final String ATTRIBUT_MIN_ELEVE = "nbrMinEleves";
	private static final String ATTRIBUT_MAX_ELEVE = "nbrMaxEleves";
	private static final String ATTRIBUT_CONTRAT_PRO = "contratPro";
	private static final String ATTRIBUT_CONFIDENTIALITE = "confidentialite";
	private static final String ATTRIBUT_ETAT_SUJET = "etat";
	private static final String ATTRIBUT_LIENS = "liens";
	private static final String ATTRIBUT_INTERETS = "interets";
	private static final String ATTRIBUT_NOTE_INTERET = "noteInteretTechno";
	private static final String ATTRIBUT_NOTE_INTERET_SUJET = "noteInteretSujet";
	private static final String[] ATTRIBUTS_NOMS = { ATTRIBUT_ID_SUJET, ATTRIBUT_TITRE, ATTRIBUT_DESCRIPTION,
			ATTRIBUT_MIN_ELEVE, ATTRIBUT_MAX_ELEVE, ATTRIBUT_CONTRAT_PRO, ATTRIBUT_CONFIDENTIALITE, ATTRIBUT_ETAT_SUJET,
			ATTRIBUT_LIENS, ATTRIBUT_INTERETS, ATTRIBUT_NOTE_INTERET, ATTRIBUT_NOTE_INTERET_SUJET };
	private static final String[] ATTRIBUTS_NOMS_ID_FIN = { ATTRIBUT_TITRE, ATTRIBUT_DESCRIPTION, ATTRIBUT_MIN_ELEVE,
			ATTRIBUT_MAX_ELEVE, ATTRIBUT_CONTRAT_PRO, ATTRIBUT_CONFIDENTIALITE, ATTRIBUT_ETAT_SUJET, ATTRIBUT_LIENS,
			ATTRIBUT_INTERETS, ATTRIBUT_NOTE_INTERET, ATTRIBUT_NOTE_INTERET_SUJET, ATTRIBUT_ID_FORM,
			ATTRIBUT_ID_SUJET };

	/* Requetes SQL Sujet */
	private static final String SQL_INSERT_SUJET = "INSERT INTO `Sujet`(`titre`, `description`, `nbrMinEleves`, `nbrMaxEleves`, `contratPro`, `confidentialite`, `etat`, `liens`, `interets`, `noteInteretTechno`,`noteInteretSujet`,`idForm`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";
	private static final String SQL_SELECT_TOUT = "SELECT * FROM Sujet";

	/* Requetes SQL OptionSujets */
	private static final String SQL_INSERT_OPTION = "INSERT INTO `OptionSujets`(`idOption`,`idSujet`) VALUES (?,?)";
	private static final String SQL_SELECT_SUJETS_OPTION = "SELECT su.* FROM Sujet su, OptionSujets os WHERE os.idSujet=su.idSujet and os.idOption=?";
	private static final String SQL_SELECT_SUJETS_JURY_MANQUANT = "SELECT DISTINCT su.* FROM Sujet su, Equipe eq, Soutenance sou, jurysoutenance ju WHERE eq.idSujet = su.idSujet AND sou.idEquipe = eq.idEquipe AND sou.idJurySoutenance IS NULL OR eq.idSujet = su.idSujet AND sou.idEquipe = eq.idEquipe AND sou.idJurySoutenance = ju.idJurySoutenance AND( ju.idProf1 IS NULL OR ju.idProf2 IS NULL )";

	/* Requetes SQL PorteurSujet */
	private static final String SQL_INSERT_PORTEUR_SUJET = "INSERT INTO `PorteurSujet`(`idSujet`, `idUtilisateur`, `annee`) VALUES (?,?,?)";
	private static final String SQL_SELECT_SUJET = "SELECT su.* FROM Sujet su, PorteurSujet ps WHERE ps.idSujet=su.idSujet and ps.idUtilisateur=?";
	private static final String ATTRIBUT_ID_UTILISATEUR = "idUtilisateur";

	/* Requetes SQL ProfesseurSujet */
	private static final String SQL_INSERT_PROFESSEUR_SUJET = "INSERT INTO `ProfesseurSujet`(`idProfesseur`, `idSujet`, `fonction`, `valide`) VALUES (?,?,?,?)";
	private static final String SQL_SELECT_SUJETS_PROFESSEUR = "SELECT su.* FROM Sujet su, ProfesseurSujet ps WHERE ps.idSujet=su.idSujet and ps.idProfesseur=?";
	private static final String SQL_SELECT_SUJETS_PROFESSEUR_REFERENT = "SELECT su.* FROM Sujet su, ProfesseurSujet ps WHERE ps.idSujet=su.idSujet and ps.fonction='référent' and ps.idProfesseur=? ";
	private static final String SQL_SELECT_SUJETS_PROFESSEUR_REFERENT_NON_VALIDE = "SELECT su.* FROM Sujet su, ProfesseurSujet ps WHERE ps.valide='non' AND ps.idSujet=su.idSujet and ps.fonction='référent' and ps.idProfesseur=?";
	private static final String SQL_SELECT_SUJETS_SANS_REFERENT = "SELECT DISTINCT su.* FROM Sujet su, ProfesseurSujet ps WHERE su.idSujet NOT iN (SELECT DISTINCT su.idSujet FROM Sujet su, ProfesseurSujet ps WHERE ps.fonction ='référent' AND (ps.valide='oui' OR ps.valide='non') AND ps.idSujet = su.idSujet)  AND (su.etat='publie' OR su.etat='attribue')";

	/* Requetes SQL Poster */
	private static final String SQL_SELECT_IDPOSTER = "SELECT idPoster FROM Poster WHERE Poster.idSujet =?";

	/* Requetes SQL JuryPoster */
	private static final String SQL_SELECT_SUJETS_ASSOCIES_JURY_POSTER = "SELECT su.* FROM Equipe eq, Sujet su, JuryPoster jp WHERE jp.idEquipe=eq.idEquipe and eq.idSujet=su.idSujet and ? in(jp.idProf1,jp.idProf2,jp.idProf3)";

	/* Requetes SQL JurySoutenance */
	private static final String SQL_SELECT_SUJETS_ASSOCIES_JURY_SOUTENANCE = "SELECT su.* FROM Equipe eq, Sujet su, JurySoutenance js, Soutenance so WHERE js.idJurySoutenance=so.idJurySoutenance and so.idEquipe=eq.idEquipe and eq.idSujet=su.idSujet and ? in(js.idProf1,js.idProf2)";

	/* Requetes SQL pour trouver un sujet à partir de son ID */
	private static final String SQL_SELECT_SUJET_A_PARTIR_ID = "SELECT * FROM Sujet WHERE idSujet= ? ";

	/* Requetes SQL pour trouver un sujet à partir de son IDFORM */
	private static final String SQL_SELECT_SUJET_A_PARTIR_IDFORM = "SELECT * FROM Sujet WHERE idForm= ? ";

	/*
	 * Requetes SQL pour trouver les sujets dont les posters sont validés et
	 * possèdent une option particulière
	 */
	private static final String SQL_SELECT_SUJET_OPTION_VALIDES = "SELECT * FROM Sujet, Poster , OptionESEO WHERE Sujet.idSujet=Poster.idSujet AND Poster.valide='oui' AND OptionESEO.idOption= ? ";

	/* Constantes pour éviter la duplication de code */
	private static final String DELETE = "DELETE";

	/* Logger */
	private static Logger logger = Logger.getLogger(SujetDAO.class.getName());

	/**
	 * Constructeur de DAO.
	 *
	 * @param daoFactory
	 *            la Factory permettant la création d'une connexion à la BDD.
	 */
	public SujetDAO(DAOFactory daoFactory) {
		super(daoFactory);
	}

	// ########################################################################################################
	// # Méthodes CRUD concernant le bean Sujet #
	// ########################################################################################################

	/**
	 * Insère un Sujet dans la BDD à partir des attributs spécifiés dans un bean
	 * Sujet.
	 *
	 * @param sujet
	 *            le Sujet que l'on souhaite insérer dans la BDD à partir du bean
	 *            Sujet.
	 */
	@Override
	public void creer(Sujet sujet) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			// les "?" de la requête sont comblés par les attributs de l'objet en paramètre
			// ceux-ci peuvent être nuls à condition que la BDD accepte les données de type
			// "null"
			preparedStatement = initialisationRequetePreparee(connection, SQL_INSERT_SUJET, true, sujet.getTitre(),
					sujet.getDescription(), sujet.getNbrMinEleves(), sujet.getNbrMaxEleves(), sujet.getContratPro(),
					sujet.getConfidentialite(), String.valueOf(sujet.getEtat()), sujet.getLiens(), sujet.getInterets(),
					sujet.getNoteInteretTechno(), sujet.getNoteInteretSujet(), sujet.getIdForm());
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec de la création de l'objet, aucune ligne ajoutée dans la table.", e);
		} finally {
			// fermeture des ressources utilisées
			fermetures(preparedStatement, connection);
		}
	}

	/**
	 * Liste tous les Sujets ayant pour attributs les mêmes que ceux spécifiés dans
	 * un bean Sujet.
	 *
	 * @param sujet
	 *            le Sujet que l'on souhaite trouver dans la BDD.
	 * @return sujet la liste des Sujets trouvés dans la BDD.
	 */
	@Override
	public List<Sujet> trouver(Sujet sujet) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<Sujet> sujets = new ArrayList<>();
		// tableau de Strings regroupant tous les noms des attributs d'un objet ainsi
		// que les valeurs correspondantes
		String[][] attributs = { ATTRIBUTS_NOMS,
				{ String.valueOf(sujet.getIdSujet()), sujet.getTitre(), sujet.getDescription(),
						String.valueOf(sujet.getNbrMinEleves()), String.valueOf(sujet.getNbrMaxEleves()),
						String.valueOf(sujet.getContratProInt()), String.valueOf(sujet.getConfidentialiteInt()),
						String.valueOf(sujet.getEtat()), sujet.getLiens(), sujet.getInterets(),
						String.valueOf(sujet.getNoteInteretTechno()), String.valueOf(sujet.getNoteInteretSujet()) } };
		try {
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			// mise en forme de la requête SELECT en fonction des attributs de l'objet sujet
			preparedStatement = initialisationRequetePreparee(connection, "SELECT", NOM_ENTITE, attributs, false);
			resultSet = preparedStatement.executeQuery();
			// récupération des valeurs des attributs de la BDD pour les mettre dans une
			// liste
			while (resultSet.next()) {
				sujets.add(map(resultSet));
			}
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec de la recherche de l'objet.", e);
		} finally {
			// fermeture des ressources utilisées
			fermetures(resultSet, preparedStatement, connection);
		}
		return sujets;
	}

	/**
	 * Renvoie un Sujet à partir de son ID.
	 *
	 * @param idSujet
	 *            l'ID du Sujet que l'on souhaite trouver dans la BDD.
	 * @return sujet le sujet trouvé dans la BDD.
	 */
	public Sujet trouver(Long idSujet) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		Sujet sujet = null;
		try {
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			preparedStatement = initialisationRequetePreparee(connection, SQL_SELECT_SUJET_A_PARTIR_ID, true,
					String.valueOf(idSujet));
			resultSet = preparedStatement.executeQuery();
			resultSet.next();
			sujet = map(resultSet);
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec de la recherche d'un sujet à partir de son ID.", e);
		} finally {
			// fermeture des ressources utilisées
			fermetures(resultSet, preparedStatement, connection);
		}
		return sujet;
	}

	/**
	 * Renvoie un Sujet à partir de son IDFORM.
	 *
	 * @param idForm
	 *            l'IDFORM du Sujet que l'on souhaite trouver dans la BDD.
	 * @return sujet le sujet trouvé dans la BDD.
	 */
	public Sujet trouverFromForm(Integer idForm) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		Sujet sujet = null;
		try {
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			preparedStatement = initialisationRequetePreparee(connection, SQL_SELECT_SUJET_A_PARTIR_IDFORM, true,
					String.valueOf(idForm));
			resultSet = preparedStatement.executeQuery();
			resultSet.next();
			sujet = map(resultSet);
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec de la recherche d'un sujet à partir de son IDFORM.", e);
		} finally {
			// fermeture des ressources utilisées
			fermetures(resultSet, preparedStatement, connection);
		}
		return sujet;
	}

	/**
	 * Modifie UN Sujet ayant pour attributs les mêmes que ceux spécifiés dans un
	 * bean Sujet et la même clé primaire. Cette clé primaire ne peut être modifiée.
	 *
	 * @param sujet
	 *            le Sujet que l'on souhaite modifier dans la BDD.
	 */
	@Override
	public void modifier(Sujet sujet) {
		// tableau de Strings regroupant tous les noms des attributs d'un objet ainsi
		// que les valeurs correspondantes
		// /!\ bien placer la clé primaire et sa valeur à la FIN du tableau sinon erreur
		String[][] attributs = { ATTRIBUTS_NOMS_ID_FIN,
				{ sujet.getTitre(), sujet.getDescription(), String.valueOf(sujet.getNbrMinEleves()),
						String.valueOf(sujet.getNbrMaxEleves()), String.valueOf(sujet.getContratProInt()),
						String.valueOf(sujet.getConfidentialiteInt()), String.valueOf(sujet.getEtat()),
						sujet.getLiens(), sujet.getInterets(), String.valueOf(sujet.getNoteInteretTechno()),
						String.valueOf(sujet.getNoteInteretSujet()), String.valueOf(sujet.getIdForm()),
						String.valueOf(sujet.getIdSujet()) } };

		/* Traite la mise à jour de la BDD */
		traitementUpdate(this.getDaoFactory(), NOM_ENTITE, attributs, logger);
	}

	/**
	 * Supprime tous les Sujets ayant pour attributs les mêmes que ceux spécifiés
	 * dans un bean Sujet.
	 *
	 * @param sujet
	 *            le Sujet que l'on souhaite supprimer dans la BDD.
	 */
	@Override
	public void supprimer(Sujet sujet) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		// tableau de Strings regroupant tous les noms des attributs d'un objet ainsi
		// que les valeurs correspondantes
		String[][] attributs = { ATTRIBUTS_NOMS,
				{ String.valueOf(sujet.getIdSujet()), sujet.getTitre(), sujet.getDescription(),
						String.valueOf(sujet.getNbrMinEleves()), String.valueOf(sujet.getNbrMaxEleves()),
						String.valueOf(sujet.getContratProInt()), String.valueOf(sujet.getConfidentialiteInt()),
						String.valueOf(sujet.getEtat()), sujet.getLiens(), sujet.getInterets(),
						String.valueOf(sujet.getNoteInteretTechno()), String.valueOf(sujet.getNoteInteretSujet()), } };
		try {
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			// mise en forme de la requête DELETE en fonction des attributs de l'objet
			// utilisateur
			preparedStatement = initialisationRequetePreparee(connection, DELETE, NOM_ENTITE, attributs, false);
			int statut = preparedStatement.executeUpdate();
			if (statut == 0) {
				throw new SQLException();
			} else {
				// suppression du bean
				sujet.setIdSujet(null);
			}
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec de la suppression de l'objet, aucune ligne supprimée de la table.", e);
		} finally {
			// fermeture des ressources utilisées
			fermetures(preparedStatement, connection);
		}
	}

	/**
	 * Liste tous les Sujets présents dans la BDD.
	 *
	 * @return sujets la liste des Sujets présents dans la BDD.
	 */
	@Override
	public List<Sujet> lister() {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<Sujet> sujets = new ArrayList<>();
		try {
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			preparedStatement = connection.prepareStatement(SQL_SELECT_TOUT);
			resultSet = preparedStatement.executeQuery();
			// récupération des valeurs des attributs de la BDD pour les mettre dans une
			// liste
			while (resultSet.next()) {
				sujets.add(map(resultSet));
			}
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec du listage des objets.", e);
		} finally {
			fermetures(resultSet, preparedStatement, connection);
		}
		return sujets;
	}

	// ########################################################################################################
	// # Méthodes liant Sujet et Utilisateur à travers PorteurSujet #
	// ########################################################################################################

	/**
	 * Attribue un Sujet à un Utilisateur. L'attribution se fait uniquement via les
	 * ID de ces deux objets.
	 *
	 * @param utilisateur
	 *            l'utilisateur à qui il faut attribuer le sujet.
	 * @param sujet
	 *            le sujet à qui il faut attribuer l'utilisateur.
	 */
	public void creerPorteurSujet(Utilisateur utilisateur, Sujet sujet) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		/*
		 * On prends une annee de début, ici celle des LD car ce seront toutes les mêmes
		 * anneeDebut
		 */
		AnneeScolaireDAO anneeScolaireDAO = this.getDaoFactory().getAnneeScolaireDAO();
		AnneeScolaire anneeScolaire = new AnneeScolaire();
		anneeScolaire.setIdOption((long) 1);
		int annee = anneeScolaireDAO.trouver(anneeScolaire).get(0).getAnneeDebut();
		try {
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			// les "?" de la requête sont comblés par les attributs de l'objet en paramètre
			preparedStatement = initialisationRequetePreparee(connection, SQL_INSERT_PORTEUR_SUJET, true,
					sujet.getIdSujet(), utilisateur.getIdUtilisateur(), annee);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec de la création de porteurSujet, aucune ligne ajoutée dans la table.", e);
		} finally {
			// fermeture des ressources utilisées
			fermetures(preparedStatement, connection);
		}
	}

	/**
	 * Liste les Sujets portés par un Utilisateur.
	 *
	 * @param utilisateur
	 *            l'utilisateur concerné.
	 * @return une liste des sujets à qui sont attribués l'utilisateur.
	 */
	public List<Sujet> trouverSujetsPortes(Utilisateur utilisateur) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<Sujet> sujets = new ArrayList<>();
		try {
			// vérification que l'utilisateur rentré n'est pas nul
			if (utilisateur.getIdUtilisateur() == null) {
				throw new SQLException();
			}
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			// mise en forme de la requête SELECT en fonction des attributs de l'objet
			preparedStatement = connection.prepareStatement(SQL_SELECT_SUJET, Statement.NO_GENERATED_KEYS);
			preparedStatement.setLong(1, utilisateur.getIdUtilisateur());
			resultSet = preparedStatement.executeQuery();
			// récupération des valeurs des attributs de la BDD pour les mettre dans une
			// liste
			while (resultSet.next()) {
				sujets.add(map(resultSet));
			}
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec de la recherche des sujets portés par un utilisateur.", e);
		} finally {
			// fermeture des ressources utilisées
			fermetures(resultSet, preparedStatement, connection);
		}
		return sujets;
	}

	/**
	 * Supprime la liaison entre un Sujet et un Utilisateur. La suppression se fait
	 * uniquement via les ID de ces deux objets.
	 *
	 * @param utilisateur
	 *            l'utilisateur que l'on souhaite dissocier du sujet.
	 * @param sujet
	 *            le sujet que l'on souhaite dissocier de l'utilisateur.
	 */
	public void supprimerPorteurSujet(Sujet sujet, Utilisateur utilisateur) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		// tableau de Strings regroupant tous les noms des attributs d'un objet ainsi
		// que les valeurs correspondantes
		String[][] attributs = { { ATTRIBUT_ID_SUJET, ATTRIBUT_ID_UTILISATEUR },
				{ String.valueOf(sujet.getIdSujet()), String.valueOf(utilisateur.getIdUtilisateur()) } };
		try {
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			preparedStatement = initialisationRequetePreparee(connection, DELETE, NOM_ENTITE_PORTEUR_SUJET, attributs,
					false);
			int statut = preparedStatement.executeUpdate();
			if (statut == 0) {
				throw new SQLException();
			}
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec de la suppression de porteurSujet, aucune ligne supprimée de la table.", e);
		} finally {
			// fermeture des ressources utilisées
			fermetures(preparedStatement, connection);
		}
	}

	// ########################################################################################################
	// # Méthodes liant Professeur et Sujet à travers ProfesseurSujet #
	// ########################################################################################################

	/**
	 * Attribue un Sujet à un Professeur. L'attribution se fait uniquement via les
	 * ID de ces deux objets.
	 *
	 * @param professeur
	 *            le professeur à qui il faut attribuer le sujet.
	 * @param sujet
	 *            le sujet à qui il faut attribuer le professeur.
	 * @param fonction
	 *            la fonction du professeur sur ce sujet.
	 * @param valide
	 *            la validation ou non du sujet par le professeur.
	 */
	public void creerProfesseurSujet(Professeur professeur, Sujet sujet, String fonction, String valide) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			// les "?" de la requête sont comblés par les attributs de l'objet en paramètre
			preparedStatement = initialisationRequetePreparee(connection, SQL_INSERT_PROFESSEUR_SUJET, true,
					professeur.getIdProfesseur(), sujet.getIdSujet(), fonction, valide);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec de la création de professeurSujet, aucune ligne ajoutée dans la table.", e);
		} finally {
			// fermeture des ressources utilisées
			fermetures(preparedStatement, connection);
		}
	}

	/**
	 * Liste les Sujets attribués à un Professeur.
	 *
	 * @param professeur
	 *            le professeur associé au sujet.
	 */
	public List<Sujet> trouverSujetsProfesseur(Professeur professeur) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<Sujet> sujets = new ArrayList<>();
		try {
			// vérification que l'utilisateur rentré n'est pas nul
			if (professeur.getIdProfesseur() == null) {
				throw new SQLException();
			}
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			// mise en forme de la requête SELECT en fonction des attributs de l'objet
			preparedStatement = connection.prepareStatement(SQL_SELECT_SUJETS_PROFESSEUR, Statement.NO_GENERATED_KEYS);
			preparedStatement.setLong(1, professeur.getIdProfesseur());
			resultSet = preparedStatement.executeQuery();
			// récupération des valeurs des attributs de la BDD pour les mettre dans une
			// liste
			while (resultSet.next()) {
				sujets.add(map(resultSet));
			}
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec de la recherche des sujets attribués à un professeur.", e);
		} finally {
			// fermeture des ressources utilisées
			fermetures(resultSet, preparedStatement, connection);
		}
		return sujets;
	}

	/**
	 * Liste les Sujets pour lesquels le professeur est référent.
	 *
	 * @param professeur
	 *            le professeur associé au sujet.
	 */
	public List<Sujet> trouverSujetsProfesseurReferent(Professeur professeur) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<Sujet> sujets = new ArrayList<>();
		try {
			// vérification que l'utilisateur rentré n'est pas nul
			if (professeur.getIdProfesseur() == null) {
				throw new SQLException();
			}
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			// mise en forme de la requête SELECT en fonction des attributs de l'objet
			preparedStatement = connection.prepareStatement(SQL_SELECT_SUJETS_PROFESSEUR_REFERENT,
					Statement.NO_GENERATED_KEYS);
			preparedStatement.setLong(1, professeur.getIdProfesseur());
			resultSet = preparedStatement.executeQuery();
			// récupération des valeurs des attributs de la BDD pour les mettre dans une
			// liste
			while (resultSet.next()) {
				sujets.add(map(resultSet));
			}
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec de la recherche des sujets attribués à un professeur référent.", e);
		} finally {
			// fermeture des ressources utilisées
			fermetures(resultSet, preparedStatement, connection);
		}
		return sujets;
	}

	/**
	 * Liste les Sujets pour lesquels le professeur est référent mais non validé.
	 *
	 * @param professeur
	 *            le professeur associé au sujet.
	 */
	public List<Sujet> trouverSujetsProfesseurReferentNonValide(Professeur professeur) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<Sujet> sujets = new ArrayList<>();
		try {
			// vérification que l'utilisateur rentré n'est pas nul
			if (professeur.getIdProfesseur() == null) {
				throw new SQLException();
			}
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.getDaoFactory().getConnection();
			// mise en forme de la requête SELECT en fonction des attributs de l'objet
			preparedStatement = connection.prepareStatement(SQL_SELECT_SUJETS_PROFESSEUR_REFERENT_NON_VALIDE,
					Statement.NO_GENERATED_KEYS);
			preparedStatement.setLong(1, professeur.getIdProfesseur());
			resultSet = preparedStatement.executeQuery();
			// récupération des valeurs des attributs de la BDD pour les mettre dans une
			// liste
			while (resultSet.next()) {
				sujets.add(map(resultSet));
			}
		} catch (SQLException e) {
			logger.log(Level.FATAL,
					"Échec de la recherche des sujets attribués à un professeur référent qui n'est pas encore validé.",
					e);
		} finally {
			// fermeture des ressources utilisées
			fermetures(resultSet, preparedStatement, connection);
		}
		return sujets;
	}

	/**
	 * Supprime la liaison entre un Professeur et un Sujet. La suppression se fait
	 * uniquement via les ID de ces deux objets.
	 *
	 * @param sujet
	 *            le sujet que l'on souhaite dissocier du professeur.
	 * @param professeur
	 *            le professeur que l'on souhaite dissocier du sujet.
	 */
	public void supprimerProfesseurSujet(Sujet sujet, Professeur professeur) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		// tableau de Strings regroupant tous les noms des attributs d'un objet ainsi
		// que les valeurs correspondantes
		String[][] attributs = { { ATTRIBUT_ID_SUJET, ProfesseurDAO.ATTRIBUT_ID_PROFESSEUR },
				{ String.valueOf(sujet.getIdSujet()), String.valueOf(professeur.getIdProfesseur()) } };
		try {
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			preparedStatement = initialisationRequetePreparee(connection, DELETE, NOM_ENTITE_PROFESSEUR_SUJET,
					attributs, false);
			int statut = preparedStatement.executeUpdate();
			if (statut == 0) {
				throw new SQLException();
			}
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec de la suppression de professeurSujet, aucune ligne supprimée de la table.",
					e);
		} finally {
			// fermeture des ressources utilisées
			fermetures(preparedStatement, connection);
		}
	}

	/**
	 * Liste les Sujets qui ne possèdent pas de référent.
	 *
	 * @return sujetsSansReferent la liste des sujets sans référent.
	 */
	public List<Sujet> listerSujetsSansReferent() {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<Sujet> sujetsSansReferent = new ArrayList<>();
		try {
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			preparedStatement = connection.prepareStatement(SQL_SELECT_SUJETS_SANS_REFERENT,
					Statement.NO_GENERATED_KEYS);
			resultSet = preparedStatement.executeQuery();
			// récupération des valeurs des attributs de la BDD pour les mettre dans une
			// liste
			while (resultSet.next()) {
				sujetsSansReferent.add(map(resultSet));
			}
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec du listage des sujets sans référent.", e);
		} finally {
			// fermeture des ressources utilisées
			fermetures(resultSet, preparedStatement, connection);
		}
		return sujetsSansReferent;
	}

	// ########################################################################################################
	// # Méthodes liant OptionESEO et Sujet à travers OptionSujet #
	// ########################################################################################################

	/**
	 * Attribue des Options à un Sujet. L'attribution se fait uniquement via les ID
	 * de ces deux objets.
	 *
	 * @param options
	 *            les options à attribuer.
	 * @param sujet
	 *            le sujet à qui il faut attribuer les options.
	 */
	public void attribuerOptionsSujet(Sujet sujet, List<OptionESEO> options) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			// création de la requete
			StringBuilder requeteBuilder = new StringBuilder();
			requeteBuilder.append(SQL_INSERT_OPTION);
			for (int i = 1; i < options.size(); i++) {
				requeteBuilder.append(",(?, ?)");
			}

			// les "?" de la requête sont comblés par les attributs de l'objet en paramètre
			preparedStatement = connection.prepareStatement(requeteBuilder.toString(), Statement.NO_GENERATED_KEYS);
			for (int i = 0; i < options.size(); i++) {
				preparedStatement.setObject((2 * i) + 1, options.get(i).getIdOption());
				preparedStatement.setObject((2 * i) + 2, sujet.getIdSujet());
			}
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec de la création de optionSujet, aucune ligne ajoutée dans la table.", e);
		} finally {
			// fermeture des ressources utilisées
			fermetures(preparedStatement, connection);
		}
	}

	/**
	 * Liste tous les Sujets possedant une Option donnée.
	 *
	 * @param optionESEO
	 *            l'option donnée.
	 * @return sujets la liste de tous les Sujets possedant optionESEO.
	 */
	public List<Sujet> trouverSujetOption(OptionESEO optionESEO) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<Sujet> sujets = new ArrayList<>();
		try {
			// vérification que l'utilisateur rentré n'est pas nul
			if (optionESEO.getIdOption() == null) {
				throw new SQLException();
			}
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			// mise en forme de la requête SELECT en fonction des attributs de l'objet
			preparedStatement = connection.prepareStatement(SQL_SELECT_SUJETS_OPTION, Statement.NO_GENERATED_KEYS);
			preparedStatement.setLong(1, optionESEO.getIdOption());
			resultSet = preparedStatement.executeQuery();
			// récupération des valeurs des attributs de la BDD pour les mettre dans une
			// liste
			while (resultSet.next()) {
				sujets.add(map(resultSet));
			}
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec de la recherche des sujets possédant une option donnée.", e);
		} finally {
			// fermeture des ressources utilisées
			fermetures(resultSet, preparedStatement, connection);
		}
		return sujets;
	}
	
	/**
	 * Liste tous les Sujets associés à une soutenance sans jury.
	 *
	 * @param optionESEO
	 *            l'option donnée.
	 * @return sujets la liste de tous les Sujets possedant optionESEO.
	 */
	public List<Sujet> trouverSujetJuryManquant() {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<Sujet> sujets = new ArrayList<>();
		try {
			
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			// mise en forme de la requête SELECT en fonction des attributs de l'objet
			preparedStatement = connection.prepareStatement(SQL_SELECT_SUJETS_JURY_MANQUANT, Statement.NO_GENERATED_KEYS);
			resultSet = preparedStatement.executeQuery();
			// récupération des valeurs des attributs de la BDD pour les mettre dans une
			// liste
			while (resultSet.next()) {
				sujets.add(map(resultSet));
			}
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec de la recherche des sujets possédant une option donnée.", e);
		} finally {
			// fermeture des ressources utilisées
			fermetures(resultSet, preparedStatement, connection);
		}
		return sujets;
	}

	/**
	 * Supprime la liaison entre l'Option d'un sujet et le Sujet. La suppression se
	 * fait uniquement via les ID de ces deux objets.
	 *
	 * @param sujet
	 *            le sujet que l'on souhaite dissocier de l'option.
	 * @param option
	 *            l'option que l'on souhaite dissocier du sujet.
	 */
	public void supprimerOptionSujet(Sujet sujet, OptionESEO option) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		// tableau de Strings regroupant tous les noms des attributs d'un objet
		// ainsi que les valeurs correspondantes
		String[][] attributs = { { ATTRIBUT_ID_SUJET, OptionESEODAO.ATTRIBUT_ID_OPTION },
				{ String.valueOf(sujet.getIdSujet()), String.valueOf(option.getIdOption()) } };
		try {
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			preparedStatement = initialisationRequetePreparee(connection, DELETE, NOM_ENTITE_OPTION_SUJET, attributs,
					false);
			int statut = preparedStatement.executeUpdate();
			if (statut == 0) {
				throw new SQLException();
			}
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec de la suppression de optionSujet, aucune ligne supprimée de la table.", e);
		} finally {
			// fermeture des ressources utilisées
			fermetures(preparedStatement, connection);
		}
	}

	// ########################################################################################################
	// # Méthodes concernant le bean Poster #
	// ########################################################################################################

	/**
	 * Indique si un Sujet possède ou non un Poster.
	 *
	 * @param sujet
	 *            le sujet concerné.
	 * @return un booléen indiquant si le sujet possède ou non un poster.
	 */
	public boolean possedePoster(Sujet sujet) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		boolean possedePoster = false;
		try {
			// vérification que le sujet rentré n'est pas nul
			if (sujet.getIdSujet() == null) {
				throw new SQLException();
			}
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			// mise en forme de la requête SELECT en fonction des attributs de l'objet
			preparedStatement = initialisationRequetePreparee(connection, SQL_SELECT_IDPOSTER, true,
					sujet.getIdSujet());
			resultSet = preparedStatement.executeQuery();
			// si la query retourne un résultat alors le sujet possède déjà un poster
			if (resultSet.next()) {
				possedePoster = true;
			}
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec de la recherche de la présence d'un poster dans un sujet.", e);
		} finally {
			// fermeture des ressources utilisées
			fermetures(resultSet, preparedStatement, connection);
		}
		return possedePoster;
	}

	/**
	 * Liste tous les Sujets dont les Posters sont validés et qui possèdent une
	 * Option particulière.
	 *
	 * @param options
	 *            la liste des options concernées.
	 * @return sujets la liste des sujets.
	 */
	public List<Sujet> trouverSujetsPostersValidesOptions(List<OptionESEO> options) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<Sujet> sujets = new ArrayList<>();
		try {
			// vérification que le sujet rentré n'est pas nul
			if (options.isEmpty()) {
				throw new SQLException();
			}
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			for (OptionESEO option : options) {
				Long idOption = option.getIdOption();
				preparedStatement = initialisationRequetePreparee(connection, SQL_SELECT_SUJET_OPTION_VALIDES, true,
						idOption);
				resultSet = preparedStatement.executeQuery();
				resultSet.next();
				sujets.add(map(resultSet));
			}
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec de la recherche de sujets dont le poster est validé.", e);
		} finally {
			// fermeture des ressources utilisées
			fermetures(resultSet, preparedStatement, connection);
		}
		return sujets;
	}

	// ########################################################################################################
	// # Méthodes concernant les beans Jury #
	// ########################################################################################################

	/**
	 * Liste les Sujets associés à un JurySoutenance associé à un Utilisateur en
	 * particulier.
	 *
	 * @param utilisateur
	 *            l'utilisateur dont on souhaite connaître les sujets qu'il a
	 *            d'attribués en tant que jurySoutenance.
	 * @return sujets la liste des sujets associés à un jurySoutenance associé à
	 *         l'utilisateur.
	 */
	public List<Sujet> trouverSujetsJurySoutenance(Utilisateur utilisateur) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<Sujet> sujets = new ArrayList<>();
		try {
			// vérification que le sujet rentré n'est pas nul
			if (utilisateur.getIdUtilisateur() == null) {
				throw new SQLException();
			}
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			// mise en forme de la requête SELECT en fonction des attributs de l'objet
			preparedStatement = initialisationRequetePreparee(connection, SQL_SELECT_SUJETS_ASSOCIES_JURY_SOUTENANCE,
					true, utilisateur.getIdUtilisateur());
			resultSet = preparedStatement.executeQuery();
			// récupération des valeurs des attributs de la BDD pour les mettre dans une
			// liste
			while (resultSet.next()) {
				sujets.add(map(resultSet));
			}
		} catch (SQLException e) {
			logger.log(Level.WARN,
					"Échec de la recherche de sujets associés à un jurySoutenance associé à un utilisateur.", e);
		} finally {
			// fermeture des ressources utilisées
			fermetures(resultSet, preparedStatement, connection);
		}
		return sujets;
	}

	/**
	 * Liste les Sujets associés à un JuryPoster associé à un Utilisateur en
	 * particulier.
	 *
	 * @param utilisateur
	 *            l'utilisateur dont on souhaite connaître les sujets qu'il a
	 *            d'attribués en tant que juryPoster.
	 * @return sujets la liste des sujets associés à un juryPoster associé à
	 *         l'utilisateur.
	 */
	public List<Sujet> trouverSujetsJuryPoster(Utilisateur utilisateur) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<Sujet> sujets = new ArrayList<>();
		try {
			// vérification que le sujet rentré n'est pas nul
			if (utilisateur.getIdUtilisateur() == null) {
				throw new SQLException();
			}
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			preparedStatement = initialisationRequetePreparee(connection, SQL_SELECT_SUJETS_ASSOCIES_JURY_POSTER, true,
					utilisateur.getIdUtilisateur());
			resultSet = preparedStatement.executeQuery();
			// récupération des valeurs des attributs de la BDD pour les mettre dans une
			// liste
			while (resultSet.next()) {
				sujets.add(map(resultSet));
			}
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec de la recherche de sujets associés à un juryPoster associé à un utilisateur.",
					e);
		} finally {
			// fermeture des ressources utilisées
			fermetures(resultSet, preparedStatement, connection);
		}
		return sujets;
	}

	// #################################################
	// # Méthodes privées #
	// #################################################

	/**
	 * Fait la correspondance (le mapping) entre une ligne issue de la table Sujet
	 * (un ResultSet) et un bean Sujet.
	 *
	 * @param resultSet
	 *            la ligne issue de la table Sujet.
	 * @return sujet le bean dont on souhaite faire la correspondance.
	 * @throws SQLException
	 */
	public static Sujet map(ResultSet resultSet) throws SQLException {
		Sujet sujet = new Sujet();
		sujet.setIdSujet(resultSet.getLong(ATTRIBUT_ID_SUJET));
		sujet.setTitre(resultSet.getString(ATTRIBUT_TITRE));
		sujet.setDescription(resultSet.getString(ATTRIBUT_DESCRIPTION));
		sujet.setNbrMinEleves(resultSet.getInt(ATTRIBUT_MIN_ELEVE));
		sujet.setNbrMaxEleves(resultSet.getInt(ATTRIBUT_MAX_ELEVE));
		sujet.setContratPro(resultSet.getBoolean(ATTRIBUT_CONTRAT_PRO));
		sujet.setConfidentialite(resultSet.getBoolean(ATTRIBUT_CONFIDENTIALITE));
		sujet.setEtatString(resultSet.getString(ATTRIBUT_ETAT_SUJET));
		sujet.setLiens(resultSet.getString(ATTRIBUT_LIENS));
		sujet.setInterets(resultSet.getString(ATTRIBUT_INTERETS));
		sujet.setNoteInteretTechno(resultSet.getFloat(ATTRIBUT_NOTE_INTERET));
		sujet.setNoteInteretSujet(resultSet.getFloat(ATTRIBUT_NOTE_INTERET_SUJET));
		sujet.setIdForm(resultSet.getInt(ATTRIBUT_ID_FORM));
		if(resultSet.wasNull()) {
			sujet.setIdForm(null);
		}
		return sujet;
	}

}