package fr.eseo.ld.dao;

import fr.eseo.ld.beans.Parametre;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static fr.eseo.ld.dao.DAOUtilitaire.fermetures;
import static fr.eseo.ld.dao.DAOUtilitaire.initialisationRequetePreparee;

/**
 * Classe du DAO du bean Parametre.
 *
 * <p>
 * Cette classe implémente toutes les méthodes CRUD définies dans sa classe
 * mère.
 * </p>
 *
 * @author Dimitri Jarneau
 */
public class ParametreDAO extends DAO<Parametre> {

	private static final String SQL_INSERT = "INSERT INTO Parametres (nomParametre, valeur) VALUES (?, ?)";
	private static final String SQL_SELECT_TOUT = "SELECT * FROM Parametres";
	private static final String SQL_UPDATE = "UPDATE Parametres SET valeur = ? WHERE nomParametre = ?";

	public static final String IDENTIFIANT_LIEN_FORM_SUJET = "lienFormulaireSujet";

	private static Logger logger = Logger.getLogger(ParametreDAO.class.getName());

	private static final String ATTRIBUT_NOM_PARAMETRE = "nomParametre";
	private static final String ATTRIBUT_VALEUR = "valeur";
	private static final String[] ATTRIBUTS_NOMS = { ATTRIBUT_NOM_PARAMETRE, ATTRIBUT_VALEUR };
	private String nomEntiteParametre = "Parametres";

	/**
	 * Constructeur de DAO.
	 *
	 * @param daoFactory
	 *            la Factory permettant la création d'une connexion à la BDD.
	 */
	public ParametreDAO(DAOFactory daoFactory) {
		super(daoFactory);
	}
	// ########################################################################################################
	// # Méthodes CRUD concernant le bean Parametre #
	// ########################################################################################################

	/**
	 * Insère un Parametre dans la BDD à partir des attributs spécifiés dans un bean
	 * Parametre.
	 *
	 * @param parametre
	 *            le parametre que l'on souhaite insérer dans la BDD à partir du
	 *            bean Parametre.
	 */
	@Override
	public void creer(Parametre parametre) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			// les "?" de la requête sont comblés par les attributs de l'objet en paramètre
			// ceux-ci peuvent être nuls à condition que la BDD accepte les données de type
			// "null"
			preparedStatement = initialisationRequetePreparee(connection, SQL_INSERT, true, parametre.getNomParametre(),
					parametre.getValeur());
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec de la création de l'objet, aucune ligne ajoutée dans la table.", e);
		} finally {
			// fermeture des ressources utilisées
			fermetures(preparedStatement, connection);
		}
	}

	/**
	 * Liste tous les Parametres ayant pour attributs les mêmes que ceux spécifiés
	 * dans un bean Parametre.
	 *
	 * @param parametre
	 *            le Parametre que l'on souhaite trouver dans la BDD.
	 * @return parametres la liste des Parametres trouvés dans la BDD.
	 */
	@Override
	public List<Parametre> trouver(Parametre parametre) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<Parametre> parametres = new ArrayList<>();
		// tableau de Strings regroupant tous les noms des attributs d'un objet ainsi
		// que les valeurs correspondantes
		String[][] attributs = { ATTRIBUTS_NOMS,
				{ String.valueOf(parametre.getNomParametre()), String.valueOf(parametre.getValeur()) } };
		try {
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			// mise en forme de la requête SELECT en fonction des attributs de l'objet
			// parametre
			preparedStatement = initialisationRequetePreparee(connection, "SELECT", nomEntiteParametre, attributs, false);
			resultSet = preparedStatement.executeQuery();
			// récupération des valeurs des attributs de la BDD pour les mettre dans une
			// liste
			while (resultSet.next()) {
				parametres.add(map(resultSet));
			}
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec de la recherche de l'objet.", e);
		} finally {
			// fermeture des ressources utilisées
			fermetures(resultSet, preparedStatement, connection);
		}
		return parametres;
	}

	/**
	 * Modifie UN parametre ayant pour attributs les mêmes que ceux spécifiés dans
	 * un bean Parametre et la même clé primaire. Cette clé primaire ne peut être
	 * modifiée.
	 *
	 * @param parametre
	 *            le Parametre que l'on souhaite modifier dans la BDD.
	 */
	@Override
	public void modifier(Parametre parametre) {
		/* Traite la mise à jour de la BDD */
		traitementUpdateParametre(this.getDaoFactory(), parametre.getNomParametre(), parametre.getValeur(), logger);
	}

	/**
	 * Supprime tous les Parametres ayant pour attributs les mêmes que ceux
	 * spécifiés dans un bean Parametre.
	 *
	 * @param parametre
	 *            le Parametre que l'on souhaite supprimer dans la BDD.
	 */
	@Override
	public void supprimer(Parametre parametre) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		// tableau de Strings regroupant tous les noms des attributs d'un objet ainsi
		// que les valeurs correspondantes
		String[][] attributs = { ATTRIBUTS_NOMS,
				{ String.valueOf(parametre.getNomParametre()), String.valueOf(parametre.getValeur()) } };
		try {
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			preparedStatement = initialisationRequetePreparee(connection, "DELETE", nomEntiteParametre, attributs, false);
			int statut = preparedStatement.executeUpdate();
			if (statut == 0) {
				throw new SQLException();
			} else {
				// suppression du bean
				parametre.setNomParametre(null);
			}
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec de la suppression de l'objet, aucune ligne supprimée de la table.", e);
		} finally {
			// fermeture des ressources utilisées
			fermetures(preparedStatement, connection);
		}
	}

	/**
	 * Liste tous les Parametres présents dans la BDD.
	 *
	 * @return parametres la liste des Parametres présents dans la BDD.
	 */
	@Override
	public List<Parametre> lister() {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<Parametre> parametres = new ArrayList<>();
		try {
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			preparedStatement = connection.prepareStatement(SQL_SELECT_TOUT);
			resultSet = preparedStatement.executeQuery();
			// récupération des valeurs des attributs de la BDD pour les mettre dans une
			// liste
			while (resultSet.next()) {
				parametres.add(map(resultSet));
			}
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec du listage des objets.", e);
		} finally {
			fermetures(resultSet, preparedStatement, connection);
		}
		return parametres;
	}

	/**
	 * Fait la correspondance (le mapping) entre une ligne issue de la table
	 * Parametre (un ResultSet) et un bean parametre.
	 *
	 * @param resultSet
	 *            la ligne issue de la table Parametre.
	 * @return parametre le bean dont on souhaite faire la correspondance.
	 * @throws SQLException Exception sql
	 */
	public static Parametre map(ResultSet resultSet) throws SQLException {
		Parametre parametre = new Parametre();
		parametre.setNomParametre(resultSet.getString(ATTRIBUT_NOM_PARAMETRE));
		parametre.setValeur(resultSet.getString(ATTRIBUT_VALEUR));

		return parametre;
	}

	/**
	 * Initialise la requete de mise a jour des parametres
	 * 
	 * @param connection
	 *            connexion
	 * @param nomParam
	 *            Nom du parametre
	 * @param valeur
	 *            valeur
	 * @return Requete preparee
	 */
	private static PreparedStatement initialisationRequetePrepareeUpdate(Connection connection, String nomParam,
			String valeur) {
		// remplacement des "?" par les valeurs des attributs
		PreparedStatement preparedStatement = null;
		try {
			preparedStatement = connection.prepareStatement(ParametreDAO.SQL_UPDATE, Statement.NO_GENERATED_KEYS);
			preparedStatement.setObject(1, valeur);
			preparedStatement.setObject(2, nomParam);
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec de la requête.", e);
		}
		return preparedStatement;
	}

	/**
	 * Traite la mise à jour de la BDD induite par la méthode void modifier(T
	 * objet).
	 *
	 * @param daoFactory
	 *            la DAOFactory qui permet de créer une connexion.
	 * @param nomParam
	 *            le nom du parametre
	 * @param valeur
	 *            la valeur a associer
	 * @param logger
	 *            le logger associé à l'entité à traiter.
	 */
	private static void traitementUpdateParametre(DAOFactory daoFactory, String nomParam, String valeur,
			Logger logger) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = daoFactory.getConnection();
			// mise en forme de la requête UPDATE en fonction des attributs de l'objet
			preparedStatement = initialisationRequetePrepareeUpdate(connection, nomParam, valeur);

			if(preparedStatement != null) {
				preparedStatement.executeUpdate();
			} else {
				logger.log(Level.WARN, "Échec de la mise à jour de l'objet, aucune ligne modifiée dans la table.");
			}
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec de la mise à jour de l'objet, aucune ligne modifiée dans la table.", e);
		} finally {
			// fermeture des ressources utilisées
			fermetures(preparedStatement, connection);
		}
	}

}
