package fr.eseo.ld.dao;

import fr.eseo.ld.pgl.beans.Equipe;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static fr.eseo.ld.dao.DAOUtilitaire.fermetures;
import static fr.eseo.ld.dao.DAOUtilitaire.initialisationRequetePreparee;

/**
 * Classe du DAO du bean Equipe (Pgl).
 *
 * <p>
 * Cette classe implémente toutes les méthodes CRUD définies dans sa classe
 * mère.
 * </p>
 */
public class PglEquipeDAO extends DAO<Equipe> {

    /* Noms des entités */
    private static final String NOM_ENTITE = "pgl_equipe";

    /* Attributs de l'entité NoteEquipe */
    private static final String ATTRIBUT_ID_EQUIPE = "idEquipe";
    public static final String ATTRIBUT_NOM = "nom";
    public static final String ATTRIBUT_REF_PROJET = "refProjet";
    public static final String ATTRIBUT_REF_ANNEE = "refAnnee";

    private static final String[] ATTRIBUTS_NOMS = {ATTRIBUT_ID_EQUIPE, ATTRIBUT_NOM, ATTRIBUT_REF_PROJET, ATTRIBUT_REF_ANNEE};

    private static Logger logger = Logger.getLogger(PglEquipeDAO.class.getName());

    /* Requetes SQL Equipe */
    private static final String SQL_INSERT_SPRINT = "INSERT INTO `pgl_equipe`(`nom`, `refProjet`, `refAnnee`) VALUES (?,?,?)";
    private static final String SQL_SELECT_TOUT = "SELECT * FROM pgl_equipe";


    /* Requetes SQL pour trouver une note à partir de son ID */
    private static final String SQL_SELECT_EQUIPE_A_PARTIR_ID = "SELECT * FROM pgl_equipe WHERE idEquipe=?";

	// requete permettant de retrouver l'id du sujet sur lequel travaille un
	// étudiant
	private static final String SQL_SELECT_EQUIPE_I2 = "SELECT * FROM pgl_equipe,pgl_etudiantequipe WHERE pgl_equipe.idEquipe= pgl_etudiantequipe.idEquipe and pgl_etudiantequipe.idEtudiant= ? ";

    
    /**
     * Constructeur de DAO.
     *
     * @param daoFactory la Factory permettant la création d'une connexion à la BDD.
     */
    PglEquipeDAO(DAOFactory daoFactory) {
        super(daoFactory);
    }

    /**
     * Insère une Equipe dans la BDD à partir des attributs spécifiés dans un bean
     * Equipe.
     *
     * @param equipe l'equipe que l'on souhaite insérer dans la BDD à partir du bean
     *               Equipe.
     */
    @Override
    public void creer(Equipe equipe) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            // création d'une connexion grâce à la DAOFactory placée en attribut de la
            // classe
            connection = this.creerConnexion();
            // les "?" de la requête sont comblés par les attributs de l'objet en paramètre
            // ceux-ci peuvent être nuls à condition que la BDD accepte les données de type
            // "null"
            preparedStatement = initialisationRequetePreparee(connection, SQL_INSERT_SPRINT, true,
                    equipe.getNom(), equipe.getRefProjet(), equipe.getRefAnnee());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            logger.log(Level.WARN, "Échec de la création de l'objet, aucune ligne ajoutée dans la table.", e);
        } finally {
            // fermeture des ressources utilisées
            fermetures(preparedStatement, connection);
        }
    }

    /**
     * Liste tous les Equipe ayant pour attributs les mêmes que ceux spécifiés dans
     * un bean Equipe.
     *
     * @param equipe l'equipe que l'on souhaite trouver dans la BDD.
     * @return equipe la liste des Equipes trouvés dans la BDD.
     */
    @Override
    public List<Equipe> trouver(fr.eseo.ld.pgl.beans.Equipe equipe) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        List<fr.eseo.ld.pgl.beans.Equipe> equipes = new ArrayList<>();
        // tableau de Strings regroupant tous les noms des attributs d'un objet ainsi
        // que les valeurs correspondantes
        String[][] attributs = {ATTRIBUTS_NOMS,
                {String.valueOf(equipe.getIdEquipe()), String.valueOf(equipe.getNom()), String.valueOf(equipe.getRefProjet()),
                        String.valueOf(equipe.getRefAnnee())
                }};
        try {
            // création d'une connexion grâce à la DAOFactory placée en attribut de la classe
            connection = this.creerConnexion();
            // mise en forme de la requête SELECT en fonction des attributs de l'objet sujet

            preparedStatement = initialisationRequetePreparee(connection, "SELECT", NOM_ENTITE, attributs, true);
            resultSet = preparedStatement.executeQuery();
            // récupération des valeurs des attributs de la BDD pour les mettre dans une liste
            while (resultSet.next()) {
                equipes.add(map(resultSet));
            }
        } catch (SQLException e) {
            logger.log(Level.WARN, "Échec de la recherche de l'objet Equipe. ", e);
        } finally {
            fermetures(resultSet, preparedStatement, connection);
        }
        return equipes;
    }

    /**
     * Renvoie une Equipe à partir de son ID.
     *
     * @param idEquipe l'ID de l'Equipe que l'on souhaite trouver dans la BDD.
     * @return equipe l'equipe trouvé dans la BDD.
     */
    public Equipe trouver(Long idEquipe) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Equipe equipe = null;
        try {
            // création d'une connexion grâce à la DAOFactory placée en attribut de la
            // classe
            connection = this.creerConnexion();
            preparedStatement = initialisationRequetePreparee(connection, SQL_SELECT_EQUIPE_A_PARTIR_ID, true,
                    String.valueOf(idEquipe));
            resultSet = preparedStatement.executeQuery();
            resultSet.next();
            equipe = map(resultSet);
        } catch (SQLException e) {
            logger.log(Level.WARN, "Échec de la recherche d'une equipe à partir de son ID.", e);
        } finally {
            // fermeture des ressources utilisées
            fermetures(resultSet, preparedStatement, connection);
        }
        return equipe;
    }

	/**
	 * Renvoie l'Equipe d'un Etudiant.
	 * 
	 * @param idEtudiant
	 *            l'ID de l'etudiant.
	 * @return equipe l'equipe de l'etudiant.
	 */
	public Equipe trouverEqpEtu(Long idEtudiant) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		Equipe equipe = null;
		try {
			// vérification que l'ID de l'étudiant n'est pas nul
			if (idEtudiant == null) {
				throw new SQLException();
			}
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			preparedStatement = initialisationRequetePreparee(connection, SQL_SELECT_EQUIPE_I2, true,
					String.valueOf(idEtudiant));
			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				equipe = map(resultSet);
			}
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec de la recherche de l'équipe de l'etudiant.", e);
		} finally {
			// fermeture des ressources utilisées
			fermetures(resultSet, preparedStatement, connection);
		}
		return equipe;
	}

    /**
     * Modifie une Equipe ayant pour attributs les mêmes que ceux spécifiés dans un
     * bean Equipe et la même clé primaire. Cette clé primaire ne peut être
     * modifiée.
     *
     * @param equipe la noteEquipe que l'on souhaite modifier dans la BDD.
     */
    @Override
    public void modifier(Equipe equipe) {
        //Update table SET <....>
        Map<String, Object> attributs = new HashMap<>();
        attributs.put(ATTRIBUT_NOM, equipe.getNom());
        attributs.put(ATTRIBUT_REF_PROJET, equipe.getRefProjet());
        attributs.put(ATTRIBUT_REF_ANNEE, equipe.getRefAnnee());

        //Preparation de WHERE
        Map<String, Object> whereAtt = new HashMap<>();
        whereAtt.put(ATTRIBUT_ID_EQUIPE, equipe.getIdEquipe());

        //Modifier
        DAOUtilitaire.executeUpdate(this.getDaoFactory(), NOM_ENTITE, attributs, whereAtt);
    }

    /**
     * Supprime tous les Equipes ayant pour attributs les mêmes que ceux spécifiés
     * dans un bean Equipe.
     *
     * @param equipe l'equipe que l'on souhaite supprimer dans la BDD.
     */
    @Override
    public void supprimer(Equipe equipe) {
        Map<String, Object> identifiants = new HashMap<>();
        identifiants.put(ATTRIBUT_ID_EQUIPE, equipe.getIdEquipe());

        DAOUtilitaire.executeDelete(this.getDaoFactory(), NOM_ENTITE, identifiants);
    }

    /**
     * Liste tous les Equipes présents dans la BDD.
     *
     * @return equipe la liste des Equipes présents dans la BDD.
     */
    @Override
    public List<Equipe> lister() {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        List<Equipe> equipes = new ArrayList<>();
        try {
            // création d'une connexion grâce à la DAOFactory placée en attribut de la
            // classe
            connection = this.creerConnexion();
            preparedStatement = connection.prepareStatement(SQL_SELECT_TOUT);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                equipes.add(map(resultSet));
            }
            // récupération des valeurs des attributs de la BDD pour les mettre dans une
            // liste
        } catch (SQLException e) {
            logger.log(Level.WARN, "Échec du listage des objets equipes. ", e);
        } finally {
            fermetures(resultSet, preparedStatement, connection);
        }
        return equipes;
    }


    // #################################################
    // # Méthodes privées #
    // #################################################

    /**
     * Fait la correspondance (le mapping) entre une ligne issue de la table Equipe
     * (un ResultSet) et un bean Equipe.
     *
     * @param resultSet la ligne issue de la table Equipe.
     * @return equipe le bean dont on souhaite faire la correspondance.
     */
    public static Equipe map(ResultSet resultSet) throws SQLException {
        Equipe equipe = new Equipe();
        equipe.setIdEquipe(resultSet.getLong(ATTRIBUT_ID_EQUIPE));
        equipe.setNom(resultSet.getString(ATTRIBUT_NOM));
        equipe.setRefProjet(resultSet.getLong(ATTRIBUT_REF_PROJET));
        equipe.setRefAnnee(resultSet.getLong(ATTRIBUT_REF_ANNEE));
        return equipe;
    }

}
