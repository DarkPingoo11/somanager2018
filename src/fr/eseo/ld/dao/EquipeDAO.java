package fr.eseo.ld.dao;

import fr.eseo.ld.beans.Equipe;
import fr.eseo.ld.beans.EtudiantEquipe;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static fr.eseo.ld.dao.DAOUtilitaire.*;

/**
 * Classe du DAO du bean Equipe.
 * 
 * <p>
 * Cette classe implémente toutes les méthodes CRUD définies dans sa classe
 * mère.
 * </p>
 * 
 * @author Cécilia PINSARD
 */
public class EquipeDAO extends DAO<Equipe> {

	private static final String NOM_ENTITE = "Equipe";
	private static final String ATTRIBUT_ID_EQUIPE = "idEquipe";
	private static final String ATTRIBUT_ANNEE = "annee";
	private static final String ATTRIBUT_TAILLE = "taille";
	private static final String ATTRIBUT_ID_SUJET = "idSujet";
	private static final String ATTRIBUT_VALIDE = "valide";
	private static final String NOM_ENTITE_ETUDIANT_EQUIPE = "EtudiantEquipe";
	private static final String ATTRIBUT_ID_ETUDIANT = "idEtudiant";
	private static final String[] ATTRIBUTS_NOMS = { ATTRIBUT_ID_EQUIPE, ATTRIBUT_ANNEE, ATTRIBUT_TAILLE,
			ATTRIBUT_ID_SUJET, ATTRIBUT_VALIDE };
	private static final String[] ATTRIBUTS_NOMS_ID_FIN = { ATTRIBUT_ANNEE, ATTRIBUT_TAILLE, ATTRIBUT_ID_SUJET,
			ATTRIBUT_VALIDE, ATTRIBUT_ID_EQUIPE };
	private static final String[] ATTRIBUTS_NOMS_ETUDIANT_EQUIPE = { ATTRIBUT_ID_ETUDIANT, ATTRIBUT_ID_EQUIPE };

	// Tous les attributs du bean Equipe doivent être écrits dans la requête INSERT
	private static final String SQL_INSERT = "INSERT INTO Equipe (annee, taille, idSujet, valide) VALUES (?, ?, ?, ?)";
	private static final String SQL_SELECT_TOUT = "SELECT * FROM Equipe";
	private static final String SQL_SELECT_EQUIPE_SUJET = "SELECT * FROM Equipe WHERE Equipe.idSujet = ?";
	private static final String SQL_SELECT_TOUT_ETUDIANT_EQUIPE = "SELECT * FROM EtudiantEquipe";
	private static final String SQL_INSERT_ETUDIANT_EQUIPE = "INSERT INTO EtudiantEquipe (idEtudiant, idEquipe) VALUES (?, ?);";

	// requete permettant de retrouver l'id du sujet sur lequel travaille un
	// étudiant
	private static final String SQL_SELECT_EQUIPE = "SELECT * FROM Equipe,EtudiantEquipe WHERE Equipe.idEquipe= EtudiantEquipe.idEquipe and EtudiantEquipe.idEtudiant= ? ";

	private static Logger logger = Logger.getLogger(EquipeDAO.class.getName());

	/**
	 * Constructeur de DAO.
	 * 
	 * @param daoFactory
	 *            la Factory permettant la création d'une connexion à la BDD.
	 */
	public EquipeDAO(DAOFactory daoFactory) {
		super(daoFactory);
	}

	/**
	 * Insère une Equipe dans la BDD à partir des attributs spécifiés dans un bean
	 * Equipe.
	 * 
	 * @param equipe
	 *            l'Equipe que l'on souhaite insérer dans la BDD à partir du bean
	 *            Equipe.
	 */
	@Override
	public void creer(Equipe equipe) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			// les "?" de la requête sont comblés par les attributs de l'objet en paramètre
			// ceux-ci peuvent être nuls à condition que la BDD accepte les données de type
			// "null"
			preparedStatement = initialisationRequetePreparee(connection, SQL_INSERT, true, equipe.getAnnee(),
					equipe.getTaille(), equipe.getIdSujet(), equipe.getValide());
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec de la création de l'objet, aucune ligne ajoutée dans la table.", e);
		} finally {
			// fermeture des ressources utilisées
			fermetures(preparedStatement, connection);
		}
	}

	/**
	 * Liste toutes les Equipes ayant pour attributs les mêmes que ceux spécifiés
	 * dans un bean Equipe.
	 * 
	 * @param equipe
	 *            l'Equipe que l'on souhaite trouver dans la BDD.
	 * @return equipes la liste des Equipes trouvés dans la BDD.
	 */
	@Override
	public List<Equipe> trouver(Equipe equipe) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<Equipe> equipes = new ArrayList<>();
		// tableau de Strings regroupant tous les noms des attributs d'un objet ainsi
		// que les valeurs correspondantes
		String[][] attributs = { ATTRIBUTS_NOMS,
				{ equipe.getIdEquipe(), String.valueOf(equipe.getAnnee()), String.valueOf(equipe.getTaille()),
						String.valueOf(equipe.getIdSujet()), String.valueOf(equipe.getValide()) } };
		try {
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			// mise en forme de la requête SELECT en fonction des attributs de l'objet
			// equipe
			preparedStatement = initialisationRequetePreparee(connection, "SELECT", NOM_ENTITE, attributs, false);
			resultSet = preparedStatement.executeQuery();
			// récupération des valeurs des attributs de la BDD pour les mettre dans une
			// liste
			while (resultSet.next()) {
				equipes.add(map(resultSet));
			}
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec de la recherche de l'objet.", e);
		} finally {
			// fermeture des ressources utilisées
			fermetures(resultSet, preparedStatement, connection);
		}
		return equipes;
	}

	/**
	 * Modifie UNE Equipe ayant pour attributs les mêmes que ceux spécifiés dans un
	 * bean Equipe et la même clé primaire. Cette clé primaire ne peut être
	 * modifiée.
	 * 
	 * @param equipe
	 *            l'Equipe que l'on souhaite modifier dans la BDD.
	 */
	@Override
	public void modifier(Equipe equipe) {
		// tableau de Strings regroupant tous les noms des attributs d'un objet ainsi
		// que les valeurs correspondantes
		// /!\ bien placer la clé primaire et sa valeur à la FIN du tableau sinon erreur
		String[][] attributs = { ATTRIBUTS_NOMS_ID_FIN,
				{ String.valueOf(equipe.getAnnee()), String.valueOf(equipe.getTaille()),
						String.valueOf(equipe.getIdSujet()), equipe.getValide(), equipe.getIdEquipe() } };

		/* Traite la mise à jour de la BDD */
		traitementUpdate(this.getDaoFactory(), NOM_ENTITE, attributs, logger);
	}

	/**
	 * Supprime toutes les Equipes ayant pour attributs les mêmes que ceux spécifiés
	 * dans un bean Equipe.
	 * 
	 * @param equipe
	 *            l'Equipe que l'on souhaite supprimer dans la BDD.
	 */
	@Override
	public void supprimer(Equipe equipe) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		// tableau de Strings regroupant tous les noms des attributs d'un objet ainsi
		// que les valeurs correspondantes
		String[][] attributs = { ATTRIBUTS_NOMS, { equipe.getIdEquipe(), String.valueOf(equipe.getAnnee()),
				String.valueOf(equipe.getTaille()), String.valueOf(equipe.getIdSujet()), equipe.getValide() } };
		try {
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			preparedStatement = initialisationRequetePreparee(connection, "DELETE", NOM_ENTITE, attributs, false);
			int statut = preparedStatement.executeUpdate();
			if (statut == 0) {
				throw new SQLException();
			} else {
				// suppression du bean
				equipe.setIdEquipe(null);
			}
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec de la suppression de l'objet, aucune ligne supprimée de la table.", e);
		} finally {
			// fermeture des ressources utilisées
			fermetures(preparedStatement, connection);
		}
	}

	/**
	 * Liste toutes les Equipes présentes dans la BDD.
	 * 
	 * @return equipes la liste des Equipes présentes dans la BDD.
	 */
	@Override
	public List<Equipe> lister() {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<Equipe> equipes = new ArrayList<>();
		try {
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			preparedStatement = connection.prepareStatement(SQL_SELECT_TOUT);
			resultSet = preparedStatement.executeQuery();
			// récupération des valeurs des attributs de la BDD pour les mettre dans une
			// liste
			while (resultSet.next()) {
				equipes.add(map(resultSet));
			}
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec du listage des objets.", e);
		} finally {
			fermetures(resultSet, preparedStatement, connection);
		}
		return equipes;
	}

	/**
	 * Insère un EtudiantEquipe dans la BDD à partir des attributs spécifiés dans un
	 * bean EtudiantEquipe.
	 * 
	 * @param etudiantEquipe
	 *            l'EtudiantEquipe que l'on souhaite insérer dans la BDD à partir du
	 *            bean EtudiantEquipe.
	 */
	public void attribuerEtudiantEquipe(EtudiantEquipe etudiantEquipe) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			// les "?" de la requête sont comblés par les attributs de l'objet en paramètre
			// ceux-ci peuvent être nuls à condition que la BDD accepte les données de type
			// "null"
			preparedStatement = initialisationRequetePreparee(connection, SQL_INSERT_ETUDIANT_EQUIPE, true,
					etudiantEquipe.getIdEtudiant(), etudiantEquipe.getIdEquipe());
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec de la création de l'etudiantEquipe, aucune ligne ajoutée dans la table.", e);
		} finally {
			// fermeture des ressources utilisées
			fermetures(preparedStatement, connection);
		}
	}

	/**
	 * Liste tous les EtudiantEquipe ayant pour attributs les mêmes que ceux
	 * spécifiés dans un bean EtudiantEquipe.
	 * 
	 * @param etudiantEquipe
	 *            l'EtudiantEquipe que l'on souhaite trouver dans la BDD.
	 * @return etudiantEquipe la liste des EtudiantEquipe trouvés dans la BDD.
	 */
	public List<EtudiantEquipe> trouverEtudiantEquipe(EtudiantEquipe etudiantEquipe) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<EtudiantEquipe> etudiantEquipes = new ArrayList<>();
		// tableau de Strings regroupant tous les noms des attributs d'un objet ainsi
		// que les valeurs correspondantes
		String[][] attributs = { ATTRIBUTS_NOMS_ETUDIANT_EQUIPE,
				{ String.valueOf(etudiantEquipe.getIdEtudiant()), etudiantEquipe.getIdEquipe() } };
		try {
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			// mise en forme de la requête SELECT en fonction des attributs de l'objet
			// etudiantEquipe
			preparedStatement = initialisationRequetePreparee(connection, "SELECT", NOM_ENTITE_ETUDIANT_EQUIPE,
					attributs, false);
			resultSet = preparedStatement.executeQuery();
			// récupération des valeurs des attributs de la BDD pour les mettre dans une
			// liste
			while (resultSet.next()) {
				etudiantEquipes.add(mapEtudiantEquipe(resultSet));
			}
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec de la recherche de l'etudiantEquipe.", e);
		} finally {
			// fermeture des ressources utilisées
			fermetures(resultSet, preparedStatement, connection);
		}
		return etudiantEquipes;
	}

	/**
	 * Supprime tous les EtudiantEquipe ayant pour attributs les mêmes que ceux
	 * spécifiés dans un bean EtudiantEquipe.
	 * 
	 * @param etudiantEquipe
	 *            l'EtudiantEquipe que l'on souhaite supprimer dans la BDD.
	 */
	public void supprimerEtudiantEquipe(EtudiantEquipe etudiantEquipe) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		// tableau de Strings regroupant tous les noms des attributs d'un objet ainsi
		// que les valeurs correspondantes
		String[][] attributs = { ATTRIBUTS_NOMS_ETUDIANT_EQUIPE,
				{ String.valueOf(etudiantEquipe.getIdEtudiant()), etudiantEquipe.getIdEquipe() } };
		try {
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			preparedStatement = initialisationRequetePreparee(connection, "DELETE", NOM_ENTITE_ETUDIANT_EQUIPE,
					attributs, false);
			int statut = preparedStatement.executeUpdate();
			if (statut == 0) {
				throw new SQLException();
			}
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec de la suppression de l'etudiantEquipe, aucune ligne supprimée de la table.",
					e);
		} finally {
			// fermeture des ressources utilisées
			fermetures(preparedStatement, connection);
		}
	}

	/**
	 * Liste tous les EtudiantEquipe présents dans la BDD.
	 * 
	 * @return etudiantEquipes la liste des EtudiantEquipe présents dans la BDD.
	 */
	public List<EtudiantEquipe> listerEtudiantEquipe() {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<EtudiantEquipe> etudiantEquipes = new ArrayList<>();
		try {
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			preparedStatement = connection.prepareStatement(SQL_SELECT_TOUT_ETUDIANT_EQUIPE);
			resultSet = preparedStatement.executeQuery();
			// récupération des valeurs des attributs de la BDD pour les mettre dans une
			// liste
			while (resultSet.next()) {
				etudiantEquipes.add(mapEtudiantEquipe(resultSet));
			}
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec du listage des etudiantEquipe.", e);
		} finally {
			fermetures(resultSet, preparedStatement, connection);
		}
		return etudiantEquipes;
	}

	/**
	 * Liste toutes les Equipes qui traitent un sujet spécifique.
	 * 
	 * @param idSujet
	 *            l'ID du sujet concerné.
	 * @return equipes la liste des Equipes qui traitent ce sujet.
	 */
	public List<Equipe> listerEquipeSujet(Long idSujet) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<Equipe> equipes = new ArrayList<>();
		try {
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			preparedStatement = connection.prepareStatement(SQL_SELECT_EQUIPE_SUJET, Statement.NO_GENERATED_KEYS);
			preparedStatement.setLong(1, idSujet);
			resultSet = preparedStatement.executeQuery();
			// récupération des valeurs des attributs de la BDD pour les mettre dans une
			// liste
			while (resultSet.next()) {
				equipes.add(map(resultSet));
			}
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec du listage des equipes qui traitent un sujet donné.", e);
		} finally {
			// fermeture des ressources utilisées
			fermetures(resultSet, preparedStatement, connection);
		}
		return equipes;
	}

	/**
	 * Renvoie l'Equipe d'un Etudiant.
	 * 
	 * @param idEtudiant
	 *            l'ID de l'etudiant.
	 * @return equipe l'equipe de l'etudiant.
	 */
	public Equipe trouver(Long idEtudiant) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		Equipe equipe = null;
		try {
			// vérification que l'ID de l'étudiant n'est pas nul
			if (idEtudiant == null) {
				throw new SQLException();
			}
			// création d'une connexion grâce à la DAOFactory placée en attribut de la
			// classe
			connection = this.creerConnexion();
			preparedStatement = initialisationRequetePreparee(connection, SQL_SELECT_EQUIPE, true,
					String.valueOf(idEtudiant));
			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				equipe = map(resultSet);
			}
		} catch (SQLException e) {
			logger.log(Level.WARN, "Échec de la recherche de l'équipe de l'etudiant.", e);
		} finally {
			// fermeture des ressources utilisées
			fermetures(resultSet, preparedStatement, connection);
		}
		return equipe;
	}


	/**
	 * Fait la correspondance (le mapping) entre une ligne issue de la table Equipe
	 * (un ResultSet) et un bean Equipe.
	 * 
	 * @param resultSet
	 *            la ligne issue de la table Equipe.
	 * @return equipe le bean dont on souhaite faire la correspondance.
	 * @throws SQLException
	 */
	public static Equipe map(ResultSet resultSet) throws SQLException {
		Equipe equipe = new Equipe();
		equipe.setIdEquipe(resultSet.getString(ATTRIBUT_ID_EQUIPE));
		equipe.setAnnee(resultSet.getInt(ATTRIBUT_ANNEE));
		equipe.setTaille(resultSet.getInt(ATTRIBUT_TAILLE));
		equipe.setIdSujet(resultSet.getLong(ATTRIBUT_ID_SUJET));
		equipe.setValide(resultSet.getString(ATTRIBUT_VALIDE));
		return equipe;
	}

	/**
	 * Fait la correspondance (le mapping) entre une ligne issue de la table
	 * EtudiantEquipe (un ResultSet) et un bean EtudiantEquipe.
	 * 
	 * @param resultSet
	 *            la ligne issue de la table EtudiantEquipe.
	 * @return etudiantEquipe le bean dont on souhaite faire la correspondance.
	 * @throws SQLException
	 */
	public static EtudiantEquipe mapEtudiantEquipe(ResultSet resultSet) throws SQLException {
		EtudiantEquipe etudiantEquipe = new EtudiantEquipe();
		etudiantEquipe.setIdEtudiant(resultSet.getLong(ATTRIBUT_ID_ETUDIANT));
		etudiantEquipe.setIdEquipe(resultSet.getString(ATTRIBUT_ID_EQUIPE));
		return etudiantEquipe;
	}

}