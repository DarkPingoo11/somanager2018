package fr.eseo.ld.beans;

/**
 * Classe du bean Parametre.
 *
 * <p>
 * Définition des attributs de l'objet Parametre ainsi que des accesseurs et des
 * mutateurs.
 * </p>
 *
 * @author Dimitri Jarneau
 */
public class Parametre {
	/*
	 * Attributs
	 */
	private String nomParametre;
	private String valeur;

	/**
	 * Constructeur par défaut.
	 */
	public Parametre() {
		super();
	}

	/* Accesseurs et Mutateurs */

	public String getNomParametre() {
		return nomParametre;
	}

	public void setNomParametre(String nomParametre) {
		this.nomParametre = nomParametre;
	}

	public String getValeur() {
		return valeur;
	}

	public void setValeur(String valeur) {
		this.valeur = valeur;
	}

	/**
	 * Redéfinition de la méthode toString().
	 */
	@Override
	public String toString() {
		return "Parametre{" + "nomParametre='" + nomParametre + '\'' + ", valeur='" + valeur + '\'' + '}';
	}
}
