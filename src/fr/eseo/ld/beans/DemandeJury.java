package fr.eseo.ld.beans;

/**
 * Classe du bean DemandeJury.
 * 
 * <p>
 * Définition des attributs de l'objet DemandeJury ainsi que des accesseurs et
 * des mutateurs.
 * </p>
 */
public class DemandeJury {

	/* Attributs */
	private Long idDemande;
	private String sujetsConcernes;
	private String commentaire;
	private String nomOption;

	/**
	 * Constructeur par défaut.
	 */
	public DemandeJury() {
		super();
	}

	/* Accesseurs et Mutateurs */
	/**
	 * Accessseur pour récupérer l'ID de la demande
	 * 
	 * @return idDemande
	 */
	public Long getIdDemande() {
		return this.idDemande;
	}

	/**
	 * Mutateur pour modifier l'ID de la demande
	 * 
	 * @param idDemande
	 */
	public void setIdDemande(Long idDemande) {
		this.idDemande = idDemande;
	}

	/**
	 * Accessseur pour récupérer les sujets concernés par la demande
	 * 
	 * @return sujetsConcernes
	 */
	public String getSujetsConcernes() {
		return this.sujetsConcernes;
	}

	/**
	 * Mutateur pour modifier les sujets concernés par la demande
	 * 
	 * @param sujetsConcernes
	 */
	public void setSujetsConcernes(String sujetsConcernes) {
		this.sujetsConcernes = sujetsConcernes;
	}

	/**
	 * Accessseur pour récupérer le commentaire de la demande
	 * 
	 * @return commentaire
	 */
	public String getCommentaire() {
		return this.commentaire;
	}

	/**
	 * Mutateur pour modifier le commentaire de la demande
	 * 
	 * @param commentaire
	 */
	public void setCommentaire(String commentaire) {
		this.commentaire = commentaire;
	}

	/**
	 * Accessseur pour récupérer l'option de la demande
	 * 
	 * @return nomOption
	 */
	public String getNomOption() {
		return nomOption;
	}

	/**
	 * Mutateur pour modifier l'option de la demande
	 * 
	 * @param nomOption
	 */
	public void setNomOption(String nomOption) {
		this.nomOption = nomOption;
	}

	/**
	 * Redéfinition de la méthode toString().
	 */
	public String toString() {
		return "DemandeJury [idDemande=" + this.idDemande + ", sujetsConcernes=" + this.sujetsConcernes
				+ ", commentaire=" + this.commentaire + "]";
	}
}