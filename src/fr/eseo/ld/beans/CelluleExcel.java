package fr.eseo.ld.beans;

public class CelluleExcel {

    private String donnees;
    private String hexColorBackground;
    private int width;
    private int height;

    public CelluleExcel() {
        //Methode par défaut
    }

    public String getDonnees() {
        return donnees;
    }

    public void setDonnees(String donnees) {
        this.donnees = donnees;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public String getHexColorBackground() {
        return hexColorBackground;
    }

    public void setHexColorBackground(String hexColorBackground) {
        this.hexColorBackground = hexColorBackground;
    }
}
