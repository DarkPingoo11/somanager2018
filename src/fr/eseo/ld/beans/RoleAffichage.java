package fr.eseo.ld.beans;

/**
 * Classe du bean Role, reaménagé pour l'affichage.
 * 
 * <p>
 * Définition des attributs de l'objet Role ainsi que des accesseurs et des
 * mutateurs.
 * </p>
 */
public class RoleAffichage extends Role{

	/* Attributs */
	private String nomComplet;
	private String icone;

	/**
	 * Constructeur par défaut.
	 */
	public RoleAffichage() {
		super();
	}

	public String getNomComplet() {
		return nomComplet;
	}

	public void setNomComplet(String nomComplet) {
		this.nomComplet = nomComplet;
	}

	public String getIcone() {
		return icone;
	}

	public void setIcone(String icone) {
		this.icone = icone;
	}
}