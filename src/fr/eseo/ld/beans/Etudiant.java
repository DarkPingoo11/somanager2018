package fr.eseo.ld.beans;

/**
 * Classe du bean Etudiant.
 * 
 * <p>
 * Définition des attributs de l'objet Etudiant ainsi que des accesseurs et des
 * mutateurs.
 * </p>
 */
public class Etudiant {

	/* Attributs */
	private Long idEtudiant;
	private Integer annee;
	private String contratPro;
	private Float noteIntermediaire;
	private Float noteProjet;
	private Float noteSoutenance;
	private Float notePoster;

	/**
	 * Constructeur par défaut.
	 */
	public Etudiant() {
		super();
	}

	/* Accesseurs et Mutateurs */
	/**
	 * Accessseur pour récupérer l'ID de l'étudiant
	 * 
	 * @return idEtudiant
	 */
	public Long getIdEtudiant() {
		return this.idEtudiant;
	}

	/**
	 * Mutateur pour modifier l'ID de l'étudiant
	 * 
	 * @param idEtudiant
	 */
	public void setIdEtudiant(Long idEtudiant) {
		this.idEtudiant = idEtudiant;
	}

	/**
	 * Accessseur pour récupérer l'année de l'étudiant
	 * 
	 * @return annee
	 */
	public Integer getAnnee() {
		return this.annee;
	}

	/**
	 * Mutateur pour modifier l'année de l'étudiant
	 * 
	 * @param annee
	 */
	public void setAnnee(Integer annee) {
		this.annee = annee;
	}

	/**
	 * Accessseur pour récupérer si l'étudiant est en contrat pro ou non
	 * 
	 * @return contratPro
	 */
	public String getContratPro() {
		return this.contratPro;
	}

	/**
	 * Mutateur pour modifier si l'étudiant est en contrat pro ou non
	 * 
	 * @param contratPro
	 */
	public void setContratPro(String contratPro) {
		this.contratPro = contratPro;
	}

	/**
	 * Accessseur pour récupérer la note intermédiaire de l'étudiant
	 * 
	 * @return noteIntermediaire
	 */
	public Float getNoteIntermediaire() {
		return this.noteIntermediaire;
	}

	/**
	 * Mutateur pour modifier la note intermédiaire de l'étudiant
	 * 
	 * @param noteIntermediaire
	 */
	public void setNoteIntermediaire(Float noteIntermediaire) {
		this.noteIntermediaire = noteIntermediaire;
	}

	/**
	 * Accessseur pour récupérer la note de fin de projet de l'étudiant
	 * 
	 * @return noteProjet
	 */
	public Float getNoteProjet() {
		return this.noteProjet;
	}

	/**
	 * Mutateur pour modifier la note de fin de projet de l'étudiant
	 * 
	 * @param noteProjet
	 */
	public void setNoteProjet(Float noteProjet) {
		this.noteProjet = noteProjet;
	}

	/**
	 * Accessseur pour récupérer la note de soutenance de l'étudiant
	 * 
	 * @return noteSoutenance
	 */
	public Float getNoteSoutenance() {
		return this.noteSoutenance;
	}

	/**
	 * Mutateur pour modifier la note de soutenance de l'étudiant
	 * 
	 * @param noteSoutenance
	 */
	public void setNoteSoutenance(Float noteSoutenance) {
		this.noteSoutenance = noteSoutenance;
	}

	/**
	 * Accessseur pour récupérer la note de poster de l'étudiant
	 * 
	 * @return notePoster
	 */
	public Float getNotePoster() {
		return this.notePoster;
	}

	/**
	 * Mutateur pour modifier la note de poster de l'étudiant
	 * 
	 * @param notePoster
	 */
	public void setNotePoster(Float notePoster) {
		this.notePoster = notePoster;
	}

	/**
	 * Redéfinition de la méthode toString().
	 */
	@Override
	public String toString() {
		return "Etudiant [idEtudiant=" + this.idEtudiant + ", annee=" + this.annee + ", contratPro=" + this.contratPro
				+ ", noteIntermediaire=" + this.noteIntermediaire + ", noteProjet=" + this.noteProjet
				+ ", noteSoutenance=" + this.noteSoutenance + ", notePoster=" + this.notePoster + "]";
	}

}