package fr.eseo.ld.beans;

import java.util.List;

/**
 * Classe du bean ResultatFiltreUtilisateur.
 * 
 * <p>
 * Définition des attributs de l'objet ResultatFiltreUtilisateur ainsi que des accesseurs et
 * des mutateurs.
 * </p>
 */
public class ResultatFiltreUtilisateur{

	/* Attributs */
	private List<FiltreUtilisateur> filtreUtilisateurs;
	private Integer resultats;
	private Integer page;

	/**
	 * Constructeur par défaut.
	 */
	public ResultatFiltreUtilisateur() {
		super();
	}

	/**
	 * Récupère la liste des résultats filtreUtilisateur
	 * @return resultats
	 */
	public List<FiltreUtilisateur> getFiltreUtilisateurs() {
		return filtreUtilisateurs;
	}

	/**
	 * Défini la liste des résultats du filtreUtilisateur
	 * @param filtreUtilisateurs resultats
	 */
	public void setFiltreUtilisateurs(List<FiltreUtilisateur> filtreUtilisateurs) {
		this.filtreUtilisateurs = filtreUtilisateurs;
	}

	/**
	 * Récupère le nombre de lignes retournées
	 * @return nombre de lignes
	 */
	public Integer getResultats() {
		return resultats;
	}

	/**
	 * Défini le nombre de lignes retournées
	 * @param resultats nombre de lignes
	 */
	public void setResultats(Integer resultats) {
		this.resultats = resultats;
	}

	/**
	 * Récupère la page courante
	 * @return page courante
	 */
	public Integer getPage() {
		return page;
	}

	/**
	 * Défini la page courante
	 * @param page page courante
	 */
	public void setPage(Integer page) {
		this.page = page;
	}
}