package fr.eseo.ld.beans;

/**
 * Classe du bean NotePoster.
 * 
 * <p>
 * Définition des attributs de l'objet NotePoster ainsi que des accesseurs et
 * des mutateurs.
 * </p>
 */
public class NotePoster {

	/* Attributs */
	private Long idProfesseur;
	private Long idPoster;
	private Long idEtudiant;
	private Float note;

	/**
	 * Constructeur par défaut.
	 */
	public NotePoster() {
		super();
	}

	/* Accesseurs et Mutateurs */
	/**
	 * Accessseur pour récupérer l'ID du professeur qui met la note
	 * 
	 * @return idProfesseur
	 */
	public Long getIdProfesseur() {
		return this.idProfesseur;
	}

	/**
	 * Mutateur pour modifier l'ID du professeur qui met la note
	 * 
	 * @param idProfesseur
	 */
	public void setIdProfesseur(Long idProfesseur) {
		this.idProfesseur = idProfesseur;
	}

	/**
	 * Accessseur pour récupérer l'ID du poster concerné
	 * 
	 * @return idPoster
	 */
	public Long getIdPoster() {
		return this.idPoster;
	}

	/**
	 * Mutateur pour modifier l'ID du poster concerné
	 * 
	 * @param idPoster
	 */
	public void setIdPoster(Long idPoster) {
		this.idPoster = idPoster;
	}

	/**
	 * Accessseur pour récupérer l'ID de l'étudiant noté
	 * 
	 * @return idEtudiant
	 */
	public Long getIdEtudiant() {
		return this.idEtudiant;
	}

	/**
	 * Mutateur pour modifier l'ID de l'étudiant noté
	 * 
	 * @param idEtudiant
	 */
	public void setIdEtudiant(Long idEtudiant) {
		this.idEtudiant = idEtudiant;
	}

	/**
	 * Accessseur pour récupérer la note attribuée
	 * 
	 * @return note
	 */
	public Float getNote() {
		return this.note;
	}

	/**
	 * Mutateur pour modifier la note attribuée
	 * 
	 * @param note
	 */
	public void setNote(Float note) {
		this.note = note;
	}

	/**
	 * Redéfinition de la méthode toString().
	 */
	public String toString() {
		return "NotePoster [idProfesseur=" + this.getIdProfesseur() + ", idPoster=" + this.getIdPoster()
				+ ", idEtudiant=" + this.getIdEtudiant() + ", note=" + this.getNote() + "]";
	}

}