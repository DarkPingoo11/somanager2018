package fr.eseo.ld.beans;

/**
 * Classe du bean JurySoutenance.
 * 
 * <p>
 * Définition des attributs de l'objet JurySoutenance ainsi que des accesseurs
 * et des mutateurs.
 * </p>
 */
public class JurySoutenance {

	/* Attributs */
	private String idJurySoutenance;
	private Long idProf1;
	private Long idProf2;

	/**
	 * Constructeur par défaut.
	 */
	public JurySoutenance() {
		super();
	}

	/* Accesseurs et Mutateurs */
	/**
	 * Accessseur pour récupérer l'ID du jury d'évaluation de la soutenance
	 * 
	 * @return idJurySoutenance
	 */
	public String getIdJurySoutenance() {
		return this.idJurySoutenance;
	}

	/**
	 * Mutateur pour modifier l'ID du jury d'évaluation de la soutenance
	 * 
	 * @param idJurySoutenance
	 */
	public void setIdJurySoutenance(String idJurySoutenance) {
		this.idJurySoutenance = idJurySoutenance;
	}

	/**
	 * Accessseur pour récupérer l'ID du premier professeur du jury
	 * 
	 * @return idProf1
	 */
	public Long getIdProf1() {
		return this.idProf1;
	}

	/**
	 * Mutateur pour modifier l'ID du premier professeur du jury
	 * 
	 * @param idProf1
	 */
	public void setIdProf1(Long idProf1) {
		this.idProf1 = idProf1;
	}

	/**
	 * Accessseur pour récupérer l'ID du deuxième professeur du jury
	 * 
	 * @return idProf2
	 */
	public Long getIdProf2() {
		return this.idProf2;
	}

	/**
	 * Mutateur pour modifier l'ID du deuxième professeur du jury
	 * 
	 * @param idProf2
	 */
	public void setIdProf2(Long idProf2) {
		this.idProf2 = idProf2;
	}

	/**
	 * Redéfinition de la méthode toString().
	 */
	@Override
	public String toString() {
		return "JurySoutenance [idJurySoutenance=" + this.idJurySoutenance + ", idProf1=" + this.idProf1 + ", idProf2="
				+ this.idProf2 + "]";
	}

}