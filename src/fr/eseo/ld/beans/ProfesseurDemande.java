package fr.eseo.ld.beans;

/**
 * Classe du bean ProfesseurDemande.
 * 
 * <p>
 * Définition des attributs de l'objet ProfesseurDemande ainsi que des
 * accesseurs et des mutateurs.
 * </p>
 */
public class ProfesseurDemande {

	/* Attributs */
	private Long idProfesseur;
	private Long idDemande;

	/**
	 * Constructeur par défaut.
	 */
	public ProfesseurDemande() {
		super();
	}

	/* Accesseurs et Mutateurs */
	/**
	 * Accessseur pour récupérer l'ID du professeur
	 * 
	 * @return idProfesseur
	 */
	public Long getIdProfesseur() {
		return this.idProfesseur;
	}

	/**
	 * Mutateur pour modifier l'ID du professeur
	 * 
	 * @param idProfesseur
	 */
	public void setIdProfesseur(Long idProfesseur) {
		this.idProfesseur = idProfesseur;
	}

	/* Accesseurs et Mutateurs */
	/**
	 * Accessseur pour récupérer l'ID de la demande
	 * 
	 * @return idDemande
	 */
	public Long getIdDemande() {
		return this.idDemande;
	}

	/**
	 * Mutateur pour modifier l'ID de la demande
	 * 
	 * @param idDemande
	 */
	public void setIdDemande(Long idDemande) {
		this.idDemande = idDemande;
	}

	/**
	 * Redéfinition de la méthode toString().
	 */
	public String toString() {
		return "ProfesseurSujet [idProfesseur=" + this.idProfesseur + ", idDemande=" + this.idDemande + "]";
	}

}