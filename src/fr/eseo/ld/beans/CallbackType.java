package fr.eseo.ld.beans;

public enum CallbackType {

	SUCCESS("success"), ERROR("danger"), WARNING("warning"), INFO("info");

	private String value;

	CallbackType(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}
}
