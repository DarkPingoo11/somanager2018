package fr.eseo.ld.beans;

public class ModeleExcel {

    private String nomFichier;
    private Integer ligneHeader;
    private Integer colId;
    private Integer colNom;
    private Integer colNote;
    private Integer colCom;
    private Integer colMoyenne;
    private Integer colGrade;

    public ModeleExcel() {
        //Methode par défaut
    }


    public String getNomFichier() {
        return nomFichier;
    }

    public void setNomFichier(String nomFichier) {
        this.nomFichier = nomFichier;
    }

    public Integer getLigneHeader() {
        return ligneHeader;
    }

    public void setLigneHeader(Integer ligneHeader) {
        this.ligneHeader = ligneHeader;
    }

    public Integer getColId() {
        return colId;
    }

    public void setColId(Integer colId) {
        this.colId = colId;
    }

    public Integer getColNom() {
        return colNom;
    }

    public void setColNom(Integer colNom) {
        this.colNom = colNom;
    }

    public Integer getColGrade() {
        return colGrade;
    }

    public void setColGrade(Integer colGrade) {
        this.colGrade = colGrade;
    }

    public Integer getColMoyenne() {
        return colMoyenne;
    }

    public void setColMoyenne(Integer colMoyenne) {
        this.colMoyenne = colMoyenne;
    }

    public Integer getColNote() {
        return colNote;
    }

    public void setColNote(Integer colNote) {
        this.colNote = colNote;
    }

    public Integer getColCom() {
        return colCom;
    }

    public void setColCom(Integer colCom) {
        this.colCom = colCom;
    }
}
