package fr.eseo.ld.beans;

public enum RoleType {

    APPLICATIF("Applicatif"),
    EQUIPE("Equipe");

    private String nom;

    RoleType(String nom) {
        this.nom = nom;
    }

    public String getNom() {
        return nom;
    }
}
