package fr.eseo.ld.beans;

import java.util.List;

/**
 * Classe du bean FiltreUtilisateur.
 * 
 * <p>
 * Définition des attributs de l'objet FiltreUtilisateur ainsi que des accesseurs et
 * des mutateurs.
 * </p>
 */
public class FiltreUtilisateur extends Utilisateur{

	/* Attributs */
	private List<Role> roles;
	private Integer annee;

	/**
	 * Constructeur par défaut.
	 */
	public FiltreUtilisateur() {
		super();
	}

	/* Accesseurs et Mutateurs */

	/**
	 * Récupère la liste des identifiants des rôles de l'utilisateur
	 * @return liste des identifiants des rôles
	 */
	public List<Role> getRoles() {
		return roles;
	}

	/**
	 * Défini la liste des identifiants des rôles
	 * @param roles identifiants
	 */
	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}

	/**
	 * Récupère l'année scolaire de l'utilisateur
	 * @return annee
	 */
	public Integer getAnnee() {
		return annee;
	}

	/**
	 * Défini l'année scolaire de l'utilisateur
	 * @param annee année scolaire
	 */
	public void setAnnee(Integer annee) {
		this.annee = annee;
	}
}