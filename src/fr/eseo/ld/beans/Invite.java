package fr.eseo.ld.beans;

/**
 * Classe du bean Invite.
 *
 * <p>
 * Définition des attributs de l'objet Professeur ainsi que des accesseurs et
 * des mutateurs.
 * </p>
 */
public class Invite {
    /* Attributs */
    Long refReunion;
    Long refUtilisateur;
    String participe;

    public Long getRefReunion() {
        return refReunion;
    }

    public void setRefReunion(Long refReunion) {
        this.refReunion = refReunion;
    }

    public Long getRefUtilisateur() {
        return refUtilisateur;
    }

    public void setRefUtilisateur(Long refUtilisateur) {
        this.refUtilisateur = refUtilisateur;
    }

    public String getParticipe() {
        return participe;
    }

    public void setParticipe(String participe) {
        this.participe = participe;
    }

    @Override
    public String toString() {
        return "Invite{" +
                "refReunion=" + refReunion +
                ", refUtilisateur=" + refUtilisateur +
                ", participe=" + participe +
                '}';
    }
}
