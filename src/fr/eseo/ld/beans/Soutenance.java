package fr.eseo.ld.beans;

import java.util.Date;

/**
 * Classe du bean Soutenance.
 * 
 * <p>
 * Définition des attributs de l'objet Soutenance ainsi que des accesseurs et
 * des mutateurs.
 * </p>
 */
public class Soutenance {

	/* Attributs */
	private Long idSoutenance;
	private Date dateSoutenance;
	private String idJurySoutenance;
	private String idEquipe;

	/**
	 * Constructeur par défaut.
	 */
	public Soutenance() {
		super();
	}

	/* Accesseurs et Mutateurs */
	/**
	 * Accessseur pour récupérer l'ID de la soutenance
	 * 
	 * @return idSoutenance
	 */
	public Long getIdSoutenance() {
		return this.idSoutenance;
	}

	/**
	 * Mutateur pour modifier l'ID de la soutenance
	 * 
	 * @param idSoutenance
	 */
	public void setIdSoutenance(Long idSoutenance) {
		this.idSoutenance = idSoutenance;
	}

	/**
	 * Accessseur pour récupérer la date et l'heure de la soutenance
	 * 
	 * @return dateSoutenance
	 */
	public Date getDateSoutenance() {
		return this.dateSoutenance;
	}

	/**
	 * Mutateur pour modifier la date et l'heure de la soutenance
	 * 
	 * @param dateSoutenance
	 */
	public void setDateSoutenance(Date dateSoutenance) {
		this.dateSoutenance = dateSoutenance;
	}

	/**
	 * Accessseur pour récupérer l'ID du jury de la soutenance
	 * 
	 * @return idJurySoutenance
	 */
	public String getIdJurySoutenance() {
		return this.idJurySoutenance;
	}

	/**
	 * Mutateur pour modifier l'ID du jury de la soutenance
	 * 
	 * @param idJurySoutenance
	 */
	public void setIdJurySoutenance(String idJurySoutenance) {
		this.idJurySoutenance = idJurySoutenance;
	}

	/**
	 * Accessseur pour récupérer l'ID de l'équipe qui passe la soutenance
	 * 
	 * @return idEquipe
	 */
	public String getIdEquipe() {
		return this.idEquipe;
	}

	/**
	 * Mutateur pour modifier l'ID de l'équipe qui passe la soutenance
	 * 
	 * @param idEquipe
	 */
	public void setIdEquipe(String idEquipe) {
		this.idEquipe = idEquipe;
	}

	/**
	 * Redéfinition de la méthode toString().
	 */
	public String toString() {
		return "Soutenance [idSoutenance=" + this.idSoutenance + ", dateSoutenance=" + this.dateSoutenance + ", idJurySoutenance="
				+ this.idJurySoutenance + ", idEquipe=" + this.idEquipe + "]";
	}

}