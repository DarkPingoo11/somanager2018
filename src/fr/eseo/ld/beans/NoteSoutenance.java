package fr.eseo.ld.beans;

/**
 * Classe du bean NoteSoutenance.
 * 
 * <p>
 * Définition des attributs de l'objet NoteSoutenance ainsi que des accesseurs
 * et des mutateurs.
 * </p>
 */
public class NoteSoutenance {

	/* Attributs */
	private Long idProfesseur;
	private Long idEtudiant;
	private Long idSoutenance;
	private Float note;

	/**
	 * Constructeur par défaut.
	 */
	public NoteSoutenance() {
		super();
	}

	/* Accesseurs et Mutateurs */
	/**
	 * Accessseur pour récupérer l'ID du professeur qui met la note
	 * 
	 * @return idProfesseur
	 */
	public Long getIdProfesseur() {
		return this.idProfesseur;
	}

	/**
	 * Mutateur pour modifier l'ID du professeur qui met la note
	 * 
	 * @param idProfesseur
	 */
	public void setIdProfesseur(Long idProfesseur) {
		this.idProfesseur = idProfesseur;
	}

	/**
	 * Accessseur pour récupérer l'ID de l'étudiant noté
	 * 
	 * @return idEtudiant
	 */
	public Long getIdEtudiant() {
		return this.idEtudiant;
	}

	/**
	 * Mutateur pour modifier l'ID de l'étudiant noté
	 * 
	 * @param idEtudiant
	 */
	public void setIdEtudiant(Long idEtudiant) {
		this.idEtudiant = idEtudiant;
	}

	/**
	 * Accessseur pour récupérer l'ID de la soutenance concernée
	 * 
	 * @return idSoutenance
	 */
	public Long getIdSoutenance() {
		return this.idSoutenance;
	}

	/**
	 * Mutateur pour modifier l'ID de la soutenance concernée
	 * 
	 * @param idSoutenance
	 */
	public void setIdSoutenance(Long idSoutenance) {
		this.idSoutenance = idSoutenance;
	}

	/**
	 * Accessseur pour récupérer la note attribuée
	 * 
	 * @return note
	 */
	public Float getNote() {
		return this.note;
	}

	/**
	 * Mutateur pour modifier la note attribuée
	 * 
	 * @param note
	 */
	public void setNote(Float note) {
		this.note = note;
	}

	/**
	 * Redéfinition de la méthode toString().
	 */
	public String toString() {
		return "NoteSoutenance [idProfesseur=" + this.idProfesseur + ", idEtudiant=" + this.idEtudiant
				+ ", idSoutenance=" + this.idSoutenance + ", note=" + this.note + "]";
	}

}