package fr.eseo.ld.beans;

import com.google.gson.JsonObject;

public class Callback {
	private static final String JSON_CALLBACK_TYPE 	= "type";
	private static final String JSON_CALLBACK_MSG 	= "message";

	private String type;
	private String message;

	/**
	 * Constructeur par défaut
	 */
	public Callback() {
	}

	/**
	 * Callback d'erreur avec message
	 * 
	 * @param message
	 *            Message de succes
	 */
	public Callback(String message) {
		this("danger", message);
	}

	/**
	 * Callback personnalisé, instancié avec un enum
	 * 
	 * @param type
	 *            enum du type de callback
	 * @param message
	 *            Message de callback
	 */
	public Callback(CallbackType type, String message) {
		this(type.getValue(), message);
	}

	/**
	 * Callback personnalisé
	 * 
	 * @param type
	 *            success / danger / info / warning
	 * @param message
	 *            Message de callback
	 */
	public Callback(String type, String message) {
		this.type = type;
		this.message = message;
	}

	/**
	 * GetMessage
	 * 
	 * @return Message du callback
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * SetMessage
	 * 
	 * @param message
	 *            Message du callback
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * GetType
	 * 
	 * @return Type du callback (success / danger / info / warning)
	 */
	public String getType() {
		return type;
	}

	/**
	 * SetType
	 * 
	 * @param type
	 *            Type du callback (success / danger / info / warning)
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * Renvoies l'objet JSON correspondant
	 * @return un objet JSON
	 */
	public JsonObject getJsonObject() {
		JsonObject json = new JsonObject();
		json.addProperty(JSON_CALLBACK_TYPE, this.type.equalsIgnoreCase("success") ? "success" : "error");
		json.addProperty(JSON_CALLBACK_MSG, this.getMessage());

		return json;
	}
}
