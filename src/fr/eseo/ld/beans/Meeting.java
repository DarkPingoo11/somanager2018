package fr.eseo.ld.beans;
/**
 * Classe du bean Meeting.
 *
 * <p>
 * Définition des attributs de l'objet Meeting ainsi que des accesseurs et des
 * mutateurs.
 * </p>
 */
public class Meeting {


    /* Attributs */
    private Long idReunion;
    private String dateReunion;
    private String compteRendu;
    private String titre;
    private String description;
    private String lieu;
    private Long refUtilisateur;

    public Long getIdReunion() {
        return idReunion;
    }

    public void setIdReunion(Long idReunion) {
        this.idReunion = idReunion;
    }

    public String getDate() {
        return dateReunion;
    }

    public void setDate(String dateReunion) { this.dateReunion = dateReunion; }

    public String getCompteRendu() {
        return compteRendu;
    }

    public void setCompteRendu(String compteRendu) {
        this.compteRendu = compteRendu;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLieu() {
        return lieu;
    }

    public void setLieu(String lieu) {
        this.lieu = lieu;
    }

    public Long getRefUtilisateur() {
        return refUtilisateur;
    }

    public void setRefUtilisateur(Long refUtilisateur) {
        this.refUtilisateur = refUtilisateur;
    }

    @Override
    public String toString() {
        return "Meeting{" +
                "idReunion=" + idReunion +
                ", dateReunion=" + dateReunion +
                ", compteRendu=" + compteRendu +
                ", titre=" + titre +
                ", description=" + description +
                ", lieu=" + lieu +
                ", refUtilisateur=" + refUtilisateur +
                '}';
    }
}
