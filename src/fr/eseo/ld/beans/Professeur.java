package fr.eseo.ld.beans;

/**
 * Classe du bean Professeur.
 * 
 * <p>
 * Définition des attributs de l'objet Professeur ainsi que des accesseurs et
 * des mutateurs.
 * </p>
 */
public class Professeur {

	/* Attributs */
	private Long idProfesseur;

	/**
	 * Constructeur par défaut.
	 */
	public Professeur() {
		super();
	}

	/* Accesseurs et Mutateurs */
	/**
	 * Accessseur pour récupérer l'ID du professeur
	 * 
	 * @return idProfesseur
	 */
	public Long getIdProfesseur() {
		return this.idProfesseur;
	}

	/**
	 * Mutateur pour modifier l'ID du professeur
	 * 
	 * @param idProfesseur
	 */
	public void setIdProfesseur(Long idProfesseur) {
		this.idProfesseur = idProfesseur;
	}

	/**
	 * Redéfinition de la méthode toString().
	 */
	public String toString() {
		return "Professeur [idProfesseur = " + this.idProfesseur + "]";
	}

}