package fr.eseo.ld.beans;

import java.util.Date;

/**
 * Classe du bean JuryPoster.
 * 
 * <p>
 * Définition des attributs de l'objet JuryPoster ainsi que des accesseurs et
 * des mutateurs.
 * </p>
 */
public class JuryPoster {

	/* Attributs */
	private String idJuryPoster;
	private Long idProf1;
	private Long idProf2;
	private Long idProf3;
	private Date date;
	private String idEquipe;

	/**
	 * Constructeur par défaut.
	 */
	public JuryPoster() {
		super();
	}

	/* Accesseurs et Mutateurs */
	/**
	 * Accessseur pour récupérer l'ID du jury d'évaluation du poster
	 * 
	 * @return idJuryPoster
	 */
	public String getIdJuryPoster() {
		return this.idJuryPoster;
	}

	/**
	 * Mutateur pour modifier l'ID du jury d'évaluation du poster
	 * 
	 * @param idJuryPoster
	 */
	public void setIdJuryPoster(String idJuryPoster) {
		this.idJuryPoster = idJuryPoster;
	}

	/**
	 * Accessseur pour récupérer l'ID du premier professeur du jury
	 * 
	 * @return idProf1
	 */
	public Long getIdProf1() {
		return this.idProf1;
	}

	/**
	 * Mutateur pour modifier l'ID du premier professeur du jury
	 * 
	 * @param idProf1
	 */
	public void setIdProf1(Long idProf1) {
		this.idProf1 = idProf1;
	}

	/**
	 * Accessseur pour récupérer l'ID du deuxième professeur du jury
	 * 
	 * @return idProf2
	 */
	public Long getIdProf2() {
		return this.idProf2;
	}

	/**
	 * Mutateur pour modifier l'ID du deuxième professeur du jury
	 * 
	 * @param idProf2
	 */
	public void setIdProf2(Long idProf2) {
		this.idProf2 = idProf2;
	}

	/**
	 * Accessseur pour récupérer l'ID du troisième professeur du jury
	 * 
	 * @return idProf3
	 */
	public Long getIdProf3() {
		return this.idProf3;
	}

	/**
	 * Mutateur pour modifier l'ID du troisième professeur du jury
	 * 
	 * @param idProf3
	 */
	public void setIdProf3(Long idProf3) {
		this.idProf3 = idProf3;
	}

	/**
	 * Accessseur pour récupérer la date et l'heure d'évaluation du poster
	 * 
	 * @return date
	 */
	public Date getDate() {
		return this.date;
	}

	/**
	 * Mutateur pour modifier la date et l'heure d'évaluation du poster
	 * 
	 * @param date
	 */
	public void setDate(Date date) {
		this.date = date;
	}

	/**
	 * Accessseur pour récupérer l'ID de l'équipe dont c'est le poster
	 * 
	 * @return idEquipe
	 */
	public String getIdEquipe() {
		return this.idEquipe;
	}

	/**
	 * Mutateur pour modifier l'ID de l'équipe dont c'est le poster
	 * 
	 * @param idEquipe
	 */
	public void setIdEquipe(String idEquipe) {
		this.idEquipe = idEquipe;
	}

	/**
	 * Redéfinition de la méthode toString().
	 */
	@Override
	public String toString() {
		return "JuryPoster [idJuryPoster=" + this.idJuryPoster + ", idProf1=" + this.idProf1 + ", idProf2="
				+ this.idProf2 + ", idProf3=" + this.idProf3 + ", date=" + this.date + ", idEquipe=" + this.idEquipe
				+ "]";
	}

}