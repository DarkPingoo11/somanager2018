package fr.eseo.ld.beans;

import java.util.Date;

/**
 * Classe du bean AnneeScolaire.
 * 
 * <p>
 * Définition des attributs de l'objet AnneeScolaire ainsi que des accesseurs et
 * des mutateurs.
 * </p>
 * 
 * @author Pierre Cesmat
 */
public class AnneeScolaire {

	/* Attributs */
	private Long idAnneeScolaire;
	private Integer anneeDebut;
	private Integer anneeFin;
	private Date dateDebutProjet;
	private Date dateMiAvancement;
	private Date dateDepotPoster;
	private Date dateSoutenanceFinale;
	private Long idOption;

	/**
	 * Constructeur par défaut.
	 */
	public AnneeScolaire() {
		super();
	}

	/* Accesseurs et Mutateurs */
	/**
	 * Accessseur pour récupérer l'ID de l'année scolaire
	 * 
	 * @return idAnneeScolaire
	 */
	public Long getIdAnneeScolaire() {
		return this.idAnneeScolaire;
	}

	/**
	 * Mutateur pour modifier l'ID de l'année scolaire
	 * 
	 * @param idAnneeScolaire
	 */
	public void setIdAnneeScolaire(Long idAnneeScolaire) {
		this.idAnneeScolaire = idAnneeScolaire;
	}

	/**
	 * Accessseur pour récupérer l'année de début de l'année scolaire
	 * 
	 * @return anneeDebut
	 */
	public Integer getAnneeDebut() {
		return this.anneeDebut;
	}

	/**
	 * Mutateur pour modifier l'année de début de l'année scolaire
	 * 
	 * @param anneeDebut
	 */
	public void setAnneeDebut(Integer anneeDebut) {
		this.anneeDebut = anneeDebut;
	}

	/**
	 * Accessseur pour récupérer l'année de fin de l'année scolaire
	 * 
	 * @return anneeFin
	 */
	public Integer getAnneeFin() {
		return this.anneeFin;
	}

	/**
	 * Mutateur pour modifier l'année de fin de l'année scolaire
	 * 
	 * @param anneeFin
	 */
	public void setAnneeFin(Integer anneeFin) {
		this.anneeFin = anneeFin;
	}

	/**
	 * Accesseur pour récupérer la date du début des projets de PFE
	 * 
	 * @return dateDebutProjet
	 */
	public Date getDateDebutProjet() {
		return this.dateDebutProjet;
	}

	/**
	 * Mutateur pour modifier la date du début des projets de PFE
	 * 
	 * @param dateDebutProjet
	 */
	public void setDateDebutProjet(Date dateDebutProjet) {
		this.dateDebutProjet = dateDebutProjet;
	}

	/**
	 * Accesseur pour récupérer la date de mi-avancement des projets de PFE
	 * 
	 * @return dateMiAvancement
	 */
	public Date getDateMiAvancement() {
		return this.dateMiAvancement;
	}

	/**
	 * Mutateur pour modifier la date de mi-avancement des projets de PFE
	 * 
	 * @param dateMiAvancement
	 */
	public void setDateMiAvancement(Date dateMiAvancement) {
		this.dateMiAvancement = dateMiAvancement;
	}

	/**
	 * Accesseur pour récupérer la date de dépôt limite des posters
	 * 
	 * @return dateDepotPoster
	 */
	public Date getDateDepotPoster() {
		return this.dateDepotPoster;
	}

	/**
	 * Mutateur pour modifier la date de dépôt limite des posters
	 * 
	 * @param dateDepotPoster
	 */
	public void setDateDepotPoster(Date dateDepotPoster) {
		this.dateDepotPoster = dateDepotPoster;
	}

	/**
	 * Accesseur pour récupérer la date limite des soutenances finales
	 * 
	 * @return dateSoutenanceFinale
	 */
	public Date getDateSoutenanceFinale() {
		return this.dateSoutenanceFinale;
	}

	/**
	 * Mutateur pour modifier la date limite des soutenances finales
	 * 
	 * @param dateSoutenanceFinale
	 */
	public void setDateSoutenanceFinale(Date dateSoutenanceFinale) {
		this.dateSoutenanceFinale = dateSoutenanceFinale;
	}

	/**
	 * Accesseur pour récupérer l'ID de l'option que plannifie l'année scolaire
	 * 
	 * @return idOption
	 */
	public Long getIdOption() {
		return this.idOption;
	}

	/**
	 * Mutateur pour modifier l'ID de l'option que plannifie l'année scolaire
	 * 
	 * @param idOption
	 */
	public void setIdOption(Long idOption) {
		this.idOption = idOption;
	}

	/**
	 * Redéfinition de la méthode toString().
	 */
	@Override
	public String toString() {
		return "AnneeScolaire [idAnneeScolaire=" + this.idAnneeScolaire + ", anneeDebut=" + this.anneeDebut
				+ ", anneeFin=" + this.anneeFin + ", dateDebutProjet=" + this.dateDebutProjet + ", dateMiAvancement="
				+ this.dateMiAvancement + ", dateDepotPoster=" + this.dateDepotPoster + ", dateSoutenanceFinale="
				+ this.dateSoutenanceFinale + ", idOption=" + this.idOption + "]";
	}

}