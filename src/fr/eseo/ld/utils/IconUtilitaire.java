package fr.eseo.ld.utils;

import fr.eseo.ld.beans.OptionESEO;
import fr.eseo.ld.beans.Role;

public class IconUtilitaire {

    /**
     * Classe utilitaire non instanciable
     */
    private IconUtilitaire() {
        throw new IllegalStateException("Utility class");
    }

    /**
     * Récupère l'icone pour l'option correspondante
     * @param option OptionESEO
     * @return Icone font-awesome
     */
    public static String getIconForOptionESEO(OptionESEO option) {
        switch(option.getNomOption().toUpperCase()) {
            case("LD"):
                return "desktop";
            case("SE"):
                return "car";
            case("OC"):
                return "microchip";
            case("BIO"):
                return "heartbeat";
            case("NRJ"):
                return "lightbulb";
            case("CC"):
                return "cloud";
            case("IIT"):
                return "sitemap";
            case("DSMT"):
                return "rss";
            case("BD"):
                return "database";
            case("TOUT"):
                return "asterisk";
            default:
                return "question";
        }
    }

    /**
     * Récupère l'icone pour le role correspondant
     * @param role Role
     * @return Icone font-awesome
     */
    public static String getIconForRole(Role role) {
        switch(role.getIdRole().intValue()) {
            case 1:
                return "graduation-cap";
            case 2:
                return "key";
            case 3:
                return "user";
            case 4:
                return "user";
            case 5:
                return "user-secret";
            case 6:
                return "hands-helping";
            case 9:
                return "briefcase";
            case 10:
                return "comments";
            case 11:
                return "balance-scale";
            case 12:
                return "cubes";
            case 13:
                return "sitemap";
            default:
                return "question";
        }
    }
}
