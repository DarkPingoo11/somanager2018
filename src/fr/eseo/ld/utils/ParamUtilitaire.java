package fr.eseo.ld.utils;

import java.util.Calendar;

public class ParamUtilitaire {

    private ParamUtilitaire() {}

    /**
     * Retourne la valeur entière d'une chaine de caractères.
     * Renvoies null si la chaine est vide ou n'est pas un nombre
     * @param str Chaine de caractères
     * @return Un nombre si la chaine est un nombre valide, null sinon
     */
    public static Integer getInt(String str) {
        if(str == null || str.length() == 0) {
            return null;
        }

        Integer val;
        try {
            val = Integer.parseInt(str);
        } catch(NumberFormatException ignored){
            val = null;
        }

        return val;
    }

    /**
     * Retourne la valeur flotante d'une chaine de caractères.
     * Renvoies null si la chaine est vide ou n'est pas un nombre
     * @param str Chaine de caractères
     * @return Un nombre si la chaine est un nombre valide, null sinon
     */
    public static Float getFloat(String str) {
        if(str == null || str.length() == 0) {
            return null;
        }

        Float val;
        try {
            val = Float.parseFloat(str);
        } catch(NumberFormatException ignored){
            val = null;
        }

        return val;
    }

    /**
     * Vérifie si les objets passés en paramètre sont null
     * @param objets Objets a tester
     * @return Vrai si ils ne sont pas null, false sinon
     */
    public static boolean notNull(Object... objets) {
        for(Object item : objets) {
            if(item == null) {
                return false;
            }
            if(item instanceof String && (item.equals("none") || item.equals(""))) {
                return false;
            }
        }
        return true;
    }

    /**
     * Récupère l'année scolaire courante
     * Si on est en 2017-2018, renvoies 2017
     * @return Annee scolaire courante
     */
    public static Integer getAnneeCourante() {
        //récupération de l'année courante
        //Si il se trouve entre Janvier et Aout, l'année scolaire a commencé un an avant
        Calendar calendar = Calendar.getInstance();
        Integer year = calendar.get(Calendar.YEAR);
        return calendar.get(Calendar.MONTH) < 8 ? year - 1 : year;
    }
}
