package fr.eseo.ld.utils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.JsonObject;
import fr.eseo.ld.beans.Callback;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.PrintWriter;

public class CallbackUtilitaire {

	private static final String ATT_CALLBACK_MESSAGE = "callback";

	/**
	 * Classe utilitaire non instanciable
	 */
	private CallbackUtilitaire() {
		throw new IllegalStateException("Utility class");
	}

	/**
	 * Définit le message de retour de la page
	 *
	 * @param request
	 *            Objet request
	 * @param callback
	 *            Objet callback
	 */
	public static void setCallback(HttpServletRequest request, Callback... callback) {
		request.setAttribute(ATT_CALLBACK_MESSAGE, callback);
	}

	/**
	 * Ajoute le/les message(s) de retour de la page
	 *
	 * @param request
	 *            Objet request
	 * @param callback
	 *            Objet callback
	 */
	public static void addCallback(HttpServletRequest request, Callback... callback) {
		if(request.getAttribute(ATT_CALLBACK_MESSAGE) != null) {
			Callback[] existant 	= (Callback[]) request.getAttribute(ATT_CALLBACK_MESSAGE);
			Callback[] newCallback	= new Callback[existant.length+callback.length];

			System.arraycopy(existant, 0, newCallback, 0, existant.length);
			System.arraycopy(callback, 0, newCallback, existant.length, callback.length);

			request.setAttribute(ATT_CALLBACK_MESSAGE, newCallback);
		} else {
			request.setAttribute(ATT_CALLBACK_MESSAGE, callback);
		}
	}

	public static void sendJsonResponse(HttpServletResponse response, Callback callback){
		sendJsonResponse(response, callback.getJsonObject());
	}

	public static void sendJsonResponse(HttpServletResponse response, JsonObject json) {
		//Définition du type de réponse
		response.setContentType("application/json");
		try {
			PrintWriter out = response.getWriter();
			out.print(json.toString());
			out.flush();
		} catch(IOException e) {
			Logger logger = Logger.getLogger(CallbackUtilitaire.class.getName());
			logger.log(Level.WARN, "Erreur lors du renvoi de la réponse JSON : ", e);
		}

	}
}


