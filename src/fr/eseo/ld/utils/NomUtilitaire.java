package fr.eseo.ld.utils;

import fr.eseo.ld.beans.OptionESEO;
import fr.eseo.ld.beans.Role;

public class NomUtilitaire {

    /**
     * Classe utilitaire non instanciable
     */
    private NomUtilitaire() {
        throw new IllegalStateException("Utility class");
    }

    /**
     * Récupère le nom complet pour le role correspondant et l'option associée
     * @param role Role
     * @return Nom complet du rôle
     * (1, 'etudiant'),
     * (2, 'admin'),
     * (3, 'prof'),
     * (4, 'profOption'),
     * (5, 'profResponsable'),
     * (6, 'assistant'),
     * (9, 'entrepriseExt'),
     * (10, 'serviceCom');
     * (11, 'scrumMaster');
     * (12, 'productOwner');
     * (13, 'profReferent');
     */
    public static String getNomCompletRole(Role role, OptionESEO option) {
        String nomOption = option == null ? "" : " " + option.getNomOption();

        switch(role.getIdRole().intValue()) {
            case 1:
                return "Étudiant" + nomOption;
            case 2:
                return "Administrateur";
            case 3:
                return "Professeur" + nomOption;
            case 4:
                return "Professeur d'option" + nomOption;
            case 5:
                return "Professeur responsable" + nomOption;
            case 6:
                return "Assistant";
            case 9:
                return "Entreprise Extérieure";
            case 10:
                return "Service Communication";
            case 11:
                return "Scrum Master";
            case 12:
                return "Product Owner";
            case 13:
                return "Professeur référent";
            default:
                return role.getNomRole();
        }
    }

    /**
     * Récupère le nom complet pour le role correspondant, sans option particulière
     * @param role Role
     * @return Nom complet du rôle
     */
    public static String getNomCompletRole(Role role) {
        return getNomCompletRole(role, null);
    }
}
