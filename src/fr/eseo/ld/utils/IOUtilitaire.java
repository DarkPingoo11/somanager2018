package fr.eseo.ld.utils;

import com.mysql.jdbc.Connection;
import fr.eseo.ld.beans.Poster;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import java.io.*;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class IOUtilitaire {
    private static Logger logger = Logger.getLogger(IOUtilitaire.class.getName());

    private IOUtilitaire() {
    }

    /**
     * Lire un fichier properties et stocket le résultat dans un hashMap
     *
     * @param path Chemin vers le fichier
     * @return Fichier sous forme de hashmap
     */
    public static Map<String, String> lireFichierProperties(String path) {

        Properties prop = new Properties();
        HashMap<String, String> properties = new HashMap<>();

        try (InputStream input = IOUtilitaire.class.getResourceAsStream(path)) {
            prop.load(input);

            for (String pn : prop.stringPropertyNames()) {
                properties.put(pn, prop.getProperty(pn));
            }
        } catch (IOException e) {
            logger.log(Level.WARN, "Impossible de lire le fichier properties", e);
        }
        return properties;
    }

    /**
     * Ecrit dans un fichier Properties
     *
     * @param path       Chemin vers le fichier (chemin absolu /src/< path >...)
     * @param properties Propriétées sous la forme de hashmap
     * @return True si l'écrite est reussie, false sinon
     */
    public static boolean ecrireFichierProperties(String path, Map<String, String> properties) {
        Properties prop = new Properties();

        try (OutputStream output = new FileOutputStream(IOUtilitaire.class.getResource(path).getFile())) {

            for (Map.Entry<String, String> propertie : properties.entrySet()) {
                prop.setProperty(propertie.getKey(), propertie.getValue());
            }

            prop.store(output, null);
        } catch (IOException e) {
            logger.log(Level.WARN, "Impossible d'écrire dans le fichier properties", e);
            return false;
        }

        return true;
    }


    /**
     * Teste la connexion a une base de données
     * @param url jdbc://...
     * @param driver Driver utilisé
     * @param username Nom d'utilisateur pour la BDD
     * @param password Mot de passe
     * @return True si succes, false sinons
     */
    public static boolean testConnexion(String url, String driver, String username, String password) {
        boolean test = false;
        try {
            Class.forName(driver);
        } catch (ClassNotFoundException e) {
            logger.log(Level.WARN, "Erreur de driver", e);
        }

        try (Connection connexion = (Connection) DriverManager.getConnection( url, username, password)){
            test = connexion != null;
        } catch ( SQLException e) {
            logger.log(Level.WARN, "Erreur lors de la connexion.",e);
        }

        return test;
    }

    /**
     * Zip tous les posters d'une liste.
     *
     * @param directory
     *            le dossier zippé.
     * @param posters
     *            la liste des posters.
     * @return byte[] le tableau de données binaires.
     * @throws IOException si erreur
     */
    public static byte[] zipFiles(File directory, List<Poster> posters) throws IOException {
        /* Récupération des noms des posters */
        List<String> nomPosters = new ArrayList<>();
        for (Poster poster : posters) {
            nomPosters.add(getNomPoster(poster));
        }

        /* Zip de chaque poster */
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ZipOutputStream zos = new ZipOutputStream(baos);
        byte[] bytes = new byte[2048];

        for (String nomPoster : nomPosters) {
            FileInputStream fis = new FileInputStream(
                    directory.getPath() + System.getProperty("file.separator") + nomPoster);
            try(BufferedInputStream bis = new BufferedInputStream(fis)){

                zos.putNextEntry(new ZipEntry(nomPoster));
                int bytesRead;
                while ((bytesRead = bis.read(bytes)) != -1) {
                    zos.write(bytes, 0, bytesRead);
                }
                zos.closeEntry();
                fis.close();
            }
        }
        zos.flush();
        baos.flush();
        zos.close();
        baos.close();

        return baos.toByteArray();
    }

    /**
     * Méthode utilitaire qui a pour unique but d'analyser l'en-tête
     * "content-disposition", et de vérifier si le paramètre "filename" y est
     * présent. Si oui, alors le champ traité est de type File et la méthode
     * retourne son nom, sinon il s'agit d'un champ de formulaire classique et la
     * méthode retourne null.
     *
     * @param poster le poster dont on souhaite récupérer le nom.
     * @return nomPoster le nom du poster.
     */
    private static String getNomPoster(Poster poster) {
        String nomPoster = poster.getChemin();
        nomPoster = nomPoster.substring(nomPoster.lastIndexOf('/') + 1).substring(nomPoster.lastIndexOf('\\') + 1);
        return nomPoster;
    }


}