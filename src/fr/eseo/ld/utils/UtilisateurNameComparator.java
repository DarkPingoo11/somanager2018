package fr.eseo.ld.utils;
import fr.eseo.ld.beans.Utilisateur;

import java.util.Comparator;

public class UtilisateurNameComparator implements Comparator<Utilisateur>{


	@Override
	/**
	 * Compare tous les utilsateurs pour determiner l'ordre (triés par noms, ordre alpha)
	 * 
	 * @return un entier négatif, un zéro ou un entier positif car le premier argument est inférieur, égal ou supérieur au second..
	 */
	public int compare(Utilisateur u1, Utilisateur u2) {
		return u1.getNom().compareTo(u2.getNom());
	}

}
