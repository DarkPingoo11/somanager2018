package fr.eseo.ld.filters;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Filtre permettant la restriction de l'accès à certaines pages lorsque
 * l'utilisateur n'est pas connecté.
 */
public class ConnexionRestriction implements Filter {

	private static final String VUE_CONNEXION = "/WEB-INF/formulaires/connexion.jsp";

	@Override
	public void init(FilterConfig fConfig) throws ServletException {
		// Rien à réaliser
	}

	@Override
	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain)
			throws IOException, ServletException {
		/* Cast des objets request et response */
		HttpServletRequest request = (HttpServletRequest) req;
		HttpServletResponse response = (HttpServletResponse) res;

		/* Récupération de la session depuis la requête */
		HttpSession session = request.getSession();

		/* Restriction ou non */
		if (session.getAttribute(FilterUtilitaire.ATT_SESSION_USER) == null) { // si l'utilisateur n'est pas connecté
			/* Redirection vers la page de connexion */
			request.getRequestDispatcher(VUE_CONNEXION).forward(request, response);
		} else {
			/* Affichage de la page restreinte */
			chain.doFilter(request, response);
		}
	}

	@Override
	public void destroy() {
		// Rien à réaliser
	}

}