package fr.eseo.ld.filters;

import java.io.IOException;
import java.util.List;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.eseo.ld.beans.Role;
import fr.eseo.ld.beans.Utilisateur;
import fr.eseo.ld.dao.DAOFactory;
import fr.eseo.ld.dao.NotificationDAO;

/**
 * Classe Utilitaire pour les filters.
 * 
 * <p>
 * Cette classe centralise des attributs et des méthodes utilisés par les
 * filters.
 * </p>
 * 
 * @author Maxime LENORMAND
 */
public final class FilterUtilitaire {

	protected static final String CONF_DAO_FACTORY = "daoFactory";

	protected static final String ATT_SESSION_USER = "utilisateur";
	protected static final String ATT_SESSION_ROLES = "roles";
	protected static final String ATT_SESSION_ETUDIANT_EQUIPE = "etudiantEquipe";
	protected static final String ATT_SESSION_PROFESSEUR_SUJET = "professeurSujet";

	protected static final String ROLE_ETUDIANT = "etudiant";
	protected static final String ROLE_ASSISTANT = "assistant";
	protected static final String ROLE_PROF = "prof";
	protected static final String ROLE_PROF_OPTION = "profOption";
	protected static final String ROLE_PROF_RESPONSABLE = "profResponsable";
	protected static final String ROLE_ENTREPRISE_EXT = "entrepriseExt";
	protected static final String ROLE_ADMIN = "admin";
	protected static final String ROLE_PRODUCT_OWNER = "productOwner";
	protected static final String ROLE_PROF_REFERENT = "profReferent";

	protected static final String VUE_INDEX = "/Dashboard";

	/**
	 * Constructeur caché par défaut (car c'est une classe finale utilitaire,
	 * contenant uniquement des méthodes appelées de manière statique).
	 */
	private FilterUtilitaire() {

	}

	/**
	 * Initialise un filtre.
	 * 
	 * @param fConfig
	 *            la configuration initiale du filtre.
	 * @return notificationDAO le DAO de Notification.
	 */
	protected static NotificationDAO initialiserFiltre(FilterConfig fConfig) {
		/* Récupération d'instances de DAO */
		DAOFactory daoFactory = (DAOFactory) fConfig.getServletContext()
				.getAttribute(FilterUtilitaire.CONF_DAO_FACTORY);
		return daoFactory != null ? daoFactory.getNotificationDao() : null;
	}

	/**
	 * Restreint l'accès à la page en fonction des rôles de l'utilisateur.
	 * 
	 * @param req
	 *            le ServletRequest.
	 * @param res
	 *            le ServletResponse.
	 * @param chain
	 *            le FilterChain.
	 * @param rolesAutorises
	 *            la liste des rôles autorisés à accéder à la page restreinte.
	 * @param notificationDAO
	 *            le DAO de l'objet Notification permettant une notification
	 *            d'erreur.
	 * @throws IOException
	 * @throws ServletException
	 */
	@SuppressWarnings("unchecked")
	protected static void restreindreAccesRoles(ServletRequest req, ServletResponse res, FilterChain chain,
			List<String> rolesAutorises, NotificationDAO notificationDAO) throws IOException, ServletException {
		/* Cast des objets request et response */
		HttpServletRequest request = (HttpServletRequest) req;
		HttpServletResponse response = (HttpServletResponse) res;

		/* Récupération de la session depuis la requête */
		HttpSession session = request.getSession();

		/* Récupération des rôles en session */
		List<Role> rolesUtilisateur = (List<Role>) session.getAttribute(ATT_SESSION_ROLES);

		/* Restriction ou non */
		if (!possedeBonsRoles(rolesUtilisateur, rolesAutorises)) {
			/* Redirection vers la page d'accueil */
			response.sendRedirect(request.getContextPath() + VUE_INDEX);
			/* Récupération de l'ID de l'Utilisateur en session */
			Utilisateur utilisateur = (Utilisateur) session.getAttribute(ATT_SESSION_USER);
			Long idUtilisateur = utilisateur.getIdUtilisateur();
			/* Ajout d'une notification */
			notificationDAO.ajouterNotification(idUtilisateur,
					"Vous ne possédez pas les droits suffisants pour accéder à cette page !");
		} else {
			/* Affichage de la page restreinte */
			chain.doFilter(request, response);
		}
	}

	/**
	 * Vérifie que l'utilisateur possède les bons roles pour accéder à la page
	 * restreinte.
	 * 
	 * @param rolesUtilisateur
	 *            la liste des rôles de l'utilisateur.
	 * @param rolesAutorises
	 *            la liste des rôles autorisés à accéder à la page restreinte.
	 * @return un booléen indiquant si l'utilisateur possède ou non les bons roles.
	 */
	private static boolean possedeBonsRoles(List<Role> rolesUtilisateur, List<String> rolesAutorises) {
		if (rolesUtilisateur != null) {
			for (Role role : rolesUtilisateur) {
				if (rolesAutorises.contains(role.getNomRole())) {
					return true;
				}
			}
		}
		return false;
	}

}