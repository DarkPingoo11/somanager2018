package fr.eseo.ld.filters;

import static fr.eseo.ld.filters.FilterUtilitaire.initialiserFiltre;
import static fr.eseo.ld.filters.FilterUtilitaire.restreindreAccesRoles;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.eseo.ld.beans.Utilisateur;
import fr.eseo.ld.dao.NotificationDAO;

/**
 * Filtre permettant la restriction de l'accès à la page CreerEquipe en fonction
 * des rôles de l'utilisateur.
 */
public class CreerEquipeRestriction implements Filter {

	private NotificationDAO notificationDAO;

	@Override
	public void init(FilterConfig fConfig) throws ServletException {
		/* Récupération d'instances de DAO */
		this.notificationDAO = initialiserFiltre(fConfig);
	}

	@Override
	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain)
			throws IOException, ServletException {
		/* Cast des objets request et response */
		HttpServletRequest request = (HttpServletRequest) req;
		HttpServletResponse response = (HttpServletResponse) res;

		/* Récupération de l'attribut etudiantEquipe en session */
		HttpSession session = request.getSession();
		boolean dansEquipe = (boolean) session.getAttribute(FilterUtilitaire.ATT_SESSION_ETUDIANT_EQUIPE);
		if (dansEquipe) {
			/* Redirection vers la page d'accueil */
			response.sendRedirect(request.getContextPath() + FilterUtilitaire.VUE_INDEX);
			/* Récupération de l'ID de l'Utilisateur en session */
			Utilisateur utilisateur = (Utilisateur) session.getAttribute(FilterUtilitaire.ATT_SESSION_USER);
			Long idUtilisateur = utilisateur.getIdUtilisateur();
			/* Ajout d'une notification */
			this.notificationDAO.ajouterNotification(idUtilisateur,
					"Vous appartenez déjà une équipe et ne pouvez plus en créer une autre !");
		} else {
			/* Liste des rôles autorisés à accéder à la page restreinte */
			List<String> rolesAutorises = new ArrayList<>();
			rolesAutorises.add(FilterUtilitaire.ROLE_ETUDIANT);
			rolesAutorises.add(FilterUtilitaire.ROLE_PROF_RESPONSABLE);
			/* Restreint l'accès à la page en fonction des rôles de l'utilisateur */
			restreindreAccesRoles(req, res, chain, rolesAutorises, this.notificationDAO);
		}
	}

	@Override
	public void destroy() {
		// Rien à réaliser
	}

}