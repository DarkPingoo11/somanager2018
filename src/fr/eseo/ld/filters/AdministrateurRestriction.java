package fr.eseo.ld.filters;

import static fr.eseo.ld.filters.FilterUtilitaire.initialiserFiltre;
import static fr.eseo.ld.filters.FilterUtilitaire.restreindreAccesRoles;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import fr.eseo.ld.dao.NotificationDAO;

/**
 * Filtre permettant la restriction de l'accès aux pages réservées à
 * l'Administrateur.
 */
public class AdministrateurRestriction implements Filter {

	private NotificationDAO notificationDAO;

	@Override
	public void init(FilterConfig fConfig) throws ServletException {
		/* Récupération d'instances de DAO */
		this.notificationDAO = initialiserFiltre(fConfig);
	}

	@Override
	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain)
			throws IOException, ServletException {
		/* Liste des rôles autorisés à accéder à la page restreinte */
		List<String> rolesAutorises = new ArrayList<>();
		rolesAutorises.add(FilterUtilitaire.ROLE_ADMIN);

		/* Restreint l'accès à la page en fonction des rôles de l'utilisateur */
		restreindreAccesRoles(req, res, chain, rolesAutorises, this.notificationDAO);
	}

	@Override
	public void destroy() {
		// Rien à réaliser
	}

}