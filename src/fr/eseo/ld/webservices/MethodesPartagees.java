package fr.eseo.ld.webservices;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jws.WebService;

import org.mindrot.jbcrypt.BCrypt;

import fr.eseo.ld.beans.Equipe;
import fr.eseo.ld.beans.Etudiant;
import fr.eseo.ld.beans.EtudiantEquipe;
import fr.eseo.ld.beans.JuryPoster;
import fr.eseo.ld.beans.JurySoutenance;
import fr.eseo.ld.beans.NotePoster;
import fr.eseo.ld.beans.Poster;
import fr.eseo.ld.beans.Professeur;
import fr.eseo.ld.beans.Sujet;
import fr.eseo.ld.beans.Utilisateur;
import fr.eseo.ld.dao.DAOFactory;
import fr.eseo.ld.dao.EquipeDAO;
import fr.eseo.ld.dao.EtudiantDAO;
import fr.eseo.ld.dao.JuryPosterDAO;
import fr.eseo.ld.dao.JurySoutenanceDAO;
import fr.eseo.ld.dao.NotePosterDAO;
import fr.eseo.ld.dao.PosterDAO;
import fr.eseo.ld.dao.SujetDAO;
import fr.eseo.ld.dao.UtilisateurDAO;
import fr.eseo.ld.ldap.AuthentificationAD;

/**
 * Classe implémentant les WebServices concernant les Sujet, mises à disposition
 * avec l'application SoManager *
 * 
 * @author Thomas MENARD
 *
 */
@WebService(targetNamespace = "http://webservices.ld.eseo.fr/", endpointInterface = "fr.eseo.ld.webservices.MethodesPartageesSEI", portName = "MethodesPartageesPort", serviceName = "MethodesPartageesService")
public class MethodesPartagees implements MethodesPartageesSEI {

	// Variables utilisées pour l'authentification
	protected static final String CHAMP_MOT_DE_PASSE = "motDePasse";
	private static final String ERREUR_SAISIE = "Identifiant ou Mot de passe incorrect.";

	DAOFactory daoFactory = DAOFactory.getInstance();

	// Instanciation de SujetDAO
	SujetDAO sujetDAO = daoFactory.getSujetDao();

	// Instanciation de SujetDAO
	UtilisateurDAO utilisateurDAO = daoFactory.getUtilisateurDao();

	// Instanciation de JuryDAO
	JuryPosterDAO juryPosterDao = daoFactory.getJuryPosterDAO();

	// Instanciation de JuryDAO
	JurySoutenanceDAO jurySoutenanceDao = daoFactory.getJurySoutenanceDAO();

	// Instanciation de PosterDAO
	PosterDAO posterDAO = daoFactory.getPosterDao();

	// Instanciation de EquipDAO
	EquipeDAO equipeDAO = daoFactory.getEquipeDAO();

	// Instanciation de EtudiantDAO
	EtudiantDAO etudiantDAO = daoFactory.getEtudiantDao();

	// Instanciation de NotePosterDAO
	NotePosterDAO notePosterDAO = daoFactory.getNotePosterDao();

	/**
	 * Permet d'authentifier un utilisateur sur l'application
	 * 
	 * @param identifiant
	 *            de l'utilisateur
	 * @param motDePasse
	 *            de l'utilisateur
	 * @return une map de erreurs d'authentification. Si celle-ci est vide,
	 *         l'authentification est validée. Si celle ci est
	 */
	public Map<String, String> authentificationUtilisateur(String identifiant, String motDePasse) {

		/*
		 * Utilisation d'UtilisateurDAO pour chercher un utilisateur via son identifiant
		 */
		Utilisateur utilisateur = new Utilisateur();
		utilisateur.setIdentifiant(identifiant);
		List<Utilisateur> utilisateurs = this.utilisateurDAO.trouver(utilisateur);

		Map<String, String> erreurs = new HashMap<>();

		if (utilisateurs.isEmpty()) { // si aucun utilisateur ne possède cet identifiant
			erreurs.put(CHAMP_MOT_DE_PASSE, ERREUR_SAISIE);

			// Si l'utilisateur appartient à l'AD
		} else if ("activeDirectory".equals(utilisateurs.get(0).getHash())) {
			AuthentificationAD autentification = new AuthentificationAD();
			String nomCommun = autentification.recupererNomCommun(identifiant);
			if (autentification.connexionAD(nomCommun, motDePasse) == null) {
				erreurs.put(CHAMP_MOT_DE_PASSE, ERREUR_SAISIE);
			}
			if ("non".equals(utilisateurs.get(0).getValide())) {
				erreurs.put(CHAMP_MOT_DE_PASSE, "Compte non validé par l'administrateur.");
			}

			// Si l'utilisateur n'appartient pas à l'AD
		} else {
			if (!BCrypt.checkpw(motDePasse, utilisateurs.get(0).getHash())) {
				erreurs.put(CHAMP_MOT_DE_PASSE, ERREUR_SAISIE);
			}
			if ("non".equals(utilisateurs.get(0).getValide())) {
				erreurs.put(CHAMP_MOT_DE_PASSE, "Compte non validé par l'administrateur.");
			}
		}
		return erreurs;
	}

	/**
	 * Retourne la liste de tous les beans des sujets présents dans la base de
	 * données
	 * 
	 * @return une List<Sujet> de tout les sujets
	 */
	public List<Sujet> listerSujets() {
		return sujetDAO.lister();
	}

	/**
	 * Liste les sujets auquels un utilisateur (obligatoirement professeur) est
	 * associé avec un jury de poster
	 * 
	 * @param identifiant
	 *            du compte utilisé
	 * @param mot
	 *            de passe du compte utilisé
	 * @return la liste des sujets associés à l'utilisateur avec un jury de poster
	 */
	public List<Sujet> listerSujetJurysPosterUtilisateur(String identifiant, String motDePasse) {
		List<Sujet> listeSujets = Collections.emptyList();
		if (authentificationUtilisateur(identifiant, motDePasse).isEmpty()) {
			Utilisateur utilisateur = new Utilisateur();
			utilisateur.setIdentifiant(identifiant);
			utilisateur = this.utilisateurDAO.trouver(utilisateur).get(0);
			listeSujets = sujetDAO.trouverSujetsJuryPoster(utilisateur);
		}
		return listeSujets;
	}

	/**
	 * Liste les sujets auquel un utilisateur (obligatoirement professeur) est
	 * associé avec un jury de soutenance
	 * 
	 * @param identifiant
	 *            du compte utilisé
	 * @param mot
	 *            de passe du compte utilisé
	 * @return la liste des jurys associés à l'utilisateur
	 */
	public List<Sujet> listerSujetJurysSoutenanceUtilisateur(String identifiant, String motDePasse) {
		List<Sujet> listeSujets = Collections.emptyList();
		if (authentificationUtilisateur(identifiant, motDePasse).isEmpty()) {
			Utilisateur utilisateur = new Utilisateur();
			utilisateur.setIdentifiant(identifiant);
			utilisateur = this.utilisateurDAO.trouver(utilisateur).get(0);
			return sujetDAO.trouverSujetsJurySoutenance(utilisateur);
		}
		return listeSujets;
	}

	/**
	 * Liste les jurys poster auquels un utilisateur (obligatoirement professeur)
	 * est associé
	 * 
	 * @param identifiant
	 *            du compte utilisé
	 * @param mot
	 *            de passe du compte utilisé
	 * @return la liste des jurys associés à l'utilisateur
	 */

	public List<JuryPoster> listerJurysPosterUtilisateur(String identifiant, String motDePasse) {
		List<JuryPoster> listeJuryPoster = Collections.emptyList();
		if (authentificationUtilisateur(identifiant, motDePasse).isEmpty()) {
			Utilisateur utilisateur = new Utilisateur();
			utilisateur.setIdentifiant(identifiant);
			utilisateur = this.utilisateurDAO.trouver(utilisateur).get(0);
			listeJuryPoster = juryPosterDao.listerJurysUtilisateur(utilisateur);
		}
		return listeJuryPoster;
	}

	/**
	 * Liste les jurys soutenance auquels un utilisateur (obligatoirement
	 * professeur) est associé
	 * 
	 * @param identifiant
	 *            du compte utilisé
	 * @param mot
	 *            de passe du compte utilisé
	 * @return la liste des jurys associés à l'utilisateur
	 */
	public List<JurySoutenance> listerJurysSoutenanceUtilisateur(String identifiant, String motDePasse) {
		List<JurySoutenance> listeJurySoutenance = Collections.emptyList();
		if (authentificationUtilisateur(identifiant, motDePasse).isEmpty()) {
			Utilisateur utilisateur = new Utilisateur();
			utilisateur.setIdentifiant(identifiant);
			utilisateur = this.utilisateurDAO.trouver(utilisateur).get(0);
			listeJurySoutenance = jurySoutenanceDao.listerJurysUtilisateur(utilisateur);
		}
		return listeJurySoutenance;
	}

	/**
	 * Liste les posters auquel un professeur est affilié avec un jury poster
	 * 
	 * @param identifiant
	 * @param motDePasse
	 * @return la liste des posters
	 */
	public List<Poster> listerPosters(String identifiant, String motDePasse) {
		List<Poster> listePosters = new ArrayList<>();
		if (authentificationUtilisateur(identifiant, motDePasse).isEmpty()) {
			Utilisateur utilisateur = new Utilisateur();
			utilisateur.setIdentifiant(identifiant);
			/*
			 * On récupère la liste des jurys de Poster auquels l'utilisateur est affillié
			 */
			List<JuryPoster> listeJurysUtilisateur = listerJurysPosterUtilisateur(identifiant, motDePasse);
			/* Pour chaque jury, on récupère le poster associé */
			for (JuryPoster jury : listeJurysUtilisateur) {
				String idEquipe = jury.getIdEquipe();
				Equipe equipe = new Equipe();
				equipe.setIdEquipe(idEquipe);
				equipe = equipeDAO.trouver(equipe).get(0);
				Long idSujet = equipe.getIdSujet();
				Poster poster = posterDAO.trouverPoster(idSujet);
				if (poster != null) {
					listePosters.add(poster);
				}
			}
		}
		return listePosters;
	}

	/**
	 * Permet de noter un etudiant sur son poster
	 * 
	 * @param identifiant
	 *            de l'utilisateur professeur
	 * @param motDePasse
	 *            de l'utilisateur professeur
	 * @param posterEvalue,
	 *            bean du poster à évalué
	 * @param note,
	 *            note à attribuer aux etudiants concernés par le poster
	 */
	public boolean noterPoster(String identifiant, String motDePasse, Poster posterEvalue, Etudiant etudiant,
			float note) {
		if (authentificationUtilisateur(identifiant, motDePasse).isEmpty()) {

			/*
			 * On vérifie d'abord que le poster rentré fait partie du JuryPoster du
			 * professeur
			 */
			List<Poster> listePoster = listerPosters(identifiant, motDePasse);
			for (Poster poster : listePoster) {
				if (posterEvalue.getIdPoster().equals(poster.getIdPoster())) {

					/*
					 * On récupère l'ID de l'utilisateur (professeur) connecté
					 */
					Utilisateur utilisateur = new Utilisateur();
					utilisateur.setIdentifiant(identifiant);
					List<Utilisateur> utilisateurs = this.utilisateurDAO.trouver(utilisateur);

					/* On crée le bean du professeur qui veut noter */
					Professeur prof = new Professeur();
					prof.setIdProfesseur(utilisateurs.get(0).getIdUtilisateur());

					/*
					 * On cherche si l'étudiant que veut noter l'utilisateur fait partie de l'équipe
					 */
					Equipe equipe = new Equipe();
					equipe.setIdSujet(poster.getIdSujet());
					equipe = this.equipeDAO.trouver(equipe).get(0);
					EtudiantEquipe etudiantEquipe = new EtudiantEquipe();
					etudiantEquipe.setIdEquipe(equipe.getIdEquipe());
					List<EtudiantEquipe> listeEtudiantEquipe = this.equipeDAO.trouverEtudiantEquipe(etudiantEquipe);
					if (verificationEtudiantEquipe(listeEtudiantEquipe, etudiant)) {
						return attribuerNote(etudiant, prof, poster, note);
					}
				}
			}

		}
		return false;
	}

	/**
	 * Méthode privée permettant d'attribuer une note pour un étudiant dans la table
	 * NotePoster
	 * 
	 * @param etudiant,
	 *            l'étudiant à noter
	 * @param note
	 *            de l'étudiant
	 */

	private boolean attribuerNote(Etudiant etudiant, Professeur professeur, Poster poster, float note) {
		NotePoster notePoster = new NotePoster();
		notePoster.setIdEtudiant(etudiant.getIdEtudiant());
		notePoster.setIdProfesseur(professeur.getIdProfesseur());
		notePoster.setIdPoster(poster.getIdPoster());

		if (notePosterDAO.trouver(notePoster).isEmpty()) {
			notePoster.setNote(note);
			notePosterDAO.creer(notePoster);
		} else {
			notePoster.setNote(note);
			notePosterDAO.modifier(notePoster);
		}
		return true;

	}

	/**
	 * Méthode privée permettant de vérifier si l'étudiant mis en paramètre
	 * appartient bien à l'équipe représentée par le liste des EtudiantEquipe
	 * 
	 * @param listeEtudiantEquipe
	 *            : la liste des étudiants de l'équipe représentée par le bean
	 *            EtudiantEquipe
	 * @param etudiant
	 *            : l'étudiant ou l'on veut vérifier son appartenance
	 * @return un boolean, en fonction de l'appartenance de l'étudiant
	 */

	boolean verificationEtudiantEquipe(List<EtudiantEquipe> listeEtudiantEquipe, Etudiant etudiant) {
		for (EtudiantEquipe etudiantEquipe : listeEtudiantEquipe) {
			if (etudiantEquipe.getIdEtudiant().equals(etudiant.getIdEtudiant())) {
				return true;
			}
		}
		return false;
	}

}
