package fr.eseo.ld.webservices;

import java.util.List;
import java.util.Map;

import javax.jws.WebService;

import fr.eseo.ld.beans.Etudiant;
import fr.eseo.ld.beans.JuryPoster;
import fr.eseo.ld.beans.JurySoutenance;
import fr.eseo.ld.beans.Poster;
import fr.eseo.ld.beans.Sujet;

@WebService(name = "MethodesPartageesSEI", targetNamespace = "http://webservices.ld.eseo.fr/")
public interface MethodesPartageesSEI {

	/**
	 * Permet d'authentifier un utilisateur sur l'application
	 * 
	 * @param identifiant
	 *            de l'utilisateur
	 * @param motDePasse
	 *            de l'utilisateur
	 * @return une map de erreurs d'authentification. Si celle-ci est vide,
	 *         l'authentification est validée. Si celle ci est
	 */
	Map<String, String> authentificationUtilisateur(String identifiant, String motDePasse);

	/**
	 * Retourne la liste de tous les beans des sujets présents dans la base de
	 * données
	 * 
	 * @return une List<Sujet> de tout les sujets
	 */
	List<Sujet> listerSujets();

	/**
	 * Liste les sujets auquels un utilisateur (obligatoirement professeur) est
	 * associé avec un jury de poster
	 * 
	 * @param identifiant
	 *            du compte utilisé
	 * @param mot
	 *            de passe du compte utilisé
	 * @return la liste des sujets associés à l'utilisateur avec un jury de poster
	 */
	List<Sujet> listerSujetJurysPosterUtilisateur(String identifiant, String motDePasse);

	/**
	 * Liste les sujets auquel un utilisateur (obligatoirement professeur) est
	 * associé avec un jury de soutenance
	 * 
	 * @param identifiant
	 *            du compte utilisé
	 * @param mot
	 *            de passe du compte utilisé
	 * @return la liste des jurys associés à l'utilisateur
	 */
	List<Sujet> listerSujetJurysSoutenanceUtilisateur(String identifiant, String motDePasse);

	/**
	 * Liste les jurys poster auquels un utilisateur (obligatoirement professeur)
	 * est associé
	 * 
	 * @param identifiant
	 *            du compte utilisé
	 * @param mot
	 *            de passe du compte utilisé
	 * @return la liste des jurys associés à l'utilisateur
	 */

	List<JuryPoster> listerJurysPosterUtilisateur(String identifiant, String motDePasse);

	/**
	 * Liste les jurys soutenance auquels un utilisateur (obligatoirement
	 * professeur) est associé
	 * 
	 * @param identifiant
	 *            du compte utilisé
	 * @param mot
	 *            de passe du compte utilisé
	 * @return la liste des jurys associés à l'utilisateur
	 */
	List<JurySoutenance> listerJurysSoutenanceUtilisateur(String identifiant, String motDePasse);

	/**
	 * Liste les posters auquel un professeur est affilié avec un jury poster
	 * 
	 * @param identifiant
	 * @param motDePasse
	 * @return la liste des posters
	 */
	List<Poster> listerPosters(String identifiant, String motDePasse);

	/**
	 * Permet de noter un etudiant sur son poster
	 * 
	 * @param identifiant
	 *            de l'utilisateur professeur
	 * @param motDePasse
	 *            de l'utilisateur professeur
	 * @param posterEvalue,
	 *            bean du poster à évalué
	 * @param note,
	 *            note à attribuer aux etudiants concernés par le poster
	 */
	boolean noterPoster(String identifiant, String motDePasse, Poster posterEvalue, Etudiant etudiant, float note);

}