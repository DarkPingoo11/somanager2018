package fr.eseo.ld.ldap;

import java.util.Properties;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.naming.ldap.InitialLdapContext;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

/**
 * Classe permettant l'authentification sur le serveur LDAP de l'école.
 * 
 * @author Thomas MENARD
 */
public class AuthentificationAD extends LDAP {

	private static Logger logger = Logger.getLogger(AuthentificationAD.class.getName());

	/**
	 * Initialise les paramètres du serveur LDAP {@link #LDAP()}.
	 */
	public AuthentificationAD() {
		super();
	}

	/**
	 * Vérifie l'existence de l'id de l'utilisateur rentré en paramètre (dans le
	 * filtre)
	 * 
	 * @param contexte
	 *            la connexion initialement créée avec la méthode
	 *            {@link #initialiseConnexion}.
	 * @param filtre
	 *            le filtre exprimant ce que l'on souhaite chercher.
	 * @param attribut
	 *            le nom de l'attribut que l'on veut en retour, ici le "cn".
	 * @return nom complet de l'utilisateur.
	 */
	private String rechercherID(InitialLdapContext contexte, String filtre, String attribut) {
		/* Création des variables utilisées lors de la recherche */
		SearchControls searchControls = new SearchControls();
		searchControls.setSearchScope(SearchControls.SUBTREE_SCOPE);
		NamingEnumeration<SearchResult> resultatRecherche;
		try {
			/* Lancement de la recherche dans l'AD spécifié */
			resultatRecherche = contexte.search(this.contexteDN, filtre, searchControls);

			/* Traitement des résultats et recherche spécifique sur l'attribut <attribut> */
			while (resultatRecherche.hasMoreElements()) {
				SearchResult sr = resultatRecherche.next();
				Attributes attributs = sr.getAttributes();
				Attribute a = attributs.get(attribut);
				if (a != null) {
					return a.get().toString();
				}
			}
		} catch (Exception e) {
			logger.log(Level.WARN, "Erreur lors de la recherche de l'ID sur le serveur LDAP " + e.getMessage(), e);
		}
		return null;
	}

	/**
	 * Renvoie le nom complet de l'utilisateur rentré en paramètre si celui ci
	 * existe. Null sinon.
	 * 
	 * @param identifiant
	 *            l'identifiant qu'utilise l'utilisateur quand il se connecte.
	 *            Exemple : menardth
	 * @return resultat un string contenant le nom complet de l'utilisateur. Exemple
	 *         : thomas menard
	 */
	public String recupererNomCommun(String identifiant) {
		String resultat;
		/* Création du contexte */
		InitialLdapContext contexte = initialiseConnexion();

		/*
		 * Recherche de l'identifiant rentré en paramètre, le resultat renvoie le
		 * nomComplet de l'utilisateur
		 */
		resultat = rechercherID(contexte, "(&(objectclass=person)(uid=" + identifiant + "))", "cn");
		try {
			contexte.close();
		} catch (Exception e) {
			logger.log(Level.WARN, "Erreur lors de la recherche du nomCommun sur le serveur LDAP" + e.getMessage(), e);
		}
		return resultat;
	}

	/**
	 * Authentifie l'utilisateur sur le LDAP et initialise le contexte de la
	 * connexion.
	 * 
	 * @param cn
	 *            le CommonName de l'utilisateur sur le serveur LDAP.
	 * @param motDePasse
	 *            le mot de passe de l'utilisateur sur le LDAP.
	 * @return le contexte de la connexion. Si celui-ci est nul, la connexion a
	 *         échoué.
	 */
	public InitialLdapContext connexionAD(String cn, String motDePasse) {
		String serverLogin = "cn=" + cn + "," + this.contexteDNUtilisateur;
		// on remplit un tableau avec les parametres d'environnement et de connexion au
		// LDAP
		Properties propriete = new Properties();
		propriete.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
		propriete.put(Context.PROVIDER_URL, "ldap://" + this.serveur + ":" + this.port + "/");
		propriete.put(Context.SECURITY_AUTHENTICATION, "simple");
		propriete.put(Context.SECURITY_PRINCIPAL, serverLogin);
		propriete.put(Context.SECURITY_CREDENTIALS, motDePasse);

		InitialLdapContext contexte = null;
		// si la creation du contexte génère une erreur alors les identifiants
		// sont faux et donc la connexion n'est pas autorisée
		try {
			contexte = new InitialLdapContext(propriete, null);
			contexte.close();
		} catch (NamingException e) {
			logger.log(Level.WARN, "Erreur lors de la connexion.", e);
		}
		return contexte;
	}

}