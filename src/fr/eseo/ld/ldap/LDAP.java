package fr.eseo.ld.ldap;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.naming.Context;
import javax.naming.NamingException;
import javax.naming.ldap.InitialLdapContext;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

/**
 * Classe mère LDAP regroupant les méthodes et attributs utilisés dans les
 * classes filles.
 * 
 * On y retrouve le chargement des paramètres du LDAP contenus dans les fichiers
 * de propriété. On peut aussi tester la connexion et renvoyer la liste des
 * attributs du LDAP.
 * 
 * @author Thomas MENARD
 */
public class LDAP {

	protected static final String FICHIER_PROPERTIES = "fr/eseo/ld/ldap/annuaire.properties";
	protected static final String FICHIER_PROPERTIES_DEFAUT = "fr/eseo/ld/ldap/annuaireDefaut.properties";
	protected static final String PROPERTY_CONTEXTE_DN = "CONTEXTE_DN";
	protected static final String PROPERTY_CONTEXTE_DN_UTILISATEUR = "CONTEXTE_DN_UTILISATEUR";
	protected static final String PROPERTY_SERVEUR = "SERVEUR";
	protected static final String PROPERTY_PORT = "PORT";
	protected static final String PROPERTY_NOM_ID = "NOM_ID";
	protected static final String PROPERTY_PRENOM_ID = "PRENOM_ID";
	protected static final String PROPERTY_MAIL_ID = "MAIL_ID";
	protected static final String PROPERTY_ID_ID = "ID_ID";
	protected static final String PROPERTY_CN_UTILISATEUR = "CN_UTILISATEUR";
	protected static final String PROPERTY_MDP = "MDP_UTILISATEUR";
	protected static final String PROPERTY_CN_ADMIN = "CN_ADMIN";
	protected static final String PROPERTY_MDP_ADMIN = "MDP_ADMIN";

	protected String contexteDN;
	protected String serveur;
	protected String port;
	protected String nomID;
	protected String prenomID;
	protected String mailID;
	protected String idID;
	protected String contexteDNUtilisateur;
	protected String cnUtilisateur;
	protected String motDePasseUtilisateur;
	protected String cnAdmin;
	protected String mdpAdmin;

	private static Logger logger = Logger.getLogger(LDAP.class.getName());

	/**
	 * Constructeur du LDAP.
	 */
	public LDAP() {
		List<String> liste = lireFichierPropriete(FICHIER_PROPERTIES);
		this.contexteDN = liste.get(0);
		this.serveur = liste.get(1);
		this.port = liste.get(2);
		this.nomID = liste.get(3);
		this.prenomID = liste.get(4);
		this.mailID = liste.get(5);
		this.idID = liste.get(6);
		this.contexteDNUtilisateur = liste.get(7);
		this.cnUtilisateur = liste.get(8);
		this.motDePasseUtilisateur = liste.get(9);
		this.cnAdmin = liste.get(10);
		this.mdpAdmin = liste.get(11);
	}

	/**
	 * Lit le fichier de propriété (annuaire.properties).
	 * 
	 * @param fichierURL
	 *            l'URL du fichier.
	 * @return listeAttributs la liste comportant les attributs contenus dans le
	 *         fichier de propriété.
	 */
	private List<String> lireFichierPropriete(String fichierURL) {
		List<String> listeAttributs = new ArrayList<>();
		Properties properties = new Properties();
		ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
		InputStream fichierProperties = classLoader.getResourceAsStream(fichierURL);
		try {
			properties.load(fichierProperties);
			listeAttributs.add(properties.getProperty(PROPERTY_CONTEXTE_DN));
			listeAttributs.add(properties.getProperty(PROPERTY_SERVEUR));
			listeAttributs.add(properties.getProperty(PROPERTY_PORT));
			listeAttributs.add(properties.getProperty(PROPERTY_NOM_ID));
			listeAttributs.add(properties.getProperty(PROPERTY_PRENOM_ID));
			listeAttributs.add(properties.getProperty(PROPERTY_MAIL_ID));
			listeAttributs.add(properties.getProperty(PROPERTY_ID_ID));
			listeAttributs.add(properties.getProperty(PROPERTY_CONTEXTE_DN_UTILISATEUR));
			listeAttributs.add(properties.getProperty(PROPERTY_CN_UTILISATEUR));
			listeAttributs.add(properties.getProperty(PROPERTY_MDP));
			listeAttributs.add(properties.getProperty(PROPERTY_CN_ADMIN));
			listeAttributs.add(properties.getProperty(PROPERTY_MDP_ADMIN));

		} catch (IOException e) {
			logger.log(Level.WARN, "Echec de la lecture du fichier de propriété : " + e.getMessage(), e);
		}
		return listeAttributs;
	}

	/**
	 * Initialise une connexion anonyme avec le serveur LDAP. Si la création du
	 * contexte génère une erreur, vérifie la connexion et les paramètres sur
	 * serveur (ip ou port). Utilisée pour pouvoir vérifier l'existence d'un
	 * utilisateur.
	 * 
	 * @return contexte le contexte de type InitialLdapContext.
	 */
	protected InitialLdapContext initialiseConnexion() {
		String serverLogin = "cn=" + this.cnUtilisateur + "," + this.contexteDNUtilisateur;
		// on remplit un tableau avec les paramètres d'environnement et de connexion au
		// LDAP
		Properties propriete = new Properties();
		propriete.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
		propriete.put(Context.PROVIDER_URL, "ldap://" + this.serveur + ":" + this.port + "/");
		propriete.put(Context.SECURITY_AUTHENTICATION, "simple");
		propriete.put(Context.SECURITY_PRINCIPAL, serverLogin);
		propriete.put(Context.SECURITY_CREDENTIALS, this.motDePasseUtilisateur);
		InitialLdapContext contexte = null;

		// si la creation du contexte génère une erreur, alors les identifiants
		// sont faux et donc la connexion n'est pas autorisée
		try {
			contexte = new InitialLdapContext(propriete, null);
		} catch (NamingException e) {
			logger.log(Level.WARN, "Erreur lors de la connexion.", e);
		}
		return contexte;
	}

	/**
	 * Teste la connexion au serveur LDAP. On utilise la connexion anonyme et on ne
	 * vérifie ici que les attributs de connexion (port + serveur + mdp +
	 * cnUtilisateur).
	 * 
	 * @return un boolean traduisant l'état de la connexion
	 */
	public boolean testConnexion() {
		return initialiseConnexion() != null;
	}

	/**
	 * Renvoie la liste des attributs du fichier de propriété par défaut.
	 * 
	 * @return la liste des attributs du fichier de propriété par défaut.
	 */
	public List<String> lireFichierProprieteParDefaut() {
		return lireFichierPropriete(FICHIER_PROPERTIES_DEFAUT);
	}

}