package fr.eseo.ld.ldap;

import java.util.ArrayList;
import java.util.List;

import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.naming.ldap.InitialLdapContext;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import fr.eseo.ld.beans.Utilisateur;
import fr.eseo.ld.dao.DAOFactory;
import fr.eseo.ld.dao.UtilisateurDAO;

/**
 * Classe permettant de lire l'annuaire AD de façon anonyme de d'y trouver les
 * utilisateurs présents. Ces mêmes utilsateurs pourront par la suite être
 * intégrés dans la base de données du projet (nom, prenom, mail, id).
 * 
 * @author Thomas MENARD
 */
public class VerificationAnnuaire extends LDAP {

	private static Logger logger = Logger.getLogger(VerificationAnnuaire.class.getName());

	private static final String HASH_PAR_DEFAUT = "activeDirectory";

	/**
	 * Initialise les paramètres du serveur LDAP {@link #LDAP()}.
	 */
	public VerificationAnnuaire() {
		super();
	}

	/**
	 * Recherche la totalité des utilisateurs dans l'annuaire et les place dans une
	 * ArrayList pour être synchronisé avec la BDD existante.
	 * 
	 * @param contexte
	 *            le contexte créé lors de l'initialisation de la connexion
	 *            {@link #initialiseConnexionS()}.
	 * @param filtre
	 *            le filtre qui determine la façon dont la recherche est effectuée.
	 */
	public void synchroniserBDD() {
		InitialLdapContext contexte = this.initialiseConnexion();
		List<Utilisateur> listeUtilisateur = this.rechercherUtilisateursAD(contexte);
		this.mettreAJourUtilisateursBDD(listeUtilisateur);
	}

	/**
	 * Renvoie une liste de tous les utilisateurs contenus dans l'annuaire.
	 * 
	 * @param contexte
	 *            le contexte de connexion au LDAP.
	 * @return listeUtilisateurs la liste de tous les utilisateurs.
	 */
	private List<Utilisateur> rechercherUtilisateursAD(InitialLdapContext contexte) {
		/* Création des variables utilisées lors de la recherche */
		SearchControls searchControls = new SearchControls();
		searchControls.setSearchScope(SearchControls.SUBTREE_SCOPE);
		NamingEnumeration<SearchResult> resultatRecherche;

		/*
		 * Initialisation de la liste contenant les utilisateurs présents dans
		 * l'annuaire
		 */
		List<Utilisateur> listeUtilisateurs = new ArrayList<>();

		/*
		 * Création du filtre utilisé lors de la recherche, ici on recherche toutes les
		 * personnes sur l'annuaire
		 */
		String filtre = "(&(objectclass=person)(cn=*))";
		try {
			resultatRecherche = contexte.search(this.contexteDN, filtre, searchControls);
			/*
			 * Pour chaque résultat, récupération de l'email, du nom, du prénom et de
			 * l'identifiant de l'utilisateur
			 */
			while (resultatRecherche.hasMoreElements()) {
				SearchResult sr = resultatRecherche.next();
				Attributes attributs = sr.getAttributes();
				// on crée l'objet utilisateur en utilisant les attributs récupérés
				Utilisateur utilisateur = map(attributs);
				// on ajoute l'utilisateur à la liste
				listeUtilisateurs.add(utilisateur);
			}
		} catch (Exception e) {
			logger.log(Level.WARN, "Impossible de se connecter au serveur LDAP" + e.getMessage(), e);
		}
		return listeUtilisateurs;
	}

	/**
	 * Met à jour dans la base des données les utilisateurs présents dans la liste
	 * {@link listeUtilisateurs}. Si l'utilisateur n'existe pas, il est créé. S'il
	 * existe déjà, il est mis à jour.
	 * 
	 * @param listeUtilisateurs
	 *            la liste des utilisateurs à mettre à jour.
	 */
	private void mettreAJourUtilisateursBDD(List<Utilisateur> listeUtilisateursLDAP) {
		DAOFactory daoFactory = DAOFactory.getInstance();
		UtilisateurDAO utilisateurDAO = daoFactory.getUtilisateurDao();
		List<Utilisateur> listeUtilisateurBDD = utilisateurDAO.lister();

		for (Utilisateur utilisateurLDAP : listeUtilisateursLDAP) {
			if (utilisateurLDAP != null) {
				/* Récupération des informations de l'utilisateur <utilisateurLDAP> */
				String nom = utilisateurLDAP.getNom();
				String prenom = utilisateurLDAP.getPrenom();
				String identifiant = utilisateurLDAP.getIdentifiant();

				/* Recherche de son équivalent dans la BDD */
				Utilisateur utilisateur = trouverUtilisateur(listeUtilisateurBDD, nom, prenom, identifiant);

				// si l'utilisateur renvoyé est nul, c'est qu'il n'existe pas dans la base de
				// données
				if (utilisateur == null) {
					utilisateurLDAP.setHash(HASH_PAR_DEFAUT);
					utilisateurLDAP.setValide("oui");
					utilisateurDAO.creer(utilisateurLDAP);

					// sinon, l'utilisateur est dans notre BDD et on met à jour ses attributs
				} else if ((HASH_PAR_DEFAUT).equals(utilisateur.getHash())) {
					utilisateurLDAP.setIdUtilisateur(utilisateur.getIdUtilisateur());
					utilisateurDAO.modifier(utilisateurLDAP);
				}
			}
		}
	}

	/**
	 * Renvoie le bean utilisateur contenu dans la liste qui correspond aux nom,
	 * prénom et identidiant rentrés en paramètre.
	 *
	 * @param listeUtilisateurs
	 *            la liste contenant tous les utilisateurs.
	 * @param nom
	 *            le nom recherché.
	 * @param prenom
	 *            le prenom recherché.
	 * @param identifiant
	 *            l'identifiant recherché.
	 * @return utilisateur l'utilisateur trouvé, null sinon.
	 */
	private Utilisateur trouverUtilisateur(List<Utilisateur> listeUtilisateurs, String nom, String prenom,
			String identifiant) {
		Utilisateur utilisateurReturn = null;
		for (Utilisateur utilisateur : listeUtilisateurs) {
			if ((utilisateur.getNom().equalsIgnoreCase(nom)) && (utilisateur.getPrenom().equalsIgnoreCase(prenom))
					&& (utilisateur.getIdentifiant().equalsIgnoreCase(identifiant))) {
				utilisateurReturn = utilisateur;
			}
		}
		return utilisateurReturn;
	}

	/**
	 * Récupère les résultats d'une recherche sur l'annuaire et fait correspondre
	 * avec les informations dont nous avons besoin pour créer un utilisateur.
	 * 
	 * @param attributs
	 *            le resultat de la recherche sur l'annuaire.
	 * @return utilisateur l'utilisateur de type Utilisateur.
	 */
	private Utilisateur map(Attributes attributs) {
		Utilisateur utilisateur = null;

		/* Récupération de tous les attributs */
		Attribute nomAttribut = attributs.get(this.nomID);
		Attribute prenomAttribut = attributs.get(this.prenomID);
		Attribute mailAttribut = attributs.get(this.mailID);
		Attribute idAttribut = attributs.get(this.idID);

		// si le nom, le prénom et l'identifiant sont nuls, alors l'utilisateur
		// n'est pas créé et la fonction renvoie un null
		if (nomAttribut != null && prenomAttribut != null && idAttribut != null) {
			try {
				String nom = nomAttribut.get().toString();
				String prenom = prenomAttribut.get().toString();
				String id = idAttribut.get().toString();
				String mail = mailAttribut.get().toString();
				utilisateur = new Utilisateur();
				utilisateur.setNom(nom);
				utilisateur.setPrenom(prenom);
				utilisateur.setIdentifiant(id);
				utilisateur.setEmail(mail);
				utilisateur.setValide("oui");
				utilisateur.setHash(HASH_PAR_DEFAUT);
			} catch (NamingException e) {
				logger.log(Level.WARN, "Un des attributs de l'utilisateur n'existe pas dans l'AD : " + e.getMessage(),
						e);
			}
		}
		return utilisateur;
	}

}