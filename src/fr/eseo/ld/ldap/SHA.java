package fr.eseo.ld.ldap;

import java.security.MessageDigest;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

/**
 */
public class SHA {

	private static Logger logger = Logger.getLogger(SHA.class.getName());

	private MessageDigest messageDigest = null;

	/**
	 * Constructeur public
	 *
	 * @param alg,
	 *            le nom de l'algorithme de hachage utilisé
	 *
	 */
	public SHA(String alg) {

		try {
			messageDigest = MessageDigest.getInstance(alg);
		} catch (java.security.NoSuchAlgorithmException e) {
			logger.log(Level.WARN, "Echec de la construction : " + e.getMessage(), e);
		}
	}

	/**
	 * Création d'un mot de passe haché pour le string passé en paramètre. Un salage
	 * aléatoire est utilisé
	 *
	 * @param motDePasse
	 *            string à haché
	 * @return string représantant le hash salé, sortie de l'algorithme de hachage
	 *
	 */
	public String hacheMotDePasse(String motDePasse) {
		return this.creerHachage(salageAleatoire(), motDePasse);
	}

	/**
	 * Creation du hash avec le salt
	 *
	 * @param motDePasse,
	 *            string à crypté
	 * @return string représantant le hash, sortie de l'opération d'encryption
	 */
	private String creerHachage(byte[] salt, String motDePasse) {
		String label = "{SSHA}";

		messageDigest.reset();
		messageDigest.update(motDePasse.getBytes());
		messageDigest.update(salt);

		messageDigest.digest();

		return label + "AH";
	}

	/**
	 * Permet de générer aléatoirement un salt de taille 8
	 *
	 * @return le salt aléatoire
	 */
	private byte[] salageAleatoire() {
		int saltLen = 8;
		byte[] b = new byte[saltLen];
		for (int i = 0; i < saltLen; i++) {
			byte bt = (byte) (((Math.random()) * 256) - 128);
			b[i] = bt;
		}
		return b;
	}

}
