package fr.eseo.ld.ldap;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

/**
 * Classe implémentant les méthodes et fonctions permettant de modifier le
 * fichier de configuration pour l'accès au serveur LDAP.
 * 
 * @author Thomas MENARD
 */
public class FichierPropriete extends LDAP {

	private Properties properties = new Properties();
	private URL url;

	private static Logger logger = Logger.getLogger(FichierPropriete.class.getName());

	/**
	 * Constructeur par défaut.
	 */
	public FichierPropriete() {
		super();
	}

	/**
	 * Renvoie la liste des attributs contenus dans le fichier de propriétés.
	 * 
	 * @return listeAttributs la liste des attributs contenus dans le fichier de
	 *         propriétés.
	 */
	public List<String> lireFichier() {
		List<String> listeAttributs = new ArrayList<>();
		listeAttributs.add(this.contexteDN);
		listeAttributs.add(this.serveur);
		listeAttributs.add(this.port);
		listeAttributs.add(this.nomID);
		listeAttributs.add(this.prenomID);
		listeAttributs.add(this.mailID);
		listeAttributs.add(this.idID);
		listeAttributs.add(this.contexteDNUtilisateur);
		listeAttributs.add(this.cnUtilisateur);
		listeAttributs.add(this.motDePasseUtilisateur);
		listeAttributs.add(this.cnAdmin);
		listeAttributs.add(this.mdpAdmin);
		return listeAttributs;
	}

	/**
	 * Modifie les paramètres d'accès au LDAP en prenant en compte les nouveaux
	 * attributs.
	 * 
	 * @param nouveauxAttributs
	 *            les nouveaux paramètres de la classe.
	 */
	public void modifierFichier(String[] nouveauxAttributs) {
		/*
		 * Création des différentes variables utilisées lors de la lecture du fichier de
		 * propriétés
		 */
		this.properties = new Properties();
		ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
		InputStream fichierProperties = classLoader.getResourceAsStream(LDAP.FICHIER_PROPERTIES);

		try {
			// on vérifie que le tableau d'attributs contient bien 12 attributs
			if (nouveauxAttributs.length != 12) {
				throw new IOException();
			}

			// on charge le fichier de propriétés
			this.properties.load(fichierProperties);

			// on change les différentes propriétés avec les attributs rentrés en paramètre
			// de la méthode
			changerAttributs(nouveauxAttributs);

			this.getClass().getClassLoader();
			// on récupère le chemin du fichier de paramètres
			this.url = this.getClass().getResource("annuaire.properties");

			// on remplace les attributs de l'annuaire avec les nouvelles valeurs
			this.ecrireDansFichier();
		} catch (IOException | URISyntaxException e) {
			logger.log(Level.WARN, "Impossible de lire ou de modifier le fichier de paramètres d'accès au LDAP ", e);
		}
	}

	/**
	 * Ecrit dans un fichier.
	 * 
	 * @param url
	 *            chemin du fichier.
	 * @param properties
	 *            fichier de propriétés à réécrire.
	 * @throws URISyntaxException
	 * @throws IOException
	 */
	private void ecrireDansFichier() throws URISyntaxException, IOException {
		/* Changement des paramètres présent dans annuaire.properties */
		File fileObject = new File(this.url.toURI());
		FileOutputStream out = new FileOutputStream(fileObject);
		this.properties.store(out, null);
		out.close();
	}

	/**
	 * Change les paramètres de la classe avec les attributs présents dans le
	 * tableau de Strings mis en paramètre.
	 * 
	 * @param nouveauxAttributs
	 *            les nouveaux paramètres de la classe.
	 */
	private void changerAttributs(String[] nouveauxAttributs) {
		this.properties.setProperty(LDAP.PROPERTY_CONTEXTE_DN, nouveauxAttributs[0]);
		this.contexteDN = nouveauxAttributs[0];
		this.properties.setProperty(LDAP.PROPERTY_SERVEUR, nouveauxAttributs[1]);
		this.serveur = nouveauxAttributs[1];
		this.properties.setProperty(LDAP.PROPERTY_PORT, nouveauxAttributs[2]);
		this.port = nouveauxAttributs[2];
		this.properties.setProperty(LDAP.PROPERTY_NOM_ID, nouveauxAttributs[3]);
		this.nomID = nouveauxAttributs[3];
		this.properties.setProperty(LDAP.PROPERTY_PRENOM_ID, nouveauxAttributs[4]);
		this.prenomID = nouveauxAttributs[4];
		this.properties.setProperty(LDAP.PROPERTY_MAIL_ID, nouveauxAttributs[5]);
		this.mailID = nouveauxAttributs[5];
		this.properties.setProperty(LDAP.PROPERTY_ID_ID, nouveauxAttributs[6]);
		this.idID = nouveauxAttributs[6];
		this.properties.setProperty(LDAP.PROPERTY_CONTEXTE_DN_UTILISATEUR, nouveauxAttributs[7]);
		this.contexteDNUtilisateur = nouveauxAttributs[7];
		this.properties.setProperty(LDAP.PROPERTY_CN_UTILISATEUR, nouveauxAttributs[8]);
		this.cnUtilisateur = nouveauxAttributs[8];
		this.properties.setProperty(LDAP.PROPERTY_MDP, nouveauxAttributs[9]);
		this.motDePasseUtilisateur = nouveauxAttributs[9];
		this.properties.setProperty(LDAP.PROPERTY_CN_ADMIN, nouveauxAttributs[10]);
		this.cnAdmin = nouveauxAttributs[10];
		this.properties.setProperty(LDAP.PROPERTY_MDP_ADMIN, nouveauxAttributs[11]);
		this.mdpAdmin = nouveauxAttributs[11];
	}

}