package fr.eseo.ld.ldap;

import java.io.UnsupportedEncodingException;
import java.util.Properties;

import javax.naming.Context;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.BasicAttribute;
import javax.naming.directory.BasicAttributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.ModificationItem;
import javax.naming.ldap.InitialLdapContext;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import fr.eseo.ld.beans.Utilisateur;

/**
 * Classe permettant d'ajouter des utilisateurs dans le LDAP (optionnel) et de
 * changer son mot de passe.
 * 
 * @author MENARD Thomas
 */
public class GestionUtilisateurLDAP extends LDAP {

	private static Logger logger = Logger.getLogger(GestionUtilisateurLDAP.class.getName());

	private static SHA sha = new SHA("SHA");

	/**
	 * Constructeur par défaut.
	 */
	public GestionUtilisateurLDAP() {
		super();
	}

	/**
	 * Crée une connexion avec le compte administrateur.
	 * 
	 * @return contexte la connexion créée.
	 */
	private InitialLdapContext connexionAdministrateur() {
		// on remplit un tableau avec les parametres d'environnement et de connexion au
		// LDAP
		Properties propriete = new Properties();
		propriete.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
		propriete.put(Context.PROVIDER_URL, "ldap://" + this.serveur + ":" + this.port + "/");
		propriete.put(Context.SECURITY_AUTHENTICATION, "simple");
		propriete.put(Context.SECURITY_PRINCIPAL, this.cnAdmin);
		propriete.put(Context.SECURITY_CREDENTIALS, this.mdpAdmin);

		InitialLdapContext contexte = null;
		// si la creation du contexte génère une erreur, alors les identifiants
		// sont faux et donc la connexion n'est pas autorisée
		try {
			contexte = new InitialLdapContext(propriete, null);
		} catch (NamingException e) {
			logger.log(Level.WARN, "Impossible d'initialiser une connexion sécurisée au serveur LDAP" + e.getMessage(),
					e);
		}
		return contexte;
	}

	/**
	 * Ajoute un utilisateur dans l'annuaire.
	 * 
	 * @param identifiant
	 *            l'identifiant de l'utilisateur ajouté.
	 * @param nom
	 *            le nom de l'utilisateur ajouté.
	 * @param prenom
	 *            le prénom de l'utilisateur ajouté.
	 * @param email
	 *            l'email de l'utilisateur ajouté.
	 * @throws UnsupportedEncodingException
	 */
	public void ajouterUtilisateur(String identifiant, String nom, String prenom, String email, String motDePasse) {
		Utilisateur utilisateur = mapUtilisateur(identifiant, nom, prenom, email);
		Attributes attributes = new BasicAttributes();
		Attribute attribute = new BasicAttribute("objectClass");
		attribute.add("top");
		attribute.add("inetOrgPerson");
		attributes.put(attribute);

		String cn = utilisateur.getPrenom().toLowerCase() + " " + utilisateur.getNom().toLowerCase();
		Attribute utilisateurCn = new BasicAttribute("cn", cn);
		Attribute utilisateurNom = new BasicAttribute(this.nomID, utilisateur.getNom());
		Attribute utilisateurPrenom = new BasicAttribute(this.nomID, utilisateur.getPrenom());
		Attribute utilisateurMail = new BasicAttribute(this.mailID, utilisateur.getEmail());
		Attribute utilisateurIdentifiant = new BasicAttribute(this.idID, utilisateur.getIdentifiant());

		Attribute utilisateurMotDePasse = new BasicAttribute("userPassword", sha.hacheMotDePasse(motDePasse));

		Attribute oc = new BasicAttribute("objectclass");
		oc.add("top");
		oc.add("inetOrgPerson");

		attributes.put(utilisateurCn);
		attributes.put(utilisateurNom);
		attributes.put(utilisateurPrenom);
		attributes.put(utilisateurMail);
		attributes.put(utilisateurMotDePasse);
		attributes.put(utilisateurIdentifiant);
		String entreeDN = "cn=" + cn + "," + this.contexteDNUtilisateur;
		InitialLdapContext contexte = connexionAdministrateur();
		if (contexte == null) {
			logger.log(Level.WARN, "Impossible de se connecter au LDAP en tant qu'administrateur");
		} else {
			try {
				contexte.createSubcontext(entreeDN, attributes);
			} catch (NamingException e) {
				logger.log(Level.WARN, "Impossible d'ajouter un utilisateur sur le serveur LDAP" + e.getMessage(), e);
			}
		}
	}

	/**
	 * Modifie le mot de passe sur l'OpenLDAP de l'application.
	 * 
	 * @param entreeCN
	 *            le Nom Commun de l'utilisateur.
	 * @param motDePasse
	 *            le nouveau mot de passe de l'utilisateur.
	 */
	public void modifierMotDePasse(String entreeCN, String motDePasse) {
		ModificationItem[] mods = new ModificationItem[1];

		Attribute nouveauMDP = new BasicAttribute("userPassword", sha.hacheMotDePasse(motDePasse));
		mods[0] = new ModificationItem(DirContext.REPLACE_ATTRIBUTE, nouveauMDP);

		String entreeDN = "cn=" + entreeCN + "," + this.contexteDNUtilisateur;
		InitialLdapContext contexte = connexionAdministrateur();
		if (contexte == null) {
			logger.log(Level.WARN, "Impossible de se connecter au LDAP en tant qu'administrateur");
		} else if (motDePasse.isEmpty() || entreeCN.isEmpty()) {
			logger.log(Level.WARN, "Il manque des élements pour changer le mot de passe sur le LDAP");
		} else {
			try {
				contexte.modifyAttributes(entreeDN, mods);
			} catch (NamingException e) {
				logger.log(Level.WARN, "Impossible d'ajouter un utilisateur sur le serveur LDAP" + e.getMessage(), e);
			}
		}
	}

	/**
	 * Fait la correspondance (le mapping) entre une ligne issue de la liste
	 * listeAttributs et un bean Utilisateur.
	 * 
	 * @return utilisateur le bean dont on souhaite faire la correspondance.
	 */
	private Utilisateur mapUtilisateur(String identifiant, String nom, String prenom, String email) {
		Utilisateur utilisateur = new Utilisateur();
		utilisateur.setIdentifiant(identifiant);
		utilisateur.setNom(nom);
		utilisateur.setPrenom(prenom);
		utilisateur.setEmail(email);
		return utilisateur;
	}

}