package fr.eseo.ld.ldap;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.ByteArrayOutputStream;
import java.util.List;

import org.apache.log4j.Appender;
import org.apache.log4j.Layout;
import org.apache.log4j.Logger;
import org.apache.log4j.SimpleLayout;
import org.apache.log4j.WriterAppender;
import org.easymock.EasyMockRunner;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(EasyMockRunner.class)
public class FichierProprieteTest {

	private static Logger logger = Logger.getLogger(FichierPropriete.class.getName());

	private static FichierPropriete fichierPropriete;

	private static final String contexteDN = "dc=ldcr,dc=tp";
	private static final String serveur = "192.168.4.11";
	private static final String port = "389";
	private static final String nomID = "sn";
	private static final String prenomID = "givenName";
	private static final String mailID = "mail";
	private static final String idID = "uid";
	private static final String contexteDNUtilisateur = "ou=people,dc=ldcr,dc=tp";
	private static final String cnUtilisateur = "thomas menard";
	private static final String motDePasseUtilisateur = "network";
	private static final String motDePasseAdmin = "network";
	private static final String dnAdmin = "cn=admin,dc=ldcr,dc=tp";

	private static final String contexteDN_modif = "dc=ldcr,dc=tp,test";
	private static final String serveur_modif = "192.168.4.12";
	private static final String port_modif = "400";
	private static final String nomID_modif = "sn_test";
	private static final String prenomID_modif = "givenName_test";
	private static final String mailID_modif = "mail_test";
	private static final String idID_modif = "uid_test";
	private static final String contexteDNUtilisateur_modif = "ou=people,dc=ldcr,dc=tp,test";
	private static final String cnUtilisateur_modif = "nimporteQuoi";
	private static final String motDePasseUtilisateur_modif = "azerty";
	private static final String motDePasseAdmin_modif = "nimporteQuoi222";
	private static final String dnAdmin_modif = "azertyazeart";

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		fichierPropriete = new FichierPropriete();
		
		 List<String> listePropriete = fichierPropriete.lireFichierProprieteParDefaut();
		
		 String[] nouveauxAttributs = { listePropriete.get(0), listePropriete.get(1), listePropriete.get(2),
					listePropriete.get(3), listePropriete.get(4), listePropriete.get(5), listePropriete.get(6),
					listePropriete.get(7), listePropriete.get(8),listePropriete.get(9),listePropriete.get(10),listePropriete.get(11) };
		
		 fichierPropriete.modifierFichier(nouveauxAttributs);
	}

	@Test
	public void test_1lireAttributFichier() {
		List<String> resultatRecupere = fichierPropriete.lireFichier();
		assertEquals("Mauvais element ", contexteDN, resultatRecupere.get(0));
		assertEquals("Mauvais element ", serveur, resultatRecupere.get(1));
		assertEquals("Mauvais element ", port, resultatRecupere.get(2));
		assertEquals("Mauvais element ", nomID, resultatRecupere.get(3));
		assertEquals("Mauvais element ", prenomID, resultatRecupere.get(4));
		assertEquals("Mauvais element ", mailID, resultatRecupere.get(5));
		assertEquals("Mauvais element ", idID, resultatRecupere.get(6));
		assertEquals("Mauvais element ", contexteDNUtilisateur, resultatRecupere.get(7));
		assertEquals("Mauvais element ", cnUtilisateur, resultatRecupere.get(8));
		assertEquals("Mauvais element ", motDePasseUtilisateur, resultatRecupere.get(9));
		assertEquals("Mauvais element ", dnAdmin, resultatRecupere.get(10));
		assertEquals("Mauvais element ", motDePasseAdmin, resultatRecupere.get(11));
	}

	@Test
	public void test_2modifierAttributFichier() {
		String[] nouveauxAttributs = { contexteDN_modif, serveur_modif, port_modif, nomID_modif, prenomID_modif,
				mailID_modif, idID_modif, contexteDNUtilisateur_modif, cnUtilisateur_modif,motDePasseUtilisateur_modif,dnAdmin_modif,motDePasseAdmin_modif };
		fichierPropriete.modifierFichier(nouveauxAttributs);
		fichierPropriete = new FichierPropriete();
		List<String> resultatRecupere = fichierPropriete.lireFichier();
		assertEquals("Mauvais element ", contexteDN_modif, resultatRecupere.get(0));
		assertEquals("Mauvais element ", serveur_modif, resultatRecupere.get(1));
		assertEquals("Mauvais element ", port_modif, resultatRecupere.get(2));
		assertEquals("Mauvais element ", nomID_modif, resultatRecupere.get(3));
		assertEquals("Mauvais element ", prenomID_modif, resultatRecupere.get(4));
		assertEquals("Mauvais element ", mailID_modif, resultatRecupere.get(5));
		assertEquals("Mauvais element ", idID_modif, resultatRecupere.get(6));
		assertEquals("Mauvais element ", contexteDNUtilisateur_modif, resultatRecupere.get(7));
		assertEquals("Mauvais element ", cnUtilisateur_modif, resultatRecupere.get(8));
		assertEquals("Mauvais element ", motDePasseUtilisateur_modif, resultatRecupere.get(9));
		assertEquals("Mauvais element ", dnAdmin_modif, resultatRecupere.get(10));
		assertEquals("Mauvais element ", motDePasseAdmin_modif, resultatRecupere.get(11));

	}

	@Test
	public void test_3modifierAttributFichierParDefaut() {
		List<String> listePropriete = fichierPropriete.lireFichierProprieteParDefaut();
		String[] nouveauxAttributs = { listePropriete.get(0), listePropriete.get(1), listePropriete.get(2),
				listePropriete.get(3), listePropriete.get(4), listePropriete.get(5), listePropriete.get(6),
				listePropriete.get(7), listePropriete.get(8),listePropriete.get(9),listePropriete.get(10),listePropriete.get(11) };
		fichierPropriete.modifierFichier(nouveauxAttributs);
		List<String> resultatRecupere = fichierPropriete.lireFichier();
		assertEquals("Mauvais element", contexteDN, resultatRecupere.get(0));
		assertEquals("Mauvais element", serveur, resultatRecupere.get(1));
		assertEquals("Mauvais element", port, resultatRecupere.get(2));
		assertEquals("Mauvais element", nomID, resultatRecupere.get(3));
		assertEquals("Mauvais element", prenomID, resultatRecupere.get(4));
		assertEquals("Mauvais element", mailID, resultatRecupere.get(5));
		assertEquals("Mauvais element", idID, resultatRecupere.get(6));
		assertEquals("Mauvais element", contexteDNUtilisateur, resultatRecupere.get(7));
		assertEquals("Mauvais element", cnUtilisateur, resultatRecupere.get(8));
		assertEquals("Mauvais element", motDePasseUtilisateur, resultatRecupere.get(9));
		assertEquals("Mauvais element ", dnAdmin, resultatRecupere.get(10));
		assertEquals("Mauvais element ", motDePasseAdmin, resultatRecupere.get(11));

	}

	@Test
	public void test_4modifierAttributFichier_Log() {
		/* Configuraton du logger */
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		Layout layout = new SimpleLayout();
		Appender appender = new WriterAppender(layout, out);
		logger.addAppender(appender);

		try {
			String[] nouveauxAttributs = {};
			fichierPropriete.modifierFichier(nouveauxAttributs);
			String messageLog = out.toString();
			assertTrue("Mauvais logging",
					messageLog.contains("Impossible de lire ou de modifier le fichier de paramètres d'accès au LDAP"));
		} finally {
			/* Configuraton initiale du logger */
			logger.removeAppender(appender);
		}
	}
	
}