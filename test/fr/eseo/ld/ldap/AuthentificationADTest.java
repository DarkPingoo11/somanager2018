package fr.eseo.ld.ldap;

import static org.junit.Assert.*;

import java.io.ByteArrayOutputStream;
import javax.naming.ldap.InitialLdapContext;
import org.apache.log4j.Appender;
import org.apache.log4j.Layout;
import org.apache.log4j.Logger;
import org.apache.log4j.SimpleLayout;
import org.apache.log4j.WriterAppender;
import org.easymock.EasyMock;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;


/**
 * Classe de tests unitaires JUnit 4 de la classe AuthentificationAD.
 *
 * @version 1.0
 * @author Thomas MENARD
 *
 * @see fr.eseo.ld.ldap.AuthentificationAD
 * @see org.junit
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class AuthentificationADTest {

	private static Logger logger = Logger.getLogger(AuthentificationAD.class.getName());

	private static AuthentificationAD authentificationAD;
	private static final String TEST_UID = "testSuspendu";
	private static final String TEST_UID_INCONNU = "testSuspenduInconnu";
	private static final String TEST_CN = "test test";
	private static final String TEST_CN_INCONNU = "inconnu";
	private static final String TEST_MDP = "network";
	private static final String TEST_MDP_INCONNU = "inconnu";

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		authentificationAD = new AuthentificationAD();
	}

	@Test
	public void test2_recupererNomCommun() {
		String resultatRecupere = authentificationAD.recupererNomCommun(TEST_UID);
		assertEquals("Le CN n'est pas le bon", TEST_CN, resultatRecupere);
	}

	@Test
	public void test3_recupererNomCommunInconnu() {
		String resultatRecupere = authentificationAD.recupererNomCommun(TEST_UID_INCONNU);
		assertEquals("Le CN retourné existe", null, resultatRecupere);
	}

	@Test
	public void test4_recupererNomCommunLog() {
		/* Configuraton du logger */
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		Layout layout = new SimpleLayout();
		Appender appender = new WriterAppender(layout, out);
		logger.addAppender(appender);

		try {
			AuthentificationAD authentificationADMock = EasyMock.createMockBuilder(AuthentificationAD.class)
					.addMockedMethod("initialiseConnexion").createMock();

			authentificationADMock.recupererNomCommun(TEST_UID_INCONNU);
			/* Recherche du bean dans la BDD */
			String messageLog = out.toString();
			assertTrue("Mauvais logging",
					messageLog.contains("Erreur lors de la recherche de l'ID sur le serveur LDAP"));
		} finally {
			/* Configuraton initiale du logger */
			logger.removeAppender(appender);
		}
	}

	@Test
	public void test4_testConnexionSecure() {
		InitialLdapContext contexte = authentificationAD.connexionAD(TEST_CN, TEST_MDP);
		assertNotNull(contexte);
	}

	@Test
	public void test5_1testConnexionSecureInconnu() {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		Layout layout = new SimpleLayout();
		Appender appender = new WriterAppender(layout, out);
		logger.addAppender(appender);

		try {
			InitialLdapContext contexte = authentificationAD.connexionAD(TEST_CN_INCONNU, TEST_MDP_INCONNU);
			assertNull(contexte);
			String messageLog = out.toString();
			assertTrue("Mauvais logging", messageLog.contains("Erreur lors de la connexion"));
		} finally {
			logger.removeAppender(appender);
		}
	}

	@Test
	public void test5_2testConnexionSecureInconnu() {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		Layout layout = new SimpleLayout();
		Appender appender = new WriterAppender(layout, out);
		logger.addAppender(appender);

		try {
			InitialLdapContext contexte = authentificationAD.connexionAD(TEST_CN, TEST_MDP_INCONNU);
			assertNull(contexte);
			String messageLog = out.toString();
			assertTrue("Mauvais logging", messageLog.contains("Erreur lors de la connexion"));
		} finally {
			logger.removeAppender(appender);
		}
	}

	@Test
	public void test5_3testConnexionSecureInconnu() {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		Layout layout = new SimpleLayout();
		Appender appender = new WriterAppender(layout, out);
		logger.addAppender(appender);

		try {
			InitialLdapContext contexte = authentificationAD.connexionAD(TEST_CN_INCONNU, TEST_MDP);
			assertNull(contexte);
			String messageLog = out.toString();
			assertTrue("Mauvais logging", messageLog.contains("Erreur lors de la connexion"));
		} finally {
			logger.removeAppender(appender);
		}
	}

	@Test
	public void test5_4testConnexionSecureInconnu() {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		Layout layout = new SimpleLayout();
		Appender appender = new WriterAppender(layout, out);
		logger.addAppender(appender);

		try {
			InitialLdapContext contexte = authentificationAD.connexionAD("", "");
			assertNull(contexte);
			String messageLog = out.toString();
			assertTrue("Mauvais logging", messageLog.contains("Erreur lors de la connexion"));
		} finally {
			logger.removeAppender(appender);
		}
	}
	
}