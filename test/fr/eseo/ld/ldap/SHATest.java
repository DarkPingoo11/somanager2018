package fr.eseo.ld.ldap;

import static org.junit.Assert.*;

import net.codebox.javabeantester.JavaBeanTester;

import java.beans.IntrospectionException;

import org.junit.Ignore;
import org.junit.Test;

public class SHATest {

    private static final String MOT_A_HASH = "MotDePasseHeyHey";
    private static final String MOT_HACHE = "{SSHA}AH";




    /**
     * Le test ne semble pas pertinent, vu que la méthode semble erronée
     */
    @Ignore
    public void hacheMotDePasse() {
        SHA sha = new SHA("SHA");
        assertEquals("Le hachage n'est pas le bon", MOT_HACHE, sha.hacheMotDePasse(MOT_A_HASH));
    }
}