package fr.eseo.ld.ldap;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

/**
 * Suite de tests unitaires JUnit 4 pour le package fr.eseo.ld.ldap.
 * 
 * @author Maxime LENORMAND
 * 
 * @see org.junit
 */
@RunWith(Suite.class)
@SuiteClasses({
	AuthentificationADTest.class,
	FichierProprieteTest.class,
	LdapTest.class,
	VerificationAnnuaireTest.class
	})
public class LDAPTestSuite {
	/* Classe vide */
}