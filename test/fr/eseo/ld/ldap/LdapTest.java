package fr.eseo.ld.ldap;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;


@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class LdapTest {

	private LDAP ldap;

	private static final String contexteDN = "dc=ldcr,dc=tp";
	private static final String serveur = "192.168.4.11";
	private static final String port = "389";
	private static final String nomID = "sn";
	private static final String prenomID = "givenName";
	private static final String mailID = "mail";
	private static final String idID = "uid";
	private static final String contexteDNUtilisateur = "ou=people,dc=ldcr,dc=tp";
	private static final String cnUtilisateur = "thomas menard";
	private static final String motDePasseUtilisateur = "network";
	private static final String motDePasseAdmin = "network";
	private static final String dnAdmin = "cn=admin,dc=ldcr,dc=tp";

	
	@Before
	public void setUp() {
		FichierPropriete fichierPropriete = new FichierPropriete();
		
		 List<String> listePropriete = fichierPropriete.lireFichierProprieteParDefaut();
		
		 String[] nouveauxAttributs = { listePropriete.get(0), listePropriete.get(1), listePropriete.get(2),
					listePropriete.get(3), listePropriete.get(4), listePropriete.get(5), listePropriete.get(6),
					listePropriete.get(7), listePropriete.get(8),listePropriete.get(9),listePropriete.get(10),listePropriete.get(11) };
		
		 fichierPropriete.modifierFichier(nouveauxAttributs);
		
		ldap = new LDAP();
	}

	@Test
	public void test1_ConnexionLDAP() {
		boolean resultatRecupere = ldap.testConnexion();
		assertEquals("Pas de connexion", true, resultatRecupere);
	}

	@Test
	public void test2_LireFichierProprieteDefaut() {
		List<String> resultatRecupere = ldap.lireFichierProprieteParDefaut();
		assertEquals("Mauvais element ", contexteDN, resultatRecupere.get(0));
		assertEquals("Mauvais element ", serveur, resultatRecupere.get(1));
		assertEquals("Mauvais element ", port, resultatRecupere.get(2));
		assertEquals("Mauvais element ", nomID, resultatRecupere.get(3));
		assertEquals("Mauvais element ", prenomID, resultatRecupere.get(4));
		assertEquals("Mauvais element ", mailID, resultatRecupere.get(5));
		assertEquals("Mauvais element ", idID, resultatRecupere.get(6));
		assertEquals("Mauvais element ", contexteDNUtilisateur, resultatRecupere.get(7));
		assertEquals("Mauvais element ", cnUtilisateur, resultatRecupere.get(8));
		assertEquals("Mauvais element ", motDePasseUtilisateur, resultatRecupere.get(9));
		assertEquals("Mauvais element ", dnAdmin, resultatRecupere.get(10));
		assertEquals("Mauvais element ", motDePasseAdmin, resultatRecupere.get(11));

	}

}