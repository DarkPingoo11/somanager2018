package fr.eseo.ld.objets.exportexcel;

import org.junit.Test;

import java.util.HashMap;

import static junit.framework.TestCase.assertEquals;

public class ENJNotesTest {

    @Test
    public void getNotes() {
        ENJNotes enjNotes = new ENJNotes();
        assertEquals("Mauvaise taille", 0, enjNotes.getNotes().size());
    }

    @Test
    public void setNotes() {
        ENJNotes enjNotes = new ENJNotes();
        HashMap<ENJMatiere, Float> map = new HashMap<>();
        map.put(new ENJMatiere("L", 1f), 12f);
        enjNotes.setNotes(map);
        assertEquals("Mauvaise taille", 1, enjNotes.getNotes().size());
    }

    @Test
    public void addNote() {
        ENJNotes enjNotes = new ENJNotes();
        enjNotes.addNote(new ENJMatiere("Libelle", 0.1f), 12f);
        assertEquals("Mauvaise taille", 1, enjNotes.getNotes().size());
    }
}