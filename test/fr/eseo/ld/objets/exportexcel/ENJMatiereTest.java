package fr.eseo.ld.objets.exportexcel;

import net.codebox.javabeantester.JavaBeanTester;
import org.junit.Test;

import java.beans.IntrospectionException;

/**
 * Classe de test de ENJMatiereDAO
 *
 * @author Tristan LE GACQUE
 * @see ENJMatiere
 **/
public class ENJMatiereTest {

    @Test
    public void testBean() throws IntrospectionException {
        JavaBeanTester.test(ENJMatiere.class);
    }
}