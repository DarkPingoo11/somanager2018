package fr.eseo.ld.objets.exportexcel;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

/**
 * Suite de tests unitaires JUnit 4
 *
 * @author Tristan LE GACQUE
 */
@RunWith(Suite.class)
@SuiteClasses({
        ENJNotesTest.class,
        ENJMatiereTest.class,
        ExportTypeTest.class,
        ExcelNotesJuryTest.class
})
public class ObjetsTestSuite {
} 


