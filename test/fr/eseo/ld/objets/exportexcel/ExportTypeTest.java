package fr.eseo.ld.objets.exportexcel;

import static org.junit.Assert.*;

import net.codebox.javabeantester.JavaBeanTester;

import java.beans.IntrospectionException;

import org.junit.Test;

public class ExportTypeTest {

    @Test
    public void getId() {
        assertEquals(1, ExportType.AUTOCOMPLETE.getId());
        assertEquals(0, ExportType.NORMAL.getId());
    }

    @Test
    public void getExportFromId() {
        assertEquals(ExportType.NORMAL, ExportType.getExportFromId(0));
        assertEquals(ExportType.AUTOCOMPLETE, ExportType.getExportFromId(1));
    }

    @Test
    public void getExportFromIdNull() {
        assertNull(ExportType.getExportFromId(200));
    }
}