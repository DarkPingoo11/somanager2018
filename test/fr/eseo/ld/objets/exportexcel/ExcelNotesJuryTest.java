package fr.eseo.ld.objets.exportexcel;

import fr.eseo.ld.beans.Utilisateur;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class ExcelNotesJuryTest {

    private static final Integer COL_ID_UTILISATEUR = 0;
    private static final Integer COL_NOM_UTILISATEUR = 3;
    private static final Integer COL_NOTE = 4;
    private static final Integer COL_COMMENTAIRE = 5;
    private static final Integer HEADER_ROW = 1;

    private static final Integer TEST_ID_UTILISATEUR = 1;
    private static final String TEST_NOM_UTILISATEUR = "DUPONT";
    private static final String TEST_PRENOM_UTILISATEUR = "Jean";
    private static final Float TEST_NOTE_SOUTENANCE = 12.4f;
    private static final Float TEST_NOTE_POSTER = 16.2f;

    private static final float DELTA = 0.01f;
    private static final Integer COL_MOYENNE = 6;
    private static final Integer COL_GRADE = 7;

    private ExcelNotesJury enj;

    @Before
    public void beforeTest() {
        //Définition du workbook
        Workbook workbook = new XSSFWorkbook();
        workbook.createSheet();

        //Définition des parametres
        HashMap<ENJParams, Integer> params = new HashMap<>();
        params.put(ENJParams.COL_ID_UTILISATEUR, COL_ID_UTILISATEUR);
        params.put(ENJParams.COL_NOM_UTILISATEUR, COL_NOM_UTILISATEUR);
        params.put(ENJParams.COL_NOTE, COL_NOTE);
        params.put(ENJParams.COL_COMMENTAIRE, COL_COMMENTAIRE);
        params.put(ENJParams.COL_MOYENNE, COL_MOYENNE);
        params.put(ENJParams.COL_GRADE, COL_GRADE);

        //Définition de l'objet ExcelNoteJury
        this.enj = new ExcelNotesJury(workbook, params, HEADER_ROW);
        for(int i = 0; i < 5; i ++) {
            Row r = this.enj.getSheet().createRow(i);
            for(int j = 0; j < 10; j++) {
                r.createCell(j);
            }
        }
    }


    @Test
    public void insererNotesNormal() {
        //Création de l'utilisateur
        Row r = this.enj.getSheet().getRow(2);
        r.getCell(COL_ID_UTILISATEUR).setCellValue(1);
        r.getCell(COL_NOM_UTILISATEUR).setCellValue(TEST_NOM_UTILISATEUR + " " + TEST_PRENOM_UTILISATEUR);

        Utilisateur user = new Utilisateur();
        user.setIdUtilisateur((long)TEST_ID_UTILISATEUR);
        user.setNom(TEST_NOM_UTILISATEUR);
        user.setPrenom(TEST_PRENOM_UTILISATEUR);

        ENJMatiere mat1 = new ENJMatiere();
        ENJMatiere mat2 = new ENJMatiere();
        mat1.setLibelle("Poster");
        mat2.setLibelle("Soutenance");
        mat1.setCoefficient(0.4f);
        mat2.setCoefficient(0.6f);

        ENJNotes notes = new ENJNotes();
        notes.addNote(mat1, TEST_NOTE_POSTER);
        notes.addNote(mat2, TEST_NOTE_SOUTENANCE);

        HashMap<Utilisateur, ENJNotes> map = new HashMap<>();
        map.put(user, notes);

        List<ENJMatiere> matieres = new ArrayList<>();
        matieres.add(mat1);
        matieres.add(mat2);

        //Insertion
        this.enj.insererNotes(map, matieres, ExportType.NORMAL);

        //Test
        assertEquals(TEST_NOTE_POSTER, (float)r.getCell(COL_NOTE).getNumericCellValue(), DELTA);
        assertEquals(TEST_NOTE_SOUTENANCE, (float)r.getCell(COL_NOTE+2).getNumericCellValue(), DELTA);
    }

    @Test
    public void insererNotesAutocomplete() throws IOException {
        //Création de la ligne par défaut
        Row row = this.enj.getSheet().getRow(2);
        row.getCell(COL_ID_UTILISATEUR).setCellValue(1);
        row.getCell(COL_NOM_UTILISATEUR).setCellValue(TEST_NOM_UTILISATEUR + " " + TEST_PRENOM_UTILISATEUR);


        //Création des utilisateurs
        String[] noms       = new String[]{"HAMMOUDI", "LE GACQUE", "MERAND", "RENAUD", "ROINEAU"};
        String[] prenoms    = new String[]{"Slimane", "Tristan", "Céline", "Martin", "Corentin"};
        float[] notesP      = new float[]{8.4f, 11.5f, 5, 18.8f, 14};
        float[] notesS      = new float[]{17, 4.5f, 16.9f, 7, 12.2f};
        HashMap<Utilisateur, ENJNotes> map = new HashMap<>();

        ENJMatiere mat1 = new ENJMatiere();
        ENJMatiere mat2 = new ENJMatiere();
        mat1.setLibelle("Poster");
        mat2.setLibelle("Soutenance");
        mat1.setCoefficient(0.4f);
        mat2.setCoefficient(0.6f);

        List<ENJMatiere> matieres = new ArrayList<>();
        matieres.add(mat1);
        matieres.add(mat2);

        for(int i = 0; i < notesP.length; i++) {
            Utilisateur user = new Utilisateur();
            ENJNotes notes = new ENJNotes();

            user.setIdUtilisateur(i+1L);
            user.setNom(noms[i]);
            user.setPrenom(prenoms[i]);

            notes.addNote(mat1, notesP[i]);
            notes.addNote(mat2, notesS[i]);
            map.put(user, notes);
        }

        //Insertion
        this.enj.insererNotes(map, matieres, ExportType.AUTOCOMPLETE);
        //this.enj.enregistrerFichier(FileUtils.openOutputStream(new File("C:\\Users\\trist\\IdeaProjects\\ProjetLD2018\\out_log/file.xlsx")));
        //Test
        for(Map.Entry<Utilisateur, ENJNotes> u : map.entrySet()) {
            int rowNum = u.getKey().getIdUtilisateur().intValue() + HEADER_ROW;
            Row r = this.enj.getSheet().getRow(rowNum);

            float noteS = u.getValue().getNotes().get(mat2);
            float noteP =  u.getValue().getNotes().get(mat1);

            System.out.println("Ligne n°" + (rowNum));
            System.out.println("NOM => " + u.getKey().getNom());

            System.out.println(r.getCell(COL_NOM_UTILISATEUR).getStringCellValue());

            assertEquals(noteP, (float)r.getCell(COL_NOTE).getNumericCellValue(), DELTA);
            assertEquals(noteS, (float)r.getCell(COL_NOTE+2).getNumericCellValue(), DELTA);
        }

    }

    @Ignore
    public void enregistrerFichier() throws IOException {
        File f = new File("To define");
        this.enj.enregistrerFichier(new FileOutputStream(f));
    }

    @Test
    public void getEtudiantsANoterFromExcel() {
    }

    @Test
    public void getSheet() throws NoSuchFieldException, IllegalAccessException {
        Field f = this.enj.getClass().getDeclaredField("sheet");
        f.setAccessible(true);
        assertEquals(f.get(this.enj), this.enj.getSheet());
    }

    @Test
    public void getWorkbook() throws NoSuchFieldException, IllegalAccessException {
        Field f = this.enj.getClass().getDeclaredField("workbook");
        f.setAccessible(true);
        assertEquals(f.get(this.enj), this.enj.getWorkbook());
    }

    @Test
    public void getColonnesNotes() throws NoSuchFieldException, IllegalAccessException {
        Field f = this.enj.getClass().getDeclaredField("colonnesNotes");
        f.setAccessible(true);
        assertEquals(f.get(this.enj), this.enj.getColonnesNotes());
    }

    @Test
    public void setColonneNote() throws NoSuchFieldException, IllegalAccessException {
        HashMap<ENJParams, Integer> params = new HashMap<>();
        params.put(ENJParams.COL_ID_UTILISATEUR, COL_ID_UTILISATEUR);
        params.put(ENJParams.COL_NOM_UTILISATEUR, COL_NOM_UTILISATEUR);
        params.put(ENJParams.COL_NOTE, COL_NOTE);
        params.put(ENJParams.COL_GRADE, COL_GRADE);
        params.put(ENJParams.COL_MOYENNE, COL_MOYENNE);
        params.put(ENJParams.COL_COMMENTAIRE, COL_COMMENTAIRE);
        for(Map.Entry<ENJParams, Integer> param : params.entrySet()) {
            this.enj.setColonneNote(param.getKey(), param.getValue());
        }

        Field f = this.enj.getClass().getDeclaredField("colonnesNotes");
        f.setAccessible(true);
        HashMap<ENJParams, Integer> recupParams = (HashMap<ENJParams, Integer>) f.get(this.enj);
        for(Map.Entry<ENJParams, Integer> param : recupParams.entrySet()) {
            assertEquals(param.getValue(), params.get(param.getKey()));
        }

    }

}