package fr.eseo.ld.config;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

/**
 * Suite de tests unitaires JUnit 4 pour le package fr.eseo.ld.config.
 * 
 * @author Maxime LENORMAND
 * 
 * @see org.junit
 */
@RunWith(Suite.class)
@SuiteClasses({
	InitialisationDAOFactoryTest.class
})
public class ConfigTestSuite {
	/* Classe vide */
}