package fr.eseo.ld.config;

import fr.eseo.ld.dao.DAOFactory;
import org.easymock.*;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;

import static org.easymock.EasyMock.anyString;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.expectLastCall;

/**
 * Classe de tests unitaires JUnit 4 de la classe InitialisationDAOFactory.
 *
 * <p>Utilisation de mocks (construits avec EasyMock).</p>
 *
 * @version 1.0
 * @author Maxime LENORMAND
 *
 * @see fr.eseo.ld.config.InitialisationDAOFactory
 * @see org.junit
 * @see org.easymock
 */
@RunWith(EasyMockRunner.class)
public class InitialisationDAOFactoryTest extends EasyMockSupport {
	
	private static final String ATT_DAO_FACTORY = "daoFactory";
	
	@TestSubject
	private InitialisationDAOFactory initialisationDAOFactory = new InitialisationDAOFactory();
	
	@Mock
	private ServletContextEvent servletContextEvenMock;
	
	@Mock(type = MockType.NICE)
	private ServletContext servletContextMock;

	/**
	 * Teste la méthode public void contextInitialized(ServletContextEvent event).
	 */
	@Test
	public void testContextInitialized() {
		/* Récupération du ServletContext lors du chargement de l'application */
		expect(this.servletContextEvenMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getRealPath(anyString())).andReturn("/logs/nothing.doc");
		/* Instanciation de notre DAOFactory */
		final DAOFactory daoFactory = DAOFactory.getInstance();

		/* Enregistrement dans un attribut ayant pour portée toute l'application */
		this.servletContextMock.setAttribute(ATT_DAO_FACTORY, daoFactory);
		expectLastCall().asStub();
		
		this.replayAll();
		
		/* Cycle de vie d'un ServletContextListener */
		this.initialisationDAOFactory.contextInitialized(this.servletContextEvenMock);
		this.initialisationDAOFactory.contextDestroyed(this.servletContextEvenMock);
		
		this.verifyAll();
	}

}