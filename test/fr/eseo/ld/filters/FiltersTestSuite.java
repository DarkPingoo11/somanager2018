package fr.eseo.ld.filters;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

/**
 * Suite de tests unitaires JUnit 4 pour le package fr.eseo.ld.filters.
 * 
 * @author Maxime LENORMAND
 * 
 * @see org.junit
 */
@RunWith(Suite.class)
@SuiteClasses({
	AdministrateurRestrictionTest.class,
	AjouterSujetRestrictionTest.class,
	ConnexionRestrictionTest.class,
	CreerEquipeRestrictionTest.class,
	FilterUtilitaireTest.class,
	GererSujetsRestrictionTest.class,
	ProfResponsableRestrictionTest.class,
	ProfsRestrictionTest.class,
	RejoindreRestrictionTest.class
})
public class FiltersTestSuite {
	/* Classe vide */
}