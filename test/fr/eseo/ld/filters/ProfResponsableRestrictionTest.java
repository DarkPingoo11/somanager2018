package fr.eseo.ld.filters;

import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.expectLastCall;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.easymock.EasyMockRunner;
import org.easymock.EasyMockSupport;
import org.easymock.Mock;
import org.easymock.TestSubject;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import fr.eseo.ld.beans.Role;
import fr.eseo.ld.beans.Utilisateur;
import fr.eseo.ld.dao.DAOFactory;
import fr.eseo.ld.dao.NotificationDAO;

/**
 * Classe de tests unitaires JUnit 4 de la classe ProfResponsableRestriction.
 *
 * <p>Utilisation de mocks (construits avec EasyMock).</p>
 *
 * @version 1.0
 * @author Maxime LENORMAND
 *
 * @see fr.eseo.ld.filters.ProfResponsableRestriction
 * @see org.junit
 * @see org.easymock
 */
@RunWith(EasyMockRunner.class)
public class ProfResponsableRestrictionTest extends EasyMockSupport {

	private static final String CONF_DAO_FACTORY = "daoFactory";
	
	private static final String ATT_SESSION_USER = "utilisateur";
	private static final String ATT_SESSION_ROLES = "roles";
	
	private static final Long ID_UTILISATEUR = 11L;
	
	private static final String CONTEXT_PATH = "SoManager/GererApplication";
	private static final String VUE_INDEX = "/Dashboard";
	
	private static NotificationDAO notificationDAO;
	
	@TestSubject
	private ProfResponsableRestriction profResponsableRestriction = new ProfResponsableRestriction();

	@Mock
	private FilterConfig filterConfigMock;
	
	@Mock
	private ServletContext servletContextMock;

	@Mock
	private HttpServletRequest httpServletRequestMock;
	
	@Mock
	private HttpServletResponse httpservletResponseMock;
	
	@Mock
	private FilterChain filterChainMock;
	
	@Mock
	private HttpSession httpSessionMock;
	
	@Mock
	private DAOFactory daoFactoryMock;
	
	/**
	 * Récupère des instances de DAO.
	 * 
	 * @throws Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		notificationDAO = DAOFactory.getInstance().getNotificationDao();
	}

	/**
	 * Teste la méthode public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain).
	 * 
	 * <p>Cas où l'utilisateur possède les bons rôles : affichage de la page restreinte.</p>
	 * 
	 * @throws IOException
	 * @throws ServletException
	 */
	@Test
	public void testDoFilterBonsRoles() throws IOException, ServletException {
		/* Mock de la méthode init(FilterConfig fConfig) afin de simplifier le test */
		this.profResponsableRestriction = partialMockBuilder(ProfResponsableRestriction.class).addMockedMethod("init").createMock();
		this.profResponsableRestriction.init(this.filterConfigMock);
		expectLastCall();
		
		/* Cas où l'utilisateur possède les bons rôles */
		final Role bonRole = new Role();
		bonRole.setNomRole("profResponsable");
		final List<Role> listeRoles = new ArrayList<>();
		listeRoles.add(bonRole);
		expect(this.httpServletRequestMock.getSession()).andReturn(this.httpSessionMock);
		expect(this.httpSessionMock.getAttribute(ATT_SESSION_ROLES)).andReturn(listeRoles);

		/* Affichage de la page restreinte */
		this.filterChainMock.doFilter(this.httpServletRequestMock, this.httpservletResponseMock);
		expectLastCall();
		
		this.replayAll();

		/* Cycle de vie d'un filtre */
		this.profResponsableRestriction.init(this.filterConfigMock);
		this.profResponsableRestriction.doFilter(this.httpServletRequestMock, this.httpservletResponseMock, this.filterChainMock);
		this.profResponsableRestriction.destroy();

		this.verifyAll();
	}
	
	/**
	 * Teste la méthode public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain).
	 * 
	 * <p>Cas où l'utilisateur ne possède pas les bons rôles : redirection vers la page d'accueil.</p>
	 * 
	 * @throws IOException
	 * @throws ServletException
	 */
	@Test
	public void testDoFilterMauvaisRoles() throws IOException, ServletException {
		/* Récupération d'instances de DAO */
		expect(this.filterConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
		expect(this.daoFactoryMock.getNotificationDao()).andReturn(notificationDAO);
		
		/* Cas où l'utilisateur ne possède pas les bons rôles */
		final Role mauvaisRole = new Role();
		mauvaisRole.setNomRole("etudiant");
		final List<Role> listeRoles = new ArrayList<>();
		listeRoles.add(mauvaisRole);
		expect(this.httpServletRequestMock.getSession()).andReturn(this.httpSessionMock);
		expect(this.httpSessionMock.getAttribute(ATT_SESSION_ROLES)).andReturn(listeRoles);
		final Utilisateur utilisateur = new Utilisateur();
		utilisateur.setIdUtilisateur(ID_UTILISATEUR);
		expect(this.httpSessionMock.getAttribute(ATT_SESSION_USER)).andReturn(utilisateur);

		/* Redirection vers la page d'accueil */
		expect(this.httpServletRequestMock.getContextPath()).andReturn(CONTEXT_PATH);
		this.httpservletResponseMock.sendRedirect(CONTEXT_PATH + VUE_INDEX);
		expectLastCall();

		this.replayAll();

		/* Cycle de vie d'un filtre */
		this.profResponsableRestriction.init(this.filterConfigMock);
		this.profResponsableRestriction.doFilter(this.httpServletRequestMock, this.httpservletResponseMock, this.filterChainMock);
		this.profResponsableRestriction.destroy();

		this.verifyAll();
	}
	
	/**
	 * Teste la méthode public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain).
	 * 
	 * <p>Cas où l'utilisateur ne possède aucun rôle : redirection vers la page d'accueil.</p>
	 * 
	 * @throws IOException
	 * @throws ServletException
	 */
	@Test
	public void testDoFilterAucunRole() throws IOException, ServletException {
		/* Récupération d'instances de DAO */
		expect(this.filterConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
		expect(this.daoFactoryMock.getNotificationDao()).andReturn(notificationDAO);
		
		/* Cas où l'utilisateur ne possède aucun rôle */
		expect(this.httpServletRequestMock.getSession()).andReturn(this.httpSessionMock);
		expect(this.httpSessionMock.getAttribute(ATT_SESSION_ROLES)).andReturn(null);
		final Utilisateur utilisateur = new Utilisateur();
		utilisateur.setIdUtilisateur(ID_UTILISATEUR);
		expect(this.httpSessionMock.getAttribute(ATT_SESSION_USER)).andReturn(utilisateur);

		/* Redirection vers la page d'accueil */
		expect(this.httpServletRequestMock.getContextPath()).andReturn(CONTEXT_PATH);
		this.httpservletResponseMock.sendRedirect(CONTEXT_PATH + VUE_INDEX);
		expectLastCall();

		this.replayAll();

		/* Cycle de vie d'un filtre */
		this.profResponsableRestriction.init(this.filterConfigMock);
		this.profResponsableRestriction.doFilter(this.httpServletRequestMock, this.httpservletResponseMock, this.filterChainMock);
		this.profResponsableRestriction.destroy();

		this.verifyAll();
	}

}