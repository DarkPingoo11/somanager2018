package fr.eseo.ld.filters;

import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.expectLastCall;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.easymock.EasyMockRunner;
import org.easymock.EasyMockSupport;
import org.easymock.Mock;
import org.easymock.TestSubject;
import org.junit.Test;
import org.junit.runner.RunWith;

import fr.eseo.ld.beans.Utilisateur;

/**
 * Classe de tests unitaires JUnit 4 de la classe ConnexionRestriction.
 *
 * <p>Utilisation de mocks (construits avec EasyMock).</p>
 *
 * @version 1.0
 * @author Maxime LENORMAND
 *
 * @see fr.eseo.ld.filters.ConnexionRestriction
 * @see org.junit
 * @see org.easymock
 */
@RunWith(EasyMockRunner.class)
public class ConnexionRestrictionTest extends EasyMockSupport {
	
	private static final String ATT_SESSION_USER = "utilisateur";
	private static final String VUE_CONNEXION = "/WEB-INF/formulaires/connexion.jsp";
	
	@TestSubject
	private ConnexionRestriction connexionRestriction = new ConnexionRestriction();

	@Mock
	private FilterConfig filterConfigMock;

	@Mock
	private HttpServletRequest httpServletRequestMock;
	
	@Mock
	private HttpServletResponse httpservletResponseMock;
	
	@Mock
	private FilterChain filterChainMock;
	
	@Mock
	private HttpSession httpSessionMock;
	
	@Mock
	private RequestDispatcher requestDispatcherMock;

	/**
	 * Teste la méthode public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain).
	 * 
	 * <p>Cas où l'utilisateur est connecté : affichage de la page restreinte.</p>
	 * 
	 * @throws IOException
	 * @throws ServletException
	 */
	@Test
	public void testDoFilterConnecte() throws IOException, ServletException {
		/* Cas où l'utilisateur est connecté */
		final Utilisateur utilisateur = new Utilisateur();
		expect(this.httpServletRequestMock.getSession()).andReturn(this.httpSessionMock);
		expect(this.httpSessionMock.getAttribute(ATT_SESSION_USER)).andReturn(utilisateur);

		/* Affichage de la page restreinte */
		this.filterChainMock.doFilter(this.httpServletRequestMock, this.httpservletResponseMock);
		expectLastCall();
		
		this.replayAll();

		/* Cycle de vie d'un filtre */
		this.connexionRestriction.init(this.filterConfigMock);
		this.connexionRestriction.doFilter(this.httpServletRequestMock, this.httpservletResponseMock, this.filterChainMock);
		this.connexionRestriction.destroy();

		this.verifyAll();
	}
	
	/**
	 * Teste la méthode public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain).
	 * 
	 * <p>Cas où l'utilisateur est non connecté : redirection vers la page de connexion.</p>
	 * 
	 * @throws IOException
	 * @throws ServletException
	 */
	@Test
	public void testDoFilterNonConnecte() throws IOException, ServletException {
		/* Cas où l'utilisateur est non connecté */
		expect(this.httpServletRequestMock.getSession()).andReturn(this.httpSessionMock);
		expect(this.httpSessionMock.getAttribute(ATT_SESSION_USER)).andReturn(null);

		/* Redirection vers la page de connexion */
		expect(this.httpServletRequestMock.getRequestDispatcher(VUE_CONNEXION)).andReturn(this.requestDispatcherMock);
		this.requestDispatcherMock.forward(this.httpServletRequestMock, this.httpservletResponseMock);
		expectLastCall();

		this.replayAll();

		/* Cycle de vie d'un filtre */
		this.connexionRestriction.init(this.filterConfigMock);
		this.connexionRestriction.doFilter(this.httpServletRequestMock, this.httpservletResponseMock, this.filterChainMock);
		this.connexionRestriction.destroy();

		this.verifyAll();
	}

}