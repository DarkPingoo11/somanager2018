package fr.eseo.ld.filters;

import static org.junit.Assert.assertTrue;

import java.lang.reflect.Constructor;
import java.lang.reflect.Modifier;

import org.junit.Test;

/**
 * Classe de tests unitaires JUnit 4 de la classe FilterUtilitaire.
 * 
 * <p>Utilisation de la réflexivité.</p>
 *
 * @version 1.0
 * @author Maxime LENORMAND
 *
 * @see fr.eseo.ld.filters.FilterUtilitaire
 * @see org.junit
 */
public class FilterUtilitaireTest {

	/**
	 * Teste le constructeur privé de la classe utilitaire grâce à la réflexion.
	 * 
	 * @throws Exception
	 */
	@Test
	public void testConstructeurPrive() throws Exception {
		final Constructor<FilterUtilitaire> constructeur = FilterUtilitaire.class.getDeclaredConstructor();
		assertTrue("Le constructeur n'est pas privé", Modifier.isPrivate(constructeur.getModifiers()));
		constructeur.setAccessible(true);
		constructeur.newInstance();
	}

}