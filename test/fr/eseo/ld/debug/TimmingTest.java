package fr.eseo.ld.debug;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TimmingTest {

    @Test
    @SuppressWarnings({"squid:S2925"})
    public void testCalcul1Seconde() throws InterruptedException {
        Timming t = Timming.startNewTimming();
        Thread.sleep(1000);
        t.stop();

        assertEquals(t.getDurationInMillis(), 1000, 1);
    }

}