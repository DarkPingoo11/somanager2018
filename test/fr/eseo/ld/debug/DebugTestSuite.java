package fr.eseo.ld.debug;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

/**
 * Suite de tests unitaires JUnit 4
 *
 * @author Tristan LE GACQUE
 */
@RunWith(Suite.class)
@SuiteClasses({
       TimmingTest.class
})
public class DebugTestSuite {
} 


