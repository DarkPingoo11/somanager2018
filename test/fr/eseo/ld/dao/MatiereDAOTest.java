package fr.eseo.ld.dao;

import fr.eseo.ld.beans.AnneeScolaire;
import fr.eseo.ld.beans.OptionESEO;
import fr.eseo.ld.pgl.beans.Matiere;
import fr.eseo.ld.pgl.beans.Sprint;
import fr.eseo.testing.DAOTesting;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.sql.SQLException;

/**
 * Classe de test de MatiereDAO
 *
 * @author Tristan LE GACQUE
 * @see MatiereDAO
 **/
public class MatiereDAOTest {

    private static DAOTesting<Matiere> daoTesting;

    /**
     * Définition du DaoTesting
     *
     * @throws Exception exception
     */
    @BeforeClass
    public static void setUp() throws Exception {
        //Définition du dao factory
        daoTesting = new DAOTesting<>();
        daoTesting.initDao(daoTesting.getDaoFactory().getMatiereDAO());

        daoTesting.addAlias(Sprint.class, "pgl_sprint");
        daoTesting.addAlias(Matiere.class, "pgl_matiere");

        daoTesting.truncateTable(OptionESEO.class);
        daoTesting.truncateTable(AnneeScolaire.class);
        daoTesting.truncateTable(Sprint.class);
    }

    @AfterClass
    public static void tearDown() throws Exception {
        daoTesting.truncateUsedTables();
    }

    @Before
    public void beforeTest() throws SQLException, IllegalAccessException {
        daoTesting.truncateUsedTables();

        //Définition des dépendances
        OptionESEO optionESEO = daoTesting.getOptionESEO();
        AnneeScolaire anneeScolaire = daoTesting.getAnneeScolaire();
        Sprint sprint = daoTesting.getSprint();

        //Ajout des objets a la base de données
        daoTesting.createObjectInDatabase(optionESEO);
        daoTesting.createObjectInDatabase(anneeScolaire);
        daoTesting.createObjectInDatabase(sprint);
        //Supprimer les Matières crées par défaut
        daoTesting.truncateTable(Matiere.class);
    }


    /**
     * Teste la méthode public void creer.
     *
     * @see MatiereDAO#creer(Matiere)
     */
    @Test
    public void testCreer() {
        /* Insertion du bean dans la BDD */
        Matiere matiere = daoTesting.getMatiere();
        Matiere matiereAttendu = daoTesting.getMatiere();

        daoTesting.testCreerCRUD(matiere, matiereAttendu);
    }

    /**
     * Teste la méthode public trouver
     *
     * @see MatiereDAO#trouver(Matiere)
     */
    @Test
    public void testTrouver() {
        /* Recherche du bean dans la BDD */
        Matiere matiere = daoTesting.getMatiere();
        Matiere matiereAttendu = daoTesting.getMatiere();

        daoTesting.testTrouverCRUD(matiere, matiereAttendu);
    }


    /**
     * Teste la méthode public void modifier
     *
     * @see MatiereDAO#modifier(Matiere)
     */
    @Test
    public void testModifier() {
        Matiere matiere = daoTesting.getMatiere();
        Matiere matiereMod = daoTesting.getMatiere();

        matiereMod.setLibelle("autreM");

        daoTesting.testModifierCRUD(matiere, matiereMod, matiereMod);
    }

    /**
     * Teste la méthode public lister().
     *
     * @see MatiereDAO#lister()
     */
    @Test
    public void testLister() {
        Matiere matiere = daoTesting.getMatiere();
        Matiere matiereAttendu = daoTesting.getMatiere();

        daoTesting.testListerCRUD(new Matiere[]{matiere}, new Matiere[]{matiereAttendu});
    }

    /**
     * Teste la méthode public void supprimer(NoteInteretTechno NoteInteretTechno).
     *
     * @see MatiereDAO#supprimer(Matiere)
     */
    @Test
    public void testSupprimer() throws IllegalAccessException {
        Matiere matiere = daoTesting.getMatiere();

        daoTesting.testSupprimerCRUD(matiere);
    }

    /**
     * Test les erreurs
     */
    @Test
    public void testLogging() {
        daoTesting.testLoggingCRUD();
    }

    @Test
    public void trouver() {
    }
}