package fr.eseo.ld.dao;

import fr.eseo.ld.beans.ModeleExcel;
import fr.eseo.testing.DAOTesting;
import org.junit.*;

import java.sql.SQLException;
import java.util.Random;

public class ModeleExcelDAOTest {

    private static final String NOM_FICHIER = "Modele de test CRUD";
    private static final Integer LIGNE_HEADER = 12;
    private static final Integer LIGNE_HEADER_MODIF = 8;

    private static DAOTesting<ModeleExcel> daoTesting;

    @BeforeClass
    public static void setUp() {
        //Définition du dao factory
        daoTesting = new DAOTesting<>();
        daoTesting.initDao(daoTesting.getDaoFactory().getModeleExcelDAO());
    }

    @AfterClass
    public static void tearDown() throws Exception {
        daoTesting.truncateTable();
    }

    @Before
    public void beforeTest() throws SQLException {
        daoTesting.truncateTable();
    }

    @Test
    public void creer() {
        ModeleExcel modele = new ModeleExcel();
        modele.setNomFichier(NOM_FICHIER);
        modele.setLigneHeader(LIGNE_HEADER);

        ModeleExcel modeleAttendu = getModeleExcelAttendu();

        daoTesting.testCreerCRUD(modele, modeleAttendu);
    }

    @Test
    public void trouver() {
        ModeleExcel modele = new ModeleExcel();
        modele.setNomFichier(NOM_FICHIER);
        modele.setLigneHeader(LIGNE_HEADER);

        ModeleExcel modeleAttendu = getModeleExcelAttendu();

        daoTesting.testTrouverCRUD(modele, modeleAttendu);
    }

    @Test
    public void modifier() {
        ModeleExcel modele = new ModeleExcel();
        modele.setNomFichier(NOM_FICHIER);
        modele.setLigneHeader(LIGNE_HEADER);

        ModeleExcel modeleModifier = new ModeleExcel();
        modeleModifier.setNomFichier(NOM_FICHIER);
        modeleModifier.setLigneHeader(LIGNE_HEADER_MODIF);

        ModeleExcel modeleAttendu = getModeleExcelAttendu();
        modeleAttendu.setNomFichier(NOM_FICHIER);
        modeleAttendu.setLigneHeader(LIGNE_HEADER_MODIF);

        daoTesting.testModifierCRUD(modele, modeleModifier, modeleAttendu);
    }

    @Test
    public void supprimer() {
        ModeleExcel modele = new ModeleExcel();
        modele.setNomFichier(NOM_FICHIER);
        modele.setLigneHeader(LIGNE_HEADER);

        daoTesting.testSupprimerCRUD(modele);
    }

    @Test
    public void lister() {
        ModeleExcel[] modeles = new ModeleExcel[10];
        ModeleExcel[] modelesAttendus = new ModeleExcel[10];

        for(int i = 0; i < modeles.length; i++) {
            Random r = new Random();
            Integer col1 = r.nextInt(100);
            Integer col2 = r.nextInt(100);

            modeles[i] = new ModeleExcel();
            modeles[i].setNomFichier(NOM_FICHIER + i);
            modeles[i].setLigneHeader(LIGNE_HEADER);
            modeles[i].setColId(col1);
            modeles[i].setColNom(col2);

            modelesAttendus[i] = getModeleExcelAttendu();
            modelesAttendus[i].setNomFichier(NOM_FICHIER + i);
            modelesAttendus[i].setLigneHeader(LIGNE_HEADER);
            modelesAttendus[i].setColId(col1);
            modelesAttendus[i].setColNom(col2);
        }

        daoTesting.testListerCRUD(modeles, modelesAttendus);
    }

    private ModeleExcel getModeleExcelAttendu() {
        ModeleExcel modeleAttendu = new ModeleExcel();
        modeleAttendu.setNomFichier(NOM_FICHIER);
        modeleAttendu.setLigneHeader(LIGNE_HEADER);
        modeleAttendu.setColNote(0);
        modeleAttendu.setColCom(0);
        modeleAttendu.setColNom(0);
        modeleAttendu.setColId(0);
        modeleAttendu.setColMoyenne(0);
        modeleAttendu.setColGrade(0);
        return modeleAttendu;
    }
}