package fr.eseo.ld.dao;

import fr.eseo.ld.beans.*;
import fr.eseo.ld.pgl.beans.Equipe;
import fr.eseo.ld.pgl.beans.RoleEquipe;
import fr.eseo.ld.pgl.beans.Sprint;
import fr.eseo.testing.DAOTesting;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.sql.SQLException;

/**
 * Classe de test de RoleEquipeDAO
 *
 * @author Tristan LE GACQUE
 * @see PglRoleEquipeDAO
 **/
public class PglRoleEquipeDAOTest {

    private static DAOTesting<RoleEquipe> daoTesting;

    /**
     * Définition du DaoTesting
     *
     * @throws Exception exception
     */
    @BeforeClass
    public static void setUp() throws Exception {
        //Définition du dao factory
        daoTesting = new DAOTesting<>();
        daoTesting.initDao(daoTesting.getDaoFactory().getPglRoleEquipeDAO());

        daoTesting.addAlias(Equipe.class, "pgl_equipe");
        daoTesting.addAlias(Sprint.class, "pgl_sprint");
        daoTesting.addAlias(RoleEquipe.class, "pgl_roleequipe");
    }

    @AfterClass
    public static void tearDown() throws Exception {
        daoTesting.truncateUsedTables();
    }

    @Before
    public void beforeTest() throws SQLException, IllegalAccessException {
        daoTesting.truncateUsedTables();

        //Définition des dépendances
        OptionESEO optionESEO = daoTesting.getOptionESEO();
        AnneeScolaire anneeScolaire = daoTesting.getAnneeScolaire();

        Utilisateur user = daoTesting.getUtilisateur();
        Professeur professeur = daoTesting.getProfesseur();
        Sprint sprint = daoTesting.getSprint();
        Equipe equipe = daoTesting.getEquipePGL();

        Role role = daoTesting.getRole();
        role.setType(RoleType.APPLICATIF.getNom());

        //Ajout des objets a la base de données
        daoTesting.createObjectInDatabase(optionESEO);
        daoTesting.createObjectInDatabase(anneeScolaire);
        daoTesting.createObjectInDatabase(user);
        daoTesting.createObjectInDatabase(professeur);
        daoTesting.createObjectInDatabase(sprint);
        daoTesting.createObjectInDatabase(equipe);
        daoTesting.createObjectInDatabase(role);
    }


    /**
     * Teste la méthode public void creer.
     *
     * @see PglRoleEquipeDAO#creer(RoleEquipe)
     */
    @Test
    public void testCreer() {
        /* Insertion du bean dans la BDD */
        RoleEquipe RoleEquipe = daoTesting.getRoleEquipe();
        RoleEquipe RoleEquipeAttendu = daoTesting.getRoleEquipe();

        daoTesting.testCreerCRUD(RoleEquipe, RoleEquipeAttendu);
    }

    /**
     * Teste la méthode public trouver
     *
     * @see PglRoleEquipeDAO#trouver(RoleEquipe)
     */
    @Test
    public void testTrouver() {
        /* Recherche du bean dans la BDD */
        RoleEquipe RoleEquipe = daoTesting.getRoleEquipe();
        RoleEquipe RoleEquipeAttendu = daoTesting.getRoleEquipe();

        daoTesting.testTrouverCRUD(RoleEquipe, RoleEquipeAttendu);
    }


    /**
     * Teste la méthode public void modifier
     *
     * @see PglRoleEquipeDAO#modifier(RoleEquipe)
     */
    @Test
    public void testModifier() {
        RoleEquipe RoleEquipe = daoTesting.getRoleEquipe();
        RoleEquipe RoleEquipeMod = daoTesting.getRoleEquipe();

        daoTesting.testModifierCRUD(RoleEquipe, RoleEquipeMod, RoleEquipeMod);
    }

    /**
     * Teste la méthode public lister().
     *
     * @see PglRoleEquipeDAO#lister()
     */
    @Test
    public void testLister() {
        RoleEquipe RoleEquipe = daoTesting.getRoleEquipe();
        RoleEquipe RoleEquipeAttendu = daoTesting.getRoleEquipe();

        daoTesting.testListerCRUD(new RoleEquipe[]{RoleEquipe}, new RoleEquipe[]{RoleEquipeAttendu});
    }

    /**
     * Teste la méthode public void supprimer(NoteInteretTechno NoteInteretTechno).
     *
     * @see PglRoleEquipeDAO#supprimer(RoleEquipe)
     */
    @Test
    public void testSupprimer() {
        RoleEquipe RoleEquipe = daoTesting.getRoleEquipe();

        daoTesting.testSupprimerCRUD(RoleEquipe);
    }

    /**
     * Test les erreurs
     */
    @Test
    public void testLogging() {
        daoTesting.testLoggingCRUD();
    }
}