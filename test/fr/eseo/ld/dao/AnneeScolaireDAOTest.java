package fr.eseo.ld.dao;

import fr.eseo.ld.beans.AnneeScolaire;
import fr.eseo.ld.beans.OptionESEO;
import fr.eseo.testing.DAOTesting;
import org.apache.log4j.*;
import org.easymock.EasyMock;
import org.easymock.EasyMockRunner;
import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

import java.io.ByteArrayOutputStream;
import java.sql.SQLException;

import static org.easymock.EasyMock.*;
import static org.junit.Assert.assertTrue;

/**
 * Classe de tests unitaires JUnit 4 de la classe AnneeScolaireDAO.
 *
 * <p>Utilisation du modèle DAO.</p>
 *
 * @version 1.0
 * @author Maxime LENORMAND
 *
 * @see fr.eseo.ld.dao.AnneeScolaireDAO
 * @see org.junit
 */
@RunWith(EasyMockRunner.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class AnneeScolaireDAOTest {
	
	private static final Long LONG_TEST = 1L;
	private static final Integer INTEGER_TEST = 2017;
	private static final Integer INTEGER_TEST_MODIFIE = 2018;
	
	private static Logger logger = Logger.getLogger(AnneeScolaireDAO.class.getName());
	
	private static AnneeScolaireDAO anneeScolaireDAO;
	private static DAOTesting<AnneeScolaire> daoTesting;
	@BeforeClass
	public static void setUp() throws Exception {
		//Définition du dao factory
		daoTesting = new DAOTesting<>();
		daoTesting.initDao(daoTesting.getDaoFactory().getAnneeScolaireDAO());
		anneeScolaireDAO = daoTesting.getDaoFactory().getAnneeScolaireDAO();

		daoTesting.truncateTable(OptionESEO.class);
	}

	@AfterClass
	public static void tearDown() throws Exception {
		daoTesting.truncateUsedTables();
	}

	@Before
	public void beforeTest() throws SQLException, IllegalAccessException {
		daoTesting.truncateUsedTables();

		daoTesting.createObjectInDatabase(daoTesting.getOptionESEO());
	}

	/**
	 * Teste la méthode public void creer(AnneeScolaire anneeScolaire).
	 */
	@Test
	public void test1_1Creer() {
		/* Insertion du bean dans la BDD */
		AnneeScolaire as = daoTesting.getAnneeScolaire();

		daoTesting.testCreerCRUD(as, as);
	}
	
	/**
	 * Teste la méthode public void creer(AnneeScolaire anneeScolaire).
	 * <p>Cas d'une erreur lors de la création de la connexion.</p>
	 * @throws SQLException 
	 */
	@Test
	public void test1_2CreerLog() throws SQLException {
		/* Configuraton du logger */
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		Layout layout = new SimpleLayout();
		Appender appender = new WriterAppender(layout, out);
		logger.addAppender(appender);
		
		try {
			/* Mock de la méthode creerConnexion() */
			AnneeScolaireDAO anneeScolaireDAOMock = EasyMock.createMockBuilder(AnneeScolaireDAO.class)
					.addMockedMethod("creerConnexion").createMock();
			expect(anneeScolaireDAOMock.creerConnexion()).andThrow(new SQLException());
			
			replay(anneeScolaireDAOMock);
			
			/* Insertion du bean dans la BDD */
			anneeScolaireDAOMock.creer(daoTesting.getAnneeScolaire());
			
			/* Vérification du logging */
			String messageLog = out.toString();
			assertTrue("Mauvais logging", messageLog.contains("Échec de la création de l'objet"));
			
			verify(anneeScolaireDAOMock);
		} finally {
			/* Configuraton initiale du logger */
			logger.removeAppender(appender);
		}
	}
	
	/**
	 * Teste la méthode public List<AnneeScolaire> trouver(AnneeScolaire anneeScolaire).
	 */
	@Test
	public void test2_1Trouver() {
		/* Recherche du bean dans la BDD */
		AnneeScolaire as = daoTesting.getAnneeScolaire();

		daoTesting.testTrouverCRUD(as, as);
	}
	
	/**
	 * Teste la méthode public List<AnneeScolaire> trouver(AnneeScolaire anneeScolaire).
	 * <p>Cas de la recherche d'un bean sans attributs.</p>
	 */
	@Test
	public void test2_2TrouverLog() {
		/* Configuraton du logger */
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		Layout layout = new SimpleLayout();
		Appender appender = new WriterAppender(layout, out);
		logger.addAppender(appender);
		
		try {
			/* Recherche du bean sans attributs dans la BDD */
			AnneeScolaire anneeScolaire = new AnneeScolaire();
			anneeScolaireDAO.trouver(anneeScolaire);

			/* Vérification du logging */
			String messageLog = out.toString();
			assertTrue("Mauvais logging", messageLog.contains("Échec de la recherche"));
		} finally {
			/* Configuraton initiale du logger */
			logger.removeAppender(appender);
		}
	}
	
	/**
	 * Teste la méthode public void modifier(AnneeScolaire anneeScolaire).
	 */
	@Test
	public void test3_1Modifier() {
		/* Modification du bean dans la BDD */
		AnneeScolaire as = daoTesting.getAnneeScolaire();

		AnneeScolaire asM = daoTesting.getAnneeScolaire();
		asM.setAnneeFin(INTEGER_TEST_MODIFIE);

		daoTesting.testModifierCRUD(as, asM, asM);
	}
	
	/**
	 * Teste la méthode public List<AnneeScolaire> lister().
	 */
	@Test
	public void test4_1Lister() throws IllegalAccessException {
		AnneeScolaire[] annees = new AnneeScolaire[1];
		AnneeScolaire[] anneesAttendus = new AnneeScolaire[1];

		for(int i = 0; i < annees.length; i++) {
			annees[i] = daoTesting.getAnneeScolaire();
			annees[i].setIdAnneeScolaire(i + 1L);

			anneesAttendus[i] = daoTesting.getAnneeScolaire();
			anneesAttendus[i].setIdAnneeScolaire(i + 1L);
		}

		daoTesting.testListerCRUD(annees, anneesAttendus);
	}
	
	/**
	 * Teste la méthode public List<AnneeScolaire> lister().
	 * <p>Cas d'une erreur lors de la création de la connexion.</p>
	 * @throws SQLException 
	 */
	@Test
	public void test4_2ListerLog() throws SQLException {
		/* Configuraton du logger */
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		Layout layout = new SimpleLayout();
		Appender appender = new WriterAppender(layout, out);
		logger.addAppender(appender);
		
		try {
			/* Mock de la méthode creerConnexion() */
			AnneeScolaireDAO anneeScolaireDAOMock = EasyMock.createMockBuilder(AnneeScolaireDAO.class)
					.addMockedMethod("creerConnexion").createMock();
			expect(anneeScolaireDAOMock.creerConnexion()).andThrow(new SQLException());
			
			replay(anneeScolaireDAOMock);
			
			/* Recherche du bean dans la BDD */
			anneeScolaireDAOMock.lister();
			
			/* Vérification du logging */
			String messageLog = out.toString();
			assertTrue("Mauvais logging", messageLog.contains("Échec du listage"));
			
			verify(anneeScolaireDAOMock);
		} finally {
			/* Configuraton initiale du logger */
			logger.removeAppender(appender);
		}
	}
	
	/**
	 * Teste la méthode public void supprimer(AnneeScolaire AnneeScolaire).
	 */
	@Test
	public void test5_1Supprimer() {
		/* Suppression du bean dans la BDD */
		AnneeScolaire anneeScolaire = daoTesting.getAnneeScolaire();
		daoTesting.testSupprimerCRUD(anneeScolaire);
	}
	
	/**
	 * Teste la méthode public void supprimer(AnneeScolaire anneeScolaire).
	 * <p>Cas de la suppression d'un bean non présent dans la BDD.</p>
	 */
	@Test
	public void test5_2SupprimerLog() {
		/* Configuraton du logger */
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		Layout layout = new SimpleLayout();
		Appender appender = new WriterAppender(layout, out);
		logger.addAppender(appender);

		try {
			/* Suppression du bean dans la BDD */
			AnneeScolaire anneeScolaire = daoTesting.getAnneeScolaire();
			anneeScolaireDAO.supprimer(anneeScolaire);

			/* Vérification du logging */
			String messageLog = out.toString();
			assertTrue("Mauvais logging", messageLog.contains("Échec de la suppression"));
		} finally {
			/* Configuraton initiale du logger */
			logger.removeAppender(appender);
		}
	}
	
}