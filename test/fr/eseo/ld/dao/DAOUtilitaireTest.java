package fr.eseo.ld.dao;

import org.junit.Test;

import java.lang.reflect.Constructor;
import java.lang.reflect.Modifier;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import static fr.eseo.ld.dao.DAOUtilitaire.fermetures;
import static org.junit.Assert.assertTrue;

/**
 * Classe de tests unitaires JUnit 4 de la classe DAOUtilitaire.
 * 
 * <p>Utilisation de la réflexivité.</p>
 *
 * @version 1.0
 * @author Maxime LENORMAND
 *
 * @see fr.eseo.ld.dao.DAOUtilitaire
 * @see org.junit
 */
public class DAOUtilitaireTest {
	
	/**
	 * Teste le constructeur privé de la classe utilitaire grâce à la réflexion.
	 * 
	 * @throws Exception
	 */
	@Test
	public void testConstructeurPrive() throws Exception {
		final Constructor<DAOUtilitaire> constructeur = DAOUtilitaire.class.getDeclaredConstructor();
		assertTrue("Le constructeur n'est pas privé", Modifier.isPrivate(constructeur.getModifiers()));
		constructeur.setAccessible(true);
		constructeur.newInstance();
	}
	
	/**
	 * Teste la méthode private static String creationDebutRequete(String choixCRUD, String nomEntite).
	 * <p>Cas d'un choixCRUD inexistant.</p>
	 * 
	 * @throws SQLException 
	 */
	@Test(expected = Exception.class)
	public void testCreationDebutRequeteException() throws SQLException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		String[][] attributs = null;
		try {
			connection = DAOFactory.getInstance().getConnection();
			DAOUtilitaire.initialisationRequetePreparee(connection, "CREATE", "Sujet", attributs, false);
		} catch (SQLException e) {
			throw new DAOException("test", e);
		} finally {
			fermetures(preparedStatement, connection);
		}
	}

}