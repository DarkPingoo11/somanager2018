package fr.eseo.ld.dao;

import fr.eseo.ld.beans.*;
import fr.eseo.ld.objets.exportexcel.ENJNotes;
import fr.eseo.testing.DAOTesting;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNotNull;

/**
 * Classe de test de UtilisateurDAO
 *
 * @author Tristan LE GACQUE
 * @see UtilisateurDAO
 **/
public class UtilisateurDAOTest {

    private static DAOTesting<Utilisateur> daoTesting;

    /**
     * Définition du DaoTesting
     *
     * @throws Exception exception
     */
    @BeforeClass
    public static void setUp() throws Exception {
        //Définition du dao factory
        daoTesting = new DAOTesting<>();
        daoTesting.initDao(daoTesting.getDaoFactory().getUtilisateurDao());
        daoTesting.addAlias(Meeting.class, "Reunion");
        daoTesting.addAlias(Invite.class, "invite_reunion");

        daoTesting.truncateTable(Utilisateur.class);
        daoTesting.truncateTable(OptionESEO.class);
        daoTesting.truncateTable(AnneeScolaire.class);
        daoTesting.truncateTable(Role.class);
        daoTesting.truncateTable(RoleUtilisateur.class);
        daoTesting.truncateTable(Etudiant.class);
        daoTesting.truncateTable(PorteurSujet.class);
        daoTesting.truncateTable(Professeur.class);
        daoTesting.truncateTable(ProfesseurSujet.class);
        daoTesting.truncateTable(Sujet.class);
        daoTesting.truncateTable(Equipe.class);
        daoTesting.truncateTable(EtudiantEquipe.class);
        daoTesting.truncateTable(NotePoster.class);
        daoTesting.truncateTable(JuryPoster.class);
        daoTesting.truncateTable(Poster.class);
        daoTesting.truncateTable(Meeting.class);
        daoTesting.truncateTable(Invite.class);

    }

    @AfterClass
    public static void tearDown() throws Exception {
        daoTesting.truncateUsedTables();
    }

    @Before
    public void beforeTest() throws SQLException, IllegalAccessException {
        daoTesting.truncateUsedTables();
    }


    /**
     * Teste la méthode public void creer.
     */
    @Test
    public void testCreer() {
        /* Insertion du bean dans la BDD */
        Utilisateur utilisateur = daoTesting.getUtilisateur();
        Utilisateur utilisateurAttendu = daoTesting.getUtilisateur();

        daoTesting.testCreerCRUD(utilisateur, utilisateurAttendu);
    }

    /**
     * Teste la méthode public trouver
     */
    @Test
    public void testTrouver() {
        /* Recherche du bean dans la BDD */
        Utilisateur utilisateur = daoTesting.getUtilisateur();
        Utilisateur utilisateurAttendu = daoTesting.getUtilisateur();

        daoTesting.testTrouverCRUD(utilisateur, utilisateurAttendu);
    }


    /**
     * Teste la méthode public void modifier
     */
    @Test
    public void testModifier() {
        Utilisateur utilisateur = daoTesting.getUtilisateur();
        Utilisateur utilisateurMod = daoTesting.getUtilisateur();

        utilisateurMod.setPrenom("DeuxiemeP");

        daoTesting.testModifierCRUD(utilisateur, utilisateurMod, utilisateurMod);
    }

    /**
     * Teste la méthode public lister().
     */
    @Test
    public void testLister() {
        Utilisateur utilisateur = daoTesting.getUtilisateur();
        Utilisateur utilisateurAttendu = daoTesting.getUtilisateur();

        daoTesting.testListerCRUD(new Utilisateur[]{utilisateur}, new Utilisateur[]{utilisateurAttendu});
    }

    /**
     * Teste la méthode public void supprimer(NoteInteretTechno NoteInteretTechno).
     */
    @Test
    public void testSupprimer() {
        Utilisateur utilisateur = daoTesting.getUtilisateur();

        daoTesting.testSupprimerCRUD(utilisateur);
    }

    /**
     * Test les erreurs
     */
    @Test
    public void testLogging() {
        daoTesting.testLoggingCRUD();
    }

    @Test
    public void associerRoleOption() throws IllegalAccessException {
        //Dépendances
        OptionESEO optionESEO = daoTesting.getOptionESEO();
        Role role = daoTesting.getRole();
        Utilisateur utilisateur = daoTesting.getUtilisateur();
        UtilisateurDAO dao = daoTesting.getDaoFactory().getUtilisateurDao();

        //Dépendances
        daoTesting.createObjectInDatabase(optionESEO);
        daoTesting.createObjectInDatabase(role);
        daoTesting.createObjectInDatabase(utilisateur);

        //Test
        dao.associerRoleOption(utilisateur, role, optionESEO);

        //Verification
        final List<Role> found = dao.trouverRole(utilisateur);
        assertEquals("Mauvaise taille", 1, found.size());
        daoTesting.compareObjects(role, found.get(0));
    }

    @Test
    public void supprimerRoleOption() throws IllegalAccessException {
        //Dépendances
        OptionESEO optionESEO = daoTesting.getOptionESEO();
        Role role = daoTesting.getRole();
        Utilisateur utilisateur = daoTesting.getUtilisateur();
        RoleUtilisateur roleU = daoTesting.getRoleUtilisateur();

        //Dépendances
        daoTesting.createObjectInDatabase(optionESEO);
        daoTesting.createObjectInDatabase(role);
        daoTesting.createObjectInDatabase(utilisateur);
        daoTesting.createObjectInDatabase(roleU);

        //Test
        UtilisateurDAO dao = daoTesting.getDaoFactory().getUtilisateurDao();
        dao.supprimerRoleOption(utilisateur, role, optionESEO);

        //Verification
        final List<Role> found = dao.trouverRole(utilisateur);
        assertEquals("Mauvaise taille", 0, found.size());
    }

    @Test
    public void trouverRole() throws IllegalAccessException {
        //Dépendances
        OptionESEO optionESEO = daoTesting.getOptionESEO();
        Role role = daoTesting.getRole();
        Utilisateur utilisateur = daoTesting.getUtilisateur();
        RoleUtilisateur roleU = daoTesting.getRoleUtilisateur();

        //Dépendances
        daoTesting.createObjectInDatabase(optionESEO);
        daoTesting.createObjectInDatabase(role);
        daoTesting.createObjectInDatabase(utilisateur);
        daoTesting.createObjectInDatabase(roleU);

        //Test
        UtilisateurDAO dao = daoTesting.getDaoFactory().getUtilisateurDao();
        final List<Role> found = dao.trouverRole(utilisateur);

        //
        assertEquals("Mauvaise taille", 1, found.size());
        daoTesting.compareObjects(role, found.get(0));
    }

    /**
     * @see UtilisateurDAO#trouverOption(Utilisateur)
     */
    @Test
    public void trouverOption() throws IllegalAccessException {
        //Dépendances
        OptionESEO optionESEO = daoTesting.getOptionESEO();
        Role role = daoTesting.getRole();
        Utilisateur utilisateur = daoTesting.getUtilisateur();
        RoleUtilisateur roleU = daoTesting.getRoleUtilisateur();

        //Dépendances
        daoTesting.createObjectInDatabase(optionESEO);
        daoTesting.createObjectInDatabase(role);
        daoTesting.createObjectInDatabase(utilisateur);
        daoTesting.createObjectInDatabase(roleU);

        //Test
        UtilisateurDAO dao = daoTesting.getDaoFactory().getUtilisateurDao();
        final List<OptionESEO> found = dao.trouverOption(utilisateur);

        //
        assertEquals("Mauvaise taille", 1, found.size());
        daoTesting.compareObjects(optionESEO, found.get(0));
    }

    /**
     * @see UtilisateurDAO#trouverPorteurSujet(Sujet)
     */
    @Test
    public void trouverPorteurSujet() throws IllegalAccessException {
        //Dépendances
        OptionESEO optionESEO = daoTesting.getOptionESEO();
        AnneeScolaire anneeScolaire = daoTesting.getAnneeScolaire();
        Sujet sujet = daoTesting.getSujet();
        Utilisateur utilisateur = daoTesting.getUtilisateur();
        PorteurSujet porteurSujet = daoTesting.getPorteurSujet();

        //Dépendances
        daoTesting.createObjectInDatabase(optionESEO);
        daoTesting.createObjectInDatabase(anneeScolaire);
        daoTesting.createObjectInDatabase(sujet);
        daoTesting.createObjectInDatabase(utilisateur);
        daoTesting.createObjectInDatabase(porteurSujet);

        //Test
        UtilisateurDAO dao = daoTesting.getDaoFactory().getUtilisateurDao();
        final List<Utilisateur> found = dao.trouverPorteurSujet(sujet);

        //Assert
        assertEquals("Mauvaise taille", 1, found.size());
        daoTesting.compareObjects(utilisateur, found.get(0));
    }

    /**
     * @see UtilisateurDAO#listerEtudiants()
     */
    @Test
    public void listerEtudiants() throws IllegalAccessException {
        //Dépendances
        OptionESEO optionESEO = daoTesting.getOptionESEO();
        AnneeScolaire anneeScolaire = daoTesting.getAnneeScolaire();
        Sujet sujet = daoTesting.getSujet();
        Utilisateur utilisateur = daoTesting.getUtilisateur();
        Etudiant etudiant = daoTesting.getEtudiant();
        Role role = daoTesting.getRole();
        RoleUtilisateur roleUtilisateur = daoTesting.getRoleUtilisateur();
        role.setNomRole("Etudiant");

        //Dépendances
        daoTesting.createObjectInDatabase(optionESEO);
        daoTesting.createObjectInDatabase(role);
        daoTesting.createObjectInDatabase(anneeScolaire);
        daoTesting.createObjectInDatabase(sujet);
        daoTesting.createObjectInDatabase(utilisateur);
        daoTesting.createObjectInDatabase(etudiant);
        daoTesting.createObjectInDatabase(roleUtilisateur);

        //Objet attendu
        utilisateur.setIdentifiant(null);
        utilisateur.setHash(null);
        utilisateur.setEmail(null);
        utilisateur.setValide(null);

        //Test
        UtilisateurDAO dao = daoTesting.getDaoFactory().getUtilisateurDao();
        final List<Utilisateur> found = dao.listerEtudiants();

        //Assert
        assertEquals("Mauvaise taille", 1, found.size());
        daoTesting.compareObjects(utilisateur, found.get(0));
    }

    /**
     * @see UtilisateurDAO#listerEtudiantsDisponibles()
     */
    @Test
    public void listerEtudiantsDisponibles() throws IllegalAccessException {
        //Dépendances
        OptionESEO optionESEO = daoTesting.getOptionESEO();
        AnneeScolaire anneeScolaire = daoTesting.getAnneeScolaire();
        Sujet sujet = daoTesting.getSujet();
        Utilisateur utilisateur = daoTesting.getUtilisateur();
        Etudiant etudiant = daoTesting.getEtudiant();
        Role role = daoTesting.getRole();
        RoleUtilisateur roleUtilisateur = daoTesting.getRoleUtilisateur();
        role.setNomRole("Etudiant");

        //Dépendances
        daoTesting.createObjectInDatabase(optionESEO);
        daoTesting.createObjectInDatabase(role);
        daoTesting.createObjectInDatabase(anneeScolaire);
        daoTesting.createObjectInDatabase(sujet);
        daoTesting.createObjectInDatabase(utilisateur);
        daoTesting.createObjectInDatabase(etudiant);
        daoTesting.createObjectInDatabase(roleUtilisateur);

        //Objet attendu
        utilisateur.setIdentifiant(null);
        utilisateur.setHash(null);
        utilisateur.setEmail(null);
        utilisateur.setValide(null);

        //Test
        UtilisateurDAO dao = daoTesting.getDaoFactory().getUtilisateurDao();
        final List<Utilisateur> found = dao.listerEtudiantsDisponibles();

        //Assert
        assertEquals("Mauvaise taille", 1, found.size());
        daoTesting.compareObjects(utilisateur, found.get(0));
    }

    /**
     * @see UtilisateurDAO#listerEtudiantEquipe()
     */
    @Test
    public void listerEtudiantEquipe() throws IllegalAccessException {
        //Dépendances
        OptionESEO optionESEO = daoTesting.getOptionESEO();
        AnneeScolaire anneeScolaire = daoTesting.getAnneeScolaire();
        Sujet sujet = daoTesting.getSujet();
        Utilisateur utilisateur = daoTesting.getUtilisateur();
        Etudiant etudiant = daoTesting.getEtudiant();
        Equipe equipe = daoTesting.getEquipe();
        Role role = daoTesting.getRole();
        RoleUtilisateur roleUtilisateur = daoTesting.getRoleUtilisateur();
        EtudiantEquipe etudiantEquipe = daoTesting.getEtudiantEquipe();
        role.setNomRole("Etudiant");

        //Dépendances
        daoTesting.createObjectInDatabase(optionESEO);
        daoTesting.createObjectInDatabase(role);
        daoTesting.createObjectInDatabase(anneeScolaire);
        daoTesting.createObjectInDatabase(sujet);
        daoTesting.createObjectInDatabase(utilisateur);
        daoTesting.createObjectInDatabase(etudiant);
        daoTesting.createObjectInDatabase(equipe);
        daoTesting.createObjectInDatabase(roleUtilisateur);
        daoTesting.createObjectInDatabase(etudiantEquipe);

        //Objet attendu
        utilisateur.setIdentifiant(null);
        utilisateur.setHash(null);
        utilisateur.setEmail(null);
        utilisateur.setValide(null);

        //Test
        UtilisateurDAO dao = daoTesting.getDaoFactory().getUtilisateurDao();
        final List<Utilisateur> found = dao.listerEtudiantEquipe();

        //Assert
        assertEquals("Mauvaise taille", 1, found.size());
        daoTesting.compareObjects(utilisateur, found.get(0));
    }

    /**
     * @see UtilisateurDAO#listerProfesseur()
     */
    @Test
    public void listerProfesseur() throws IllegalAccessException {
        //Dépendances
        OptionESEO optionESEO = daoTesting.getOptionESEO();
        AnneeScolaire anneeScolaire = daoTesting.getAnneeScolaire();
        Utilisateur utilisateur = daoTesting.getUtilisateur();
        Professeur professeur = daoTesting.getProfesseur();

        //Dépendances
        daoTesting.createObjectInDatabase(optionESEO);
        daoTesting.createObjectInDatabase(anneeScolaire);
        daoTesting.createObjectInDatabase(utilisateur);
        daoTesting.createObjectInDatabase(professeur);

        //Test
        UtilisateurDAO dao = daoTesting.getDaoFactory().getUtilisateurDao();
        final List<Utilisateur> found = dao.listerProfesseur();

        //Assert
        assertEquals("Mauvaise taille", 1, found.size());
        daoTesting.compareObjects(utilisateur, found.get(0));
    }

    /**
     * @see UtilisateurDAO#chercherUtilisateur(String, Integer, Integer, Integer, int, Boolean)
     */
    @Test
    public void chercherUtilisateur() throws IllegalAccessException {
        //Dépendances
        Utilisateur utilisateur = daoTesting.getUtilisateur();

        //Dépendances
        daoTesting.createObjectInDatabase(utilisateur);

        //Test
        UtilisateurDAO dao = daoTesting.getDaoFactory().getUtilisateurDao();
        final ResultatFiltreUtilisateur resultatFiltreUtilisateur = dao.chercherUtilisateur(utilisateur.getNom().substring(2), null, null, 20, 1, null);

        List<FiltreUtilisateur> filtreUtilisateurs = resultatFiltreUtilisateur.getFiltreUtilisateurs();
        FiltreUtilisateur filtreUtilisateurT = filtreUtilisateurs.get(0);

        //attendu
        FiltreUtilisateur filtreUtilisateur = new FiltreUtilisateur();
        filtreUtilisateur.setAnnee(0);
        filtreUtilisateur.setRoles(null);
        filtreUtilisateur.setIdUtilisateur(utilisateur.getIdUtilisateur());
        filtreUtilisateur.setIdentifiant(utilisateur.getIdentifiant());
        filtreUtilisateur.setNom(utilisateur.getNom());
        filtreUtilisateur.setPrenom(utilisateur.getPrenom());
        filtreUtilisateur.setEmail(utilisateur.getEmail());
        filtreUtilisateur.setHash(utilisateur.getHash());
        filtreUtilisateur.setValide(utilisateur.getValide());

        //Assert
        assertNotNull("Aucun résultat", resultatFiltreUtilisateur);
        assertEquals("Mauvaise taille", 1, filtreUtilisateurs.size());
        daoTesting.compareObjects(filtreUtilisateur, filtreUtilisateurT);
    }

    /**
     * @see UtilisateurDAO#listerProfesseurSujet()
     */
    @Test
    public void listerProfesseurSujet() throws IllegalAccessException {
        //Dépendances
        OptionESEO optionESEO = daoTesting.getOptionESEO();
        AnneeScolaire anneeScolaire = daoTesting.getAnneeScolaire();
        Sujet sujet = daoTesting.getSujet();
        Utilisateur utilisateur = daoTesting.getUtilisateur();
        Professeur professeur = daoTesting.getProfesseur();
        ProfesseurSujet professeurSujet = daoTesting.getProfesseurSujet();

        //Dépendances
        daoTesting.createObjectInDatabase(optionESEO);
        daoTesting.createObjectInDatabase(anneeScolaire);
        daoTesting.createObjectInDatabase(sujet);
        daoTesting.createObjectInDatabase(utilisateur);
        daoTesting.createObjectInDatabase(professeur);
        daoTesting.createObjectInDatabase(professeurSujet);

        //Test
        UtilisateurDAO dao = daoTesting.getDaoFactory().getUtilisateurDao();
        final List<ProfesseurSujet> found = dao.listerProfesseurSujet();

        //Assert
        assertEquals("Mauvaise taille", 1, found.size());
        daoTesting.compareObjects(professeurSujet, found.get(0));
    }

    /**
     * @see UtilisateurDAO#trouverProfesseursAttribuesAUnSujet(Sujet)
     */
    @Test
    public void trouverProfesseursAttribuesAUnSujet() throws IllegalAccessException {
        //Dépendances
        OptionESEO optionESEO = daoTesting.getOptionESEO();
        AnneeScolaire anneeScolaire = daoTesting.getAnneeScolaire();
        Sujet sujet = daoTesting.getSujet();
        Utilisateur utilisateur = daoTesting.getUtilisateur();
        Professeur professeur = daoTesting.getProfesseur();
        ProfesseurSujet professeurSujet = daoTesting.getProfesseurSujet();

        //Dépendances
        daoTesting.createObjectInDatabase(optionESEO);
        daoTesting.createObjectInDatabase(anneeScolaire);
        daoTesting.createObjectInDatabase(sujet);
        daoTesting.createObjectInDatabase(utilisateur);
        daoTesting.createObjectInDatabase(professeur);
        daoTesting.createObjectInDatabase(professeurSujet);

        //Test
        UtilisateurDAO dao = daoTesting.getDaoFactory().getUtilisateurDao();
        final List<ProfesseurSujet> found = dao.trouverProfesseursAttribuesAUnSujet(sujet);

        //Assert
        assertEquals("Mauvaise taille", 1, found.size());
        daoTesting.compareObjects(professeurSujet, found.get(0));
    }

    /**
     * @see UtilisateurDAO#trouverProfesseursSujet(Utilisateur)
     */
    @Test
    public void trouverProfesseursSujet() throws IllegalAccessException {
        //Dépendances
        OptionESEO optionESEO = daoTesting.getOptionESEO();
        AnneeScolaire anneeScolaire = daoTesting.getAnneeScolaire();
        Sujet sujet = daoTesting.getSujet();
        Utilisateur utilisateur = daoTesting.getUtilisateur();
        Professeur professeur = daoTesting.getProfesseur();
        ProfesseurSujet professeurSujet = daoTesting.getProfesseurSujet();

        //Dépendances
        daoTesting.createObjectInDatabase(optionESEO);
        daoTesting.createObjectInDatabase(anneeScolaire);
        daoTesting.createObjectInDatabase(sujet);
        daoTesting.createObjectInDatabase(utilisateur);
        daoTesting.createObjectInDatabase(professeur);
        daoTesting.createObjectInDatabase(professeurSujet);

        //Test
        UtilisateurDAO dao = daoTesting.getDaoFactory().getUtilisateurDao();
        final List<ProfesseurSujet> found = dao.trouverProfesseursSujet(utilisateur);

        //Assert
        assertEquals("Mauvaise taille", 1, found.size());
        daoTesting.compareObjects(professeurSujet, found.get(0));
    }

    /**
     * @see UtilisateurDAO#trouverProfesseurAValider()
     */
    @Test
    public void trouverProfesseurAValider() throws IllegalAccessException {
        //Dépendances
        OptionESEO optionESEO = daoTesting.getOptionESEO();
        AnneeScolaire anneeScolaire = daoTesting.getAnneeScolaire();
        Sujet sujet = daoTesting.getSujet();
        Utilisateur utilisateur = daoTesting.getUtilisateur();
        Professeur professeur = daoTesting.getProfesseur();
        ProfesseurSujet professeurSujet = daoTesting.getProfesseurSujet();
        professeurSujet.setValide("non");

        //Dépendances
        daoTesting.createObjectInDatabase(optionESEO);
        daoTesting.createObjectInDatabase(anneeScolaire);
        daoTesting.createObjectInDatabase(sujet);
        daoTesting.createObjectInDatabase(utilisateur);
        daoTesting.createObjectInDatabase(professeur);
        daoTesting.createObjectInDatabase(professeurSujet);

        //Test
        UtilisateurDAO dao = daoTesting.getDaoFactory().getUtilisateurDao();
        final List<Utilisateur> found = dao.trouverProfesseurAValider();

        //Assert
        assertEquals("Mauvaise taille", 1, found.size());
        daoTesting.compareObjects(utilisateur, found.get(0));
    }

    /**
     * @see UtilisateurDAO#trouverProfesseurSansSujet()
     */
    @Test
    public void trouverProfesseurSansSujet() throws IllegalAccessException {
        //Dépendances
        OptionESEO optionESEO = daoTesting.getOptionESEO();
        AnneeScolaire anneeScolaire = daoTesting.getAnneeScolaire();
        Sujet sujet = daoTesting.getSujet();
        Utilisateur utilisateur = daoTesting.getUtilisateur();
        Professeur professeur = daoTesting.getProfesseur();
        Role role1 = daoTesting.getRole();
        Role role2 = daoTesting.getRole();
        Role role3 = daoTesting.getRole();
        Role role4 = daoTesting.getRole();
        RoleUtilisateur roleUtilisateur = daoTesting.getRoleUtilisateur();
        role1.setIdRole(1L);
        role2.setIdRole(2L);
        role3.setIdRole(3L);
        role4.setIdRole(4L);
        role2.setNomRole("role2");
        role3.setNomRole("role3");
        role4.setNomRole("profOption");
        roleUtilisateur.setIdRole(role4.getIdRole());

        //Dépendances
        daoTesting.createObjectInDatabase(optionESEO);
        daoTesting.createObjectInDatabase(anneeScolaire);
        daoTesting.createObjectInDatabase(sujet);
        daoTesting.createObjectInDatabase(role1);
        daoTesting.createObjectInDatabase(role2);
        daoTesting.createObjectInDatabase(role3);
        daoTesting.createObjectInDatabase(role4);
        daoTesting.createObjectInDatabase(utilisateur);
        daoTesting.createObjectInDatabase(professeur);
        daoTesting.createObjectInDatabase(roleUtilisateur);

        //Test
        UtilisateurDAO dao = daoTesting.getDaoFactory().getUtilisateurDao();
        final List<Utilisateur> found = dao.trouverProfesseurSansSujet();

        //Assert
        assertEquals("Mauvaise taille", 1, found.size());
        daoTesting.compareObjects(utilisateur, found.get(0));
    }

    /**
     * @see UtilisateurDAO#listerProfesseursSujetsFonction(String)
     */
    @Test
    public void listerProfesseursSujetsFonction() throws IllegalAccessException {
        //Dépendances
        OptionESEO optionESEO = daoTesting.getOptionESEO();
        AnneeScolaire anneeScolaire = daoTesting.getAnneeScolaire();
        Sujet sujet = daoTesting.getSujet();
        Utilisateur utilisateur = daoTesting.getUtilisateur();
        Professeur professeur = daoTesting.getProfesseur();
        ProfesseurSujet professeurSujet = daoTesting.getProfesseurSujet();

        //Dépendances
        daoTesting.createObjectInDatabase(optionESEO);
        daoTesting.createObjectInDatabase(anneeScolaire);
        daoTesting.createObjectInDatabase(sujet);
        daoTesting.createObjectInDatabase(utilisateur);
        daoTesting.createObjectInDatabase(professeur);
        daoTesting.createObjectInDatabase(professeurSujet);

        //Test
        UtilisateurDAO dao = daoTesting.getDaoFactory().getUtilisateurDao();
        final List<ProfesseurSujet> found = dao.listerProfesseursSujetsFonction(professeurSujet.getFonction().getFonction());

        //Assert
        assertEquals("Mauvaise taille", 1, found.size());
        daoTesting.compareObjects(professeurSujet, found.get(0));
    }

    /**
     * @see UtilisateurDAO#recupererNotesUtilisateursPFE(Integer, Integer)
     */
    @Test
    public void recupererNotesUtilisateurs() throws IllegalAccessException {
        //Dépendances
        OptionESEO optionESEO = daoTesting.getOptionESEO();
        AnneeScolaire anneeScolaire = daoTesting.getAnneeScolaire();
        Sujet sujet = daoTesting.getSujet();
        Utilisateur utilisateur = daoTesting.getUtilisateur();
        Utilisateur utilisateurE = daoTesting.getUtilisateur();
        Professeur professeur = daoTesting.getProfesseur();
        Etudiant etudiant = daoTesting.getEtudiant();
        Equipe equipe = daoTesting.getEquipe();
        EtudiantEquipe etudiantEquipe = daoTesting.getEtudiantEquipe();
        ProfesseurSujet professeurSujet = daoTesting.getProfesseurSujet();
        NotePoster notePoster = daoTesting.getNotePoster();
        JuryPoster juryPoster = daoTesting.getJuryPoster();
        Poster poster = daoTesting.getPoster();
        Role role = daoTesting.getRole();
        RoleUtilisateur roleUtilisateur = daoTesting.getRoleUtilisateur();
        roleUtilisateur.setIdUtilisateur(2L);

        utilisateurE.setIdUtilisateur(2L);
        utilisateurE.setIdentifiant("etudiant");
        utilisateurE.setEmail("etudiant@no.fr");
        etudiantEquipe.setIdEtudiant(2L);
        etudiant.setIdEtudiant(2L);
        notePoster.setIdEtudiant(2L);

        //Dépendances
        daoTesting.createObjectInDatabase(optionESEO);
        daoTesting.createObjectInDatabase(anneeScolaire);
        daoTesting.createObjectInDatabase(sujet);
        daoTesting.createObjectInDatabase(role);
        daoTesting.createObjectInDatabase(utilisateur);
        daoTesting.createObjectInDatabase(utilisateurE);
        daoTesting.createObjectInDatabase(professeur);
        daoTesting.createObjectInDatabase(professeurSujet);
        daoTesting.createObjectInDatabase(etudiant);
        daoTesting.createObjectInDatabase(equipe);
        daoTesting.createObjectInDatabase(poster);
        daoTesting.createObjectInDatabase(etudiantEquipe);
        daoTesting.createObjectInDatabase(juryPoster);
        daoTesting.createObjectInDatabase(notePoster);
        daoTesting.createObjectInDatabase(roleUtilisateur);


        //Test
        UtilisateurDAO dao = daoTesting.getDaoFactory().getUtilisateurDao();
        final Map<Utilisateur, ENJNotes> found = dao.recupererNotesUtilisateursPFE(optionESEO.getIdOption().intValue(), anneeScolaire.getAnneeDebut());

        //Assert
        assertEquals("Mauvaise taille", 1, found.size());
    }

    /**
     * @see UtilisateurDAO#trouverCreateurReunion(Meeting)
     */
    @Test
    public void trouverCreateurReunion() throws IllegalAccessException {
        //Dépendances
        Utilisateur utilisateur = daoTesting.getUtilisateur();
        Meeting meeting = daoTesting.getMeeting();

        //Dépendances
        daoTesting.createObjectInDatabase(utilisateur);
        daoTesting.createObjectInDatabase(meeting);

        //Test
        UtilisateurDAO dao = daoTesting.getDaoFactory().getUtilisateurDao();
        final List<Utilisateur> found = dao.trouverCreateurReunion(meeting);

        //Assert
        assertEquals("Mauvaise taille", 1, found.size());
        daoTesting.compareObjects(utilisateur, found.get(0));
    }

    /**
     * @see UtilisateurDAO#trouverInvitesReunion(Meeting)
     **/
    @Test
    public void TrouverInvitesReunion() throws IllegalAccessException {
        //Dépendances
        Utilisateur utilisateur = daoTesting.getUtilisateur();
        Utilisateur utilisateur2 = daoTesting.getUtilisateur();
        Meeting meeting = daoTesting.getMeeting();
        Invite invite = daoTesting.getInvite();

        utilisateur2.setIdentifiant("u2");
        utilisateur2.setEmail("no2@no.fr");
        utilisateur2.setIdUtilisateur(2L);
        invite.setRefUtilisateur(2L);

        //Dépendances
        daoTesting.createObjectInDatabase(utilisateur);
        daoTesting.createObjectInDatabase(utilisateur2);
        daoTesting.createObjectInDatabase(meeting);
        daoTesting.createObjectInDatabase(invite);


        //Test
        UtilisateurDAO dao = daoTesting.getDaoFactory().getUtilisateurDao();
        final List<Utilisateur> found = dao.trouverInvitesReunion(meeting);

        //Assert
        assertEquals("Mauvaise taille", 1, found.size());
        daoTesting.compareObjects(utilisateur2, found.get(0));
    }
}