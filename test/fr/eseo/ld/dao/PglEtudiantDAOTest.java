package fr.eseo.ld.dao;

import fr.eseo.ld.beans.AnneeScolaire;
import fr.eseo.ld.beans.OptionESEO;
import fr.eseo.ld.beans.Utilisateur;
import fr.eseo.ld.pgl.beans.Etudiant;
import fr.eseo.testing.DAOTesting;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.sql.SQLException;

/**
 * Classe de test de PglEtudiantDAO
 *
 * @author Tristan LE GACQUE
 * @see PglEtudiantDAO
 **/
public class PglEtudiantDAOTest {

    private static DAOTesting<Etudiant> daoTesting;

    /**
     * Définition du DaoTesting
     *
     * @throws Exception exception
     */
    @BeforeClass
    public static void setUp() throws Exception {
        //Définition du dao factory
        daoTesting = new DAOTesting<>();
        daoTesting.initDao(daoTesting.getDaoFactory().getPglEtudiantDAO());

        daoTesting.addAlias(Etudiant.class, "pgl_etudiant");

        daoTesting.truncateTable(OptionESEO.class);
        daoTesting.truncateTable(Utilisateur.class);
        daoTesting.truncateTable(AnneeScolaire.class);
    }

    @AfterClass
    public static void tearDown() throws Exception {
        daoTesting.truncateUsedTables();
    }

    @Before
    public void beforeTest() throws SQLException, IllegalAccessException {
        daoTesting.truncateUsedTables();

        //Définition des dépendances
        OptionESEO optionESEO = daoTesting.getOptionESEO();
        AnneeScolaire anneeScolaire = daoTesting.getAnneeScolaire();
        Utilisateur user = daoTesting.getUtilisateur();

        //Ajout des objets a la base de données
        daoTesting.createObjectInDatabase(user);
        daoTesting.createObjectInDatabase(optionESEO);
        daoTesting.createObjectInDatabase(anneeScolaire);
    }


    /**
     * Teste la méthode public void creer.
     *
     * @see PglEtudiantDAO#creer(Etudiant)
     */
    @Test
    public void testCreer() {
        /* Insertion du bean dans la BDD */
        Etudiant pgletudiant = daoTesting.getEtudiantPgl();
        Etudiant pgletudiantAttendu = daoTesting.getEtudiantPgl();

        daoTesting.testCreerCRUD(pgletudiant, pgletudiantAttendu);
    }

    /**
     * Teste la méthode public trouver
     *
     * @see PglEtudiantDAO#trouver(Etudiant)
     */
    @Test
    public void testTrouver() {
        /* Recherche du bean dans la BDD */
        Etudiant pgletudiant = daoTesting.getEtudiantPgl();
        Etudiant pgletudiantAttendu = daoTesting.getEtudiantPgl();

        daoTesting.testTrouverCRUD(pgletudiant, pgletudiantAttendu);
    }


    /**
     * Teste la méthode public void modifier
     *
     * @see PglEtudiantDAO#modifier(Etudiant)
     */
    public void testModifier() {
        Etudiant pgletudiant = daoTesting.getEtudiantPgl();
        Etudiant pgletudiantMod = daoTesting.getEtudiantPgl();
        Etudiant pgletudiantModAttendu = daoTesting.getEtudiantPgl();

        daoTesting.testModifierCRUD(pgletudiant, pgletudiantMod, pgletudiantModAttendu);
    }

    /**
     * Teste la méthode public lister().
     *
     * @see PglEtudiantDAO#lister()
     */
    @Test
    public void testLister() {
        Etudiant pgletudiant = daoTesting.getEtudiantPgl();
        Etudiant pgletudiantAttendu = daoTesting.getEtudiantPgl();

        daoTesting.testListerCRUD(new Etudiant[]{pgletudiant}, new Etudiant[]{pgletudiantAttendu});
    }

    /**
     * Teste la méthode public void supprimer(NoteInteretTechno NoteInteretTechno).
     *
     * @see PglEtudiantDAO#supprimer(Etudiant)
     */
    @Test
    public void testSupprimer() {
        Etudiant pgletudiant = daoTesting.getEtudiantPgl();

        daoTesting.testSupprimerCRUD(pgletudiant);
    }

    /**
     * Test les erreurs
     */
    @Test
    public void testLogging() {
        daoTesting.testLoggingCRUD();
    }
}