package fr.eseo.ld.dao;

import fr.eseo.ld.beans.*;
import fr.eseo.testing.DAOTesting;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.sql.SQLException;
import java.util.List;

import static junit.framework.TestCase.assertEquals;

/**
 * Classe de test de OptionESEODAO
 *
 * @author Tristan LE GACQUE
 * @see OptionESEODAO
 **/
public class OptionESEODAOTest {

    private static DAOTesting<OptionESEO> daoTesting;

    /**
     * Définition du DaoTesting
     *
     * @throws Exception exception
     */
    @BeforeClass
    public static void setUp() throws Exception {
        //Définition du dao factory
        daoTesting = new DAOTesting<>();
        daoTesting.initDao(daoTesting.getDaoFactory().getOptionESEODAO());
        daoTesting.addAlias(OptionSujet.class, "optionsujets");

        daoTesting.truncateTable(Sujet.class);
        daoTesting.truncateTable(OptionSujet.class);
        daoTesting.truncateTable(OptionESEO.class);
        daoTesting.truncateTable(Utilisateur.class);
        daoTesting.truncateTable(Role.class);
        daoTesting.truncateTable(RoleUtilisateur.class);
    }

    @AfterClass
    public static void tearDown() throws Exception {
        daoTesting.truncateUsedTables();
    }

    @Before
    public void beforeTest() throws SQLException, IllegalAccessException {
        daoTesting.truncateUsedTables();
    }


    /**
     * Teste la méthode public void creer.
     */
    @Test
    public void testCreer() {
        /* Insertion du bean dans la BDD */
        OptionESEO optioneseo = daoTesting.getOptionESEO();
        OptionESEO optioneseoAttendu = daoTesting.getOptionESEO();

        daoTesting.testCreerCRUD(optioneseo, optioneseoAttendu);
    }

    /**
     * Teste la méthode public trouver
     */
    @Test
    public void testTrouver() {
        /* Recherche du bean dans la BDD */
        OptionESEO optioneseo = daoTesting.getOptionESEO();
        OptionESEO optioneseoAttendu = daoTesting.getOptionESEO();

        daoTesting.testTrouverCRUD(optioneseo, optioneseoAttendu);
    }


    /**
     * Teste la méthode public void modifier
     */
    @Test
    public void testModifier() {
        OptionESEO optioneseo = daoTesting.getOptionESEO();
        OptionESEO optioneseoMod = daoTesting.getOptionESEO();

        daoTesting.testModifierCRUD(optioneseo, optioneseoMod, optioneseoMod);
    }

    /**
     * Teste la méthode public lister().
     */
    @Test
    public void testLister() {
        OptionESEO optioneseo = daoTesting.getOptionESEO();
        OptionESEO optioneseoAttendu = daoTesting.getOptionESEO();

        daoTesting.testListerCRUD(new OptionESEO[]{optioneseo}, new OptionESEO[]{optioneseoAttendu});
    }

    /**
     * Teste la méthode public void supprimer(NoteInteretTechno NoteInteretTechno).
     */
    @Test
    public void testSupprimer() {
        OptionESEO optioneseo = daoTesting.getOptionESEO();

        daoTesting.testSupprimerCRUD(optioneseo);
    }

    /**
     * Test les erreurs
     */
    @Test
    public void testLogging() {
        daoTesting.testLogging(DAOTesting.CRUD.TROUVER, DAOTesting.CRUD.SUPPRIMER);
    }

    @Test
    public void trouverOptionSujet() throws IllegalAccessException {
        Sujet sujet = daoTesting.getSujet();
        OptionSujet optionSujet = daoTesting.getOptionSujet();
        OptionESEO optionESEO = daoTesting.getOptionESEO();

        daoTesting.createObjectInDatabase(optionESEO);
        daoTesting.createObjectInDatabase(sujet);
        daoTesting.createObjectInDatabase(optionSujet);

        final List<OptionESEO> find = daoTesting.getDaoFactory().getOptionESEODAO().trouverOptionSujet(sujet);
        assertEquals("Mauvais nombre de résultats", 1, find.size());
        daoTesting.compareObjects(optionESEO, find.get(0));
    }

    @Test
    public void trouverOptionUtilisateur() throws IllegalAccessException {
        Utilisateur utilisateur = daoTesting.getUtilisateur();
        OptionESEO optionESEO = daoTesting.getOptionESEO();
        Role role = daoTesting.getRole();
        RoleUtilisateur roleUtilisateur = daoTesting.getRoleUtilisateur();

        daoTesting.createObjectInDatabase(utilisateur);
        daoTesting.createObjectInDatabase(optionESEO);
        daoTesting.createObjectInDatabase(role);
        daoTesting.createObjectInDatabase(roleUtilisateur);

        final List<OptionESEO> find = daoTesting.getDaoFactory().getOptionESEODAO().trouverOptionUtilisateur(utilisateur);
        assertEquals("Mauvais nombre de résultats", 1, find.size());
        daoTesting.compareObjects(optionESEO, find.get(0));
    }

    @Test
    public void trouverOptionRoleUtilisateur() throws IllegalAccessException {
        Utilisateur utilisateur = daoTesting.getUtilisateur();
        OptionESEO optionESEO = daoTesting.getOptionESEO();
        Role role = daoTesting.getRole();
        RoleUtilisateur roleUtilisateur = daoTesting.getRoleUtilisateur();

        daoTesting.createObjectInDatabase(utilisateur);
        daoTesting.createObjectInDatabase(optionESEO);
        daoTesting.createObjectInDatabase(role);
        daoTesting.createObjectInDatabase(roleUtilisateur);

        final List<OptionESEO> find = daoTesting.getDaoFactory().getOptionESEODAO().trouverOptionRoleUtilisateur(
                utilisateur.getIdUtilisateur(), role.getIdRole());
        assertEquals("Mauvais nombre de résultats", 1, find.size());
        daoTesting.compareObjects(optionESEO, find.get(0));
    }
}