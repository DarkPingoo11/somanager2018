package fr.eseo.ld.dao;

import fr.eseo.ld.beans.*;
import fr.eseo.testing.DAOTesting;
import org.apache.log4j.*;
import org.easymock.EasyMock;
import org.easymock.EasyMockRunner;
import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

import java.io.ByteArrayOutputStream;
import java.sql.SQLException;

import static org.easymock.EasyMock.*;
import static org.junit.Assert.assertTrue;

/**
 * Classe de tests unitaires JUnit 4 de la classe NoteInteretTechnoDAO.
 *
 * <p>Utilisation du modèle DAO.</p>
 *
 * @version 1.0
 * @author Maxime LENORMAND
 *
 * @see NoteInteretTechnoDAO
 * @see org.junit
 */
@RunWith(EasyMockRunner.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class NoteInteretTechnoDAOTest {

	private static final Long LONG_TEST_PROFESSEUR = 1L;
	private static final Long LONG_TEST_SUJET = 1L;
	private static final Integer INTEGER_TEST = 1;
	private static final Integer INTEGER_TEST_MODIFIE = 5;

	private static Logger logger = Logger.getLogger(NoteInteretTechnoDAO.class.getName());

	private static NoteInteretTechnoDAO NoteInteretTechnoDAO;


	private static DAOTesting<NoteInteretTechno> daoTesting;

	@BeforeClass
	public static void setUp() throws Exception {
		//Définition du dao factory
		daoTesting = new DAOTesting<>();
		daoTesting.initDao(daoTesting.getDaoFactory().getNoteInteretTechnoDAO());
		NoteInteretTechnoDAO = daoTesting.getDaoFactory().getNoteInteretTechnoDAO();
		daoTesting.addAlias(NoteInteret.class, NoteInteretTechno.class.getName());
	}

	@AfterClass
	public static void tearDown() throws Exception {
		daoTesting.truncateUsedTables();
	}

	@Before
	public void beforeTest() throws SQLException, IllegalAccessException {
		daoTesting.truncateUsedTables();

		Sujet s = new Sujet();
		s.setDescription("Description");
		s.setTitre("Titre");
		s.setContratPro(false);
		s.setConfidentialite(false);
		s.setNbrMinEleves(0);
		s.setNbrMaxEleves(1);
		s.setEtat(EtatSujet.DEPOSE);
		Utilisateur u = new Utilisateur();
		u.setIdUtilisateur(LONG_TEST_PROFESSEUR);
		u.setEmail("no@no.fr");
		u.setPrenom("PRenom");
		u.setNom("Nom");
		u.setValide("oui");
		u.setIdentifiant("testtesttest");
		u.setHash("hash");
		Professeur prof = new Professeur();
		prof.setIdProfesseur(LONG_TEST_PROFESSEUR);

		daoTesting.createObjectInDatabase(s);
		daoTesting.createObjectInDatabase(u);
		daoTesting.createObjectInDatabase(prof);
	}

	public NoteInteretTechno getNoteInteretTechno() {
		NoteInteretTechno NoteInteretTechno = new NoteInteretTechno();
		NoteInteretTechno.setIdProfesseur(LONG_TEST_PROFESSEUR);
		NoteInteretTechno.setIdSujet(LONG_TEST_SUJET);
		NoteInteretTechno.setNote(INTEGER_TEST);
		return NoteInteretTechno;
	}

	/**
	 * Teste la méthode public void creer(NoteInteretTechno NoteInteretTechno).
	 */
	@Test
	public void test1_1Creer() {
		/* Insertion du bean dans la BDD */
		NoteInteretTechno NoteInteretTechno = getNoteInteretTechno();

		daoTesting.testCreerCRUD(NoteInteretTechno, NoteInteretTechno);
	}

	/**
	 * Teste la méthode public List<NoteInteretTechno> trouver(NoteInteretTechno NoteInteretTechno).
	 */
	@Test
	public void test2_1Trouver() {
		/* Recherche du bean dans la BDD */
		NoteInteretTechno NoteInteretTechno = getNoteInteretTechno();

		daoTesting.testTrouverCRUD(NoteInteretTechno, NoteInteretTechno);
	}

	/**
	 * Teste la méthode public List<NoteInteretTechno> trouver(NoteInteretTechno NoteInteretTechno).
	 * <p>Cas de la recherche d'un bean sans attributs.</p>
	 */
	@Test
	public void test2_2TrouverLog() {
		/* Configuraton du logger */
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		Layout layout = new SimpleLayout();
		Appender appender = new WriterAppender(layout, out);
		logger.addAppender(appender);

		try {
			/* Recherche du bean sans attributs dans la BDD */
			NoteInteretTechno NoteInteretTechno = new NoteInteretTechno();
			NoteInteretTechnoDAO.trouver(NoteInteretTechno);

			/* Vérification du logging */
			String messageLog = out.toString();
			assertTrue("Mauvais logging", messageLog.contains("Échec de la recherche"));
		} finally {
			/* Configuraton initiale du logger */
			logger.removeAppender(appender);
		}
	}

	/**
	 * Teste la méthode public void modifier(NoteInteretTechno NoteInteretTechno).
	 */
	@Test
	public void test3_1Modifier() {
		/* Modification du bean dans la BDD */
		NoteInteretTechno NoteInteretTechno = getNoteInteretTechno();
		NoteInteretTechno NoteInteretTechnoM = getNoteInteretTechno();
		NoteInteretTechnoM.setNote(INTEGER_TEST_MODIFIE);

		daoTesting.testModifierCRUD(NoteInteretTechno, NoteInteretTechnoM, NoteInteretTechnoM);
	}

	/**
	 * Teste la méthode public List<NoteInteretTechno> lister().
	 */
	@Test
	public void test4_1Lister() throws IllegalAccessException {
		NoteInteretTechno NoteInteretTechno = getNoteInteretTechno();

		daoTesting.testListerCRUD(new NoteInteretTechno[]{NoteInteretTechno}, new NoteInteretTechno[]{NoteInteretTechno});
	}

	/**
	 * Teste la méthode public List<NoteInteretTechno> lister().
	 * <p>Cas d'une erreur lors de la création de la connexion.</p>
	 * @throws SQLException
	 */
	@Test
	public void test4_2ListerLog() throws SQLException {
		/* Configuraton du logger */
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		Layout layout = new SimpleLayout();
		Appender appender = new WriterAppender(layout, out);
		logger.addAppender(appender);

		try {
			/* Mock de la méthode creerConnexion() */
			NoteInteretTechnoDAO NoteInteretTechnoDAOMock = EasyMock.createMockBuilder(NoteInteretTechnoDAO.class)
					.addMockedMethod("creerConnexion").createMock();
			expect(NoteInteretTechnoDAOMock.creerConnexion()).andThrow(new SQLException());

			replay(NoteInteretTechnoDAOMock);

			/* Recherche du bean dans la BDD */
			NoteInteretTechnoDAOMock.lister();

			/* Vérification du logging */
			String messageLog = out.toString();
			assertTrue("Mauvais logging", messageLog.contains("Échec du listage"));

			verify(NoteInteretTechnoDAOMock);
		} finally {
			/* Configuraton initiale du logger */
			logger.removeAppender(appender);
		}
	}

	/**
	 * Teste la méthode public void supprimer(NoteInteretTechno NoteInteretTechno).
	 */
	@Test
	public void test5_1Supprimer() {
		NoteInteretTechno NoteInteretTechno = getNoteInteretTechno();

		daoTesting.testSupprimerCRUD(NoteInteretTechno);
	}

	/**
	 * Teste la méthode public void supprimer(NoteInteretTechno NoteInteretTechno).
	 * <p>Cas de la suppression d'un bean non présent dans la BDD.</p>
	 */
	@Test
	public void test5_2SupprimerLog() {
		/* Configuraton du logger */
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		Layout layout = new SimpleLayout();
		Appender appender = new WriterAppender(layout, out);
		logger.addAppender(appender);

		try {
			/* Suppression du bean dans la BDD */
			NoteInteretTechnoDAO.supprimer((NoteInteretTechno)getNoteInteretTechno());

			/* Vérification du logging */
			String messageLog = out.toString();
			assertTrue("Mauvais logging", messageLog.contains("Échec de la suppression"));
		} finally {
			/* Configuraton initiale du logger */
			logger.removeAppender(appender);
		}
	}

}