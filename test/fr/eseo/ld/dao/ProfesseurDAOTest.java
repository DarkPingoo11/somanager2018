package fr.eseo.ld.dao;

import fr.eseo.ld.beans.*;
import fr.eseo.testing.DAOTesting;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.sql.SQLException;
import java.util.List;

import static junit.framework.TestCase.assertEquals;

/**
 * Classe de test de ProfesseurDAO
 *
 * @author Tristan LE GACQUE
 * @see ProfesseurDAO
 **/
public class ProfesseurDAOTest {

    private static final Long ID_DEFAUT = 1L;
    private static DAOTesting<Professeur> daoTesting;

    /**
     * Définition du DaoTesting
     *
     * @throws Exception exception
     */
    @BeforeClass
    public static void setUp() throws Exception {
        //Définition du dao factory
        daoTesting = new DAOTesting<>();
        daoTesting.initDao(daoTesting.getDaoFactory().getProfesseurDAO());

        daoTesting.truncateTable(OptionESEO.class);
        daoTesting.truncateTable(Role.class);
        daoTesting.truncateTable(RoleUtilisateur.class);
        daoTesting.truncateTable(Utilisateur.class);
        daoTesting.truncateTable(Professeur.class);
    }

    @AfterClass
    public static void tearDown() throws Exception {
        daoTesting.truncateUsedTables();
    }

    @Before
    public void beforeTest() throws SQLException, IllegalAccessException {
        daoTesting.truncateUsedTables();

        //Définition des dépendances
        Utilisateur user = daoTesting.getUtilisateur();

        //Ajout des objets a la base de données
        daoTesting.createObjectInDatabase(user);
    }


    /**
     * Teste la méthode public void creer.
     */
    @Test
    public void testCreer() {
        /* Insertion du bean dans la BDD */
        Professeur professeur = daoTesting.getProfesseur();
        Professeur professeurAttendu = daoTesting.getProfesseur();

        daoTesting.testCreerCRUD(professeur, professeurAttendu);
    }

    /**
     * Teste la méthode public trouver
     */
    @Test
    public void testTrouver() {
        /* Recherche du bean dans la BDD */
        Professeur professeur = daoTesting.getProfesseur();
        Professeur professeurAttendu = daoTesting.getProfesseur();

        daoTesting.testTrouverCRUD(professeur, professeurAttendu);
    }


    /**
     * Teste la méthode public void modifier
     */
    @Test
    public void testModifier() {
        Professeur professeur = daoTesting.getProfesseur();
        Professeur professeurMod = daoTesting.getProfesseur();

        //Un prof ne peux pas changer de clé primaire

        daoTesting.testModifierCRUD(professeur, professeurMod, professeurMod);
    }

    /**
     * Teste la méthode public lister().
     */
    @Test
    public void testLister() {
        Professeur professeur = daoTesting.getProfesseur();
        Professeur professeurAttendu = daoTesting.getProfesseur();

        daoTesting.testListerCRUD(new Professeur[]{professeur}, new Professeur[]{professeurAttendu});
    }

    /**
     * Teste la méthode public void supprimer(NoteInteretTechno NoteInteretTechno).
     */
    @Test
    public void testSupprimer() {
        Professeur professeur = daoTesting.getProfesseur();

        daoTesting.testSupprimerCRUD(professeur);
    }

    /**
     * Test les erreurs
     */
    @Test
    public void testLogging() {
        daoTesting.testLogging(DAOTesting.CRUD.CREER, DAOTesting.CRUD.TROUVER, DAOTesting.CRUD.SUPPRIMER);
    }

    @Test
    public void listerProfesseursDisponibles() throws IllegalAccessException {
        //Dépendances
        Professeur prof = daoTesting.getProfesseur();
        daoTesting.createObjectInDatabase(prof);

        //Objet attendu
        Utilisateur utilisateur = new Utilisateur();
        utilisateur.setIdUtilisateur(ID_DEFAUT);
        utilisateur.setNom(daoTesting.getUtilisateur().getNom());
        utilisateur.setPrenom(daoTesting.getUtilisateur().getPrenom());


        //Test
        final List<Utilisateur> utilisateurs = daoTesting.getDaoFactory().getProfesseurDAO().listerProfesseursDisponibles();
        assertEquals("La liste contient ne contient pas le bon nombre d'éléments", 1, utilisateurs.size());
        daoTesting.compareObjects(utilisateur, utilisateurs.get(0));
    }

    @Test
    public void listerProfesseursDisponiblesOption() throws IllegalAccessException {
        createProfOptionDependance();

        //Objet attendu
        OptionESEO option = daoTesting.getOptionESEO();
        Utilisateur utilisateur = new Utilisateur();
        utilisateur.setIdUtilisateur(ID_DEFAUT);
        utilisateur.setNom(daoTesting.getUtilisateur().getNom());
        utilisateur.setPrenom(daoTesting.getUtilisateur().getPrenom());

        //Test
        final List<Utilisateur> utilisateurs = daoTesting.getDaoFactory().getProfesseurDAO().listerProfesseursDisponiblesOption(option);
        assertEquals("La liste contient ne contient pas le bon nombre d'éléments", 1, utilisateurs.size());
        daoTesting.compareObjects(utilisateur, utilisateurs.get(0));
    }

    @Test
    public void listerProfesseursOption() throws IllegalAccessException {
        createProfOptionDependance();

        //Objet attendu
        OptionESEO option = daoTesting.getOptionESEO();
        Utilisateur utilisateur = new Utilisateur();
        utilisateur.setIdUtilisateur(ID_DEFAUT);
        utilisateur.setNom(daoTesting.getUtilisateur().getNom());
        utilisateur.setPrenom(daoTesting.getUtilisateur().getPrenom());

        //Test
        final List<Utilisateur> utilisateurs = daoTesting.getDaoFactory().getProfesseurDAO().listerProfesseursOption(option);
        assertEquals("La liste contient ne contient pas le bon nombre d'éléments", 1, utilisateurs.size());
        daoTesting.compareObjects(utilisateur, utilisateurs.get(0));
    }

    private void createProfOptionDependance() throws IllegalAccessException {
        OptionESEO option = daoTesting.getOptionESEO();
        Role role = daoTesting.getRole();
        RoleUtilisateur ru = daoTesting.getRoleUtilisateur();
        Professeur prof = daoTesting.getProfesseur();

        daoTesting.createObjectInDatabase(prof);
        daoTesting.createObjectInDatabase(option);
        daoTesting.createObjectInDatabase(role);
        daoTesting.createObjectInDatabase(ru);
    }
}