package fr.eseo.ld.dao;

import fr.eseo.ld.beans.*;
import fr.eseo.testing.DAOTesting;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.sql.SQLException;

public class PosterDAOTest {

    private static DAOTesting<Poster> daoTesting;

    /**
     * Définition du DaoTesting
     *
     * @throws Exception exception
     */
    @BeforeClass
    public static void setUp() throws Exception {
        //Définition du dao factory
        daoTesting = new DAOTesting<>();
        daoTesting.initDao(daoTesting.getDaoFactory().getPosterDao());
    }

    @AfterClass
    public static void tearDown() throws Exception {
        daoTesting.truncateUsedTables();
    }

    @Before
    public void beforeTest() throws SQLException, IllegalAccessException {
        daoTesting.truncateUsedTables();

        //Définition des dépendances
        OptionESEO optionESEO = daoTesting.getOptionESEO();
        AnneeScolaire anneeScolaire = daoTesting.getAnneeScolaire();
        Sujet sujet = daoTesting.getSujet();
        Equipe equipe = daoTesting.getEquipe();

        //Ajout des objets a la base de données
        daoTesting.createObjectInDatabase(optionESEO);
        daoTesting.createObjectInDatabase(anneeScolaire);
        daoTesting.createObjectInDatabase(sujet);
        daoTesting.createObjectInDatabase(equipe);
    }


    /**
     * Teste la méthode public void creer.
     */
    @Test
    public void testCreer() {
        /* Insertion du bean dans la BDD */
        Poster poster = daoTesting.getPoster();
        Poster posterAttendu = daoTesting.getPoster();

        daoTesting.testCreerCRUD(poster, posterAttendu);
    }

    /**
     * Teste la méthode public trouver
     */
    @Test
    public void testTrouver() {
        /* Recherche du bean dans la BDD */
        Poster poster = daoTesting.getPoster();
        Poster posterAttendu = daoTesting.getPoster();

        daoTesting.testTrouverCRUD(poster, posterAttendu);
    }


    /**
     * Teste la méthode public void modifier
     */
    @Test
    public void testModifier() {
        Poster poster = daoTesting.getPoster();
        Poster posterMod = daoTesting.getPoster();

        posterMod.setChemin("AutreChemin");

        daoTesting.testModifierCRUD(poster, posterMod, posterMod);
    }

    /**
     * Teste la méthode public lister().
     * Regarder ModeleExcelDao pour un exemple plus complet
     *
     * @see ModeleExcelDAOTest
     */
    @Test
    public void testLister() {
        Poster poster = daoTesting.getPoster();
        Poster posterAttendu = daoTesting.getPoster();

        daoTesting.testListerCRUD(new Poster[]{poster}, new Poster[]{posterAttendu});
    }

    /**
     * Teste la méthode public void supprimer(NoteInteretTechno NoteInteretTechno).
     */
    @Test
    public void testSupprimer() {
        Poster poster = daoTesting.getPoster();

        daoTesting.testSupprimerCRUD(poster);
    }

    /**
     * Test les erreurs
     */
    @Test
    public void testLogging() {
        daoTesting.testLoggingCRUD();
    }
}