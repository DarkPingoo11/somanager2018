package fr.eseo.ld.dao;

import fr.eseo.ld.beans.DemandeJury;
import fr.eseo.ld.beans.Utilisateur;
import fr.eseo.testing.DAOTesting;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.sql.SQLException;

/**
 * Classe de test de DemandeJuryDAO
 *
 * @author Tristan LE GACQUE
 * @see DemandeJuryDAO
 **/
public class DemandeJuryDAOTest {

    private static DAOTesting<DemandeJury> daoTesting;

    /**
     * Définition du DaoTesting
     *
     * @throws Exception exception
     */
    @BeforeClass
    public static void setUp() throws Exception {
        //Définition du dao factory
        daoTesting = new DAOTesting<>();
        daoTesting.initDao(daoTesting.getDaoFactory().getDemandeJuryDAO());
    }

    @AfterClass
    public static void tearDown() throws Exception {
        daoTesting.truncateUsedTables();
    }

    @Before
    public void beforeTest() throws SQLException, IllegalAccessException {
        daoTesting.truncateUsedTables();

        //Définition des dépendances
        Utilisateur user = daoTesting.getUtilisateur();

        //Ajout des objets a la base de données
        daoTesting.createObjectInDatabase(user);
    }


    /**
     * Teste la méthode public void creer.
     *
     * @see DemandeJuryDAO#creer(DemandeJury)
     */
    @Test
    public void testCreer() {
        /* Insertion du bean dans la BDD */
        DemandeJury demandejury = daoTesting.getDemandeJury();
        DemandeJury demandejuryAttendu = daoTesting.getDemandeJury();

        daoTesting.testCreerCRUD(demandejury, demandejuryAttendu);
    }

    /**
     * Teste la méthode public trouver
     *
     * @see DemandeJuryDAO#trouver(DemandeJury)
     */
    @Test
    public void testTrouver() {
        /* Recherche du bean dans la BDD */
        DemandeJury demandejury = daoTesting.getDemandeJury();
        DemandeJury demandejuryAttendu = daoTesting.getDemandeJury();

        daoTesting.testTrouverCRUD(demandejury, demandejuryAttendu);
    }


    /**
     * Teste la méthode public void modifier
     *
     * @see DemandeJuryDAO#modifier(DemandeJury)
     */
    @Test
    public void testModifier() {
        DemandeJury demandejury = daoTesting.getDemandeJury();
        DemandeJury demandejuryMod = daoTesting.getDemandeJury();

        demandejuryMod.setCommentaire("Un autre");

        daoTesting.testModifierCRUD(demandejury, demandejuryMod, demandejuryMod);
    }

    /**
     * Teste la méthode public lister().
     *
     * @see DemandeJuryDAO#lister()
     */
    @Test
    public void testLister() {
        DemandeJury demandejury = daoTesting.getDemandeJury();
        DemandeJury demandejuryAttendu = daoTesting.getDemandeJury();

        daoTesting.testListerCRUD(new DemandeJury[]{demandejury}, new DemandeJury[]{demandejuryAttendu});
    }

    /**
     * Teste la méthode public void supprimer(NoteInteretTechno NoteInteretTechno).
     *
     * @see DemandeJuryDAO#supprimer(DemandeJury)
     */
    @Test
    public void testSupprimer() {
        DemandeJury demandejury = daoTesting.getDemandeJury();

        daoTesting.testSupprimerCRUD(demandejury);
    }

    /**
     * Test les erreurs
     */
    @Test
    public void testLogging() {
        daoTesting.testLoggingCRUD();
    }
}