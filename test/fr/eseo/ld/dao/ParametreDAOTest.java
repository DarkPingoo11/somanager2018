package fr.eseo.ld.dao;

import fr.eseo.ld.beans.Parametre;
import fr.eseo.testing.DAOTesting;
import org.apache.log4j.*;
import org.easymock.EasyMock;
import org.easymock.EasyMockRunner;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

import java.io.ByteArrayOutputStream;
import java.sql.SQLException;
import java.util.List;

import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.replay;
import static org.easymock.EasyMock.verify;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


@RunWith(EasyMockRunner.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ParametreDAOTest {

    private static final String STRING_NOM_TEST = "lienFormulaireSujet";
    private static final String STRING_VALEUR_TEST = "";

    private static Parametre parametre;
    private static ParametreDAO parametreDAO;

    private static Logger logger = Logger.getLogger(ParametreDAO.class.getName());
    private static DAOTesting<Parametre> daoTesting;

    /**
     * Récupère une instance de DAO et initialise le bean correspondant.
     *
     * @throws Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        daoTesting = new DAOTesting<>();
        daoTesting.initDao(daoTesting.getDaoFactory().getParametreDAO());

        parametre = new Parametre();
        parametre.setNomParametre(STRING_NOM_TEST);
        parametre.setValeur(STRING_VALEUR_TEST);
        parametreDAO = daoTesting.getDaoFactory().getParametreDAO();
    }

    /**
     * Teste la méthode public void creer(Parametre parametre).
     */
    @Test
    public void test1_1Creer() {
        /* Insertion du bean dans la BDD */
        parametreDAO.creer(parametre);

        /* Vérification des attributs insérés */
        final List<Parametre> parametres = parametreDAO.trouver(parametre);
        final String resultatAttendu = STRING_VALEUR_TEST;
        System.out.println(parametres.size());

        if(this.parametreDAO.trouver(parametre).size() == 1) {
            //Si il existe, on update
            System.out.println("hello");
        } else {
            System.out.println("error");
        }
        final String resultatTrouve = parametres.get(0).getValeur();
        assertEquals("Mauvaise anneeDebut", resultatAttendu, resultatTrouve);
    }
    /**
     * Teste la méthode public void creer(Parametre parametre).
     * <p>Cas d'une erreur lors de la création de la connexion.</p>
     * @throws SQLException
     */
    @Test
    public void test1_2CreerLog() throws SQLException {
        /* Configuraton du logger */
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        Layout layout = new SimpleLayout();
        Appender appender = new WriterAppender(layout, out);
        logger.addAppender(appender);

        try {
            /* Mock de la méthode creerConnexion() */
            ParametreDAO parametreDAOMock = EasyMock.createMockBuilder(ParametreDAO.class)
                    .addMockedMethod("creerConnexion").createMock();
            expect(parametreDAOMock.creerConnexion()).andThrow(new SQLException());

            replay(parametreDAOMock);

            /* Insertion du bean dans la BDD */
            parametreDAOMock.creer(parametre);

            /* Vérification du logging */
            String messageLog = out.toString();
            assertTrue("Mauvais logging", messageLog.contains("Échec de la création de l'objet"));

            verify(parametreDAOMock);
        } finally {
            /* Configuraton initiale du logger */
            logger.removeAppender(appender);
        }
    }


    /**
     * Teste la méthode public List<Parametre> trouver(Parametre parametre).
     */
    @Test
    public void test2_1Trouver() {
        /* Recherche du bean dans la BDD */
        final List<Parametre> parametres = parametreDAO.trouver(parametre);

        /* Vérification des attributs trouvés */
        final String resultatAttendu = STRING_NOM_TEST;
        final String resultatTrouve = parametres.get(0).getNomParametre();
        assertEquals("Mauvais idProf1", resultatAttendu, resultatTrouve);
    }

    /**
     * Teste la méthode public List<Parametre> trouver(Parametre parametre).
     * <p>Cas de la recherche d'un bean sans attributs.</p>
     */
    @Test
    public void test2_2TrouverLog() {
        /* Configuraton du logger */
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        Layout layout = new SimpleLayout();
        Appender appender = new WriterAppender(layout, out);
        logger.addAppender(appender);

        try {
            /* Recherche du bean sans attributs dans la BDD */
            Parametre parametre = new Parametre();
            parametreDAO.trouver(parametre);

            /* Vérification du logging */
            String messageLog = out.toString();
            assertTrue("Mauvais logging", messageLog.contains("Échec de la recherche"));
        } finally {
            /* Configuraton initiale du logger */
            logger.removeAppender(appender);
        }
    }


    /**
     * Teste la méthode public void modifier(Parametre parametre).
     */
    @Test
    public void test3_1Modifier() {
        /* Modification du bean dans la BDD */
        parametre.setValeur(STRING_VALEUR_TEST) ;;
        parametreDAO.modifier(parametre);

        /* Vérification des attributs modifiés */
        final List<Parametre> parametres = parametreDAO.trouver(parametre);
        final String resultatAttendu = STRING_VALEUR_TEST;
        final String resultatTrouve = parametres.get(0).getValeur();
        assertEquals("Mauvais idProf2", resultatAttendu, resultatTrouve);
    }

    /**
     * Teste la méthode public List<Parametre> lister().
     */
    @Test
    public void test4_1Lister() {
        /* Recherche du bean dans la BDD */
        final List<Parametre> parametres = parametreDAO.lister();

        /* Vérification que le bean inséré est présent dans la liste */
        boolean estPresent = false;
        int i = 0;
        while (!estPresent && i < parametres.size()) {
            if (ParametreDAO.IDENTIFIANT_LIEN_FORM_SUJET.equals(parametres.get(i).getNomParametre())) {
                estPresent = true;
            }
            i++;
        }
        assertTrue("Le bean inséré n'est pas présent dans la liste", estPresent);
    }

    /**
     * Teste la méthode public List<Parametre> lister().
     * <p>Cas d'une erreur lors de la création de la connexion.</p>
     * @throws SQLException
     */
    @Test
    public void test4_2ListerLog() throws SQLException {
        /* Configuraton du logger */
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        Layout layout = new SimpleLayout();
        Appender appender = new WriterAppender(layout, out);
        logger.addAppender(appender);

        try {
            /* Mock de la méthode creerConnexion() */
            ParametreDAO parametreDAOMock = EasyMock.createMockBuilder(ParametreDAO.class)
                    .addMockedMethod("creerConnexion").createMock();
            expect(parametreDAOMock.creerConnexion()).andThrow(new SQLException());

            replay(parametreDAOMock);

            /* Recherche du bean dans la BDD */
            parametreDAOMock.lister();

            /* Vérification du logging */
            String messageLog = out.toString();
            assertTrue("Mauvais logging", messageLog.contains("Échec du listage"));

            verify(parametreDAOMock);
        } finally {
            /* Configuraton initiale du logger */
            logger.removeAppender(appender);
        }
    }

    /**
     * Teste la méthode public void supprimer(Parametre parametre).
     */
    @Test
    public void test5_1Supprimer() {
        /* Suppression du bean dans la BDD */
        parametreDAO.supprimer(parametre);

        /* Vérification de la suppression */
        final List<Parametre> parametres = parametreDAO.trouver(parametre);
        assertTrue("Le bean n'a pas été supprimé", parametres.isEmpty());
    }

    /**
     * Teste la méthode public void supprimer(Parametre parametre).
     * <p>Cas de la suppression d'un bean non présent dans la BDD.</p>
     */
    @Test
    public void test5_2SupprimerLog() {
        /* Configuraton du logger */
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        Layout layout = new SimpleLayout();
        Appender appender = new WriterAppender(layout, out);
        logger.addAppender(appender);

        try {
            /* Suppression du bean dans la BDD */
            parametreDAO.supprimer(parametre);
            /* Vérification du logging */
            String messageLog = out.toString();
            assertTrue("Mauvais logging", messageLog.contains("Échec de la suppression"));
        } finally {
            /* Configuraton initiale du logger */
            logger.removeAppender(appender);
        }
    }

}
