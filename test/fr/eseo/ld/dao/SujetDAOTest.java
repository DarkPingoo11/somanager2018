package fr.eseo.ld.dao;

import fr.eseo.ld.beans.*;
import fr.eseo.testing.DAOTesting;
import org.apache.log4j.*;
import org.easymock.EasyMock;
import org.easymock.EasyMockRunner;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

import java.io.ByteArrayOutputStream;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static junit.framework.TestCase.fail;
import static org.easymock.EasyMock.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Classe de tests unitaires JUnit 4 de la classe SujetDAO.
 *
 * <p>Utilisation du modèle DAO.</p>
 *
 * @version 1.0
 * @author Thomas MENARD et Maxime LENORMAND
 *
 * @see fr.eseo.ld.dao.SujetDAO
 * @see org.junit
 */
@RunWith(EasyMockRunner.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class SujetDAOTest {

	private static final String STRING_TEST = "test";
	private static final String STRING_TEST_MODIFIE = "test2";
	private static final String STRING_TEST_TITRE_POSTER_VALIDE = "Prothèse d'articulation contrôlée";
	private static final String STRING_TEST_TITRE_SANS_REFERENT = "Un sujet pas comme les autres";
	private static final Integer INTEGER_TEST = 1;
	private static final Long LONG_TEST_UTILISATEUR = 1L;
	private static final Long LONG_TEST_PROFESSEUR = 2L;
	private static final Long LONG_TEST_SUJET_POSTER = 1L;
	private static final Long LONG_TEST_OPTION = 1L;
	
	private static Logger logger = Logger.getLogger(SujetDAO.class.getName());
	private static DAOTesting<Sujet> daoTesting;

	private static Sujet sujet;
	private static Utilisateur utilisateur;
	private static Professeur professeur;
	private static OptionESEO option;
	private static SujetDAO sujetDAO;
	
	/**
	 * Récupère une instance de DAO et initialise le bean correspondant.
	 * 
	 * @throws Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		daoTesting = new DAOTesting<>();
		daoTesting.initDao(daoTesting.getDaoFactory().getSujetDao());
		daoTesting.addAlias(OptionSujet.class, "optionsujets");

		daoTesting.truncateTable();

		sujet = new Sujet();
		sujet.setTitre(STRING_TEST);
		sujet.setDescription(STRING_TEST);
		sujet.setNbrMinEleves(INTEGER_TEST);
		sujet.setNbrMaxEleves(INTEGER_TEST);
		sujet.setEtat(EtatSujet.DEPOSE);
		
		utilisateur = new Utilisateur();
		utilisateur.setIdUtilisateur(LONG_TEST_UTILISATEUR);
		
		professeur = new Professeur();
		professeur.setIdProfesseur(LONG_TEST_PROFESSEUR);
		
		option = new OptionESEO();
		option.setIdOption(LONG_TEST_OPTION);
		
		sujetDAO = daoTesting.getDaoFactory().getSujetDao();
	}

	@AfterClass
	public static void after() throws SQLException {
		daoTesting.truncateUsedTables();
	}

	/**
	 * Teste la méthode public void creer(Sujet sujet).
	 */
	@Test
	public void test1_1Creer() {
		/* Insertion du bean dans la BDD */
		sujetDAO.creer(sujet);
		
		/* Ajout de la clé primaire générée au bean */
		final Long idSujet = sujetDAO.trouver(sujet).get(0).getIdSujet();
		sujet.setIdSujet(idSujet);
		
		/* Vérification des attributs insérés */
		final List<Sujet> sujets = sujetDAO.trouver(sujet);
		final String resultatAttendu = STRING_TEST;
		final String resultatTrouve = sujets.get(0).getTitre();
		assertEquals("Mauvais titre", resultatAttendu, resultatTrouve);
	}
	
	/**
	 * Teste la méthode public void creer(Sujet sujet).
	 * <p>Cas d'une erreur lors de la création de la connexion.</p>
	 * @throws SQLException 
	 */
	@Test
	public void test1_2CreerLog() throws SQLException {
		/* Configuraton du logger */
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		Layout layout = new SimpleLayout();
		Appender appender = new WriterAppender(layout, out);
		logger.addAppender(appender);
		
		try {
			/* Mock de la méthode creerConnexion() */
			SujetDAO sujetDAOMock = EasyMock.createMockBuilder(SujetDAO.class).addMockedMethod("creerConnexion")
					.createMock();
			expect(sujetDAOMock.creerConnexion()).andThrow(new SQLException());
			
			replay(sujetDAOMock);
			
			/* Insertion du bean dans la BDD */
			sujetDAOMock.creer(sujet);
			
			/* Vérification du logging */
			String messageLog = out.toString();
			assertTrue("Mauvais logging", messageLog.contains("Échec de la création de l'objet"));
			
			verify(sujetDAOMock);
		} finally {
			/* Configuraton initiale du logger */
			logger.removeAppender(appender);
		}
	}
	
	/**
	 * Teste la méthode public void creerProfesseurSujet(Professeur professeur, Sujet sujet, String fonction, String valide).
	 * <p>Cas de l'insertion d'un bean déjà présent dans la BDD.</p>
	 */
	@Test
	public void test1_5CreerProfesseurSujetLog() {
		/* Configuraton du logger */
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		Layout layout = new SimpleLayout();
		Appender appender = new WriterAppender(layout, out);
		logger.addAppender(appender);
		
		try {
			/* Insertion du bean dans la BDD */
			sujetDAO.creerProfesseurSujet(professeur, sujet, "referent", "non");

			/* Vérification du logging */
			String messageLog = out.toString();
			assertTrue("Mauvais logging", messageLog.contains("Échec de la création de professeurSujet"));
		} finally {
			/* Configuraton initiale du logger */
			logger.removeAppender(appender);
		}
	}


	
	/**
	 * Teste la méthode public void attribuerOptionsSujet(Sujet sujet, List<OptionESEO> options).
	 * <p>Cas de l'insertion d'un bean déjà présent dans la BDD.</p>
	 */
	@Test
	public void test1_7AttribuerOptionsSujetLog() {
		/* Configuraton du logger */
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		Layout layout = new SimpleLayout();
		Appender appender = new WriterAppender(layout, out);
		logger.addAppender(appender);
		
		try {
			/* Insertion du bean dans la BDD */
			List<OptionESEO> options = new ArrayList<>();
			options.add(option);
			sujetDAO.attribuerOptionsSujet(sujet, options);

			/* Vérification du logging */
			String messageLog = out.toString();
			assertTrue("Mauvais logging", messageLog.contains("Échec de la création de optionSujet"));
		} finally {
			/* Configuraton initiale du logger */
			logger.removeAppender(appender);
		}
	}
	
	/**
	 * Teste la méthode public List<Sujet> trouver(Sujet sujet).
	 */
	@Test
	public void test2_1_1Trouver() {
		/* Recherche du bean dans la BDD */
		final List<Sujet> sujets = sujetDAO.trouver(sujet);
		
		/* Vérification des attributs trouvés */
		final String resultatAttendu = STRING_TEST;
		final String resultatTrouve = sujets.get(0).getTitre();
		assertEquals("Mauvais titre", resultatAttendu, resultatTrouve);
	}
	
	/**
	 * Teste la méthode public List<Sujet> trouver(Sujet sujet).
	 * <p>Cas de la recherche d'un bean sans attributs.</p>
	 */
	@Test
	public void test2_1_2TrouverLog() {
		/* Configuraton du logger */
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		Layout layout = new SimpleLayout();
		Appender appender = new WriterAppender(layout, out);
		logger.addAppender(appender);
		
		try {
			/* Recherche du bean sans attributs dans la BDD */
			Sujet sujet = new Sujet();
			sujetDAO.trouver(sujet);

			/* Vérification du logging */
			String messageLog = out.toString();
			assertTrue("Mauvais logging", messageLog.contains("Échec de la recherche"));
		} finally {
			/* Configuraton initiale du logger */
			logger.removeAppender(appender);
		}
	}
	
	/**
	 * Teste la méthode public List<Sujet> trouver(Long idSujet).
	 */
	@Test
	public void test2_2_1TrouverIdSujet() {
		/* Recherche du bean dans la BDD */
		final Sujet sujetTrouve = sujetDAO.trouver(sujet.getIdSujet());
		
		/* Vérification des attributs trouvés */
		final String resultatAttendu = STRING_TEST;
		final String resultatTrouve = sujetTrouve.getTitre();
		assertEquals("Mauvais titre", resultatAttendu, resultatTrouve);
	}
	
	/**
	 * Teste la méthode public List<Sujet> trouver(Long idSujet).
	 * <p>Cas de la recherche d'un bean sans attributs.</p>
	 */
	@Test
	public void test2_2_2TrouverIdSujetLog() {
		/* Configuraton du logger */
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		Layout layout = new SimpleLayout();
		Appender appender = new WriterAppender(layout, out);
		logger.addAppender(appender);
		
		try {
			/* Recherche du bean sans attributs dans la BDD */
			Long idSujet = null;
			sujetDAO.trouver(idSujet);

			/* Vérification du logging */
			String messageLog = out.toString();
			assertTrue("Mauvais logging", messageLog.contains("Échec de la recherche"));
		} finally {
			/* Configuraton initiale du logger */
			logger.removeAppender(appender);
		}
	}
	

	
	/**
	 * Teste la méthode public List<Sujet> trouverSujetsPortes(Utilisateur utilisateur).
	 * <p>Cas de la recherche d'un bean sans attributs.</p>
	 */
	@Test
	public void test2_3_2TrouverSujetsPortesLog() {
		/* Configuraton du logger */
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		Layout layout = new SimpleLayout();
		Appender appender = new WriterAppender(layout, out);
		logger.addAppender(appender);
		
		try {
			/* Recherche du bean sans attributs dans la BDD */
			Utilisateur utilisateur = new Utilisateur();
			sujetDAO.trouverSujetsPortes(utilisateur);

			/* Vérification du logging */
			String messageLog = out.toString();
			assertTrue("Mauvais logging", messageLog.contains("Échec de la recherche des sujets portés par un utilisateur."));
		} finally {
			/* Configuraton initiale du logger */
			logger.removeAppender(appender);
		}
	}
	
	/**
	 * Teste la méthode public List<Sujet> trouverSujetsProfesseur(Professeur professeur).
	 * <p>Cas de la recherche d'un bean sans attributs.</p>
	 */
	@Test
	public void test2_4_2TrouverSujetsProfesseurLog() {
		/* Configuraton du logger */
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		Layout layout = new SimpleLayout();
		Appender appender = new WriterAppender(layout, out);
		logger.addAppender(appender);
		
		try {
			/* Recherche du bean sans attributs dans la BDD */
			Professeur professeur = new Professeur();
			sujetDAO.trouverSujetsProfesseur(professeur);

			/* Vérification du logging */
			String messageLog = out.toString();
			assertTrue("Mauvais logging", messageLog.contains("Échec de la recherche des sujets attribués à un professeur."));
		} finally {
			/* Configuraton initiale du logger */
			logger.removeAppender(appender);
		}
	}
	
	/**
	 * Teste la méthode public List<Sujet> trouverSujetsProfesseurReferent(Professeur professeur).
	 * <p>Cas de la recherche d'un bean sans attributs.</p>
	 */
	@Test
	public void test2_4_3TrouverSujetsProfesseurReferentLog() {
		/* Configuraton du logger */
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		Layout layout = new SimpleLayout();
		Appender appender = new WriterAppender(layout, out);
		logger.addAppender(appender);
		
		try {
			/* Recherche du bean sans attributs dans la BDD */
			Professeur professeur = new Professeur();
			sujetDAO.trouverSujetsProfesseurReferent(professeur);

			/* Vérification du logging */
			String messageLog = out.toString();
			assertTrue("Mauvais logging", messageLog.contains("Échec de la recherche des sujets attribués à un professeur référent."));
		} finally {
			/* Configuraton initiale du logger */
			logger.removeAppender(appender);
		}
	}
	
	/**
	 * Teste la méthode public List<Sujet> trouverSujetsProfesseurReferentNonValide(Professeur professeur).
	 * <p>Cas de la recherche d'un bean sans attributs.</p>
	 */
	@Test
	public void test2_4_4TrouverSujetsProfesseurReferentNonValideLog() {
		/* Configuraton du logger */
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		Layout layout = new SimpleLayout();
		Appender appender = new WriterAppender(layout, out);
		logger.addAppender(appender);
		
		try {
			/* Recherche du bean sans attributs dans la BDD */
			Professeur professeur = new Professeur();
			sujetDAO.trouverSujetsProfesseurReferentNonValide(professeur);

			/* Vérification du logging */
			String messageLog = out.toString();
			assertTrue("Mauvais logging", messageLog.contains("Échec de la recherche des sujets attribués à un professeur référent qui n'est pas encore validé."));
		} finally {
			/* Configuraton initiale du logger */
			logger.removeAppender(appender);
		}
	}
	
	/**
	 * Teste la méthode public List<Sujet> trouverSujetOption(OptionESEO optionESEO).
	 * <p>Cas de la recherche d'un bean sans attributs.</p>
	 */
	@Test
	public void test2_5_2TrouverSujetOptionLog() {
		/* Configuraton du logger */
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		Layout layout = new SimpleLayout();
		Appender appender = new WriterAppender(layout, out);
		logger.addAppender(appender);
		
		try {
			/* Recherche du bean sans attributs dans la BDD */
			OptionESEO option = new OptionESEO();
			sujetDAO.trouverSujetOption(option);

			/* Vérification du logging */
			String messageLog = out.toString();
			assertTrue("Mauvais logging", messageLog.contains("Échec de la recherche des sujets possédant une option donnée."));
		} finally {
			/* Configuraton initiale du logger */
			logger.removeAppender(appender);
		}
	}
	
	/**
	 * Teste la méthode public boolean possedePoster(Sujet sujet).
	 * <p>Cas de l'echec de la recherche.</p>
	 */
	@Test
	public void test2_6_1PossedePosterFaux() {
		/* Recherche du bean dans la BDD */
		final boolean possedePoster = sujetDAO.possedePoster(sujet);
		
		/* Vérification des attributs trouvés */
		final boolean resultatAttendu = false;
		final boolean resultatTrouve = possedePoster;
		assertEquals("Mauvais possedePoster", resultatAttendu, resultatTrouve);
	}
	
	/**
	 * Teste la méthode public boolean possedePoster(Sujet sujet).
	 * <p>Cas de la recherche d'un bean sans attributs.</p>
	 */
	@Test
	public void test2_6_2PossedePosterLog() {
		/* Configuraton du logger */
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		Layout layout = new SimpleLayout();
		Appender appender = new WriterAppender(layout, out);
		logger.addAppender(appender);
		
		try {
			/* Recherche du bean sans attributs dans la BDD */
			Sujet sujet = new Sujet();
			sujetDAO.possedePoster(sujet);

			/* Vérification du logging */
			String messageLog = out.toString();
			assertTrue("Mauvais logging", messageLog.contains("Échec de la recherche de la présence d'un poster dans un sujet."));
		} finally {
			/* Configuraton initiale du logger */
			logger.removeAppender(appender);
		}
	}
	
	/**
	 * Teste la méthode public List<Sujet> trouverSujetsPostersValidesOptions(List<OptionESEO> options).
	 * <p>Cas de la recherche d'un bean sans attributs.</p>
	 */
	@Test
	public void test2_7_2TrouverSujetsPostersValidesOptionsLog() {
		/* Configuraton du logger */
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		Layout layout = new SimpleLayout();
		Appender appender = new WriterAppender(layout, out);
		logger.addAppender(appender);
		
		try {
			/* Recherche du bean sans attributs dans la BDD */
			List<OptionESEO> options = new ArrayList<>();
			sujetDAO.trouverSujetsPostersValidesOptions(options);

			/* Vérification du logging */
			String messageLog = out.toString();
			assertTrue("Mauvais logging", messageLog.contains("Échec de la recherche de sujets dont le poster est validé."));
		} finally {
			/* Configuraton initiale du logger */
			logger.removeAppender(appender);
		}
	}
	
	/**
	 * Teste la méthode public List<Sujet> trouverSujetsJurySoutenance(Utilisateur utilisateur).
	 * <p>Cas de la recherche d'un bean sans attributs.</p>
	 */
	@Test
	public void test2_8_1TrouverSujetsJurySoutenanceLog() {
		/* Configuraton du logger */
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		Layout layout = new SimpleLayout();
		Appender appender = new WriterAppender(layout, out);
		logger.addAppender(appender);
		
		try {
			/* Recherche du bean sans attributs dans la BDD */
			Utilisateur utilisateur = new Utilisateur();
			sujetDAO.trouverSujetsJurySoutenance(utilisateur);

			/* Vérification du logging */
			String messageLog = out.toString();
			assertTrue("Mauvais logging", messageLog.contains("Échec de la recherche de sujets associés à un jurySoutenance associé à un utilisateur."));
		} finally {
			/* Configuraton initiale du logger */
			logger.removeAppender(appender);
		}
	}
	
	/**
	 * Teste la méthode public List<Sujet> trouverSujetsJuryPoster(Utilisateur utilisateur).
	 * <p>Cas de la recherche d'un bean sans attributs.</p>
	 */
	@Test
	public void test2_9_1TrouverSujetsJuryPosterLog() {
		/* Configuraton du logger */
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		Layout layout = new SimpleLayout();
		Appender appender = new WriterAppender(layout, out);
		logger.addAppender(appender);
		
		try {
			/* Recherche du bean sans attributs dans la BDD */
			Utilisateur utilisateur = new Utilisateur();
			sujetDAO.trouverSujetsJuryPoster(utilisateur);

			/* Vérification du logging */
			String messageLog = out.toString();
			assertTrue("Mauvais logging", messageLog.contains("Échec de la recherche de sujets associés à un juryPoster associé à un utilisateur."));
		} finally {
			/* Configuraton initiale du logger */
			logger.removeAppender(appender);
		}
	}
	
	/**
	 * Teste la méthode public void modifier(Sujet sujet).
	 */
	@Test
	public void test3_1Modifier() {
		/* Modification du bean dans la BDD */
		sujet.setTitre(STRING_TEST_MODIFIE);;
		sujetDAO.modifier(sujet);
		
		/* Vérification des attributs modifiés */
		final List<Sujet> sujets = sujetDAO.trouver(sujet);
		final String resultatAttendu = STRING_TEST_MODIFIE;
		final String resultatTrouve = sujets.get(0).getTitre();
		assertEquals("Mauvais test", resultatAttendu, resultatTrouve);
	}
	
	/**
	 * Teste la méthode public List<Sujet> lister().
	 */
	@Test
	public void test4_1Lister() {
		/* Recherche du bean dans la BDD */
		final List<Sujet> sujets = sujetDAO.lister();
		
		/* Vérification que le bean inséré est présent dans la liste */
		boolean estPresent = false;
		int i = 0;
		while (!estPresent && i < sujets.size()) {
			if (STRING_TEST_MODIFIE.equals(sujets.get(i).getTitre())) {
				estPresent = true;
			}
			i++;
		}
		assertTrue("Le bean inséré n'est pas présent dans la liste", estPresent);
	}
	
	/**
	 * Teste la méthode public List<Sujet> lister().
	 * <p>Cas d'une erreur lors de la création de la connexion.</p>
	 * @throws SQLException 
	 */
	@Test
	public void test4_2ListerLog() throws SQLException {
		/* Configuraton du logger */
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		Layout layout = new SimpleLayout();
		Appender appender = new WriterAppender(layout, out);
		logger.addAppender(appender);
		
		try {
			/* Mock de la méthode creerConnexion() */
			SujetDAO sujetDAOMock = EasyMock.createMockBuilder(SujetDAO.class).addMockedMethod("creerConnexion")
					.createMock();
			expect(sujetDAOMock.creerConnexion()).andThrow(new SQLException());
			
			replay(sujetDAOMock);
			
			/* Recherche du bean dans la BDD */
			sujetDAOMock.lister();
			
			/* Vérification du logging */
			String messageLog = out.toString();
			assertTrue("Mauvais logging", messageLog.contains("Échec du listage"));
			
			verify(sujetDAOMock);
		} finally {
			/* Configuraton initiale du logger */
			logger.removeAppender(appender);
		}
	}
	

	
	/**
	 * Teste la méthode public List<Sujet> listerSujetsSansReferent().
	 * <p>Cas d'une erreur lors de la création de la connexion.</p>
	 * @throws SQLException 
	 */
	@Test
	public void test4_4ListerSujetsSansReferentLog() throws SQLException {
		/* Configuraton du logger */
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		Layout layout = new SimpleLayout();
		Appender appender = new WriterAppender(layout, out);
		logger.addAppender(appender);
		
		try {
			/* Mock de la méthode creerConnexion() */
			SujetDAO sujetDAOMock = EasyMock.createMockBuilder(SujetDAO.class).addMockedMethod("creerConnexion")
					.createMock();
			expect(sujetDAOMock.creerConnexion()).andThrow(new SQLException());
			
			replay(sujetDAOMock);
			
			/* Recherche du bean dans la BDD */
			sujetDAOMock.listerSujetsSansReferent();
			
			/* Vérification du logging */
			String messageLog = out.toString();
			assertTrue("Mauvais logging", messageLog.contains("Échec du listage des sujets sans référent"));
			
			verify(sujetDAOMock);
		} finally {
			/* Configuraton initiale du logger */
			logger.removeAppender(appender);
		}
	}
	
	/**
	 * Teste la méthode public void supprimerPorteurSujet(Sujet sujet, Utilisateur utilisateur).
	 */
	@Test
	public void test5_1SupprimerPorteurSujet() {
		/* Suppression du bean dans la BDD */
		sujetDAO.supprimerPorteurSujet(sujet, utilisateur);

		/* Vérification de la suppression */
		final List<Sujet> sujets = sujetDAO.trouverSujetsPortes(utilisateur);
		assertTrue("Le bean n'a pas été supprimé", sujets.isEmpty());
	}
	
	/**
	 * Teste la méthode public void supprimerPorteurSujet(Sujet sujet, Utilisateur utilisateur).
	 * <p>Cas de la suppression d'un bean non présent dans la BDD.</p>
	 */
	@Test
	public void test5_2SupprimerPorteurSujetLog() {
		/* Configuraton du logger */
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		Layout layout = new SimpleLayout();
		Appender appender = new WriterAppender(layout, out);
		logger.addAppender(appender);

		try {
			/* Suppression du bean dans la BDD */
			sujetDAO.supprimerPorteurSujet(sujet, utilisateur);

			/* Vérification du logging */
			String messageLog = out.toString();
			assertTrue("Mauvais logging", messageLog.contains("Échec de la suppression de porteurSujet"));
		} finally {
			/* Configuraton initiale du logger */
			logger.removeAppender(appender);
		}
	}
	
	/**
	 * Teste la méthode public void supprimerProfesseurSujet(Sujet sujet, Professeur professeur).
	 */
	@Test
	public void test5_3SupprimerProfesseurSujet() {
		/* Suppression du bean dans la BDD */
		sujetDAO.supprimerProfesseurSujet(sujet, professeur);

		/* Vérification de la suppression */
		final List<Sujet> sujets = sujetDAO.trouverSujetsPortes(utilisateur);
		assertTrue("Le bean n'a pas été supprimé", sujets.isEmpty());
	}
	
	/**
	 * Teste la méthode public void supprimerProfesseurSujet(Sujet sujet, Professeur professeur).
	 * <p>Cas de la suppression d'un bean non présent dans la BDD.</p>
	 */
	@Test
	public void test5_4SupprimerProfesseurSujetLog() {
		/* Configuraton du logger */
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		Layout layout = new SimpleLayout();
		Appender appender = new WriterAppender(layout, out);
		logger.addAppender(appender);

		try {
			/* Suppression du bean dans la BDD */
			sujetDAO.supprimerProfesseurSujet(sujet, professeur);

			/* Vérification du logging */
			String messageLog = out.toString();
			assertTrue("Mauvais logging", messageLog.contains("Échec de la suppression de professeurSujet"));
		} finally {
			/* Configuraton initiale du logger */
			logger.removeAppender(appender);
		}
	}
	
	/**
	 * Teste la méthode public void supprimerOptionSujet(Sujet sujet, OptionESEO option).
	 */
	@Test
	public void test5_5SupprimerOptionsSujet() {
		/* Suppression du bean dans la BDD */
		sujetDAO.supprimerOptionSujet(sujet, option);

		/* Vérification de la suppression */
		final List<Sujet> sujets = sujetDAO.trouverSujetOption(option);
		boolean estPasPresent = true;
		int i = 0;
		while (estPasPresent && i < sujets.size()) {
			if (STRING_TEST_MODIFIE.equals(sujets.get(i).getTitre())) {
				estPasPresent = false;
			}
			i++;
		}
		assertTrue("Le bean inséré est présent dans la liste", estPasPresent);
	}
	
	/**
	 * Teste la méthode public void supprimerOptionSujet(Sujet sujet, OptionESEO option).
	 * <p>Cas de la suppression d'un bean non présent dans la BDD.</p>
	 */
	@Test
	public void test5_6SupprimerOptionsSujetLog() {
		/* Configuraton du logger */
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		Layout layout = new SimpleLayout();
		Appender appender = new WriterAppender(layout, out);
		logger.addAppender(appender);

		try {
			/* Suppression du bean dans la BDD */
			sujetDAO.supprimerOptionSujet(sujet, option);

			/* Vérification du logging */
			String messageLog = out.toString();
			assertTrue("Mauvais logging", messageLog.contains("Échec de la suppression de optionSujet"));
		} finally {
			/* Configuraton initiale du logger */
			logger.removeAppender(appender);
		}
	}
	
	/**
	 * Teste la méthode public void supprimer(Sujet sujet).
	 */
	@Test
	public void test5_7Supprimer() {
		/* Suppression du bean dans la BDD */
		sujetDAO.supprimer(sujet);

		/* Vérification de la suppression */
		final List<Sujet> sujets = sujetDAO.trouver(sujet);
		assertTrue("Le bean n'a pas été supprimé", sujets.isEmpty());
	}
	
	/**
	 * Teste la méthode public void supprimer(Sujet sujet).
	 * <p>Cas de la suppression d'un bean non présent dans la BDD.</p>
	 */
	@Test
	public void test5_8SupprimerLog() {
		/* Configuraton du logger */
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		Layout layout = new SimpleLayout();
		Appender appender = new WriterAppender(layout, out);
		logger.addAppender(appender);

		try {
			/* Suppression du bean dans la BDD */
			sujetDAO.supprimer(sujet);

			/* Vérification du logging */
			String messageLog = out.toString();
			assertTrue("Mauvais logging", messageLog.contains("Échec de la suppression"));
		} finally {
			/* Configuraton initiale du logger */
			logger.removeAppender(appender);
		}
	}
	
	/**
	 * Teste la méthode public void modifier(Sujet sujet).
	 * <p>Cas de la modification d'un bean non présent dans la BDD.</p>
	 */
	@Test
	public void test6ModifierLog() {
		/* Configuraton du logger */
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		Layout layout = new SimpleLayout();
		Appender appender = new WriterAppender(layout, out);
		logger.addAppender(appender);
		
		try {
			/* Modification du bean dans la BDD */
			sujetDAO.modifier(sujet);

			/* Vérification du logging */
			String messageLog = out.toString();
			assertTrue("Mauvais logging", messageLog.contains("Échec de la mise à jour"));
		} finally {
			/* Configuraton initiale du logger */
			logger.removeAppender(appender);
		}
	}

	/**
	 * Teste la méthode public void creerPorteurSujet(Utilisateur utilisateur, Sujet sujet).
	 */
	@Test
	public void test7_1CreerPorteurSujet() {
		//Insertion des dependances
		insererDependances();

		/* Insertion du bean dans la BDD */
		sujetDAO.creerPorteurSujet(daoTesting.getUtilisateur(), daoTesting.getSujet());

		/* Vérification des attributs insérés */
		final List<Sujet> sujets = sujetDAO.trouverSujetsPortes(utilisateur);
		assertEquals("Mauvaise taille", 1, sujets.size());
		daoTesting.compareObjects(daoTesting.getSujet(), sujets.get(0));
	}

	/**
	 * Teste la méthode public List<Sujet> trouverSujetsPortes(Utilisateur utilisateur).
	 */
	@Test
	public void test7_2TrouverSujetsPortes() throws IllegalAccessException {
		//Insertion des dependances
		insererDependances();
		PorteurSujet porteurSujet = daoTesting.getPorteurSujet();
		daoTesting.createObjectInDatabase(porteurSujet);

		/* Test */
		final List<Sujet> sujets = sujetDAO.trouverSujetsPortes(utilisateur);
		assertEquals("Mauvaise taille", 1, sujets.size());
		daoTesting.compareObjects(daoTesting.getSujet(), sujets.get(0));
	}

	/**
	 * Teste la méthode public List<Sujet> listerSujetsSansReferent().
	 */
	@Test
	public void test8_1ListerSujetsSansReferent() throws IllegalAccessException {
		insererDependances();
		Sujet sujet2 = daoTesting.getSujet();
		sujet2.setIdSujet(2L);
		sujet2.setTitre(STRING_TEST_TITRE_SANS_REFERENT);
		sujet2.setEtat(EtatSujet.PUBLIE);
		Professeur professeur = daoTesting.getProfesseur();
		ProfesseurSujet professeurSujet = daoTesting.getProfesseurSujet();

		daoTesting.createObjectInDatabase(sujet2);
		daoTesting.createObjectInDatabase(professeur);
		daoTesting.createObjectInDatabase(professeurSujet);

		/* Recherche du bean dans la BDD */
		final List<Sujet> sujets = sujetDAO.listerSujetsSansReferent();

		/* Vérification que le bean inséré est présent dans la liste */
		boolean estPresent = false;
		int i = 0;
		while (!estPresent && i < sujets.size()) {

			if (STRING_TEST_TITRE_SANS_REFERENT.equals(sujets.get(i).getTitre())) {
				estPresent = true;
				daoTesting.compareObjects(sujet2, sujets.get(i));
			}
			i++;
		}
		assertTrue("Le bean inséré n'est pas présent dans la liste", estPresent);
	}


	/**
	 * Teste la méthode public void creerPorteurSujet(Utilisateur utilisateur, Sujet sujet).
	 * <p>Cas de l'insertion d'un bean déjà présent dans la BDD.</p>
	 */
	@Test
	public void test8_2CreerPorteurSujetLog() {
		insererDependances();

		/* Configuraton du logger */
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		Layout layout = new SimpleLayout();
		Appender appender = new WriterAppender(layout, out);
		logger.addAppender(appender);

		try {
			/* Insertion du bean dans la BDD */
			sujetDAO.creerPorteurSujet(new Utilisateur(), new Sujet());

			/* Vérification du logging */
			String messageLog = out.toString();
			assertTrue("Mauvais logging", messageLog.contains("Échec de la création de porteurSujet"));
		} finally {
			/* Configuraton initiale du logger */
			logger.removeAppender(appender);
		}
	}

	/**
	 * Teste la méthode public void creerProfesseurSujet(Professeur professeur, Sujet sujet, String fonction, String valide).
	 */
	@Test
	public void test9_1CreerProfesseurSujet() throws IllegalAccessException {
		insererDependances();
		Professeur professeur = daoTesting.getProfesseur();

		daoTesting.createObjectInDatabase(professeur);

		/* Insertion du bean dans la BDD */
		sujetDAO.creerProfesseurSujet(professeur, daoTesting.getSujet(), "referent", "non");

		/* Vérification des attributs insérés */
		final List<Sujet> sujets = sujetDAO.trouverSujetsProfesseur(professeur);
		boolean estPresent = false;
		int i = 0;
		while (!estPresent && i < sujets.size()) {
			if (daoTesting.getSujet().getTitre().equals(sujets.get(i).getTitre())) {
				estPresent = true;
				daoTesting.compareObjects(daoTesting.getSujet(), sujets.get(i));
			}
			i++;
		}
		assertTrue("Mauvais titre", estPresent);
	}

	/**
	 * Teste la méthode public List<Sujet> trouverSujetsProfesseur(Professeur professeur).
	 */
	@Test
	public void test9_2TrouverSujetsProfesseur() throws IllegalAccessException {
		insererDependances();
		Professeur professeur = daoTesting.getProfesseur();
		ProfesseurSujet professeurSujet = daoTesting.getProfesseurSujet();
		daoTesting.createObjectInDatabase(professeur);
		daoTesting.createObjectInDatabase(professeurSujet);

		/* Recherche du bean dans la BDD */
		final List<Sujet> sujets = sujetDAO.trouverSujetsProfesseur(professeur);

		/* Vérification des attributs insérés */
		boolean estPresent = false;
		int i = 0;
		while (!estPresent && i < sujets.size()) {
			if (daoTesting.getSujet().getTitre().equals(sujets.get(i).getTitre())) {
				estPresent = true;
				daoTesting.compareObjects(daoTesting.getSujet(), sujets.get(i));
			}
			i++;
		}
		assertTrue("Mauvais titre", estPresent);
	}

	/**
	 * Teste la méthode public boolean possedePoster(Sujet sujet).
	 * <p>Cas du succès de la recherche.</p>
	 */
	@Test
	public void test9_3PossedePosterVrai() {
		/* Recherche du bean dans la BDD */
		final Sujet sujet = daoTesting.getSujet();
		final boolean possedePoster = sujetDAO.possedePoster(sujet);

		/* Vérification des attributs trouvés */
		final boolean resultatAttendu = false;
		assertEquals("Mauvais possedePoster", resultatAttendu, possedePoster);
	}

	/**
	 * Teste la méthode public List<Sujet> trouverSujetsPostersValidesOptions(List<OptionESEO> options).
	 */
	@Test
	public void test9_4TrouverSujetsPostersValidesOptions() throws IllegalAccessException {
		insererDependances();
		Poster poster = daoTesting.getPoster();
		Equipe equipe = daoTesting.getEquipe();
		poster.setValide("oui");

		daoTesting.createObjectInDatabase(equipe);
		daoTesting.createObjectInDatabase(poster);

		/* Recherche du bean dans la BDD */
		final List<OptionESEO> options = new ArrayList<>();
		options.add(daoTesting.getOptionESEO());
		final List<Sujet> sujets = sujetDAO.trouverSujetsPostersValidesOptions(options);

		/* Vérification que le bean inséré est présent dans la liste */
		boolean estPresent = false;
		int i = 0;
		while (!estPresent && i < sujets.size()) {
			if (daoTesting.getSujet().getTitre().equals(sujets.get(i).getTitre())) {
				estPresent = true;
				daoTesting.compareObjects(daoTesting.getSujet(), sujets.get(i));
			}
			i++;
		}
		assertTrue("Le bean inséré n'est pas présent dans la liste", estPresent);
	}

	/**
	 * Teste la méthode public List<Sujet> trouverSujetOption(OptionESEO optionESEO).
	 */
	@Test
	public void test9_5TrouverSujetOption() throws IllegalAccessException {
		insererDependances();
		Equipe equipe = daoTesting.getEquipe();
		OptionSujet optionSujet = daoTesting.getOptionSujet();

		daoTesting.createObjectInDatabase(equipe);
		daoTesting.createObjectInDatabase(optionSujet);

		/* Recherche du bean dans la BDD */
		final List<Sujet> sujets = sujetDAO.trouverSujetOption(daoTesting.getOptionESEO());

		/* Vérification que le bean inséré est présent dans la liste */
		boolean estPresent = false;
		int i = 0;
		while (!estPresent && i < sujets.size()) {
			if (daoTesting.getSujet().getTitre().equals(sujets.get(i).getTitre())) {
				estPresent = true;
				daoTesting.compareObjects(daoTesting.getSujet(), sujets.get(i));
			}
			i++;
		}
		assertTrue("Le bean inséré n'est pas présent dans la liste", estPresent);
	}

	/**
	 * Teste la méthode public void attribuerOptionsSujet(Sujet sujet, List<OptionESEO> options).
	 */
	@Test
	public void test9_6AttribuerOptionsSujet() {
		insererDependances();
		/* Insertion du bean dans la BDD */
		final List<OptionESEO> options = new ArrayList<>();
		options.add(daoTesting.getOptionESEO());
		sujetDAO.attribuerOptionsSujet(daoTesting.getSujet(), options);

		/* Vérification que le bean inséré est présent dans la liste */
		List<Sujet> sujets = sujetDAO.trouverSujetOption(daoTesting.getOptionESEO());
		boolean estPresent = false;
		int i = 0;
		while (!estPresent && i < sujets.size()) {
			if (daoTesting.getSujet().getTitre().equals(sujets.get(i).getTitre())) {
				estPresent = true;
			}
			i++;
		}
		assertTrue("Le bean inséré n'est pas présent dans la liste", estPresent);
	}

	/**
	 * Insere les dependances en BDD en supprimant en avance toute insertion réalisée
	 */
	private void insererDependances() {
		try {
			daoTesting.truncateTable(OptionESEO.class);
			daoTesting.truncateTable(AnneeScolaire.class);
			daoTesting.truncateTable(Utilisateur.class);
			daoTesting.truncateTable(PorteurSujet.class);
			daoTesting.truncateTable(Sujet.class);
			daoTesting.truncateTable(Professeur.class);
			daoTesting.truncateTable(ProfesseurSujet.class);
			daoTesting.truncateUsedTables();
		} catch (SQLException e) {
			fail(e.getMessage());
		}


		OptionESEO optionESEO = daoTesting.getOptionESEO();
		AnneeScolaire anneeScolaire = daoTesting.getAnneeScolaire();
		Utilisateur utilisateur = daoTesting.getUtilisateur();
		Sujet sujet = daoTesting.getSujet();


		try {
			daoTesting.createObjectInDatabase(optionESEO);
			daoTesting.createObjectInDatabase(anneeScolaire);
			daoTesting.createObjectInDatabase(utilisateur);
			daoTesting.createObjectInDatabase(sujet);
		} catch (IllegalAccessException e) {
			fail(e.getMessage());
		}

	}
}