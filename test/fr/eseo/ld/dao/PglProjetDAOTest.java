package fr.eseo.ld.dao;

import fr.eseo.ld.beans.Professeur;
import fr.eseo.ld.beans.Utilisateur;
import fr.eseo.ld.pgl.beans.Projet;
import fr.eseo.testing.DAOTesting;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.sql.SQLException;

/**
 * Classe de test de ProjetDAO
 *
 * @author Tristan LE GACQUE
 * @see PglProjetDAO
 **/
public class PglProjetDAOTest {

    private static DAOTesting<Projet> daoTesting;

    /**
     * Définition du DaoTesting
     *
     * @throws Exception exception
     */
    @BeforeClass
    public static void setUp() throws Exception {
        //Définition du dao factory
        daoTesting = new DAOTesting<>();
        daoTesting.initDao(daoTesting.getDaoFactory().getPglProjetDAO());
        daoTesting.addAlias(Projet.class, "pgl_projet");

        daoTesting.truncateTable(Utilisateur.class);
        daoTesting.truncateTable(Professeur.class);
    }

    @AfterClass
    public static void tearDown() throws Exception {
        daoTesting.truncateUsedTables();
    }

    @Before
    public void beforeTest() throws SQLException, IllegalAccessException {
        daoTesting.truncateUsedTables();

        //Définition des dépendances
        Utilisateur user = daoTesting.getUtilisateur();
        Professeur professeur = daoTesting.getProfesseur();

        //Ajout des objets a la base de données
        daoTesting.createObjectInDatabase(user);
        daoTesting.createObjectInDatabase(professeur);
    }


    /**
     * Teste la méthode public void creer.
     *
     * @see PglProjetDAO#creer(Projet)
     */
    @Test
    public void testCreer() {
        /* Insertion du bean dans la BDD */
        Projet pglprojet = daoTesting.getProjet();
        Projet pglprojetAttendu = daoTesting.getProjet();

        daoTesting.testCreerCRUD(pglprojet, pglprojetAttendu);
    }

    /**
     * Teste la méthode public trouver
     *
     * @see PglProjetDAO#trouver(Projet)
     */
    @Test
    public void testTrouver() {
        /* Recherche du bean dans la BDD */
        Projet pglprojet = daoTesting.getProjet();
        Projet pglprojetAttendu = daoTesting.getProjet();

        daoTesting.testTrouverCRUD(pglprojet, pglprojetAttendu);
    }


    /**
     * Teste la méthode public void modifier
     *
     * @see PglProjetDAO#modifier(Projet)
     */
    @Test
    public void testModifier() {
        Projet pglprojet = daoTesting.getProjet();
        Projet pglprojetMod = daoTesting.getProjet();

        pglprojetMod.setTitre("Titre2");

        daoTesting.testModifierCRUD(pglprojet, pglprojetMod, pglprojetMod);
    }

    /**
     * Teste la méthode public lister().
     *
     * @see PglProjetDAO#lister()
     */
    @Test
    public void testLister() {
        Projet pglprojet = daoTesting.getProjet();
        Projet pglprojetAttendu = daoTesting.getProjet();

        daoTesting.testListerCRUD(new Projet[]{pglprojet}, new Projet[]{pglprojetAttendu});
    }

    /**
     * Teste la méthode public void supprimer(NoteInteretTechno NoteInteretTechno).
     *
     * @see PglProjetDAO#supprimer(Projet)
     */
    @Test
    public void testSupprimer() {
        Projet pglprojet = daoTesting.getProjet();

        daoTesting.testSupprimerCRUD(pglprojet);
    }

    /**
     * Test les erreurs
     */
    @Test
    public void testLogging() {
        //Aucune erreur si on insère des objets null, toutes les valeurs par défaut sont null
    }
}