package fr.eseo.ld.dao;

import fr.eseo.ld.beans.Commentaire;
import fr.eseo.ld.beans.Sujet;
import fr.eseo.ld.beans.Utilisateur;
import fr.eseo.testing.DAOTesting;
import org.apache.log4j.*;
import org.easymock.EasyMock;
import org.easymock.EasyMockRunner;
import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

import java.io.ByteArrayOutputStream;
import java.sql.SQLException;
import java.util.Random;

import static org.easymock.EasyMock.*;
import static org.junit.Assert.assertTrue;

/**
 * Classe de tests unitaires JUnit 4 de la classe CommentaireDAO.
 *
 * <p>Utilisation du modèle DAO.</p>
 *
 * @version 1.0
 * @author Maxime LENORMAND
 *
 * @see fr.eseo.ld.dao.CommentaireDAO
 * @see org.junit
 */
@RunWith(EasyMockRunner.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class CommentaireDAOTest {
	
	private static final String STRING_TEST_MODIFIE = "test2";

    private static Logger logger = Logger.getLogger(CommentaireDAO.class.getName());

	private static DAOTesting<Commentaire> daoTesting;
    private static CommentaireDAO commentaireDAO;

    /**
	 * Récupère une instance de DAO et initialise le bean correspondant.
	 * 
	 * @throws Exception exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		daoTesting = new DAOTesting<>();
		daoTesting.initDao(daoTesting.getDaoFactory().getCommentaireDao());

		commentaireDAO = daoTesting.getDaoFactory().getCommentaireDao();
	}

    @AfterClass
    public static void tearDown() throws Exception {
        daoTesting.truncateUsedTables();
    }

    @Before
    public void beforeTest() throws SQLException, IllegalAccessException {
        daoTesting.truncateUsedTables();
        Sujet s = daoTesting.getSujet();
        Utilisateur u = daoTesting.getUtilisateur();

        daoTesting.createObjectInDatabase(s);
        daoTesting.createObjectInDatabase(u);
    }


	
	/**
	 * Teste la méthode public void creer(Commentaire commentaire).
	 */
	@Test
	public void test1_1Creer() {
	    Commentaire commentaire = daoTesting.getCommentaire();

	    daoTesting.testCreerCRUD(commentaire, commentaire);
	}
	
	/**
	 * Teste la méthode public void creer(Commentaire commentaire).
	 * <p>Cas d'une erreur lors de la création de la connexion.</p>
	 * @throws SQLException exception
	 */
	@Test
	public void test1_2CreerLog() throws SQLException {
		/* Configuraton du logger */
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		Layout layout = new SimpleLayout();
		Appender appender = new WriterAppender(layout, out);
		logger.addAppender(appender);
		
		try {
			/* Mock de la méthode creerConnexion() */
			CommentaireDAO commentaireDAOMock = EasyMock.createMockBuilder(CommentaireDAO.class)
					.addMockedMethod("creerConnexion").createMock();
			expect(commentaireDAOMock.creerConnexion()).andThrow(new SQLException());
			
			replay(commentaireDAOMock);
			
			/* Insertion du bean dans la BDD */
			commentaireDAOMock.creer(daoTesting.getCommentaire());
			
			/* Vérification du logging */
			String messageLog = out.toString();
			assertTrue("Mauvais logging", messageLog.contains("Échec de la création de l'objet"));
			
			verify(commentaireDAOMock);
		} finally {
			/* Configuraton initiale du logger */
			logger.removeAppender(appender);
		}
	}
	
	/**
	 * Teste la méthode public List<Commentaire> trouver(Commentaire commentaire).
	 */
	@Test
	public void test2_1Trouver() {
		/* Recherche du bean dans la BDD */
        Commentaire commentaire = daoTesting.getCommentaire();

        daoTesting.testTrouverCRUD(commentaire, commentaire);
	}
	
	/**
	 * Teste la méthode public List<Commentaire> trouver(Commentaire commentaire).
	 * <p>Cas de la recherche d'un bean sans attributs.</p>
	 */
	@Test
	public void test2_2TrouverLog() {
		/* Configuraton du logger */
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		Layout layout = new SimpleLayout();
		Appender appender = new WriterAppender(layout, out);
		logger.addAppender(appender);
		
		try {
			/* Recherche du bean sans attributs dans la BDD */
			Commentaire commentaire = new Commentaire();
			commentaireDAO.trouver(commentaire);

			/* Vérification du logging */
			String messageLog = out.toString();
			assertTrue("Mauvais logging", messageLog.contains("Échec de la recherche"));
		} finally {
			/* Configuraton initiale du logger */
			logger.removeAppender(appender);
		}
	}
	
	/**
	 * Teste la méthode public void modifier(Commentaire commentaire).
	 */
	@Test
	public void test3_1Modifier() {
		/* Modification du bean dans la BDD */
        Commentaire commentaire = daoTesting.getCommentaire();
        Commentaire commentaireMod = daoTesting.getCommentaire();

        commentaireMod.setContenu(STRING_TEST_MODIFIE);
        daoTesting.testModifierCRUD(commentaire, commentaireMod, commentaireMod);
	}
	
	/**
	 * Teste la méthode public List<Commentaire> lister().
     * METHOD NOT IMPLEMENTED
	 */
	@Ignore("Methode non implémentée")
	public void test4_1Lister() throws IllegalAccessException {
        Commentaire[] commentaires = new Commentaire[10];
        Commentaire[] commentairesAttendus = new Commentaire[10];
        String alphabet = "abcdefghijklmnopqrstuvwxyz ";
        for(int i = 0; i < commentaires.length; i++) {
            Random r = new Random();
            StringBuilder randomContent = new StringBuilder();
            for(int j = 0; j < 50; j++) {
                randomContent.append(alphabet.charAt(r.nextInt(alphabet.length())));
            }

            commentaires[i] = daoTesting.getCommentaire();
            commentaires[i].setContenu(randomContent.toString());
            commentaires[i].setIdCommentaire(i+1L);

            commentairesAttendus[i] = daoTesting.getCommentaire();
            commentairesAttendus[i].setContenu(randomContent.toString());
            commentairesAttendus[i].setIdCommentaire(i+1L);
        }

        daoTesting.testListerCRUD(commentaires, commentairesAttendus);
	}
	
	/**
	 * Teste la méthode public void supprimer(Commentaire commentaire).
	 */
	@Test
	public void test5_1Supprimer() {
		/* Suppression du bean dans la BDD */
        Commentaire commentaire = daoTesting.getCommentaire();
        daoTesting.testSupprimerCRUD(commentaire);
	}
	
	/**
	 * Teste la méthode public void supprimer(Commentaire commentaire).
	 * <p>Cas de la suppression d'un bean non présent dans la BDD.</p>
	 */
	@Test
	public void test5_2SupprimerLog() {
		/* Configuraton du logger */
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		Layout layout = new SimpleLayout();
		Appender appender = new WriterAppender(layout, out);
		logger.addAppender(appender);

		try {
			/* Suppression du bean dans la BDD */
			commentaireDAO.supprimer(daoTesting.getCommentaire());

			/* Vérification du logging */
			String messageLog = out.toString();
			assertTrue("Mauvais logging", messageLog.contains("Échec de la suppression"));
		} finally {
			/* Configuraton initiale du logger */
			logger.removeAppender(appender);
		}
	}
	
}