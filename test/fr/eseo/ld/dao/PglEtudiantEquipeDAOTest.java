package fr.eseo.ld.dao;

import fr.eseo.ld.beans.AnneeScolaire;
import fr.eseo.ld.beans.OptionESEO;
import fr.eseo.ld.beans.Utilisateur;
import fr.eseo.ld.pgl.beans.Equipe;
import fr.eseo.ld.pgl.beans.Etudiant;
import fr.eseo.ld.pgl.beans.EtudiantEquipe;
import fr.eseo.ld.pgl.beans.Sprint;
import fr.eseo.testing.DAOTesting;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.sql.SQLException;
import java.util.List;

import static junit.framework.TestCase.assertEquals;

/**
 * Classe de test de EtudiantEquipeDAO
 *
 * @author Tristan LE GACQUE
 * @see PglEtudiantEquipeDAO
 **/
public class PglEtudiantEquipeDAOTest {

    private static DAOTesting<EtudiantEquipe> daoTesting;
    private static PglEtudiantEquipeDAO dao;

    /**
     * Définition du DaoTesting
     *
     * @throws Exception exception
     */
    @BeforeClass
    public static void setUp() throws Exception {
        //Définition du dao factory
        daoTesting = new DAOTesting<>();
        daoTesting.initDao(daoTesting.getDaoFactory().getPglEtudiantEquipeDAO());
        dao = daoTesting.getDaoFactory().getPglEtudiantEquipeDAO();

        daoTesting.addAlias(Equipe.class, "pgl_equipe");
        daoTesting.addAlias(Sprint.class, "pgl_sprint");
        daoTesting.addAlias(Etudiant.class, "pgl_etudiant");
        daoTesting.addAlias(EtudiantEquipe.class, "pgl_etudiantequipe");

        daoTesting.truncateTable(OptionESEO.class);
        daoTesting.truncateTable(Utilisateur.class);
        daoTesting.truncateTable(AnneeScolaire.class);
        daoTesting.truncateTable(Equipe.class);
        daoTesting.truncateTable(Etudiant.class);
        daoTesting.truncateTable(Sprint.class);
        daoTesting.truncateTable(Equipe.class);
    }

    @AfterClass
    public static void tearDown() throws Exception {
        daoTesting.truncateUsedTables();
    }

    @Before
    public void beforeTest() throws SQLException, IllegalAccessException {
        daoTesting.truncateUsedTables();

        //Définition des dépendances
        OptionESEO optionESEO = daoTesting.getOptionESEO();
        Utilisateur user = daoTesting.getUtilisateur();
        AnneeScolaire anneeScolaire =daoTesting.getAnneeScolaire();
        Sprint sprint = daoTesting.getSprint();
        Sprint sprint2 = daoTesting.getSprint();
        Sprint sprint3 = daoTesting.getSprint();
        Etudiant etudiant = daoTesting.getEtudiantPgl();
        Equipe equipe = daoTesting.getEquipePGL();
        Equipe equipe2 = daoTesting.getEquipePGL();
        equipe2.setNom("equipe2");
        equipe2.setIdEquipe(2L);
        sprint2.setNumero(2);
        sprint2.setIdSprint(2L);
        sprint2.setDateDebut("2018-06-01");
        sprint3.setNumero(3);
        sprint3.setIdSprint(3L);
        sprint3.setDateDebut("2018-08-01");

        //Ajout des objets a la base de données
        daoTesting.createObjectInDatabase(optionESEO);
        daoTesting.createObjectInDatabase(anneeScolaire);
        daoTesting.createObjectInDatabase(user);
        daoTesting.createObjectInDatabase(etudiant);
        daoTesting.createObjectInDatabase(sprint);
        daoTesting.createObjectInDatabase(sprint2);
        daoTesting.createObjectInDatabase(sprint3);
        daoTesting.createObjectInDatabase(equipe);
        daoTesting.createObjectInDatabase(equipe2);
    }


    /**
     * Teste la méthode public void creer.
     *
     * @see PglEtudiantEquipeDAO#creer(EtudiantEquipe)
     */
    @Test
    public void testCreer() {
        /* Insertion du bean dans la BDD */
        EtudiantEquipe pgletudiantequipe = daoTesting.getEtudiantEquipePgl();
        EtudiantEquipe att1 = daoTesting.getEtudiantEquipePgl();
        EtudiantEquipe att2 = daoTesting.getEtudiantEquipePgl();
        EtudiantEquipe att3 = daoTesting.getEtudiantEquipePgl();
        att2.setIdSprint(2L);
        att3.setIdSprint(3L);

        dao.creer(pgletudiantequipe);

        //On dois trouver 3 entrées
        final List<EtudiantEquipe> lister = dao.lister();
        assertEquals("Il n'y a pas assez d'entrées", 3, lister.size());
        daoTesting.compareObjects(att1, lister.get(0));
        daoTesting.compareObjects(att2, lister.get(1));
        daoTesting.compareObjects(att3, lister.get(2));
    }

    /**
     * Teste la méthode public void creer.
     *
     * @see PglEtudiantEquipeDAO#creer(EtudiantEquipe)
     */
    @Test
    public void testCreer_2() {
        /* Insertion du bean dans la BDD */
        EtudiantEquipe att2 = daoTesting.getEtudiantEquipePgl();
        EtudiantEquipe att3 = daoTesting.getEtudiantEquipePgl();
        att2.setIdSprint(2L);
        att3.setIdSprint(3L);

        dao.creer(att2);

        //On dois trouver 3 entrées
        final List<EtudiantEquipe> lister = dao.lister();
        assertEquals("Il n'y a pas assez d'entrées", 2, lister.size());
        daoTesting.compareObjects(att2, lister.get(0));
        daoTesting.compareObjects(att3, lister.get(1));
    }

    /**
     * Teste la méthode public trouver
     *
     * @see PglEtudiantEquipeDAO#trouver(EtudiantEquipe)
     */
    @Test
    public void testTrouver() {
        /* Recherche du bean dans la BDD */
        EtudiantEquipe pgletudiantequipe = daoTesting.getEtudiantEquipePgl();
        EtudiantEquipe pgletudiantequipeAttendu = daoTesting.getEtudiantEquipePgl();

        daoTesting.testTrouverCRUD(pgletudiantequipe, pgletudiantequipeAttendu);
    }


    /**
     * Teste la méthode public void modifier
     *
     * @see PglEtudiantEquipeDAO#modifier(EtudiantEquipe)
     */
    @Test
    public void testModifier() {
        EtudiantEquipe pgletudiantequipe = daoTesting.getEtudiantEquipePgl();
        EtudiantEquipe pgletudiantequipeMod = daoTesting.getEtudiantEquipePgl();

        pgletudiantequipeMod.setIdSprint(2L);
        pgletudiantequipeMod.setIdEquipe(2L);
        EtudiantEquipe att1 = daoTesting.getEtudiantEquipePgl();
        EtudiantEquipe att2 = daoTesting.getEtudiantEquipePgl();
        EtudiantEquipe att3 = daoTesting.getEtudiantEquipePgl();
        att2.setIdSprint(2L);
        att3.setIdSprint(3L);
        att2.setIdEquipe(2L);
        att3.setIdEquipe(2L);


        dao.creer(pgletudiantequipe);
        //Tester
        dao.modifier(pgletudiantequipeMod);

        //On dois trouver 3 entrées
        final List<EtudiantEquipe> lister = dao.lister();
        assertEquals("Il n'y a pas assez d'entrées", 3, lister.size());
        daoTesting.compareObjects(att1, lister.get(0));
        daoTesting.compareObjects(att2, lister.get(1));
        daoTesting.compareObjects(att3, lister.get(2));
    }

    /**
     * Teste la méthode public lister().
     *
     * @see PglEtudiantEquipeDAO#lister()
     */
    @Test
    public void testLister() {
        EtudiantEquipe pgletudiantequipe = daoTesting.getEtudiantEquipePgl();
        EtudiantEquipe pgletudiantequipeAttendu = daoTesting.getEtudiantEquipePgl();

        daoTesting.testListerCRUD(new EtudiantEquipe[]{pgletudiantequipe}, new EtudiantEquipe[]{pgletudiantequipeAttendu});
    }

    /**
     * Teste la méthode public void supprimer(NoteInteretTechno NoteInteretTechno).
     *
     * @see PglEtudiantEquipeDAO#supprimer(EtudiantEquipe)
     */
    @Test
    public void testSupprimer() {
        EtudiantEquipe pgletudiantequipe = daoTesting.getEtudiantEquipePgl();

        daoTesting.testSupprimerCRUD(pgletudiantequipe);
    }

    /**
     * Test les erreurs
     */
    @Test
    public void testLogging() {
        //daoTesting.testLoggingCRUD();
    }
}