package fr.eseo.ld.dao;


import fr.eseo.ld.beans.Invite;
import fr.eseo.ld.beans.Meeting;
import fr.eseo.ld.beans.Utilisateur;
import fr.eseo.testing.DAOTesting;
import org.easymock.EasyMockRunner;
import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

import java.sql.SQLException;

/**
 * Classe de tests unitaires JUnit 4 de la classe InviteReunionDAO.
 *
 * <p>Utilisation du modèle DAO.</p>
 *
 * @author Dimitri J
 * @version 1.0
 * @see InviteReunionDAO
 * @see org.junit
 */
@RunWith(EasyMockRunner.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class InviteReunionDAOTest {

    private static DAOTesting<Invite> daoTesting;

    /**
     * Définition du DaoTesting
     * @throws Exception exception
     */
    @BeforeClass
    public static void setUp() throws Exception {
        //Définition du dao factory
        daoTesting = new DAOTesting<>();
        daoTesting.initDao(daoTesting.getDaoFactory().getInviteReunionDAO());
        daoTesting.addAlias(Invite.class, "Invite_reunion");
        daoTesting.addAlias(Meeting.class, "Reunion");

        daoTesting.truncateTable(Utilisateur.class);
        daoTesting.truncateTable(Meeting.class);
    }

    @AfterClass
    public static void tearDown() throws Exception {
        daoTesting.truncateUsedTables();
    }

    @Before
    public void beforeTest() throws SQLException, IllegalAccessException {
        daoTesting.truncateUsedTables();

        //Définition des dépendances
        Utilisateur user = daoTesting.getUtilisateur();
        Meeting reunion = daoTesting.getMeeting();

        //Ajout des objets a la base de données
        daoTesting.createObjectInDatabase(user);
        daoTesting.createObjectInDatabase(reunion);
    }

    /**
     * Teste la méthode public void creer(Invite invite).
     */
    @Test
    public void test1_1Creer() {
        /* Insertion du bean dans la BDD */
        Invite invite = daoTesting.getInvite();

        daoTesting.testCreerCRUD(invite, invite);
    }

    /**
     * Teste la méthode public List<Invite> trouver(Invite invite).
     */
    @Test
    public void test2_1Trouver() {
        /* Insertion du bean dans la BDD */
        Invite invite = daoTesting.getInvite();

        daoTesting.testTrouverCRUD(invite, invite);
    }

    /**
     * Teste la méthode public void modifier(Invite invite).
     */
    @Test
    public void test3_1Modifier() {
        Invite invite = daoTesting.getInvite();
        Invite inviteM = daoTesting.getInvite();
        inviteM.setParticipe("0");

        daoTesting.testModifierCRUD(invite, inviteM, inviteM);
    }


    /**
     * Teste la méthode public List<Invite> lister().
     */
    @Test
    public void test4_1Lister() {
        /* Insertion du bean dans la BDD */
        Invite invite = daoTesting.getInvite();

        daoTesting.testListerCRUD(new Invite[]{invite}, new Invite[]{invite});
    }

    /**
     * Teste la méthode public void supprimer(Invite invite).
     */
    @Test
    public void test5_1Supprimer() {
        /* Insertion du bean dans la BDD */
        Invite invite = daoTesting.getInvite();

        daoTesting.testSupprimerCRUD(invite);
    }

}
