package fr.eseo.ld.dao;

import fr.eseo.ld.beans.*;
import fr.eseo.testing.DAOTesting;
import org.apache.log4j.*;
import org.easymock.EasyMock;
import org.easymock.EasyMockRunner;
import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

import java.io.ByteArrayOutputStream;
import java.sql.SQLException;

import static org.easymock.EasyMock.*;
import static org.junit.Assert.assertTrue;

/**
 * Classe de tests d'intégration JUnit 4 de la classe JurySoutenanceDAO.
 *
 * <p>Utilisation du modèle DAO.</p>
 *
 * @version 1.0
 * @author Maxime LENORMAND
 *
 * @see fr.eseo.ld.dao.JurySoutenanceDAO
 * @see org.junit
 */
@RunWith(EasyMockRunner.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class JurySoutenanceDAOTest {

	private static final Long LONG_IDETU = 2L;

	private static Logger logger = Logger.getLogger(JurySoutenanceDAO.class.getName());

	private static JurySoutenanceDAO jurySoutenanceDAO;

	private static DAOTesting<JurySoutenance> daoTesting;

	/**
	 * Définition du DaoTesting
	 * @throws Exception exception
	 */
	@BeforeClass
	public static void setUp() throws Exception {
		//Définition du dao factory
		daoTesting = new DAOTesting<>();
		daoTesting.initDao(daoTesting.getDaoFactory().getJurySoutenanceDAO());
		jurySoutenanceDAO = daoTesting.getDaoFactory().getJurySoutenanceDAO();

		daoTesting.truncateTable(Utilisateur.class);
		daoTesting.truncateTable(Professeur.class);
		daoTesting.truncateTable(Sujet.class);
		daoTesting.truncateTable(Etudiant.class);
		daoTesting.truncateTable(OptionESEO.class);
		daoTesting.truncateTable(AnneeScolaire.class);
		daoTesting.truncateTable(Equipe.class);
		daoTesting.truncateTable(EtudiantEquipe.class);
	}

	@AfterClass
	public static void tearDown() throws Exception {
		daoTesting.truncateUsedTables();
	}

	@Before
	public void beforeTest() throws SQLException, IllegalAccessException {
		daoTesting.truncateUsedTables();

		//Définition des dépendances
		Utilisateur user = daoTesting.getUtilisateur();
		Utilisateur user2 = daoTesting.getUtilisateur();
		Professeur prof = daoTesting.getProfesseur();
		Sujet sujet = daoTesting.getSujet();
		Etudiant etu = daoTesting.getEtudiant();
		OptionESEO option = daoTesting.getOptionESEO();
		AnneeScolaire annee = daoTesting.getAnneeScolaire();
		Equipe equipe = daoTesting.getEquipe();
		EtudiantEquipe etudiantEquipe = daoTesting.getEtudiantEquipe();
		user2.setIdUtilisateur(LONG_IDETU);
		user2.setIdentifiant("testtest2");
		user2.setEmail("no2@no.fr");
		etu.setIdEtudiant(LONG_IDETU);
		etudiantEquipe.setIdEtudiant(LONG_IDETU);

		//Ajout des objets a la base de données
		daoTesting.createObjectInDatabase(option);
		daoTesting.createObjectInDatabase(annee);
		daoTesting.createObjectInDatabase(sujet);
		daoTesting.createObjectInDatabase(user);
		daoTesting.createObjectInDatabase(user2);
		daoTesting.createObjectInDatabase(prof);
		daoTesting.createObjectInDatabase(etu);
		daoTesting.createObjectInDatabase(equipe);
		daoTesting.createObjectInDatabase(etudiantEquipe);
	}


	/**
	 * Teste la méthode public void creer(JurySoutenance jurySoutenance).
	 */
	@Test
	public void test1_1Creer() {
		/* Insertion du bean dans la BDD */
		JurySoutenance jury = daoTesting.getJurySoutenance();
		daoTesting.testCreerCRUD(jury, jury);
	}

	/**
	 * Teste la méthode public List<JurySoutenance> trouver(JurySoutenance jurySoutenance).
	 */
	@Test
	public void test2_1Trouver() {
		JurySoutenance jury = daoTesting.getJurySoutenance();

		daoTesting.testTrouverCRUD(jury, jury);
	}

	/**
	 * Teste la méthode public void modifier(JurySoutenance jurySoutenance).
	 */
	@Test
	public void test3_1Modifier() throws IllegalAccessException {
		JurySoutenance jury = daoTesting.getJurySoutenance();
		JurySoutenance jury2 = daoTesting.getJurySoutenance();

		Utilisateur u3 = daoTesting.getUtilisateur();
		u3.setIdUtilisateur(3L);
		u3.setIdentifiant("testtest3");
		u3.setEmail("no3@no.fr");
		Professeur prof = daoTesting.getProfesseur();
		prof.setIdProfesseur(3L);

		daoTesting.createObjectInDatabase(u3);
		daoTesting.createObjectInDatabase(prof);

		jury2.setIdProf2(3L);
		daoTesting.testModifierCRUD(jury, jury2, jury2);
	}

	/**
	 * Teste la méthode public List<JurySoutenance> lister().
	 */
	@Test
	public void test4_1Lister() {
		JurySoutenance jury = daoTesting.getJurySoutenance();

		daoTesting.testListerCRUD(new JurySoutenance[]{jury}, new JurySoutenance[]{jury});
	}

	/**
	 * Teste la méthode public List<JurySoutenance> lister().
	 * <p>Cas d'une erreur lors de la création de la connexion.</p>
	 * @throws SQLException exception
	 */
	@Test
	public void test4_2ListerLog() throws SQLException {
		/* Configuraton du logger */
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		Layout layout = new SimpleLayout();
		Appender appender = new WriterAppender(layout, out);
		logger.addAppender(appender);

		try {
			/* Mock de la méthode creerConnexion() */
			JurySoutenanceDAO jurySoutenanceDAOMock = EasyMock.createMockBuilder(JurySoutenanceDAO.class)
					.addMockedMethod("creerConnexion").createMock();
			expect(jurySoutenanceDAOMock.creerConnexion()).andThrow(new SQLException());

			replay(jurySoutenanceDAOMock);

			/* Recherche du bean dans la BDD */
			jurySoutenanceDAOMock.lister();

			/* Vérification du logging */
			String messageLog = out.toString();
			assertTrue("Mauvais logging", messageLog.contains("Échec du listage"));

			verify(jurySoutenanceDAOMock);
		} finally {
			/* Configuraton initiale du logger */
			logger.removeAppender(appender);
		}
	}

	/**
	 * Teste la méthode public List<JurySoutenance> listerJurysUtilisateur(Utilisateur utilisateur).
	 * <p>Cas de la recherche d'un bean sans attributs.</p>
	 */
	@Test
	public void test4_3ListerJuryUtilisateurLog() {
		/* Configuraton du logger */
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		Layout layout = new SimpleLayout();
		Appender appender = new WriterAppender(layout, out);
		logger.addAppender(appender);

		try {
			/* Recherche du bean dans la BDD */
			Utilisateur utilisateur = new Utilisateur();
			jurySoutenanceDAO.listerJurysUtilisateur(utilisateur);

			/* Vérification du logging */
			String messageLog = out.toString();
			assertTrue("Mauvais logging", messageLog.contains("Échec du listage des jurySoutenance associés à un utilisateur."));

		} finally {
			/* Configuraton initiale du logger */
			logger.removeAppender(appender);
		}
	}

	/**
	 * Teste la méthode public void supprimer(JurySoutenance jurySoutenance).
	 */
	@Test
	public void test5_1Supprimer() {
		JurySoutenance jury = daoTesting.getJurySoutenance();

		daoTesting.testListerCRUD(new JurySoutenance[]{jury}, new JurySoutenance[]{jury});
	}

	/**
	 * Teste la méthode public void supprimer(JurySoutenance jurySoutenance).
	 * <p>Cas de la suppression d'un bean non présent dans la BDD.</p>
	 */
	@Test
	public void test5_2SupprimerLog() {
		/* Configuraton du logger */
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		Layout layout = new SimpleLayout();
		Appender appender = new WriterAppender(layout, out);
		logger.addAppender(appender);

		try {
			/* Suppression du bean dans la BDD */
			jurySoutenanceDAO.supprimer(new JurySoutenance());

			/* Vérification du logging */
			String messageLog = out.toString();
			assertTrue("Mauvais logging", messageLog.contains("Échec de la suppression"));
		} finally {
			/* Configuraton initiale du logger */
			logger.removeAppender(appender);
		}
	}

	/**
	 * Teste la méthode public void modifier(JurySoutenance jurySoutenance).
	 * <p>Cas de la modification d'un bean non présent dans la BDD.</p>
	 */
	@Test
	public void test6_1ModifierLog() {
		/* Configuraton du logger */
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		Layout layout = new SimpleLayout();
		Appender appender = new WriterAppender(layout, out);
		logger.addAppender(appender);

		try {
			/* Modification du bean dans la BDD */
			jurySoutenanceDAO.modifier(new JurySoutenance());

			/* Vérification du logging */
			String messageLog = out.toString();
			assertTrue("Mauvais logging", messageLog.contains("Échec de la mise à jour"));
		} finally {
			/* Configuraton initiale du logger */
			logger.removeAppender(appender);
		}
	}

}