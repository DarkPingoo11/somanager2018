package fr.eseo.ld.dao;

import fr.eseo.ld.beans.*;
import fr.eseo.testing.DAOTesting;
import org.apache.log4j.*;
import org.easymock.EasyMock;
import org.easymock.EasyMockRunner;
import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

import java.io.ByteArrayOutputStream;
import java.sql.SQLException;

import static org.easymock.EasyMock.*;
import static org.junit.Assert.assertTrue;

/**
 * Classe de tests unitaires JUnit 4 de la classe NoteInteretSujetDAO.
 *
 * <p>Utilisation du modèle DAO.</p>
 *
 * @version 1.0
 * @author Maxime LENORMAND
 *
 * @see fr.eseo.ld.dao.NoteInteretSujetDAO
 * @see org.junit
 */
@RunWith(EasyMockRunner.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class NoteInteretSujetDAOTest {

	private static final Long LONG_TEST_PROFESSEUR = 1L;
	private static final Long LONG_TEST_SUJET = 1L;
	private static final Integer INTEGER_TEST = 1;
	private static final Integer INTEGER_TEST_MODIFIE = 5;

	private static Logger logger = Logger.getLogger(NoteInteretSujetDAO.class.getName());

	private static NoteInteretSujetDAO noteInteretSujetDAO;


	private static DAOTesting<NoteInteretSujet> daoTesting;

	@BeforeClass
	public static void setUp() throws Exception {
		//Définition du dao factory
		daoTesting = new DAOTesting<>();
		daoTesting.initDao(daoTesting.getDaoFactory().getNoteInteretSujetDAO());
		noteInteretSujetDAO = daoTesting.getDaoFactory().getNoteInteretSujetDAO();
		daoTesting.addAlias(NoteInteret.class, NoteInteretSujet.class.getName());
	}

	@AfterClass
	public static void tearDown() throws Exception {
		daoTesting.truncateUsedTables();
	}

	@Before
	public void beforeTest() throws SQLException, IllegalAccessException {
		daoTesting.truncateUsedTables();

		Sujet s = new Sujet();
		s.setDescription("Description");
		s.setTitre("Titre");
		s.setContratPro(false);
		s.setConfidentialite(false);
		s.setNbrMinEleves(0);
		s.setNbrMaxEleves(1);
		s.setEtat(EtatSujet.DEPOSE);
		Utilisateur u = new Utilisateur();
		u.setIdUtilisateur(LONG_TEST_PROFESSEUR);
		u.setEmail("no@no.fr");
		u.setPrenom("PRenom");
		u.setNom("Nom");
		u.setValide("oui");
		u.setIdentifiant("testtesttest");
		u.setHash("hash");
		Professeur prof = new Professeur();
		prof.setIdProfesseur(LONG_TEST_PROFESSEUR);

		daoTesting.createObjectInDatabase(s);
		daoTesting.createObjectInDatabase(u);
		daoTesting.createObjectInDatabase(prof);
	}

	public NoteInteretSujet getNoteInteretSujet() {
		NoteInteretSujet noteInteretSujet = new NoteInteretSujet();
		noteInteretSujet.setIdProfesseur(LONG_TEST_PROFESSEUR);
		noteInteretSujet.setIdSujet(LONG_TEST_SUJET);
		noteInteretSujet.setNote(INTEGER_TEST);
		return noteInteretSujet;
	}

	/**
	 * Teste la méthode public void creer(NoteInteretSujet noteInteretSujet).
	 */
	@Test
	public void test1_1Creer() {
		/* Insertion du bean dans la BDD */
		NoteInteretSujet noteInteretSujet = getNoteInteretSujet();

		daoTesting.testCreerCRUD(noteInteretSujet, noteInteretSujet);
	}

	/**
	 * Teste la méthode public List<NoteInteretSujet> trouver(NoteInteretSujet noteInteretSujet).
	 */
	@Test
	public void test2_1Trouver() {
		/* Recherche du bean dans la BDD */
		NoteInteretSujet noteInteretSujet = getNoteInteretSujet();

		daoTesting.testTrouverCRUD(noteInteretSujet, noteInteretSujet);
	}

	/**
	 * Teste la méthode public List<NoteInteretSujet> trouver(NoteInteretSujet noteInteretSujet).
	 * <p>Cas de la recherche d'un bean sans attributs.</p>
	 */
	@Test
	public void test2_2TrouverLog() {
		/* Configuraton du logger */
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		Layout layout = new SimpleLayout();
		Appender appender = new WriterAppender(layout, out);
		logger.addAppender(appender);

		try {
			/* Recherche du bean sans attributs dans la BDD */
			NoteInteretSujet noteInteretSujet = new NoteInteretSujet();
			noteInteretSujetDAO.trouver(noteInteretSujet);

			/* Vérification du logging */
			String messageLog = out.toString();
			assertTrue("Mauvais logging", messageLog.contains("Échec de la recherche"));
		} finally {
			/* Configuraton initiale du logger */
			logger.removeAppender(appender);
		}
	}

	/**
	 * Teste la méthode public void modifier(NoteInteretSujet noteInteretSujet).
	 */
	@Test
	public void test3_1Modifier() {
		/* Modification du bean dans la BDD */
		NoteInteretSujet noteInteretSujet = getNoteInteretSujet();
		NoteInteretSujet noteInteretSujetM = getNoteInteretSujet();
		noteInteretSujetM.setNote(INTEGER_TEST_MODIFIE);

		daoTesting.testModifierCRUD(noteInteretSujet, noteInteretSujetM, noteInteretSujetM);
	}

	/**
	 * Teste la méthode public List<NoteInteretSujet> lister().
	 */
	@Test
	public void test4_1Lister() throws IllegalAccessException {
		NoteInteretSujet noteInteretSujet = getNoteInteretSujet();

		daoTesting.testListerCRUD(new NoteInteretSujet[]{noteInteretSujet}, new NoteInteretSujet[]{noteInteretSujet});
	}

	/**
	 * Teste la méthode public List<NoteInteretSujet> lister().
	 * <p>Cas d'une erreur lors de la création de la connexion.</p>
	 * @throws SQLException
	 */
	@Test
	public void test4_2ListerLog() throws SQLException {
		/* Configuraton du logger */
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		Layout layout = new SimpleLayout();
		Appender appender = new WriterAppender(layout, out);
		logger.addAppender(appender);

		try {
			/* Mock de la méthode creerConnexion() */
			NoteInteretSujetDAO noteInteretSujetDAOMock = EasyMock.createMockBuilder(NoteInteretSujetDAO.class)
					.addMockedMethod("creerConnexion").createMock();
			expect(noteInteretSujetDAOMock.creerConnexion()).andThrow(new SQLException());

			replay(noteInteretSujetDAOMock);

			/* Recherche du bean dans la BDD */
			noteInteretSujetDAOMock.lister();

			/* Vérification du logging */
			String messageLog = out.toString();
			assertTrue("Mauvais logging", messageLog.contains("Échec du listage"));

			verify(noteInteretSujetDAOMock);
		} finally {
			/* Configuraton initiale du logger */
			logger.removeAppender(appender);
		}
	}

	/**
	 * Teste la méthode public void supprimer(NoteInteretSujet noteInteretSujet).
	 */
	@Test
	public void test5_1Supprimer() {
		NoteInteretSujet noteInteretSujet = getNoteInteretSujet();

		daoTesting.testSupprimerCRUD(noteInteretSujet);
	}

	/**
	 * Teste la méthode public void supprimer(NoteInteretSujet noteInteretSujet).
	 * <p>Cas de la suppression d'un bean non présent dans la BDD.</p>
	 */
	@Test
	public void test5_2SupprimerLog() {
		/* Configuraton du logger */
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		Layout layout = new SimpleLayout();
		Appender appender = new WriterAppender(layout, out);
		logger.addAppender(appender);

		try {
			/* Suppression du bean dans la BDD */
			noteInteretSujetDAO.supprimer((NoteInteretSujet)getNoteInteretSujet());

			/* Vérification du logging */
			String messageLog = out.toString();
			assertTrue("Mauvais logging", messageLog.contains("Échec de la suppression"));
		} finally {
			/* Configuraton initiale du logger */
			logger.removeAppender(appender);
		}
	}

}