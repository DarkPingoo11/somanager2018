package fr.eseo.ld.dao;

import fr.eseo.ld.beans.*;
import fr.eseo.testing.DAOTesting;
import org.apache.log4j.*;
import org.easymock.EasyMock;
import org.easymock.EasyMockRunner;
import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

import java.io.ByteArrayOutputStream;
import java.sql.SQLException;
import java.util.List;

import static org.easymock.EasyMock.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Classe de tests d'intégration JUnit 4 de la classe EquipeDAO.
 *
 * <p>Utilisation du modèle DAO.</p>
 *
 * @version 1.0
 * @author Maxime LENORMAND
 *
 * @see fr.eseo.ld.dao.EquipeDAO
 * @see org.junit
 */
@RunWith(EasyMockRunner.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class EquipeDAOTest {
	
	private static final Long LONG_TEST = 1L;
	private static final Integer INTEGER_TEST = 1;
	private static final Integer INTEGER_TEST_MODIFIE = 5;
	private static final Long LONG_TEST_ETUDIANT_EQUIPE = 1L;
	
	private static Logger logger = Logger.getLogger(EquipeDAO.class.getName());
	private static DAOTesting<Equipe> daoTesting;
	private static EquipeDAO equipeDAO;
	/**
	 * Récupère une instance de DAO et initialise le bean correspondant.
	 * 
	 * @throws Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		daoTesting = new DAOTesting<>();
		daoTesting.initDao(daoTesting.getDaoFactory().getEquipeDAO());
		equipeDAO = daoTesting.getDaoFactory().getEquipeDAO();
		daoTesting.addAlias(Meeting.class, "Reunion");
	}

	@AfterClass
	public static void tearDown() throws Exception {
		daoTesting.truncateUsedTables();
	}

	@Before
	public void beforeTest() throws SQLException, IllegalAccessException {
		daoTesting.truncateUsedTables();

		//Ajout des objets a la base de données
		daoTesting.createObjectInDatabase(daoTesting.getSujet());
		daoTesting.createObjectInDatabase(daoTesting.getOptionESEO());
		daoTesting.createObjectInDatabase(daoTesting.getAnneeScolaire());
		daoTesting.createObjectInDatabase(daoTesting.getUtilisateur());
		daoTesting.createObjectInDatabase(daoTesting.getEtudiant());
	}

	private void creerDependancesEtudiantEquipe() throws IllegalAccessException {
		Equipe equipe = daoTesting.getEquipe();
		EtudiantEquipe ee = daoTesting.getEtudiantEquipe();
		daoTesting.createObjectInDatabase(equipe);
		daoTesting.createObjectInDatabase(ee);
	}

	/**
	 * Teste la méthode public void creer(Equipe equipe).
	 */
	@Test
	public void test1_1Creer() {
		/* Insertion du bean dans la BDD */
		Equipe equipe = daoTesting.getEquipe();

		daoTesting.testCreerCRUD(equipe, equipe);
	}
	
	/**
	 * Teste la méthode public void creer(Equipe equipe).
	 * <p>Cas d'une erreur lors de la création de la connexion.</p>
	 * @throws SQLException 
	 */
	@Test
	public void test1_2CreerLog() throws SQLException {
		/* Configuraton du logger */
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		Layout layout = new SimpleLayout();
		Appender appender = new WriterAppender(layout, out);
		logger.addAppender(appender);
		
		try {
			/* Mock de la méthode creerConnexion() */
			EquipeDAO equipeDAOMock = EasyMock.createMockBuilder(EquipeDAO.class).addMockedMethod("creerConnexion")
					.createMock();
			expect(equipeDAOMock.creerConnexion()).andThrow(new SQLException());
			
			replay(equipeDAOMock);
			
			/* Insertion du bean dans la BDD */
			equipeDAOMock.creer(daoTesting.getEquipe());
			
			/* Vérification du logging */
			String messageLog = out.toString();
			assertTrue("Mauvais logging", messageLog.contains("Échec de la création de l'objet"));
			
			verify(equipeDAOMock);
		} finally {
			/* Configuraton initiale du logger */
			logger.removeAppender(appender);
		}
	}
	
	/**
	 * Teste la méthode public void attribuerEtudiantEquipe(EtudiantEquipe etudiantEquipe).
	 */
	@Test
	public void test1_2AttribuerEtudiantEquipe() throws IllegalAccessException {
		/* Insertion du bean dans la BDD */
		creerDependancesEtudiantEquipe();
		
		/* Vérification des attributs insérés */
		final List<EtudiantEquipe> etudiantEquipes = equipeDAO.trouverEtudiantEquipe(daoTesting.getEtudiantEquipe());
		final Long resultatTrouve = etudiantEquipes.get(0).getIdEtudiant();
		assertEquals("Mauvais idEtudiant", LONG_TEST_ETUDIANT_EQUIPE, resultatTrouve);
	}
	
	/**
	 * Teste la méthode public void attribuerEtudiantEquipe(EtudiantEquipe etudiantEquipe).
	 * <p>Cas de l'insertion d'un bean déjà présent dans la BDD.</p>
	 */
	@Test
	public void test1_3AttribuerEtudiantEquipeLog() {
		/* Configuraton du logger */
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		Layout layout = new SimpleLayout();
		Appender appender = new WriterAppender(layout, out);
		logger.addAppender(appender);
		
		try {
			/* Insertion du bean dans la BDD */
			equipeDAO.attribuerEtudiantEquipe(daoTesting.getEtudiantEquipe());

			/* Vérification du logging */
			String messageLog = out.toString();
			assertTrue("Mauvais logging", messageLog.contains("Échec de la création"));
		} finally {
			/* Configuraton initiale du logger */
			logger.removeAppender(appender);
		}
	}
	
	/**
	 * Teste la méthode public List<Equipe> trouver(Equipe equipe).
	 */
	@Test
	public void test2_1Trouver() {
		/* Recherche du bean dans la BDD */
		Equipe equipe = daoTesting.getEquipe();

		daoTesting.testTrouverCRUD(equipe, equipe);
	}
	
	/**
	 * Teste la méthode public List<Equipe> trouver(Equipe equipe).
	 * <p>Cas de la recherche d'un bean sans attributs.</p>
	 */
	@Test
	public void test2_2TrouverLog() {
		/* Configuraton du logger */
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		Layout layout = new SimpleLayout();
		Appender appender = new WriterAppender(layout, out);
		logger.addAppender(appender);
		
		try {
			/* Recherche du bean sans attributs dans la BDD */
			Equipe equipe = new Equipe();
			equipeDAO.trouver(equipe);

			/* Vérification du logging */
			String messageLog = out.toString();
			assertTrue("Mauvais logging", messageLog.contains("Échec de la recherche"));
		} finally {
			/* Configuraton initiale du logger */
			logger.removeAppender(appender);
		}
	}


	/**
	 * Teste la méthode public List<EtudiantEquipe> trouverEtudiantEquipe(EtudiantEquipe etudiantEquipe).
	 */
	@Test
	public void test2_3TrouverEtudiantEquipe() throws IllegalAccessException {
		creerDependancesEtudiantEquipe();

		/* Recherche du bean dans la BDD */
		final List<EtudiantEquipe> etudiantEquipes = equipeDAO.trouverEtudiantEquipe(daoTesting.getEtudiantEquipe());
		
		/* Vérification des attributs trouvés */
		final Long resultatTrouve = etudiantEquipes.get(0).getIdEtudiant();
		assertEquals("Mauvais idEtudiant", LONG_TEST_ETUDIANT_EQUIPE, resultatTrouve);
	}
	
	/**
	 * Teste la méthode public List<EtudiantEquipe> trouverEtudiantEquipe(EtudiantEquipe etudiantEquipe).
	 * <p>Cas de la recherche d'un bean sans attributs.</p>
	 */
	@Test
	public void test2_4TrouverEtudiantEquipeLog() throws IllegalAccessException {
		creerDependancesEtudiantEquipe();

		/* Configuraton du logger */
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		Layout layout = new SimpleLayout();
		Appender appender = new WriterAppender(layout, out);
		logger.addAppender(appender);
		
		try {
			/* Recherche du bean sans attributs dans la BDD */
			EtudiantEquipe etudiantEquipe = new EtudiantEquipe();
			equipeDAO.trouverEtudiantEquipe(etudiantEquipe);

			/* Vérification du logging */
			String messageLog = out.toString();
			assertTrue("Mauvais logging", messageLog.contains("Échec de la recherche de l'etudiantEquipe."));
		} finally {
			/* Configuraton initiale du logger */
			logger.removeAppender(appender);
		}
	}
	
	/**
	 * Teste la méthode public Equipe trouver(Long idEtudiant).
	 */
	@Test
	public void test2_5TrouverIDEtudiant() throws IllegalAccessException {
		creerDependancesEtudiantEquipe();

		/* Recherche du bean dans la BDD */
		final Equipe equipe = equipeDAO.trouver(LONG_TEST);
		
		/* Vérification des attributs trouvés */
		final Integer resultatTrouve = equipe.getTaille();
		assertEquals("Mauvaise taille", daoTesting.getEquipe().getTaille(), resultatTrouve);
	}

	/**
	 * Teste la méthode public void modifier(Equipe equipe).
	 */
	@Test
	public void test3_1Modifier() {
		/* Modification du bean dans la BDD */
		Equipe equipe = daoTesting.getEquipe();
		Equipe equipeMod = daoTesting.getEquipe();
		equipeMod.setTaille(INTEGER_TEST_MODIFIE);

		daoTesting.testModifierCRUD(equipe, equipeMod, equipeMod);
	}
	
	/**
	 * Teste la méthode public List<Equipe> lister().
	 */
	@Test
	public void test4_1Lister() throws IllegalAccessException {
		Equipe equipe = daoTesting.getEquipe();

		daoTesting.testListerCRUD(new Equipe[]{equipe}, new Equipe[]{equipe});
	}
	
	/**
	 * Teste la méthode public void List<Equipe> lister().
	 * <p>Cas d'une erreur lors de la création de la connexion.</p>
	 * @throws SQLException 
	 */
	@Test
	public void test4_2ListerLog() throws SQLException {
		/* Configuraton du logger */
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		Layout layout = new SimpleLayout();
		Appender appender = new WriterAppender(layout, out);
		logger.addAppender(appender);
		
		try {
			/* Mock de la méthode creerConnexion() */
			EquipeDAO equipeDAOMock = EasyMock.createMockBuilder(EquipeDAO.class).addMockedMethod("creerConnexion")
					.createMock();
			expect(equipeDAOMock.creerConnexion()).andThrow(new SQLException());
			
			replay(equipeDAOMock);
			
			/* Recherche du bean dans la BDD */
			equipeDAOMock.lister();
			
			/* Vérification du logging */
			String messageLog = out.toString();
			assertTrue("Mauvais logging", messageLog.contains("Échec du listage des objets."));
			
			verify(equipeDAOMock);
		} finally {
			/* Configuraton initiale du logger */
			logger.removeAppender(appender);
		}
	}
	
	/**
	 * Teste la méthode public List<EtudiantEquipe> listerEtudiantEquipe().
	 */
	@Test
	public void test4_3ListerEtudiantEquipe() throws IllegalAccessException {
		creerDependancesEtudiantEquipe();
		/* Recherche dans la BDD */
		final List<EtudiantEquipe> etudiantEquipes = equipeDAO.listerEtudiantEquipe();
		
		/* Recherche dans la liste */
		boolean estPresent = false;
		int i = 0;
		while (!estPresent && i < etudiantEquipes.size()) {
			if (LONG_TEST_ETUDIANT_EQUIPE.equals(etudiantEquipes.get(i).getIdEtudiant())) {
				estPresent = true;
			}
			i++;
		}
		assertTrue("Mauvais listage", estPresent);
	}
	
	/**
	 * Teste la méthode public void List<EtudiantEquipe> listerEtudiantEquipe().
	 * <p>Cas d'une erreur lors de la création de la connexion.</p>
	 * @throws SQLException 
	 */
	@Test
	public void test4_4ListerEtudiantEquipeLog() throws SQLException, IllegalAccessException {
		creerDependancesEtudiantEquipe();

		/* Configuraton du logger */
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		Layout layout = new SimpleLayout();
		Appender appender = new WriterAppender(layout, out);
		logger.addAppender(appender);
		
		try {
			/* Mock de la méthode creerConnexion() */
			EquipeDAO equipeDAOMock = EasyMock.createMockBuilder(EquipeDAO.class).addMockedMethod("creerConnexion")
					.createMock();
			expect(equipeDAOMock.creerConnexion()).andThrow(new SQLException());
			
			replay(equipeDAOMock);
			
			/* Recherche du bean dans la BDD */
			equipeDAOMock.listerEtudiantEquipe();
			
			/* Vérification du logging */
			String messageLog = out.toString();
			assertTrue("Mauvais logging", messageLog.contains("Échec du listage des etudiantEquipe."));
			
			verify(equipeDAOMock);
		} finally {
			/* Configuraton initiale du logger */
			logger.removeAppender(appender);
		}
	}
	
	/**
	 * Teste la méthode public List<Equipe> listerEquipeSujet(Long idSujet).
	 */
	@Test
	public void test4_5ListerEquipeSujet() throws IllegalAccessException {
		/* Recherche dans la BDD */
		creerDependancesEtudiantEquipe();

		final List<Equipe> equipes = equipeDAO.listerEquipeSujet(LONG_TEST);
		Equipe equipe = daoTesting.getEquipe();

		/* Recherche dans la liste */
		boolean estPresent = false;
		int i = 0;
		while (!estPresent && i < equipes.size()) {
			if (equipe.getIdEquipe().equals(equipes.get(i).getIdEquipe())) {
				estPresent = true;
			}
			i++;
		}
		assertTrue("Mauvais listage", estPresent);
	}
	
	/**
	 * Teste la méthode public void List<Equipe> listerEquipeSujet(Long idSujet).
	 * <p>Cas d'une erreur lors de la création de la connexion.</p>
	 * @throws SQLException 
	 */
	@Test
	public void test4_6ListerEquipeSujetLog() throws SQLException {
		/* Configuraton du logger */
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		Layout layout = new SimpleLayout();
		Appender appender = new WriterAppender(layout, out);
		logger.addAppender(appender);
		
		try {
			/* Mock de la méthode creerConnexion() */
			EquipeDAO equipeDAOMock = EasyMock.createMockBuilder(EquipeDAO.class).addMockedMethod("creerConnexion")
					.createMock();
			expect(equipeDAOMock.creerConnexion()).andThrow(new SQLException());
			
			replay(equipeDAOMock);
			
			/* Recherche du bean dans la BDD */
			equipeDAOMock.listerEquipeSujet(LONG_TEST);
			
			/* Vérification du logging */
			String messageLog = out.toString();
			assertTrue("Mauvais logging", messageLog.contains("Échec du listage des equipes qui traitent un sujet donné."));
			
			verify(equipeDAOMock);
		} finally {
			/* Configuraton initiale du logger */
			logger.removeAppender(appender);
		}
	}
	
	/**
	 * Teste la méthode public void supprimerEtudiantEquipe(EtudiantEquipe etudiantEquipe).
	 */
	@Test
	public void test5_1SupprimerEtudiantEquipe() throws IllegalAccessException {
		creerDependancesEtudiantEquipe();
		/* Suppression du bean dans la BDD */
		equipeDAO.supprimerEtudiantEquipe(daoTesting.getEtudiantEquipe());

		/* Vérification de la suppression */
		final List<EtudiantEquipe> etudiantEquipes = equipeDAO.trouverEtudiantEquipe(daoTesting.getEtudiantEquipe());
		assertTrue("Le bean n'a pas été supprimé", etudiantEquipes.isEmpty());
	}
	
	/**
	 * Teste la méthode public void supprimerEtudiantEquipe(EtudiantEquipe etudiantEquipe).
	 * <p>Cas de la suppression d'un bean non présent dans la BDD.</p>
	 */
	@Test
	public void test5_2SupprimerEtudiantEquipeLog() throws IllegalAccessException {
		creerDependancesEtudiantEquipe();
		/* Configuraton du logger */
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		Layout layout = new SimpleLayout();
		Appender appender = new WriterAppender(layout, out);
		logger.addAppender(appender);

		try {
			/* Suppression du bean dans la BDD */
			equipeDAO.supprimerEtudiantEquipe(new EtudiantEquipe());

			/* Vérification du logging */
			String messageLog = out.toString();

			assertTrue("Mauvais logging", messageLog.contains("Échec de la suppression"));
		} finally {
			/* Configuraton initiale du logger */
			logger.removeAppender(appender);
		}
	}
	
	/**
	 * Teste la méthode public void supprimer(Equipe equipe).
	 */
	@Test
	public void test5_3Supprimer() {
		/* Insertion du bean dans la BDD */
		Equipe equipe = daoTesting.getEquipe();

		daoTesting.testSupprimerCRUD(equipe);
	}
	
	/**
	 * Teste la méthode public void supprimer(Equipe equipe).
	 * <p>Cas de la suppression d'un bean non présent dans la BDD.</p>
	 */
	@Test
	public void test5_4SupprimerLog() {
		/* Configuraton du logger */
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		Layout layout = new SimpleLayout();
		Appender appender = new WriterAppender(layout, out);
		logger.addAppender(appender);

		try {
			/* Suppression du bean dans la BDD */
			equipeDAO.supprimer(daoTesting.getEquipe());

			/* Vérification du logging */
			String messageLog = out.toString();
			assertTrue("Mauvais logging", messageLog.contains("Échec de la suppression"));
		} finally {
			/* Configuraton initiale du logger */
			logger.removeAppender(appender);
		}
	}
	
	/**
	 * Teste la méthode public void modifier(Equipe equipe).
	 * <p>Cas de la modification d'un bean non présent dans la BDD.</p>
	 */
	@Test
	public void test6_1ModifierLog() {
		/* Configuraton du logger */
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		Layout layout = new SimpleLayout();
		Appender appender = new WriterAppender(layout, out);
		logger.addAppender(appender);
		
		try {
			/* Modification du bean dans la BDD */
			Equipe equipe = daoTesting.getEquipe();

			equipe.setIdEquipe(null);
			equipeDAO.modifier(equipe);

			/* Vérification du logging */
			String messageLog = out.toString();
			assertTrue("Mauvais logging", messageLog.contains("Échec de la mise à jour"));
		} finally {
			/* Configuraton initiale du logger */
			logger.removeAppender(appender);
		}
	}

	@Test
	public void testLogging() {
		daoTesting.testLoggingCRUD();
	}

}