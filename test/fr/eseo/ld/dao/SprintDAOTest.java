package fr.eseo.ld.dao;

import fr.eseo.ld.beans.AnneeScolaire;
import fr.eseo.ld.beans.OptionESEO;
import fr.eseo.ld.pgl.beans.Sprint;
import fr.eseo.testing.DAOTesting;
import org.apache.log4j.*;
import org.easymock.EasyMock;
import org.easymock.EasyMockRunner;
import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

import java.io.ByteArrayOutputStream;
import java.sql.SQLException;
import java.text.ParseException;

import static junit.framework.TestCase.assertTrue;
import static org.easymock.EasyMock.*;

/**
 * Classe de tests unitaires JUnit 4 de la classe SprintDAO.
 *
 * <p>Utilisation du modèle DAO.</p>
 *
 * @author Dimitri J
 * @version 1.0
 * @see fr.eseo.ld.dao.SprintDAO
 * @see org.junit
 */
@RunWith(EasyMockRunner.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class SprintDAOTest {


    private static DAOTesting<Sprint> daoTesting;

    private static final String DATE_STR = "2018-03-04";
    private static final String DATE_STR2 = "2018-03-30";

    /* Attributs de l'entité Sprint */
    private static final Long LONG_TEST = 1L;
    private static final Integer NBR_HEURES = 5;
    private static final Long REF_ANNEE = 1L;
    private static final Float COEFFICIENT = 0.F;

    private static Logger logger = Logger.getLogger(SprintDAO.class.getName());

    private static SprintDAO sprintDAO;

    @BeforeClass
    public static void setUp() throws Exception {
        //Définition du dao factory
        daoTesting = new DAOTesting<>();
        daoTesting.initDao(daoTesting.getDaoFactory().getSprintDAO());
        daoTesting.addAlias(Sprint.class, "pgl_sprint");

        /*Tests pour les logs*/
        sprintDAO = daoTesting.getDaoFactory().getSprintDAO();

        daoTesting.truncateTable(OptionESEO.class);
        daoTesting.truncateTable(AnneeScolaire.class);
    }

    @AfterClass
    public static void tearDown() throws Exception {
        daoTesting.truncateUsedTables();
    }

    @Before
    public void beforeTest() throws SQLException, IllegalAccessException {
        daoTesting.truncateUsedTables();

        OptionESEO option = daoTesting.getOptionESEO();
        AnneeScolaire annee = daoTesting.getAnneeScolaire();
        option.setNomOption("LD");

        daoTesting.createObjectInDatabase(option);
        daoTesting.createObjectInDatabase(annee);
    }


    @Test
    public void creer() throws IllegalAccessException {
        Sprint sprint = daoTesting.getSprint();
        daoTesting.testCreerCRUD(sprint, sprint);
    }

    @Test
    public void trouver() {
        Sprint sprint = daoTesting.getSprint();
        daoTesting.testTrouverCRUD(sprint, sprint);
    }


    @Test
    public void modifier() {
        Sprint sprint = daoTesting.getSprint();

        Sprint sprintModifier = daoTesting.getSprint();
        sprintModifier.setDateDebut("2018-10-10");

        Sprint sprintAttendu = daoTesting.getSprint();
        sprintAttendu.setDateDebut("2018-10-10");

        daoTesting.testModifierCRUD(sprint, sprintModifier, sprintAttendu);
    }


    @Test
    public void supprimer() {
        Sprint sprint = daoTesting.getSprint();

        daoTesting.testSupprimerCRUD(sprint);
    }

    @Test
    public void lister() throws IllegalAccessException, ParseException {
        Sprint[] sprints = new Sprint[2];
        Sprint[] sprintsAttendu = new Sprint[2];

        for (int i = 0; i < sprints.length; i++) {

            sprints[i] = daoTesting.getSprint();
            sprints[i].setIdSprint(LONG_TEST + i);
            sprints[i].setDateDebut(DATE_STR);
            sprints[i].setDateFin(DATE_STR);
            sprints[i].setNbrHeures(NBR_HEURES);
            sprints[i].setRefAnnee(REF_ANNEE);
            sprints[i].setCoefficient(COEFFICIENT);

            sprintsAttendu[i] = daoTesting.getSprint();
            sprintsAttendu[i].setIdSprint(LONG_TEST + i);
            sprintsAttendu[i].setDateDebut(DATE_STR);
            sprintsAttendu[i].setDateFin(DATE_STR);
            sprintsAttendu[i].setNbrHeures(NBR_HEURES);
            sprintsAttendu[i].setRefAnnee(REF_ANNEE);
            sprintsAttendu[i].setCoefficient(COEFFICIENT);
        }
        daoTesting.testListerCRUD(sprints, sprintsAttendu);
    }

    /**
     * Tests sur les logs
     */


    /**
     * Teste la méthode public void creer(Sprint sprint).
     * <p>Cas d'une erreur lors de la création de la connexion.</p>
     *
     * @throws SQLException
     */

    @Test
    public void test1_2CreerLog() throws SQLException {
        /* Configuraton du logger */
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        Layout layout = new SimpleLayout();
        Appender appender = new WriterAppender(layout, out);
        logger.addAppender(appender);

        try {
            /* Mock de la méthode creerConnexion() */
            SprintDAO sprintDAOMock = EasyMock.createMockBuilder(SprintDAO.class).addMockedMethod("creerConnexion")
                    .createMock();
            expect(sprintDAOMock.creerConnexion()).andThrow(new SQLException());

            replay(sprintDAOMock);

            /* Insertion du bean dans la BDD */
            sprintDAOMock.creer(daoTesting.getSprint());

            /* Vérification du logging */
            String messageLog = out.toString();
            assertTrue("Mauvais logging", messageLog.contains("Échec de la création de l'objet"));

            verify(sprintDAOMock);
        } finally {
            /* Configuraton initiale du logger */
            logger.removeAppender(appender);
        }
    }

    /**
     * Teste la méthode public List<Sprint> trouver(Sprint sprint).
     * <p>Cas de la recherche d'un bean sans attributs.</p>
     */
    @Test
    public void test2_2TrouverLog() {
        /* Configuraton du logger */
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        Layout layout = new SimpleLayout();
        Appender appender = new WriterAppender(layout, out);
        logger.addAppender(appender);

        try {
            /* Recherche du bean sans attributs dans la BDD */
            Sprint sprint = new Sprint();

            sprintDAO.trouver(sprint);

            /* Vérification du logging */
            String messageLog = out.toString();
            assertTrue("Mauvais logging", messageLog.contains("Échec de la recherche de l'objet Sprint."));
        } finally {
            /* Configuraton initiale du logger */
            logger.removeAppender(appender);
        }
    }

    /**
     * Teste la méthode public List<Sprint> trouver(Long idSprint).
     * <p>Cas de la recherche d'un bean sans attributs.</p>
     */
    @Test
    public void test2_2_2TrouverIdSprintLog() {
        /* Configuraton du logger */
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        Layout layout = new SimpleLayout();
        Appender appender = new WriterAppender(layout, out);
        logger.addAppender(appender);

        try {
            /* Recherche du bean sans attributs dans la BDD */
            Long idSprint = null;
            sprintDAO.trouver(idSprint);

            /* Vérification du logging */
            String messageLog = out.toString();
            assertTrue("Mauvais logging", messageLog.contains("Échec de la recherche d'un sprint à partir de son ID."));
        } finally {
            /* Configuraton initiale du logger */
            logger.removeAppender(appender);
        }
    }

    /**
     * Teste la méthode public List<Sprint> lister().
     * <p>Cas d'une erreur lors de la création de la connexion.</p>
     *
     * @throws SQLException
     */
    @Test
    public void test4_2ListerLog() throws SQLException {
        /* Configuraton du logger */
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        Layout layout = new SimpleLayout();
        Appender appender = new WriterAppender(layout, out);
        logger.addAppender(appender);

        try {
            /* Mock de la méthode creerConnexion() */
            SprintDAO sprintDAOMock = EasyMock.createMockBuilder(SprintDAO.class).addMockedMethod("creerConnexion")
                    .createMock();
            expect(sprintDAOMock.creerConnexion()).andThrow(new SQLException());

            replay(sprintDAOMock);

            /* Recherche du bean dans la BDD */
            sprintDAOMock.lister();

            /* Vérification du logging */
            String messageLog = out.toString();
            assertTrue("Mauvais logging", messageLog.contains("Échec du listage"));

            verify(sprintDAOMock);
        } finally {
            /* Configuraton initiale du logger */
            logger.removeAppender(appender);
        }
    }



}
