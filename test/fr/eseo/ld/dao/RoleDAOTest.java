package fr.eseo.ld.dao;

import fr.eseo.ld.beans.OptionESEO;
import fr.eseo.ld.beans.Role;
import fr.eseo.ld.beans.RoleUtilisateur;
import fr.eseo.ld.beans.Utilisateur;
import fr.eseo.testing.DAOTesting;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.sql.SQLException;
import java.util.List;

import static junit.framework.TestCase.assertEquals;

/**
 * Classe de test de RoleDAO
 *
 * @author Tristan LE GACQUE
 * @see RoleDAO
 **/
public class RoleDAOTest {

    private static DAOTesting<Role> daoTesting;

    /**
     * Définition du DaoTesting
     *
     * @throws Exception exception
     */
    @BeforeClass
    public static void setUp() throws Exception {
        //Définition du dao factory
        daoTesting = new DAOTesting<>();
        daoTesting.initDao(daoTesting.getDaoFactory().getRoleDAO());
    }

    @AfterClass
    public static void tearDown() throws Exception {
        daoTesting.truncateUsedTables();
    }

    @Before
    public void beforeTest() throws SQLException, IllegalAccessException {
        daoTesting.truncateUsedTables();

    }


    /**
     * Teste la méthode public void creer.
     */
    @Test
    public void testCreer() {
        /* Insertion du bean dans la BDD */
        Role role = daoTesting.getRole();
        Role roleAttendu = daoTesting.getRole();

        daoTesting.testCreerCRUD(role, roleAttendu);
    }

    /**
     * Teste la méthode public trouver
     */
    @Test
    public void testTrouver() {
        /* Recherche du bean dans la BDD */
        Role role = daoTesting.getRole();
        Role roleAttendu = daoTesting.getRole();

        daoTesting.testTrouverCRUD(role, roleAttendu);
    }


    /**
     * Teste la méthode public void modifier
     */
    @Test
    public void testModifier() {
        Role role = daoTesting.getRole();
        Role roleMod = daoTesting.getRole();

        roleMod.setNomRole("Role2");

        daoTesting.testModifierCRUD(role, roleMod, roleMod);
    }

    /**
     * Teste la méthode public lister().
     * Regarder ModeleExcelDao pour un exemple plus complet
     *
     * @see ModeleExcelDAOTest
     */
    @Test
    public void testLister() {
        Role role = daoTesting.getRole();
        Role roleAttendu = daoTesting.getRole();

        daoTesting.testListerCRUD(new Role[]{role}, new Role[]{roleAttendu});
    }

    /**
     * Teste la méthode public void supprimer(NoteInteretTechno NoteInteretTechno).
     */
    @Test
    public void testSupprimer() {
        Role role = daoTesting.getRole();

        daoTesting.testSupprimerCRUD(role);
    }

    @Test
    public void listerRolesApplicatifs() throws IllegalAccessException {
        //Définition des dépendances
        Role[] roles = new Role[12];

        for(int i = 0; i < roles.length; i++) {
            Role role = daoTesting.getRole();
            role.setIdRole(i + 1L);
            role.setNomRole("Role"+i);

            roles[i] = role;
            daoTesting.createObjectInDatabase(role);
        }

        //Test
        final List<Role> rolesT = daoTesting.getDaoFactory().getRoleDAO().listerRolesApplicatifs();
        assertEquals("La taille n'est pas bonne", roles.length, rolesT.size());
        for(int i = 0; i < rolesT.size(); i++) {
            daoTesting.compareObjects(roles[i], rolesT.get(i));
        }
    }

    @Test
    public void listerRoleUtilisateur() throws IllegalAccessException {
        //Création des objets de dépendance
        OptionESEO option = daoTesting.getOptionESEO();
        Role role = daoTesting.getRole();
        Utilisateur user = daoTesting.getUtilisateur();
        RoleUtilisateur roleUtilisateur = daoTesting.getRoleUtilisateur();

        daoTesting.createObjectInDatabase(option);
        daoTesting.createObjectInDatabase(role);
        daoTesting.createObjectInDatabase(user);
        daoTesting.createObjectInDatabase(roleUtilisateur);

        final List<RoleUtilisateur> roleUtilisateurs = daoTesting.getDaoFactory().getRoleDAO().listerRoleUtilisateur(role);
        assertEquals("Il n'y a pas qu'un seul résultat", 1, roleUtilisateurs.size());
        daoTesting.compareObjects(roleUtilisateur, roleUtilisateurs.get(0));
    }
}