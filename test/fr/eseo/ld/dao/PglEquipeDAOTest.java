package fr.eseo.ld.dao;

import fr.eseo.ld.beans.AnneeScolaire;
import fr.eseo.ld.beans.OptionESEO;
import fr.eseo.ld.beans.Utilisateur;
import fr.eseo.ld.pgl.beans.Equipe;
import fr.eseo.ld.pgl.beans.Etudiant;
import fr.eseo.ld.pgl.beans.Sprint;
import fr.eseo.testing.DAOTesting;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.sql.SQLException;

/**
 * Classe de test de PglEquipeDAO
 *
 * @author Tristan LE GACQUE
 * @see PglEquipeDAO
 **/
public class PglEquipeDAOTest {

    private static DAOTesting<Equipe> daoTesting;

    /**
     * Définition du DaoTesting
     *
     * @throws Exception exception
     */
    @BeforeClass
    public static void setUp() throws Exception {
        //Définition du dao factory
        daoTesting = new DAOTesting<>();
        daoTesting.initDao(daoTesting.getDaoFactory().getPglEquipeDAO());

        daoTesting.addAlias(Equipe.class, "pgl_equipe");
        daoTesting.addAlias(Sprint.class, "pgl_sprint");
        daoTesting.addAlias(Etudiant.class, "pgl_etudiant");

        daoTesting.truncateTable(OptionESEO.class);
        daoTesting.truncateTable(Utilisateur.class);
        daoTesting.truncateTable(AnneeScolaire.class);
        daoTesting.truncateTable(Equipe.class);
        daoTesting.truncateTable(Etudiant.class);
        daoTesting.truncateTable(Sprint.class);
    }

    @AfterClass
    public static void tearDown() throws Exception {
        daoTesting.truncateUsedTables();
    }

    @Before
    public void beforeTest() throws SQLException, IllegalAccessException {
        daoTesting.truncateUsedTables();

        //Définition des dépendances
        OptionESEO optionESEO = daoTesting.getOptionESEO();
        Utilisateur user = daoTesting.getUtilisateur();
        AnneeScolaire anneeScolaire =daoTesting.getAnneeScolaire();
        Sprint sprint = daoTesting.getSprint();
        Etudiant etudiant = daoTesting.getEtudiantPgl();


        //Ajout des objets a la base de données
        daoTesting.createObjectInDatabase(optionESEO);
        daoTesting.createObjectInDatabase(anneeScolaire);
        daoTesting.createObjectInDatabase(user);
        daoTesting.createObjectInDatabase(etudiant);
        daoTesting.createObjectInDatabase(sprint);
    }


    /**
     * Teste la méthode public void creer.
     *
     * @see PglEquipeDAO#creer(Equipe)
     */
    @Test
    public void testCreer() {
        /* Insertion du bean dans la BDD */
        Equipe pglequipe = daoTesting.getEquipePGL();
        Equipe pglequipeAttendu = daoTesting.getEquipePGL();
        pglequipeAttendu.setRefProjet(0L);

        daoTesting.testCreerCRUD(pglequipe, pglequipeAttendu);
    }

    /**
     * Teste la méthode public trouver
     *
     * @see PglEquipeDAO#trouver(Equipe)
     */
    @Test
    public void testTrouver() {
        /* Recherche du bean dans la BDD */
        Equipe pglequipe = daoTesting.getEquipePGL();
        Equipe pglequipeAttendu = daoTesting.getEquipePGL();
        pglequipeAttendu.setRefProjet(0L);

        daoTesting.testTrouverCRUD(pglequipe, pglequipeAttendu);
    }


    /**
     * Teste la méthode public void modifier
     *
     * @see PglEquipeDAO#modifier(Equipe)
     */
    @Test
    public void testModifier() {
        Equipe pglequipe = daoTesting.getEquipePGL();
        Equipe pglequipeMod = daoTesting.getEquipePGL();
        Equipe pglequipeModA = daoTesting.getEquipePGL();

        pglequipeMod.setNom("autreE");
        pglequipeModA.setNom("autreE");
        pglequipeModA.setRefProjet(0L);

        daoTesting.testModifierCRUD(pglequipe, pglequipeMod, pglequipeModA);
    }

    /**
     * Teste la méthode public lister().
     *
     * @see PglEquipeDAO#lister()
     */
    @Test
    public void testLister() {
        Equipe pglequipe = daoTesting.getEquipePGL();
        Equipe pglequipeAttendu = daoTesting.getEquipePGL();
        pglequipeAttendu.setRefProjet(0L);

        daoTesting.testListerCRUD(new Equipe[]{pglequipe}, new Equipe[]{pglequipeAttendu});
    }

    /**
     * Teste la méthode public void supprimer(NoteInteretTechno NoteInteretTechno).
     *
     * @see PglEquipeDAO#supprimer(Equipe)
     */
    @Test
    public void testSupprimer() {
        Equipe pglequipe = daoTesting.getEquipePGL();

        daoTesting.testSupprimerCRUD(pglequipe);
    }

    /**
     * Test les erreurs
     */
    @Test
    public void testLogging() {
        daoTesting.testLoggingCRUD();
    }
}