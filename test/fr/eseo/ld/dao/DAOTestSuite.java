package fr.eseo.ld.dao;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

/**
 * Suite de tests unitaires JUnit 4 pour le package fr.eseo.ld.dao.
 *
 * @author Maxime LENORMAND
 *
 * @see org.junit
 */
@RunWith(Suite.class)
@SuiteClasses({
		AnneeScolaireDAOTest.class,
        BonusMalusDAOTest.class,
        CommentaireDAOTest.class,
		CompositionJuryDAOTest.class,
        DAOUtilitaireTest.class,
		EquipeDAOTest.class,
		EtudiantDAOTest.class,
        InviteReunionDAOTest.class,
		JuryPosterDAOTest.class,
		JurySoutenanceDAOTest.class,
        MatiereDAOTest.class,
		MeetingDAOTest.class,
		ModeleExcelDAOTest.class,
		PglNoteDAOTest.class,
        NoteInteretSujetDAOTest.class,
        NoteInteretTechnoDAOTest.class,
        NotePosterDAOTest.class,
        NoteSoutenanceDAOTest.class,
        NotificationDAOTest.class,
		OptionESEODAOTest.class,
		ParametreDAOTest.class,
		PartageRoleDAOTest.class,
        PglEquipeDAOTest.class,
		PglEtudiantDAOTest.class,
		PglEtudiantEquipeDAOTest.class,
		PglRoleEquipeDAOTest.class,
        PglSoutenanceDAOTest.class,
		PglProjetDAOTest.class,
        PosterDAOTest.class,
        ProfesseurDAOTest.class,
        ProfesseurSujetDAOTest.class,
		RoleDAOTest.class,
		SoutenanceDAOTest.class,
		SprintDAOTest.class,
		SujetDAOTest.class,
		UtilisateurDAOTest.class
})
public class DAOTestSuite {
	/* Classe vide */
}