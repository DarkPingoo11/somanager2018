package fr.eseo.ld.dao;

import fr.eseo.ld.beans.Etudiant;
import fr.eseo.ld.beans.Utilisateur;
import fr.eseo.testing.DAOTesting;
import org.apache.log4j.*;
import org.easymock.EasyMock;
import org.easymock.EasyMockRunner;
import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

import java.io.ByteArrayOutputStream;
import java.sql.SQLException;

import static org.easymock.EasyMock.*;
import static org.junit.Assert.assertTrue;

/**
 * Classe de tests unitaires JUnit 4 de la classe EtudiantDAO.
 *
 * <p>Utilisation du modèle DAO.</p>
 *
 * @version 1.0
 * @author Maxime LENORMAND
 *
 * @see fr.eseo.ld.dao.EtudiantDAO
 * @see org.junit
 */
@RunWith(EasyMockRunner.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class EtudiantDAOTest {

	private static final Long LONG_TEST = 1L;
	private static final Float FLOAT_TEST_MODIFIE = 20f;
	
	private static Logger logger = Logger.getLogger(EtudiantDAO.class.getName());
	private static EtudiantDAO etudiantDAO;
	private static DAOTesting<Etudiant> daoTesting;

	@BeforeClass
	public static void setUp() throws Exception {
		//Définition du dao factory
		daoTesting = new DAOTesting<>();
		daoTesting.initDao(daoTesting.getDaoFactory().getEtudiantDao());
		etudiantDAO = daoTesting.getDaoFactory().getEtudiantDao();
	}

	@AfterClass
	public static void tearDown() throws Exception {
		daoTesting.truncateUsedTables();
	}

	@Before
	public void beforeTest() throws SQLException, IllegalAccessException {
		daoTesting.truncateUsedTables();
		Utilisateur u = new Utilisateur();
		u.setEmail("no@no.fr");
		u.setPrenom("PRenom");
		u.setNom("Nom");
		u.setValide("oui");
		u.setIdentifiant("testtesttest");
		u.setHash("hash");
		daoTesting.createObjectInDatabase(u);
	}

	private Etudiant getEtudiant() {
		Etudiant etudiant = new Etudiant();
		etudiant.setIdEtudiant(LONG_TEST);
		etudiant.setContratPro("non");
		etudiant.setAnnee(2017);
		etudiant.setNoteIntermediaire(-1f);
		etudiant.setNotePoster(-1f);
		etudiant.setNoteSoutenance(-1f);
		etudiant.setNoteProjet(-1f);
		return etudiant;
	}




	/**
	 * Teste la méthode public void creer(Etudiant etudiant).
	 */
	@Test
	public void test1_1Creer() {
		/* Insertion du bean dans la BDD */
		Etudiant etudiant = getEtudiant();

		daoTesting.testCreerCRUD(etudiant, etudiant);
	}
	
	/**
	 * Teste la méthode public List<Etudiant> trouver(Etudiant etudiant).
	 */
	@Test
	public void test2_1Trouver() {
		/* Recherche du bean dans la BDD */
		Etudiant etudiant = getEtudiant();

		daoTesting.testTrouverCRUD(etudiant, etudiant);
	}
	
	/**
	 * Teste la méthode public List<Etudiant> trouver(Etudiant etudiant).
	 * <p>Cas de la recherche d'un bean sans attributs.</p>
	 */
	@Test
	public void test2_2TrouverLog() {
		/* Configuraton du logger */
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		Layout layout = new SimpleLayout();
		Appender appender = new WriterAppender(layout, out);
		logger.addAppender(appender);
		
		try {
			/* Recherche du bean sans attributs dans la BDD */
			Etudiant etudiant = new Etudiant();
			etudiantDAO.trouver(etudiant);

			/* Vérification du logging */
			String messageLog = out.toString();
			assertTrue("Mauvais logging", messageLog.contains("Échec de la recherche"));
		} finally {
			/* Configuraton initiale du logger */
			logger.removeAppender(appender);
		}
	}
	
	/**
	 * Teste la méthode public void modifier(Etudiant etudiant).
	 */
	@Test
	public void test3_1Modifier() {
		/* Modification du bean dans la BDD */
		Etudiant etudiant = getEtudiant();
		Etudiant etudiantM = getEtudiant();
		etudiantM.setNoteProjet(FLOAT_TEST_MODIFIE);

		daoTesting.testModifierCRUD(etudiant, etudiantM, etudiantM);
	}
	
	/**
	 * Teste la méthode public List<Etudiant> lister().
	 */
	@Test
	public void test4_1Lister() throws IllegalAccessException {
		/* Recherche du bean dans la BDD */
		Etudiant etudiant = getEtudiant();

		daoTesting.testListerCRUD(new Etudiant[]{etudiant}, new Etudiant[]{etudiant});
	}
	
	/**
	 * Teste la méthode public List<Etudiant> lister().
	 * <p>Cas d'une erreur lors de la création de la connexion.</p>
	 * @throws SQLException 
	 */
	@Test
	public void test4_2ListerLog() throws SQLException {
		/* Configuraton du logger */
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		Layout layout = new SimpleLayout();
		Appender appender = new WriterAppender(layout, out);
		logger.addAppender(appender);
		
		try {
			/* Mock de la méthode creerConnexion() */
			EtudiantDAO etudiantDAOMock = EasyMock.createMockBuilder(EtudiantDAO.class).addMockedMethod("creerConnexion")
					.createMock();
			expect(etudiantDAOMock.creerConnexion()).andThrow(new SQLException());
			
			replay(etudiantDAOMock);
			
			/* Recherche du bean dans la BDD */
			etudiantDAOMock.lister();
			
			/* Vérification du logging */
			String messageLog = out.toString();
			assertTrue("Mauvais logging", messageLog.contains("Échec du listage"));
			
			verify(etudiantDAOMock);
		} finally {
			/* Configuraton initiale du logger */
			logger.removeAppender(appender);
		}
	}
	
	/**
	 * Teste la méthode public void supprimer(Etudiant etudiant).
	 */
	@Test
	public void test5_1Supprimer() {
		/* Suppression du bean dans la BDD */
		Etudiant etudiant = getEtudiant();

		daoTesting.testSupprimerCRUD(etudiant);
	}
	
	/**
	 * Teste la méthode public void supprimer(Etudiant etudiant).
	 * <p>Cas de la suppression d'un bean non présent dans la BDD.</p>
	 */
	@Test
	public void test5_2SupprimerLog() {
		/* Configuraton du logger */
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		Layout layout = new SimpleLayout();
		Appender appender = new WriterAppender(layout, out);
		logger.addAppender(appender);

		try {
			/* Suppression du bean dans la BDD */
			etudiantDAO.supprimer(getEtudiant());

			/* Vérification du logging */
			String messageLog = out.toString();
			assertTrue("Mauvais logging", messageLog.contains("Échec de la suppression"));
		} finally {
			/* Configuraton initiale du logger */
			logger.removeAppender(appender);
		}
	}
	
}