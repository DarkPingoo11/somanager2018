package fr.eseo.ld.dao;

import fr.eseo.ld.beans.AnneeScolaire;
import fr.eseo.ld.beans.OptionESEO;
import fr.eseo.ld.beans.Utilisateur;
import fr.eseo.ld.objets.exportexcel.ENJNotes;
import fr.eseo.ld.pgl.beans.*;
import fr.eseo.ld.utils.ParamUtilitaire;
import fr.eseo.testing.DAOTesting;
import org.junit.*;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertTrue;

/**
 * Classe de test de NoteDAO
 *
 * @author Tristan LE GACQUE
 * @see PglNoteDAO
 **/
public class PglNoteDAOTest {

    private static DAOTesting<Note> daoTesting;

    private int[] notes         = new int[]{12, 8, 14, 1, 2, 18, 13, 20};
    private int[] matieres      = new int[]{1,  1,  2, 4, 3,  1,  1, 5};
    private int[] users         = new int[]{2,  2,  2, 2, 3,  4,  4, 2};
    private int[] evals         = new int[]{5,  6,  7, 8, 9,  10,  11, 1};
    private static final int ANNEE_TEST = 2017;

    /**
     * Définition du DaoTesting
     *
     * @throws Exception exception
     */
    @BeforeClass
    public static void setUp() throws Exception {
        //Définition du dao factory
        daoTesting = new DAOTesting<>();
        daoTesting.initDao(daoTesting.getDaoFactory().getPglNoteDAO());

        daoTesting.addAlias(Equipe.class, "pgl_equipe");
        daoTesting.addAlias(Matiere.class, "pgl_matiere");
        daoTesting.addAlias(Sprint.class, "pgl_sprint");
        daoTesting.addAlias(BonusMalus.class, "pgl_bonusmalus");
        daoTesting.addAlias(Etudiant.class, "pgl_etudiant");
        daoTesting.addAlias(Note.class, "pgl_note");
        daoTesting.addAlias(EtudiantEquipe.class, "pgl_etudiantequipe");

        daoTesting.truncateTable(OptionESEO.class);
        daoTesting.truncateTable(Utilisateur.class);
        daoTesting.truncateTable(AnneeScolaire.class);
        daoTesting.truncateTable(Equipe.class);
        daoTesting.truncateTable(Etudiant.class);
        daoTesting.truncateTable(Matiere.class);
        daoTesting.truncateTable(Sprint.class);
        daoTesting.truncateTable(EtudiantEquipe.class);
    }

    @AfterClass
    public static void tearDown() throws Exception {
      daoTesting.truncateUsedTables();
    }

    @Before
    public void beforeTest() throws SQLException, IllegalAccessException {
        daoTesting.truncateUsedTables();

        //Définition des dépendances
        OptionESEO optionESEO = daoTesting.getOptionESEO();
        Utilisateur user = daoTesting.getUtilisateur();
        AnneeScolaire anneeScolaire =daoTesting.getAnneeScolaire();
        Equipe equipe = daoTesting.getEquipePGL();
        Matiere matiere = daoTesting.getMatiere();
        Sprint sprint = daoTesting.getSprint();
        Etudiant etudiant = daoTesting.getEtudiantPgl();


        //Ajout des objets a la base de données
        daoTesting.createObjectInDatabase(optionESEO);
        daoTesting.createObjectInDatabase(anneeScolaire);
        daoTesting.createObjectInDatabase(user);
        daoTesting.createObjectInDatabase(equipe);
        daoTesting.createObjectInDatabase(etudiant);
        daoTesting.createObjectInDatabase(sprint);
        //Supprimer les Matières crées par défaut
        daoTesting.truncateTable(Matiere.class);

        daoTesting.createObjectInDatabase(matiere);
    }


    /**
     * Teste la méthode public void creer.
     *
     * @see PglNoteDAO#creer(Note)
     */
    @Test
    public void testCreer() {
        /* Insertion du bean dans la BDD */
        Note noteequipe = daoTesting.getNote();
        Note noteequipeAttendu = daoTesting.getNote();

        daoTesting.testCreerCRUD(noteequipe, noteequipeAttendu);
    }

    /**
     * Teste la méthode public trouver
     *
     * @see PglNoteDAO#trouver(Note)
     */
    @Test
    public void testTrouver() {
        /* Recherche du bean dans la BDD */
        Note noteequipe = daoTesting.getNote();
        Note noteequipeAttendu = daoTesting.getNote();

        daoTesting.testTrouverCRUD(noteequipe, noteequipeAttendu);
    }


    /**
     * Teste la méthode public void modifier
     *
     * @see PglNoteDAO#modifier(Note)
     */
    @Test
    public void testModifier() {
        Note noteequipe = daoTesting.getNote();
        Note noteequipeMod = daoTesting.getNote();

        noteequipe.setNote(8F);

        daoTesting.testModifierCRUD(noteequipe, noteequipeMod, noteequipeMod);
    }

    /**
     * Teste la méthode public lister().
     *
     * @see PglNoteDAO#lister()
     */
    @Test
    public void testLister() {
        Note noteequipe = daoTesting.getNote();
        Note noteequipeAttendu = daoTesting.getNote();

        daoTesting.testListerCRUD(new Note[]{noteequipe}, new Note[]{noteequipeAttendu});
    }

    /**
     * Teste la méthode public void supprimer(NoteInteretTechno NoteInteretTechno).
     *
     * @see PglNoteDAO#supprimer(Note)
     */
    @Test
    public void testSupprimer() {
        Note noteequipe = daoTesting.getNote();

        daoTesting.testSupprimerCRUD(noteequipe);
    }

    /**
     * Test les erreurs
     */
    @Test
    public void testLogging() {
        daoTesting.testLoggingCRUD();
    }

    @Test
    public void trouver() throws IllegalAccessException {
        Note noteequipe = daoTesting.getNote();

        daoTesting.createObjectInDatabase(noteequipe);
        final Note trouver = daoTesting.getDaoFactory().getPglNoteDAO().trouver(noteequipe.getIdNote());

        assertNotNull("L'objet est null", trouver);
        daoTesting.compareObjects(noteequipe, trouver);
    }

    @Test
    public void recupererNotesMoyennesPglUtilisateur() throws SQLException, IllegalAccessException {
        genererNotesDependances();

        Utilisateur utilisateur = daoTesting.getUtilisateur();
        utilisateur.setIdUtilisateur(2L);

        //Récupération des notes de l'utilisateur 2
        List<Note> notesT = daoTesting.getDaoFactory().getPglNoteDAO().recupererNotesMoyennesPglUtilisateur(utilisateur);

        Note note = daoTesting.getNote();
        note.setRefEvaluateur(1L);
        note.setRefEtudiant(utilisateur.getIdUtilisateur());
        note.setIdNote(0L);

        HashMap<Integer, Float> matiereMoyenne = new HashMap<>();
        matiereMoyenne.put(1, 10.0f);
        matiereMoyenne.put(2, 14.0f);
        matiereMoyenne.put(4, 1.0f);
        matiereMoyenne.put(5, 20.0f);

        //Check
        checkCorrespondances(notesT, matiereMoyenne, utilisateur.getIdUtilisateur());
    }

    @Test
    public void recupererNotesMoyennesPglUtilisateur1() throws SQLException, IllegalAccessException {
        genererNotesDependances();

        Utilisateur utilisateur = daoTesting.getUtilisateur();
        utilisateur.setIdUtilisateur(2L);

        Sprint sprint = daoTesting.getSprint();

        //Récupération des notes de l'utilisateur 2
        List<Note> notesT = daoTesting.getDaoFactory().getPglNoteDAO().recupererNotesMoyennesPglUtilisateur(utilisateur, sprint);

        HashMap<Integer, Float> matiereMoyenne = new HashMap<>();
        matiereMoyenne.put(1, 10.0f);
        matiereMoyenne.put(2, 14.0f);
        matiereMoyenne.put(4, 1.0f);

        //Check
        checkCorrespondances(notesT, matiereMoyenne, utilisateur.getIdUtilisateur());
    }

    @Test
    public void recupererNotesMoyennesPglUtilisateur2() throws SQLException, IllegalAccessException {
        genererNotesDependances();

        Utilisateur utilisateur = daoTesting.getUtilisateur();
        utilisateur.setIdUtilisateur(2L);

        Sprint sprint = daoTesting.getSprint();

        //Récupération des notes de l'utilisateur 2
        List<Note> notesT = daoTesting.getDaoFactory().getPglNoteDAO().recupererNotesMoyennesPglUtilisateur(utilisateur, ANNEE_TEST);

        HashMap<Integer, Float> matiereMoyenne = new HashMap<>();
        matiereMoyenne.put(1, 10.0f);
        matiereMoyenne.put(2, 14.0f);
        matiereMoyenne.put(4, 1.0f);
        matiereMoyenne.put(5, 20.0f);

        //Check
        checkCorrespondances(notesT, matiereMoyenne, utilisateur.getIdUtilisateur());
    }

    @Test
    public void recupererNotesMoyennesPglUtilisateurParSprint() throws SQLException, IllegalAccessException {
        genererNotesDependances();

        final Map<Utilisateur, ENJNotes> utilisateurENJNotesMap = daoTesting.getDaoFactory().getPglNoteDAO().recupererNotesMoyennesPglUtilisateurParSprint(ParamUtilitaire.getAnneeCourante());
        assertEquals("Mauvais nombre de notes", 3, utilisateurENJNotesMap.size());
    }

    @Test
    public void recupererNotesMoyenneEquipeSprints() throws SQLException, IllegalAccessException {
        genererNotesDependances();

        final Map<Equipe, ENJNotes> equipeENJNotesMap = daoTesting.getDaoFactory().getPglNoteDAO().recupererNotesMoyenneEquipeSprints(ParamUtilitaire.getAnneeCourante());

        assertTrue("Mauvais nombre de notes", !equipeENJNotesMap.isEmpty());
    }

    @Test
    public void recupererMatieresANoterUtilisateur() throws SQLException, IllegalAccessException {
        genererNotesDependances();


        Utilisateur u = new Utilisateur();
        u.setIdUtilisateur(null);
        Utilisateur eval = new Utilisateur();
        eval.setIdUtilisateur(103L);

        final Map<Utilisateur, Map<Matiere, Note>> utilisateurMapMap = daoTesting.getDaoFactory().getPglNoteDAO().recupererMatieresANoterUtilisateur(u, eval, null, null, 1, null);

        for(Map.Entry<Utilisateur, Map<Matiere, Note>> map : utilisateurMapMap.entrySet()) {
            //System.out.println(map.getKey().getNom() + " " + map.getKey().getPrenom() + " =====================");

            for(Map.Entry<Matiere, Note> val : map.getValue().entrySet()) {
                //System.out.println(val.getKey().getLibelle() + " (" + val.getKey().getCoefficient() + ") : " + val.getValue().getNote());
            }

            assertEquals("Mauvais nombre de matieres", 5, map.getValue().size());
        }

        assertEquals("Mauvais nombre de résultat", 3, utilisateurMapMap.size());


    }


    /**
     * Ajoute les dépendances à la BDD pour les tests de moyennes
     * @throws IllegalAccessException exception
     * @throws SQLException exception
     */
    private void genererNotesDependances() throws IllegalAccessException, SQLException {
        Sprint sprint2 = daoTesting.getSprint();
        sprint2.setIdSprint(2L);
        sprint2.setNumero(2);
        daoTesting.createObjectInDatabase(sprint2);
        daoTesting.truncateTable(Matiere.class);
        daoTesting.createObjectInDatabase(daoTesting.getMatiere());

        for(int i = 0; i < 3; i++) {
            Utilisateur etu = daoTesting.getUtilisateur();
            etu.setIdUtilisateur(i+2L);
            etu.setEmail("email"+i+"test@test.fr");
            etu.setIdentifiant("etudi"+i);

            Etudiant etudiant = daoTesting.getEtudiantPgl();
            etudiant.setIdEtudiant(etu.getIdUtilisateur());

            Matiere matiere = daoTesting.getMatiere();
            matiere.setIdMatiere(i+2L);
            matiere.setLibelle("matiere"+i);

            daoTesting.createObjectInDatabase(etu);
            daoTesting.createObjectInDatabase(etudiant);
            daoTesting.createObjectInDatabase(matiere);


            EtudiantEquipe ee = daoTesting.getEtudiantEquipePgl();
            ee.setIdSprint(1L);
            ee.setIdEquipe(1L);
            ee.setIdEtudiant(i+2L);

            daoTesting.createObjectInDatabase(ee);
        }
        for(int i = 5; i < 12; i++) {
            Utilisateur eval = daoTesting.getUtilisateur();
            eval.setIdUtilisateur((long) i);
            eval.setEmail("email"+i+"test@test.fr");
            eval.setIdentifiant("eval"+i);

            daoTesting.createObjectInDatabase(eval);
        }

        Matiere matiere = daoTesting.getMatiere();
        matiere.setIdMatiere(5L);
        matiere.setLibelle("matiere5");
        matiere.setRefSprint(2L);
        daoTesting.createObjectInDatabase(matiere);

        for(int i = 0; i < notes.length; i++) {
            Note note = new Note();
            note.setIdNote(i + 1L);
            note.setRefMatiere((long) matieres[i]);
            note.setRefEtudiant((long) users[i]);
            note.setRefEvaluateur((long) evals[i]);
            note.setNote(notes[i]+0F);

            daoTesting.createObjectInDatabase(note);
        }
    }

    /**
     * Vérifie les correspondances entre les deux listes
     * @param notesT notes trouvées
     * @param matiereMoyenne notes attendues
     * @param idEtudiant id de l'etudiant concerné
     */
    private void checkCorrespondances(List<Note> notesT, HashMap<Integer, Float> matiereMoyenne, Long idEtudiant) {
        //Comparaison des résultats
        List<Note> notesAttendues = new ArrayList<>();

        Note note = daoTesting.getNote();
        note.setRefEvaluateur(1L);
        note.setRefEtudiant(idEtudiant);
        note.setIdNote(0L);

        int i = 0;
        for(Map.Entry<Integer, Float> entry : matiereMoyenne.entrySet()) {
            note.setRefMatiere((long) entry.getKey());
            note.setNote(entry.getValue());
            note.setRefEvaluateur((long) evals[i++]);
            notesAttendues.add(note);
        }

        int correspondances = 0;
        for(Note noteTrouvee : notesT) {

            for(Note noteAttendu : notesAttendues) {
                if(noteAttendu.getRefMatiere().equals(noteTrouvee.getRefMatiere())) {
                    //daoTesting.compareObjects(noteAttendu, noteTrouvee);
                    correspondances++;
                }
            }
        }

        assertEquals("Pas assez de notes trouvées", notesAttendues.size(), correspondances);
    }

}