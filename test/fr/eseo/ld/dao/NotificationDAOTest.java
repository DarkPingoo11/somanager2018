package fr.eseo.ld.dao;

import fr.eseo.ld.beans.Notification;
import fr.eseo.ld.beans.Utilisateur;
import fr.eseo.testing.DAOTesting;
import org.junit.*;

import java.sql.SQLException;
import java.util.List;

import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Classe de test de NotificationDAO
 *
 * @author Tristan LE GACQUE
 * @see NotificationDAO
 **/
public class NotificationDAOTest {

    private static DAOTesting<Notification> daoTesting;
    private static final Long ID_DEFAUT = 1L;

    /**
     * Définition du DaoTesting
     *
     * @throws Exception exception
     */
    @BeforeClass
    public static void setUp() throws Exception {
        //Définition du dao factory
        daoTesting = new DAOTesting<>();
        daoTesting.initDao(daoTesting.getDaoFactory().getNotificationDao());
    }

    @AfterClass
    public static void tearDown() throws Exception {
        daoTesting.truncateUsedTables();
    }

    @Before
    public void beforeTest() throws SQLException, IllegalAccessException {
        daoTesting.truncateUsedTables();

        //Définition des dépendances
        Utilisateur user = daoTesting.getUtilisateur();

        //Ajout des objets a la base de données
        daoTesting.createObjectInDatabase(user);
    }


    /**
     * Teste la méthode public void creer.
     */
    @Test
    public void testCreer() {
        /* Insertion du bean dans la BDD */
        Notification notification = daoTesting.getNotification();
        Notification notificationAttendu = daoTesting.getNotification();

        daoTesting.testCreerCRUD(notification, notificationAttendu);
    }

    /**
     * Teste la méthode public trouver
     */
    @Test
    public void testTrouver() {
        /* Recherche du bean dans la BDD */
        Notification notification = daoTesting.getNotification();
        Notification notificationAttendu = daoTesting.getNotification();

        daoTesting.testTrouverCRUD(notification, notificationAttendu);
    }


    /**
     * Teste la méthode public void modifier
     */
    @Test
    public void testModifier() {
        Notification notification = daoTesting.getNotification();
        Notification notificationMod = daoTesting.getNotification();

        notificationMod.setVue(1);

        daoTesting.testModifierCRUD(notification, notificationMod, notificationMod);
    }

    /**
     * @see NotificationDAO
     */
    @Ignore("Méthode non implémentée")
    public void testLister() {
    }

    /**
     * Teste la méthode public void supprimer(NoteInteretTechno NoteInteretTechno).
     */
    @Test
    public void testSupprimer() {
        Notification notification = daoTesting.getNotification();

        daoTesting.testSupprimerCRUD(notification);
    }

    @Test
    public void ajouterNotification() {
        Notification notification = daoTesting.getNotification();
        notification.setLien("Dashboard");
        daoTesting.getDaoFactory().getNotificationDao().ajouterNotification(ID_DEFAUT, notification.getCommentaire());

        //Puis on verifie qu'il existe dans la base
        List<Notification> found = daoTesting.getDaoFactory().getNotificationDao().trouver(notification);

        assertTrue("Aucun résultat n'est retourné", !found.isEmpty());
        assertEquals("Trop de résultats sont retournés", 1, found.size());

        daoTesting.compareObjects(notification, found.get(0));

    }
}