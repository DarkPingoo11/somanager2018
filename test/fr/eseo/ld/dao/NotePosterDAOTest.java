package fr.eseo.ld.dao;

import fr.eseo.ld.beans.*;
import fr.eseo.testing.DAOTesting;
import org.apache.log4j.*;
import org.easymock.EasyMock;
import org.easymock.EasyMockRunner;
import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

import java.io.ByteArrayOutputStream;
import java.sql.SQLException;

import static org.easymock.EasyMock.*;
import static org.junit.Assert.assertTrue;

/**
 * Classe de tests unitaires JUnit 4 de la classe NotePosterDAO.
 *
 * <p>Utilisation du modèle DAO.</p>
 *
 * @version 1.0
 * @author Maxime LENORMAND
 *
 * @see fr.eseo.ld.dao.NotePosterDAO
 * @see org.junit
 */
@RunWith(EasyMockRunner.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class NotePosterDAOTest {

	private static final Long LONG_TEST_PROFESSEUR = 2L;

	private static Logger logger = Logger.getLogger(NotePosterDAO.class.getName());
	private static DAOTesting<NotePoster> daoTesting;

	/**
	 * Définition du DaoTesting
	 * @throws Exception exception
	 */
	@BeforeClass
	public static void setUp() throws Exception {
		//Définition du dao factory
		daoTesting = new DAOTesting<>();
		daoTesting.initDao(daoTesting.getDaoFactory().getNotePosterDao());

		daoTesting.truncateTable(OptionESEO.class);
		daoTesting.truncateTable(AnneeScolaire.class);
		daoTesting.truncateTable(Sujet.class);
		daoTesting.truncateTable(Equipe.class);
		daoTesting.truncateTable(Poster.class);
        daoTesting.truncateTable(Utilisateur.class);
        daoTesting.truncateTable(Etudiant.class);
        daoTesting.truncateTable(Professeur.class);
        daoTesting.truncateTable(EtudiantEquipe.class);
        daoTesting.truncateTable(JuryPoster.class);

	}

	@AfterClass
	public static void tearDown() throws Exception {
		daoTesting.truncateUsedTables();
	}

	@Before
	public void beforeTest() throws SQLException, IllegalAccessException {
		daoTesting.truncateUsedTables();

		//Définition des dépendances
		OptionESEO option = daoTesting.getOptionESEO();
		AnneeScolaire annee = daoTesting.getAnneeScolaire();
		Sujet sujet = daoTesting.getSujet();
		Equipe equipe = daoTesting.getEquipe();
		Poster poster = daoTesting.getPoster();
		Utilisateur u1 = daoTesting.getUtilisateur();
		Utilisateur u2 = daoTesting.getUtilisateur();
		Etudiant etudiant = daoTesting.getEtudiant();
		Professeur prof = daoTesting.getProfesseur();
		EtudiantEquipe ee = daoTesting.getEtudiantEquipe();
		JuryPoster jury = daoTesting.getJuryPoster();
        jury.setIdProf1(LONG_TEST_PROFESSEUR);
        jury.setIdProf2(LONG_TEST_PROFESSEUR);
        jury.setIdProf3(LONG_TEST_PROFESSEUR);
		u2.setEmail("mail2");
		u2.setIdentifiant("identifiant2");
		u2.setIdUtilisateur(LONG_TEST_PROFESSEUR);
		prof.setIdProfesseur(u2.getIdUtilisateur());

		//Ajout des objets a la base de données
		daoTesting.createObjectInDatabase(option);
		daoTesting.createObjectInDatabase(annee);
		daoTesting.createObjectInDatabase(sujet);
		daoTesting.createObjectInDatabase(equipe);
		daoTesting.createObjectInDatabase(poster);
		daoTesting.createObjectInDatabase(u1);
		daoTesting.createObjectInDatabase(u2);
		daoTesting.createObjectInDatabase(prof);
		daoTesting.createObjectInDatabase(etudiant);
		daoTesting.createObjectInDatabase(ee);
        daoTesting.createObjectInDatabase(jury);
	}



	/**
	 * Teste la méthode public void creer(NotePoster notePoster).
	 */
	@Test
	public void test1_1Creer() {
		/* Insertion du bean dans la BDD */
		NotePoster note = daoTesting.getNotePoster();
		note.setIdProfesseur(LONG_TEST_PROFESSEUR);

		daoTesting.testCreerCRUD(note, note);
	}

	/**
	 * Teste la méthode public List<NotePoster> trouver(NotePoster notePoster).
	 */
	@Test
	public void test2_1Trouver() {
		/* Insertion du bean dans la BDD */
		NotePoster note = daoTesting.getNotePoster();
		note.setIdProfesseur(LONG_TEST_PROFESSEUR);

		daoTesting.testTrouverCRUD(note, note);
	}

	/**
	 * Teste la méthode public void modifier(NotePoster notePoster).
	 */
	@Test
	public void test3_1Modifier() {
		/* Insertion du bean dans la BDD */
		NotePoster note = daoTesting.getNotePoster();
		NotePoster note2 = daoTesting.getNotePoster();
		note.setIdProfesseur(LONG_TEST_PROFESSEUR);
		note2.setIdProfesseur(LONG_TEST_PROFESSEUR);

		note2.setNote(20f);

		daoTesting.testModifierCRUD(note, note2,note2);
	}

	/**
	 * Teste la méthode public List<NotePoster> lister().
	 */
	@Test
	public void test4_1Lister() {
		/* Recherche du bean dans la BDD */
		NotePoster note = daoTesting.getNotePoster();
		note.setIdProfesseur(LONG_TEST_PROFESSEUR);

		daoTesting.testListerCRUD(new NotePoster[]{note}, new NotePoster[]{note});
	}

	/**
	 * Teste la méthode public List<NotePoster> lister().
	 * <p>Cas d'une erreur lors de la création de la connexion.</p>
	 * @throws SQLException exceptiob
	 */
	@Test
	public void test4_2ListerLog() throws SQLException {
		/* Configuraton du logger */
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		Layout layout = new SimpleLayout();
		Appender appender = new WriterAppender(layout, out);
		logger.addAppender(appender);

		try {
			/* Mock de la méthode creerConnexion() */
			NotePosterDAO notePosterDAOMock = EasyMock.createMockBuilder(NotePosterDAO.class)
					.addMockedMethod("creerConnexion").createMock();
			expect(notePosterDAOMock.creerConnexion()).andThrow(new SQLException());

			replay(notePosterDAOMock);

			/* Recherche du bean dans la BDD */
			notePosterDAOMock.lister();

			/* Vérification du logging */
			String messageLog = out.toString();
			assertTrue("Mauvais logging", messageLog.contains("Échec du listage"));

			verify(notePosterDAOMock);
		} finally {
			/* Configuraton initiale du logger */
			logger.removeAppender(appender);
		}
	}

	/**
	 * Teste la méthode public void supprimer(NotePoster notePoster).
	 */
	@Test
	public void test5_1Supprimer() {
		/* Insertion du bean dans la BDD */
		NotePoster note = daoTesting.getNotePoster();
		note.setIdProfesseur(LONG_TEST_PROFESSEUR);

		daoTesting.testSupprimerCRUD(note);
	}

	@Test
	public void testLogging() {
		daoTesting.testLoggingCRUD();
	}

}