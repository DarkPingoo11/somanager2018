package fr.eseo.ld.dao;

import fr.eseo.ld.pgl.beans.Soutenance;
import fr.eseo.testing.DAOTesting;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.sql.SQLException;

/**
 * Classe de test de PglSoutenanceDAO
 *
 * @author Tristan LE GACQUE
 * @see PglSoutenanceDAO
 **/
public class PglSoutenanceDAOTest {

    private static DAOTesting<Soutenance> daoTesting;

    /**
     * Définition du DaoTesting
     *
     */
    @BeforeClass
    public static void setUp() {
        //Définition du dao factory
        daoTesting = new DAOTesting<>();
        daoTesting.initDao(daoTesting.getDaoFactory().getPGLSoutenanceDAO());

        daoTesting.addAlias(Soutenance.class, "pgl_soutenance");
    }

    @AfterClass
    public static void tearDown() throws Exception {
        daoTesting.truncateUsedTables();
    }

    @Before
    public void beforeTest() throws SQLException, IllegalAccessException {
        daoTesting.truncateUsedTables();
    }

    /**
     * Teste la méthode public void creer.
     *
     * @see PglSoutenanceDAO#creer(Soutenance)
     */
    @Test
    public void testCreer() {
        /* Insertion du bean dans la BDD */
        Soutenance pglsoutenance = daoTesting.getSoutenancePGL();
        Soutenance pglsoutenanceAttendu = daoTesting.getSoutenancePGL();

        daoTesting.testCreerCRUD(pglsoutenance, pglsoutenanceAttendu);
    }

    /**
     * Teste la méthode public trouver
     *
     * @see PglSoutenanceDAO#trouver(Soutenance)
     */
    @Test
    public void testTrouver() {
        /* Recherche du bean dans la BDD */
        Soutenance pglsoutenance = daoTesting.getSoutenancePGL();
        Soutenance pglsoutenanceAttendu = daoTesting.getSoutenancePGL();

        daoTesting.testTrouverCRUD(pglsoutenance, pglsoutenanceAttendu);
    }


    /**
     * Teste la méthode public void modifier
     *
     * @see PglSoutenanceDAO#modifier(Soutenance)
     */
    @Test
    public void testModifier() {
        Soutenance pglsoutenance = daoTesting.getSoutenancePGL();
        Soutenance pglsoutenanceMod = daoTesting.getSoutenancePGL();

        pglsoutenanceMod.setLibelle("Autre");
        daoTesting.testModifierCRUD(pglsoutenance, pglsoutenanceMod, pglsoutenanceMod);
    }

    /**
     * Teste la méthode public lister().
     *
     * @see PglSoutenanceDAO#lister()
     */
    @Test
    public void testLister() {
        Soutenance pglsoutenance = daoTesting.getSoutenancePGL();
        Soutenance pglsoutenanceAttendu = daoTesting.getSoutenancePGL();

        daoTesting.testListerCRUD(new Soutenance[]{pglsoutenance}, new Soutenance[]{pglsoutenanceAttendu});
    }

    /**
     * Teste la méthode public void supprimer(NoteInteretTechno NoteInteretTechno).
     *
     * @see PglSoutenanceDAO#supprimer(Soutenance)
     */
    @Test
    public void testSupprimer() {
        Soutenance pglsoutenance = daoTesting.getSoutenancePGL();

        daoTesting.testSupprimerCRUD(pglsoutenance);
    }

    /**
     * Test les erreurs
     */
    @Test
    public void testLogging() {
        daoTesting.testLogging(DAOTesting.CRUD.MODIFIER, DAOTesting.CRUD.SUPPRIMER, DAOTesting.CRUD.TROUVER);
    }
}