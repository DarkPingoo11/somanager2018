package fr.eseo.ld.dao;

import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.replay;
import static org.easymock.EasyMock.verify;
import static org.junit.Assert.assertTrue;

import java.io.ByteArrayOutputStream;
import java.sql.SQLException;
import java.util.List;

import fr.eseo.ld.beans.*;
import fr.eseo.testing.DAOTesting;
import org.apache.log4j.Appender;
import org.apache.log4j.Layout;
import org.apache.log4j.Logger;
import org.apache.log4j.SimpleLayout;
import org.apache.log4j.WriterAppender;
import org.junit.*;

import org.easymock.EasyMock;
import org.easymock.EasyMockRunner;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

/**
 * Classe de tests unitaires JUnit 4 de la classe ProfesseurSujetDAO.
 *
 * <p>Utilisation du modèle DAO.</p>
 *
 * @version 1.0
 * @author Quentin Pichavant
 *
 * @see fr.eseo.ld.dao.ProfesseurSujetDAO
 * @see org.junit
 */

@RunWith(EasyMockRunner.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ProfesseurSujetDAOTest {

	private static final Long LONG_TEST = 11L;

	private static Logger logger = Logger.getLogger(ProfesseurSujetDAO.class.getName());

	private static ProfesseurSujet professeurSujet;
	private static ProfesseurSujetDAO professeurSujetDAO;
    private static final Long ID_PROF = 1L;
    private static final Long ID_SUJET = 1L;
    private static final String NOMTEST = "nomprenomTEST";
    private static final String VALIDE = "oui";
    private static final FonctionProfesseurSujet FONCTION_PROFESSEUR_SUJET = FonctionProfesseurSujet.CONSULTANT;
    private static final FonctionProfesseurSujet FONCTION_PROFESSEUR_MODIF = FonctionProfesseurSujet.CONSULTANT;

    private static DAOTesting<ProfesseurSujet> daoTesting;


	/**
	 * Récupère une instance de DAO et initialise le bean correspondant.
	 *
	 * @throws Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
        //Définition du dao factory
        daoTesting = new DAOTesting<>();
        daoTesting.initDao(daoTesting.getDaoFactory().getProfesseurSujetDAO());

        //Truncate
        daoTesting.truncateTable();
        daoTesting.truncateTable(Sujet.class);
        daoTesting.truncateTable(Professeur.class);
        daoTesting.truncateTable(Utilisateur.class);

        //Création d'un sujet type
        Sujet s = creerSujet();

        //Création du prof
        Utilisateur user = new Utilisateur();
        user.setIdUtilisateur(ID_PROF);
        user.setPrenom(NOMTEST);
        user.setNom(NOMTEST);
        user.setIdentifiant(NOMTEST);
        user.setValide("oui");

        Professeur prof = new Professeur();
        prof.setIdProfesseur(ID_PROF);

        daoTesting.getDaoFactory().getUtilisateurDao().creer(user);
        daoTesting.getDaoFactory().getSujetDao().creer(s);
        daoTesting.getDaoFactory().getProfesseurDAO().creer(prof);
	}

    @AfterClass
    public static void tearDown() throws Exception {
        daoTesting.truncateTable();
        daoTesting.truncateTable(Sujet.class);
        daoTesting.truncateTable(Professeur.class);
        daoTesting.truncateTable(Utilisateur.class);
    }

    @Before
    public void beforeTest() throws SQLException {
        daoTesting.truncateTable();
    }

    /**
     * Créer un prof
     * @return Prof crée
     */
    private ProfesseurSujet creerProf() {
        ProfesseurSujet prof = new ProfesseurSujet();
        prof.setIdProfesseur(ID_PROF);
        prof.setIdSujet(ID_SUJET);
        prof.setValide(VALIDE);
        prof.setFonction(FONCTION_PROFESSEUR_SUJET);
        return prof;
    }

    private static Sujet creerSujet() {
        Sujet s = new Sujet();
        s.setIdSujet(ID_SUJET);
        s.setEtat(EtatSujet.VALIDE);
        s.setNbrMinEleves(0);
        s.setNbrMaxEleves(10);
        s.setConfidentialite(false);
        s.setContratPro(false);
        s.setTitre("Titre");
        s.setDescription("Description");
        return s;
    }

    /**
     * Teste la méthode public List<ProfesseurSujet> lister().
     */
    @Ignore
    public void test1_Lister() {
        /* Recherche du bean dans la BDD */
        final List<ProfesseurSujet> professeursSujets = professeurSujetDAO.lister();

        /* Vérification que le bean inséré est présent dans la liste */
        boolean estPresent = false;
        int i = 0;
        while (!estPresent && i < professeursSujets.size()) {
            if (LONG_TEST.equals(professeursSujets.get(i).getIdProfesseur())) {
                estPresent = true;
            }
            i++;
        }
        assertTrue("Le bean inséré n'est pas présent dans la liste", estPresent);
    }


    /**
     * Teste la méthode public List<Professeur> lister().
     * <p>Cas d'une erreur lors de la création de la connexion.</p>
     * @throws SQLException
     */
    @Ignore
    public void test1_ListerLog() throws SQLException {
        /* Configuraton du logger */
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        Layout layout = new SimpleLayout();

        Appender appender = new WriterAppender(layout, out);
        logger.addAppender(appender);

        try {
            /* Mock de la méthode creerConnexion() */
            ProfesseurSujetDAO professeurSujetDAOMock = EasyMock.createMockBuilder(ProfesseurSujetDAO.class)
                    .addMockedMethod("creerConnexion").createMock();
            expect(professeurSujetDAOMock.creerConnexion()).andThrow(new SQLException());

            replay(professeurSujetDAOMock);

            /* Recherche du bean dans la BDD */
            professeurSujetDAOMock.lister();

            /* Vérification du logging */
            String messageLog = out.toString();
            assertTrue("Mauvais logging", messageLog.contains("Échec du listage"));

            verify(professeurSujetDAOMock);
        } finally {
            /* Configuraton initiale du logger */
            logger.removeAppender(appender);
        }
    }

    @Ignore
    public void creer() {
        ProfesseurSujet prof = creerProf();
        ProfesseurSujet profAttendu = creerProf();

        daoTesting.testCreerCRUD(prof, profAttendu);
    }

    @Ignore
    public void trouver() {
        ProfesseurSujet prof = creerProf();
        ProfesseurSujet profAttendu = creerProf();

        daoTesting.testTrouverCRUD(prof, profAttendu);
    }

    @Ignore
    public void modifier() {
        ProfesseurSujet prof = creerProf();
        ProfesseurSujet profAttendu = creerProf();
        ProfesseurSujet profModifier = creerProf();

        profModifier.setFonction(FONCTION_PROFESSEUR_MODIF);
        profAttendu.setFonction(FONCTION_PROFESSEUR_MODIF);

        daoTesting.testModifierCRUD(prof, profModifier, profAttendu);
    }

    @Ignore
    public void supprimer() {
        ProfesseurSujet prof = creerProf();

        daoTesting.testSupprimerCRUD(prof);
    }

    @Test
    public void lister() throws SQLException, IllegalAccessException {
        ProfesseurSujet[] profs = new ProfesseurSujet[1];
        ProfesseurSujet[] profsAttendus = new ProfesseurSujet[1];

        profs[0] = creerProf();
        profs[0].setFonction(FonctionProfesseurSujet.INTERESSE);
        profs[0].setIdSujet(1L);

        profsAttendus[0] = creerProf();
        profsAttendus[0].setFonction(FonctionProfesseurSujet.INTERESSE);
        profsAttendus[0].setIdSujet(1L);

        daoTesting.testListerCRUD(profs, profsAttendus);
    }

    @Ignore
    public void creerConnexion() {
    }
}