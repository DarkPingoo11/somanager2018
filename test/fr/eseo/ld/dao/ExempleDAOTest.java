package fr.eseo.ld.dao;

import fr.eseo.ld.beans.Professeur;
import fr.eseo.ld.beans.Utilisateur;
import fr.eseo.testing.DAOTesting;
import org.junit.*;
import org.junit.runners.MethodSorters;

import java.sql.SQLException;

/**
 * Exemple de test utilisant DAOTesting
 *
 * <p>Utilisation de DAOTesting</p>
 * Classe utilisée pour l'exemple : Professeur
 *
 * @version 1.0
 * @author Tristan LE GACQUE
 *
 * @see DAOTesting
 * @see org.junit
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ExempleDAOTest {

	private static DAOTesting<Professeur> daoTesting;

	/**
	 * Définition du DaoTesting
	 * @throws Exception exception
	 */
	@BeforeClass
	public static void setUp() throws Exception {
		//Définition du dao factory
		daoTesting = new DAOTesting<>();
		daoTesting.initDao(daoTesting.getDaoFactory().getProfesseurDAO());
	}

	@AfterClass
	public static void tearDown() throws Exception {
		daoTesting.truncateUsedTables();
	}

	@Before
	public void beforeTest() throws SQLException, IllegalAccessException {
		daoTesting.truncateUsedTables();

		//Définition des dépendances
		Utilisateur user = daoTesting.getUtilisateur();
		Professeur prof = daoTesting.getProfesseur();

		//Ajout des objets a la base de données
		daoTesting.createObjectInDatabase(user);
		daoTesting.createObjectInDatabase(prof);
	}



	/**
	 * Teste la méthode public void creer(NoteInteretTechno NoteInteretTechno).
	 */
	@Test
	public void testCreer() {
		/* Insertion du bean dans la BDD */
		Professeur prof = daoTesting.getProfesseur();
		Professeur profAttendu = daoTesting.getProfesseur();

		daoTesting.testCreerCRUD(prof, profAttendu);
	}

	/**
	 * Teste la méthode public List<NoteInteretTechno> trouver(NoteInteretTechno NoteInteretTechno).
	 */
	@Test
	public void testTrouver() {
		/* Recherche du bean dans la BDD */
		Professeur prof = daoTesting.getProfesseur();
		Professeur profAttendu = daoTesting.getProfesseur();

		daoTesting.testTrouverCRUD(prof, profAttendu);
	}


	/**
	 * Teste la méthode public void modifier(NoteInteretTechno NoteInteretTechno).
	 * @see ModeleExcelDAOTest
	 */
	@Test
	public void testModifier() {
		//Pour le coup on ne peux pas modifier un Professeur
		//Mais pour une autre classe, il suffit d'avoir un Deux objets, l'initial et le modifié
		Professeur prof = daoTesting.getProfesseur();
		Professeur profModifier = daoTesting.getProfesseur();

		daoTesting.testModifierCRUD(prof, profModifier, profModifier);
	}

	/**
	 * Teste la méthode public lister().
	 * Regarder ModeleExcelDao pour un exemple plus complet
	 * @see ModeleExcelDAOTest
	 */
	@Test
	public void testLister() throws IllegalAccessException {
		Professeur prof = daoTesting.getProfesseur();

		daoTesting.testListerCRUD(new Professeur[]{prof}, new Professeur[]{prof});
	}

	/**
	 * Teste la méthode public void supprimer(NoteInteretTechno NoteInteretTechno).
	 */
	@Test
	public void testSupprimer() {
		Professeur prof = daoTesting.getProfesseur();

		daoTesting.testSupprimerCRUD(prof);
	}

	/**
	 * Test les erreurs
	 */
	@Test
	public void testLogging() {
		daoTesting.testLoggingCRUD();
	}
}