package fr.eseo.ld.dao;

import fr.eseo.ld.beans.*;
import fr.eseo.testing.DAOTesting;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.sql.SQLException;

public class PartageRoleDAOTest {

    private static DAOTesting<PartageRole> daoTesting;

    /**
     * Définition du DaoTesting
     *
     * @throws Exception exception
     */
    @BeforeClass
    public static void setUp() throws Exception {
        //Définition du dao factory
        daoTesting = new DAOTesting<>();
        daoTesting.initDao(daoTesting.getDaoFactory().getPartageDAO());
    }

    @AfterClass
    public static void tearDown() throws Exception {
        daoTesting.truncateUsedTables();
    }

    @Before
    public void beforeTest() throws SQLException, IllegalAccessException {
        daoTesting.truncateUsedTables();

        //Définition des dépendances
        Utilisateur user = daoTesting.getUtilisateur();
        Utilisateur user2 = daoTesting.getUtilisateur();
        Professeur prof = daoTesting.getProfesseur();
        OptionESEO option = daoTesting.getOptionESEO();
        Role role = daoTesting.getRole();
        user2.setIdUtilisateur(2L);
        user2.setIdentifiant("user2");
        user2.setEmail("u2@test.fr");
        prof.setIdProfesseur(2L);

        //Ajout des objets a la base de données
        daoTesting.createObjectInDatabase(user);
        daoTesting.createObjectInDatabase(user2);
        daoTesting.createObjectInDatabase(prof);
        daoTesting.createObjectInDatabase(option);
        daoTesting.createObjectInDatabase(role);
    }


    /**
     * Teste la méthode public void creer.
     */
    @Test
    public void testCreer() {
        /* Insertion du bean dans la BDD */
        PartageRole partagerole = daoTesting.getPartageRole();
        PartageRole partageroleAttendu = daoTesting.getPartageRole();

        daoTesting.testCreerCRUD(partagerole, partageroleAttendu);
    }

    /**
     * Teste la méthode public trouver
     */
    @Test
    public void testTrouver() {
        /* Recherche du bean dans la BDD */
        PartageRole partagerole = daoTesting.getPartageRole();
        PartageRole partageroleAttendu = daoTesting.getPartageRole();

        daoTesting.testTrouverCRUD(partagerole, partageroleAttendu);
    }


    /**
     * Teste la méthode public void modifier
     */
    @Test
    public void testModifier() {
        PartageRole partagerole = daoTesting.getPartageRole();
        PartageRole partageroleMod = daoTesting.getPartageRole();

        partageroleMod.setDateFin("2022-01-01 12:00:00.0");

        daoTesting.testModifierCRUD(partagerole, partageroleMod, partageroleMod);
    }

    /**
     * Teste la méthode public lister().
     * Regarder ModeleExcelDao pour un exemple plus complet
     *
     * @see ModeleExcelDAOTest
     */
    @Test
    public void testLister() {
        PartageRole partagerole = daoTesting.getPartageRole();
        PartageRole partageroleAttendu = daoTesting.getPartageRole();

        daoTesting.testListerCRUD(new PartageRole[]{partagerole}, new PartageRole[]{partageroleAttendu});
    }

    /**
     * Teste la méthode public void supprimer(NoteInteretTechno NoteInteretTechno).
     */
    @Test
    public void testSupprimer() {
        PartageRole partagerole = daoTesting.getPartageRole();

        daoTesting.testSupprimerCRUD(partagerole);
    }

    /**
     * Test les erreurs
     */
    @Test
    public void testLogging() {
        daoTesting.testLoggingCRUD();
    }
}