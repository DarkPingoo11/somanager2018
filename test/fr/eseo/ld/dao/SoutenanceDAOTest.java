package fr.eseo.ld.dao;

import fr.eseo.ld.beans.*;
import fr.eseo.testing.DAOTesting;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Classe de test de SoutenanceDAO
 *
 * @author Tristan LE GACQUE
 * @see SoutenanceDAO
 **/
public class SoutenanceDAOTest {

    private static final Long LONG_TEST_PROFESSEUR = 2L;
    private static DAOTesting<Soutenance> daoTesting;

    /**
     * Définition du DaoTesting
     *
     * @throws Exception exception
     */
    @BeforeClass
    public static void setUp() throws Exception {
        //Définition du dao factory
        daoTesting = new DAOTesting<>();
        daoTesting.initDao(daoTesting.getDaoFactory().getSoutenanceDAO());

        daoTesting.truncateTable(JurySoutenance.class);
        daoTesting.truncateTable(EtudiantEquipe.class);
        daoTesting.truncateTable(Equipe.class);
        daoTesting.truncateTable(Etudiant.class);
        daoTesting.truncateTable(Professeur.class);
        daoTesting.truncateTable(Utilisateur.class);
        daoTesting.truncateTable(Sujet.class);
        daoTesting.truncateTable(AnneeScolaire.class);
        daoTesting.truncateTable(OptionESEO.class);
    }

    @AfterClass
    public static void tearDown() throws Exception {
        daoTesting.truncateUsedTables();
    }

    @Before
    public void beforeTest() throws SQLException, IllegalAccessException {
        daoTesting.truncateUsedTables();

        //Définition des dépendances
        OptionESEO option = daoTesting.getOptionESEO();
        AnneeScolaire annee = daoTesting.getAnneeScolaire();
        Sujet sujet = daoTesting.getSujet();
        Equipe equipe = daoTesting.getEquipe();
        Utilisateur u1 = daoTesting.getUtilisateur();
        Utilisateur u2 = daoTesting.getUtilisateur();
        Etudiant etudiant = daoTesting.getEtudiant();
        Professeur prof = daoTesting.getProfesseur();
        EtudiantEquipe ee = daoTesting.getEtudiantEquipe();
        JurySoutenance jury = daoTesting.getJurySoutenance();
        jury.setIdProf1(LONG_TEST_PROFESSEUR);
        jury.setIdProf2(LONG_TEST_PROFESSEUR);
        u2.setEmail("mail2");
        u2.setIdentifiant("identifiant2");
        u2.setIdUtilisateur(LONG_TEST_PROFESSEUR);
        prof.setIdProfesseur(u2.getIdUtilisateur());

        //Ajout des objets a la base de données
        daoTesting.createObjectInDatabase(option);
        daoTesting.createObjectInDatabase(annee);
        daoTesting.createObjectInDatabase(sujet);
        daoTesting.createObjectInDatabase(equipe);
        daoTesting.createObjectInDatabase(u1);
        daoTesting.createObjectInDatabase(u2);
        daoTesting.createObjectInDatabase(prof);
        daoTesting.createObjectInDatabase(etudiant);
        daoTesting.createObjectInDatabase(ee);
        daoTesting.createObjectInDatabase(jury);
    }


    /**
     * Teste la méthode public void creer.
     */
    @Test
    public void testCreer() {
        /* Insertion du bean dans la BDD */
        Soutenance soutenance = daoTesting.getSoutenance();
        Soutenance soutenanceAttendu = daoTesting.getSoutenance();

        daoTesting.testCreerCRUD(soutenance, soutenanceAttendu);
    }

    /**
     * Teste la méthode public trouver
     */
    @Test
    public void testTrouver() {
        /* Recherche du bean dans la BDD */
        Soutenance soutenance = daoTesting.getSoutenance();
        Soutenance soutenanceAttendu = daoTesting.getSoutenance();

        daoTesting.testTrouverCRUD(soutenance, soutenanceAttendu);
    }


    /**
     * Teste la méthode public void modifier
     */
    @Test
    public void testModifier() throws ParseException {
        Soutenance soutenance = daoTesting.getSoutenance();
        Soutenance soutenanceMod = daoTesting.getSoutenance();

        Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("2018-01-01 12:11:00");
        soutenanceMod.setDateSoutenance(date);

        daoTesting.testModifierCRUD(soutenance, soutenanceMod, soutenanceMod);
    }

    /**
     * Teste la méthode public lister().
     * Regarder ModeleExcelDao pour un exemple plus complet
     *
     * @see ModeleExcelDAOTest
     */
    @Test
    public void testLister() {
        Soutenance soutenance = daoTesting.getSoutenance();
        Soutenance soutenanceAttendu = daoTesting.getSoutenance();

        daoTesting.testListerCRUD(new Soutenance[]{soutenance}, new Soutenance[]{soutenanceAttendu});
    }

    /**
     * Teste la méthode public void supprimer(NoteInteretTechno NoteInteretTechno).
     */
    @Test
    public void testSupprimer() {
        Soutenance soutenance = daoTesting.getSoutenance();

        daoTesting.testSupprimerCRUD(soutenance);
    }

    /**
     * Test les erreurs
     */
    @Test
    public void testLogging() {
        daoTesting.testLoggingCRUD();
    }

    @Test
    public void trouverSoutenanceSujet() throws IllegalAccessException {
        Sujet s = daoTesting.getSujet();
        Soutenance soutenance = daoTesting.getSoutenance();

        //Création de l'objet
        daoTesting.createObjectInDatabase(soutenance);

        //Test
        final Soutenance soutenanceT = daoTesting.getDaoFactory().getSoutenanceDAO().trouverSoutenanceSujet(s);
        daoTesting.compareObjects(soutenance, soutenanceT);
    }
}