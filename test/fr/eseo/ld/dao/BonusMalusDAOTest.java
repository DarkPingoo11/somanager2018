package fr.eseo.ld.dao;

import fr.eseo.ld.beans.AnneeScolaire;
import fr.eseo.ld.pgl.beans.*;
import fr.eseo.ld.beans.OptionESEO;
import fr.eseo.ld.beans.Utilisateur;
import fr.eseo.testing.DAOTesting;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.sql.SQLException;

/**
 * Classe de test de BonusMalusDAO
 *
 * @author Tristan LE GACQUE
 * @see BonusMalusDAO
 **/
public class BonusMalusDAOTest {

    private static DAOTesting<BonusMalus> daoTesting;

    /**
     * Définition du DaoTesting
     *
     * @throws Exception exception
     */
    @BeforeClass
    public static void setUp() throws Exception {
        //Définition du dao factory
        daoTesting = new DAOTesting<>();
        daoTesting.initDao(daoTesting.getDaoFactory().getBonusMalusDAO());

        daoTesting.addAlias(Equipe.class, "pgl_equipe");
        daoTesting.addAlias(Sprint.class, "pgl_sprint");
        daoTesting.addAlias(BonusMalus.class, "pgl_bonusmalus");
        daoTesting.addAlias(Etudiant.class, "pgl_etudiant");

        daoTesting.truncateTable(OptionESEO.class);
        daoTesting.truncateTable(Utilisateur.class);
        daoTesting.truncateTable(AnneeScolaire.class);
        daoTesting.truncateTable(Equipe.class);
        daoTesting.truncateTable(Etudiant.class);
        daoTesting.truncateTable(Sprint.class);
    }

    @AfterClass
    public static void tearDown() throws Exception {
        daoTesting.truncateUsedTables();
    }

    @Before
    public void beforeTest() throws SQLException, IllegalAccessException {
        daoTesting.truncateUsedTables();

        //Définition des dépendances
        OptionESEO optionESEO = daoTesting.getOptionESEO();
        Utilisateur user = daoTesting.getUtilisateur();
        AnneeScolaire anneeScolaire =daoTesting.getAnneeScolaire();
        Equipe equipe = daoTesting.getEquipePGL();
        Sprint sprint = daoTesting.getSprint();
        Etudiant etudiant = daoTesting.getEtudiantPgl();


        //Ajout des objets a la base de données
        daoTesting.createObjectInDatabase(optionESEO);
        daoTesting.createObjectInDatabase(anneeScolaire);
        daoTesting.createObjectInDatabase(user);
        daoTesting.createObjectInDatabase(equipe);
        daoTesting.createObjectInDatabase(etudiant);
        daoTesting.createObjectInDatabase(sprint);
    }


    /**
     * Teste la méthode public void creer.
     */
    @Test
    public void testCreer() {
        /* Insertion du bean dans la BDD */
        BonusMalus bonusmalus = daoTesting.getBonusMalus();
        BonusMalus bonusmalusAttendu = daoTesting.getBonusMalus();

        daoTesting.testCreerCRUD(bonusmalus, bonusmalusAttendu);
    }

    /**
     * Teste la méthode public trouver
     */
    @Test
    public void testTrouver() {
        /* Recherche du bean dans la BDD */
        BonusMalus bonusmalus = daoTesting.getBonusMalus();
        BonusMalus bonusmalusAttendu = daoTesting.getBonusMalus();

        daoTesting.testTrouverCRUD(bonusmalus, bonusmalusAttendu);
    }


    /**
     * Teste la méthode public void modifier
     */
    @Test
    public void testModifier() {
        BonusMalus bonusmalus = daoTesting.getBonusMalus();
        BonusMalus bonusmalusMod = daoTesting.getBonusMalus();

        bonusmalusMod.setValidation(BonusMalus.VALIDATION_TYPE.REFUSER);

        daoTesting.testModifierCRUD(bonusmalus, bonusmalusMod, bonusmalusMod);
    }

    /**
     * Teste la méthode public lister().
     */
    @Test
    public void testLister() {
        BonusMalus bonusmalus = daoTesting.getBonusMalus();
        BonusMalus bonusmalusAttendu = daoTesting.getBonusMalus();

        daoTesting.testListerCRUD(new BonusMalus[]{bonusmalus}, new BonusMalus[]{bonusmalusAttendu});
    }

    /**
     * Teste la méthode public void supprimer(NoteInteretTechno NoteInteretTechno).
     */
    @Test
    public void testSupprimer() {
        BonusMalus bonusmalus = daoTesting.getBonusMalus();

        daoTesting.testSupprimerCRUD(bonusmalus);
    }

    /**
     * Test les erreurs
     */
    @Test
    public void testLogging() {
        daoTesting.testLogging(DAOTesting.CRUD.CREER, DAOTesting.CRUD.SUPPRIMER, DAOTesting.CRUD.MODIFIER);
    }
}