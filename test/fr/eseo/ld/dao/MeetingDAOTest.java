package fr.eseo.ld.dao;


import fr.eseo.ld.beans.Meeting;
import fr.eseo.ld.beans.Utilisateur;
import fr.eseo.testing.DAOTesting;
import org.apache.log4j.*;
import org.easymock.EasyMock;
import org.easymock.EasyMockRunner;
import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

import java.io.ByteArrayOutputStream;
import java.sql.SQLException;

import static junit.framework.TestCase.assertTrue;
import static org.easymock.EasyMock.*;

/**
 * Classe de tests unitaires JUnit 4 de la classe MeetingDAO.
 *
 * <p>Utilisation du modèle DAO.</p>
 *
 * @author Dimitri J
 * @version 1.0
 * @see fr.eseo.ld.dao.MeetingDAO
 * @see org.junit
 */
@RunWith(EasyMockRunner.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class MeetingDAOTest {

    private static final String STRING_TEST = "test";
    private static final String STRING_TEST_MODIFIE = "test2";
    private static final String STRING_TEST_LIEU = "B002";
    private static final String STRING_TEST_DATE = "2018-05-23 00:00:00.0";
    private static final Long LONG_TEST_UTILISATEUR = 1L;
    private static final Long LONG_TEST_OPTION = 1L;
    private static final Long LONG_TEST_REUNION = 1L;

    private static Logger logger = Logger.getLogger(MeetingDAO.class.getName());
    private static Meeting meeting;
    private static Utilisateur utilisateur;
    private static MeetingDAO meetingDAO;


    private static DAOTesting<Meeting> daoTesting;


    @BeforeClass
    public static void setUp() throws Exception {
        //Définition du dao factory
        daoTesting = new DAOTesting<>();
        daoTesting.initDao(daoTesting.getDaoFactory().getMeetingDAO());
        daoTesting.addAlias(Meeting.class, "reunion");

        meetingDAO = daoTesting.getDaoFactory().getMeetingDAO();
    }

    @AfterClass
    public static void tearDown() throws Exception {
        daoTesting.truncateTable();
        daoTesting.truncateTable(Utilisateur.class);
    }

    @Before
    public void beforeTest() throws SQLException, IllegalAccessException {
        daoTesting.truncateTable();
        daoTesting.truncateTable(Utilisateur.class);
        this.creerUtilisateur();
    }


    @Test
    public void creer() throws IllegalAccessException {
        Meeting meeting = this.createMeeting();

        daoTesting.testCreerCRUD(meeting, meeting);
    }

    @Test
    public void trouver() {
        Meeting meeting = this.createMeeting();

        daoTesting.testTrouverCRUD(meeting, meeting);
    }


    @Test
    public void modifier() {
        Meeting meeting = this.createMeeting();
        Meeting meetingModifier = this.createMeeting();

        meetingModifier.setTitre(STRING_TEST_MODIFIE);

        daoTesting.testModifierCRUD(meeting, meetingModifier, meetingModifier);
    }


    @Test
    public void supprimer() {
        Meeting meeting = this.createMeeting();

        daoTesting.testSupprimerCRUD(meeting);
    }

    private void creerUtilisateur() throws IllegalAccessException {
        Utilisateur utilisateur = new Utilisateur();
        utilisateur.setIdUtilisateur(LONG_TEST_UTILISATEUR);
        utilisateur.setEmail("test@test.fr");
        utilisateur.setHash("nope");
        utilisateur.setIdentifiant("testuser");
        utilisateur.setNom("TEST");
        utilisateur.setPrenom("TEST");
        utilisateur.setValide("oui");

        daoTesting.createObjectInDatabase(utilisateur);
    }

    private Meeting createMeeting() {
        Meeting meeting = new Meeting();
        meeting.setIdReunion(LONG_TEST_REUNION);
        meeting.setTitre(STRING_TEST);
        meeting.setDescription(STRING_TEST);
        meeting.setDate(STRING_TEST_DATE);
        meeting.setLieu(STRING_TEST);
        meeting.setCompteRendu(STRING_TEST);
        meeting.setRefUtilisateur(LONG_TEST_UTILISATEUR);

        return meeting;
    }

    @Test
    public void lister() throws SQLException, IllegalAccessException {
        Meeting[] meetings = new Meeting[2];
        Meeting[] meetings1 = new Meeting[2];

        for (int i = 0; i < meetings.length; i++) {

            meetings[i] = new Meeting();
            meetings[i].setIdReunion(LONG_TEST_REUNION + i);
            meetings[i].setTitre(STRING_TEST);
            meetings[i].setDescription(STRING_TEST);
            meetings[i].setDate(STRING_TEST_DATE);
            meetings[i].setLieu(STRING_TEST);
            meetings[i].setCompteRendu(STRING_TEST);
            meetings[i].setRefUtilisateur(LONG_TEST_UTILISATEUR);

            meetings1[i] = new Meeting();
            meetings1[i].setIdReunion(LONG_TEST_REUNION + i);
            meetings1[i].setTitre(STRING_TEST);
            meetings1[i].setDescription(STRING_TEST);
            meetings1[i].setDate(STRING_TEST_DATE);
            meetings1[i].setLieu(STRING_TEST);
            meetings1[i].setCompteRendu(STRING_TEST);
            meetings1[i].setRefUtilisateur(LONG_TEST_UTILISATEUR);

        }

        daoTesting.testListerCRUD(meetings, meetings1);
    }

    /**
     * Teste la méthode public void creer(Meeting meeting).
     * <p>Cas d'une erreur lors de la création de la connexion.</p>
     *
     * @throws SQLException
     */
    @Test
    public void test1_2CreerLog() throws SQLException {
        /* Configuraton du logger */
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        Layout layout = new SimpleLayout();
        Appender appender = new WriterAppender(layout, out);
        logger.addAppender(appender);

        try {
            /* Mock de la méthode creerConnexion() */
            MeetingDAO meetingDAOMock = EasyMock.createMockBuilder(MeetingDAO.class).addMockedMethod("creerConnexion")
                    .createMock();
            expect(meetingDAOMock.creerConnexion()).andThrow(new SQLException());

            replay(meetingDAOMock);

            /* Insertion du bean dans la BDD */
            meetingDAOMock.creer(meeting);

            /* Vérification du logging */
            String messageLog = out.toString();
            assertTrue("Mauvais logging", messageLog.contains("Échec de la création de l'objet"));

            verify(meetingDAOMock);
        } finally {
            /* Configuraton initiale du logger */
            logger.removeAppender(appender);
        }
    }


    /**
     * Teste la méthode public List<Meeting> trouver(Meeting meeting).
     * <p>Cas de la recherche d'un bean sans attributs.</p>
     */
    @Test
    public void test2_2TrouverLog() {
        /* Configuraton du logger */
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        Layout layout = new SimpleLayout();
        Appender appender = new WriterAppender(layout, out);
        logger.addAppender(appender);

        try {
            /* Recherche du bean sans attributs dans la BDD */
            Meeting meeting = new Meeting();
            meetingDAO.trouver(meeting);

            /* Vérification du logging */
            String messageLog = out.toString();
            assertTrue("Mauvais logging", messageLog.contains("Échec de la recherche de l'objet. "));
        } finally {
            /* Configuraton initiale du logger */
            logger.removeAppender(appender);
        }
    }


    /**
     * Teste la méthode public List<Meeting> trouver(Long idMeeting).
     * <p>Cas de la recherche d'un bean sans attributs.</p>
     */
    @Test
    public void test2_2_2TrouverIdMeetingLog() {
        /* Configuraton du logger */
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        Layout layout = new SimpleLayout();
        Appender appender = new WriterAppender(layout, out);
        logger.addAppender(appender);

        try {
            /* Recherche du bean sans attributs dans la BDD */
            Long idMeeting = null;
            meetingDAO.trouver(idMeeting);

            /* Vérification du logging */
            String messageLog = out.toString();
            assertTrue("Mauvais logging", messageLog.contains("Échec de la recherche"));
        } finally {
            /* Configuraton initiale du logger */
            logger.removeAppender(appender);
        }
    }

    /**
     * Teste la méthode public List<Meeting> trouverMeetingUtilisateur(Utilisateur utilisateur).
     * <p>Cas de la recherche d'un bean sans attributs.</p>
     */
    @Test
    public void test2_3_2TrouverMeetingUtilisateur() {
        /* Configuraton du logger */
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        Layout layout = new SimpleLayout();
        Appender appender = new WriterAppender(layout, out);
        logger.addAppender(appender);

        try {
            /* Recherche du bean sans attributs dans la BDD */
            Utilisateur utilisateur = new Utilisateur();
            meetingDAO.trouverMeetingUtilisateur(utilisateur);

            /* Vérification du logging */
            String messageLog = out.toString();
            assertTrue("Mauvais logging", messageLog.contains("Échec de la recherche des meetings invités par un utilisateur."));
        } finally {
            /* Configuraton initiale du logger */
            logger.removeAppender(appender);
        }
    }

    /**
     * Teste la méthode public List<Meeting> lister().
     * <p>Cas d'une erreur lors de la création de la connexion.</p>
     *
     * @throws SQLException
     */
    @Test
    public void test4_2ListerLog() throws SQLException {
        /* Configuraton du logger */
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        Layout layout = new SimpleLayout();
        Appender appender = new WriterAppender(layout, out);
        logger.addAppender(appender);

        try {
            /* Mock de la méthode creerConnexion() */
            MeetingDAO meetingDAOMock = EasyMock.createMockBuilder(MeetingDAO.class).addMockedMethod("creerConnexion")
                    .createMock();
            expect(meetingDAOMock.creerConnexion()).andThrow(new SQLException());

            replay(meetingDAOMock);

            /* Recherche du bean dans la BDD */
            meetingDAOMock.lister();

            /* Vérification du logging */
            String messageLog = out.toString();
            assertTrue("Mauvais logging", messageLog.contains("Échec du listage"));

            verify(meetingDAOMock);
        } finally {
            /* Configuraton initiale du logger */
            logger.removeAppender(appender);
        }
    }

    /**
     * Teste la méthode public void supprimer(Meeting meeting).
     * <p>Cas de la suppression d'un bean non présent dans la BDD.</p>
     */
    @Test
    public void test5_2SupprimerLog() {
        /* Configuraton du logger */
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        Layout layout = new SimpleLayout();
        Appender appender = new WriterAppender(layout, out);
        logger.addAppender(appender);
        try {

            Meeting meeting = this.createMeeting();
            /* Suppression du bean dans la BDD */
            meetingDAO.supprimer(meeting);
            /* Vérification du logging */
            String messageLog = out.toString();
            assertTrue("Mauvais logging ", messageLog.contains("Échec de la suppression"));
        } finally {
            /* Configuraton initiale du logger */
            logger.removeAppender(appender);
        }
    }

}
