package fr.eseo.ld.dao;

import fr.eseo.ld.beans.Professeur;
import fr.eseo.ld.beans.Utilisateur;
import fr.eseo.ld.pgl.beans.*;
import fr.eseo.testing.DAOTesting;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.sql.SQLException;

/**
 * Classe de test de CompositionJuryDAO
 *
 * @author Tristan LE GACQUE
 * @see CompositionJuryDAO
 **/
public class CompositionJuryDAOTest {

    private static DAOTesting<CompositionJury> daoTesting;

    /**
     * Définition du DaoTesting
     *
     * @throws Exception exception
     */
    @BeforeClass
    public static void setUp() throws Exception {
        //Définition du dao factory
        daoTesting = new DAOTesting<>();
        daoTesting.initDao(daoTesting.getDaoFactory().getCompositionJuryDAO());

        daoTesting.addAlias(Etudiant.class, "pgl_etudiant");
        daoTesting.addAlias(Soutenance.class, "pgl_soutenance");
        daoTesting.addAlias(CompositionJury.class, "pgl_compositionJury");

        daoTesting.truncateTable(Utilisateur.class);
        daoTesting.truncateTable(Professeur.class);
        daoTesting.truncateTable(Soutenance.class);
    }

    @AfterClass
    public static void tearDown() throws Exception {
        daoTesting.truncateUsedTables();
    }

    @Before
    public void beforeTest() throws SQLException, IllegalAccessException {
        daoTesting.truncateUsedTables();

        //Définition des dépendances
        Utilisateur user = daoTesting.getUtilisateur();
        Professeur prof = daoTesting.getProfesseur();
        Soutenance soutenance = daoTesting.getSoutenancePGL();

        //Ajout des objets a la base de données
        daoTesting.createObjectInDatabase(user);
        daoTesting.createObjectInDatabase(prof);
        daoTesting.createObjectInDatabase(soutenance);
    }


    /**
     * Teste la méthode public void creer.
     *
     * @see CompositionJuryDAO#creer(CompositionJury)
     */
    @Test
    public void testCreer() {
        /* Insertion du bean dans la BDD */
        CompositionJury compositionjury = daoTesting.getCompositionJury();
        CompositionJury compositionjuryAttendu = daoTesting.getCompositionJury();

        daoTesting.testCreerCRUD(compositionjury, compositionjuryAttendu);
    }

    /**
     * Teste la méthode public trouver
     *
     * @see CompositionJuryDAO#trouver(CompositionJury)
     */
    @Test
    public void testTrouver() {
        /* Recherche du bean dans la BDD */
        CompositionJury compositionjury = daoTesting.getCompositionJury();
        CompositionJury compositionjuryAttendu = daoTesting.getCompositionJury();

        daoTesting.testTrouverCRUD(compositionjury, compositionjuryAttendu);
    }


    /**
     * Teste la méthode public void modifier
     *
     * @see CompositionJuryDAO#modifier(CompositionJury)
     */
    @Test
    public void testModifier() {
        CompositionJury compositionjury = daoTesting.getCompositionJury();
        CompositionJury compositionjuryMod = daoTesting.getCompositionJury();

        daoTesting.testModifierCRUD(compositionjury, compositionjuryMod, compositionjuryMod);
    }

    /**
     * Teste la méthode public lister().
     *
     * @see CompositionJuryDAO#lister()
     */
    @Test
    public void testLister() {
        CompositionJury compositionjury = daoTesting.getCompositionJury();
        CompositionJury compositionjuryAttendu = daoTesting.getCompositionJury();

        daoTesting.testListerCRUD(new CompositionJury[]{compositionjury}, new CompositionJury[]{compositionjuryAttendu});
    }

    /**
     * Teste la méthode public void supprimer(NoteInteretTechno NoteInteretTechno).
     *
     * @see CompositionJuryDAO#supprimer(CompositionJury)
     */
    @Test
    public void testSupprimer() {
        CompositionJury compositionjury = daoTesting.getCompositionJury();

        daoTesting.testSupprimerCRUD(compositionjury);
    }

    /**
     * Test les erreurs
     */
    @Test
    public void testLogging() {
        daoTesting.testLoggingCRUD();
    }
}