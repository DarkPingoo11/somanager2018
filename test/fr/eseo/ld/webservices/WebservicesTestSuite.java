package fr.eseo.ld.webservices;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

/**
 * Suite de tests unitaires JUnit 4 pour le package fr.eseo.ld.webservices.
 * 
 * @author Maxime LENORMAND
 * 
 * @see org.junit
 */
@RunWith(Suite.class)
@SuiteClasses({
//	MethodesWebServiceTest.class
})
public class WebservicesTestSuite {
	/* Classe vide */
}