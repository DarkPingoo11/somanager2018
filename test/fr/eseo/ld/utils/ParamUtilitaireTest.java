package fr.eseo.ld.utils;

import static org.junit.Assert.*;

import net.codebox.javabeantester.JavaBeanTester;

import java.beans.IntrospectionException;

import org.junit.Test;

public class ParamUtilitaireTest {

    @Test
    public void testBean() throws IntrospectionException {
        JavaBeanTester.test(ParamUtilitaire.class);
    }

    @Test
    public void getInt() {
        Integer i = 12;
        assertEquals(i, ParamUtilitaire.getInt("12"));
    }

    @Test
    public void getInt2() {
        Integer i = 12;
        assertEquals(i, ParamUtilitaire.getInt(i+""));
    }

    @Test
    public void getIntNull() {
        assertNull(ParamUtilitaire.getInt(""));
    }

    @Test
    public void getIntException() {
        assertNull(ParamUtilitaire.getInt(""));
    }

    @Test
    public void notNull() {
        assertTrue("Les objets sont nulls", ParamUtilitaire.notNull("a", new Object(), 12));
    }

    @Test
    public void notNull2() {
        assertTrue("Les objets ne sont pas nulls", !ParamUtilitaire.notNull("a", new Object(), null));
    }
}