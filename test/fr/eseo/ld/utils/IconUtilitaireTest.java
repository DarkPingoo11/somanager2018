package fr.eseo.ld.utils;

import static org.junit.Assert.*;

import fr.eseo.ld.beans.OptionESEO;
import fr.eseo.ld.beans.Role;
import net.codebox.javabeantester.JavaBeanTester;

import java.beans.IntrospectionException;

import org.junit.Test;

public class IconUtilitaireTest {

    @Test
    public void getIconForOptionESEO() {
        String[] options = new String[]{"LD", "SE", "OC", "BIO", "NRJ", "CC", "IIT", "DSMT", "BD", "TOUT", "NOPE"};
        String[] icones = new String[]{"desktop", "car", "microchip", "heartbeat", "lightbulb", "cloud", "sitemap",
                "rss", "database", "asterisk", "question"};

        for(int i = 0; i < options.length; i++) {
            OptionESEO option = new OptionESEO();
            option.setNomOption(options[i]);
            assertEquals("Mauvais icone", icones[i], IconUtilitaire.getIconForOptionESEO(option));
        }
    }

    @Test
    public void getIconForRole() {
        int[] roles = new int[]{1,2,3,4,5,6,9,10,2000};
        String[] icones = new String[]{"graduation-cap", "key", "user", "user", "user-secret", "hands-helping", "briefcase",
                "comments", "question"};

        for(int i = 0; i < roles.length; i++) {
            Role role = new Role();
            role.setIdRole((long)roles[i]);
            assertEquals("Mauvais icone", icones[i], IconUtilitaire.getIconForRole(role));
        }
    }
}