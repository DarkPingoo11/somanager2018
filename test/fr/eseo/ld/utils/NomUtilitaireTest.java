package fr.eseo.ld.utils;

import fr.eseo.ld.beans.OptionESEO;
import fr.eseo.ld.beans.Role;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class NomUtilitaireTest {

    @Test
    public void getNomCompletRole() {
        int[] roles = new int[]{1,2,3,4,5,6,9,10,2000};
        String[] options = new String[]{"LD","", "SE","BIO", "NRJ", "", "", "", ""};
        String[] noms = new String[]{"Étudiant LD", "Administrateur", "Professeur SE", "Professeur d'option BIO",
                "Professeur responsable NRJ", "Assistant", "Entreprise Extérieure", "Service Communication",
                null};

        for(int i = 0; i < roles.length; i++) {
            Role role = new Role();
            OptionESEO option = new OptionESEO();
            role.setIdRole((long)roles[i]);
            option.setNomOption(options[i]);
            assertEquals("Mauvais nom", noms[i], NomUtilitaire.getNomCompletRole(role, option));
        }
    }

}