package fr.eseo.ld.utils;

import static org.junit.Assert.*;

import fr.eseo.ld.beans.Utilisateur;
import net.codebox.javabeantester.JavaBeanTester;

import java.beans.IntrospectionException;

import org.junit.Test;

public class UtilisateurNameComparatorTest {

    @Test
    public void compareAvant() {
        Utilisateur u1 = new Utilisateur();
        Utilisateur u2 = new Utilisateur();

        u1.setNom("AUTON");
        u1.setPrenom("Paul");

        u2.setNom("BERTIER");
        u2.setPrenom("Pierre");

        UtilisateurNameComparator comparator = new UtilisateurNameComparator();

        assertTrue(comparator.compare(u1, u2) < 0);
    }

    @Test
    public void compareApres() {
        Utilisateur u1 = new Utilisateur();
        Utilisateur u2 = new Utilisateur();

        u1.setNom("MERAND");
        u1.setPrenom("Céline");

        u2.setNom("BERTIER");
        u2.setPrenom("Pierre");

        UtilisateurNameComparator comparator = new UtilisateurNameComparator();

        assertTrue(comparator.compare(u1, u2) > 0);
    }

    @Test
    public void compareEgal() {
        Utilisateur u1 = new Utilisateur();
        Utilisateur u2 = new Utilisateur();

        u1.setNom("BERTIER");
        u1.setPrenom("Pierre");

        u2.setNom("BERTIER");
        u2.setPrenom("Pierre");

        UtilisateurNameComparator comparator = new UtilisateurNameComparator();

        assertEquals(0, comparator.compare(u1, u2));
    }
}