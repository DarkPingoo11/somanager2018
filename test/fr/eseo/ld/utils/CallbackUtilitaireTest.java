package fr.eseo.ld.utils;

import static org.easymock.EasyMock.*;
import static org.junit.Assert.*;

import com.google.gson.JsonObject;
import fr.eseo.ld.beans.Callback;
import fr.eseo.ld.beans.CallbackType;
import net.codebox.javabeantester.JavaBeanTester;

import java.beans.IntrospectionException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;

import org.easymock.*;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RunWith(EasyMockRunner.class)
public class CallbackUtilitaireTest extends EasyMockSupport {

    private static final String ATT_CALLBACK_MESSAGE = "callback";
    private static final String TEST_CALLBACK_MESSAGE = "Un message de callback";
    private static final String TEST_CALLBACK_MESSAGE2 = "Un message de callback 2";
    private static final String TEST_CALLBACK_MESSAGE3 = "Un message de callback 3";

    @Mock
    private HttpServletRequest request;

    @Mock
    private HttpServletResponse response;


    @Test
    public void setCallback() {
        Capture<String> captureArg = Capture.newInstance();
        Capture<Callback[]> captureMethod = Capture.newInstance();
        request.setAttribute(EasyMock.capture(captureArg), EasyMock.capture(captureMethod));


        //Start
        this.replayAll();

        Callback c = new Callback(TEST_CALLBACK_MESSAGE);
        CallbackUtilitaire.setCallback(request, c);

        this.verifyAll();

        //Vérification du tableau
        for(Callback captured : captureMethod.getValue()) {
            assertEquals("Le callback n'a pas bien été set", captured, c);
        }

        assertEquals("Mauvaise définition d'attribut", ATT_CALLBACK_MESSAGE, captureArg.getValue());
    }

    @Test
    public void addCallbackNull() {
        Capture<String> captureArg = Capture.newInstance();
        Capture<Callback[]> captureMethod = Capture.newInstance();

        expect(request.getAttribute(ATT_CALLBACK_MESSAGE)).andReturn(null);
        request.setAttribute(EasyMock.capture(captureArg), EasyMock.capture(captureMethod));

        //Start
        this.replayAll();

        Callback c = new Callback(TEST_CALLBACK_MESSAGE);
        CallbackUtilitaire.addCallback(request, c);

        this.verifyAll();

        //Vérification du tableau
        for(Callback captured : captureMethod.getValue()) {
            assertEquals("Le callback n'a pas bien été set", captured, c);
        }

        assertEquals("Mauvaise définition d'attribut", ATT_CALLBACK_MESSAGE, captureArg.getValue());
    }

    @Test
    public void addCallbackNotNull() {
        Capture<String> captureArg = Capture.newInstance();
        Capture<Callback[]> captureMethod = Capture.newInstance();
        Callback c = new Callback(TEST_CALLBACK_MESSAGE);
        Callback c1 = new Callback(TEST_CALLBACK_MESSAGE2);
        Callback c2 = new Callback(TEST_CALLBACK_MESSAGE3);

        Callback[] callbacks = new Callback[]{c};

        expect(request.getAttribute(ATT_CALLBACK_MESSAGE)).andReturn(callbacks).times(2);
        request.setAttribute(EasyMock.capture(captureArg), EasyMock.capture(captureMethod));

        //Start
        this.replayAll();

        CallbackUtilitaire.addCallback(request, c1, c2);

        this.verifyAll();

        //Vérification du tableau
        ArrayList<Callback> tableau = new ArrayList<>(Arrays.asList(captureMethod.getValue()));
        ArrayList<Callback> existant = new ArrayList<>(Arrays.asList(c1, c2));

        for(Callback callback : existant) {
            assertTrue("Les callbacks n'ont pas bien été ajoutés", tableau.contains(c));
        }

        assertEquals("Mauvaise définition d'attribut", ATT_CALLBACK_MESSAGE, captureArg.getValue());
    }

    @Test
    public void sendJsonResponse() throws IOException {

        //Définition du json
        Callback c = new Callback(CallbackType.SUCCESS, TEST_CALLBACK_MESSAGE);
        JsonObject json = c.getJsonObject();
        String jsonExpected = "{\"type\":\"success\",\"message\":\"Un message de callback\"}";

        PrintWriter writer = mock(PrintWriter.class);
        Capture<String> jsonCaptured = Capture.newInstance();

        response.setContentType("application/json");
        expect(response.getWriter()).andReturn(writer);

        writer.print(capture(jsonCaptured));
        writer.flush();

        //Démarrage du mock
        this.replayAll();
        CallbackUtilitaire.sendJsonResponse(response, c);
        assertEquals("Les objets json ne sont pas égaux", jsonExpected, jsonCaptured.getValue());
        this.verifyAll();
    }
}