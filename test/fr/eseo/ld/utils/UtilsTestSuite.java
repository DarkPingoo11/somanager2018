package fr.eseo.ld.utils;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

/**
 * Suite de tests unitaires JUnit 4
 *
 * @author Tristan LE GACQUE
 */
@RunWith(Suite.class)
@SuiteClasses({
        CallbackUtilitaireTest.class,
        PixelUtilitaireTest.class,
        IconUtilitaireTest.class,
        NomUtilitaireTest.class,
        ParamUtilitaireTest.class,
        UtilisateurNameComparatorTest.class
})
public class UtilsTestSuite {
} 


