package fr.eseo.ld.utils;

import static org.junit.Assert.*;

import net.codebox.javabeantester.JavaBeanTester;

import java.beans.IntrospectionException;

import org.junit.Test;

public class PixelUtilitaireTest {

    private static final int TEST_PIXELS = 200;
    private static final short TEST_WIDTH = 7315;
    private static final short TEST_HEIGHT = 4000;

    @Test
    public void pixel2WidthUnits() {
        int width = PixelUtilitaire.pixel2WidthUnits(TEST_PIXELS);
        assertEquals("La conversion est mauvaise", 7314, width);
    }

    @Test
    public void widthUnits2Pixel() {
        int pixels = PixelUtilitaire.widthUnits2Pixel(TEST_WIDTH);
        assertEquals("La conversion est mauvaise", 200, pixels);
    }

    @Test
    public void heightUnits2Pixel() {
        int pixels = PixelUtilitaire.heightUnits2Pixel(TEST_HEIGHT);
        assertEquals("La conversion est mauvaise", 200, pixels);
    }
}