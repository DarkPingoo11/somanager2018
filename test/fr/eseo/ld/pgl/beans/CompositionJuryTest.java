package fr.eseo.ld.pgl.beans;

import net.codebox.javabeantester.JavaBeanTester;
import org.junit.Test;

import java.beans.IntrospectionException;

/**
 * Classe de test de CompositionJuryDAO
 *
 * @author Tristan LE GACQUE
 * @see CompositionJury
 **/
public class CompositionJuryTest {

    @Test
    public void testBean() throws IntrospectionException {
        JavaBeanTester.test(CompositionJury.class);
    }
}