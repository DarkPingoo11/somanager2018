package fr.eseo.ld.pgl.beans;

import net.codebox.javabeantester.JavaBeanTester;
import org.junit.Test;

import java.beans.IntrospectionException;

/**
 * Classe de test de RoleEquipeDAO
 *
 * @author Tristan LE GACQUE
 * @see RoleEquipe
 **/
public class RoleEquipeTest {

    @Test
    public void testBean() throws IntrospectionException {
        JavaBeanTester.test(RoleEquipe.class);
    }
}