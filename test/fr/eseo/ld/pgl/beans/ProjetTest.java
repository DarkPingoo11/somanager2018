package fr.eseo.ld.pgl.beans;

import net.codebox.javabeantester.JavaBeanTester;
import org.junit.Test;

import java.beans.IntrospectionException;

/**
 * Classe de test de ProjetDAO
 *
 * @author Tristan LE GACQUE
 * @see Projet
 **/
public class ProjetTest {

    @Test
    public void testBean() throws IntrospectionException {
        JavaBeanTester.test(Projet.class);
    }
}