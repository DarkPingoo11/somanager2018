package fr.eseo.ld.pgl.beans;

import net.codebox.javabeantester.JavaBeanTester;
import org.junit.Test;

import java.beans.IntrospectionException;

/**
 * Classe de test de EvalueDAO
 *
 * @author Tristan LE GACQUE
 * @see Evalue
 **/
public class EvalueTest {

    @Test
    public void testBean() throws IntrospectionException {
        JavaBeanTester.test(Evalue.class);
    }
}