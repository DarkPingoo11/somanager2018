package fr.eseo.ld.pgl.beans;

import net.codebox.javabeantester.JavaBeanTester;
import org.junit.Test;

import java.beans.IntrospectionException;

import static junit.framework.TestCase.assertEquals;

/**
 * Classe de test de SprintDAO
 *
 * @author Tristan LE GACQUE
 * @see Sprint
 **/
public class SprintTest {

    private static final Long LONG_TEST = 1L;
    private static final String DATE = "2018-09-03";
    private static final Integer NBR_HEURES = 5;
    private static final Long REF_ANNEE = 2L;
    private static final Float COEFFICIENT = 0.F;


    @Test
    public void testBean() throws IntrospectionException {
        JavaBeanTester.test(Sprint.class);
    }

    /**
     * Teste la méthode public String toString().
     */
    @Test
    public void testToString() {
        final Sprint sprint = new Sprint();
        sprint.setIdSprint(LONG_TEST);
        sprint.setDateDebut(DATE);
        sprint.setDateFin(DATE);
        sprint.setNbrHeures(NBR_HEURES);
        sprint.setRefAnnee(REF_ANNEE);
        sprint.setCoefficient(COEFFICIENT)
        ;

        final String resultatAttendu = "Sprint{" +
                "idSprint=" + LONG_TEST +
                ", dateDebut=" + DATE +
                ", dateFin=" + DATE +
                ", nbrHeures=" + NBR_HEURES +
                ", coefficient=" + COEFFICIENT +
                ", refAnnee=" + REF_ANNEE +
                '}';
        final String resultatTrouve = sprint.toString();

        assertEquals("Mauvais toString()", resultatAttendu, resultatTrouve);
    }
}