package fr.eseo.ld.pgl.beans;

import net.codebox.javabeantester.JavaBeanTester;
import org.junit.Test;

import java.beans.IntrospectionException;

/**
 * Classe de test de NoteDAO
 *
 * @author Tristan LE GACQUE
 * @see Note
 **/
public class NoteTest {

    @Test
    public void testBean() throws IntrospectionException {
        JavaBeanTester.test(Note.class);
    }
}