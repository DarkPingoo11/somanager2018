package fr.eseo.ld.pgl.beans;

import net.codebox.javabeantester.JavaBeanTester;
import org.junit.Test;

import java.beans.IntrospectionException;
import java.lang.reflect.Field;

import static junit.framework.TestCase.assertNull;
import static junit.framework.TestCase.assertTrue;

/**
 * Classe de test de BonusMalusDAO
 *
 * @author Tristan LE GACQUE
 * @see BonusMalus
 **/
public class BonusMalusTest {

    @Test
    public void testBean() throws IntrospectionException {
        JavaBeanTester.test(BonusMalus.class, "validation");
    }

    @Test
    public void testValidationGet() throws NoSuchFieldException, IllegalAccessException {
        BonusMalus bonusMalus = new BonusMalus();
        Field f = bonusMalus.getClass().getDeclaredField("validation");
        f.setAccessible(true);
        f.set(bonusMalus, "accepter");

        assertTrue("Erreur pour la properties 'validation'", bonusMalus.getValidation().equalsIgnoreCase("accepter"));
    }

    @Test
    public void testValidationSet() throws NoSuchFieldException, IllegalAccessException {
        BonusMalus bonusMalus = new BonusMalus();
        bonusMalus.setValidation(BonusMalus.VALIDATION_TYPE.REFUSER);

        Field f = bonusMalus.getClass().getDeclaredField("validation");
        f.setAccessible(true);

        String validation = f.get(bonusMalus).toString();

        assertTrue("Erreur pour la properties 'validation'", validation.equalsIgnoreCase("refuser"));
    }

    @Test
    public void testValidationSet2() throws NoSuchFieldException, IllegalAccessException {
        BonusMalus bonusMalus = new BonusMalus();
        bonusMalus.setValidation("refuser");

        Field f = bonusMalus.getClass().getDeclaredField("validation");
        f.setAccessible(true);

        String validation = f.get(bonusMalus).toString();

        assertTrue("Erreur pour la properties 'validation'", validation.equalsIgnoreCase("refuser"));
    }

    @Test
    public void testValidationSet3() throws NoSuchFieldException, IllegalAccessException {
        BonusMalus bonusMalus = new BonusMalus();
        bonusMalus.setValidation("Incqdqdkojqsodjqos");

        Field f = bonusMalus.getClass().getDeclaredField("validation");
        f.setAccessible(true);

        assertNull("Erreur pour la properties 'validation'", f.get(bonusMalus));
    }
}