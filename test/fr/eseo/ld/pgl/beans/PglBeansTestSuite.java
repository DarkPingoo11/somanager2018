package fr.eseo.ld.pgl.beans;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

/**
 * Suite de tests unitaires JUnit 4 pour le package fr.eseo.ld.beans.
 *
 * @author Tristan LE GACQUE
 *
 * @see org.junit
 */
@RunWith(Suite.class)
@SuiteClasses({
		BonusMalusTest.class,
		CompositionJuryTest.class,
		EquipeTest.class,
		EtudiantTest.class,
		EtudiantEquipeTest.class,
		EvalueTest.class,
		MatiereTest.class,
		NoteTest.class,
		ProjetTest.class,
		RoleEquipeTest.class,
		SoutenanceTest.class,
		SprintTest.class
})
public class PglBeansTestSuite {
	/* Classe vide */
}