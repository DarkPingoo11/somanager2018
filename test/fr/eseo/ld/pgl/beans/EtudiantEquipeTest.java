package fr.eseo.ld.pgl.beans;

import net.codebox.javabeantester.JavaBeanTester;
import org.junit.Test;

import java.beans.IntrospectionException;

/**
 * Classe de test de EtudiantEquipeDAO
 *
 * @author Tristan LE GACQUE
 * @see EtudiantEquipe
 **/
public class EtudiantEquipeTest {

    @Test
    public void testBean() throws IntrospectionException {
        JavaBeanTester.test(EtudiantEquipe.class);
    }
}