package fr.eseo.ld.pgl.beans;

import net.codebox.javabeantester.JavaBeanTester;
import org.junit.Test;

import java.beans.IntrospectionException;

/**
 * Classe de test de MatiereDAO
 *
 * @author Tristan LE GACQUE
 * @see Matiere
 **/
public class MatiereTest {

    @Test
    public void testBean() throws IntrospectionException {
        JavaBeanTester.test(Matiere.class);
    }
}