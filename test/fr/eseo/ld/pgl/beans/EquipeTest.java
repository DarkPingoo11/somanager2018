package fr.eseo.ld.pgl.beans;

import net.codebox.javabeantester.JavaBeanTester;
import org.junit.Test;

import java.beans.IntrospectionException;

/**
 * Classe de test de EquipeDAO
 *
 * @author Tristan LE GACQUE
 * @see Equipe
 **/
public class EquipeTest {

    @Test
    public void testBean() throws IntrospectionException {
        JavaBeanTester.test(Equipe.class);
    }
}