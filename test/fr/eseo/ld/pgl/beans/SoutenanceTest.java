package fr.eseo.ld.pgl.beans;

import net.codebox.javabeantester.JavaBeanTester;
import org.junit.Test;

import java.beans.IntrospectionException;

/**
 * Classe de test de SoutenanceDAO
 *
 * @author Tristan LE GACQUE
 * @see Soutenance
 **/
public class SoutenanceTest {

    @Test
    public void testBean() throws IntrospectionException {
        JavaBeanTester.test(Soutenance.class);
    }
}