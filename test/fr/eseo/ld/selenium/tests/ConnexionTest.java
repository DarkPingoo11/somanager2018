package fr.eseo.ld.selenium.tests;

import static fr.eseo.ld.selenium.SeleniumUtilitaire.initialiserDriver;
import static org.junit.Assert.assertEquals;

import java.util.ResourceBundle;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.WebDriver;

import fr.eseo.ld.selenium.pages.Accueil;
import fr.eseo.ld.selenium.pages.Connexion;

/**
 * Classe de tests fonctionnels Selenium de la Connexion d'un utilisateur.
 * 
 * <p>Utilisation du Page Model Object et du Page Factory Design Pattern.</p>
 * 
 * @version 1.0
 * @author Maxime LENORMAND
 * 
 * @see org.openqa.selenium
 * @see org.junit
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ConnexionTest {

	/* Accès au fichier properties */
	private static final ResourceBundle PROPERTIES = ResourceBundle.getBundle("fr.eseo.ld.selenium.selenium");

	private static final String PROPERTY_ADRESSE_ACCUEIL = "adresseAccueil";
	private static final String PROPERTY_TITRE_ACCUEIL = "titreAccueil";
	private static final String PROPERTY_TITRE_DASHBOARD = "titreDashboard";
	private static final String PROPERTY_TITRE_CONNEXION = "titreConnexion";
	private static final String PROPERTY_IDENTIFIANT_CONNEXION = "identifiantEtudiant";
	private static final String PROPERTY_MDP_CONNEXION = "motDePasseEtudiant";

	private static WebDriver driver;

	private Accueil accueil;
	private Connexion connexion;

	/**
	 * Initialise le driver.
	 * 
	 * @throws Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		/* Initialisation du driver */
		driver = initialiserDriver(driver);
		/* Pré-conditions aux tests */
		driver.get(PROPERTIES.getString(PROPERTY_ADRESSE_ACCUEIL));
	}

	/**
	 * Ferme le navigateur.
	 * 
	 * @throws Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		/* Fermeture du navigateur */
		driver.quit();
	}

	@Test
	public void test1OuvirPageConnexion() {
		this.accueil = new Accueil(driver);
		this.accueil.cliquerConnexion();
		this.connexion = this.accueil.seConnecterErreur("azerty", "azerty");
		assertEquals(PROPERTIES.getString(PROPERTY_TITRE_CONNEXION), this.connexion.getTitrePage());
	}

	@Test
	public void test2SeConnecter() {
		this.connexion = new Connexion(driver);
		this.accueil = this.connexion.seConnecter(PROPERTIES.getString(PROPERTY_IDENTIFIANT_CONNEXION),
				PROPERTIES.getString(PROPERTY_MDP_CONNEXION));
		assertEquals(PROPERTIES.getString(PROPERTY_TITRE_DASHBOARD), this.accueil.getTitrePage());
	}

	@Test
	public void test3SeDeconnecter() {
		this.accueil = new Accueil(driver);
		this.accueil.cliquerConnexion();
		this.accueil.cliquerDeconnexion();
		assertEquals(PROPERTIES.getString(PROPERTY_TITRE_ACCUEIL), this.accueil.getTitrePage());
	}

}