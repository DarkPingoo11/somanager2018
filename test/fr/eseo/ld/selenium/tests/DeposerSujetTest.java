package fr.eseo.ld.selenium.tests;

import fr.eseo.ld.beans.Sujet;
import fr.eseo.ld.dao.DAOFactory;
import fr.eseo.ld.dao.SujetDAO;
import fr.eseo.ld.selenium.pages.Accueil;
import fr.eseo.ld.selenium.pages.DeposerSujet;
import org.junit.*;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import static fr.eseo.ld.selenium.SeleniumUtilitaire.initialiserDriver;
import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertEquals;

/**
 * Classe de tests fonctionnels Selenium du depot de sujet
 * 
 * <p>Utilisation du Page Model Object et du Page Factory Design Pattern.</p>
 * 
 * @version 1.0
 * @author Tristan LE GACQUE
 * 
 * @see org.openqa.selenium
 * @see org.junit
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class DeposerSujetTest {

	/* Accès au fichier properties */
	private static final ResourceBundle PROPERTIES = ResourceBundle.getBundle("fr.eseo.ld.selenium.selenium");

	private static final String PROPERTY_TEST = "test";
	private static final String PROPERTY_ADRESSE_ACCUEIL = "adresseAccueil";
	private static final String PROPERTY_TITRE_DEPOSER_SUJET = "titreDeposerSujet";
    private static final String PROPERTY_FICHIER_CSV = "fichierDepotCsv";
    private static final String PROPERTY_FICHIER_CSV_VIDE = "fichierDepotCsvVide";
	private static final String PROPERTY_IDENTIFIANT_ASSISTANT = "identifiantAssistant";
	private static final String PROPERTY_MDP_ASSISTANT = "motDePasseAssistant";
    private static final String PROPERTY_SUCCESS_DEPOT = "notifDepotSuccess";
    private static final String PROPERTY_ERROR_EMPTY_DEPOT = "notifDepotErrorVide";
	private static final String PROPERTY_SUCCESS_CHANGEMENT_LINK = "notifChangementSuccess";
	private static final String PROPERTY_ERROR_CHANGEMENT_LINK = "notifChangementError";


    private static WebDriver driver;
	private static SujetDAO sujetDAO;

	private Accueil accueil;
	private DeposerSujet deposerSujet;

	/**
	 * Initialise le driver et récupère une instance de DAO Sujet.
	 * 
	 * @throws Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		/* Initialisation du driver */
		driver = initialiserDriver(driver);
		/* Pré-conditions aux tests */
		driver.get(PROPERTIES.getString(PROPERTY_ADRESSE_ACCUEIL));
		/* Récupération d'une instance de DAO Utilisateur */
		sujetDAO = DAOFactory.getInstance().getSujetDao();
	}

	/**
	 * Ferme le navigateur et supprime le sujet inséré dans la BDD.
	 * 
	 * @throws Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		/* Fermeture du navigateur */
		driver.quit();
		/* Suppression du sujet inséré dans la BDD */
		final Sujet sujet = new Sujet();
		sujet.setTitre(PROPERTIES.getString(PROPERTY_TEST));
		sujetDAO.supprimer(sujet);
	}



	@Test
	public void test1OuvrirPageDepotSujet() {
		this.accueil = new Accueil(driver);
		this.accueil.cliquerConnexion();

		this.accueil.seConnecterSucces(PROPERTIES.getString(PROPERTY_IDENTIFIANT_ASSISTANT),
				PROPERTIES.getString(PROPERTY_MDP_ASSISTANT));
		this.accueil.cliquerSujet();
		this.deposerSujet = this.accueil.cliquerDeposerSujet();
		assertEquals(PROPERTIES.getString(PROPERTY_TITRE_DEPOSER_SUJET), this.deposerSujet.getTitrePage());
	}

	@Test(timeout=4000)
	public void test2DeposerSujetParcourir() {
		this.deposerSujet = new DeposerSujet(driver);

		this.deposerSujet.deposerSujet(PROPERTIES.getString(PROPERTY_FICHIER_CSV));
		String nomFichier = PROPERTIES.getString(PROPERTY_FICHIER_CSV);
		nomFichier = nomFichier.substring(nomFichier.lastIndexOf("/")+1);
		assertEquals(nomFichier, this.deposerSujet.getNomFichier());
	}

	@Test
	public void test3DeposerSujetEnvoyer() {
        this.deposerSujet = new DeposerSujet(driver);
        this.deposerSujet.envoyerSujet();


		WebElement alert = this.deposerSujet.getCallbackAlert();

		assertNotNull(alert);
		assertEquals(PROPERTIES.getString(PROPERTY_SUCCESS_DEPOT), alert.getText());

		//Vérifier l'envoi du sujet
        final Sujet sujet = new Sujet();
        sujet.setTitre(PROPERTIES.getString(PROPERTY_TEST));
        List<Sujet> sujets = DAOFactory.getInstance().getSujetDao().trouver(sujet);

        assertEquals("Le sujet n'est pas ajouté (0 au lieu de 1)", 1, sujets.size());
	}

    @Test
    public void test4DeposerSujetVide() {
        this.deposerSujet = new DeposerSujet(driver);

        this.deposerSujet.deposerSujet(PROPERTIES.getString(PROPERTY_FICHIER_CSV_VIDE));
        String nomFichier = PROPERTIES.getString(PROPERTY_FICHIER_CSV_VIDE);
        nomFichier = nomFichier.substring(nomFichier.lastIndexOf("/")+1);

        assertEquals(nomFichier, this.deposerSujet.getNomFichier());
    }

    @Test
    public void test5DeposerSujetVideEnvoyer() {
        this.deposerSujet = new DeposerSujet(driver);
        this.deposerSujet.envoyerSujet();

        WebElement alert = this.deposerSujet.getCallbackAlert();
        assertNotNull(alert);
        assertEquals(PROPERTIES.getString(PROPERTY_ERROR_EMPTY_DEPOT), alert.getText());
    }

    ///TESTS SUR MODIFICATION DE LIEN FORMULAIRE
/*
Test verification ouverture du'ne 2nd fenetre équivalente au formulaire
 */
	@Test
	public void test7CliquerLien(){
		this.deposerSujet = new DeposerSujet(driver);
		this.deposerSujet.cliquerLienFormulaire();
		String test = this.deposerSujet.getTitrePage();
		//Recupere le nombre de fenetres ouvertes
		// 1ere fenetre = SoManager et 2nd formulaire Google
		// test par rapport au nbr de fenetre car pas possible avec id etc si changement du lien de formulaire
		List<String> browserTabs = new ArrayList<String> (driver.getWindowHandles());
		int nombreFenetresOuvertes = browserTabs.size();
		assertEquals(nombreFenetresOuvertes, 2);
	}
	/*
    Test vérification de la modification du lien pour un lien valide
    test sur le callback
     */
	@Test
	public void test6ModifierLienValideFormulaire(){
		this.deposerSujet = new DeposerSujet(driver);
		String lienInseserDB = "https://google.com/";
		this.deposerSujet.cliquerFieldLien(lienInseserDB);
		WebElement alert = this.deposerSujet.getCallbackAlert();
		assertNotNull(alert);
		assertEquals(PROPERTIES.getString(PROPERTY_SUCCESS_CHANGEMENT_LINK), alert.getText());
	}
	/*
        Test vérification de la modification du lien pour un lien non valide
        test sur le callback
         */
	@Test
	public void test8ModifierLienNonValideFormulaire(){
		this.deposerSujet = new DeposerSujet(driver);
		this.deposerSujet.cliquerFieldLien("hp://google/");
		WebElement alert = this.deposerSujet.getCallbackAlert();
		assertNotNull(alert);
		assertEquals(PROPERTIES.getString(PROPERTY_ERROR_CHANGEMENT_LINK), alert.getText());
	}

	/*
            Test vérification de la modification du lien pour un lien non valide
             */
	@Test
	public void test9ModifierLien(){
		this.deposerSujet = new DeposerSujet(driver);
		String lienInseserDB = "https://google.com/";
		this.deposerSujet.cliquerFieldLien(lienInseserDB);
		WebElement alert = this.deposerSujet.getCallbackAlert();
		String lien = this.deposerSujet.getHrefValeur();
		System.out.println("lien "+lien);
		System.out.println("lienDB "+lienInseserDB);
		assertEquals(lien, lienInseserDB);
	}

}