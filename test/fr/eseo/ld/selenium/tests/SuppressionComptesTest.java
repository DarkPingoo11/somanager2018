package fr.eseo.ld.selenium.tests;

import static fr.eseo.ld.selenium.SeleniumUtilitaire.initialiserDriver;
import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import fr.eseo.ld.selenium.pages.SuppressionComptes;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.WebDriver;

import fr.eseo.ld.beans.Sujet;
import fr.eseo.ld.dao.DAOFactory;
import fr.eseo.ld.dao.SujetDAO;
import fr.eseo.ld.selenium.pages.Accueil;
import fr.eseo.ld.selenium.pages.SuppressionComptes;
import fr.eseo.ld.selenium.pages.SuppressionComptes;

public class SuppressionComptesTest {

	/* Accès au fichier properties */
	private static final ResourceBundle PROPERTIES = ResourceBundle.getBundle("fr.eseo.ld.selenium.selenium");

	private static final String PROPERTY_TEST = "test";
	private static final String PROPERTY_ADRESSE_ACCUEIL = "adresseAccueil";
	private static final String PROPERTY_TITRE_DEPOSER_SUJET = "titreSuppressionComptes";
	private static final String PROPERTY_IDENTIFIANT_ADMIN = "identifiantAdmin";
	private static final String PROPERTY_MDP_ADMIN = "motDePasseAdmin";
    private static final String PROPERTY_SUCCESS_SUPPRESSION = "notifSuppressionSuccess";
    private static final String PROPERTY_ERROR_EMPTY_SUPPRESSION = "notifSuppressionErrorVide";


    private static WebDriver driver;
	private static SujetDAO sujetDAO;

	private Accueil accueil;
	private SuppressionComptes suppressionComptes;

	/**
	 * Initialise le driver et récupère une instance de DAO Sujet.
	 * 
	 * @throws Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		/* Initialisation du driver */
		driver = initialiserDriver(driver);
		/* Pré-conditions aux tests */
		driver.get(PROPERTIES.getString(PROPERTY_ADRESSE_ACCUEIL));
	}
	
	@Test
	public void test1OuvrirPageSupprimerSujet() {
		this.accueil = new Accueil(driver);
		this.accueil.cliquerConnexion();
		this.accueil.seConnecterSucces(PROPERTIES.getString(PROPERTY_IDENTIFIANT_ADMIN),
				PROPERTIES.getString(PROPERTY_MDP_ADMIN));
		this.accueil.cliquerAdministrateur();
	}


}
