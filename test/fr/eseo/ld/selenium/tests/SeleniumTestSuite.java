package fr.eseo.ld.selenium.tests;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

/**
 * Suite de tests unitaires JUnit 4 pour le package fr.eseo.ld.selenium.tests.
 *
 * @author Maxime LENORMAND
 *
 * @see org.junit
 */
@RunWith(Suite.class)
@SuiteClasses({
		//Arret des tests séléniums, l'interface a trop changer pour les maintenir
		/*
		AjouterSujetTest.class,
		ConnexionTest.class,
		CreerEquipeTest.class,
		InscriptionTest.class,
		DeposerSujetTest.class,
		GererApplicationTest.class,
		GererComptesTest.class,
		SuppressionComptesTest.class
		*/
})
public class SeleniumTestSuite {
	/* Classe vide */
}