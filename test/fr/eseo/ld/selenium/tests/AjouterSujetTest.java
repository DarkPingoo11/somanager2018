package fr.eseo.ld.selenium.tests;

import static fr.eseo.ld.selenium.SeleniumUtilitaire.initialiserDriver;
import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.WebDriver;

import fr.eseo.ld.beans.Sujet;
import fr.eseo.ld.dao.DAOFactory;
import fr.eseo.ld.dao.SujetDAO;
import fr.eseo.ld.selenium.pages.Accueil;
import fr.eseo.ld.selenium.pages.AjouterSujet;

/**
 * Classe de tests fonctionnels Selenium de l'Ajout d'un sujet.
 * 
 * <p>Utilisation du Page Model Object et du Page Factory Design Pattern.</p>
 * 
 * @version 1.0
 * @author Maxime LENORMAND
 * 
 * @see org.openqa.selenium
 * @see org.junit
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class AjouterSujetTest {

	/* Accès au fichier properties */
	private static final ResourceBundle PROPERTIES = ResourceBundle.getBundle("fr.eseo.ld.selenium.selenium");

	private static final String PROPERTY_TEST = "test";
	private static final String PROPERTY_TEST_NBR_MIN_ELEVES = "1";
	private static final String PROPERTY_TEST_NBR_MAX_ELEVES = "5";
	private static final String PROPERTY_ADRESSE_ACCUEIL = "adresseAccueil";
	private static final String PROPERTY_TITRE_DASHBOARD = "titreDashboard";
	private static final String PROPERTY_TITRE_AJOUTER_SUJET = "titreAjouterSujet";
	private static final String PROPERTY_IDENTIFIANT_ETUDIANT = "identifiantEtudiant";
	private static final String PROPERTY_MDP_ETUDIANT = "motDePasseEtudiant";

	private static WebDriver driver;
	private static SujetDAO sujetDAO;

	private Accueil accueil;
	private AjouterSujet ajouterSujet;

	/**
	 * Initialise le driver et récupère une instance de DAO Sujet.
	 * 
	 * @throws Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		/* Initialisation du driver */
		driver = initialiserDriver(driver);
		/* Pré-conditions aux tests */
		driver.get(PROPERTIES.getString(PROPERTY_ADRESSE_ACCUEIL));
		/* Récupération d'une instance de DAO Utilisateur */
		sujetDAO = DAOFactory.getInstance().getSujetDao();
	}

	/**
	 * Ferme le navigateur et supprime le sujet inséré dans la BDD.
	 * 
	 * @throws Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		/* Fermeture du navigateur */
		driver.quit();
		/* Suppression de l'utilisateur inséré dans la BDD */
		final Sujet sujet = new Sujet();
		sujet.setTitre(PROPERTIES.getString(PROPERTY_TEST));
		sujetDAO.supprimer(sujet);
	}

	@Test
	public void test1OuvrirPageAjouterSujet() {
		this.accueil = new Accueil(driver);
		this.accueil.cliquerConnexion();
		this.accueil.seConnecterSucces(PROPERTIES.getString(PROPERTY_IDENTIFIANT_ETUDIANT),
				PROPERTIES.getString(PROPERTY_MDP_ETUDIANT));
		this.accueil.cliquerSujet();
		this.ajouterSujet = this.accueil.cliquerAjouterSujet();
		assertEquals(PROPERTIES.getString(PROPERTY_TITRE_AJOUTER_SUJET), this.ajouterSujet.getTitrePage());
	}

	@Test
	public void test2AjouterSujet() {
		this.ajouterSujet = new AjouterSujet(driver);
		final List<String> options = new ArrayList<>();
		options.add("LD");
		options.add("SE");
		options.add("NRJ");
		final List<String> interets = new ArrayList<>();
		interets.add("GP");
		this.accueil = this.ajouterSujet.ajouterSujet(PROPERTIES.getString(PROPERTY_TEST),
				PROPERTIES.getString(PROPERTY_TEST), PROPERTY_TEST_NBR_MIN_ELEVES, PROPERTY_TEST_NBR_MAX_ELEVES, false,
				true, options, interets, PROPERTIES.getString(PROPERTY_TEST), PROPERTIES.getString(PROPERTY_TEST));
		assertEquals(PROPERTIES.getString(PROPERTY_TITRE_DASHBOARD), this.accueil.getTitrePage());
	}

}