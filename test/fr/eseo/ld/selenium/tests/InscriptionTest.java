package fr.eseo.ld.selenium.tests;

import static fr.eseo.ld.selenium.SeleniumUtilitaire.initialiserDriver;
import static org.junit.Assert.assertEquals;

import java.util.ResourceBundle;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.WebDriver;

import fr.eseo.ld.beans.Utilisateur;
import fr.eseo.ld.dao.DAOFactory;
import fr.eseo.ld.dao.UtilisateurDAO;
import fr.eseo.ld.selenium.pages.Connexion;
import fr.eseo.ld.selenium.pages.Inscription;

/**
 * Classe de tests fonctionnels Selenium de l'Inscription d'un utilisateur.
 * 
 * <p>Utilisation du Page Model Object et du Page Factory Design Pattern.</p>
 * 
 * @version 1.0
 * @author Maxime LENORMAND
 * 
 * @see org.openqa.selenium
 * @see org.junit
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class InscriptionTest {
	
	/* Accès au fichier properties */
	private static final ResourceBundle PROPERTIES = ResourceBundle.getBundle("fr.eseo.ld.selenium.selenium");

	private static final String PROPERTY_TEST = "test";
	private static final String PROPERTY_TEST_EMAIL = "emailTest";
	private static final String PROPERTY_ADRESSE_CONNEXION = "adresseConnexion";
	private static final String PROPERTY_TITRE_INSCRIPTION = "titreInscription";
	private static final String PROPERTY_NOTIFICATION_INSCRIPTION_SUCCES = "notifInscriptionSucces";
	private static final String PROPERTY_NOTIFICATION_INSCRIPTION_ERREUR = "notifInscriptionErreur";
	
	private static WebDriver driver;
	private static UtilisateurDAO utilisateurDAO;
	
	private Connexion connexion;
	private Inscription inscription;

	/**
	 * Initialise le driver et récupère une instance de DAO Utilisateur.
	 * 
	 * @throws Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		/* Initialisation du driver */
		driver = initialiserDriver(driver);
		/* Pré-conditions aux tests */
		driver.get(PROPERTIES.getString(PROPERTY_ADRESSE_CONNEXION));
		/* Récupération d'une instance de DAO Utilisateur */
		utilisateurDAO = DAOFactory.getInstance().getUtilisateurDao();
	}

	/**
	 * Ferme le navigateur et supprime l'utilisateur inséré dans la BDD.
	 * 
	 * @throws Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		/* Fermeture du navigateur */
		driver.quit();
		/* Suppression de l'utilisateur inséré dans la BDD */
		final Utilisateur utilisateur = new Utilisateur();
		utilisateur.setIdentifiant(PROPERTIES.getString(PROPERTY_TEST));
		utilisateurDAO.supprimer(utilisateur);
	}

	@Test
	public void test1OuvrirPageInscription() {
		this.connexion = new Connexion(driver);
		this.inscription = this.connexion.cliquerInscription();
		assertEquals(PROPERTIES.getString(PROPERTY_TITRE_INSCRIPTION), this.inscription.getTitrePage());
	}
	
	@Test
	public void test2InscrireSucces() {
		this.inscription = new Inscription(driver);
		this.inscription.inscrire(PROPERTIES.getString(PROPERTY_TEST), PROPERTIES.getString(PROPERTY_TEST),
				PROPERTIES.getString(PROPERTY_TEST), PROPERTIES.getString(PROPERTY_TEST),
				PROPERTIES.getString(PROPERTY_TEST_EMAIL));
		assertEquals(PROPERTIES.getString(PROPERTY_TITRE_INSCRIPTION), this.inscription.getTitrePage());
		assertEquals(PROPERTIES.getString(PROPERTY_NOTIFICATION_INSCRIPTION_SUCCES),
				this.inscription.getNotifSucces().getText());
	}
	
	@Test
	public void test3InscrireErreur() {
		this.inscription = new Inscription(driver);
		this.inscription.inscrire(PROPERTIES.getString(PROPERTY_TEST), PROPERTIES.getString(PROPERTY_TEST),
				PROPERTIES.getString(PROPERTY_TEST), PROPERTIES.getString(PROPERTY_TEST),
				PROPERTIES.getString(PROPERTY_TEST_EMAIL));
		assertEquals(PROPERTIES.getString(PROPERTY_TITRE_INSCRIPTION), this.inscription.getTitrePage());
		assertEquals(PROPERTIES.getString(PROPERTY_NOTIFICATION_INSCRIPTION_ERREUR),
				this.inscription.getNotifErreur().getText());
	}

}