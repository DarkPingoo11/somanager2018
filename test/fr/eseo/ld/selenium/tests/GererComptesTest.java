package fr.eseo.ld.selenium.tests;

import static fr.eseo.ld.selenium.SeleniumUtilitaire.initialiserDriver;
import static org.junit.Assert.assertEquals;

import java.util.ResourceBundle;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import fr.eseo.ld.selenium.pages.Accueil;
import fr.eseo.ld.selenium.pages.GererComptes;

public class GererComptesTest {

	
	private static final ResourceBundle PROPERTIES = ResourceBundle.getBundle("fr.eseo.ld.selenium.selenium");
	private static final String PROPERTY_ADRESSE_GERERCOMPTES = "http://localhost:8080/projet-ld-s8-2018/GererComptes";
	private static final String PROPERTY_ADRESSE_ACCUEIL = "http://localhost:8080/projet-ld-s8-2018/";
	private static final String PROPERTY_IDENTIFIANT_ETUDIANT = "test";
	private static final String PROPERTY_MDP_ETUDIANT = "test";
	
	private static WebDriver driver;
	
	private GererComptes gererComptes;
	private Accueil accueil;
	
	
	/**
	 * Initialise le driver.
	 * 
	 * @throws Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		/* Initialisation du driver */
		driver = initialiserDriver(driver);
		/* Pré-conditions aux tests */
		driver.get(PROPERTIES.getString(PROPERTY_ADRESSE_ACCUEIL));
	}

	/**
	 * Ferme le navigateur.
	 * 
	 * @throws Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		/* Fermeture du navigateur */
		driver.quit();
	}
	
	@Test
	public void test1OuvirPageGererComptes() {
		this.accueil = new Accueil(driver);
		this.accueil.cliquerConnexion();
		this.accueil.seConnecterSucces(PROPERTIES.getString(PROPERTY_IDENTIFIANT_ETUDIANT),
				PROPERTIES.getString(PROPERTY_MDP_ETUDIANT));
		this.gererComptes.cliqueAdministrateur();
		this.gererComptes.cliqueGererCompte();
		this.gererComptes.cliqueChangePasswordAdmin();
		assertEquals(PROPERTIES.getString(PROPERTY_ADRESSE_GERERCOMPTES), this.gererComptes.getTitrePage());
	}
	
	@Test
	public void test2ChangerMdp() {
		this.gererComptes.remplissageIdentifiantMdp();
		this.gererComptes.cliqueSubmit();
		assertEquals(PROPERTIES.getString(PROPERTY_ADRESSE_GERERCOMPTES), this.gererComptes.getTitrePage());
	}

}
