package fr.eseo.ld.selenium.tests;

import static fr.eseo.ld.selenium.SeleniumUtilitaire.initialiserDriver;
import static org.junit.Assert.assertEquals;

import java.util.ResourceBundle;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.WebDriver;

import fr.eseo.ld.selenium.pages.Accueil;
import fr.eseo.ld.selenium.pages.GererApplication;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class GererApplicationTest {

	
	private static final ResourceBundle PROPERTIES = ResourceBundle.getBundle("fr.eseo.ld.selenium.selenium");
	private static final String PROPERTY_ADRESSE_GERERAPPLICATION = "adresseGererApp";
	private static final String PROPERTY_ADRESSE_ACCUEIL = "adresseAccueil";
	private static final String PROPERTY_IDENTIFIANT_ETUDIANT = "test";
	private static final String PROPERTY_MDP_ETUDIANT = "test";
	private static final String PROPERTY_TITRE_ACCUEIL = "titreAccueil";
	
	private static WebDriver driver;
	
	private GererApplication gererApplication;
	private Accueil accueil;
	
	
	/**
	 * Initialise le driver.
	 * 
	 * @throws Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		/* Initialisation du driver */
		driver = initialiserDriver(driver);
		/* Pré-conditions aux tests */
		driver.get(PROPERTIES.getString(PROPERTY_ADRESSE_ACCUEIL));
	}

	/**
	 * Ferme le navigateur.
	 * 
	 * @throws Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		/* Fermeture du navigateur */
		driver.quit();
	}
	
	@Test
	public void test1OuvirPageGererApplication() {
		this.accueil = new Accueil(driver);
		this.accueil.cliquerConnexion();
		this.accueil.seConnecterSucces(PROPERTIES.getString(PROPERTY_IDENTIFIANT_ETUDIANT),
				PROPERTIES.getString(PROPERTY_MDP_ETUDIANT));
		this.gererApplication.cliqueAdministrateur();
		this.gererApplication.cliqueParamApp();
		assertEquals(PROPERTIES.getString(PROPERTY_ADRESSE_GERERAPPLICATION), this.gererApplication.getTitrePage());
	}
	
	@Test
	public void test2ChangerMdp() {
		this.gererApplication.remplissageBDD();
		this.gererApplication.cliqueModifierBDD();
		assertEquals(PROPERTIES.getString(PROPERTY_TITRE_ACCUEIL), this.gererApplication.getTitrePage());
	}

}

