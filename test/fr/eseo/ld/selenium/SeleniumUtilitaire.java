package fr.eseo.ld.selenium;

import static org.junit.Assert.fail;

import java.io.File;
import java.io.IOException;
import java.util.ResourceBundle;
import java.util.concurrent.TimeUnit;

import javax.imageio.ImageIO;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;
import org.openqa.selenium.remote.DesiredCapabilities;

/**
 * Classe Utilitaire pour les tests fonctionnels Selenium.
 * 
 * <p>Cette classe centralise des méthodes utilitaires exécutées par les tests fonctionnels Selenium.<p>
 * <p>Le parametre "path" du fichier "fr.eseo.ld.selenium.selenium.properties" 
 * doit avoir pour valeur le chemin complet de l'executable PhantomJS sur votre machine.<p>
 * 
 * @author Maxime LENORMAND
 * 
 * @see org.openqa.selenium
 */
public final class SeleniumUtilitaire {

	/* Accès au fichier properties */
	private static final ResourceBundle PROPERTIES = ResourceBundle.getBundle("fr.eseo.ld.selenium.selenium");

	private static final String PROPERTY_DRIVER = "driver";
	private static final String PROPERTY_PATH = "path";

	/*
	 * Constructeur caché par défaut (car c'est une classe finale utilitaire,
	 * contenant uniquement des méthodes appelées de manière statique)
	 */
	private SeleniumUtilitaire() {

	}

	/**
	 * Initialise le driver selon la configuration souhaitée.
	 * 
	 * @param driver le driver que l'on souhaite initialiser.
	 * @return driver le driver initialisé.
	 */
	public static WebDriver initialiserDriver(WebDriver driver) {
		/* Vérification que l'executable du driver est dans le système */
		final File executable = new File(PROPERTIES.getString(PROPERTY_PATH));
		if (!executable.exists()) {
			fail("Executable introuvable.");
		}
		/* Omission des erreurs de sécurité */
		final DesiredCapabilities desiredCapabilities = new DesiredCapabilities();
		desiredCapabilities.setCapability(PhantomJSDriverService.PHANTOMJS_CLI_ARGS,
				new String[] { "--ignore-ssl-errors=yes" });
		System.setProperty(PROPERTIES.getString(PROPERTY_DRIVER), PROPERTIES.getString(PROPERTY_PATH));
		/* Utilisation du driver de PhantomJS */
		driver = new PhantomJSDriver(desiredCapabilities);
		driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		return driver;
	}
	
	/**
	 * Prend une capture d'écran du contenu de la fenêtre courante du navigateur.
	 * 
	 * @param nomFichier le nom de l'image à enregistrer au format PNG.
	 */
	public static void prendreCaptureEcran(String nomFichier, WebDriver driver) {
		try {
			File captureEcran = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			ImageIO.write(ImageIO.read(captureEcran), "PNG", new File(nomFichier));
		} catch (IOException ioe) {
			fail("La capture d'écran n'a pas pu être enregistrée : " + ioe.getMessage());
		}
	}

}