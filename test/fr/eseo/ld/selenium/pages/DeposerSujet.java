package fr.eseo.ld.selenium.pages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;

/**
 * Classe de la page DeposerSujet.
 * 
 * <p>Utilisation du Page Model Object et du Page Factory Design Pattern.</p>
 * 
 * @author Tristan LE GACQUE
 * 
 * @see org.openqa.selenium
 */
public class DeposerSujet extends Page {

	@FindBy(id = "file_selected")
	@CacheLookup
	private WebElement parcourirBouton;

	@FindBy(id = "sendCsvButton")
	@CacheLookup
	private WebElement envoyerBouton;

	@FindBy(id = "nomFichier")
	@CacheLookup
	private WebElement nomFichier;

	@FindBy(id = "callbackAlert")
	@CacheLookup
	private WebElement callbackAlert;

	@FindBy(id = "formulaireG")
	@CacheLookup
	private WebElement formulaireG;

    @FindBy(id = "nouveauLien")
    @CacheLookup
    private WebElement fieldNouveauLien;

    @FindBy(id = "affecterModif")
    @CacheLookup
    private WebElement affecterModif;

	public DeposerSujet(WebDriver driver) {
		super(driver);
	}

	public Accueil deposerSujet(String fichier) {
		//Rendre l'element visible
		String js = "arguments[0].style.height='auto'; arguments[0].style.display='block';";
		((JavascriptExecutor) this.getDriver()).executeScript(js, this.parcourirBouton);

		this.parcourirBouton.sendKeys(fichier);
		return new Accueil(this.getDriver());
	}

	public Accueil envoyerSujet() {
		this.envoyerBouton.click();
		return new Accueil(this.getDriver());
	}

	public DeposerSujet cliquerLienFormulaire() {
		this.formulaireG.click();
		return new DeposerSujet(this.getDriver());
	}

	public DeposerSujet cliquerFieldLien(String lien){
	    this.fieldNouveauLien.click();
	    this.fieldNouveauLien.sendKeys(lien);
        this.affecterModif.click();
        return new DeposerSujet(this.getDriver());
    }

    //return (string) le lien dans le champ formulaire (lien google form)
    public String getHrefValeur(){
		this.formulaireG.getAttribute("href");
		String strLien = this.formulaireG.getAttribute("href");
		return strLien;
	}

	public String getNomFichier() {
		return this.nomFichier.getAttribute("value");
	}

	public WebElement getCallbackAlert() {
		return this.callbackAlert;
	}

}