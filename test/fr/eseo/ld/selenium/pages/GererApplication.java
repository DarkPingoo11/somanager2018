package fr.eseo.ld.selenium.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;

public class GererApplication extends Page {
	
	@FindBy(id = "adresseBdd")
	@CacheLookup
	private WebElement adresseBdd;
	
	@FindBy(id = "utilsiateurBDD")
	@CacheLookup
	private WebElement utilsiateurBDD;
	
	@FindBy(id = "mdpBDD")
	@CacheLookup
	private WebElement mdpBDD;
	
	@FindBy(id = "modifierBDD")
	@CacheLookup
	private WebElement modifierBDD;
	
	@FindBy(id = "Administrateur")
	@CacheLookup
	private WebElement Administrateur;
	
	@FindBy(id = "ParamApp")
	@CacheLookup
	private WebElement ParamApp;
	
	public GererApplication(WebDriver driver) {
		super(driver);
	}
	
	public void cliqueModifierBDD() {
		this.modifierBDD.click();
	}
	
	public void cliqueAdministrateur() {
		this.Administrateur.click();
	}
	
	public void cliqueParamApp() {
		this.ParamApp.click();
	}
	
	public void remplissageBDD() {
		this.adresseBdd.sendKeys("jdbc:mysql://localhost/somanager");
		this.utilsiateurBDD.sendKeys("user");
		this.mdpBDD.sendKeys("user");
	}
}
