package fr.eseo.ld.selenium.pages;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;

/**
 * Classe de la page AjouterSujet.
 * 
 * <p>Utilisation du Page Model Object et du Page Factory Design Pattern.</p>
 * 
 * @author Maxime LENORMAND
 * 
 * @see org.openqa.selenium
 */
public class AjouterSujet extends Page {

	@FindBy(id = "titre")
	@CacheLookup
	private WebElement titre;

	@FindBy(id = "description")
	@CacheLookup
	private WebElement description;
	
	@FindBy(id = "nbrMinEleves")
	@CacheLookup
	private WebElement nbrMinEleves;
	
	@FindBy(id = "nbrMaxEleves")
	@CacheLookup
	private WebElement nbrMaxEleves;
	
	@FindBy(id = "contratProVrai")
	@CacheLookup
	private WebElement contratProVrai;
	
	@FindBy(id = "contratProFaux")
	@CacheLookup
	private WebElement contratProFaux;
	
	@FindBy(id = "confidentialiteVrai")
	@CacheLookup
	private WebElement confidentialiteVrai;
	
	@FindBy(id = "confidentialiteFaux")
	@CacheLookup
	private WebElement confidentialiteFaux;

	@FindBy(id = "CC")
	@CacheLookup
	private WebElement optionCC;

	@FindBy(id = "IIT")
	@CacheLookup
	private WebElement optionIT;

	@FindBy(id = "LD")
	@CacheLookup
	private WebElement optionLD;

	@FindBy(id = "SE")
	@CacheLookup
	private WebElement optionSE;

	@FindBy(id = "BIO")
	@CacheLookup
	private WebElement optionBIO;

	@FindBy(id = "OC")
	@CacheLookup
	private WebElement optionOC;

	@FindBy(id = "DSMT")
	@CacheLookup
	private WebElement optionDSMT;

	@FindBy(id = "NRJ")
	@CacheLookup
	private WebElement optionNRJ;

	@FindBy(id = "BD")
	@CacheLookup
	private WebElement optionBD;

	@FindBy(id = "JPO")
	@CacheLookup
	private WebElement interetJPO;

	@FindBy(id = "GP")
	@CacheLookup
	private WebElement interetGP;

	@FindBy(id = "autres")
	@CacheLookup
	private WebElement autres;

	@FindBy(id = "liens")
	@CacheLookup
	private WebElement liens;
	
	@FindBy(id = "soumettre")
	@CacheLookup
	private WebElement submit;

	public AjouterSujet(WebDriver driver) {
		super(driver);
	}

	public Accueil ajouterSujet(String titre, String description, String nbrMinEleves, String nbrMaxEleves,
			boolean contratPro, boolean confidentialite, List<String> options, List<String> interets, String autres,
			String liens) {
		this.titre.sendKeys(titre);
		this.description.sendKeys(description);
		this.nbrMinEleves.sendKeys(nbrMinEleves);
		this.nbrMaxEleves.sendKeys(nbrMaxEleves);
		if (contratPro) {
			this.contratProVrai.click();
		} else {
			this.contratProFaux.click();
		}
		if (confidentialite) {
			this.confidentialiteVrai.click();
		} else {
			this.confidentialiteFaux.click();
		}
		/* Sélection des options entrées dans la liste d'options */
		for (String option : options) {
			switch (option) {
			case "CC":
				this.optionCC.click();
				break;
			case "IIT":
				this.optionIT.click();
				break;
			case "LD":
				this.optionLD.click();
				break;
			case "SE":
				this.optionSE.click();
				break;
			case "BIO":
				this.optionBIO.click();
				break;
			case "OC":
				this.optionOC.click();
				break;
			case "DSMT":
				this.optionDSMT.click();
				break;
			case "NRJ":
				this.optionNRJ.click();
				break;
			case "BD":
				this.optionBD.click();
				break;
			}
		}
		/* Sélection des interets entrés dans la liste d'interets */
		for (String interet : interets) {
			switch (interet) {
			case "JPO":
				this.interetJPO.click();
				break;
			case "GP":
				this.interetGP.click();
				break;
			}
		}
		this.autres.sendKeys(autres);
		this.liens.sendKeys(liens);
		this.submit.click();
		return new Accueil(this.getDriver());
	}

}