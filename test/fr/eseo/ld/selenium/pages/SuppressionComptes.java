package fr.eseo.ld.selenium.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;


public class SuppressionComptes extends Page{

	public SuppressionComptes(WebDriver driver) {
		super(driver);
	}

	@FindBy(id = "suppressionCompte")
	@CacheLookup
	private WebElement suppressionCompte;
	
	
	@FindBy(id = "identifiant")
	@CacheLookup
	private WebElement identifiant;
	
	@FindBy(id = "submit")
	@CacheLookup
	private WebElement submit;
	
	@FindBy(id = "callbackAlert")
	@CacheLookup
	private WebElement callbackAlert;

	
	public Accueil suppressionCompte(String identifiant) {
		this.identifiant.sendKeys(identifiant);
		return new Accueil(this.getDriver());
	}
	
	public Accueil supprimerCompte() {
		this.submit.click();
		return new Accueil(this.getDriver());
	}
	
	public WebElement getCallbackAlert() {
		return this.callbackAlert;
	}
	
	
	
}
