package fr.eseo.ld.selenium.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

/**
 * Classe mère des pages.
 * 
 * <p>Utilisation du Page Model Object et du Page Factory Design Pattern.</p>
 * <p>Ce design pattern permet de :</p>
 * <ul>
 * 	<li>séparer les tests du modèle des pages ;</li>
 *  <li>chercher les WebElements des pages à l'aide d'annotations ;</li>
 * 	<li>centraliser les WebElements des pages et les méthodes qui agissent avec ceux-ci.</li>
 * </ul>
 * 
 * @author Maxime LENORMAND
 * 
 * @see org.openqa.selenium
 */
public class Page {
	
	private WebDriver driver;

	public Page(WebDriver driver) {
		this.driver = driver;
		/* Initialisation des WebElements définis dans les pages */
		PageFactory.initElements(driver, this);
	}
	
	public WebDriver getDriver() {
		return this.driver;
	}
	
	public void setDriver(WebDriver driver) {
		this.driver = driver;
	}
	
	public String getTitrePage() {
		return this.driver.getTitle();
	}
	
}