package fr.eseo.ld.selenium.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;

public class GererComptes extends Page{
	
	@FindBy(id = "changePasswordAdmin")
	@CacheLookup
	private WebElement changePasswordAdmin;
	
	@FindBy(id = "identifiant")
	@CacheLookup
	private WebElement identifiant;
	
	@FindBy(id = "ChangerMotDePasseAdmin")
	@CacheLookup
	private WebElement ChangerMotDePasseAdmin;
	
	@FindBy(id = "motDePasse1")
	@CacheLookup
	private WebElement motDePasse1;
	
	@FindBy(id = "nouveauMDP2")
	@CacheLookup
	private WebElement nouveauMDP2;
	
	@FindBy(id = "modifierMdpUtilisateur")
	@CacheLookup
	private WebElement modifierMdpUtilisateur;
	
	@FindBy(id = "reset")
	@CacheLookup
	private WebElement reset;
	
	@FindBy(id = "Administrateur")
	@CacheLookup
	private WebElement Administrateur;
	
	@FindBy(id = "GererCompte")
	@CacheLookup
	private WebElement GererCompte;
	
	
	public GererComptes(WebDriver driver) {
		super(driver);
	}
	
	
	public void cliqueChangePasswordAdmin() {
		this.changePasswordAdmin.click();
	}
	
	public void cliqueAdministrateur() {
		this.Administrateur.click();
	}
	
	public void cliqueGererCompte() {
		this.GererCompte.click();
	}
	
	
	public void cliqueSubmit() {
		this.modifierMdpUtilisateur.click();
	}
	
	
	public void cliqueReset() {
		this.reset.click();
	}	
	
	public void remplissageIdentifiantMdp() {
		this.identifiant.sendKeys("admin");
		this.motDePasse1.sendKeys("network5");
		this.nouveauMDP2.sendKeys("network5");
	}


}
