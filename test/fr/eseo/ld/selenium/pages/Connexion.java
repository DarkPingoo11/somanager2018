package fr.eseo.ld.selenium.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;

/**
 * Classe de la page Connexion.
 * 
 * <p>Utilisation du Page Model Object et du Page Factory Design Pattern.</p>
 * 
 * @author Maxime LENORMAND
 * 
 * @see org.openqa.selenium
 */
public class Connexion extends Page {

	@FindBy(id = "identifiant")
	@CacheLookup
	private WebElement identifiant;

	@FindBy(id = "motDePasse")
	@CacheLookup
	private WebElement motDePasse;

	@FindBy(id = "submit")
	@CacheLookup
	private WebElement submit;

	@FindBy(linkText = "Inscription")
	@CacheLookup
	private WebElement inscription;

	public Connexion(WebDriver driver) {
		super(driver);
	}

	public Accueil seConnecter(String identifiant, String motDePasse) {
		this.identifiant.sendKeys(identifiant);
		this.motDePasse.sendKeys(motDePasse);
		this.submit.click();
		return new Accueil(this.getDriver());
	}

	public Inscription cliquerInscription() {
		this.inscription.click();
		return new Inscription(this.getDriver());
	}

}