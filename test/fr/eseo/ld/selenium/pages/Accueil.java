package fr.eseo.ld.selenium.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;

/**
 * Classe de la page Accueil.
 * 
 * <p>Utilisation du Page Model Object et du Page Factory Design Pattern.</p>
 * 
 * @author Maxime LENORMAND
 * 
 * @see org.openqa.selenium
 */
public class Accueil extends Page {

	@FindBy(id = "sujet")
	@CacheLookup
	private WebElement sujet;

	@FindBy(id = "depot")
	@CacheLookup
	private WebElement depotSujet;

	@FindBy(id = "ajouterSujet")
	@CacheLookup
	private WebElement ajouterSujet;
	
	@FindBy(id = "equipe")
	@CacheLookup
	private WebElement equipe;
	
	@FindBy(id = "creerEquipe")
	@CacheLookup
	private WebElement creerEquipe;
	
	@FindBy(id = "connexion")
	@CacheLookup
	private WebElement connexion;
	
	@FindBy(id = "identifiantConnexion")
	@CacheLookup
	private WebElement identifiant;
	
	@FindBy(id = "motDePasseConnexion")
	@CacheLookup
	private WebElement motDePasse;
	
	@FindBy(id = "submitConnexion")
	@CacheLookup
	private WebElement submit;

	@FindBy(id = "deconnexion")
	@CacheLookup
	private WebElement deconnexion;
	
	@FindBy(id = "administateur")
	@CacheLookup
	private WebElement administrateur;

	public Accueil(WebDriver driver) {
		super(driver);
	}
	
	public void cliquerSujet() {
		this.sujet.click();
	}

	public AjouterSujet cliquerAjouterSujet() {
		this.ajouterSujet.click();
		return new AjouterSujet(this.getDriver());
	}

	public DeposerSujet cliquerDeposerSujet() {
		this.depotSujet.click();
		return new DeposerSujet(this.getDriver());
	}
	
	public void cliquerEquipe() {
		this.equipe.click();
	}

	public CreerEquipe cliquerCreerEquipe() {
		this.creerEquipe.click();
		return new CreerEquipe(this.getDriver());
	}

	public void cliquerConnexion() {
		this.connexion.click();
	}
	
	public void seConnecterSucces(String identifiant, String motDePasse) {
		this.identifiant.sendKeys(identifiant);
		this.motDePasse.sendKeys(motDePasse);
		this.submit.click();
	}
	
	public Connexion seConnecterErreur(String identifiant, String motDePasse) {
		this.identifiant.sendKeys(identifiant);
		this.motDePasse.sendKeys(motDePasse);
		this.submit.click();
		return new Connexion(this.getDriver());
	}

	public void cliquerDeconnexion() {
		this.deconnexion.click();
	}
	
	public void cliquerAdministrateur() {
		this.administrateur.click();
	}
	//public void cliquerGestionComptes() {
		//this.administrateur.click();
	//}

}