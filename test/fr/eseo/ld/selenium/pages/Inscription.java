package fr.eseo.ld.selenium.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;

/**
 * Classe de la page Inscription.
 * 
 * <p>Utilisation du Page Model Object et du Page Factory Design Pattern.</p>
 * 
 * @author Maxime LENORMAND
 * 
 * @see org.openqa.selenium
 */
public class Inscription extends Page {

	@FindBy(id = "nom")
	@CacheLookup
	private WebElement nom;

	@FindBy(id = "prenom")
	@CacheLookup
	private WebElement prenom;

	@FindBy(id = "identifiant")
	@CacheLookup
	private WebElement identifiant;

	@FindBy(id = "motDePasse")
	@CacheLookup
	private WebElement motDePasse;

	@FindBy(id = "email")
	@CacheLookup
	private WebElement email;
	
	@FindBy(className = "succes")
	@CacheLookup
	private WebElement succes;
	
	@FindBy(className = "erreur")
	@CacheLookup
	private WebElement erreur;

	@FindBy(id = "submitUtilisateur")
	@CacheLookup
	private WebElement submit;

	public Inscription(WebDriver driver) {
		super(driver);
	}

	public void inscrire(String nom, String prenom, String identifiant, String motDePasse, String email) {
		this.nom.sendKeys(nom);
		this.prenom.sendKeys(prenom);
		this.identifiant.sendKeys(identifiant);
		this.motDePasse.sendKeys(motDePasse);
		this.email.sendKeys(email);
		this.submit.click();
	}
	
	public WebElement getNotifSucces() {
		return this.succes;
	}
	
	public WebElement getNotifErreur() {
		return this.erreur;
	}

}