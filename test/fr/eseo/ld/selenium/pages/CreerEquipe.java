package fr.eseo.ld.selenium.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;

/**
 * Classe de la page CreerEquipe.
 * 
 * <p>Utilisation du Page Model Object et du Page Factory Design Pattern.</p>
 * 
 * @author Maxime LENORMAND
 * 
 * @see org.openqa.selenium
 */
public class CreerEquipe extends Page {
	
	@FindBy(id = "draggableEtudiant-1")
	private WebElement sujet;
	
	@FindBy(id = "membre1")
	private WebElement sujetChoisi;
	
	@FindBy(className = "erreur")
	@CacheLookup
	private WebElement erreur;
	
	@FindBy(id = "submitEquipe")
	@CacheLookup
	private WebElement submit;

	public CreerEquipe(WebDriver driver) {
		super(driver);
	}
	
	public void creerEquipeErreur() {
		this.dragAndDrop(sujet, sujetChoisi);
		//this.submit.click();
	}
	
	public WebElement getNotifErreur() {
		return this.erreur;
	}
	
	private void dragAndDrop(WebElement source, WebElement destination) {
		(new Actions(this.getDriver())).dragAndDrop(source, destination).perform();
	}
	
}