package fr.eseo.ld.beans;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.lang.reflect.Field;

import org.junit.Test;

/**
 * Classe de tests unitaires JUnit 4 de la classe Etudiant.
 *
 * <p>Utilisation de la réflexivité.</p>
 *
 * @version 1.0
 * @author Julie Avizou
 *
 * @see fr.eseo.ld.beans.Etudiant
 * @see org.junit
 */
public class EtudiantTest {
	
	private static final Integer INTEGER_TEST = 1;
	private static final Long LONG_TEST = 1L;
	private static final Float FLOAT_TEST = 1f;
	private static final String STRING_TEST = "non";
	
	private static final String ATT_ID_ETUDIANT = "idEtudiant";
	private static final String ATT_ANNEE = "annee";
	private static final String ATT_CONTRAT_PRO = "contratPro";
	private static final String ATT_NOTE_INTERMEDIAIRE = "noteIntermediaire";
	private static final String ATT_NOTE_PROJET = "noteProjet";
	private static final String ATT_NOTE_SOUTENANCE = "noteSoutenance";
	private static final String ATT_NOTE_POSTER = "notePoster";
	
	/**
	 * Teste le constructeur par défaut de la classe. 
	 */
	@Test
	public void testConstructeurParDefaut() {
		final Etudiant etudiant = new Etudiant();

		assertNull("L'attribut n'est pas nul", etudiant.getIdEtudiant());
	}
	
	/**
	 * Teste la méthode public Long getIdEtudiant().
	 * 
	 * @throws NoSuchFieldException
	 * @throws IllegalAccessException
	 */
	@Test
	public void testGetIdEtudiant () throws NoSuchFieldException, IllegalAccessException {
		final Etudiant etudiant = new Etudiant();
		
		final Field idEtudiant = etudiant.getClass().getDeclaredField(ATT_ID_ETUDIANT);
		idEtudiant.setAccessible(true);
		
		final Long resultatAttendu = LONG_TEST;
		idEtudiant.set(etudiant, resultatAttendu);
		final Long resultatTrouve = etudiant.getIdEtudiant();
		
		assertEquals("Mauvais idEtudiant", resultatAttendu, resultatTrouve);
	}
	
	/**
	 * Teste la méthode public void setIdEtudiant (Long idEtudiant).
	 * 
	 * @throws NoSuchFieldException
	 * @throws IllegalAccessException
	 */
	@Test
	public void testSetIdEtudiant () throws NoSuchFieldException, IllegalAccessException {
		final Etudiant etudiant = new Etudiant();

		final Field idEtudiant = etudiant.getClass().getDeclaredField(ATT_ID_ETUDIANT);
		idEtudiant.setAccessible(true);
		
		final Long resultatAttendu = LONG_TEST;
		etudiant.setIdEtudiant(resultatAttendu);
		final Long resultatTrouve = (Long) idEtudiant.get(etudiant);

		assertEquals("Mauvais idEtudiant", resultatAttendu, resultatTrouve);
	}
	
	/**
	 * Teste de la méthode public Integer getAnnee().
	 * 
	 * @throws NoSuchFieldException
	 * @throws IllegalAccessException
	 */
	@Test
	public void testGetAnnee() throws NoSuchFieldException, IllegalAccessException {
		final Etudiant etudiant = new Etudiant();

		final Field annee = etudiant.getClass().getDeclaredField(ATT_ANNEE);
		annee.setAccessible(true);
		
		final Integer resultatAttendu = INTEGER_TEST;
		annee.set(etudiant, resultatAttendu);
		final Integer resultatTrouve = etudiant.getAnnee();
		
		assertEquals("Mauvaise annee", resultatAttendu, resultatTrouve);
	}
	
	/**
	 * Teste de la méthode public void setAnnee(Integer annee).
	 * 
	 * @throws NoSuchFieldException
	 * @throws IllegalAccessException
	 */
	@Test
	public void testSetAnnee() throws NoSuchFieldException, IllegalAccessException {
		final Etudiant etudiant = new Etudiant();
		
		final Field annee = etudiant.getClass().getDeclaredField(ATT_ANNEE);
		annee.setAccessible(true);

		final Integer resultatAttendu = INTEGER_TEST;
		etudiant.setAnnee(resultatAttendu);
		final Integer resultatTrouve = (Integer) annee.get(etudiant);
		
		assertEquals("Mauvaise annee", resultatAttendu, resultatTrouve);
	}
	
	/**
	 * Teste de la méthode public String getContratPro().
	 * 
	 * @throws NoSuchFieldException
	 * @throws IllegalAccessException
	 */
	@Test
	public void testGetContratPro() throws NoSuchFieldException, IllegalAccessException {
		final Etudiant etudiant = new Etudiant();

		final Field contratPro = etudiant.getClass().getDeclaredField(ATT_CONTRAT_PRO);
		contratPro.setAccessible(true);
		
		final String resultatAttendu = STRING_TEST;
		contratPro.set(etudiant, resultatAttendu);
		final String resultatTrouve = etudiant.getContratPro();
		
		assertEquals("Mauvais contratPro", resultatAttendu, resultatTrouve);
	}
	
	/**
	 * Teste de la méthode public void setContratPro(String contratPro).
	 * 
	 * @throws NoSuchFieldException
	 * @throws IllegalAccessException
	 */
	@Test
	public void testSetContratPro() throws NoSuchFieldException, IllegalAccessException {
		final Etudiant etudiant = new Etudiant();
		
		final Field contratPro = etudiant.getClass().getDeclaredField(ATT_CONTRAT_PRO);
		contratPro.setAccessible(true);
		
		final String resultatAttendu = STRING_TEST;
		etudiant.setContratPro(resultatAttendu);
		final String resultatTrouve = (String) contratPro.get(etudiant);
		
		assertEquals("Mauvais contratPro", resultatAttendu, resultatTrouve);
	}
	
	/**
	 * Teste de la méthode public Float getNoteIntermediaire().
	 * 
	 * @throws NoSuchFieldException
	 * @throws IllegalAccessException
	 */
	@Test
	public void testGetNoteIntermediaire() throws NoSuchFieldException, IllegalAccessException {
		final Etudiant etudiant = new Etudiant();
		
		final Field noteIntermediaire = etudiant.getClass().getDeclaredField(ATT_NOTE_INTERMEDIAIRE);
		noteIntermediaire.setAccessible(true);
		
		final Float resultatAttendu = FLOAT_TEST;
		noteIntermediaire.set(etudiant, resultatAttendu);
		final Float resultatTrouve = etudiant.getNoteIntermediaire();
		
		assertEquals("Mauvaise noteIntermediaire", resultatAttendu, resultatTrouve);
	}
	
	/**
	 * Teste de la méthode public void setNoteIntermediaire(Float noteIntermediaire).
	 * 
	 * @throws NoSuchFieldException
	 * @throws IllegalAccessException
	 */
	@Test
	public void testSetNoteIntermediaire() throws NoSuchFieldException, IllegalAccessException {
		final Etudiant etudiant = new Etudiant();
		
		final Field noteIntermediaire = etudiant.getClass().getDeclaredField(ATT_NOTE_INTERMEDIAIRE);
		noteIntermediaire.setAccessible(true);

		final Float resultatAttendu = FLOAT_TEST;
		etudiant.setNoteIntermediaire(resultatAttendu);
		final Float resultatTrouve = (Float) noteIntermediaire.get(etudiant);
		
		assertEquals("Mauvaise noteIntermediaire", resultatAttendu, resultatTrouve);
	}
	
	/**
	 * Teste de la méthode public Float getNoteProjet().
	 * 
	 * @throws NoSuchFieldException
	 * @throws IllegalAccessException
	 */
	@Test
	public void testGetNoteProjet() throws NoSuchFieldException, IllegalAccessException {
		final Etudiant etudiant = new Etudiant();

		final Field noteProjet = etudiant.getClass().getDeclaredField(ATT_NOTE_PROJET);
		noteProjet.setAccessible(true);
		
		final Float resultatAttendu = FLOAT_TEST;
		noteProjet.set(etudiant, resultatAttendu);
		final Float resultatTrouve = etudiant.getNoteProjet();
		
		assertEquals("Mauvaise noteProjet", resultatAttendu, resultatTrouve);
	}
	
	/**
	 * Teste de la méthode public void setNoteProjet(Float noteProjet).
	 * 
	 * @throws NoSuchFieldException
	 * @throws IllegalAccessException
	 */
	@Test
	public void testSetNoteProjet() throws NoSuchFieldException, IllegalAccessException {
		final Etudiant etudiant = new Etudiant();
		
		final Field noteProjet = etudiant.getClass().getDeclaredField(ATT_NOTE_PROJET);
		noteProjet.setAccessible(true);
		
		final Float resultatAttendu = FLOAT_TEST;
		etudiant.setNoteProjet(resultatAttendu);
		final Float resultatTrouve = (Float) noteProjet.get(etudiant);
		
		assertEquals("Mauvaise noteProjet", resultatAttendu, resultatTrouve);
	}
	
	/**
	 * Teste de la méthode public Float getNoteSoutenance().
	 * 
	 * @throws NoSuchFieldException
	 * @throws IllegalAccessException
	 */
	@Test
	public void testGetNoteSoutenance() throws NoSuchFieldException, IllegalAccessException {
		final Etudiant etudiant = new Etudiant();

		final Field noteSoutenance = etudiant.getClass().getDeclaredField(ATT_NOTE_SOUTENANCE);
		noteSoutenance.setAccessible(true);
		
		final Float resultatAttendu = FLOAT_TEST;
		noteSoutenance.set(etudiant, resultatAttendu);
		final Float resultatTrouve = etudiant.getNoteSoutenance();
		
		assertEquals("Mauvaise noteSoutenance", resultatAttendu, resultatTrouve);
	}
	
	/**
	 * Teste de la méthode public void setNoteSoutenance(Float noteSoutenance).
	 * 
	 * @throws NoSuchFieldException
	 * @throws IllegalAccessException
	 */
	@Test
	public void testSetNoteSoutenance() throws NoSuchFieldException, IllegalAccessException {
		final Etudiant etudiant = new Etudiant();
		
		final Field noteSoutenance = etudiant.getClass().getDeclaredField(ATT_NOTE_SOUTENANCE);
		noteSoutenance.setAccessible(true);
		
		final Float resultatAttendu = FLOAT_TEST;
		etudiant.setNoteSoutenance(resultatAttendu);
		final Float resultatTrouve = (Float) noteSoutenance.get(etudiant);
		
		assertEquals("Mauvaise noteSoutenance", resultatAttendu, resultatTrouve);
	}
	
	/**
	 * Teste de la méthode public Float getNotePoster().
	 * 
	 * @throws NoSuchFieldException
	 * @throws IllegalAccessException
	 */
	@Test
	public void testGetNotePoster() throws NoSuchFieldException, IllegalAccessException {
		final Etudiant etudiant = new Etudiant();

		final Field notePoster = etudiant.getClass().getDeclaredField(ATT_NOTE_POSTER);
		notePoster.setAccessible(true);
		
		final Float resultatAttendu = FLOAT_TEST;
		notePoster.set(etudiant, resultatAttendu);
		final Float resultatTrouve = etudiant.getNotePoster();
		
		assertEquals("Mauvaise notePoster", resultatAttendu, resultatTrouve);
	}
	
	/**
	 * Teste de la méthode public void setNotePoster(Float notePoster).
	 * 
	 * @throws NoSuchFieldException
	 * @throws IllegalAccessException
	 */
	@Test
	public void testSetNotePoster() throws NoSuchFieldException, IllegalAccessException {
		final Etudiant etudiant = new Etudiant();
		
		final Field notePoster = etudiant.getClass().getDeclaredField(ATT_NOTE_POSTER);
		notePoster.setAccessible(true);

		final Float resultatAttendu = FLOAT_TEST;
		etudiant.setNotePoster(resultatAttendu);
		final Float resultatTrouve = (Float) notePoster.get(etudiant);
		
		assertEquals("Mauvaise notePoster", resultatAttendu, resultatTrouve);
	}
	
	/**
	 * Teste la méthode public String toString().
	 */
	@Test
	public void testToString() {
		final Etudiant etudiant = new Etudiant();
		etudiant.setIdEtudiant(LONG_TEST);
		etudiant.setAnnee(INTEGER_TEST);
		etudiant.setContratPro(STRING_TEST);
		etudiant.setNoteIntermediaire(FLOAT_TEST);
		etudiant.setNoteProjet(FLOAT_TEST);
		etudiant.setNoteSoutenance(FLOAT_TEST);
		etudiant.setNotePoster(FLOAT_TEST);

		final String resultatAttendu = "Etudiant [idEtudiant=" + LONG_TEST + ", annee=" + INTEGER_TEST + ", contratPro="
				+ STRING_TEST + ", noteIntermediaire=" + FLOAT_TEST + ", noteProjet=" + FLOAT_TEST + ", noteSoutenance="
				+ FLOAT_TEST + ", notePoster=" + FLOAT_TEST + "]";
		final String resultatTrouve = etudiant.toString();

		assertEquals("Mauvais toString()", resultatAttendu, resultatTrouve);
	}
	
}