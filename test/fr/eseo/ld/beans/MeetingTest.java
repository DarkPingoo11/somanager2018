package fr.eseo.ld.beans;

import net.codebox.javabeantester.JavaBeanTester;
import org.junit.Test;

import java.beans.IntrospectionException;

import static junit.framework.TestCase.assertEquals;

/**
 * Classe de tests unitaires JUnit 4 de la classe Meeting.
 *
 * <p>Utilisation de la réflexivité.</p>
 *
 * @author Dimitri J
 * @version 1.0
 * @see fr.eseo.ld.beans.Invite
 * @see org.junit
 */
public class MeetingTest {

    private static final String ATT_REF_UTILISATEUR = "refUtilisateur";
    private static final String ATT_ID_MEETING= "idMeeting";
    private static final String ATT_DATE_REUNION = "dateReunion";
    private static final String ATT_COMPTE_RENDU = "compteRendu";
    private static final String ATT_TITRE = "titre";
    private static final String ATT_DESCRIPTION = "description";
    private static final String ATT_LIEU = "lieu";



    private static final Long LONG_TEST = 1L;
    private static final String STRING_TEST = "non";

    @Test
    public void testBean() throws IntrospectionException {
        JavaBeanTester.test(Meeting.class);
    }

//    /**
//     * Teste le constructeur par défaut de la classe.
//     */
//    @Test
//    public void testConstructeurParDefaut() {
//        final Meeting meeting = new Meeting();
//        assertNull("L'attribut n'est pas nul", meeting.getIdReunion());
//    }
//
//    /**
//     * Teste la méthode public Long getRefUtilisateur().
//     *
//     * @throws NoSuchFieldException
//     * @throws IllegalAccessException
//     */
//    @Test
//    public void testGetRefUtilisateeur() throws NoSuchFieldException, IllegalAccessException {
//        final Meeting meeting = new Meeting();
//
//        final Field refUtilisateur = meeting.getClass().getDeclaredField(ATT_REF_UTILISATEUR);
//        refUtilisateur.setAccessible(true);
//
//        final Long resultatAttendu = LONG_TEST;
//        refUtilisateur.set(meeting, resultatAttendu);
//        final Long resultatTrouve = meeting.getRefUtilisateur();
//
//        assertEquals("Mauvais refUtilisateur", resultatAttendu, resultatTrouve);
//    }
//
//    /**
//     * Teste la méthode public void setRefUtilisateur (Long refUtilisateur).
//     *
//     * @throws NoSuchFieldException
//     * @throws IllegalAccessException
//     */
//    @Test
//    public void testSetRefUtilisateur() throws NoSuchFieldException, IllegalAccessException {
//        final Meeting meeting = new Meeting();
//
//        final Field refUtilisateur = meeting.getClass().getDeclaredField(ATT_REF_UTILISATEUR);
//        refUtilisateur.setAccessible(true);
//
//        final Long resultatAttendu = LONG_TEST;
//        refUtilisateur.set(meeting, resultatAttendu);
//        final Long resultatTrouve = (Long) refUtilisateur.get(meeting);
//
//        assertEquals("Mauvais refUtilisateur", resultatAttendu, resultatTrouve);
//    }
//
//
//    /**
//     * Teste la méthode public Long getIdReunion().
//     *
//     * @throws NoSuchFieldException
//     * @throws IllegalAccessException
//     */
//    @Test
//    public void testGetIdMeeting() throws NoSuchFieldException, IllegalAccessException {
//        final Meeting meeting = new Meeting();
//
//        final Field idMeeting = meeting.getClass().getDeclaredField(ATT_ID_MEETING);
//        idMeeting.setAccessible(true);
//
//        final Long resultatAttendu = LONG_TEST;
//        idMeeting.set(meeting, resultatAttendu);
//        final Long resultatTrouve = meeting.getIdReunion();
//
//        assertEquals("Mauvais refUtilisateur", resultatAttendu, resultatTrouve);
//    }
//
//    /**
//     * Teste la méthode public void setIdReunion (Long idMeeting).
//     *
//     * @throws NoSuchFieldException
//     * @throws IllegalAccessException
//     */
//    @Test
//    public void testSetIdMeeting() throws NoSuchFieldException, IllegalAccessException {
//        final Meeting meeting = new Meeting();
//
//        final Field idMeeting = meeting.getClass().getDeclaredField(ATT_ID_MEETING);
//        idMeeting.setAccessible(true);
//
//        final Long resultatAttendu = LONG_TEST;
//        idMeeting.set(meeting, resultatAttendu);
//        final Long resultatTrouve = (Long) idMeeting.get(meeting);
//
//        assertEquals("Mauvais refUtilisateur", resultatAttendu, resultatTrouve);
//    }
//
//    /**
//     * Teste de la méthode public Integer getDateReunion().
//     *
//     * @throws NoSuchFieldException
//     * @throws IllegalAccessException
//     */
//    @Test
//    public void testGetDateReunion() throws NoSuchFieldException, IllegalAccessException {
//        final Meeting meeting = new Meeting();
//
//        final Field dateReunion = meeting.getClass().getDeclaredField(ATT_DATE_REUNION);
//        dateReunion.setAccessible(true);
//
//        final String resultatAttendu = STRING_TEST;
//        dateReunion.set(meeting, resultatAttendu);
//        final String resultatTrouve = meeting.getDatePoster();
//
//        assertEquals("Mauvaise annee", resultatAttendu, resultatTrouve);
//    }
//    /**
//     * Teste de la méthode public void setDateReunion(String dateReunion).
//     *
//     * @throws NoSuchFieldException
//     * @throws IllegalAccessException
//     */
//    @Test
//    public void testSetDateReunion() throws NoSuchFieldException, IllegalAccessException {
//        final Meeting meeting = new Meeting();
//
//        final Field dateReunion = meeting.getClass().getDeclaredField(ATT_DATE_REUNION);
//        dateReunion.setAccessible(true);
//
//        final String resultatAttendu = STRING_TEST;
//        meeting.setDatePoster(resultatAttendu);
//        final String resultatTrouve = (String) dateReunion.get(meeting);
//
//        assertEquals("Mauvaise annee", resultatAttendu, resultatTrouve);
//    }
//
//    /**
//     * Teste de la méthode public Integer getCompteRendu().
//     *
//     * @throws NoSuchFieldException
//     * @throws IllegalAccessException
//     */
//    @Test
//    public void testGetCompteRendu() throws NoSuchFieldException, IllegalAccessException {
//        final Meeting meeting = new Meeting();
//
//        final Field compteRendu = meeting.getClass().getDeclaredField(ATT_COMPTE_RENDU);
//        compteRendu.setAccessible(true);
//
//        final String resultatAttendu = STRING_TEST;
//        compteRendu.set(meeting, resultatAttendu);
//        final String resultatTrouve = meeting.getCompteRendu();
//
//        assertEquals("Mauvaise annee", resultatAttendu, resultatTrouve);
//    }
//    /**
//     * Teste de la méthode public void setCompteRendu(String compteRendu).
//     *
//     * @throws NoSuchFieldException
//     * @throws IllegalAccessException
//     */
//    @Test
//    public void testSetCompteRendu() throws NoSuchFieldException, IllegalAccessException {
//        final Meeting meeting = new Meeting();
//
//        final Field compteRendu = meeting.getClass().getDeclaredField(ATT_COMPTE_RENDU);
//        compteRendu.setAccessible(true);
//
//        final String resultatAttendu = STRING_TEST;
//        meeting.setCompteRendu(resultatAttendu);
//        final String resultatTrouve = (String) compteRendu.get(meeting);
//
//        assertEquals("Mauvaise annee", resultatAttendu, resultatTrouve);
//    }
//
//    /**
//     * Teste de la méthode public Integer getTitre().
//     *
//     * @throws NoSuchFieldException
//     * @throws IllegalAccessException
//     */
//    @Test
//    public void testGetTitre() throws NoSuchFieldException, IllegalAccessException {
//        final Meeting meeting = new Meeting();
//
//        final Field titre = meeting.getClass().getDeclaredField(ATT_TITRE);
//        titre.setAccessible(true);
//
//        final String resultatAttendu = STRING_TEST;
//        titre.set(meeting, resultatAttendu);
//        final String resultatTrouve = meeting.getTitre();
//
//        assertEquals("Mauvaise annee", resultatAttendu, resultatTrouve);
//    }
//    /**
//     * Teste de la méthode public void setCompteRendu(String compteRendu).
//     *
//     * @throws NoSuchFieldException
//     * @throws IllegalAccessException
//     */
//    @Test
//    public void testSetTitre() throws NoSuchFieldException, IllegalAccessException {
//        final Meeting meeting = new Meeting();
//
//        final Field titre = meeting.getClass().getDeclaredField(ATT_TITRE);
//        titre.setAccessible(true);
//
//        final String resultatAttendu = STRING_TEST;
//        meeting.setTitre(resultatAttendu);
//        final String resultatTrouve = (String) titre.get(meeting);
//
//        assertEquals("Mauvaise annee", resultatAttendu, resultatTrouve);
//    }
//
//
//    /**
//     * Teste de la méthode public Integer getDescription().
//     *
//     * @throws NoSuchFieldException
//     * @throws IllegalAccessException
//     */
//    @Test
//    public void testGetDescription() throws NoSuchFieldException, IllegalAccessException {
//        final Meeting meeting = new Meeting();
//
//        final Field description = meeting.getClass().getDeclaredField(ATT_DESCRIPTION);
//        description.setAccessible(true);
//
//        final String resultatAttendu = STRING_TEST;
//        description.set(meeting, resultatAttendu);
//        final String resultatTrouve = meeting.getDescription();
//
//        assertEquals("Mauvaise annee", resultatAttendu, resultatTrouve);
//    }
//    /**
//     * Teste de la méthode public void setLieun(String lieu).
//     *
//     * @throws NoSuchFieldException
//     * @throws IllegalAccessException
//     */
//    @Test
//    public void testSetDescription() throws NoSuchFieldException, IllegalAccessException {
//        final Meeting meeting = new Meeting();
//
//        final Field description = meeting.getClass().getDeclaredField(ATT_DESCRIPTION);
//        description.setAccessible(true);
//
//        final String resultatAttendu = STRING_TEST;
//        meeting.setDescription(resultatAttendu);
//        final String resultatTrouve = (String) description.get(meeting);
//
//        assertEquals("Mauvaise annee", resultatAttendu, resultatTrouve);
//    }
//
//    /**
//     * Teste de la méthode public Integer getDescription().
//     *
//     * @throws NoSuchFieldException
//     * @throws IllegalAccessException
//     */
//    @Test
//    public void testGetLieu() throws NoSuchFieldException, IllegalAccessException {
//        final Meeting meeting = new Meeting();
//
//        final Field lieu = meeting.getClass().getDeclaredField(ATT_LIEU);
//        lieu.setAccessible(true);
//
//        final String resultatAttendu = STRING_TEST;
//        lieu.set(meeting, resultatAttendu);
//        final String resultatTrouve = meeting.getLieu();
//
//        assertEquals("Mauvaise annee", resultatAttendu, resultatTrouve);
//    }
//    /**
//     * Teste de la méthode public void setCompteRendu(String compteRendu).
//     *
//     * @throws NoSuchFieldException
//     * @throws IllegalAccessException
//     */
//    @Test
//    public void testSetLieu() throws NoSuchFieldException, IllegalAccessException {
//        final Meeting meeting = new Meeting();
//
//        final Field lieu= meeting.getClass().getDeclaredField(ATT_LIEU);
//        lieu.setAccessible(true);
//
//        final String resultatAttendu = STRING_TEST;
//        meeting.setLieu(resultatAttendu);
//        final String resultatTrouve = (String) lieu.get(meeting);
//
//        assertEquals("Mauvaise annee", resultatAttendu, resultatTrouve);
//    }
//
//
    /**
     * Teste la méthode public String toString().
     */
    @Test
    public void testToString() {
        final Meeting meeting = new Meeting();
        meeting.setRefUtilisateur(LONG_TEST);
        meeting.setIdReunion(LONG_TEST);
        meeting.setLieu(STRING_TEST);
        meeting.setDescription(STRING_TEST);
        meeting.setCompteRendu(STRING_TEST);
        meeting.setTitre(STRING_TEST);
        meeting.setDate(STRING_TEST);


        final String resultatAttendu = "Meeting{idReunion=" + LONG_TEST + ", dateReunion=" + STRING_TEST + ", compteRendu="
                + STRING_TEST + ", titre="
                + STRING_TEST + ", description="
                + STRING_TEST + ", lieu="
                + STRING_TEST + ", refUtilisateur="
                + LONG_TEST +"}";
        final String resultatTrouve = meeting.toString();

        assertEquals("Mauvais toString()", resultatAttendu, resultatTrouve);
    }


}
