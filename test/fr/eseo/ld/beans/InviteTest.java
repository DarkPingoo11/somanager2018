package fr.eseo.ld.beans;

import org.junit.Test;

import java.lang.reflect.Field;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

/**
 * Classe de tests unitaires JUnit 4 de la classe Invite.
 *
 * <p>Utilisation de la réflexivité.</p>
 *
 * @author Dimitri J
 * @version 1.0
 * @see fr.eseo.ld.beans.Invite
 * @see org.junit
 */
public class InviteTest {

    private static final String ATT_REF_UTILISATEUR = "refUtilisateur";
    private static final String ATT_REF_REUNION = "refReunion";
    private static final String ATT_PARTICIPE = "participe";


    private static final Long LONG_TEST = 1L;
    private static final String STRING_TEST = "non";


    /**
     * Teste le constructeur par défaut de la classe.
     */
    @Test
    public void testConstructeurParDefaut() {
        final Invite invite = new Invite();
        assertNull("L'attribut n'est pas nul", invite.getRefUtilisateur());
    }

    /**
     * Teste la méthode public Long getRefUtilisateur().
     *
     * @throws NoSuchFieldException
     * @throws IllegalAccessException
     */
    @Test
    public void testGetRefUtilisateeur() throws NoSuchFieldException, IllegalAccessException {
        final Invite invite = new Invite();

        final Field refUtilisateur = invite.getClass().getDeclaredField(ATT_REF_UTILISATEUR);
        refUtilisateur.setAccessible(true);

        final Long resultatAttendu = LONG_TEST;
        refUtilisateur.set(invite, resultatAttendu);
        final Long resultatTrouve = invite.getRefUtilisateur();

        assertEquals("Mauvais refUtilisateur", resultatAttendu, resultatTrouve);
    }

    /**
     * Teste la méthode public void setRefUtilisateur (Long refUtilisateur).
     *
     * @throws NoSuchFieldException
     * @throws IllegalAccessException
     */
    @Test
    public void testSetRefUtilisateur() throws NoSuchFieldException, IllegalAccessException {
        final Invite invite = new Invite();

        final Field refUtilisateur = invite.getClass().getDeclaredField(ATT_REF_UTILISATEUR);
        refUtilisateur.setAccessible(true);

        final Long resultatAttendu = LONG_TEST;
        refUtilisateur.set(invite, resultatAttendu);
        final Long resultatTrouve = (Long) refUtilisateur.get(invite);

        assertEquals("Mauvais refUtilisateur", resultatAttendu, resultatTrouve);
    }

    /**
     * Teste de la méthode public Long getReunion().
     *
     * @throws NoSuchFieldException
     * @throws IllegalAccessException
     */
    @Test
    public void testGetAReunion() throws NoSuchFieldException, IllegalAccessException {
        final Invite invite = new Invite();

        final Field refReunion = invite.getClass().getDeclaredField(ATT_REF_REUNION);
        refReunion.setAccessible(true);

        final Long resultatAttendu = LONG_TEST;
        refReunion.set(invite, resultatAttendu);
        final Long resultatTrouve = (Long) invite.getRefReunion();

        assertEquals("Mauvaise refReuion", resultatAttendu, resultatTrouve);
    }

    /**
     * Teste la méthode public void setRefReunion (Long refReunion).
     *
     * @throws NoSuchFieldException
     * @throws IllegalAccessException
     */
    @Test
    public void testSetRefReunion() throws NoSuchFieldException, IllegalAccessException {
        final Invite invite = new Invite();

        final Field refReunion = invite.getClass().getDeclaredField(ATT_REF_REUNION);
        refReunion.setAccessible(true);

        final Long resultatAttendu = LONG_TEST;
        refReunion.set(invite, resultatAttendu);
        final Long resultatTrouve = (Long) refReunion.get(invite);

        assertEquals("Mauvais refReunion", resultatAttendu, resultatTrouve);
    }

    /**
     * Teste de la méthode public String getParticipe().
     *
     * @throws NoSuchFieldException
     * @throws IllegalAccessException
     */
    @Test
    public void testGetParticipe() throws NoSuchFieldException, IllegalAccessException {
        final Invite invite = new Invite();

        final Field participe = invite.getClass().getDeclaredField(ATT_PARTICIPE);
        participe.setAccessible(true);

        final String resultatAttendu = STRING_TEST;
        participe.set(invite, resultatAttendu);
        final String resultatTrouve = invite.getParticipe();

        assertEquals("M participe", resultatAttendu, resultatTrouve);
    }

    /**
     * Teste de la méthode public void setParticipe(String participe).
     *
     * @throws NoSuchFieldException
     * @throws IllegalAccessException
     */
    @Test
    public void testSetParticipe() throws NoSuchFieldException, IllegalAccessException {
        final Invite invite = new Invite();

        final Field participe = invite.getClass().getDeclaredField(ATT_PARTICIPE);
        participe.setAccessible(true);

        final String resultatAttendu = STRING_TEST;
        invite.setParticipe(resultatAttendu);
        final String resultatTrouve = (String) participe.get(invite);

        assertEquals("Mauvais contratPro", resultatAttendu, resultatTrouve);
    }


    /**
     * Teste la méthode public String toString().
     */
    @Test
    public void testToString() {
        final Invite invite = new Invite();
        invite.setRefUtilisateur(LONG_TEST);
        invite.setRefReunion(LONG_TEST);
        invite.setParticipe(STRING_TEST);


        final String resultatAttendu = "Invite{refReunion=" + LONG_TEST + ", refUtilisateur=" + LONG_TEST + ", participe="
                + STRING_TEST + "}";
        final String resultatTrouve = invite.toString();

        assertEquals("Mauvais toString()", resultatAttendu, resultatTrouve);
    }

}
