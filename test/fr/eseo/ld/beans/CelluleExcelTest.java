package fr.eseo.ld.beans;

import static org.junit.Assert.*;

import net.codebox.javabeantester.JavaBeanTester;

import java.beans.IntrospectionException;

import org.junit.Test;

public class CelluleExcelTest {

    @Test
    public void testBean() throws IntrospectionException {
        JavaBeanTester.test(CelluleExcel.class);
    }
}