package fr.eseo.ld.beans;

import net.codebox.javabeantester.JavaBeanTester;
import org.junit.Test;

import java.beans.IntrospectionException;

/**
 * Classe de test de ModeleExcelDAO
 *
 * @author Tristan LE GACQUE
 * @see ModeleExcel
 **/
public class ModeleExcelTest {

    @Test
    public void testBean() throws IntrospectionException {
        JavaBeanTester.test(ModeleExcel.class);
    }
}