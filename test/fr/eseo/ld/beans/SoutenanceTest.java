package fr.eseo.ld.beans;

import net.codebox.javabeantester.JavaBeanTester;
import org.junit.Test;

import java.beans.IntrospectionException;
import java.util.Date;

import static org.junit.Assert.assertEquals;

/**
 * Classe de tests unitaires JUnit 4 de la classe Soutenance.
 *
 * <p>Utilisation de la réflexivité.</p>
 *
 * @version 1.0
 * @author Maxime LENORMAND
 *
 * @see fr.eseo.ld.beans.Soutenance
 * @see org.junit
 */
public class SoutenanceTest {
	
	private static final String STRING_TEST = "test";
	private static final Long LONG_TEST = 1L;
	private static final Date DATE_TEST = new Date(1L);
	
	private static final String ATT_ID_SOUTENANCE = "idSoutenance";
	private static final String ATT_DATE_SOUTENANCE = "dateSoutenance";
	private static final String ATT_ID_JURY = "idJury";
	private static final String ATT_ID_EQUIPE = "idEquipe";

    @Test
    public void testBean() throws IntrospectionException {
        JavaBeanTester.test(Soutenance.class);
    }
	

	/**
	 * Teste la méthode public String toString().
	 */
	@Test
	public void testToString() {
		final Soutenance soutenance = new Soutenance();
		soutenance.setIdSoutenance(LONG_TEST);
		soutenance.setDateSoutenance(DATE_TEST);
		soutenance.setIdJurySoutenance(STRING_TEST);
		soutenance.setIdEquipe(STRING_TEST);

		final String resultatAttendu = "Soutenance [idSoutenance=" + LONG_TEST + ", dateSoutenance=" + DATE_TEST
				+ ", idJurySoutenance=" + STRING_TEST + ", idEquipe=" + STRING_TEST + "]";
		final String resultatTrouve = soutenance.toString();

		assertEquals("Mauvais toString()", resultatAttendu, resultatTrouve);
	}
	
}