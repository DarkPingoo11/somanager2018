package fr.eseo.ld.beans;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.lang.reflect.Field;

import org.junit.Test;

/**
 * Classe de tests unitaires JUnit 4 de la classe NoteSoutenance.
 *
 * <p>Utilisation de la réflexivité.</p>
 *
 * @version 1.0
 * @author Maxime LENORMAND et Thomas MENARD
 *
 * @see fr.eseo.ld.beans.NotePoster
 * @see org.junit
 */
public class NoteSoutenanceTest {
	
	private static final Long LONG_TEST = 1L;
	private static final Float FLOAT_TEST = 1.0F;

	private static final String ATT_ID_PROFESSEUR = "idProfesseur";
	private static final String ATT_ID_SOUTENANCE = "idSoutenance";
	private static final String ATT_NOTE = "note";
	
	/**
	 * Teste le constructeur par défaut de la classe.
	 */
	@Test
	public void testConstructeurParDefaut() {
		final NoteSoutenance noteSoutenance = new NoteSoutenance();

		assertNull("L'attribut n'est pas nul", noteSoutenance.getIdEtudiant());
		assertNull("L'attribut n'est pas nul", noteSoutenance.getIdProfesseur());
		assertNull("L'attribut n'est pas nul", noteSoutenance.getIdSoutenance());
		assertNull("L'attribut n'est pas nul", noteSoutenance.getNote());
	}
	
	/**
	 * Teste la méthode public Long getIdProfesseur().
	 * 
	 * @throws Exception
	 */
	@Test
	public void testGetIdProfesseur() throws Exception {
		final NoteSoutenance noteSoutenance = NoteSoutenance.class.getDeclaredConstructor().newInstance();

		final Field idProfesseur = noteSoutenance.getClass().getDeclaredField(ATT_ID_PROFESSEUR);
		idProfesseur.setAccessible(true);

		final Long resultatAttendu = LONG_TEST;
		idProfesseur.set(noteSoutenance, resultatAttendu);
		final Long resultatTrouve = noteSoutenance.getIdProfesseur();

		assertEquals("Mauvais idProfesseur", resultatAttendu, resultatTrouve);
	}

	/**
	 * Teste la méthode public void setIdProfesseur(Long idProfesseur).
	 * 
	 * @throws Exception
	 */
	@Test
	public void testSetIdProfesseur() throws Exception {
		final NoteSoutenance noteSoutenance = NoteSoutenance.class.getDeclaredConstructor().newInstance();

		final Field idProfesseur = noteSoutenance.getClass().getDeclaredField(ATT_ID_PROFESSEUR);
		idProfesseur.setAccessible(true);

		final Long resultatAttendu = LONG_TEST;
		noteSoutenance.setIdProfesseur(resultatAttendu);
		final Long resultatTrouve = (Long) idProfesseur.get(noteSoutenance);

		assertEquals("Mauvais idProfesseur", resultatAttendu, resultatTrouve);
	}
	
	/**
	 * Teste la méthode public Long getIdSoutenance().
	 * 
	 * @throws Exception
	 */
	@Test
	public void testGetIdSoutenance() throws Exception {
		final NoteSoutenance noteSoutenance = NoteSoutenance.class.getDeclaredConstructor().newInstance();

		final Field idSoutenance = noteSoutenance.getClass().getDeclaredField(ATT_ID_SOUTENANCE);
		idSoutenance.setAccessible(true);

		final Long resultatAttendu = LONG_TEST;
		idSoutenance.set(noteSoutenance, resultatAttendu);
		final Long resultatTrouve = noteSoutenance.getIdSoutenance();

		assertEquals("Mauvais idSoutenance", resultatAttendu, resultatTrouve);
	}

	/**
	 * Teste la méthode public void setIdSoutenance(Long idPoster).
	 * 
	 * @throws Exception
	 */
	@Test
	public void testSetIdSoutenance() throws Exception {
		final NoteSoutenance noteSoutenance = NoteSoutenance.class.getDeclaredConstructor().newInstance();

		final Field idSoutenance = noteSoutenance.getClass().getDeclaredField(ATT_ID_SOUTENANCE);
		idSoutenance.setAccessible(true);

		final Long resultatAttendu = LONG_TEST;
		noteSoutenance.setIdSoutenance(resultatAttendu);
		final Long resultatTrouve = (Long) idSoutenance.get(noteSoutenance);

		assertEquals("Mauvais idSoutenance", resultatAttendu, resultatTrouve);
	}
	
	/**
	 * Teste la méthode public Long getNote().
	 * 
	 * @throws Exception
	 */
	@Test
	public void testGetNote() throws Exception {
		final NoteSoutenance noteSoutenance = NoteSoutenance.class.getDeclaredConstructor().newInstance();

		final Field note = noteSoutenance.getClass().getDeclaredField(ATT_NOTE);
		note.setAccessible(true);

		final Float resultatAttendu = FLOAT_TEST;
		note.set(noteSoutenance, resultatAttendu);
		final Float resultatTrouve = noteSoutenance.getNote();

		assertEquals("Mauvaise note", resultatAttendu, resultatTrouve);
	}

	/**
	 * Teste la méthode public void setNote(Integer note).
	 * 
	 * @throws Exception
	 */
	@Test
	public void testSetNote() throws Exception {
		final NoteSoutenance noteSoutenance = NoteSoutenance.class.getDeclaredConstructor().newInstance();

		final Field note = noteSoutenance.getClass().getDeclaredField(ATT_NOTE);
		note.setAccessible(true);

		final Float resultatAttendu = FLOAT_TEST;
		noteSoutenance.setNote(resultatAttendu);
		final Float resultatTrouve = (Float) note.get(noteSoutenance);

		assertEquals("Mauvaise note", resultatAttendu, resultatTrouve);
	}
	
	/**
	 * Teste la méthode public String toString().
	 */
	@Test
	public void testToString() {
		final NoteSoutenance noteSoutenance = new NoteSoutenance();
		noteSoutenance.setIdProfesseur(LONG_TEST);
		noteSoutenance.setIdEtudiant(LONG_TEST);
		noteSoutenance.setIdSoutenance(LONG_TEST);
		noteSoutenance.setNote(FLOAT_TEST);

		final String resultatAttendu = "NoteSoutenance [idProfesseur=" + LONG_TEST + ", idEtudiant=" + LONG_TEST
				+ ", idSoutenance=" + LONG_TEST + ", note=" + FLOAT_TEST + "]";
		final String resultatTrouve = noteSoutenance.toString();

		assertEquals("Mauvais toString()", resultatAttendu, resultatTrouve);
	}

}