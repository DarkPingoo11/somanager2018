package fr.eseo.ld.beans;

import net.codebox.javabeantester.JavaBeanTester;
import org.junit.Test;

import java.beans.IntrospectionException;

/**
 * Classe de test de DemandeJuryDAO
 *
 * @author Tristan LE GACQUE
 * @see DemandeJury
 **/
public class DemandeJuryTest {

    @Test
    public void testBean() throws IntrospectionException {
        JavaBeanTester.test(DemandeJury.class);
    }
}