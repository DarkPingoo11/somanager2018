package fr.eseo.ld.beans;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.lang.reflect.Field;

import org.junit.Test;

/**
 * Classe de tests unitaires JUnit 4 de la classe NotePoster.
 *
 * <p>Utilisation de la réflexivité.</p>
 *
 * @version 1.0
 * @author Maxime LENORMAND
 *
 * @see fr.eseo.ld.beans.NotePoster
 * @see org.junit
 */
public class NotePosterTest {
	
	private static final Float FLOAT_TEST = (float) 1;
	private static final Long LONG_TEST = 1L;

	private static final String ATT_ID_PROFESSEUR = "idProfesseur";
	private static final String ATT_ID_POSTER = "idPoster";
	private static final String ATT_ID_ETUDIANT = "idEtudiant";
	private static final String ATT_NOTE = "note";
	
	/**
	 * Teste le constructeur par défaut de la classe.
	 */
	@Test
	public void testConstructeurParDefaut() {
		final NotePoster notePoster = new NotePoster();

		assertNull("L'attribut n'est pas nul", notePoster.getIdPoster());
	}
	
	/**
	 * Teste la méthode public Long getIdProfesseur().
	 * 
	 * @throws Exception
	 */
	@Test
	public void testGetIdProfesseur() throws Exception {
		final NotePoster notePoster = NotePoster.class.getDeclaredConstructor().newInstance();

		final Field idProfesseur = notePoster.getClass().getDeclaredField(ATT_ID_PROFESSEUR);
		idProfesseur.setAccessible(true);

		final Long resultatAttendu = LONG_TEST;
		idProfesseur.set(notePoster, resultatAttendu);
		final Long resultatTrouve = notePoster.getIdProfesseur();

		assertEquals("Mauvais idProfesseur", resultatAttendu, resultatTrouve);
	}

	/**
	 * Teste la méthode public void setIdProfesseur(Long idProfesseur).
	 * 
	 * @throws Exception
	 */
	@Test
	public void testSetIdProfesseur() throws Exception {
		final NotePoster notePoster = NotePoster.class.getDeclaredConstructor().newInstance();

		final Field idProfesseur = notePoster.getClass().getDeclaredField(ATT_ID_PROFESSEUR);
		idProfesseur.setAccessible(true);

		final Long resultatAttendu = LONG_TEST;
		notePoster.setIdProfesseur(resultatAttendu);
		final Long resultatTrouve = (Long) idProfesseur.get(notePoster);

		assertEquals("Mauvais idProfesseur", resultatAttendu, resultatTrouve);
	}
	
	/**
	 * Teste la méthode public Long getIdPoster().
	 * 
	 * @throws Exception
	 */
	@Test
	public void testGetIdPoster() throws Exception {
		final NotePoster notePoster = NotePoster.class.getDeclaredConstructor().newInstance();

		final Field idPoster = notePoster.getClass().getDeclaredField(ATT_ID_POSTER);
		idPoster.setAccessible(true);

		final Long resultatAttendu = LONG_TEST;
		idPoster.set(notePoster, resultatAttendu);
		final Long resultatTrouve = notePoster.getIdPoster();

		assertEquals("Mauvais idPoster", resultatAttendu, resultatTrouve);
	}

	/**
	 * Teste la méthode public void setIdPoster(Long idPoster).
	 * 
	 * @throws Exception
	 */
	@Test
	public void testSetIdPoster() throws Exception {
		final NotePoster notePoster = NotePoster.class.getDeclaredConstructor().newInstance();

		final Field idPoster = notePoster.getClass().getDeclaredField(ATT_ID_POSTER);
		idPoster.setAccessible(true);

		final Long resultatAttendu = LONG_TEST;
		notePoster.setIdPoster(resultatAttendu);
		final Long resultatTrouve = (Long) idPoster.get(notePoster);

		assertEquals("Mauvais idPoster", resultatAttendu, resultatTrouve);
	}
	
	/**
	 * Teste la méthode public Long getIdEtudiant().
	 * 
	 * @throws Exception
	 */
	@Test
	public void testGetIdEtudiant() throws Exception {
		final NotePoster notePoster = NotePoster.class.getDeclaredConstructor().newInstance();

		final Field idEtudiant = notePoster.getClass().getDeclaredField(ATT_ID_ETUDIANT);
		idEtudiant.setAccessible(true);

		final Long resultatAttendu = LONG_TEST;
		idEtudiant.set(notePoster, resultatAttendu);
		final Long resultatTrouve = notePoster.getIdEtudiant();

		assertEquals("Mauvais idEtudiant", resultatAttendu, resultatTrouve);
	}

	/**
	 * Teste la méthode public void setIdEtudiant(Long idEtudiant).
	 * 
	 * @throws Exception
	 */
	@Test
	public void testSetIdEtudiant() throws Exception {
		final NotePoster notePoster = NotePoster.class.getDeclaredConstructor().newInstance();

		final Field idEtudiant = notePoster.getClass().getDeclaredField(ATT_ID_ETUDIANT);
		idEtudiant.setAccessible(true);

		final Long resultatAttendu = LONG_TEST;
		notePoster.setIdEtudiant(resultatAttendu);
		final Long resultatTrouve = (Long) idEtudiant.get(notePoster);

		assertEquals("Mauvais idEtudiant", resultatAttendu, resultatTrouve);
	}
	
	/**
	 * Teste la méthode public Long getNote().
	 * 
	 * @throws Exception
	 */
	@Test
	public void testGetNote() throws Exception {
		final NotePoster notePoster = NotePoster.class.getDeclaredConstructor().newInstance();

		final Field note = notePoster.getClass().getDeclaredField(ATT_NOTE);
		note.setAccessible(true);

		final Float resultatAttendu = FLOAT_TEST;
		note.set(notePoster, resultatAttendu);
		final Float resultatTrouve = notePoster.getNote();

		assertEquals("Mauvaise note", resultatAttendu, resultatTrouve);
	}

	/**
	 * Teste la méthode public void setNote(Integer note).
	 * 
	 * @throws Exception
	 */
	@Test
	public void testSetNote() throws Exception {
		final NotePoster notePoster = NotePoster.class.getDeclaredConstructor().newInstance();

		final Field note = notePoster.getClass().getDeclaredField(ATT_NOTE);
		note.setAccessible(true);

		final Float resultatAttendu = FLOAT_TEST;
		notePoster.setNote(resultatAttendu);
		final Float resultatTrouve = (Float) note.get(notePoster);

		assertEquals("Mauvaise note", resultatAttendu, resultatTrouve);
	}
	
	/**
	 * Teste la méthode public String toString().
	 */
	@Test
	public void testToString() {
		final NotePoster notePoster = new NotePoster();
		notePoster.setIdProfesseur(LONG_TEST);
		notePoster.setIdPoster(LONG_TEST);
		notePoster.setIdEtudiant(LONG_TEST);
		notePoster.setNote(FLOAT_TEST);
		
		final String resultatAttendu = "NotePoster [idProfesseur=" + LONG_TEST + ", idPoster=" + LONG_TEST
				+ ", idEtudiant=" + LONG_TEST + ", note=" + FLOAT_TEST + "]";
		final String resultatTrouve = notePoster.toString();

		assertEquals("Mauvais toString()", resultatAttendu, resultatTrouve);
	}

}