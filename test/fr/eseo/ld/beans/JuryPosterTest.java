package fr.eseo.ld.beans;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.lang.reflect.Field;
import java.util.Date;

import org.junit.Test;

/**
 * Classe de tests unitaires JUnit 4 de la classe JuryPoster.
 *
 * <p>Utilisation de la réflexivité.</p>
 *
 * @version 1.0
 * @author Maxime LENORMAND
 *
 * @see fr.eseo.ld.beans.JuryPoster
 * @see org.junit
 */
public class JuryPosterTest {
	
	private static final String STRING_TEST = "test";
	private static final Long LONG_TEST = 1L;
	private static final Date DATE_TEST = new Date(1L);
	
	private static final String ATT_ID_JURY_POSTER = "idJuryPoster";
	private static final String ATT_ID_PROF_1 = "idProf1";
	private static final String ATT_ID_PROF_2 = "idProf2";
	private static final String ATT_ID_PROF_3 = "idProf3";
	private static final String ATT_ID_EQUIPE = "idEquipe";
	private static final String ATT_DATE = "date";

	/**
	 * Teste le constructeur par défaut de la classe.
	 */
	@Test
	public void testConstructeurParDefaut() {
		final JuryPoster juryPoster = new JuryPoster();

		assertNull("L'attribut n'est pas nul", juryPoster.getIdJuryPoster());
	}
	
	/**
	 * Teste la méthode public String getIdJuryPoster().
	 * 
	 * @throws NoSuchFieldException 
	 * @throws IllegalAccessException 
	 */
	@Test
	public void testGetIdJuryPoster() throws NoSuchFieldException, IllegalAccessException {
		final JuryPoster juryPoster = new JuryPoster();

		final Field idJuryPoster = juryPoster.getClass().getDeclaredField(ATT_ID_JURY_POSTER);
		idJuryPoster.setAccessible(true);

		final String resultatAttendu = STRING_TEST;
		idJuryPoster.set(juryPoster, resultatAttendu);
		final String resultatTrouve = juryPoster.getIdJuryPoster();

		assertEquals("Mauvais idJuryPoster", resultatAttendu, resultatTrouve);
	}
	
	/**
	 * Teste la méthode public void setIdJuryPoster(String idJuryPoster).
	 * 
	 * @throws NoSuchFieldException 
	 * @throws IllegalAccessException 
	 */
	@Test
	public void testSetIdJuryPoster() throws NoSuchFieldException, IllegalAccessException {
		final JuryPoster juryPoster = new JuryPoster();

		final Field idJuryPoster = juryPoster.getClass().getDeclaredField(ATT_ID_JURY_POSTER);
		idJuryPoster.setAccessible(true);

		final String resultatAttendu = STRING_TEST;
		juryPoster.setIdJuryPoster(resultatAttendu);
		final String resultatTrouve = (String) idJuryPoster.get(juryPoster);

		assertEquals("Mauvais idJuryPoster", resultatAttendu, resultatTrouve);
	}
	
	/**
	 * Teste la méthode public Long getIdProf1().
	 * 
	 * @throws NoSuchFieldException 
	 * @throws IllegalAccessException 
	 */
	@Test
	public void testGetIdProf1() throws NoSuchFieldException, IllegalAccessException {
		final JuryPoster juryPoster = new JuryPoster();
		
        final Field idProf1 = juryPoster.getClass().getDeclaredField(ATT_ID_PROF_1);
        idProf1.setAccessible(true);
        
        final Long resultatAttendu = LONG_TEST;
        idProf1.set(juryPoster, resultatAttendu);
        final  Long resultatTrouve = juryPoster.getIdProf1();
		
		assertEquals("Mauvais idProf1", resultatAttendu, resultatTrouve);
	}
	
	/**
	 * Teste la méthode public void setIdProf1(Long idProf1).
	 * 
	 * @throws NoSuchFieldException 
	 * @throws IllegalAccessException 
	 */
	@Test
	public void testSetIdProf1() throws NoSuchFieldException, IllegalAccessException {
		final JuryPoster juryPoster = new JuryPoster();
		
        final Field idProf1 = juryPoster.getClass().getDeclaredField(ATT_ID_PROF_1);
        idProf1.setAccessible(true);
        
        final Long resultatAttendu = LONG_TEST;
        juryPoster.setIdProf1(resultatAttendu);
        final Long resultatTrouve = (Long) idProf1.get(juryPoster);
		
		assertEquals("Mauvais idProf1", resultatAttendu, resultatTrouve);
	}

	/**
	 * Teste la méthode public Long getIdProf2().
	 * 
	 * @throws NoSuchFieldException 
	 * @throws IllegalAccessException 
	 */
	@Test
	public void testGetIdProf2() throws NoSuchFieldException, IllegalAccessException {
		final JuryPoster juryPoster = new JuryPoster();
		
        final Field idProf2 = juryPoster.getClass().getDeclaredField(ATT_ID_PROF_2);
        idProf2.setAccessible(true);
        
        final Long resultatAttendu = LONG_TEST;
        idProf2.set(juryPoster, resultatAttendu);
        final  Long resultatTrouve = juryPoster.getIdProf2();
		
		assertEquals("Mauvais idProf2", resultatAttendu, resultatTrouve);
	}
	
	/**
	 * Teste la méthode public void setIdProf2(Long idProf2).
	 * 
	 * @throws NoSuchFieldException 
	 * @throws IllegalAccessException 
	 */
	@Test
	public void testSetIdProf2() throws NoSuchFieldException, IllegalAccessException {
		final JuryPoster juryPoster = new JuryPoster();
		
        final Field idProf2 = juryPoster.getClass().getDeclaredField(ATT_ID_PROF_2);
        idProf2.setAccessible(true);
        
        final Long resultatAttendu = LONG_TEST;
        juryPoster.setIdProf2(resultatAttendu);
        final Long resultatTrouve = (Long) idProf2.get(juryPoster);
		
		assertEquals("Mauvais idProf2", resultatAttendu, resultatTrouve);
	}
	
	/**
	 * Teste la méthode public Long getIdProf3().
	 * 
	 * @throws NoSuchFieldException 
	 * @throws IllegalAccessException 
	 */
	@Test
	public void testGetIdProf3() throws NoSuchFieldException, IllegalAccessException {
		final JuryPoster juryPoster = new JuryPoster();
		
        final Field idProf3 = juryPoster.getClass().getDeclaredField(ATT_ID_PROF_3);
        idProf3.setAccessible(true);
        
        final Long resultatAttendu = LONG_TEST;
        idProf3.set(juryPoster, resultatAttendu);
        final Long resultatTrouve = juryPoster.getIdProf3();
		
		assertEquals("Mauvais idProf3", resultatAttendu, resultatTrouve);
	}
	
	/**
	 * Teste la méthode public void setIdProf3(Long idProf3).
	 * 
	 * @throws NoSuchFieldException 
	 * @throws IllegalAccessException 
	 */
	@Test
	public void testSetIdProf3() throws NoSuchFieldException, IllegalAccessException {
		final JuryPoster juryPoster = new JuryPoster();
		
        final Field idProf3 = juryPoster.getClass().getDeclaredField(ATT_ID_PROF_3);
        idProf3.setAccessible(true);
        
        final Long resultatAttendu = LONG_TEST;
        juryPoster.setIdProf3(resultatAttendu);
        final Long resultatTrouve = (Long) idProf3.get(juryPoster);
		
		assertEquals("Mauvais idProf3", resultatAttendu, resultatTrouve);
	}
	
	/**
	 * Teste la méthode public Date getDatePoster().
	 * 
	 * @throws NoSuchFieldException 
	 * @throws IllegalAccessException 
	 */
	@Test
	public void testGetDate() throws NoSuchFieldException, IllegalAccessException {
		final JuryPoster juryPoster = new JuryPoster();
		
        final Field date = juryPoster.getClass().getDeclaredField(ATT_DATE);
        date.setAccessible(true);
        
        final Date resultatAttendu = DATE_TEST;
        date.set(juryPoster, resultatAttendu);
        final Date resultatTrouve = juryPoster.getDate();
		
		assertEquals("Mauvaise date", resultatAttendu, resultatTrouve);
	}
	
	/**
	 * Teste la méthode public void setDatePoster(Date date).
	 * 
	 * @throws NoSuchFieldException 
	 * @throws IllegalAccessException 
	 */
	@Test
	public void testSetDate() throws NoSuchFieldException, IllegalAccessException {
		final JuryPoster juryPoster = new JuryPoster();
		
        final Field date = juryPoster.getClass().getDeclaredField(ATT_DATE);
        date.setAccessible(true);
        
        final Date resultatAttendu = DATE_TEST;
        juryPoster.setDate(resultatAttendu);
        final Date resultatTrouve = (Date) date.get(juryPoster);
		
		assertEquals("Mauvaise date", resultatAttendu, resultatTrouve);
	}
	
	/**
	 * Teste la méthode public String getIdEquipe().
	 * 
	 * @throws NoSuchFieldException 
	 * @throws IllegalAccessException 
	 */
	@Test
	public void testGetIdEquipe() throws NoSuchFieldException, IllegalAccessException {
		final JuryPoster juryPoster = new JuryPoster();

		final Field idEquipe = juryPoster.getClass().getDeclaredField(ATT_ID_EQUIPE);
		idEquipe.setAccessible(true);

		final String resultatAttendu = STRING_TEST;
		idEquipe.set(juryPoster, resultatAttendu);
		final String resultatTrouve = juryPoster.getIdEquipe();

		assertEquals("Mauvais idEquipe", resultatAttendu, resultatTrouve);
	}
	
	/**
	 * Teste la méthode public void setIdEquipe(String idEquipe).
	 * 
	 * @throws NoSuchFieldException 
	 * @throws IllegalAccessException 
	 */
	@Test
	public void testSetIdEquipe() throws NoSuchFieldException, IllegalAccessException {
		final JuryPoster juryPoster = new JuryPoster();

		final Field idEquipe = juryPoster.getClass().getDeclaredField(ATT_ID_EQUIPE);
		idEquipe.setAccessible(true);

		final String resultatAttendu = STRING_TEST;
		juryPoster.setIdEquipe(resultatAttendu);
		final String resultatTrouve = (String) idEquipe.get(juryPoster);

		assertEquals("Mauvais idEquipe", resultatAttendu, resultatTrouve);
	}
	
	/**
	 * Teste la méthode public String toString().
	 */
	@Test
	public void testToString() {
		final JuryPoster juryPoster = new JuryPoster();
		juryPoster.setIdJuryPoster(STRING_TEST);
		juryPoster.setIdProf1(LONG_TEST);
		juryPoster.setIdProf2(LONG_TEST);
		juryPoster.setIdProf3(LONG_TEST);
		juryPoster.setDate(DATE_TEST);
		juryPoster.setIdEquipe(STRING_TEST);

		final String resultatAttendu = "JuryPoster [idJuryPoster=" + STRING_TEST + ", idProf1=" + LONG_TEST
				+ ", idProf2=" + LONG_TEST + ", idProf3=" + LONG_TEST + ", date=" + DATE_TEST + ", idEquipe="
				+ STRING_TEST + "]";
		final String resultatTrouve = juryPoster.toString();

		assertEquals("Mauvais toString()", resultatAttendu, resultatTrouve);
	}

}