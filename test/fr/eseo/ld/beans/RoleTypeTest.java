package fr.eseo.ld.beans;

import net.codebox.javabeantester.JavaBeanTester;
import org.junit.Test;

import java.beans.IntrospectionException;

import static junit.framework.TestCase.assertEquals;

public class RoleTypeTest {

    private static final String NOM_APPLICATIF = "Applicatif";
    private static final String NOM_EQUIPE = "Equipe";
    @Test
    public void testBean() throws IntrospectionException {
        JavaBeanTester.test(RoleType.class);
    }

    @Test
    public void getNom() {
        assertEquals(RoleType.APPLICATIF.getNom(), NOM_APPLICATIF);
    }

    @Test
    public void getNom2() {
        assertEquals(RoleType.EQUIPE.getNom(), NOM_EQUIPE);
    }
}