package fr.eseo.ld.beans;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

/**
 * Suite de tests unitaires JUnit 4 pour le package fr.eseo.ld.beans.
 *
 * @author Maxime LENORMAND
 *
 * @see org.junit
 */
@RunWith(Suite.class)
@SuiteClasses({
		AnneeScolaireTest.class,
		CallbackTest.class,
		CelluleExcelTest.class,
		CommentaireTest.class,
		EquipeTest.class,
		EtatSujetTest.class,
		EtudiantEquipeTest.class,
		EtudiantTest.class,
		FiltreUtilisateurTest.class,
		FonctionProfesseurSujetTest.class,
		InviteTest.class,
		JuryPosterTest.class,
		JurySoutenanceTest.class,
		MeetingTest.class,
		ModeleExcelTest.class,
		NoteInteretTechnoTest.class,
		NoteInteretSujetTest.class,
		NoteSoutenanceTest.class,
		NotePosterTest.class,
		NotificationTest.class,
		OptionESEOTest.class,
		OptionSujetTest.class,
		ParametreTest.class,
		PartageRoleTest.class,
		PorteurSujetTest.class,
		PosterTest.class,
		ProfesseurSujetTest.class,
		ProfesseurTest.class,
		ResultatFiltreUtilisateurTest.class,
		RoleTest.class,
		RoleAffichageTest.class,
		RoleUtilisateurTest.class,
		RoleTypeTest.class,
		SoutenanceTest.class,
		SujetTest.class,
		UtilisateurTest.class
})
public class BeansTestSuite {
	/* Classe vide */
}