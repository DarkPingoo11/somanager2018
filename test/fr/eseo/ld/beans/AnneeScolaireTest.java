package fr.eseo.ld.beans;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.lang.reflect.Field;
import java.util.Date;

import org.junit.Test;

/**
 * Classe de tests unitaires JUnit 4 de la classe AnneeScolaire.
 *
 * <p>Utilisation de la réflexivité.</p>
 *
 * @version 1.0
 * @author Hugo Menard
 *
 * @see fr.eseo.ld.beans.AnneeScolaire
 * @see org.junit
 */
public class AnneeScolaireTest {
	
	private static final Integer INTEGER_TEST = 1;
	private static final Long LONG_TEST = 1L;
	private static final Date DATE_TEST = new Date(1L);
	
	private static final String ATTRIBUT_ID_ANNEE_SCOLAIRE = "idAnneeScolaire";
	private static final String ATTRIBUT_ANNEE_DEBUT = "anneeDebut";
	private static final String ATTRIBUT_ANNEE_FIN = "anneeFin";
	private static final String ATTRIBUT_DATE_DEBUT_PROJET = "dateDebutProjet";
	private static final String ATTRIBUT_DATE_MI_AVANCEMENT = "dateMiAvancement";
	private static final String ATTRIBUT_DATE_DEPOT_POSTER = "dateDepotPoster";
	private static final String ATTRIBUT_DATE_SOUTENANCE_FINALE = "dateSoutenanceFinale";
	private static final String ATTRIBUT_ID_OPTION = "idOption";

	/**
	 * Teste le constructeur par défaut de la classe.
	 */
	@Test
	public void testConstructeurParDefaut() {
		final AnneeScolaire anneeScolaire = new AnneeScolaire();

		assertNull("L'attribut n'est pas nul", anneeScolaire.getIdAnneeScolaire());
	}
	
	/**
	 * Teste la méthode public Long getIdAnneeScolaire().
	 * 
	 * @throws NoSuchFieldException 
	 * @throws IllegalAccessException 
	 */
	@Test
	public void testGetIdAnneeScolaire() throws NoSuchFieldException, IllegalAccessException {
		final AnneeScolaire anneeScolaire = new AnneeScolaire();
		
        final Field idAnneeScolaire = anneeScolaire.getClass().getDeclaredField(ATTRIBUT_ID_ANNEE_SCOLAIRE);
        idAnneeScolaire.setAccessible(true);
        
        final Long resultatAttendu = LONG_TEST;
        idAnneeScolaire.set(anneeScolaire, resultatAttendu);
        final Long resultatTrouve = anneeScolaire.getIdAnneeScolaire();
		
		assertEquals("Mauvais idAnneeScolaire", resultatAttendu, resultatTrouve);
	}
	
	/**
	 * Teste la méthode public void setIdAnneeScolaire(Long idAnneeScolaire).
	 * 
	 * @throws NoSuchFieldException 
	 * @throws IllegalAccessException 
	 */
	@Test
	public void testSetIdCommentaire() throws NoSuchFieldException, IllegalAccessException {
		final AnneeScolaire anneeScolaire = new AnneeScolaire();
		
        final Field idAnneeScolaire = anneeScolaire.getClass().getDeclaredField(ATTRIBUT_ID_ANNEE_SCOLAIRE);
        idAnneeScolaire.setAccessible(true);
        
        final Long resultatAttendu = LONG_TEST;
        anneeScolaire.setIdAnneeScolaire(resultatAttendu);
        final Long resultatTrouve = (Long) idAnneeScolaire.get(anneeScolaire);
		
		assertEquals("Mauvais idAnneeScolaire", resultatAttendu, resultatTrouve);
	}
	
	/**
	 * Teste la méthode public Integer getAnneeDebut().
	 * 
	 * @throws NoSuchFieldException 
	 * @throws IllegalAccessException 
	 */
	@Test
	public void testGetAnneeDebut() throws NoSuchFieldException, IllegalAccessException {
		final AnneeScolaire anneeScolaire = new AnneeScolaire();
		
        final Field anneeDebut = anneeScolaire.getClass().getDeclaredField(ATTRIBUT_ANNEE_DEBUT);
        anneeDebut.setAccessible(true);
        
        final Integer resultatAttendu = INTEGER_TEST;
        anneeDebut.set(anneeScolaire, resultatAttendu);
        final Integer resultatTrouve = anneeScolaire.getAnneeDebut();
		
		assertEquals("Mauvaise anneeDebut", resultatAttendu, resultatTrouve);
	}
	
	/**
	 * Teste la méthode public void setAnneeDebut(Integer anneeDebut).
	 * 
	 * @throws NoSuchFieldException 
	 * @throws IllegalAccessException 
	 */
	@Test
	public void testSetAnneeDebut() throws NoSuchFieldException, IllegalAccessException {
		final AnneeScolaire anneeScolaire = new AnneeScolaire();
		
        final Field anneeDebut = anneeScolaire.getClass().getDeclaredField(ATTRIBUT_ANNEE_DEBUT);
        anneeDebut.setAccessible(true);
        
        final Integer resultatAttendu = INTEGER_TEST;
        anneeScolaire.setAnneeDebut(resultatAttendu);
        final Integer resultatTrouve = (Integer) anneeDebut.get(anneeScolaire);
		
		assertEquals("Mauvaise anneeDebut", resultatAttendu, resultatTrouve);
	}
	
	/**
	 * Teste la méthode public Integer getAnneeFin().
	 * 
	 * @throws NoSuchFieldException 
	 * @throws IllegalAccessException 
	 */
	@Test
	public void testGetAnneeFin() throws NoSuchFieldException, IllegalAccessException {
		final AnneeScolaire anneeScolaire = new AnneeScolaire();
		
        final Field anneeFin = anneeScolaire.getClass().getDeclaredField(ATTRIBUT_ANNEE_FIN);
        anneeFin.setAccessible(true);
        
        final Integer resultatAttendu = INTEGER_TEST;
        anneeFin.set(anneeScolaire, resultatAttendu);
        final Integer resultatTrouve = anneeScolaire.getAnneeFin();
		
		assertEquals("Mauvaise anneeFin", resultatAttendu, resultatTrouve);
	}
	
	/**
	 * Teste la méthode public void setAnneeFin(Integer anneeFin).
	 * 
	 * @throws NoSuchFieldException 
	 * @throws IllegalAccessException 
	 */
	@Test
	public void testSetAnneeFin() throws NoSuchFieldException, IllegalAccessException {
		final AnneeScolaire anneeScolaire = new AnneeScolaire();
		
        final Field anneeFin = anneeScolaire.getClass().getDeclaredField(ATTRIBUT_ANNEE_FIN);
        anneeFin.setAccessible(true);
        
        final Integer resultatAttendu = INTEGER_TEST;
        anneeScolaire.setAnneeFin(resultatAttendu);
        final Integer resultatTrouve = (Integer) anneeFin.get(anneeScolaire);
		
		assertEquals("Mauvaise anneeFin", resultatAttendu, resultatTrouve);
	}
	
	/**
	 * Teste la méthode public Date getDateDebutProjet().
	 * 
	 * @throws NoSuchFieldException 
	 * @throws IllegalAccessException 
	 */
	@Test
	public void testGetDateDebutProjet() throws NoSuchFieldException, IllegalAccessException {
		final AnneeScolaire anneeScolaire = new AnneeScolaire();
		
        final Field dateDebutProjet = anneeScolaire.getClass().getDeclaredField(ATTRIBUT_DATE_DEBUT_PROJET);
        dateDebutProjet.setAccessible(true);
        
        final Date resultatAttendu = DATE_TEST;
        dateDebutProjet.set(anneeScolaire, resultatAttendu);
        final Date resultatTrouve = anneeScolaire.getDateDebutProjet();
		
		assertEquals("Mauvaise dateDebutProjet", resultatAttendu, resultatTrouve);
	}
	
	/**
	 * Teste la méthode public void setDateDebutProjet(Date dateDebutProjet).
	 * 
	 * @throws NoSuchFieldException 
	 * @throws IllegalAccessException 
	 */
	@Test
	public void testSetDateDebutProjet() throws NoSuchFieldException, IllegalAccessException {
		final AnneeScolaire anneeScolaire = new AnneeScolaire();
		
        final Field dateDebutProjet = anneeScolaire.getClass().getDeclaredField(ATTRIBUT_DATE_DEBUT_PROJET);
        dateDebutProjet.setAccessible(true);
        
        final Date resultatAttendu = DATE_TEST;
        anneeScolaire.setDateDebutProjet(resultatAttendu);
        final Date resultatTrouve = (Date) dateDebutProjet.get(anneeScolaire);
		
		assertEquals("Mauvaise dateDebutProjet", resultatAttendu, resultatTrouve);
	}
	
	/**
	 * Teste la méthode public Date getDateMiAvancement().
	 * 
	 * @throws NoSuchFieldException 
	 * @throws IllegalAccessException 
	 */
	@Test
	public void testGetDateMiAvancement() throws NoSuchFieldException, IllegalAccessException {
		final AnneeScolaire anneeScolaire = new AnneeScolaire();
		
        final Field dateMiAvancement = anneeScolaire.getClass().getDeclaredField(ATTRIBUT_DATE_MI_AVANCEMENT);
        dateMiAvancement.setAccessible(true);
        
        final Date resultatAttendu = DATE_TEST;
        dateMiAvancement.set(anneeScolaire, resultatAttendu);
        final Date resultatTrouve = anneeScolaire.getDateMiAvancement();
		
		assertEquals("Mauvaise dateMiAvancement", resultatAttendu, resultatTrouve);
	}
	
	/**
	 * Teste la méthode public void setDateMiAvancement(Date dateMiAvancement).
	 * 
	 * @throws NoSuchFieldException 
	 * @throws IllegalAccessException 
	 */
	@Test
	public void testSetDateMiAvancement() throws NoSuchFieldException, IllegalAccessException {
		final AnneeScolaire anneeScolaire = new AnneeScolaire();
		
        final Field dateMiAvancement = anneeScolaire.getClass().getDeclaredField(ATTRIBUT_DATE_MI_AVANCEMENT);
        dateMiAvancement.setAccessible(true);
        
        final Date resultatAttendu = DATE_TEST;
        anneeScolaire.setDateMiAvancement(resultatAttendu);
        final Date resultatTrouve = (Date) dateMiAvancement.get(anneeScolaire);
		
		assertEquals("Mauvaise dateMiAvancement", resultatAttendu, resultatTrouve);
	}
	
	/**
	 * Teste la méthode public Date getDateDepotPoster().
	 * 
	 * @throws NoSuchFieldException 
	 * @throws IllegalAccessException 
	 */
	@Test
	public void testGetDateDepotPoster() throws NoSuchFieldException, IllegalAccessException {
		final AnneeScolaire anneeScolaire = new AnneeScolaire();
		
        final Field dateDepotPoster = anneeScolaire.getClass().getDeclaredField(ATTRIBUT_DATE_DEPOT_POSTER);
        dateDepotPoster.setAccessible(true);
        
        final Date resultatAttendu = DATE_TEST;
        dateDepotPoster.set(anneeScolaire, resultatAttendu);
        final Date resultatTrouve = anneeScolaire.getDateDepotPoster();
		
		assertEquals("Mauvaise dateDepotPoster", resultatAttendu, resultatTrouve);
	}
	
	/**
	 * Teste la méthode public void setDateDepotPoster(Date dateDepotPoster).
	 * 
	 * @throws NoSuchFieldException 
	 * @throws IllegalAccessException 
	 */
	@Test
	public void testSetDateDepotPoster() throws NoSuchFieldException, IllegalAccessException {
		final AnneeScolaire anneeScolaire = new AnneeScolaire();
		
        final Field dateDepotPoster = anneeScolaire.getClass().getDeclaredField(ATTRIBUT_DATE_DEPOT_POSTER);
        dateDepotPoster.setAccessible(true);
        
        final Date resultatAttendu = DATE_TEST;
        anneeScolaire.setDateDepotPoster(resultatAttendu);
        final Date resultatTrouve = (Date) dateDepotPoster.get(anneeScolaire);
		
		assertEquals("Mauvaise dateDepotPoster", resultatAttendu, resultatTrouve);
	}
	
	/**
	 * Teste la méthode public Date getDateSoutenanceFinale().
	 * 
	 * @throws NoSuchFieldException 
	 * @throws IllegalAccessException 
	 */
	@Test
	public void testGetDateSoutenanceFinale() throws NoSuchFieldException, IllegalAccessException {
		final AnneeScolaire anneeScolaire = new AnneeScolaire();
		
        final Field dateSoutenanceFinale = anneeScolaire.getClass().getDeclaredField(ATTRIBUT_DATE_SOUTENANCE_FINALE);
        dateSoutenanceFinale.setAccessible(true);
        
        final Date resultatAttendu = DATE_TEST;
        dateSoutenanceFinale.set(anneeScolaire, resultatAttendu);
        final Date resultatTrouve = anneeScolaire.getDateSoutenanceFinale();
		
		assertEquals("Mauvaise dateSoutenanceFinale", resultatAttendu, resultatTrouve);
	}
	
	/**
	 * Teste la méthode public void setDateSoutenanceFinale(Date dateSoutenanceFinale).
	 * 
	 * @throws NoSuchFieldException 
	 * @throws IllegalAccessException 
	 */
	@Test
	public void testSetDateSoutenanceFinale() throws NoSuchFieldException, IllegalAccessException {
		final AnneeScolaire anneeScolaire = new AnneeScolaire();
		
        final Field dateSoutenanceFinale = anneeScolaire.getClass().getDeclaredField(ATTRIBUT_DATE_SOUTENANCE_FINALE);
        dateSoutenanceFinale.setAccessible(true);
        
        final Date resultatAttendu = DATE_TEST;
        anneeScolaire.setDateSoutenanceFinale(resultatAttendu);
        final Date resultatTrouve = (Date) dateSoutenanceFinale.get(anneeScolaire);
		
		assertEquals("Mauvaise dateSoutenanceFinale", resultatAttendu, resultatTrouve);
	}
	
	/**
	 * Teste la méthode public Long getIdOption().
	 * 
	 * @throws NoSuchFieldException 
	 * @throws IllegalAccessException 
	 */
	@Test
	public void testGetIdOption() throws NoSuchFieldException, IllegalAccessException {
		final AnneeScolaire anneeScolaire = new AnneeScolaire();
		
        final Field idOption = anneeScolaire.getClass().getDeclaredField(ATTRIBUT_ID_OPTION);
        idOption.setAccessible(true);
        
        final Long resultatAttendu = LONG_TEST;
        idOption.set(anneeScolaire, resultatAttendu);
        final Long resultatTrouve = anneeScolaire.getIdOption();
		
		assertEquals("Mauvais idOption", resultatAttendu, resultatTrouve);
	}
	
	/**
	 * Teste la méthode public void setIdOption(Long idOption).
	 * 
	 * @throws NoSuchFieldException 
	 * @throws IllegalAccessException 
	 */
	@Test
	public void testSetIdOption() throws NoSuchFieldException, IllegalAccessException {
		final AnneeScolaire anneeScolaire = new AnneeScolaire();
		
        final Field idOption = anneeScolaire.getClass().getDeclaredField(ATTRIBUT_ID_OPTION);
        idOption.setAccessible(true);
        
        final Long resultatAttendu = LONG_TEST;
        anneeScolaire.setIdOption(resultatAttendu);
        final Long resultatTrouve = (Long) idOption.get(anneeScolaire);
		
		assertEquals("Mauvais idOption", resultatAttendu, resultatTrouve);
	}
	
	/**
	 * Teste la méthode public String toString().
	 */
	@Test
	public void testToString() {
		final AnneeScolaire anneeScolaire = new AnneeScolaire();
		anneeScolaire.setIdAnneeScolaire(LONG_TEST);
		anneeScolaire.setAnneeDebut(INTEGER_TEST);
		anneeScolaire.setAnneeFin(INTEGER_TEST);
		anneeScolaire.setDateDebutProjet(DATE_TEST);
		anneeScolaire.setDateMiAvancement(DATE_TEST);
		anneeScolaire.setDateDepotPoster(DATE_TEST);
		anneeScolaire.setDateSoutenanceFinale(DATE_TEST);
		anneeScolaire.setIdOption(LONG_TEST);

		final String resultatAttendu = "AnneeScolaire [idAnneeScolaire=" + LONG_TEST + ", anneeDebut=" + INTEGER_TEST
				+ ", anneeFin=" + INTEGER_TEST + ", dateDebutProjet=" + DATE_TEST + ", dateMiAvancement=" + DATE_TEST
				+ ", dateDepotPoster=" + DATE_TEST + ", dateSoutenanceFinale=" + DATE_TEST + ", idOption=" + LONG_TEST
				+ "]";
		final String resultatTrouve = anneeScolaire.toString();

		assertEquals("Mauvais toString()", resultatAttendu, resultatTrouve);
	}

}