package fr.eseo.ld.beans;

import net.codebox.javabeantester.JavaBeanTester;
import org.junit.Test;

import java.beans.IntrospectionException;

import static org.junit.Assert.assertEquals;

/**
 * Classe de tests unitaires JUnit 4 de la classe Poster.
 *
 * <p>Utilisation de la réflexivité.</p>
 *
 * @version 1.0
 * @author Julie Avizou
 *
 * @see fr.eseo.ld.beans.Poster
 * @see org.junit
 */
public class PosterTest {
	
	private static final String STRING_TEST = "test";
	private static final Long LONG_TEST = 1L;
	
	private static final String ATT_ID_POSTER = "idPoster";
	private static final String ATT_CHEMIN = "chemin";
	private static final String ATT_DATE = "date";
	private static final String ATT_ID_SUJET = "idSujet";
	private static final String ATT_VALIDE = "valide";
	private static final String ATT_ID_EQUIPE = "idEquipe";
	

	@Test
    public void testBean() throws IntrospectionException {
        JavaBeanTester.test(Poster.class);
    }
	
	/**
	 * Teste la méthode public String toString().
	 */
	@Test
	public void testToString() {
		final Poster poster = new Poster();
		poster.setIdPoster(LONG_TEST);
		poster.setIdSujet(LONG_TEST);
		poster.setDatePoster(STRING_TEST);
		poster.setChemin(STRING_TEST);
		poster.setValide(STRING_TEST);
		poster.setIdEquipe(STRING_TEST);

		final String resultatAttendu = "Poster [idPoster=" + LONG_TEST + ", chemin=" + STRING_TEST + ", datePoster="
				+ STRING_TEST + ", idSujet=" + LONG_TEST + ", valide=" + STRING_TEST + ", idEquipe=" + STRING_TEST
				+ "]";
		final String resultatTrouve = poster.toString();

		assertEquals("Mauvais toString()", resultatAttendu, resultatTrouve);
	}
	
}