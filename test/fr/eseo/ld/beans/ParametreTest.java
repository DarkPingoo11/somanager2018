package fr.eseo.ld.beans;

import com.mysql.fabric.xmlrpc.base.Param;
import org.junit.Test;

import java.lang.reflect.Field;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class ParametreTest {
    private static final String ATT_NOM_PARAMETRE = "nomParametre";
    private static final String ATT_NOM_VALEUR = "valeur";
    private static final String STRING_TEST = "test";

    /**
     * Teste le constructeur par défaut de la classe.
     */
    @Test
    public void testConstructeurParDefaut() {
        final Parametre parametre = new Parametre();
        assertNull("L'attribut n'est pas nul", parametre.getNomParametre());
    }

    /**
     * Teste la méthode public String getNomParametre().
     *
     * @throws Exception
     */
    @Test
    public void testGetNomParametre() throws Exception {
        final Parametre parametre = Parametre.class.getDeclaredConstructor().newInstance();

        final Field nomParametre = parametre.getClass().getDeclaredField(ATT_NOM_PARAMETRE);
        nomParametre.setAccessible(true);

        final String resultatAttendu = STRING_TEST;
        nomParametre.set(parametre, resultatAttendu);
        final String resultatTrouve = parametre.getNomParametre();

        assertEquals("Mauvais nom de parametre", resultatAttendu, resultatTrouve);
    }


    /**
     * Teste la méthode public void setNomParametre(String idJurySoutenance).
     *
     * @throws Exception
     */
    @Test
    public void testSetNomParametre() throws Exception {
        final Parametre parametre = Parametre.class.getDeclaredConstructor().newInstance();

        final Field nomParametre = parametre.getClass().getDeclaredField(ATT_NOM_PARAMETRE);
        nomParametre.setAccessible(true);

        final String resultatAttendu = STRING_TEST;
        parametre.setNomParametre(resultatAttendu);
        final String resultatTrouve = (String) nomParametre.get(parametre);

        assertEquals("Mauvais idJurySoutenance", resultatAttendu, resultatTrouve);
    }

    /**
     * Teste la méthode public String getValeur().
     *
     * @throws Exception
     */
    @Test
    public void testGetValeur() throws Exception {
        final Parametre parametre = Parametre.class.getDeclaredConstructor().newInstance();

        final Field nomParametre = parametre.getClass().getDeclaredField(ATT_NOM_VALEUR);
        nomParametre.setAccessible(true);

        final String resultatAttendu = STRING_TEST;
        nomParametre.set(parametre, resultatAttendu);
        final String resultatTrouve = parametre.getValeur();

        assertEquals("Mauvais nom de parametre", resultatAttendu, resultatTrouve);
    }


    /**
     * Teste la méthode public void setNomParametre(String idJurySoutenance).
     *
     * @throws Exception
     */
    @Test
    public void testSetValeur() throws Exception {
        final Parametre parametre = Parametre.class.getDeclaredConstructor().newInstance();

        final Field nomParametre = parametre.getClass().getDeclaredField(ATT_NOM_VALEUR);
        nomParametre.setAccessible(true);

        final String resultatAttendu = STRING_TEST;
        parametre.setValeur(resultatAttendu);
        final String resultatTrouve = (String) nomParametre.get(parametre);

        assertEquals("Mauvais idJurySoutenance", resultatAttendu, resultatTrouve);
    }

    /**
     * Teste la méthode public String toString().
     */
    @Test
    public void testToString() {
        final Parametre parametre = new Parametre();
        parametre.setNomParametre(STRING_TEST);
        parametre.setValeur(STRING_TEST);

        final String resultatAttendu = "Parametre{nomParametre='" + STRING_TEST + "', valeur='" + STRING_TEST
                + "'}";
        final String resultatTrouve = parametre.toString();

        assertEquals("Mauvais toString()", resultatAttendu, resultatTrouve);
    }
}
