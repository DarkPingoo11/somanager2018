package fr.eseo.ld.beans;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.lang.reflect.Field;

import org.junit.Test;

/**
 * Classe de tests unitaires JUnit 4 de la classe JurySoutenance.
 *
 * <p>Utilisation de la réflexivité.</p>
 *
 * @version 1.0
 * @author Maxime LENORMAND
 *
 * @see fr.eseo.ld.beans.JurySoutenance
 * @see org.junit
 */
public class JurySoutenanceTest {
	
	private static final String STRING_TEST = "test";
	private static final Long LONG_TEST = 1L;
	
	private static final String ATT_ID_JURY_SOUTENANCE = "idJurySoutenance";
	private static final String ATT_ID_PROF_1 = "idProf1";
	private static final String ATT_ID_PROF_2 = "idProf2";

	/**
	 * Teste le constructeur par défaut de la classe.
	 */
	@Test
	public void testConstructeurParDefaut() {
		final JurySoutenance jurySoutenance = new JurySoutenance();

		assertNull("L'attribut n'est pas nul", jurySoutenance.getIdJurySoutenance());
	}
	
	/**
	 * Teste la méthode public String getIdJurySoutenance().
	 * 
	 * @throws Exception
	 */
	@Test
	public void testGetIdJurySoutenance() throws Exception {
		final JurySoutenance jurySoutenance = JurySoutenance.class.getDeclaredConstructor().newInstance();

		final Field idJurySoutenance = jurySoutenance.getClass().getDeclaredField(ATT_ID_JURY_SOUTENANCE);
		idJurySoutenance.setAccessible(true);

		final String resultatAttendu = STRING_TEST;
		idJurySoutenance.set(jurySoutenance, resultatAttendu);
		final String resultatTrouve = jurySoutenance.getIdJurySoutenance();

		assertEquals("Mauvais idJurySoutenance", resultatAttendu, resultatTrouve);
	}
	
	/**
	 * Teste la méthode public void setIdJurySoutenance(String idJurySoutenance).
	 * 
	 * @throws Exception
	 */
	@Test
	public void testSetIdJurySoutenance() throws Exception {
		final JurySoutenance jurySoutenance = JurySoutenance.class.getDeclaredConstructor().newInstance();

		final Field idJurySoutenance = jurySoutenance.getClass().getDeclaredField(ATT_ID_JURY_SOUTENANCE);
		idJurySoutenance.setAccessible(true);

		final String resultatAttendu = STRING_TEST;
		jurySoutenance.setIdJurySoutenance(resultatAttendu);
		final String resultatTrouve = (String) idJurySoutenance.get(jurySoutenance);

		assertEquals("Mauvais idJurySoutenance", resultatAttendu, resultatTrouve);
	}

	/**
	 * Teste la méthode public Long getIdProf1().
	 * 
	 * @throws Exception
	 */
	@Test
	public void testGetIdProf1() throws Exception {
		final JurySoutenance jurySoutenance = JurySoutenance.class.getDeclaredConstructor().newInstance();

		final Field idProf1 = jurySoutenance.getClass().getDeclaredField(ATT_ID_PROF_1);
		idProf1.setAccessible(true);

		final Long resultatAttendu = LONG_TEST;
		idProf1.set(jurySoutenance, resultatAttendu);
		final Long resultatTrouve = jurySoutenance.getIdProf1();

		assertEquals("Mauvais idProf1", resultatAttendu, resultatTrouve);
	}

	/**
	 * Teste la méthode public void setIdProf1(Long idProf1).
	 * 
	 * @throws Exception
	 */
	@Test
	public void testSetIdProf1() throws Exception {
		final JurySoutenance jurySoutenance = JurySoutenance.class.getDeclaredConstructor().newInstance();

		final Field idProf1 = jurySoutenance.getClass().getDeclaredField(ATT_ID_PROF_1);
		idProf1.setAccessible(true);

		final Long resultatAttendu = LONG_TEST;
		jurySoutenance.setIdProf1(resultatAttendu);
		final Long resultatTrouve = (Long) idProf1.get(jurySoutenance);

		assertEquals("Mauvais idProf1", resultatAttendu, resultatTrouve);
	}

	/**
	 * Teste la méthode public Long getIdProf2().
	 * 
	 * @throws Exception
	 */
	@Test
	public void testGetIdProf2() throws Exception {
		final JurySoutenance jurySoutenance = JurySoutenance.class.getDeclaredConstructor().newInstance();

		final Field idProf2 = jurySoutenance.getClass().getDeclaredField(ATT_ID_PROF_2);
		idProf2.setAccessible(true);

		final Long resultatAttendu = LONG_TEST;
		idProf2.set(jurySoutenance, resultatAttendu);
		final Long resultatTrouve = jurySoutenance.getIdProf2();

		assertEquals("Mauvais idProf2", resultatAttendu, resultatTrouve);
	}

	/**
	 * Teste la méthode public void setIdProf2(Long idProf2).
	 * 
	 * @throws Exception
	 */
	@Test
	public void testSetIdProf2() throws Exception {
		final JurySoutenance jurySoutenance = JurySoutenance.class.getDeclaredConstructor().newInstance();

		final Field idProf2 = jurySoutenance.getClass().getDeclaredField(ATT_ID_PROF_2);
		idProf2.setAccessible(true);

		final Long resultatAttendu = LONG_TEST;
		jurySoutenance.setIdProf2(resultatAttendu);
		final Long resultatTrouve = (Long) idProf2.get(jurySoutenance);

		assertEquals("Mauvais idProf2", resultatAttendu, resultatTrouve);
	}

	/**
	 * Teste la méthode public String toString().
	 */
	@Test
	public void testToString() {
		final JurySoutenance jurySoutenance = new JurySoutenance();
		jurySoutenance.setIdJurySoutenance(STRING_TEST);
		jurySoutenance.setIdProf1(LONG_TEST);
		jurySoutenance.setIdProf2(LONG_TEST);

		final String resultatAttendu = "JurySoutenance [idJurySoutenance=" + STRING_TEST + ", idProf1=" + LONG_TEST
				+ ", idProf2=" + LONG_TEST + "]";
		final String resultatTrouve = jurySoutenance.toString();

		assertEquals("Mauvais toString()", resultatAttendu, resultatTrouve);
	}

}