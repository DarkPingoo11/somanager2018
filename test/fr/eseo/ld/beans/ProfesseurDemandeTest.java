package fr.eseo.ld.beans;

import net.codebox.javabeantester.JavaBeanTester;
import org.junit.Test;

import java.beans.IntrospectionException;

/**
 * Classe de test de ProfesseurDemandeDAO
 *
 * @author Tristan LE GACQUE
 * @see ProfesseurDemande
 **/
public class ProfesseurDemandeTest {

    @Test
    public void testBean() throws IntrospectionException {
        JavaBeanTester.test(ProfesseurDemande.class);
    }
}