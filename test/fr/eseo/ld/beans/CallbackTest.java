package fr.eseo.ld.beans;

import net.codebox.javabeantester.JavaBeanTester;
import org.junit.Test;

import java.beans.IntrospectionException;

import static org.junit.Assert.assertEquals;

public class CallbackTest {

    @Test
    public void testBean() throws IntrospectionException {
        JavaBeanTester.test(Callback.class);
    }

    @Test
    public void testJson() {
        Callback c = new Callback();
        String type = CallbackType.SUCCESS.getValue();
        String message = "Message de test";

        c.setType(type);
        c.setMessage(message);

        String jsonStr = "{\"type\":\"success\",\"message\":\"Message de test\"}";

        assertEquals("Les objets ne sont pas identiques", jsonStr, c.getJsonObject().toString());
    }

    @Test
    public void testConstructeur1() {
        String type = CallbackType.ERROR.getValue();
        String message = "Message de test";
        Callback c = new Callback(message);

        assertEquals("Le message n'est pas identique", message, c.getMessage());
        assertEquals("Le type par defaut n'est pas ERROR", type, c.getType());
    }

    @Test
    public void testConstructeurFull() {
        String type = CallbackType.SUCCESS.getValue();
        String message = "Message de test";
        Callback c = new Callback(type, message);

        assertEquals("Le message n'est pas identique", message, c.getMessage());
        assertEquals("Le type par defaut n'est pas SUCCESS", type, c.getType());
    }

    @Test
    public void testConstructeurFull2() {
        CallbackType ct = CallbackType.SUCCESS;
        String message = "Message de test";
        Callback c = new Callback(ct, message);

        assertEquals("Le message n'est pas identique", message, c.getMessage());
        assertEquals("Le type par defaut n'est pas SUCCESS", ct.getValue(), c.getType());
    }
}