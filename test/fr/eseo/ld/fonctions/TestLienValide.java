package fr.eseo.ld.fonctions;


import fr.eseo.ld.servlets.DepotGoogle;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class TestLienValide {
    @Parameters(name = "{index}: lien({0})={1}")
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] {
                { "https://google.fr/dsq/", true },
                { "https://google.fr/", true },
                { "https://docs.google.com/forms/d/e/1FAIpQLSc_lScSvp68sjbIY9aL30pFay9Y0D_awKYub6niJBWV5kzVCQ/viewform?usp=sf_link", true },
                { "http://google.fr/", true },
                { "google.fr/dqs/", false },
                { "http://google/dqs/", false },
        });
    }

    private String lien;
    private boolean valide;

    public TestLienValide(String lien, boolean valide) {
        this.lien = lien;
        this.valide = valide;
    }

    @Test
    public void testLienValide() {
        assertEquals(this.valide, DepotGoogle.isLienValide(this.lien));
    }
}
