package fr.eseo.ld.fonctions;

import fr.eseo.ld.filters.*;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

/**
 * Suite de tests unitaires JUnit 4 pour le package fr.eseo.ld.fonctions
 * 
 * @author Tristan LE GACQUE
 * 
 * @see org.junit
 */
@RunWith(Suite.class)
@SuiteClasses({
	TestLienValide.class
})
public class FonctionsTestSuite {
	/* Classe vide */
}