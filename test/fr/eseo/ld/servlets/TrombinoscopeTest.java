package fr.eseo.ld.servlets;

import fr.eseo.ld.beans.Utilisateur;
import fr.eseo.ld.dao.*;
import org.easymock.*;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.expectLastCall;

/**
 * Classe de tests unitaires JUnit 4 de la classe Trombinoscope.
 *
 * <p>Utilisation de mocks (construits avec EasyMock).</p>
 *
 * @version 1.0
 * @author Damien Maymard
 *
 * @see fr.eseo.ld.servlets.Trombinoscope
 * @see org.junit
 * @see org.easymock
 */
@RunWith(EasyMockRunner.class)
public class TrombinoscopeTest extends EasyMockSupport {
	
	private static final String CONF_DAO_FACTORY = "daoFactory";
	private static final String ATT_SESSION_USER = "utilisateur";
	private static final Long ID_UTILISATEUR = 10L;
	private static final Long ID_ETUDIANT_LD1 = 21L;
	private static final Long ID_ETUDIANT_SE1 = 22L;
	private static final String VUE_FORM = "/WEB-INF/formulaires/trombinoscope.jsp";

	private static OptionESEODAO optionESEODAO;
	private static EquipeDAO equipeDAO;
	private static PglEquipeDAO pglEquipeDAO;
	private static AnneeScolaireDAO anneeScolaireDAO;
	private static UtilisateurDAO utilisateurDAO;

	@Mock
	private static UtilisateurDAO utilisateurDAOMock;

	@TestSubject
	private Trombinoscope trombinoscope = new Trombinoscope();
	
	@Mock
	private ServletConfig servletConfigMock;
	
	@Mock
	private ServletContext servletContextMock;
	
	@Mock(type = MockType.NICE)
	private HttpServletRequest httpServletRequestMock;
	
	@Mock
	private HttpServletResponse httpservletResponseMock;
	
	@Mock
	private HttpSession httpSessionMock;
	
	@Mock
	private RequestDispatcher requestDispatcherMock;
	
	@Mock
	private DAOFactory daoFactoryMock;
	
	@Mock
	private Collection<Part> parts;

	/**
	 * Récupère des instances de DAO.
	 * 
	 * @throws Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		DAOFactory daoFactory = DAOFactory.getInstance();
		utilisateurDAO = daoFactory.getUtilisateurDao();
		optionESEODAO = daoFactory.getOptionESEODAO();
		equipeDAO = daoFactory.getEquipeDAO();
		pglEquipeDAO = daoFactory.getPglEquipeDAO();
		anneeScolaireDAO = daoFactory.getAnneeScolaireDAO();
	}

	/**
	 * Teste la méthode protected void doGet(HttpServletRequest request, HttpServletResponse response).
	 * 
	 * @throws ServletException
	 * @throws IOException
	 */
	@Test
	public void testDoGetEmpty() throws ServletException, IOException {
		
		/* Récupération d'instances de DAO */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
		expect(this.daoFactoryMock.getUtilisateurDao()).andReturn(utilisateurDAO);
		expect(this.daoFactoryMock.getOptionESEODAO()).andReturn(optionESEODAO);
		expect(this.daoFactoryMock.getEquipeDAO()).andReturn(equipeDAO);
		expect(this.daoFactoryMock.getPglEquipeDAO()).andReturn(pglEquipeDAO);
		expect(this.daoFactoryMock.getAnneeScolaireDAO()).andReturn(anneeScolaireDAO);
		
		/* Récupération de l'Utilisateur en session */
		expect(this.httpServletRequestMock.getSession()).andReturn(this.httpSessionMock);
		final Utilisateur utilisateur = new Utilisateur();
		final Long idUtilisateur = ID_UTILISATEUR;
		utilisateur.setIdUtilisateur(idUtilisateur);
		expect(this.httpSessionMock.getAttribute(ATT_SESSION_USER)).andReturn(utilisateur);

		/* Affichage du formulaire */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getRequestDispatcher(VUE_FORM)).andReturn(this.requestDispatcherMock);
		this.requestDispatcherMock.forward(this.httpServletRequestMock, this.httpservletResponseMock);
		expectLastCall();
		this.replayAll();

		/* Cycle de vie d'une servlet */
		this.trombinoscope.init(this.servletConfigMock);
		this.trombinoscope.doGet(this.httpServletRequestMock, this.httpservletResponseMock);
		this.trombinoscope.destroy();
		this.verifyAll();
	}
	
	@Test
	public void testDoGetFilled() throws ServletException, IOException {
		
		/* Récupération d'instances de DAO */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
		expect(this.daoFactoryMock.getUtilisateurDao()).andReturn(utilisateurDAO);
		expect(this.daoFactoryMock.getOptionESEODAO()).andReturn(optionESEODAO);
		expect(this.daoFactoryMock.getEquipeDAO()).andReturn(equipeDAO);
		expect(this.daoFactoryMock.getPglEquipeDAO()).andReturn(pglEquipeDAO);
		expect(this.daoFactoryMock.getAnneeScolaireDAO()).andReturn(anneeScolaireDAO);
		
		/* Récupération de l'Utilisateur en session */
		expect(this.httpServletRequestMock.getSession()).andReturn(this.httpSessionMock);
		final Utilisateur utilisateur = new Utilisateur();
		final Long idUtilisateur = ID_UTILISATEUR;
		utilisateur.setIdUtilisateur(idUtilisateur);
		expect(this.httpSessionMock.getAttribute(ATT_SESSION_USER)).andReturn(utilisateur);
		
		/* Récupération de la liste des étudiants */
		final Utilisateur etudiantLD1 = new Utilisateur();
		final Utilisateur etudiantSE1 = new Utilisateur();
		final Long idEtudiantLD1 = ID_ETUDIANT_LD1;
		final Long idEtudiantSE1 = ID_ETUDIANT_SE1;
		etudiantLD1.setIdUtilisateur(idEtudiantLD1);
		etudiantSE1.setIdUtilisateur(idEtudiantSE1);
		final List<Utilisateur> listeEtudiants = new ArrayList<>();
		listeEtudiants.add(etudiantLD1);
		listeEtudiants.add(etudiantSE1);
		expect(trombinoscope.getUtilisateurDAO().listerEtudiants()).andReturn(listeEtudiants);

		/* Affichage du formulaire */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getRequestDispatcher(VUE_FORM)).andReturn(this.requestDispatcherMock);
		this.requestDispatcherMock.forward(this.httpServletRequestMock, this.httpservletResponseMock);
		expectLastCall();
		this.replayAll();

		/* Cycle de vie d'une servlet */
		this.trombinoscope.init(this.servletConfigMock);
		this.trombinoscope.doGet(this.httpServletRequestMock, this.httpservletResponseMock);
		this.trombinoscope.destroy();
	}
	
	//@Test
	public void testDoPost() throws ServletException, IOException {
		
		/* Récupération d'instances de DAO */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
		expect(this.daoFactoryMock.getUtilisateurDao()).andReturn(utilisateurDAO);
		expect(this.daoFactoryMock.getOptionESEODAO()).andReturn(optionESEODAO);
		expect(this.daoFactoryMock.getEquipeDAO()).andReturn(equipeDAO);
		expect(this.daoFactoryMock.getAnneeScolaireDAO()).andReturn(anneeScolaireDAO);

		/* Récupération des données saisies */
		expect(this.httpServletRequestMock.getParts()).andReturn(parts);
		//expect(this.parts.getHeader( "content-disposition" )).andReturn("filename=\"test.pdf\"");

		//InputStream stream = new FileInputStream("/home/etudiant/Posters/2017/test.pdf");
		//expect(this.parts.getInputStream()).andReturn(stream);
		
		/* Affichage du formulaire */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getRequestDispatcher(VUE_FORM)).andReturn(this.requestDispatcherMock);
		this.requestDispatcherMock.forward(this.httpServletRequestMock, this.httpservletResponseMock);
		//expectLastCall();
		//this.replayAll();
		
		/* Cycle de vie d'une servlet */
		this.trombinoscope.doPost(this.httpServletRequestMock, this.httpservletResponseMock);
		this.trombinoscope.destroy();
	}

}