package fr.eseo.ld.servlets;

import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.expectLastCall;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.easymock.EasyMockRunner;
import org.easymock.EasyMockSupport;
import org.easymock.Mock;
import org.easymock.MockType;
import org.easymock.TestSubject;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import fr.eseo.ld.beans.AnneeScolaire;
import fr.eseo.ld.beans.Equipe;
import fr.eseo.ld.beans.EtatSujet;
import fr.eseo.ld.beans.Etudiant;
import fr.eseo.ld.beans.EtudiantEquipe;
import fr.eseo.ld.beans.OptionESEO;
import fr.eseo.ld.beans.Poster;
import fr.eseo.ld.beans.Professeur;
import fr.eseo.ld.beans.Role;
import fr.eseo.ld.beans.RoleUtilisateur;
import fr.eseo.ld.beans.Sujet;
import fr.eseo.ld.beans.Utilisateur;
import fr.eseo.ld.dao.AnneeScolaireDAO;
import fr.eseo.ld.dao.DAOFactory;
import fr.eseo.ld.dao.EquipeDAO;
import fr.eseo.ld.dao.EtudiantDAO;
import fr.eseo.ld.dao.NotificationDAO;
import fr.eseo.ld.dao.OptionESEODAO;
import fr.eseo.ld.dao.PosterDAO;
import fr.eseo.ld.dao.ProfesseurDAO;
import fr.eseo.ld.dao.RoleDAO;
import fr.eseo.ld.dao.SujetDAO;
import fr.eseo.ld.dao.UtilisateurDAO;

/**
 * Classe de tests unitaires JUnit 4 de la classe TelechargerPoster.
 *
 * <p>Utilisation de mocks (construits avec EasyMock).</p>
 *
 * @version 1.0
 * @author Maxime LENORMAND
 *
 * @see fr.eseo.ld.servlets.TelechargerPoster
 * @see org.junit
 * @see org.easymock
 */
@RunWith(EasyMockRunner.class)
public class TelechargerPosterTest extends EasyMockSupport {
	
	private static final String CONF_DAO_FACTORY = "daoFactory";
	
	private static final Long LONG_TEST = 47L;
	
	private static String cheminTest;
	private static final String NOM_POSTER_TEST = "47_test.pdf";
	private static final String CHEMIN_POSTER = "/home/etudiant/Posters/2018/";
	
	private static final String CHAMP_ID_SUJET = "idSujet";
	
	public static final int TAILLE_TAMPON = 10240;
		
	private static PosterDAO posterDAO;
	private static NotificationDAO notificationDAO;
	private static EquipeDAO equipeDAO;
	private static Utilisateur utilisateurTest;
	private static Etudiant etudiantTest;
	private static Sujet sujetTest;
	private static Equipe equipeTest;
	private static UtilisateurDAO utilisateurDAO;
	private static EtudiantDAO etudiantDAO;
	private static SujetDAO sujetDAO;
	private static EtudiantEquipe etudiantEquipeTest ;
	private static OptionESEODAO optionESEODAO;
	private static OptionESEO optionESEOTest;
	private static AnneeScolaireDAO anneeScolaireDAO;
	private static AnneeScolaire anneeTest;
	private static Poster posterTest;
	private static RoleDAO roleDAO;
	private static Role roleTest;
	private static RoleUtilisateur roleUtilisateurTest;
	private static ProfesseurDAO professeurDAO;
	private static Professeur professeurTest;

	private static Utilisateur utilisateurTest2;
	
	@TestSubject
	private TelechargerPoster telechargerPoster = new TelechargerPoster();
	
	@Mock
	private ServletConfig servletConfigMock;
	
	@Mock
	private ServletContext servletContextMock;
	
	@Mock
	private HttpServletRequest httpServletRequestMock;
	
	@Mock
	private HttpServletResponse httpservletResponseMock;
	
	@Mock(type = MockType.NICE)
	private PrintWriter outMock;
	
	@Mock
	private DAOFactory daoFactoryMock;
	
	/**
	 * Récupère une instance de DAO Poster et Insère un poster test.
	 * 
	 * @throws Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		final DAOFactory daoFactory = DAOFactory.getInstance();
		notificationDAO = daoFactory.getNotificationDao();
		posterDAO = daoFactory.getPosterDao();
		equipeDAO = daoFactory.getEquipeDAO();
		utilisateurDAO = daoFactory.getUtilisateurDao();
		etudiantDAO = daoFactory.getEtudiantDao();
		sujetDAO = daoFactory.getSujetDao();
		optionESEODAO = daoFactory.getOptionESEODAO();
		anneeScolaireDAO = daoFactory.getAnneeScolaireDAO();
		roleDAO = daoFactory.getRoleDAO();
		professeurDAO = daoFactory.getProfesseurDAO();
		
		/*Création des données de test*/
		
		/* Insertion d'un utilisateur test dans la BDD */
        Utilisateur utilisateur = new Utilisateur();
        utilisateur.setIdentifiant("test");
        utilisateur.setNom("test");
        utilisateur.setPrenom("test");
        utilisateur.setValide("oui");
        utilisateurDAO.creer(utilisateur);
        utilisateurTest = utilisateurDAO.trouver(utilisateur).get(0);
        
        Utilisateur utilisateur2 = new Utilisateur();
        utilisateur2.setIdentifiant("test2");
        utilisateur2.setNom("test2");
        utilisateur2.setPrenom("test2");
        utilisateur2.setValide("oui");
        utilisateurDAO.creer(utilisateur2);
        utilisateurTest2 = utilisateurDAO.trouver(utilisateur2).get(0);
        
        Etudiant etudiant = new Etudiant();
        etudiant.setIdEtudiant(utilisateurTest2.getIdUtilisateur());
        etudiant.setAnnee(2018);
        etudiant.setContratPro("non");
        etudiantDAO.creer(etudiant);
        etudiantTest = etudiantDAO.trouver(etudiant).get(0);
        
		Sujet sujet = new Sujet();
		sujet.setTitre("sujetTest");
		sujet.setDescription("description");
		sujet.setNbrMinEleves(1);
		sujet.setNbrMaxEleves(4);
		sujet.setEtat(EtatSujet.ATTRIBUE);
		sujetDAO.creer(sujet);
		sujetTest = sujetDAO.trouver(sujet).get(0);
		
		OptionESEO option = new OptionESEO();
		option.setIdOption(1L);
		option.setNomOption("LD");
		optionESEODAO.creer(option);
		optionESEOTest = optionESEODAO.trouver(option).get(0);
		
		AnneeScolaire annee = new AnneeScolaire();
		annee.setAnneeDebut(2018);
		annee.setAnneeFin(2019);
		annee.setIdOption(1L);
		anneeScolaireDAO.creer(annee);
		anneeTest = anneeScolaireDAO.trouver(annee).get(0);
		
		Professeur professeur = new Professeur();
        professeur.setIdProfesseur(utilisateurTest.getIdUtilisateur());;
        professeurDAO.creer(professeur);
        professeurTest = professeurDAO.trouver(professeur).get(0);
        
        Role role = new Role();
        role.setIdRole(5L);
        role.setNomRole("profResponsable");
        role.setType("applicatif");
        roleDAO.creerRole(role);
        roleTest = roleDAO.trouver(role).get(0);
        
        utilisateurDAO.associerRoleOption(utilisateurTest, roleTest, optionESEOTest);
		
		Equipe equipe = new Equipe();
		equipe.setAnnee(2018);
		equipe.setTaille(1);
		equipe.setIdSujet(sujetTest.getIdSujet());
		equipe.setValide("oui");
		equipeDAO.creer(equipe);
		equipeTest = equipeDAO.trouver(equipe).get(0);
		
		EtudiantEquipe etudiantEquipe = new EtudiantEquipe();
		etudiantEquipe.setIdEquipe(equipeTest.getIdEquipe());
		etudiantEquipe.setIdEtudiant(etudiantTest.getIdEtudiant());
		equipeDAO.attribuerEtudiantEquipe(etudiantEquipe);
		etudiantEquipeTest = equipeDAO.trouverEtudiantEquipe(etudiantEquipe).get(0);
		
		String cheminPoster = CHEMIN_POSTER + sujetTest.getIdSujet() + "_test.pdf";
		cheminTest = cheminPoster;
		Poster poster = new Poster();
		poster.setChemin(cheminPoster);
		poster.setIdEquipe(equipeTest.getIdEquipe());
		poster.setIdSujet(sujetTest.getIdSujet());
		poster.setValide("oui");
		posterDAO.creer(poster);
		posterTest = posterDAO.trouver(poster).get(0);
	}
	
	/**
     * Supprime les données de test dans la BDD.
     *
     * @throws Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
    	posterDAO.supprimer(posterTest);
    	equipeDAO.supprimerEtudiantEquipe(etudiantEquipeTest);
    	utilisateurDAO.supprimerRoleOption(utilisateurTest, roleTest, optionESEOTest);
    	roleDAO.supprimer(roleTest);
    	anneeScolaireDAO.supprimer(anneeTest);
    	optionESEODAO.supprimer(optionESEOTest);
    	sujetDAO.supprimer(sujetTest);
    	professeurDAO.supprimer(professeurTest);
        utilisateurDAO.supprimer(utilisateurTest);
        etudiantDAO.supprimer(etudiantTest);
        utilisateurDAO.supprimer(utilisateurTest2);
    }
	
	/**
	 * Teste la méthode protected void doPost(HttpServletRequest request, HttpServletResponse response).
	 * 
	 * <p>Cas de l'action "afficher".</p>
	 * 
	 * @throws ServletException
	 * @throws IOException
	 */
	@Test
	public void testDoPostAfficher() throws ServletException, IOException {
		/* Récupération d'une instance de DAO Poster */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
		expect(this.daoFactoryMock.getPosterDao()).andReturn(posterDAO);
		
		/* Récupération de l'ID du sujet concerné par le poster */
		expect(this.httpServletRequestMock.getParameter(CHAMP_ID_SUJET)).andReturn(sujetTest.getIdSujet().toString());
		
		/* Récupération des données saisies */
		expect(this.httpServletRequestMock.getParameter("action")).andReturn("afficher");
		
		/* Formatage de la response */
		this.httpservletResponseMock.setContentType("text/html");
		expectLastCall();
		expect(this.httpservletResponseMock.getWriter()).andReturn(this.outMock);
		this.httpservletResponseMock.setContentType("APPLICATION/PDF");
		expectLastCall();
		this.httpservletResponseMock.setHeader("Content-Disposition", "inline; filename=\"" + cheminTest + "\"");
		expectLastCall();
		
		this.replayAll();
		
		/* Cycle de vie d'une servlet */
		this.telechargerPoster.init(this.servletConfigMock);
		this.telechargerPoster.doPost(this.httpServletRequestMock, this.httpservletResponseMock);
		this.telechargerPoster.destroy();
		
		this.verifyAll();
	}
	
	/**
	 * Teste la méthode protected void doPost(HttpServletRequest request, HttpServletResponse response).
	 * 
	 * <p>Cas d'une autre action.</p>
	 * 
	 * @throws ServletException
	 * @throws IOException
	 */
	@Test
	public void testDoPostAutre() throws ServletException, IOException {
		/* Récupération d'une instance de DAO Poster */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
		expect(this.daoFactoryMock.getPosterDao()).andReturn(posterDAO);
		
		/* Récupération de l'ID du sujet concerné par le poster */
		expect(this.httpServletRequestMock.getParameter(CHAMP_ID_SUJET)).andReturn(sujetTest.getIdSujet().toString());
		
		/* Récupération des données saisies */
		expect(this.httpServletRequestMock.getParameter("action")).andReturn("autre");
		
		/* Formatage de la response */
		this.httpservletResponseMock.setContentType("text/html");
		expectLastCall();
		expect(this.httpservletResponseMock.getWriter()).andReturn(this.outMock);
		this.httpservletResponseMock.setContentType("APPLICATION/octet_stream");
		expectLastCall();
		this.httpservletResponseMock.setHeader("Content-Disposition", "inline; filename=\"" + sujetTest.getIdSujet() + "_test.pdf" + "\"");
		expectLastCall();
		
		this.replayAll();
		
		/* Cycle de vie d'une servlet */
		this.telechargerPoster.init(this.servletConfigMock);
		this.telechargerPoster.doPost(this.httpServletRequestMock, this.httpservletResponseMock);
		this.telechargerPoster.destroy();
		
		this.verifyAll();
	}
	
}