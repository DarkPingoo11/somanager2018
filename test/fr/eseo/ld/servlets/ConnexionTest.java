package fr.eseo.ld.servlets;

import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.expectLastCall;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.easymock.EasyMockRunner;
import org.easymock.EasyMockSupport;
import org.easymock.Mock;
import org.easymock.MockType;
import org.easymock.TestSubject;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mindrot.jbcrypt.BCrypt;

import fr.eseo.ld.beans.AnneeScolaire;
import fr.eseo.ld.beans.Equipe;
import fr.eseo.ld.beans.EtatSujet;
import fr.eseo.ld.beans.Etudiant;
import fr.eseo.ld.beans.EtudiantEquipe;
import fr.eseo.ld.beans.OptionESEO;
import fr.eseo.ld.beans.Sujet;
import fr.eseo.ld.beans.Utilisateur;
import fr.eseo.ld.dao.AnneeScolaireDAO;
import fr.eseo.ld.dao.DAOFactory;
import fr.eseo.ld.dao.EquipeDAO;
import fr.eseo.ld.dao.EtudiantDAO;
import fr.eseo.ld.dao.NotificationDAO;
import fr.eseo.ld.dao.OptionESEODAO;
import fr.eseo.ld.dao.SujetDAO;
import fr.eseo.ld.dao.UtilisateurDAO;

/**
 * Classe de tests unitaires JUnit 4 de la classe Connexion.
 *
 * <p>Utilisation de mocks (construits avec EasyMock).</p>
 *
 * @version 1.0
 * @author Renaud DESCOURS et Maxime LENORMAND
 *
 * @see fr.eseo.ld.servlets.Connexion
 * @see org.junit
 * @see org.easymock
 * 
 * BCrypt : Copyright (c) 2002 Johnny Shelley All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the author nor any contributors may be used to endorse
 * or promote products derived from this software without specific prior written
 * permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
@RunWith(EasyMockRunner.class)
public class ConnexionTest extends EasyMockSupport {

	private static final String CONF_DAO_FACTORY = "daoFactory";
	
	private static final String ID_CONNEXION_BDD_SUCCES = "Etu6";
	private static final String ID_CONNEXION_BDD_EQUIPE = "etudiant";
	private static final String ID_CONNEXION_AD_SUCCES = "menardth";
	private static final String ID_CONNEXION_ECHEC = "IdNonExistant";
	private static final String MDP_CONNEXION_BDD_SUCCES = "network1";
	private static final String MDP_CONNEXION_BDD_EQUIPE2 = "network1";
	private static final String MDP_CONNEXION_AD_SUCCES = "network";
	private static final String MDP_CONNEXION_ECHEC = "mauvaismdp";
	private static final String TEST = "test";
	private static final Long ID_ETUDIANT = 23L;
	private static final Long ID_ETUDIANT_EQUIPE = 6L;
	
	private static final String ATT_SESSION_USER = "utilisateur";
	private static final String ATT_RESULTAT = "resultat";
	private static final String ATT_ERREURS = "erreurs";
	
	private static final String CHAMP_IDENTIFIANT = "identifiant";
	private static final String CHAMP_MOT_DE_PASSE = "motDePasse";
	
	private static final String CONTEXT_PATH = "SoManager/Connexion";
	
	private static final String VUE_INDEX = "/Dashboard";
	private static final String VUE_FORM = "/WEB-INF/formulaires/connexion.jsp";

	private static final String CHAMP_ETAT_DEPOSE = "etatDepose";

	private static final String UTILISATEUR_NON_VALIDE = "user1";
	
	private static UtilisateurDAO utilisateurDAO;
	private static EquipeDAO equipeDAO;
	private static NotificationDAO notificationDAO;
	private static EtudiantDAO etudiantDAO;
	private static SujetDAO sujetDAO;
	
	private static OptionESEODAO optionESEODAO;
	private static OptionESEO optionESEOTest;
	private static AnneeScolaireDAO anneeScolaireDAO;
	private static AnneeScolaire anneeTest;
	
	private static Etudiant etudiantTest;
	private static Sujet sujetTest;
	private static Equipe equipeTest;
	private static EtudiantEquipe etudiantEquipeTest;

	private static Utilisateur utilisateurTest;

	private static Utilisateur utilisateurTest2;

	private static Utilisateur utilisateurTest3;

	private static Utilisateur utilisateurTest4;
	
	@TestSubject
	private Connexion connexion = new Connexion();
	
	@Mock
	private ServletConfig servletConfigMock;
	
	@Mock
	private ServletContext servletContextMock;
	
	@Mock
	private HttpServletRequest httpServletRequestMock;
	
	@Mock
	private HttpServletResponse httpservletResponseMock;
	
	@Mock(type = MockType.NICE)
	private HttpSession httpSessionMock;
	
	@Mock
	private RequestDispatcher requestDispatcherMock;
	
	@Mock
	private DAOFactory daoFactoryMock;
	
	@Mock
	private Utilisateur utilisateurMock;
	
	
	/**
	 * Récupère des instances de DAO.
	 * 
	 * @throws Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		final DAOFactory daoFactory = DAOFactory.getInstance();
		utilisateurDAO = daoFactory.getUtilisateurDao();
		equipeDAO = daoFactory.getEquipeDAO();
		notificationDAO = daoFactory.getNotificationDao();
		etudiantDAO = daoFactory.getEtudiantDao();
		sujetDAO = daoFactory.getSujetDao();
		anneeScolaireDAO = daoFactory.getAnneeScolaireDAO();
		optionESEODAO = daoFactory.getOptionESEODAO();
		
		/*utilisateur avec mot de passe hashé*/
		Utilisateur utilisateur = new Utilisateur();
        utilisateur.setIdentifiant("test");
        utilisateur.setNom("test");
        utilisateur.setPrenom("test");
        String hash = BCrypt.hashpw("test", BCrypt.gensalt());
		utilisateur.setHash(hash);
        utilisateur.setValide("oui");
        utilisateurDAO.creer(utilisateur);
        utilisateurTest = utilisateurDAO.trouver(utilisateur).get(0);
        
        /* utilisateur etudiant équipe*/
        Utilisateur utilisateur2 = new Utilisateur();
        utilisateur2.setIdentifiant("test2CT");
        utilisateur2.setNom("test2CT");
        utilisateur2.setPrenom("test2CT");
        String hash2 = BCrypt.hashpw("test", BCrypt.gensalt());
		utilisateur2.setHash(hash2);
        utilisateur2.setValide("oui");
        utilisateurDAO.creer(utilisateur2);
        utilisateurTest2 = utilisateurDAO.trouver(utilisateur2).get(0);
        
        Etudiant etudiant = new Etudiant();
        etudiant.setIdEtudiant(utilisateurTest2.getIdUtilisateur());
        etudiant.setAnnee(2018);
        etudiant.setContratPro("non");
        etudiantDAO.creer(etudiant);
        etudiantTest = etudiantDAO.trouver(etudiant).get(0);
        
        Sujet sujet = new Sujet();
		sujet.setTitre("sujetTest");
		sujet.setDescription("description");
		sujet.setNbrMinEleves(1);
		sujet.setNbrMaxEleves(4);
		sujet.setEtat(EtatSujet.ATTRIBUE);
		sujetDAO.creer(sujet);
		sujetTest = sujetDAO.trouver(sujet).get(0);
		
		OptionESEO option = new OptionESEO();
		option.setIdOption(1L);
		option.setNomOption("LD");
		optionESEODAO.creer(option);
		optionESEOTest = optionESEODAO.trouver(option).get(0);
		
		AnneeScolaire annee = new AnneeScolaire();
		annee.setAnneeDebut(2018);
		annee.setAnneeFin(2019);
		annee.setIdOption(1L);
		anneeScolaireDAO.creer(annee);
		anneeTest = anneeScolaireDAO.trouver(annee).get(0);
		
		Equipe equipe = new Equipe();
		equipe.setAnnee(2018);
		equipe.setTaille(1);
		equipe.setIdSujet(sujetTest.getIdSujet());
		equipe.setValide("oui");
		equipeDAO.creer(equipe);
		equipeTest = equipeDAO.trouver(equipe).get(0);
		
		EtudiantEquipe etudiantEquipe = new EtudiantEquipe();
		etudiantEquipe.setIdEquipe(equipeTest.getIdEquipe());
		etudiantEquipe.setIdEtudiant(etudiantTest.getIdEtudiant());
		equipeDAO.attribuerEtudiantEquipe(etudiantEquipe);
		etudiantEquipeTest = equipeDAO.trouverEtudiantEquipe(etudiantEquipe).get(0);
        
        /* utilisateur AD*/
        Utilisateur utilisateur3 = new Utilisateur();
        utilisateur3.setIdentifiant("test3");
        utilisateur3.setNom("test3");
        utilisateur3.setPrenom("test3");
        utilisateur3.setValide("oui");
        utilisateur3.setHash("activeDirectory");
        utilisateurDAO.creer(utilisateur3);
        utilisateurTest3 = utilisateurDAO.trouver(utilisateur3).get(0);
        
        /* utilisateur échec */
        Utilisateur utilisateur4 = new Utilisateur();
        utilisateur4.setIdentifiant("test4");
        utilisateur4.setNom("test4");
        utilisateur4.setPrenom("test4");
        utilisateur4.setValide("non");
        String hash4 = BCrypt.hashpw("test", BCrypt.gensalt());
		utilisateur4.setHash(hash4);
        utilisateurDAO.creer(utilisateur4);
        utilisateurTest4 = utilisateurDAO.trouver(utilisateur4).get(0);
	}
	
	/**
	 * Supprime les données insérées dans la BDD.
	 * 
	 * @throws Exception
	 */
	@AfterClass
	public static void tearDownAfterClass()throws Exception {
    	equipeDAO.supprimerEtudiantEquipe(etudiantEquipeTest);
    	anneeScolaireDAO.supprimer(anneeTest);
    	optionESEODAO.supprimer(optionESEOTest);
    	sujetDAO.supprimer(sujetTest);
        utilisateurDAO.supprimer(utilisateurTest);
        etudiantDAO.supprimer(etudiantTest);
        utilisateurDAO.supprimer(utilisateurTest2);
        utilisateurDAO.supprimer(utilisateurTest3);
        utilisateurDAO.supprimer(utilisateurTest4);
	}
	
	/**
	 * Teste la méthode protected void doGet(HttpServletRequest request, HttpServletResponse response).
	 * 
	 * @throws ServletException
	 * @throws IOException
	 */
	@Test
	public void testDoGet() throws ServletException, IOException {
		/* Mock de la méthode init() afin de simplifier le test */
		this.connexion = partialMockBuilder(Connexion.class).addMockedMethod("init").createMock();
		this.connexion.init();
		expectLastCall();

		/* A la réception d'une requête GET, simple affichage du formulaire */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getRequestDispatcher(VUE_FORM)).andReturn(this.requestDispatcherMock);
		this.requestDispatcherMock.forward(this.httpServletRequestMock, this.httpservletResponseMock);
		expectLastCall();

		this.replayAll();

		/* Cycle de vie d'une servlet */
		this.connexion.init(this.servletConfigMock);
		this.connexion.doGet(this.httpServletRequestMock, this.httpservletResponseMock);
		this.connexion.destroy();

		this.verifyAll();
	}
	
	/**
	 * Teste la méthode protected void doPost(HttpServletRequest request, HttpServletResponse response).
	 * 
	 * <p>Cas du succès de la connexion d'un étudiant qui est dans une équipe : redirection vers la page d'accueil.</p>
	 * 
	 * @throws ServletException
	 * @throws IOException
	 */
	@Test
	public void testDoPostSuccesEquipe() throws ServletException, IOException {
		/* Récupération d'instances de DAO */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
		expect(this.daoFactoryMock.getUtilisateurDao()).andReturn(utilisateurDAO);
		expect(this.daoFactoryMock.getEquipeDAO()).andReturn(equipeDAO);
		expect(this.daoFactoryMock.getNotificationDao()).andReturn(notificationDAO);

		/* Récupération des données saisies */
		expect(this.httpServletRequestMock.getParameter(CHAMP_IDENTIFIANT)).andReturn(utilisateurTest2.getIdentifiant());
		expect(this.httpServletRequestMock.getParameter(CHAMP_MOT_DE_PASSE)).andReturn("test");
		
		/* Ajout du bean Utilisateur à la session */
		final Utilisateur utilisateur = new Utilisateur();
		utilisateur.setIdUtilisateur(ID_ETUDIANT_EQUIPE);
		expect(this.httpServletRequestMock.getSession()).andReturn(this.httpSessionMock);
		this.httpSessionMock.setAttribute(ATT_SESSION_USER, utilisateur);
		expectLastCall().asStub();
		
		/* Redirection vers la page d'accueil */
		expect(this.httpServletRequestMock.getContextPath()).andReturn(CONTEXT_PATH);
		this.httpservletResponseMock.sendRedirect(CONTEXT_PATH + VUE_INDEX);
		expectLastCall();
		
		/* validation des champs */
		this.httpServletRequestMock.setAttribute(CHAMP_ETAT_DEPOSE, EtatSujet.DEPOSE);
		expectLastCall();
		
		this.replayAll();
		
		/* Cycle de vie d'une servlet */
		this.connexion.init(this.servletConfigMock);
		this.connexion.doPost(this.httpServletRequestMock, this.httpservletResponseMock);
		this.connexion.destroy();
		
		this.verifyAll();
	}
	
	/**
	 * Teste la méthode protected void doPost(HttpServletRequest request, HttpServletResponse response).
	 * 
	 * <p>Cas du succès de la connexion d'un étudiant qui n'est pas dans une équipe : redirection vers la page d'accueil.</p>
	 * 
	 * @throws ServletException
	 * @throws IOException
	 */
	@Test
	public void testDoPostSuccesNonEquipe() throws ServletException, IOException {
		/* Récupération d'instances de DAO */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
		expect(this.daoFactoryMock.getUtilisateurDao()).andReturn(utilisateurDAO);
		expect(this.daoFactoryMock.getEquipeDAO()).andReturn(equipeDAO);
		expect(this.daoFactoryMock.getNotificationDao()).andReturn(notificationDAO);

		/* Récupération des données saisies */
		expect(this.httpServletRequestMock.getParameter(CHAMP_IDENTIFIANT)).andReturn(utilisateurTest.getIdentifiant());
		expect(this.httpServletRequestMock.getParameter(CHAMP_MOT_DE_PASSE)).andReturn("test");
		
		/* Ajout du bean Utilisateur à la session */
		final Utilisateur utilisateur = new Utilisateur();
		utilisateur.setIdUtilisateur(ID_ETUDIANT);
		
		expect(this.httpServletRequestMock.getSession()).andReturn(this.httpSessionMock);
		this.httpSessionMock.setAttribute(ATT_SESSION_USER, utilisateur);
		expectLastCall().asStub();
		
		/* Redirection vers la page d'accueil */
		expect(this.httpServletRequestMock.getContextPath()).andReturn(CONTEXT_PATH);
		this.httpservletResponseMock.sendRedirect(CONTEXT_PATH + VUE_INDEX);
		expectLastCall();
		
		/* validation des champs */
		this.httpServletRequestMock.setAttribute(CHAMP_ETAT_DEPOSE, EtatSujet.DEPOSE);
		expectLastCall();
		
		this.replayAll();
		
		/* Cycle de vie d'une servlet */
		this.connexion.init(this.servletConfigMock);
		this.connexion.doPost(this.httpServletRequestMock, this.httpservletResponseMock);
		this.connexion.destroy();
		
		this.verifyAll();
	}
	
	/**
	 * Teste la méthode protected void doPost(HttpServletRequest request, HttpServletResponse response).
	 * 
	 * <p>Cas de l'échec de la connexion 
	 * (connexion avec un identifiant qui n'existe pas dans la BDD) : affichage des erreurs.</p>
	 * 
	 * @throws ServletException
	 * @throws IOException
	 */
	@Test
	public void testDoPostEchecIdentifiantBDD() throws ServletException, IOException {
		/* Récupération d'instances de DAO */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
		expect(this.daoFactoryMock.getUtilisateurDao()).andReturn(utilisateurDAO);
		expect(this.daoFactoryMock.getEquipeDAO()).andReturn(equipeDAO);
		expect(this.daoFactoryMock.getNotificationDao()).andReturn(notificationDAO);

		/* Récupération des données saisies */
		expect(this.httpServletRequestMock.getParameter(CHAMP_IDENTIFIANT)).andReturn(ID_CONNEXION_ECHEC);
		expect(this.httpServletRequestMock.getParameter(CHAMP_MOT_DE_PASSE)).andReturn(MDP_CONNEXION_BDD_SUCCES);
		
		/* Mise en place des erreurs dans la requete */
		final Map<String, String> erreurs = new HashMap<>();
		erreurs.put(CHAMP_MOT_DE_PASSE, "Identifiant ou Mot de passe incorrect.");
		this.httpServletRequestMock.setAttribute(ATT_ERREURS, erreurs);
		expectLastCall();
		
		/* Mise en place du resultat dans la requete */
		final String resultat = "Échec de la connexion. ";
		this.httpServletRequestMock.setAttribute(ATT_RESULTAT, resultat);
		expectLastCall();

		/* Ré-affichage du formulaire avec les erreurs */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getRequestDispatcher(VUE_FORM)).andReturn(this.requestDispatcherMock);
		this.requestDispatcherMock.forward(this.httpServletRequestMock, this.httpservletResponseMock);
		expectLastCall();
		
		/* validation des champs */
		this.httpServletRequestMock.setAttribute(CHAMP_ETAT_DEPOSE, EtatSujet.DEPOSE);
		expectLastCall();
		
		this.replayAll();
		
		/* Cycle de vie d'une servlet */
		this.connexion.init(this.servletConfigMock);
		this.connexion.doPost(this.httpServletRequestMock, this.httpservletResponseMock);
		this.connexion.destroy();
		
		this.verifyAll();
	}
	
	/**
	 * Teste la méthode protected void doPost(HttpServletRequest request, HttpServletResponse response).
	 * 
	 * <p>Cas de l'échec de la connexion 
	 * (connexion à la BDD avec un mauvais mot de passe) : affichage des erreurs.</p>
	 * 
	 * @throws ServletException
	 * @throws IOException
	 */
	@Test
	public void testDoPostEchecMotDePasseBDD() throws ServletException, IOException {
		/* Récupération d'instances de DAO */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
		expect(this.daoFactoryMock.getUtilisateurDao()).andReturn(utilisateurDAO);
		expect(this.daoFactoryMock.getEquipeDAO()).andReturn(equipeDAO);
		expect(this.daoFactoryMock.getNotificationDao()).andReturn(notificationDAO);

		/* Récupération des données saisies */
		expect(this.httpServletRequestMock.getParameter(CHAMP_IDENTIFIANT)).andReturn(utilisateurTest.getIdentifiant());
		expect(this.httpServletRequestMock.getParameter(CHAMP_MOT_DE_PASSE)).andReturn("test2");
		
		/* Mise en place des erreurs dans la requete */
		final Map<String, String> erreurs = new HashMap<>();
		erreurs.put(CHAMP_MOT_DE_PASSE, "Identifiant ou Mot de passe incorrect.");
		this.httpServletRequestMock.setAttribute(ATT_ERREURS, erreurs);
		expectLastCall();
		
		/* Mise en place du resultat dans la requete */
		final String resultat = "Échec de la connexion. ";
		this.httpServletRequestMock.setAttribute(ATT_RESULTAT, resultat);
		expectLastCall();

		/* Ré-affichage du formulaire avec les erreurs */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getRequestDispatcher(VUE_FORM)).andReturn(this.requestDispatcherMock);
		this.requestDispatcherMock.forward(this.httpServletRequestMock, this.httpservletResponseMock);
		expectLastCall();
		
		/* validation des champs */
		this.httpServletRequestMock.setAttribute(CHAMP_ETAT_DEPOSE, EtatSujet.DEPOSE);
		expectLastCall();
		
		this.replayAll();
		
		/* Cycle de vie d'une servlet */
		this.connexion.init(this.servletConfigMock);
		this.connexion.doPost(this.httpServletRequestMock, this.httpservletResponseMock);
		this.connexion.destroy();
		
		this.verifyAll();
	}
	
	/**
	 * Teste la méthode protected void doPost(HttpServletRequest request, HttpServletResponse response).
	 * 
	 * <p>Cas de l'échec de la connexion 
	 * (connexion à l'AD avec un mauvais identifiant ou un mauvais mot de passe) : affichage des erreurs.</p>
	 * 
	 * @throws ServletException
	 * @throws IOException
	 */
	@Test
	public void testDoPostEchecMotDePasseAD() throws ServletException, IOException {
		/* Récupération d'instances de DAO */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
		expect(this.daoFactoryMock.getUtilisateurDao()).andReturn(utilisateurDAO);
		expect(this.daoFactoryMock.getEquipeDAO()).andReturn(equipeDAO);
		expect(this.daoFactoryMock.getNotificationDao()).andReturn(notificationDAO);

		/* Récupération des données saisies */
		expect(this.httpServletRequestMock.getParameter(CHAMP_IDENTIFIANT)).andReturn(utilisateurTest3.getIdentifiant());
		expect(this.httpServletRequestMock.getParameter(CHAMP_MOT_DE_PASSE)).andReturn("test3");
		
		/* Mise en place des erreurs dans la requete */
		final Map<String, String> erreurs = new HashMap<>();
		erreurs.put(CHAMP_MOT_DE_PASSE, "Identifiant ou Mot de passe incorrect.");
		this.httpServletRequestMock.setAttribute(ATT_ERREURS, erreurs);
		expectLastCall();
		
		/* Mise en place du resultat dans la requete */
		final String resultat = "Échec de la connexion. ";
		this.httpServletRequestMock.setAttribute(ATT_RESULTAT, resultat);
		expectLastCall();

		/* Ré-affichage du formulaire avec les erreurs */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getRequestDispatcher(VUE_FORM)).andReturn(this.requestDispatcherMock);
		this.requestDispatcherMock.forward(this.httpServletRequestMock, this.httpservletResponseMock);
		expectLastCall();
		
		/* validation des champs */
		this.httpServletRequestMock.setAttribute(CHAMP_ETAT_DEPOSE, EtatSujet.DEPOSE);
		expectLastCall();
		
		this.replayAll();
		
		/* Cycle de vie d'une servlet */
		this.connexion.init(this.servletConfigMock);
		this.connexion.doPost(this.httpServletRequestMock, this.httpservletResponseMock);
		this.connexion.destroy();
		
		this.verifyAll();
	}
	
	/**
	 * Teste la méthode protected void doPost(HttpServletRequest request, HttpServletResponse response).
	 * 
	 * <p>Cas de l'échec de la connexion 
	 * (connexion à la BDD par un utilisateur non validé) : affichage des erreurs.</p>
	 * 
	 * @throws ServletException
	 * @throws IOException
	 */
	@Test
	public void testDoPostEchecValide() throws ServletException, IOException {
		/* Récupération d'instances de DAO */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
		expect(this.daoFactoryMock.getUtilisateurDao()).andReturn(utilisateurDAO);
		expect(this.daoFactoryMock.getEquipeDAO()).andReturn(equipeDAO);
		expect(this.daoFactoryMock.getNotificationDao()).andReturn(notificationDAO);

		/* Récupération des données saisies */
		expect(this.httpServletRequestMock.getParameter(CHAMP_IDENTIFIANT)).andReturn(utilisateurTest4.getIdentifiant());
		expect(this.httpServletRequestMock.getParameter(CHAMP_MOT_DE_PASSE)).andReturn("test");
		
		/* Mise en place des erreurs dans la requete */
		final Map<String, String> erreurs = new HashMap<>();
		erreurs.put(CHAMP_MOT_DE_PASSE, "Compte non validé par l'administrateur.");
		this.httpServletRequestMock.setAttribute(ATT_ERREURS, erreurs);
		expectLastCall();
		
		/* Mise en place du resultat dans la requete */
		final String resultat = "Échec de la connexion. ";
		this.httpServletRequestMock.setAttribute(ATT_RESULTAT, resultat);
		expectLastCall();

		/* Ré-affichage du formulaire avec les erreurs */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getRequestDispatcher(VUE_FORM)).andReturn(this.requestDispatcherMock);
		this.requestDispatcherMock.forward(this.httpServletRequestMock, this.httpservletResponseMock);
		expectLastCall();
		
		/* validation des champs */
		this.httpServletRequestMock.setAttribute(CHAMP_ETAT_DEPOSE, EtatSujet.DEPOSE);
		expectLastCall();
		
		this.replayAll();
		
		/* Cycle de vie d'une servlet */
		this.connexion.init(this.servletConfigMock);
		this.connexion.doPost(this.httpServletRequestMock, this.httpservletResponseMock);
		this.connexion.destroy();
		
		this.verifyAll();
	}
	
}