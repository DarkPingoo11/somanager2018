package fr.eseo.ld.servlets;

import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.expectLastCall;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.easymock.EasyMockRunner;
import org.easymock.EasyMockSupport;
import org.easymock.Mock;
import org.easymock.MockType;
import org.easymock.TestSubject;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import fr.eseo.ld.beans.Utilisateur;
import fr.eseo.ld.dao.DAOFactory;
import fr.eseo.ld.dao.EquipeDAO;
import fr.eseo.ld.dao.EtudiantDAO;
import fr.eseo.ld.dao.JuryPosterDAO;
import fr.eseo.ld.dao.JurySoutenanceDAO;
import fr.eseo.ld.dao.NotePosterDAO;
import fr.eseo.ld.dao.NoteSoutenanceDAO;
import fr.eseo.ld.dao.SoutenanceDAO;
import fr.eseo.ld.dao.UtilisateurDAO;

/**
 * Classe de tests unitaires JUnit 4 de la classe GererNotes.
 *
 * <p>Utilisation de mocks (construits avec EasyMock).</p>
 *
 * @version 1.0
 * @author Pierre CESMAT
 *
 * @see fr.eseo.ld.servlets.GererNotes
 * @see org.junit
 * @see org.easymock
 */
@RunWith(EasyMockRunner.class)
public class GererNotesTest extends EasyMockSupport {

	private static final String CONF_DAO_FACTORY = "daoFactory";
	
	private static final String ATT_SESSION_USER = "utilisateur";
	private static final String TEST = "test";
	private static final Long ID_PROF = 71L;
	private static final Long ID_UTILISATEUR = 71L;
	private static final String ATT_DISPO_NOTES_INTERMEDIAIRE = "dispoNotesIntermediaire";
	private static final String ATT_DISPO_NOTES_PROJET = "dispoNotesProjet";
	private static final String ATT_DISPO_NOTES_POSTER = "dispoNotesPoster";
	private static final String ATT_DISPO_NOTES_SOUTENANCE = "dispoNotesSoutenance";
	private static final String ATT_TOUTES_NOTES_INTERMEDIAIRE = "toutesNotesIntermediaire";
	private static final String ATT_TOUTES_NOTES_PROJET = "toutesNotesProjet";
	private static final String ATT_TOUTES_NOTES_POSTER = "toutesNotesPoster";
	private static final String ATT_TOUTES_NOTES_SOUTENANCE = "toutesNotesSoutenance";
	private static final String CHAMP_NOTES_INTERMEDIAIRE = "notesIntermediaire";
	private static final String CHAMP_NOTES_POSTER = "notesPoster";
	private static final String CHAMP_NOTES_PROJET = "notesProjet";
	private static final String CHAMP_NOTES_SOUTENANCE = "notesSoutenance";
	
	private static final String VUE_FORM = "/WEB-INF/formulaires/gererNotes.jsp";
	
	private static Utilisateur utilisateur;
	private static EtudiantDAO etudiantDAO;
	private static UtilisateurDAO utilisateurDAO;
	private static EquipeDAO equipeDAO;
	private static JuryPosterDAO juryPosterDAO;
	private static JurySoutenanceDAO jurySoutenanceDAO;
	private static NotePosterDAO notePosterDAO;
	private static SoutenanceDAO soutenanceDAO;
	private static NoteSoutenanceDAO noteSoutenanceDAO;
	
	@TestSubject
	private GererNotes gererNotes = new GererNotes();
	
	@Mock
	private ServletConfig servletConfigMock;
	
	@Mock
	private ServletContext servletContextMock;
	
	@Mock(type = MockType.NICE)
	private HttpServletRequest httpServletRequestMock;
	
	@Mock(type = MockType.NICE)
	private HttpServletResponse httpservletResponseMock;
	
	@Mock
	private HttpSession httpSessionMock;
	
	@Mock
	private RequestDispatcher requestDispatcherMock;
	
	@Mock
	private DAOFactory daoFactoryMock;
	
	/**
	 * Récupère des instances de DAO.
	 * 
	 * @throws Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		/* Instanciations de DAO */
		DAOFactory daoFactory = DAOFactory.getInstance();
		utilisateurDAO = daoFactory.getUtilisateurDao();
		etudiantDAO = daoFactory.getEtudiantDao();
		equipeDAO = daoFactory.getEquipeDAO();
		notePosterDAO = daoFactory.getNotePosterDao();
		soutenanceDAO = daoFactory.getSoutenanceDAO();
		noteSoutenanceDAO = daoFactory.getNoteSoutenanceDAO();
		juryPosterDAO = daoFactory.getJuryPosterDAO();
		jurySoutenanceDAO = daoFactory.getJurySoutenanceDAO();
		
	}
	
	/**
	 * Teste la méthode protected void doGet(HttpServletRequest request, HttpServletResponse response).
	 * 
	 * @throws ServletException
	 * @throws IOException
	 */
	@Test
	public void testDoGet() throws ServletException, IOException {
		/* Récupération d'instances de DAO */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
		expect(this.daoFactoryMock.getUtilisateurDao()).andReturn(utilisateurDAO);
		expect(this.daoFactoryMock.getJurySoutenanceDAO()).andReturn(jurySoutenanceDAO);
		expect(this.daoFactoryMock.getEtudiantDao()).andReturn(etudiantDAO);
		expect(this.daoFactoryMock.getEquipeDAO()).andReturn(equipeDAO);
		expect(this.daoFactoryMock.getJuryPosterDAO()).andReturn(juryPosterDAO);
		expect(this.daoFactoryMock.getNotePosterDao()).andReturn(notePosterDAO);
		expect(this.daoFactoryMock.getSoutenanceDAO()).andReturn(soutenanceDAO);
		expect(this.daoFactoryMock.getNoteSoutenanceDAO()).andReturn(noteSoutenanceDAO);
		
		/* Récupération d'attribut dans la request */
		expect(this.httpServletRequestMock.getAttribute(GererNotesTest.ATT_TOUTES_NOTES_INTERMEDIAIRE)).andReturn("non");
		expect(this.httpServletRequestMock.getAttribute(GererNotesTest.ATT_TOUTES_NOTES_POSTER)).andReturn("non");
		expect(this.httpServletRequestMock.getAttribute(GererNotesTest.ATT_TOUTES_NOTES_PROJET)).andReturn("non");
		expect(this.httpServletRequestMock.getAttribute(GererNotesTest.ATT_TOUTES_NOTES_SOUTENANCE)).andReturn("non");
		expect(this.httpServletRequestMock.getAttribute(GererNotesTest.ATT_DISPO_NOTES_INTERMEDIAIRE)).andReturn("oui");
		expect(this.httpServletRequestMock.getAttribute(GererNotesTest.ATT_DISPO_NOTES_POSTER)).andReturn("oui");
		expect(this.httpServletRequestMock.getAttribute(GererNotesTest.ATT_DISPO_NOTES_PROJET)).andReturn("oui");
		expect(this.httpServletRequestMock.getAttribute(GererNotesTest.ATT_DISPO_NOTES_SOUTENANCE)).andReturn("oui");
		
		/* Récupération de l'Utilisateur en session 
		expect(this.httpServletRequestMock.getSession()).andReturn(this.httpSessionMock);
		final Utilisateur utilisateur = new Utilisateur();
		final Long idUtilisateur = ID_UTILISATEUR;
		utilisateur.setIdUtilisateur(idUtilisateur);
		expect(this.httpSessionMock.getAttribute(ATT_SESSION_USER)).andReturn(utilisateur);
		*/
		/* Affichage du formulaire */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getRequestDispatcher(VUE_FORM)).andReturn(this.requestDispatcherMock);
		this.requestDispatcherMock.forward(this.httpServletRequestMock, this.httpservletResponseMock);
		expectLastCall();

		this.replayAll();

		/* Cycle de vie d'une servlet */
		this.gererNotes.init(this.servletConfigMock);
		this.gererNotes.doGet(this.httpServletRequestMock, this.httpservletResponseMock);
		this.gererNotes.destroy();

		this.verifyAll();
	}
	
	/**
	 * Teste la méthode protected void doPost(HttpServletRequest request, HttpServletResponse response).
	 * 
	 * @throws ServletException
	 * @throws IOException
	 */
	@Test
	public void testDoPost() throws ServletException, IOException {
		/* Récupération d'instances de DAO */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
		expect(this.daoFactoryMock.getUtilisateurDao()).andReturn(utilisateurDAO);
		expect(this.daoFactoryMock.getJurySoutenanceDAO()).andReturn(jurySoutenanceDAO);
		expect(this.daoFactoryMock.getEtudiantDao()).andReturn(etudiantDAO);
		expect(this.daoFactoryMock.getEquipeDAO()).andReturn(equipeDAO);
		expect(this.daoFactoryMock.getJuryPosterDAO()).andReturn(juryPosterDAO);
		expect(this.daoFactoryMock.getNotePosterDao()).andReturn(notePosterDAO);
		expect(this.daoFactoryMock.getSoutenanceDAO()).andReturn(soutenanceDAO);
		expect(this.daoFactoryMock.getNoteSoutenanceDAO()).andReturn(noteSoutenanceDAO);
		
		/* Récupération d'attribut dans la request */
		expect(this.httpServletRequestMock.getParameter(GererNotesTest.CHAMP_NOTES_INTERMEDIAIRE)).andReturn("oui");
		expectLastCall().times(2);
		expect(this.httpServletRequestMock.getParameter(GererNotesTest.CHAMP_NOTES_POSTER)).andReturn("oui");
		expectLastCall().times(2);
		expect(this.httpServletRequestMock.getParameter(GererNotesTest.CHAMP_NOTES_PROJET)).andReturn("oui");
		expectLastCall().times(2);
		expect(this.httpServletRequestMock.getParameter(GererNotesTest.CHAMP_NOTES_SOUTENANCE)).andReturn("oui");
		expectLastCall().times(2);

		this.replayAll();

		/* Cycle de vie d'une servlet */
		this.gererNotes.init(this.servletConfigMock);
		this.gererNotes.doPost(this.httpServletRequestMock, this.httpservletResponseMock);
		this.gererNotes.destroy();

		this.verifyAll();
	}
	
}