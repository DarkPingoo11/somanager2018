package fr.eseo.ld.servlets;

import fr.eseo.ld.beans.Utilisateur;
import fr.eseo.ld.dao.*;
import fr.eseo.ld.pgl.beans.BonusMalus;
import fr.eseo.ld.pgl.beans.Etudiant;

import org.easymock.*;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.expectLastCall;


/**
 * Classe de tests unitaires JUnit 4 de la classe GererBonusMalus.
 *
 * <p>Utilisation de mocks (construits avec EasyMock).</p>
 *
 * @version 1.0
 * @see fr.eseo.ld.servlets.GererBonusMalus
 * @see org.junit
 * @see org.easymock
 */
@RunWith(EasyMockRunner.class)
public class GererBonusMalusTest extends EasyMockSupport {

	private static final String CONF_DAO_FACTORY = "daoFactory";
	private static final Long ID_UTILISATEUR = 10L;
	private static final Long ID_ETUDIANT_ALPHA1 = 21L;
	private static final Long ID_ETUDIANT_ALPHA2 = 22L;
	private static final String VUE_FORM = "/WEB-INF/formulaires/bonusMalus.jsp";
	
	private static final String ATT_FORMULAIRE = "formulaire";
	private static final String ATT_SESSION_USER = "utilisateur";
	
	private static final String CHAMP_ID_BONUS_MALUS = "id-";
    private static final String CHAMP_VALEUR = "radio-";
    private static final String CHAMP_VALIDATION= "validation-";
    private static final String CHAMP_JUSTIFICATION = "justification-";
    private static final String CHAMP_REF_SPRINT = "refSprint-";
    private static final String CHAMP_REF_ETUDIANT = "etudiant-";
    private static final String CHAMP_REF_EVALUATEUR = "refEvaluateur-";

	private static UtilisateurDAO utilisateurDAO;
	private static PglEquipeDAO pglEquipeDAO;
	private static BonusMalusDAO bonusMalusDAO;
	private static SprintDAO sprintDAO;
	private static PglEtudiantDAO pglEtudiantDAO;
	private static AnneeScolaireDAO anneeScolaireDAO;
	private static PglNoteDAO pglNoteDAO;
	private static MatiereDAO matiereDAO;
	private static PglEtudiantEquipeDAO etudiantEquipeDAO;
	private static NotificationDAO notificationDAO;

	@Mock
	private static UtilisateurDAO utilisateurDAOMock;

	@TestSubject
	private GererBonusMalus gererBonusMalus = new GererBonusMalus();
	
	@Mock
	private ServletConfig servletConfigMock;
	
	@Mock
	private ServletContext servletContextMock;
	
	@Mock(type = MockType.NICE)
	private HttpServletRequest httpServletRequestMock;
	
	@Mock
	private HttpServletResponse httpservletResponseMock;
	
	@Mock
	private HttpSession httpSessionMock;
	
	@Mock
	private RequestDispatcher requestDispatcherMock;
	
	@Mock
	private DAOFactory daoFactoryMock;

	/**
	 * Récupère des instances de DAO.
	 * 
	 * @throws Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		DAOFactory daoFactory = DAOFactory.getInstance();
		bonusMalusDAO = daoFactory.getBonusMalusDAO();
		utilisateurDAO = daoFactory.getUtilisateurDao();
		pglEquipeDAO = daoFactory.getPglEquipeDAO();
		sprintDAO = daoFactory.getSprintDAO();
		pglEtudiantDAO = daoFactory.getPglEtudiantDAO();
		anneeScolaireDAO = daoFactory.getAnneeScolaireDAO();
		pglNoteDAO = daoFactory.getPglNoteDAO();
		matiereDAO = daoFactory.getMatiereDAO();
		etudiantEquipeDAO = daoFactory.getPglEtudiantEquipeDAO();
		notificationDAO = daoFactory.getNotificationDao();
	}

	/**
	 * Teste la méthode protected void doGet(HttpServletRequest request, HttpServletResponse response).
	 * 
	 * @throws ServletException
	 * @throws IOException
	 */
	@Test
	public void testDoGetEmpty() throws ServletException, IOException {
		
		/* Récupération d'instances de DAO */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
		expect(this.daoFactoryMock.getBonusMalusDAO()).andReturn(bonusMalusDAO);
		expect(this.daoFactoryMock.getUtilisateurDao()).andReturn(utilisateurDAO);
		expect(this.daoFactoryMock.getPglEquipeDAO()).andReturn(pglEquipeDAO);
		expect(this.daoFactoryMock.getSprintDAO()).andReturn(sprintDAO);
		expect(this.daoFactoryMock.getPglEtudiantDAO()).andReturn(pglEtudiantDAO);
		expect(this.daoFactoryMock.getAnneeScolaireDAO()).andReturn(anneeScolaireDAO);
		expect(this.daoFactoryMock.getPglNoteDAO()).andReturn(pglNoteDAO);
		expect(this.daoFactoryMock.getMatiereDAO()).andReturn(matiereDAO);
		expect(this.daoFactoryMock.getPglEtudiantEquipeDAO()).andReturn(etudiantEquipeDAO);
		expect(this.daoFactoryMock.getNotificationDao()).andReturn(notificationDAO);

		/* Récupération de l'Utilisateur en session */
		expect(this.httpServletRequestMock.getSession()).andReturn(this.httpSessionMock);
		final Utilisateur utilisateur = new Utilisateur();
		final Long idUtilisateur = ID_UTILISATEUR;
		utilisateur.setIdUtilisateur(idUtilisateur);
		expect(this.httpSessionMock.getAttribute(ATT_SESSION_USER)).andReturn(utilisateur);

		/* Affichage du formulaire */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getRequestDispatcher(VUE_FORM)).andReturn(this.requestDispatcherMock);
		this.requestDispatcherMock.forward(this.httpServletRequestMock, this.httpservletResponseMock);
		expectLastCall();
		this.replayAll();

		/* Cycle de vie d'une servlet */
		this.gererBonusMalus.init(this.servletConfigMock);
		this.gererBonusMalus.doGet(this.httpServletRequestMock, this.httpservletResponseMock);
		this.gererBonusMalus.destroy();
	}

	/**
	 * Teste la méthode protected void doGet(HttpServletRequest request, HttpServletResponse response).
	 * 
	 * @throws ServletException
	 * @throws IOException
	 */
	@Test
	public void testDoGet_ListeEtudiant() throws ServletException, IOException {
		
		/* Récupération d'instances de DAO */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
		expect(this.daoFactoryMock.getBonusMalusDAO()).andReturn(bonusMalusDAO);
		expect(this.daoFactoryMock.getUtilisateurDao()).andReturn(utilisateurDAO);
		expect(this.daoFactoryMock.getPglEquipeDAO()).andReturn(pglEquipeDAO);
		expect(this.daoFactoryMock.getSprintDAO()).andReturn(sprintDAO);
		expect(this.daoFactoryMock.getPglEtudiantDAO()).andReturn(pglEtudiantDAO);
		expect(this.daoFactoryMock.getAnneeScolaireDAO()).andReturn(anneeScolaireDAO);
		expect(this.daoFactoryMock.getPglNoteDAO()).andReturn(pglNoteDAO);
		expect(this.daoFactoryMock.getMatiereDAO()).andReturn(matiereDAO);
		expect(this.daoFactoryMock.getPglEtudiantEquipeDAO()).andReturn(etudiantEquipeDAO);
		expect(this.daoFactoryMock.getNotificationDao()).andReturn(notificationDAO);

		/* Récupération de l'Utilisateur en session */
		expect(this.httpServletRequestMock.getSession()).andReturn(this.httpSessionMock);
		final Utilisateur utilisateur = new Utilisateur();
		final Long idUtilisateur = ID_UTILISATEUR;
		utilisateur.setIdUtilisateur(idUtilisateur);
		expect(this.httpSessionMock.getAttribute(ATT_SESSION_USER)).andReturn(utilisateur);

		/* Récupération de la liste des étudiants */
		final Etudiant etudiantAlpha1 = new Etudiant();
		final Etudiant etudiantAlpha2 = new Etudiant();
		final Long idEtudiantAlpha1 = ID_ETUDIANT_ALPHA1;
		final Long idEtudiantAlpha2 = ID_ETUDIANT_ALPHA2;
		etudiantAlpha1.setIdEtudiant(idEtudiantAlpha1);
		etudiantAlpha2.setIdEtudiant(idEtudiantAlpha2);
		final List<Etudiant> listeEtudiants = new ArrayList<>();
		listeEtudiants.add(etudiantAlpha1);
		listeEtudiants.add(etudiantAlpha2);
		expect(this.httpServletRequestMock.getAttribute("listePglEtudiant")).andReturn(listeEtudiants);
		
		/* Affichage du formulaire */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getRequestDispatcher(VUE_FORM)).andReturn(this.requestDispatcherMock);
		this.requestDispatcherMock.forward(this.httpServletRequestMock, this.httpservletResponseMock);
		expectLastCall();
		this.replayAll();

		/* Cycle de vie d'une servlet */
		this.gererBonusMalus.init(this.servletConfigMock);
		this.gererBonusMalus.doGet(this.httpServletRequestMock, this.httpservletResponseMock);
		this.gererBonusMalus.destroy();
	}
	
    /**
     * Teste la méthode protected void doPost(HttpServletRequest request, HttpServletResponse response).
     *
     * @throws ServletException
     * @throws IOException
     */
    @Test
    public void test2_1DoPost_BonusMalus() throws ServletException, IOException {
    	/* Récupération d'instances de DAO */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
		expect(this.daoFactoryMock.getBonusMalusDAO()).andReturn(bonusMalusDAO);
		expect(this.daoFactoryMock.getUtilisateurDao()).andReturn(utilisateurDAO);
		expect(this.daoFactoryMock.getPglEquipeDAO()).andReturn(pglEquipeDAO);
		expect(this.daoFactoryMock.getSprintDAO()).andReturn(sprintDAO);
		expect(this.daoFactoryMock.getPglEtudiantDAO()).andReturn(pglEtudiantDAO);
		expect(this.daoFactoryMock.getAnneeScolaireDAO()).andReturn(anneeScolaireDAO);
		expect(this.daoFactoryMock.getPglNoteDAO()).andReturn(pglNoteDAO);
		expect(this.daoFactoryMock.getMatiereDAO()).andReturn(matiereDAO);
		expect(this.daoFactoryMock.getPglEtudiantEquipeDAO()).andReturn(etudiantEquipeDAO);
		expect(this.daoFactoryMock.getNotificationDao()).andReturn(notificationDAO);
		
        /* Récupération des données saisies */
        expect(this.httpServletRequestMock.getParameter(ATT_FORMULAIRE)).andReturn("ajouterMeeting");
        expectLastCall().times(4);

        /* Récupération de l'utilisateur en session */
        final Utilisateur utilisateurSession = new Utilisateur();
        final Long idUtilisateurSession = ID_UTILISATEUR;
        utilisateurSession.setIdUtilisateur(idUtilisateurSession);
        expect(this.httpServletRequestMock.getSession()).andReturn(this.httpSessionMock);
        expect(this.httpSessionMock.getAttribute(ATT_SESSION_USER)).andReturn(utilisateurSession);

        expect(this.httpServletRequestMock.getParameter(CHAMP_ID_BONUS_MALUS)).andReturn(String.valueOf(1));
        expect(this.httpServletRequestMock.getParameter(CHAMP_VALEUR)).andReturn(String.valueOf(0));
        expect(this.httpServletRequestMock.getParameter(CHAMP_VALIDATION)).andReturn("ACCEPTER");
        expect(this.httpServletRequestMock.getParameter(CHAMP_JUSTIFICATION)).andReturn("test");
        expect(this.httpServletRequestMock.getParameter(CHAMP_REF_SPRINT)).andReturn(String.valueOf(1));
        expect(this.httpServletRequestMock.getParameter(CHAMP_REF_ETUDIANT)).andReturn(String.valueOf(ID_UTILISATEUR));
        expect(this.httpServletRequestMock.getParameter(CHAMP_REF_EVALUATEUR)).andReturn(String.valueOf(ID_UTILISATEUR));       

        expect(this.httpServletRequestMock.getSession()).andReturn(this.httpSessionMock);
        expect(this.httpSessionMock.getAttribute(ATT_SESSION_USER)).andReturn(utilisateurSession);

        /* Ré-affichage du formulaire avec message de succès */
        expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
        expect(this.servletContextMock.getRequestDispatcher(VUE_FORM)).andReturn(this.requestDispatcherMock);
        this.requestDispatcherMock.forward(this.httpServletRequestMock, this.httpservletResponseMock);
        expectLastCall();


        this.replayAll();
        /* Cycle de vie d'une servlet */
        this.gererBonusMalus.init(this.servletConfigMock);
        this.gererBonusMalus.doPost(this.httpServletRequestMock, this.httpservletResponseMock);
        this.gererBonusMalus.destroy();

    }
    
    /**
     * Teste la méthode protected void doPost(HttpServletRequest request, HttpServletResponse response).
     *
     * <p>Cas d'une selection des different selectionneurs des bonus malus.</p>
     *
     * @throws ServletException
     * @throws IOException
     */
    @Test
    public void test2_1DoPostSelectAttribut() throws ServletException, IOException {
    	/* Récupération d'instances de DAO */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
		expect(this.daoFactoryMock.getBonusMalusDAO()).andReturn(bonusMalusDAO);
		expect(this.daoFactoryMock.getUtilisateurDao()).andReturn(utilisateurDAO);
		expect(this.daoFactoryMock.getPglEquipeDAO()).andReturn(pglEquipeDAO);
		expect(this.daoFactoryMock.getSprintDAO()).andReturn(sprintDAO);
		expect(this.daoFactoryMock.getPglEtudiantDAO()).andReturn(pglEtudiantDAO);
		expect(this.daoFactoryMock.getAnneeScolaireDAO()).andReturn(anneeScolaireDAO);
		expect(this.daoFactoryMock.getPglNoteDAO()).andReturn(pglNoteDAO);
		expect(this.daoFactoryMock.getMatiereDAO()).andReturn(matiereDAO);
		expect(this.daoFactoryMock.getPglEtudiantEquipeDAO()).andReturn(etudiantEquipeDAO);
		expect(this.daoFactoryMock.getNotificationDao()).andReturn(notificationDAO);
		
		 /* Récupération de l'utilisateur en session */
        final Utilisateur utilisateurSession = new Utilisateur();
        final Long idUtilisateurSession = ID_UTILISATEUR;
        utilisateurSession.setIdUtilisateur(idUtilisateurSession);
        expect(this.httpServletRequestMock.getSession()).andReturn(this.httpSessionMock);
        expect(this.httpSessionMock.getAttribute(ATT_SESSION_USER)).andReturn(utilisateurSession);
        
		/* Récupération des données saisies */
        final String anneeSelect = new String();
        final String equipeSelect = new String();
        final String sprintSelect = new String();
        expect(this.httpServletRequestMock.getParameter("anneeSelect")).andReturn(anneeSelect);
        expect(this.httpServletRequestMock.getParameter("equipeSelect")).andReturn(equipeSelect);
        expect(this.httpServletRequestMock.getParameter("sprintSelect")).andReturn(sprintSelect);

        expect(this.httpServletRequestMock.getSession()).andReturn(this.httpSessionMock);
        expect(this.httpSessionMock.getAttribute(ATT_SESSION_USER)).andReturn(utilisateurSession);

        /* Ré-affichage du formulaire avec message de succès */
        expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
        expect(this.servletContextMock.getRequestDispatcher(VUE_FORM)).andReturn(this.requestDispatcherMock);
        this.requestDispatcherMock.forward(this.httpServletRequestMock, this.httpservletResponseMock);
        expectLastCall();

        this.replayAll();
        /* Cycle de vie d'une servlet */
        this.gererBonusMalus.init(this.servletConfigMock);
        this.gererBonusMalus.doPost(this.httpServletRequestMock, this.httpservletResponseMock);
        this.gererBonusMalus.destroy();
    }
    
    /**
     * Teste la méthode protected void doPost(HttpServletRequest request, HttpServletResponse response).
     *
     * @throws ServletException
     * @throws IOException
     */
    @Test
    public void test2_1DoGet_ListeBonusMalus() throws ServletException, IOException {
    	/* Récupération d'instances de DAO */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
		expect(this.daoFactoryMock.getBonusMalusDAO()).andReturn(bonusMalusDAO);
		expect(this.daoFactoryMock.getUtilisateurDao()).andReturn(utilisateurDAO);
		expect(this.daoFactoryMock.getPglEquipeDAO()).andReturn(pglEquipeDAO);
		expect(this.daoFactoryMock.getSprintDAO()).andReturn(sprintDAO);
		expect(this.daoFactoryMock.getPglEtudiantDAO()).andReturn(pglEtudiantDAO);
		expect(this.daoFactoryMock.getAnneeScolaireDAO()).andReturn(anneeScolaireDAO);
		expect(this.daoFactoryMock.getPglNoteDAO()).andReturn(pglNoteDAO);
		expect(this.daoFactoryMock.getMatiereDAO()).andReturn(matiereDAO);
		expect(this.daoFactoryMock.getPglEtudiantEquipeDAO()).andReturn(etudiantEquipeDAO);
		expect(this.daoFactoryMock.getNotificationDao()).andReturn(notificationDAO);
		
		 /* Récupération de l'utilisateur en session */
        final Utilisateur utilisateurSession = new Utilisateur();
        final Long idUtilisateurSession = ID_UTILISATEUR;
        utilisateurSession.setIdUtilisateur(idUtilisateurSession);
        expect(this.httpServletRequestMock.getSession()).andReturn(this.httpSessionMock);
        expect(this.httpSessionMock.getAttribute(ATT_SESSION_USER)).andReturn(utilisateurSession);
        
		/* Récupération des données */
        final BonusMalus bonusMalus = new BonusMalus();
        bonusMalus.setIdBonusMalus(12L);
        final List<BonusMalus> listeTemp = new ArrayList<>();
        listeTemp.add(bonusMalus);
        expect(this.httpServletRequestMock.getAttribute("listeBonusMalus")).andReturn(listeTemp);

        expect(this.httpServletRequestMock.getSession()).andReturn(this.httpSessionMock);
        expect(this.httpSessionMock.getAttribute(ATT_SESSION_USER)).andReturn(utilisateurSession);

        /* Ré-affichage du formulaire avec message de succès */
        expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
        expect(this.servletContextMock.getRequestDispatcher(VUE_FORM)).andReturn(this.requestDispatcherMock);
        this.requestDispatcherMock.forward(this.httpServletRequestMock, this.httpservletResponseMock);
        expectLastCall();

        this.replayAll();
        /* Cycle de vie d'une servlet */
        this.gererBonusMalus.init(this.servletConfigMock);
        this.gererBonusMalus.doPost(this.httpServletRequestMock, this.httpservletResponseMock);
        this.gererBonusMalus.destroy();
    }
    
    /**
     * Teste la méthode protected void doPost(HttpServletRequest request, HttpServletResponse response).
     *
     * @throws ServletException
     * @throws IOException
     */
    @Test
    public void test2_1DoGet_Boolean() throws ServletException, IOException {
    	/* Récupération d'instances de DAO */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
		expect(this.daoFactoryMock.getBonusMalusDAO()).andReturn(bonusMalusDAO);
		expect(this.daoFactoryMock.getUtilisateurDao()).andReturn(utilisateurDAO);
		expect(this.daoFactoryMock.getPglEquipeDAO()).andReturn(pglEquipeDAO);
		expect(this.daoFactoryMock.getSprintDAO()).andReturn(sprintDAO);
		expect(this.daoFactoryMock.getPglEtudiantDAO()).andReturn(pglEtudiantDAO);
		expect(this.daoFactoryMock.getAnneeScolaireDAO()).andReturn(anneeScolaireDAO);
		expect(this.daoFactoryMock.getPglNoteDAO()).andReturn(pglNoteDAO);
		expect(this.daoFactoryMock.getMatiereDAO()).andReturn(matiereDAO);
		expect(this.daoFactoryMock.getPglEtudiantEquipeDAO()).andReturn(etudiantEquipeDAO);
		expect(this.daoFactoryMock.getNotificationDao()).andReturn(notificationDAO);
		
		 /* Récupération de l'utilisateur en session */
        final Utilisateur utilisateurSession = new Utilisateur();
        final Long idUtilisateurSession = ID_UTILISATEUR;
        utilisateurSession.setIdUtilisateur(idUtilisateurSession);
        expect(this.httpServletRequestMock.getSession()).andReturn(this.httpSessionMock);
        expect(this.httpSessionMock.getAttribute(ATT_SESSION_USER)).andReturn(utilisateurSession);
        
		/* Récupération des données */
        final Boolean elementPresentDansTableau = false;
        final Boolean toutValide = false;
        final Boolean nonEnvoyer = true;
        expect(this.httpServletRequestMock.getAttribute("elementPresentDansTableau")).andReturn(elementPresentDansTableau);
        expect(this.httpServletRequestMock.getAttribute("toutValide")).andReturn(toutValide);
        expect(this.httpServletRequestMock.getAttribute("nonEnvoyer")).andReturn(nonEnvoyer);

        expect(this.httpServletRequestMock.getSession()).andReturn(this.httpSessionMock);
        expect(this.httpSessionMock.getAttribute(ATT_SESSION_USER)).andReturn(utilisateurSession);

        /* Ré-affichage du formulaire avec message de succès */
        expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
        expect(this.servletContextMock.getRequestDispatcher(VUE_FORM)).andReturn(this.requestDispatcherMock);
        this.requestDispatcherMock.forward(this.httpServletRequestMock, this.httpservletResponseMock);
        expectLastCall();

        this.replayAll();
        /* Cycle de vie d'une servlet */
        this.gererBonusMalus.init(this.servletConfigMock);
        this.gererBonusMalus.doPost(this.httpServletRequestMock, this.httpservletResponseMock);
        this.gererBonusMalus.destroy();
    }
}