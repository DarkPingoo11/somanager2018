package fr.eseo.ld.servlets;

import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.expectLastCall;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.easymock.EasyMockRunner;
import org.easymock.EasyMockSupport;
import org.easymock.Mock;
import org.easymock.MockType;
import org.easymock.TestSubject;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

import fr.eseo.ld.beans.Equipe;
import fr.eseo.ld.beans.Utilisateur;
import fr.eseo.ld.dao.AnneeScolaireDAO;
import fr.eseo.ld.dao.DAOFactory;
import fr.eseo.ld.dao.EquipeDAO;
import fr.eseo.ld.dao.EtudiantDAO;
import fr.eseo.ld.dao.NotificationDAO;
import fr.eseo.ld.dao.OptionESEODAO;
import fr.eseo.ld.dao.SujetDAO;
import fr.eseo.ld.dao.UtilisateurDAO;

/**
 * Classe de tests unitaires JUnit 4 de la classe CreerEquipe.
 *
 * <p>Utilisation de mocks (construits avec EasyMock).</p>
 *
 * @version 1.0
 * @author Maxime LENORMAND
 *
 * @see fr.eseo.ld.servlets.CreerEquipe
 * @see org.junit
 * @see org.easymock
 */
@RunWith(EasyMockRunner.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class CreerEquipeTest extends EasyMockSupport {
	
	private static final String CONF_DAO_FACTORY = "daoFactory";
	
	private static final String ATT_SESSION_USER = "utilisateur";
	private static final Long ID_ETUDIANT_CC1 = 1338L;
	private static final Long ID_ETUDIANT_LD1 = 1339L;
	private static final Long ID_ETUDIANT_OC1 = 1341L;
	private static final Long ID_ETUDIANT_IIT1 = 1342L;
	private static final Long ID_ETUDIANT_BIO1 = 1343L;
	private static final Long ID_SUJET = 764L;
	
	private static final String CHAMP_ID_SUJET = "idSujet";
	private static final String CHAMP_ID_MEMBRE = "idMembre";
	
	private static final String CONTEXT_PATH = "SoManager/CreerEquipe";
	private static final String VUE_DASHBOARD = "/Dashboard";
	private static final String VUE_FORM = "/WEB-INF/formulaires/creerEquipe.jsp";
	
	private static SujetDAO sujetDAO;
	private static OptionESEODAO optionESEODAO;
	private static EquipeDAO equipeDAO;
	private static NotificationDAO notificationDAO;
	private static UtilisateurDAO utilisateurDAO;
	private static EtudiantDAO etudiantDAO;
	private static AnneeScolaireDAO anneeScolaireDAO;

	@TestSubject
	private CreerEquipe creerEquipe = new CreerEquipe();
	
	@Mock
	private ServletConfig servletConfigMock;
	
	@Mock
	private ServletContext servletContextMock;
	
	@Mock(type = MockType.NICE)
	private HttpServletRequest httpServletRequestMock;
	
	@Mock
	private HttpServletResponse httpservletResponseMock;
	
	@Mock(type = MockType.NICE)
	private HttpSession httpSessionMock;
	
	@Mock
	private RequestDispatcher requestDispatcherMock;
	
	@Mock
	private DAOFactory daoFactoryMock;

	/**
	 * Récupère des instances de DAO.
	 * 
	 * @throws Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		/* Instanciations de DAO */
		DAOFactory daoFactory = DAOFactory.getInstance();
		utilisateurDAO = daoFactory.getUtilisateurDao();
		optionESEODAO = daoFactory.getOptionESEODAO();
		sujetDAO = daoFactory.getSujetDao();
		equipeDAO = daoFactory.getEquipeDAO();
		etudiantDAO = daoFactory.getEtudiantDao();
		anneeScolaireDAO = daoFactory.getAnneeScolaireDAO();
		notificationDAO = daoFactory.getNotificationDao();
	}

	/**
	 * Teste la méthode protected void doGet(HttpServletRequest request, HttpServletResponse response).
	 * 
	 * @throws ServletException
	 * @throws IOException
	 */
	@Test
	public void test1DoGet() throws ServletException, IOException {
		/* Récupération d'instances de DAO */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
		expect(this.daoFactoryMock.getUtilisateurDao()).andReturn(utilisateurDAO);
		expect(this.daoFactoryMock.getOptionESEODAO()).andReturn(optionESEODAO);
		expect(this.daoFactoryMock.getSujetDao()).andReturn(sujetDAO);
		expect(this.daoFactoryMock.getEquipeDAO()).andReturn(equipeDAO);
		expect(this.daoFactoryMock.getEtudiantDao()).andReturn(etudiantDAO);
		expect(this.daoFactoryMock.getAnneeScolaireDAO()).andReturn(anneeScolaireDAO);
		expect(this.daoFactoryMock.getNotificationDao()).andReturn(notificationDAO);
		
		/* Récupération de l'Utilisateur en session */
		expect(this.httpServletRequestMock.getSession()).andReturn(this.httpSessionMock);
		final Utilisateur utilisateur = new Utilisateur();
		final Long idUtilisateur = ID_ETUDIANT_LD1;
		utilisateur.setIdUtilisateur(idUtilisateur);
		expect(this.httpSessionMock.getAttribute(ATT_SESSION_USER)).andReturn(utilisateur);
		
		/* Affichage du formulaire */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getRequestDispatcher(VUE_FORM)).andReturn(this.requestDispatcherMock);
		this.requestDispatcherMock.forward(this.httpServletRequestMock, this.httpservletResponseMock);
		expectLastCall();

		this.replayAll();

		/* Cycle de vie d'une servlet */
		this.creerEquipe.init(this.servletConfigMock); 
		this.creerEquipe.doGet(this.httpServletRequestMock, this.httpservletResponseMock);
		this.creerEquipe.destroy();

		this.verifyAll();
	}
	
	/**
	 * Teste la méthode protected void doPost(HttpServletRequest request, HttpServletResponse response).
	 * 
	 * <p>Cas de la création d'une équipe avec erreur taille trop grande.</p>
	 *  
	 * @throws ServletException
	 * @throws IOException
	 */
	@Test
	public void test2DoPostErreurTailleTropGrande() throws ServletException, IOException {
		/* Récupération d'instances de DAO */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
		expect(this.daoFactoryMock.getUtilisateurDao()).andReturn(utilisateurDAO);
		expect(this.daoFactoryMock.getOptionESEODAO()).andReturn(optionESEODAO);
		expect(this.daoFactoryMock.getSujetDao()).andReturn(sujetDAO);
		expect(this.daoFactoryMock.getEquipeDAO()).andReturn(equipeDAO);
		expect(this.daoFactoryMock.getEtudiantDao()).andReturn(etudiantDAO);
		expect(this.daoFactoryMock.getAnneeScolaireDAO()).andReturn(anneeScolaireDAO);
		expect(this.daoFactoryMock.getNotificationDao()).andReturn(notificationDAO);
		
		/* Récupération des données saisies */
		expect(this.httpServletRequestMock.getParameter(CHAMP_ID_SUJET)).andReturn(ID_SUJET.toString());
		expect(this.httpServletRequestMock.getParameter(CHAMP_ID_MEMBRE + 1)).andReturn(ID_ETUDIANT_LD1.toString());
		expectLastCall().times(2);
		expect(this.httpServletRequestMock.getParameter(CHAMP_ID_MEMBRE + 2)).andReturn(ID_ETUDIANT_IIT1.toString());
		expectLastCall().times(2);
		expect(this.httpServletRequestMock.getParameter(CHAMP_ID_MEMBRE + 3)).andReturn(ID_ETUDIANT_BIO1.toString());
		expectLastCall().times(2);
		expect(this.httpServletRequestMock.getParameter(CHAMP_ID_MEMBRE + 4)).andReturn(ID_ETUDIANT_OC1.toString());
		expectLastCall().times(2);

		/* Affichage du formulaire avec les erreurs */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getRequestDispatcher(VUE_FORM)).andReturn(this.requestDispatcherMock);
		this.requestDispatcherMock.forward(this.httpServletRequestMock, this.httpservletResponseMock);
		expectLastCall();

		this.replayAll();

		/* Cycle de vie d'une servlet */
		this.creerEquipe.init(this.servletConfigMock); 
		this.creerEquipe.doPost(this.httpServletRequestMock, this.httpservletResponseMock);
		this.creerEquipe.destroy();

		this.verifyAll();
	}
	
	/**
	 * Teste la méthode protected void doPost(HttpServletRequest request, HttpServletResponse response).
	 * 
	 * <p>Cas de la création d'une équipe avec erreur taille trop petite.</p>
	 *  
	 * @throws ServletException
	 * @throws IOException
	 */
	@Test
	public void test2DoPostErreurTailleTropPetite() throws ServletException, IOException {
		/* Récupération d'instances de DAO */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
		expect(this.daoFactoryMock.getUtilisateurDao()).andReturn(utilisateurDAO);
		expect(this.daoFactoryMock.getOptionESEODAO()).andReturn(optionESEODAO);
		expect(this.daoFactoryMock.getSujetDao()).andReturn(sujetDAO);
		expect(this.daoFactoryMock.getEquipeDAO()).andReturn(equipeDAO);
		expect(this.daoFactoryMock.getEtudiantDao()).andReturn(etudiantDAO);
		expect(this.daoFactoryMock.getAnneeScolaireDAO()).andReturn(anneeScolaireDAO);
		expect(this.daoFactoryMock.getNotificationDao()).andReturn(notificationDAO);
		
		/* Récupération des données saisies */
		expect(this.httpServletRequestMock.getParameter(CHAMP_ID_SUJET)).andReturn(ID_SUJET.toString());
		expect(this.httpServletRequestMock.getParameter(CHAMP_ID_MEMBRE + 1)).andReturn(ID_ETUDIANT_IIT1.toString());
		expectLastCall().times(2);

		/* Affichage du formulaire avec les erreurs */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getRequestDispatcher(VUE_FORM)).andReturn(this.requestDispatcherMock);
		this.requestDispatcherMock.forward(this.httpServletRequestMock, this.httpservletResponseMock);
		expectLastCall();

		this.replayAll();

		/* Cycle de vie d'une servlet */
		this.creerEquipe.init(this.servletConfigMock); 
		this.creerEquipe.doPost(this.httpServletRequestMock, this.httpservletResponseMock);
		this.creerEquipe.destroy();

		this.verifyAll();
	}
	
	/**
	 * Teste la méthode protected void doPost(HttpServletRequest request, HttpServletResponse response).
	 * 
	 * <p>Cas du succès de la création d'une équipe.</p>
	 *  
	 * @throws ServletException
	 * @throws IOException
	 */
	@Test
	public void test3DoPostSucces() throws ServletException, IOException {
		/* Récupération d'instances de DAO */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
		expect(this.daoFactoryMock.getUtilisateurDao()).andReturn(utilisateurDAO);
		expect(this.daoFactoryMock.getOptionESEODAO()).andReturn(optionESEODAO);
		expect(this.daoFactoryMock.getSujetDao()).andReturn(sujetDAO);
		expect(this.daoFactoryMock.getEquipeDAO()).andReturn(equipeDAO);
		expect(this.daoFactoryMock.getEtudiantDao()).andReturn(etudiantDAO);
		expect(this.daoFactoryMock.getAnneeScolaireDAO()).andReturn(anneeScolaireDAO);
		expect(this.daoFactoryMock.getNotificationDao()).andReturn(notificationDAO);
		
		/* Récupération des données saisies */
		expect(this.httpServletRequestMock.getParameter(CHAMP_ID_SUJET)).andReturn(ID_SUJET.toString());
		expect(this.httpServletRequestMock.getParameter(CHAMP_ID_MEMBRE + 1)).andReturn(ID_ETUDIANT_IIT1.toString());
		expectLastCall().times(2);
		expect(this.httpServletRequestMock.getParameter(CHAMP_ID_MEMBRE + 2)).andReturn(ID_ETUDIANT_CC1.toString());
		expectLastCall().times(2);
		
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getRequestDispatcher(VUE_FORM)).andReturn(this.requestDispatcherMock);
		this.requestDispatcherMock.forward(this.httpServletRequestMock, this.httpservletResponseMock);
		expectLastCall();
		
		this.replayAll();

		/* Cycle de vie d'une servlet */
		this.creerEquipe.init(this.servletConfigMock); 
		this.creerEquipe.doPost(this.httpServletRequestMock, this.httpservletResponseMock);
		this.creerEquipe.destroy();

		this.verifyAll();
	}

}