package fr.eseo.ld.servlets;

import fr.eseo.ld.beans.Meeting;
import fr.eseo.ld.beans.Utilisateur;
import fr.eseo.ld.dao.*;
import org.easymock.*;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.expectLastCall;


/**
 * Classe de tests unitaires JUnit 4 de la classe GererMeetings.
 *
 * <p>Utilisation de mocks (construits avec EasyMock).</p>
 *
 * @version 1.0
 * @see fr.eseo.ld.servlets.GererMeetings
 * @see org.junit
 * @see org.easymock
 */
@RunWith(EasyMockRunner.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class GererMeetingsTest extends EasyMockSupport {

    private static final String CONF_DAO_FACTORY = "daoFactory";

    private static final String STRING_TEST = "test";

    private static final String TEST = "testutilisateur";
    private static final String TEST_DATE = "2018/01/01";
    private static final Long ID_UTILISATEUR_TEST = 11L;
    private static final String ID_MEETING = "1";

    public static final String CHAMP_ENDROIT = "lieu";
    public static final String CHAMP_DATE_DEBUT_MEETING = "dateDebut";
    public static final String CHAMP_HEURE_RENDU_MEETING = "heureDebut";
    public static final String CHAMP_COMPTE_RENDU_MEETING = "compteRendu";
    public static final String CHAMP_DATE_HEURE_MEETING = "dateHeure";
    public static final String CHAMP_ID_MEETING = "idMeeting";
    public static final String CHAMP_ID_REUNION = "idReunion";
    public static final String CHAMP_ID_CREATEUR_MEETING = "idCreateurMeeting";


    private static final String ATT_SESSION_USER = "utilisateur";
    private static final String ATT_UTILISATEURS = "listeNomPrenomID";
    private static final String ATT_RESULTAT = "resultat";
    private static final String ATT_ERREURS = "erreurs";
    private static final String ATT_FORMULAIRE = "formulaire";
    private static final String ATT_VALIDERCOMPTE = "valider";
    private static final String ATT_ESTPROF = "false";

    private static final String CHAMP_ID_UTILISATEUR = "idUtilisateur";
    private static final String CHAMP_TITRE = "titre";
    private static final String CHAMP_DESCRIPTION = "description";
    private static final String CHAMP_IDENTIFIANT = "identifiant";
    private static final String CHAMP_MOT_DE_PASSE = "motDePasse";
    private static final String CHAMP_EMAIL = "email";

    private static final String MSG_SUCCESS_COMPTE = "{\"type\":\"success\",\"message\":\"Le compte de " +
            TEST.toUpperCase() + " " + TEST + " a bien été validé\"}";
    private static final String ATT_LISTE_MEETING_A = "listeMeeting_A";
    public static final String ATT_LISTE_UTILISATEURS = "listeUtilisateurs";

    private static final String ATT_UTILISATEUR_RECHERCHE = "utilisateurRecherche";


    private static final String VUE_FORM = "/WEB-INF/formulaires/gererMeetings.jsp";

	private static final String DATE_TEST = "2018-06-30 00:00:00";

    private static MeetingDAO meetingDAO;
    private static InviteReunionDAO inviteReunionDAO;
    private static NotificationDAO notificationDAO;
    private static UtilisateurDAO utilisateurDAO;
    private static Meeting meeting;
    private static Utilisateur utilisateur;

    @TestSubject
    private GererMeetings gererMeetings = new GererMeetings();

    @Mock
    private ServletConfig servletConfigMock;

    @Mock
    private ServletContext servletContextMock;

    @Mock(type = MockType.NICE)
    private HttpServletRequest httpServletRequestMock;

    @Mock
    private HttpServletResponse httpservletResponseMock;

    @Mock
    private HttpSession httpSessionMock;

    @Mock
    private RequestDispatcher requestDispatcherMock;

    @Mock
    private DAOFactory daoFactoryMock;

    @Mock
    private PrintWriter printWriter;
    
    @Mock
    private Utilisateur utilisateurMock;


    /**
     * Instancie des DAO et le bean utilisateur.
     *
     * @throws Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        /* Instanciations de DAO */
        final DAOFactory daoFactory = DAOFactory.getInstance();
        meetingDAO = daoFactory.getMeetingDAO();
        notificationDAO = daoFactory.getNotificationDao();
        utilisateurDAO = daoFactory.getUtilisateurDao();
        inviteReunionDAO = daoFactory.getInviteReunionDAO();


        /* Insertion d'un utilisateur test dans la BDD */
        utilisateur = new Utilisateur();
        utilisateur.setIdentifiant(STRING_TEST);
        utilisateur.setNom(STRING_TEST);
        utilisateur.setPrenom(STRING_TEST);
        utilisateur.setValide("oui");
        utilisateurDAO.creer(utilisateur);
        final Long idUtilisateur = utilisateurDAO.trouver(utilisateur).get(0).getIdUtilisateur();
        utilisateur.setIdUtilisateur(idUtilisateur);

        /* Insertion d'un meeting test dans la BDD */
        meeting = new Meeting();
        meeting.setTitre(STRING_TEST);
        meeting.setLieu(STRING_TEST);
        meeting.setDescription(STRING_TEST);
        meeting.setDate(DATE_TEST);
        meeting.setCompteRendu(STRING_TEST);
        /* attribution du meeting a l'utilisateur créer */
        meeting.setRefUtilisateur(idUtilisateur);
        meetingDAO.creer(meeting);
        final Long idMeeting = meetingDAO.trouver(meeting).get(0).getIdReunion();
        meeting.setIdReunion(idMeeting);


    }

    /**
     * Supprime le meeting inséré dans la BDD.
     *
     * @throws Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        utilisateurDAO.supprimer(utilisateur);
        meetingDAO.supprimer(meeting);
    }

    /**
     * Teste la méthode protected void doGet(HttpServletRequest request, HttpServletResponse response).
     *
     * @throws ServletException
     * @throws IOException
     */
    @Test
    public void test1DoGet() throws ServletException, IOException {
        /* Récupération d'instances de DAO */
        expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
        expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
        expect(this.daoFactoryMock.getUtilisateurDao()).andReturn(utilisateurDAO);
        expect(this.daoFactoryMock.getNotificationDao()).andReturn(notificationDAO);
        expect(this.daoFactoryMock.getMeetingDAO()).andReturn(meetingDAO);
        expect(this.daoFactoryMock.getInviteReunionDAO()).andReturn(inviteReunionDAO);



        /* Mise en place de la liste en session */
        expect(this.httpServletRequestMock.getSession()).andReturn(this.httpSessionMock);
        expect(this.httpServletRequestMock.getSession()).andReturn(this.httpSessionMock);
        /* Récupération de la liste des utilisateurs dans la BDD */
        List<Utilisateur> utilisateurs = utilisateurDAO.lister();

        /* Formatage de la liste des utilisateurs en String */
        StringBuilder liste = new StringBuilder();
        boolean premier = false;
        for (Utilisateur utilisateur : utilisateurs) {
            if (!premier) {
                liste.append(utilisateur.getNom().toUpperCase().replace(" ", "") + " "
                        + utilisateur.getPrenom().replace(" ", "") + " - "
                        + utilisateur.getIdentifiant().replace(" ", ""));
                premier = true;
            } else {
                liste.append("," + utilisateur.getNom().toUpperCase().replace(" ", "") + " "
                        + utilisateur.getPrenom().replace(" ", "") + " - "
                        + utilisateur.getIdentifiant().replace(" ", ""));
            }
        }
        expect(this.httpServletRequestMock.getSession()).andReturn(this.httpSessionMock);
        expect(this.httpSessionMock.getAttribute(ATT_SESSION_USER)).andReturn(utilisateur);
        expect(this.httpSessionMock.getAttribute(ATT_SESSION_USER)).andReturn(utilisateur);
        expect(this.httpSessionMock.getAttribute(ATT_SESSION_USER)).andReturn(utilisateur);

        this.httpSessionMock.setAttribute(ATT_UTILISATEURS, liste.toString());
        //this.httpSessionMock.setAttribute(ATT_LISTE_MEETING_A, liste.toString());
        this.httpSessionMock.setAttribute(ATT_LISTE_UTILISATEURS, liste.toString());

        expectLastCall().asStub();

        /* Affichage du formulaire */
        expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
        expect(this.servletContextMock.getRequestDispatcher(VUE_FORM)).andReturn(this.requestDispatcherMock);
        this.requestDispatcherMock.forward(this.httpServletRequestMock, this.httpservletResponseMock);
        expectLastCall();

        this.replayAll();

        /* Cycle de vie d'une servlet */
        this.gererMeetings.init(this.servletConfigMock);
        this.gererMeetings.doGet(this.httpServletRequestMock, this.httpservletResponseMock);
        this.gererMeetings.destroy();

        this.verifyAll();
    }


    /**
     * Teste la méthode protected void doPost(HttpServletRequest request, HttpServletResponse response).
     *
     * <p>Cas du succès de l'ajout du meeting.</p>
     *
     * @throws ServletException
     * @throws IOException
     */
    @Test
    public void test2_1DoPostAjouterMeetingSucces() throws ServletException, IOException {
        /* Récupération d'une instance de DAO Utilisateur */
        expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
        expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
        expect(this.daoFactoryMock.getUtilisateurDao()).andReturn(utilisateurDAO);
        expect(this.daoFactoryMock.getMeetingDAO()).andReturn(meetingDAO);
        expect(this.daoFactoryMock.getNotificationDao()).andReturn(notificationDAO);
        expect(this.daoFactoryMock.getInviteReunionDAO()).andReturn(inviteReunionDAO);

        /* Récupération des données saisies */
        expect(this.httpServletRequestMock.getParameter(ATT_FORMULAIRE)).andReturn("ajouterMeeting");
        expectLastCall().times(4);

        expect(this.httpServletRequestMock.getParameter(CHAMP_TITRE)).andReturn(TEST);
        expect(this.httpServletRequestMock.getParameter(CHAMP_DESCRIPTION)).andReturn(TEST);
        expect(this.httpServletRequestMock.getParameter(CHAMP_ENDROIT)).andReturn(TEST);
        expect(this.httpServletRequestMock.getParameter(CHAMP_COMPTE_RENDU_MEETING)).andReturn(TEST);
        expect(this.httpServletRequestMock.getParameter(CHAMP_DATE_HEURE_MEETING)).andReturn(TEST);
        expect(this.httpServletRequestMock.getParameter(CHAMP_ID_UTILISATEUR)).andReturn(String.valueOf(ID_UTILISATEUR_TEST));

        expect(this.httpServletRequestMock.getSession()).andReturn(this.httpSessionMock);
        expect(this.httpSessionMock.getAttribute(ATT_SESSION_USER)).andReturn(utilisateur);

        /* Récupération de l'utilisateur en session */
        final Utilisateur utilisateurSession = new Utilisateur();
        final Long idUtilisateurSession = ID_UTILISATEUR_TEST;
        utilisateurSession.setIdUtilisateur(idUtilisateurSession);
        expect(this.httpServletRequestMock.getSession()).andReturn(this.httpSessionMock);
        expect(this.httpSessionMock.getAttribute(ATT_SESSION_USER)).andReturn(utilisateurSession);


        /* Mise en place du resultat dans la requete */
        final String resultat = "Succès de l'inscription.";
        this.httpServletRequestMock.setAttribute(ATT_RESULTAT, resultat);
        expectLastCall();

        /* Ré-affichage du formulaire avec message de succès */
        expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
        expect(this.servletContextMock.getRequestDispatcher(VUE_FORM)).andReturn(this.requestDispatcherMock);
        this.requestDispatcherMock.forward(this.httpServletRequestMock, this.httpservletResponseMock);
        expectLastCall();

        this.replayAll();
    }


    /**
     * Teste la méthode protected void doPost(HttpServletRequest request, HttpServletResponse response).
     *
     * <p>Cas d'erreurs de l'ajout du meeting.</p>
     *
     * @throws ServletException
     * @throws IOException
     */
    @Test
    public void test2_1DoPostAjouterMeetingError() throws ServletException, IOException {
        /* Récupération d'une instance de DAO Utilisateur */
        expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
        expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
        expect(this.daoFactoryMock.getUtilisateurDao()).andReturn(utilisateurDAO);
        expect(this.daoFactoryMock.getMeetingDAO()).andReturn(meetingDAO);
        expect(this.daoFactoryMock.getNotificationDao()).andReturn(notificationDAO);
        expect(this.daoFactoryMock.getInviteReunionDAO()).andReturn(inviteReunionDAO);

        /* Récupération des données saisies */
        expect(this.httpServletRequestMock.getParameter(ATT_FORMULAIRE)).andReturn("ajouterMeeting");
        expectLastCall().times(4);

        /* Récupération de l'utilisateur en session */
        final Utilisateur utilisateurSession = new Utilisateur();
        final Long idUtilisateurSession = ID_UTILISATEUR_TEST;
        utilisateurSession.setIdUtilisateur(idUtilisateurSession);
        expect(this.httpServletRequestMock.getSession()).andReturn(this.httpSessionMock);
        expect(this.httpSessionMock.getAttribute(ATT_SESSION_USER)).andReturn(utilisateurSession);

        expect(this.httpServletRequestMock.getParameter(CHAMP_TITRE)).andReturn(null);
        expect(this.httpServletRequestMock.getParameter(CHAMP_DESCRIPTION)).andReturn(null);
        expect(this.httpServletRequestMock.getParameter(CHAMP_ENDROIT)).andReturn(null);
        expect(this.httpServletRequestMock.getParameter(CHAMP_COMPTE_RENDU_MEETING)).andReturn(null);
        expect(this.httpServletRequestMock.getParameter(CHAMP_DATE_HEURE_MEETING)).andReturn(null);
        expect(this.httpServletRequestMock.getParameter(CHAMP_ID_UTILISATEUR)).andReturn(String.valueOf(utilisateurSession));


        expect(this.httpServletRequestMock.getSession()).andReturn(this.httpSessionMock);
        expect(this.httpSessionMock.getAttribute(ATT_SESSION_USER)).andReturn(utilisateur);

        Map<String, String> erreurs = new HashMap<>();
        erreurs.put("Erreur de meeting","Erreur");
        String resultat = "Échec de l'ajout du meeting, des erreurs sont présentes. ";

        expect(this.httpSessionMock.getAttribute(ATT_RESULTAT)).andReturn(resultat);
        expect(this.httpSessionMock.getAttribute(ATT_ERREURS)).andReturn(erreurs);


        /* Ré-affichage du formulaire avec message de succès */
        expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
        expect(this.servletContextMock.getRequestDispatcher(VUE_FORM)).andReturn(this.requestDispatcherMock);
        this.requestDispatcherMock.forward(this.httpServletRequestMock, this.httpservletResponseMock);
        expectLastCall();


        this.replayAll();
        /* Cycle de vie d'une servlet */
        this.gererMeetings.init(this.servletConfigMock);
        this.gererMeetings.doPost(this.httpServletRequestMock, this.httpservletResponseMock);
        this.gererMeetings.destroy();

    }


    /**
     * Teste la méthode protected void doPost(HttpServletRequest request, HttpServletResponse response).
     *
     * <p>Cas du succès de l'ajout de l'invite</p>
     *
     * @throws ServletException
     * @throws IOException
     */
    @Test
    public void test2_1DoPostAjouterInviteSucces() throws ServletException, IOException {
        /* Récupération d'une instance de DAO Utilisateur */
        expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
        expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
        expect(this.daoFactoryMock.getUtilisateurDao()).andReturn(utilisateurDAO);
        expect(this.daoFactoryMock.getMeetingDAO()).andReturn(meetingDAO);
        expect(this.daoFactoryMock.getNotificationDao()).andReturn(notificationDAO);
        expect(this.daoFactoryMock.getInviteReunionDAO()).andReturn(inviteReunionDAO);

        /* Récupération des données saisies */
        expect(this.httpServletRequestMock.getParameter(ATT_FORMULAIRE)).andReturn("ajouterInvite");
        expectLastCall().times(4);

        expect(this.httpServletRequestMock.getParameter(ATT_UTILISATEUR_RECHERCHE)).andReturn(utilisateur.toString());


        expect(this.httpServletRequestMock.getSession()).andReturn(this.httpSessionMock);
        expect(this.httpSessionMock.getAttribute(ATT_SESSION_USER)).andReturn(utilisateur);

        expect(this.httpServletRequestMock.getParameter("selectRole")).andReturn(meeting.getTitre()+" %/% "+meeting.getIdReunion());


        /* Récupération de l'utilisateur en session */
        final Utilisateur utilisateurSession = new Utilisateur();
        final Long idUtilisateurSession = ID_UTILISATEUR_TEST;
        utilisateurSession.setIdUtilisateur(idUtilisateurSession);
        expect(this.httpServletRequestMock.getSession()).andReturn(this.httpSessionMock);
        expect(this.httpSessionMock.getAttribute(ATT_SESSION_USER)).andReturn(utilisateurSession);


        /* Ré-affichage du formulaire avec message de succès */
        expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
        expect(this.servletContextMock.getRequestDispatcher(VUE_FORM)).andReturn(this.requestDispatcherMock);
        this.requestDispatcherMock.forward(this.httpServletRequestMock, this.httpservletResponseMock);
        expectLastCall();


        this.replayAll();
        /* Cycle de vie d'une servlet */
        this.gererMeetings.init(this.servletConfigMock);
        this.gererMeetings.doPost(this.httpServletRequestMock, this.httpservletResponseMock);
        this.gererMeetings.destroy();
    }

    /**
     * Teste la méthode protected void doPost(HttpServletRequest request, HttpServletResponse response).
     *
     * <p>Cas d'erreur de l'ajout de l'invite</p>
     *
     * @throws ServletException
     * @throws IOException
     */
    @Test
    public void test2_1DoPostAjouterInviteError() throws ServletException, IOException {
        /* Récupération d'une instance de DAO Utilisateur */
        expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
        expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
        expect(this.daoFactoryMock.getUtilisateurDao()).andReturn(utilisateurDAO);
        expect(this.daoFactoryMock.getMeetingDAO()).andReturn(meetingDAO);
        expect(this.daoFactoryMock.getNotificationDao()).andReturn(notificationDAO);
        expect(this.daoFactoryMock.getInviteReunionDAO()).andReturn(inviteReunionDAO);

        /* Récupération des données saisies */
        expect(this.httpServletRequestMock.getParameter(ATT_FORMULAIRE)).andReturn("ajouterInvite");
        expectLastCall().times(4);

        expect(this.httpServletRequestMock.getParameter(ATT_UTILISATEUR_RECHERCHE)).andReturn(null);

        expect(this.httpServletRequestMock.getParameter("selectRole")).andReturn(meeting.getTitre()+" %/% "+meeting.getIdReunion());

        expect(this.httpServletRequestMock.getSession()).andReturn(this.httpSessionMock);
        expect(this.httpSessionMock.getAttribute(ATT_SESSION_USER)).andReturn(utilisateur);


        /* Récupération de l'utilisateur en session */
        final Utilisateur utilisateurSession = new Utilisateur();
        final Long idUtilisateurSession = ID_UTILISATEUR_TEST;
        utilisateurSession.setIdUtilisateur(idUtilisateurSession);
        expect(this.httpServletRequestMock.getSession()).andReturn(this.httpSessionMock);
        expect(this.httpSessionMock.getAttribute(ATT_SESSION_USER)).andReturn(utilisateurSession);


        /* Ré-affichage du formulaire avec message de succès */
        expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
        expect(this.servletContextMock.getRequestDispatcher(VUE_FORM)).andReturn(this.requestDispatcherMock);
        this.requestDispatcherMock.forward(this.httpServletRequestMock, this.httpservletResponseMock);
        expectLastCall();


        this.replayAll();
        /* Cycle de vie d'une servlet */
        this.gererMeetings.init(this.servletConfigMock);
        this.gererMeetings.doPost(this.httpServletRequestMock, this.httpservletResponseMock);
        this.gererMeetings.destroy();
    }


    /**
     * Teste la méthode protected void doPost(HttpServletRequest request, HttpServletResponse response).
     *
     * <p>Cas du succès de la suppression de l'invite</p>
     *
     * @throws ServletException
     * @throws IOException
     */
    @Test
    public void test2_1DoPostSupprimerInviteSucces() throws ServletException, IOException {
        /* Récupération d'une instance de DAO Utilisateur */
        expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
        expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
        expect(this.daoFactoryMock.getUtilisateurDao()).andReturn(utilisateurDAO);
        expect(this.daoFactoryMock.getMeetingDAO()).andReturn(meetingDAO);
        expect(this.daoFactoryMock.getNotificationDao()).andReturn(notificationDAO);
        expect(this.daoFactoryMock.getInviteReunionDAO()).andReturn(inviteReunionDAO);


        /* Récupération des données saisies */
        expect(this.httpServletRequestMock.getParameter(ATT_FORMULAIRE)).andReturn("supprimerInvite");
        expectLastCall().times(4);

        expect(this.httpServletRequestMock.getParameter(ATT_UTILISATEUR_RECHERCHE)).andReturn(utilisateur.toString());


        expect(this.httpServletRequestMock.getParameter("selectRole")).andReturn(meeting.getTitre()+" %/% "+meeting.getIdReunion());

        expect(this.httpServletRequestMock.getSession()).andReturn(this.httpSessionMock);
        expect(this.httpSessionMock.getAttribute(ATT_SESSION_USER)).andReturn(utilisateur);

        /* Récupération de l'utilisateur en session */
        final Utilisateur utilisateurSession = new Utilisateur();
        final Long idUtilisateurSession = ID_UTILISATEUR_TEST;
        utilisateurSession.setIdUtilisateur(idUtilisateurSession);
        expect(this.httpServletRequestMock.getSession()).andReturn(this.httpSessionMock);
        expect(this.httpSessionMock.getAttribute(ATT_SESSION_USER)).andReturn(utilisateurSession);

        /* Ré-affichage du formulaire avec message de succès */
        expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
        expect(this.servletContextMock.getRequestDispatcher(VUE_FORM)).andReturn(this.requestDispatcherMock);
        this.requestDispatcherMock.forward(this.httpServletRequestMock, this.httpservletResponseMock);
        expectLastCall();


        this.replayAll();
        /* Cycle de vie d'une servlet */
        this.gererMeetings.init(this.servletConfigMock);
        this.gererMeetings.doPost(this.httpServletRequestMock, this.httpservletResponseMock);
        this.gererMeetings.destroy();
    }

    /**
     * Teste la méthode protected void doPost(HttpServletRequest request, HttpServletResponse response).
     *
     * <p>Cas d'erreur de la suppression de l'invite</p>
     *
     * @throws ServletException
     * @throws IOException
     */
    @Test
    public void test2_1DoPostSupprimerInviteError() throws ServletException, IOException {
        /* Récupération d'une instance de DAO Utilisateur */
        expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
        expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
        expect(this.daoFactoryMock.getUtilisateurDao()).andReturn(utilisateurDAO);
        expect(this.daoFactoryMock.getMeetingDAO()).andReturn(meetingDAO);
        expect(this.daoFactoryMock.getNotificationDao()).andReturn(notificationDAO);
        expect(this.daoFactoryMock.getInviteReunionDAO()).andReturn(inviteReunionDAO);


        /* Récupération des données saisies */
        expect(this.httpServletRequestMock.getParameter(ATT_FORMULAIRE)).andReturn("supprimerInvite");
        expectLastCall().times(4);

        expect(this.httpServletRequestMock.getParameter(ATT_UTILISATEUR_RECHERCHE)).andReturn(null);

        expect(this.httpServletRequestMock.getParameter("selectRole")).andReturn(meeting.getTitre()+" %/% "+meeting.getIdReunion());

        expect(this.httpServletRequestMock.getSession()).andReturn(this.httpSessionMock);
        expect(this.httpSessionMock.getAttribute(ATT_SESSION_USER)).andReturn(utilisateur);

        /* Récupération de l'utilisateur en session */
        final Utilisateur utilisateurSession = new Utilisateur();
        final Long idUtilisateurSession = ID_UTILISATEUR_TEST;
        utilisateurSession.setIdUtilisateur(idUtilisateurSession);
        expect(this.httpServletRequestMock.getSession()).andReturn(this.httpSessionMock);
        expect(this.httpSessionMock.getAttribute(ATT_SESSION_USER)).andReturn(utilisateurSession);

        /* Ré-affichage du formulaire avec message de succès */
        expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
        expect(this.servletContextMock.getRequestDispatcher(VUE_FORM)).andReturn(this.requestDispatcherMock);
        this.requestDispatcherMock.forward(this.httpServletRequestMock, this.httpservletResponseMock);
        expectLastCall();


        this.replayAll();
        /* Cycle de vie d'une servlet */
        this.gererMeetings.init(this.servletConfigMock);
        this.gererMeetings.doPost(this.httpServletRequestMock, this.httpservletResponseMock);
        this.gererMeetings.destroy();
    }


    /**
     * Teste la méthode protected void doPost(HttpServletRequest request, HttpServletResponse response).
     *
     * <p>Cas du succès de la suppression du meeting.</p>
     *
     * @throws ServletException
     * @throws IOException
     */
    @Test
    public void test2_1DoPostSupprimerMeetingSucces() throws ServletException, IOException {
        /* Récupération d'une instance de DAO Utilisateur */
        expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
        expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
        expect(this.daoFactoryMock.getUtilisateurDao()).andReturn(utilisateurDAO);
        expect(this.daoFactoryMock.getMeetingDAO()).andReturn(meetingDAO);
        expect(this.daoFactoryMock.getNotificationDao()).andReturn(notificationDAO);
        expect(this.daoFactoryMock.getInviteReunionDAO()).andReturn(inviteReunionDAO);


        /* Récupération des données saisies */
        expect(this.httpServletRequestMock.getParameter(ATT_FORMULAIRE)).andReturn("supprimerMeeting");
        expectLastCall().times(4);

        expect(this.httpServletRequestMock.getParameter(CHAMP_ID_REUNION)).andReturn(ID_MEETING);

        /* Récupération de l'utilisateur en session */
        final Utilisateur utilisateurSession = new Utilisateur();
        final Long idUtilisateurSession = ID_UTILISATEUR_TEST;
        utilisateurSession.setIdUtilisateur(idUtilisateurSession);
        expect(this.httpServletRequestMock.getSession()).andReturn(this.httpSessionMock);
        expect(this.httpSessionMock.getAttribute(ATT_SESSION_USER)).andReturn(utilisateurSession);

        /* Ré-affichage du formulaire avec message de succès */
        expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
        expect(this.servletContextMock.getRequestDispatcher(VUE_FORM)).andReturn(this.requestDispatcherMock);
        this.requestDispatcherMock.forward(this.httpServletRequestMock, this.httpservletResponseMock);
        expectLastCall();


        this.replayAll();
        /* Cycle de vie d'une servlet */
        this.gererMeetings.init(this.servletConfigMock);
        this.gererMeetings.doPost(this.httpServletRequestMock, this.httpservletResponseMock);
        this.gererMeetings.destroy();
    }


    /**
     * Teste la méthode protected void doPost(HttpServletRequest request, HttpServletResponse response).
     *
     * <p>Cas d'erreur de la suppression du meeting.</p>
     *
     * @throws ServletException
     * @throws IOException
     */
    @Test
    public void test2_1DoPostSupprimerMeetingError() throws ServletException, IOException {
        /* Récupération d'une instance de DAO Utilisateur */
        expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
        expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
        expect(this.daoFactoryMock.getUtilisateurDao()).andReturn(utilisateurDAO);
        expect(this.daoFactoryMock.getMeetingDAO()).andReturn(meetingDAO);
        expect(this.daoFactoryMock.getNotificationDao()).andReturn(notificationDAO);
        expect(this.daoFactoryMock.getInviteReunionDAO()).andReturn(inviteReunionDAO);


        /* Récupération des données saisies */
        expect(this.httpServletRequestMock.getParameter(ATT_FORMULAIRE)).andReturn("supprimerMeeting");
        expectLastCall().times(4);

        expect(this.httpServletRequestMock.getParameter(CHAMP_ID_MEETING)).andReturn(null);

        expect(this.httpServletRequestMock.getSession()).andReturn(this.httpSessionMock);
        expect(this.httpSessionMock.getAttribute(ATT_SESSION_USER)).andReturn(utilisateur);

        /* Récupération de l'utilisateur en session */
        final Utilisateur utilisateurSession = new Utilisateur();
        final Long idUtilisateurSession = ID_UTILISATEUR_TEST;
        utilisateurSession.setIdUtilisateur(idUtilisateurSession);
        expect(this.httpServletRequestMock.getSession()).andReturn(this.httpSessionMock);
        expect(this.httpSessionMock.getAttribute(ATT_SESSION_USER)).andReturn(utilisateurSession);

        /* Ré-affichage du formulaire avec message de succès */
        expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
        expect(this.servletContextMock.getRequestDispatcher(VUE_FORM)).andReturn(this.requestDispatcherMock);
        this.requestDispatcherMock.forward(this.httpServletRequestMock, this.httpservletResponseMock);
        expectLastCall();


        this.replayAll();
        /* Cycle de vie d'une servlet */
        this.gererMeetings.init(this.servletConfigMock);
        this.gererMeetings.doPost(this.httpServletRequestMock, this.httpservletResponseMock);
        this.gererMeetings.destroy();
    }


    /**
     * Teste la méthode protected void doPost(HttpServletRequest request, HttpServletResponse response).
     *
     * <p>Cas du succès de la validation du compte rendu du meeting.</p>
     *
     * @throws ServletException
     * @throws IOException
     */
    @Test
    public void test2_1DoPostValiderCompteRenduMeetingSucces() throws ServletException, IOException {
        /* Récupération d'une instance de DAO Utilisateur */
        expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
        expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
        expect(this.daoFactoryMock.getUtilisateurDao()).andReturn(utilisateurDAO);
        expect(this.daoFactoryMock.getMeetingDAO()).andReturn(meetingDAO);
        expect(this.daoFactoryMock.getNotificationDao()).andReturn(notificationDAO);
        expect(this.daoFactoryMock.getInviteReunionDAO()).andReturn(inviteReunionDAO);


        /* Récupération des données saisies */
        expect(this.httpServletRequestMock.getParameter(ATT_FORMULAIRE)).andReturn("validerCompteRenduMeeting");
        expectLastCall().times(4);

        expect(this.httpServletRequestMock.getParameter(CHAMP_ID_REUNION)).andReturn(ID_MEETING);

        expect(this.httpServletRequestMock.getSession()).andReturn(this.httpSessionMock);
        expect(this.httpSessionMock.getAttribute(ATT_SESSION_USER)).andReturn(utilisateur);
        /* Récupération de l'utilisateur en session */
        final Utilisateur utilisateurSession = new Utilisateur();
        final Long idUtilisateurSession = ID_UTILISATEUR_TEST;
        utilisateurSession.setIdUtilisateur(idUtilisateurSession);
        expect(this.httpServletRequestMock.getSession()).andReturn(this.httpSessionMock);
        expect(this.httpSessionMock.getAttribute(ATT_SESSION_USER)).andReturn(utilisateurSession);

        expect(this.httpSessionMock.getAttribute("hmapMeeting")).andReturn(true);



        /* Ré-affichage du formulaire avec message de succès */
        expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
        expect(this.servletContextMock.getRequestDispatcher(VUE_FORM)).andReturn(this.requestDispatcherMock);
        this.requestDispatcherMock.forward(this.httpServletRequestMock, this.httpservletResponseMock);
        expectLastCall();


        this.replayAll();
        /* Cycle de vie d'une servlet */
        this.gererMeetings.init(this.servletConfigMock);
        this.gererMeetings.doPost(this.httpServletRequestMock, this.httpservletResponseMock);
        this.gererMeetings.destroy();
    }

    /**
     * Teste la méthode protected void doPost(HttpServletRequest request, HttpServletResponse response).
     *
     * <p>Cas d'erreur de la validation du compte rendu du meeting.</p>
     *
     * @throws ServletException
     * @throws IOException
     */
    @Test
    public void test2_1DoPostValiderCompteRenduMeetingError() throws ServletException, IOException {
        /* Récupération d'une instance de DAO Utilisateur */
        expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
        expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
        expect(this.daoFactoryMock.getUtilisateurDao()).andReturn(utilisateurDAO);
        expect(this.daoFactoryMock.getMeetingDAO()).andReturn(meetingDAO);
        expect(this.daoFactoryMock.getNotificationDao()).andReturn(notificationDAO);
        expect(this.daoFactoryMock.getInviteReunionDAO()).andReturn(inviteReunionDAO);

        /* Récupération des données saisies */
        expect(this.httpServletRequestMock.getParameter(ATT_FORMULAIRE)).andReturn("validerCompteRenduMeeting");
        expectLastCall().times(4);

        expect(this.httpServletRequestMock.getParameter(CHAMP_ID_REUNION)).andReturn(null);

        /* Récupération de l'utilisateur en session */
        final Utilisateur utilisateurSession = new Utilisateur();
        final Long idUtilisateurSession = ID_UTILISATEUR_TEST;
        utilisateurSession.setIdUtilisateur(idUtilisateurSession);
        expect(this.httpServletRequestMock.getSession()).andReturn(this.httpSessionMock);
        expect(this.httpSessionMock.getAttribute(ATT_SESSION_USER)).andReturn(utilisateurSession);

        Map<String, String> erreurs = new HashMap<>();
        erreurs.put("Erreur de meeting","Erreur");
        String resultat = "Échec de la modification du meeting, des erreurs sont présentes. ";

        expect(this.httpSessionMock.getAttribute(ATT_RESULTAT)).andReturn(resultat);
        expect(this.httpSessionMock.getAttribute(ATT_ERREURS)).andReturn(erreurs);



        expect(this.httpServletRequestMock.getSession()).andReturn(this.httpSessionMock);
        expect(this.httpSessionMock.getAttribute(ATT_SESSION_USER)).andReturn(utilisateur);

        /* Ré-affichage du formulaire avec message de succès */
        expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
        expect(this.servletContextMock.getRequestDispatcher(VUE_FORM)).andReturn(this.requestDispatcherMock);
        this.requestDispatcherMock.forward(this.httpServletRequestMock, this.httpservletResponseMock);
        expectLastCall();


        this.replayAll();
        /* Cycle de vie d'une servlet */
        this.gererMeetings.init(this.servletConfigMock);
        this.gererMeetings.doPost(this.httpServletRequestMock, this.httpservletResponseMock);
        this.gererMeetings.destroy();
    }


    /**
     * Teste la méthode protected void doPost(HttpServletRequest request, HttpServletResponse response).
     *
     * <p>Cas du succès de la modification du meeting.</p>
     *
     * @throws ServletException
     * @throws IOException
     */
    @Test
    public void test2_1DoPostModifierMeetingSucces() throws ServletException, IOException {
        /* Récupération d'une instance de DAO Utilisateur */
        expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
        expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
        expect(this.daoFactoryMock.getUtilisateurDao()).andReturn(utilisateurDAO);
        expect(this.daoFactoryMock.getMeetingDAO()).andReturn(meetingDAO);
        expect(this.daoFactoryMock.getNotificationDao()).andReturn(notificationDAO);
        expect(this.daoFactoryMock.getInviteReunionDAO()).andReturn(inviteReunionDAO);

        /* Récupération des données saisies */
        expect(this.httpServletRequestMock.getParameter(ATT_FORMULAIRE)).andReturn("modifierMeeting");
        expectLastCall().times(4);

        expect(this.httpServletRequestMock.getParameter(CHAMP_ID_REUNION)).andReturn(ID_MEETING);
        expect(this.httpServletRequestMock.getParameter(CHAMP_TITRE)).andReturn(TEST);
        expect(this.httpServletRequestMock.getParameter(CHAMP_DESCRIPTION)).andReturn(TEST);
        expect(this.httpServletRequestMock.getParameter(CHAMP_ENDROIT)).andReturn(TEST);
        expect(this.httpServletRequestMock.getParameter(CHAMP_COMPTE_RENDU_MEETING)).andReturn(TEST);
        expect(this.httpServletRequestMock.getParameter(CHAMP_DATE_HEURE_MEETING)).andReturn(TEST);
        expect(this.httpServletRequestMock.getParameter(CHAMP_ID_CREATEUR_MEETING)).andReturn(String.valueOf(ID_UTILISATEUR_TEST));

        expect(this.httpServletRequestMock.getSession()).andReturn(this.httpSessionMock);
        expect(this.httpSessionMock.getAttribute(ATT_SESSION_USER)).andReturn(utilisateur);

        /* Récupération de l'utilisateur en session */
        final Utilisateur utilisateurSession = new Utilisateur();
        final Long idUtilisateurSession = ID_UTILISATEUR_TEST;
        utilisateurSession.setIdUtilisateur(idUtilisateurSession);
        expect(this.httpServletRequestMock.getSession()).andReturn(this.httpSessionMock);
        expect(this.httpSessionMock.getAttribute(ATT_SESSION_USER)).andReturn(utilisateurSession);



        /* Mise en place du resultat dans la requete */
        final String resultat = "Succès de l'inscription.";
        this.httpServletRequestMock.setAttribute(ATT_RESULTAT, resultat);
        expectLastCall();

        /* Ré-affichage du formulaire avec message de succès */
        expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
        expect(this.servletContextMock.getRequestDispatcher(VUE_FORM)).andReturn(this.requestDispatcherMock);
        this.requestDispatcherMock.forward(this.httpServletRequestMock, this.httpservletResponseMock);
        expectLastCall();


        this.replayAll();
        /* Cycle de vie d'une servlet */
        this.gererMeetings.init(this.servletConfigMock);
        this.gererMeetings.doPost(this.httpServletRequestMock, this.httpservletResponseMock);
        this.gererMeetings.destroy();
    }

    /**
     * Teste la méthode protected void doPost(HttpServletRequest request, HttpServletResponse response).
     *
     * <p>Cas du succès de la modification du meeting.</p>
     *
     * @throws ServletException
     * @throws IOException
     */
    @Test
    public void test2_1DoPostModifierMeetingErreur() throws ServletException, IOException {
        /* Récupération d'une instance de DAO Utilisateur */
        expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
        expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
        expect(this.daoFactoryMock.getUtilisateurDao()).andReturn(utilisateurDAO);
        expect(this.daoFactoryMock.getMeetingDAO()).andReturn(meetingDAO);
        expect(this.daoFactoryMock.getNotificationDao()).andReturn(notificationDAO);
        expect(this.daoFactoryMock.getInviteReunionDAO()).andReturn(inviteReunionDAO);

        /* Récupération des données saisies */
        expect(this.httpServletRequestMock.getParameter(ATT_FORMULAIRE)).andReturn("modifierMeeting");
        expectLastCall().times(4);

        expect(this.httpServletRequestMock.getParameter(CHAMP_ID_REUNION)).andReturn(ID_MEETING);
        expect(this.httpServletRequestMock.getParameter(CHAMP_TITRE)).andReturn(null);
        expect(this.httpServletRequestMock.getParameter(CHAMP_DESCRIPTION)).andReturn(null);
        expect(this.httpServletRequestMock.getParameter(CHAMP_ENDROIT)).andReturn(TEST);
        expect(this.httpServletRequestMock.getParameter(CHAMP_COMPTE_RENDU_MEETING)).andReturn(TEST);
        expect(this.httpServletRequestMock.getParameter(CHAMP_DATE_HEURE_MEETING)).andReturn(TEST);
        expect(this.httpServletRequestMock.getParameter(CHAMP_ID_CREATEUR_MEETING)).andReturn(String.valueOf(ID_UTILISATEUR_TEST));

        expect(this.httpServletRequestMock.getSession()).andReturn(this.httpSessionMock);
        expect(this.httpSessionMock.getAttribute(ATT_SESSION_USER)).andReturn(utilisateur);

        /* Récupération de l'utilisateur en session */
        final Utilisateur utilisateurSession = new Utilisateur();
        final Long idUtilisateurSession = ID_UTILISATEUR_TEST;
        utilisateurSession.setIdUtilisateur(idUtilisateurSession);
        expect(this.httpServletRequestMock.getSession()).andReturn(this.httpSessionMock);
        expect(this.httpSessionMock.getAttribute(ATT_SESSION_USER)).andReturn(utilisateurSession);

        Map<String, String> erreurs = new HashMap<>();
        erreurs.put("Erreur de meeting","Erreur");
        String resultat = "Échec de la modification du meeting, des erreurs sont présentes. ";

        expect(this.httpSessionMock.getAttribute(ATT_RESULTAT)).andReturn(resultat);
        expect(this.httpSessionMock.getAttribute(ATT_ERREURS)).andReturn(erreurs);


        /* Ré-affichage du formulaire avec message de succès */
        expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
        expect(this.servletContextMock.getRequestDispatcher(VUE_FORM)).andReturn(this.requestDispatcherMock);
        this.requestDispatcherMock.forward(this.httpServletRequestMock, this.httpservletResponseMock);
        expectLastCall();


        this.replayAll();
        /* Cycle de vie d'une servlet */
        this.gererMeetings.init(this.servletConfigMock);
        this.gererMeetings.doPost(this.httpServletRequestMock, this.httpservletResponseMock);
        this.gererMeetings.destroy();
    }
}
