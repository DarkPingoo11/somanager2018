package fr.eseo.ld.servlets;

import fr.eseo.ld.beans.AnneeScolaire;
import fr.eseo.ld.dao.*;
import fr.eseo.ld.pgl.beans.Sprint;
import org.easymock.*;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.expectLastCall;

/**
 * Classe de tests unitaires JUnit 4 de la classe GereProjetsEtPFETEST.
 *
 * <p>Utilisation de mocks (construits avec EasyMock).</p>
 *
 * @author Dimitri Jarneau
 * @version 1.0
 * @see GestionProjetsEtPFETest
 * @see org.junit
 * @see org.easymock
 */
@RunWith(EasyMockRunner.class)
public class GestionProjetsEtPFETest extends EasyMockSupport {
    private static final String CONF_DAO_FACTORY = "daoFactory";

    private static final String ATT_SESSION_USER = "utilisateur";
    private static final String ATT_OPTION = "optionLD";
    private static final Long ID_UTILISATEUR = 11L;
    private static final Long ID_LD = 1L;

    private static final String CHAMP_ANNEE_DEBUT = "anneeDebut";
    private static final String CHAMP_ANNEE_FIN = "anneeFin";
    private static final String CHAMP_DATE_DEBUT_PROJET = "dateDebutProjet";
    private static final String CHAMP_DATE_MI_AVANCEMENT = "dateMiAvancement";
    private static final String CHAMP_DATE_DEPOT_POSTER = "dateDepotPoster";
    private static final String CHAMP_DATE_SOUTENANCE_FINALE = "dateSoutenanceFinale";
    private static final String CHAMP_SELECT_ANNEE = "selectAnnee";
    private static final String CHAMP_NBR_SPRINT = "nombreSprint";
    private static final String CHAMP_LISTE_SPRINTS = "listeSprints";
    private static final String CHAMP_ID_SPRINT = "idSprintSelect";
    private static final String CHAMP_DATE_DEBUT_SPRINT = "idDateDebut";
    private static final String CHAMP_DATE_FIN_SPRINT = "idDateFin";
    private static final String CHAMP_COEFFICIENT_SPRINT = "idCoefficient";
    private static final String CHAMP_NOMBRE_HEURES_SPRINT = "idNombreHeures";
    private static final String CHAMP_REF_ANNEE_SPRINT = "refIdAnnee";
    private static final String CHAMP_ERREUR_DATE_DEBUT_SPRINT = "dateDebutErreur";
    private static final String CHAMP_ERREUR_DATE_FIN_SPRINT = "dateFinErreur";
    private static final String CHAMP_ERREUR_NBR_HEURES_SPRINT = "nbrHErreur";
    private static final String CHAMP_ERREUR_NBR_COEFFICIENT_SPRINT = "coeffErreur";

    private static final String VUE_FORM = "/WEB-INF/formulaires/gestionProjetsEtPFE.jsp";
    private static final String NON_VALIDE = "Non valide";
    private static final String NON_VIDE = "Ne peut être vide";
    private static final String FORMAT_DATE = "\\d{4}-\\d{2}-\\d{2}";

    private static AnneeScolaire anneeScolaire;
    private static Sprint sprint;
    private static AnneeScolaireDAO anneeScolaireDAO;
    private static OptionESEODAO optionESEODAO;
    private static SprintDAO sprintDAO;
    private static MatiereDAO matiereDAO;


    @TestSubject
    private GestionProjetsEtPFE gestionProjetsEtPFE = new GestionProjetsEtPFE();

    @Mock
    private ServletConfig servletConfigMock;

    @Mock
    private ServletContext servletContextMock;

    @Mock(type = MockType.NICE)
    private HttpServletRequest httpServletRequestMock;

    @Mock
    private HttpServletResponse httpservletResponseMock;

    @Mock
    private HttpSession httpSessionMock;

    @Mock
    private RequestDispatcher requestDispatcherMock;

    @Mock
    private DAOFactory daoFactoryMock;

    /**
     * Récupère des instances de DAO.
     *
     * @throws Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        /* Instanciations de DAO */
        DAOFactory daoFactory = DAOFactory.getInstance();
        anneeScolaireDAO = daoFactory.getAnneeScolaireDAO();
        optionESEODAO = daoFactory.getOptionESEODAO();
        sprintDAO = daoFactory.getSprintDAO();
        matiereDAO = daoFactory.getMatiereDAO();

        /* Instanciation du bean anneeScolaire */
        anneeScolaire = new AnneeScolaire();
        sprint = new Sprint();
    }

    /**
     * Supprime l'année scolaire insérée dans la BDD.
     *
     * @throws Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {

    }

    /**
     * Teste la méthode protected void doGet(HttpServletRequest request, HttpServletResponse response).
     *
     * @throws ServletException
     * @throws IOException
     */
    @Test
    public void testDoGet() throws ServletException, IOException {
        /* Récupération d'instances de DAO */
        expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
        expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
        expect(this.daoFactoryMock.getAnneeScolaireDAO()).andReturn(anneeScolaireDAO);
        expect(this.daoFactoryMock.getOptionESEODAO()).andReturn(optionESEODAO);
        expect(this.daoFactoryMock.getSprintDAO()).andReturn(sprintDAO);
        expect(this.daoFactoryMock.getMatiereDAO()).andReturn(matiereDAO);


        expect(this.httpServletRequestMock.getParameter(CHAMP_LISTE_SPRINTS)).andReturn(null);

        /* A la réception d'une requête GET, simple affichage du formulaire */
        expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
        expect(this.servletContextMock.getRequestDispatcher(VUE_FORM)).andReturn(this.requestDispatcherMock);
        this.requestDispatcherMock.forward(this.httpServletRequestMock, this.httpservletResponseMock);
        expectLastCall();

        this.replayAll();

        /* Cycle de vie d'une servlet */
        this.gestionProjetsEtPFE.init(this.servletConfigMock);
        this.gestionProjetsEtPFE.doGet(this.httpServletRequestMock, this.httpservletResponseMock);
        this.gestionProjetsEtPFE.destroy();

        //this.verifyAll();
    }


    /**
     * Teste la méthode protected void doGet(HttpServletRequest request, HttpServletResponse response).
     * cas de la planification de l'année
     * Cas où les champs entrés en paramètres sont vides
     *
     * @throws ServletException
     * @throws IOException
     */
    @Test
    public void test4DoPostAjouterAnneeScolaire_attributs_vides() throws ServletException, IOException {
        /* Récupération d'instances de DAO */
        expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
        expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
        expect(this.daoFactoryMock.getAnneeScolaireDAO()).andReturn(anneeScolaireDAO);
        expect(this.daoFactoryMock.getOptionESEODAO()).andReturn(optionESEODAO);
        expect(this.daoFactoryMock.getSprintDAO()).andReturn(sprintDAO);
        expect(this.daoFactoryMock.getMatiereDAO()).andReturn(matiereDAO);


        /* Récupération des données saisies */
        expect(this.httpServletRequestMock.getParameter("formulaire")).andReturn("ajouterAnneeScolaire");

        /* Récupération des données saisies */
        expect(this.httpServletRequestMock.getParameter(CHAMP_ANNEE_DEBUT)).andReturn("");
        expect(this.httpServletRequestMock.getParameter(CHAMP_ANNEE_FIN)).andReturn("");
        expect(this.httpServletRequestMock.getParameter(CHAMP_DATE_DEBUT_PROJET)).andReturn("");
        expect(this.httpServletRequestMock.getParameter(CHAMP_DATE_MI_AVANCEMENT)).andReturn("");
        expect(this.httpServletRequestMock.getParameter(CHAMP_DATE_DEPOT_POSTER)).andReturn("");
        expect(this.httpServletRequestMock.getParameter(CHAMP_DATE_SOUTENANCE_FINALE)).andReturn("");
        expect(this.httpServletRequestMock.getParameter("LD")).andReturn(null);
        expect(this.httpServletRequestMock.getParameter("SE")).andReturn(null);
        expect(this.httpServletRequestMock.getParameter("OC")).andReturn(null);
        expect(this.httpServletRequestMock.getParameter("BIO")).andReturn(null);
        expect(this.httpServletRequestMock.getParameter("NRJ")).andReturn(null);
        expect(this.httpServletRequestMock.getParameter("CC")).andReturn(null);
        expect(this.httpServletRequestMock.getParameter("IIT")).andReturn(null);
        expect(this.httpServletRequestMock.getParameter("DSMT")).andReturn(null);
        expect(this.httpServletRequestMock.getParameter("BD")).andReturn(null);
        expect(this.httpServletRequestMock.getParameter("")).andReturn(null);
        
        /* A la réception d'une requête GET, simple affichage du formulaire */
        expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
        expect(this.servletContextMock.getRequestDispatcher(VUE_FORM)).andReturn(this.requestDispatcherMock);
        this.requestDispatcherMock.forward(this.httpServletRequestMock, this.httpservletResponseMock);
        expectLastCall();
        
        this.replayAll();

        /* Cycle de vie d'une servlet */
        this.gestionProjetsEtPFE.init(this.servletConfigMock);
        this.gestionProjetsEtPFE.doPost(this.httpServletRequestMock, this.httpservletResponseMock);
        this.gestionProjetsEtPFE.destroy();

        //this.verifyAll();
    }

    /**
     * Teste la méthode protected void doGet(HttpServletRequest request, HttpServletResponse response).
     * cas de la planification de l'année
     * Cas où les champs entrés en paramètres sont valide mais une incohérence entre l'année de début et de fin
     *
     * @throws ServletException
     * @throws IOException
     */
    @Test
    public void test4DoPostAjouterAnneeScolaire_attributs_valide_pb_debut_fin() throws ServletException, IOException {
        /* Récupération d'instances de DAO */
        expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
        expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
        expect(this.daoFactoryMock.getAnneeScolaireDAO()).andReturn(anneeScolaireDAO);
        expect(this.daoFactoryMock.getOptionESEODAO()).andReturn(optionESEODAO);
        expect(this.daoFactoryMock.getSprintDAO()).andReturn(sprintDAO);
        expect(this.daoFactoryMock.getMatiereDAO()).andReturn(matiereDAO);


        /* Récupération des données saisies */
        expect(this.httpServletRequestMock.getParameter("formulaire")).andReturn("ajouterAnneeScolaire");
        /* Récupération des données saisies */
        expect(this.httpServletRequestMock.getParameter(CHAMP_ANNEE_DEBUT)).andReturn("2018");
        expectLastCall().times(2);
        expect(this.httpServletRequestMock.getParameter(CHAMP_ANNEE_FIN)).andReturn("2018");
        expectLastCall().times(2);
        expect(this.httpServletRequestMock.getParameter(CHAMP_DATE_DEBUT_PROJET)).andReturn("2018-10-10");
        expectLastCall().times(2);
        expect(this.httpServletRequestMock.getParameter(CHAMP_DATE_MI_AVANCEMENT)).andReturn("2018-10-10");
        expectLastCall().times(2);
        expect(this.httpServletRequestMock.getParameter(CHAMP_DATE_DEPOT_POSTER)).andReturn("2018-10-10");
        expectLastCall().times(2);
        expect(this.httpServletRequestMock.getParameter(CHAMP_DATE_SOUTENANCE_FINALE)).andReturn("2018-10-10");
        expectLastCall().times(2);
        expect(this.httpServletRequestMock.getParameter("LD")).andReturn("LD");
        expect(this.httpServletRequestMock.getParameter("SE")).andReturn(null);
        expect(this.httpServletRequestMock.getParameter("OC")).andReturn(null);
        expect(this.httpServletRequestMock.getParameter("BIO")).andReturn(null);
        expect(this.httpServletRequestMock.getParameter("NRJ")).andReturn(null);
        expect(this.httpServletRequestMock.getParameter("CC")).andReturn(null);
        expect(this.httpServletRequestMock.getParameter("IIT")).andReturn(null);
        expect(this.httpServletRequestMock.getParameter("DSMT")).andReturn(null);
        expect(this.httpServletRequestMock.getParameter("BD")).andReturn(null);
        expect(this.httpServletRequestMock.getParameter("")).andReturn(null);
        
        /* A la réception d'une requête GET, simple affichage du formulaire */
        expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
        expect(this.servletContextMock.getRequestDispatcher(VUE_FORM)).andReturn(this.requestDispatcherMock);
        this.requestDispatcherMock.forward(this.httpServletRequestMock, this.httpservletResponseMock);
        expectLastCall();
        
        this.replayAll();

        /* Cycle de vie d'une servlet */
        this.gestionProjetsEtPFE.init(this.servletConfigMock);
        this.gestionProjetsEtPFE.doPost(this.httpServletRequestMock, this.httpservletResponseMock);
        this.gestionProjetsEtPFE.destroy();

        //this.verifyAll();
    }

    /**
     * Teste la méthode protected void doGet(HttpServletRequest request, HttpServletResponse response).
     * cas de la planification de l'année
     * Cas où les champs entrés en paramètres sont valide mais une incohérence avec les dates
     *
     * @throws ServletException
     * @throws IOException
     */
    @Test
    public void test4DoPostAjouterAnneeScolaire_attributs_valide_pb_coherence() throws ServletException, IOException {
        /* Récupération d'instances de DAO */
        expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
        expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
        expect(this.daoFactoryMock.getAnneeScolaireDAO()).andReturn(anneeScolaireDAO);
        expect(this.daoFactoryMock.getOptionESEODAO()).andReturn(optionESEODAO);
        expect(this.daoFactoryMock.getSprintDAO()).andReturn(sprintDAO);
        expect(this.daoFactoryMock.getMatiereDAO()).andReturn(matiereDAO);


        /* Récupération des données saisies */
        expect(this.httpServletRequestMock.getParameter("formulaire")).andReturn("ajouterAnneeScolaire");
        /* Récupération des données saisies */
        expect(this.httpServletRequestMock.getParameter(CHAMP_ANNEE_DEBUT)).andReturn("2017");
        expectLastCall().times(2);
        expect(this.httpServletRequestMock.getParameter(CHAMP_ANNEE_FIN)).andReturn("2018");
        expectLastCall().times(2);
        expect(this.httpServletRequestMock.getParameter(CHAMP_DATE_DEBUT_PROJET)).andReturn("2016-10-10");
        expectLastCall().times(2);
        expect(this.httpServletRequestMock.getParameter(CHAMP_DATE_MI_AVANCEMENT)).andReturn("2016-10-10");
        expectLastCall().times(2);
        expect(this.httpServletRequestMock.getParameter(CHAMP_DATE_DEPOT_POSTER)).andReturn("2016-10-10");
        expectLastCall().times(2);
        expect(this.httpServletRequestMock.getParameter(CHAMP_DATE_SOUTENANCE_FINALE)).andReturn("2016-10-10");
        expectLastCall().times(2);
        expect(this.httpServletRequestMock.getParameter("LD")).andReturn("LD");
        expect(this.httpServletRequestMock.getParameter("SE")).andReturn(null);
        expect(this.httpServletRequestMock.getParameter("OC")).andReturn(null);
        expect(this.httpServletRequestMock.getParameter("BIO")).andReturn(null);
        expect(this.httpServletRequestMock.getParameter("NRJ")).andReturn(null);
        expect(this.httpServletRequestMock.getParameter("CC")).andReturn(null);
        expect(this.httpServletRequestMock.getParameter("IIT")).andReturn(null);
        expect(this.httpServletRequestMock.getParameter("DSMT")).andReturn(null);
        expect(this.httpServletRequestMock.getParameter("BD")).andReturn(null);
        expect(this.httpServletRequestMock.getParameter("")).andReturn(null);
        
        /* A la réception d'une requête GET, simple affichage du formulaire */
        expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
        expect(this.servletContextMock.getRequestDispatcher(VUE_FORM)).andReturn(this.requestDispatcherMock);
        this.requestDispatcherMock.forward(this.httpServletRequestMock, this.httpservletResponseMock);
        expectLastCall();
        
        this.replayAll();

        /* Cycle de vie d'une servlet */
        this.gestionProjetsEtPFE.init(this.servletConfigMock);
        this.gestionProjetsEtPFE.doPost(this.httpServletRequestMock, this.httpservletResponseMock);
        this.gestionProjetsEtPFE.destroy();

        //this.verifyAll();
    }

    /**
     * Teste la méthode protected void doGet(HttpServletRequest request, HttpServletResponse response).
     * <p>
     * Cas où les champs entrés en paramètres sont valide mais une incohérence avec les dates
     *
     * @throws ServletException * @throws IOException
     */
    @Test
    public void test4DoPostAjouterAnneeScolaire_attributs_v_ajout_annee() throws ServletException, IOException {
        /* Récupération d'instances de DAO */
        expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
        expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
        expect(this.daoFactoryMock.getAnneeScolaireDAO()).andReturn(anneeScolaireDAO);
        expect(this.daoFactoryMock.getOptionESEODAO()).andReturn(optionESEODAO);
        expect(this.daoFactoryMock.getSprintDAO()).andReturn(sprintDAO);
        expect(this.daoFactoryMock.getMatiereDAO()).andReturn(matiereDAO);


        /* Récupération des données saisies */
        expect(this.httpServletRequestMock.getParameter("formulaire")).andReturn("ajouterAnneeScolaire");
        /* Récupération des données saisies */
        expect(this.httpServletRequestMock.getParameter(CHAMP_ANNEE_DEBUT)).andReturn("2016");
        expectLastCall().times(2);
        expect(this.httpServletRequestMock.getParameter(CHAMP_ANNEE_FIN)).andReturn("2017");
        expectLastCall().times(2);
        expect(this.httpServletRequestMock.getParameter(CHAMP_DATE_DEBUT_PROJET)).andReturn("2016-09-01");
        expectLastCall().times(2);
        expect(this.httpServletRequestMock.getParameter(CHAMP_DATE_MI_AVANCEMENT)).andReturn("2016-12-01");
        expectLastCall().times(2);
        expect(this.httpServletRequestMock.getParameter(CHAMP_DATE_DEPOT_POSTER)).andReturn("2017-06-21");
        expectLastCall().times(2);
        expect(this.httpServletRequestMock.getParameter(CHAMP_DATE_SOUTENANCE_FINALE)).andReturn("2017-06-25");
        expectLastCall().times(2);
        expect(this.httpServletRequestMock.getParameter("LD")).andReturn("LD");
        expect(this.httpServletRequestMock.getParameter("SE")).andReturn(null);
        expect(this.httpServletRequestMock.getParameter("OC")).andReturn(null);
        expect(this.httpServletRequestMock.getParameter("BIO")).andReturn(null);
        expect(this.httpServletRequestMock.getParameter("NRJ")).andReturn(null);
        expect(this.httpServletRequestMock.getParameter("CC")).andReturn(null);
        expect(this.httpServletRequestMock.getParameter("IIT")).andReturn(null);
        expect(this.httpServletRequestMock.getParameter("DSMT")).andReturn(null);
        expect(this.httpServletRequestMock.getParameter("BD")).andReturn(null);
        expect(this.httpServletRequestMock.getParameter("")).andReturn(null);
        
        /* A la réception d'une requête GET, simple affichage du formulaire */
        expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
        expect(this.servletContextMock.getRequestDispatcher(VUE_FORM)).andReturn(this.requestDispatcherMock);
        this.requestDispatcherMock.forward(this.httpServletRequestMock, this.httpservletResponseMock);
        expectLastCall();

        this.replayAll();

        /* Cycle de vie d'une servlet */
        this.gestionProjetsEtPFE.init(this.servletConfigMock);
        this.gestionProjetsEtPFE.doPost(this.httpServletRequestMock, this.httpservletResponseMock);
        this.gestionProjetsEtPFE.destroy();

        //this.verifyAll();
    }


    /**
     * Teste la méthode protected void doGet(HttpServletRequest request, HttpServletResponse response).
     * Cas de la selection du nbr de sprint
     *
     * @throws ServletException
     * @throws IOException
     */
    @Test
    public void testDoPostSelect_Nbr_sprint() throws ServletException, IOException {
        /* Récupération d'instances de DAO */
        expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
        expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
        expect(this.daoFactoryMock.getAnneeScolaireDAO()).andReturn(anneeScolaireDAO);
        expect(this.daoFactoryMock.getOptionESEODAO()).andReturn(optionESEODAO);
        expect(this.daoFactoryMock.getSprintDAO()).andReturn(sprintDAO);
        expect(this.daoFactoryMock.getMatiereDAO()).andReturn(matiereDAO);


        /* Récupération des données saisies */
        expect(this.httpServletRequestMock.getParameter("formulaire")).andReturn("selectNbrSprint");

        /* Récupération des données saisies */
        expect(this.httpServletRequestMock.getParameter(CHAMP_SELECT_ANNEE)).andReturn("AnneeScolaire [idAnneeScolaire= 1,]");
        expect(this.httpServletRequestMock.getParameter(CHAMP_NBR_SPRINT)).andReturn("3");

        /* A la réception d'une requête GET, simple affichage du formulaire */
        expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
        expect(this.servletContextMock.getRequestDispatcher(VUE_FORM)).andReturn(this.requestDispatcherMock);
        this.requestDispatcherMock.forward(this.httpServletRequestMock, this.httpservletResponseMock);
        expectLastCall();
        
        this.replayAll();

        /* Cycle de vie d'une servlet */
        this.gestionProjetsEtPFE.init(this.servletConfigMock);
        this.gestionProjetsEtPFE.doPost(this.httpServletRequestMock, this.httpservletResponseMock);
        this.gestionProjetsEtPFE.destroy();

        //this.verifyAll();
    }

    /**
     * Teste la méthode protected void doGet(HttpServletRequest request, HttpServletResponse response).
     * Cas de la modification des sprints
     *
     * @throws ServletException
     * @throws IOException
     */
    @Test
    public void testDoPost_Modifier_Sprint() throws ServletException, IOException {
        /* Récupération d'instances de DAO */
        expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
        expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
        expect(this.daoFactoryMock.getAnneeScolaireDAO()).andReturn(anneeScolaireDAO);
        expect(this.daoFactoryMock.getOptionESEODAO()).andReturn(optionESEODAO);
        expect(this.daoFactoryMock.getSprintDAO()).andReturn(sprintDAO);
        expect(this.daoFactoryMock.getMatiereDAO()).andReturn(matiereDAO);


        /* Récupération des données saisies */
        expect(this.httpServletRequestMock.getParameter("formulaire")).andReturn("modifierSprint");

        /* Récupération des données saisies */
        expect(this.httpServletRequestMock.getParameter(CHAMP_ID_SPRINT)).andReturn("2");
        expectLastCall().times(2);
        expect(this.httpServletRequestMock.getParameter(CHAMP_DATE_DEBUT_SPRINT)).andReturn("2016-09-01");
        expectLastCall().times(2);
        expect(this.httpServletRequestMock.getParameter(CHAMP_DATE_FIN_SPRINT)).andReturn("2017-06-25");
        expectLastCall().times(2);
        expect(this.httpServletRequestMock.getParameter(CHAMP_NOMBRE_HEURES_SPRINT)).andReturn("30");
        expectLastCall().times(2);
        expect(this.httpServletRequestMock.getParameter(CHAMP_COEFFICIENT_SPRINT)).andReturn("0.0");
        expectLastCall().times(2);
        expect(this.httpServletRequestMock.getParameter(CHAMP_REF_ANNEE_SPRINT)).andReturn("1");
        expectLastCall().times(2);
        
        /* A la réception d'une requête GET, simple affichage du formulaire */
        expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
        expect(this.servletContextMock.getRequestDispatcher(VUE_FORM)).andReturn(this.requestDispatcherMock);
        this.requestDispatcherMock.forward(this.httpServletRequestMock, this.httpservletResponseMock);
        expectLastCall();

        this.replayAll();

        /* Cycle de vie d'une servlet */
        this.gestionProjetsEtPFE.init(this.servletConfigMock);
        this.gestionProjetsEtPFE.doPost(this.httpServletRequestMock, this.httpservletResponseMock);
        this.gestionProjetsEtPFE.destroy();

        //this.verifyAll();
    }


}
