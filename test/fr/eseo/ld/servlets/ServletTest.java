package fr.eseo.ld.servlets;

import fr.eseo.ld.beans.OptionESEO;
import fr.eseo.ld.beans.Role;
import fr.eseo.ld.beans.Utilisateur;
import fr.eseo.ld.dao.*;
import org.easymock.*;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.expectLastCall;

/**
 * Classe de tests unitaires JUnit 4 de la classe Dashboard.
 *
 * <p>Utilisation de mocks (construits avec EasyMock).</p>
 *
 * @version 1.0
 * @author Tristan LE GACQUE
 *
 * @see Dashboard
 * @see org.junit
 * @see org.easymock
 */
@RunWith(EasyMockRunner.class)
public abstract class ServletTest extends EasyMockSupport {

	@Mock(type = MockType.NICE)
	private HttpServletRequest httpServletRequestMock;
	
	@Mock(type = MockType.NICE)
	private HttpServletResponse httpservletResponseMock;
	
	@Mock
	private HttpSession httpSessionMock;

	@Mock
	private ServletConfig servletConfigMock;

	@Mock
	private ServletContext servletContextMock;
	
	@Mock(type = MockType.NICE)
	private RequestDispatcher requestDispatcherMock;

	@Mock(type = MockType.NICE)
	private DAOFactory daoFactoryMock;


	public HttpServletRequest getRequest() {
		return httpServletRequestMock;
	}

	public HttpServletResponse getResponse() {
		return httpservletResponseMock;
	}

	public HttpSession getHttpSessionMock() {
		return httpSessionMock;
	}

	public void setHttpSessionMock(HttpSession httpSessionMock) {
		this.httpSessionMock = httpSessionMock;
	}

	public RequestDispatcher getRequestDispatcher() {
		return requestDispatcherMock;
	}

	public void setRequestDispatcherMock(RequestDispatcher requestDispatcherMock) {
		this.requestDispatcherMock = requestDispatcherMock;
	}

	public DAOFactory getDaoFactoryMock() {
		return daoFactoryMock;
	}

	public void setDaoFactoryMock(DAOFactory daoFactoryMock) {
		this.daoFactoryMock = daoFactoryMock;
	}

	public ServletConfig getServletConfigMock() {
		return servletConfigMock;
	}

	public ServletContext getServletContextMock() {
		return servletContextMock;
	}

	public abstract void servletLife() throws ServletException, IOException;
}