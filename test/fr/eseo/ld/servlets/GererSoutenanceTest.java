package fr.eseo.ld.servlets;

import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.expectLastCall;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.easymock.EasyMockRunner;
import org.easymock.EasyMockSupport;
import org.easymock.Mock;
import org.easymock.MockType;
import org.easymock.TestSubject;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import fr.eseo.ld.beans.OptionESEO;
import fr.eseo.ld.beans.Role;
import fr.eseo.ld.dao.DAOFactory;
import fr.eseo.ld.dao.EquipeDAO;
import fr.eseo.ld.dao.JuryPosterDAO;
import fr.eseo.ld.dao.JurySoutenanceDAO;
import fr.eseo.ld.dao.OptionESEODAO;
import fr.eseo.ld.dao.PosterDAO;
import fr.eseo.ld.dao.ProfesseurDAO;
import fr.eseo.ld.dao.ProfesseurSujetDAO;
import fr.eseo.ld.dao.SoutenanceDAO;
import fr.eseo.ld.dao.SujetDAO;
import fr.eseo.ld.dao.UtilisateurDAO;

/**
 * Classe de tests unitaires JUnit 4 de la classe GererSoutenance.
 *
 * <p>Utilisation de mocks (construits avec EasyMock).</p>
 *
 * @version 1.0
 * @author Maxime LENORMAND
 *
 * @see fr.eseo.ld.servlets.GererSoutenance
 * @see org.junit
 * @see org.easymock
 */
@RunWith(EasyMockRunner.class)
public class GererSoutenanceTest extends EasyMockSupport {
	
	private static final String CONF_DAO_FACTORY = "daoFactory";
	
	private static final String[] PARAMETRES_LONG = { "3" };
	private static final String[] PARAMETRES_STRING = { "2017_001" };
	private static final String[] PARAMETRES_DATE = { "03-06-2018 14:14" };
	
	private static final String ATT_SESSION_ROLES = "roles";
	private static final String ATT_SESSION_OPTIONS = "options";
	
	private static final String HEADER = "referer";
	private static final String VUE_FORM = "/WEB-INF/formulaires/gererJurysPFE.jsp";
	
	private static SujetDAO sujetDAO;
	private static OptionESEODAO optionESEODAO;
	private static ProfesseurDAO professeurDAO;
	private static JuryPosterDAO juryPosterDAO;
	private static EquipeDAO equipeDAO;

	private static SoutenanceDAO soutenanceDAO;

	private static JurySoutenanceDAO jurySoutenanceDAO;

	private static PosterDAO posterDAO;

	private static UtilisateurDAO utilisateurDAO;

	private static ProfesseurSujetDAO professeurSujetDAO;

	@TestSubject
	private GererJuryPFE gererSoutenance = new GererJuryPFE();
	
	@Mock
	private ServletConfig servletConfigMock;
	
	@Mock
	private ServletContext servletContextMock;
	
	@Mock(type = MockType.NICE)
	private HttpServletRequest httpServletRequestMock;
	
	@Mock(type = MockType.NICE)
	private HttpServletResponse httpservletResponseMock;
	
	@Mock(type = MockType.NICE)
	private HttpSession httpSessionMock;
	
	@Mock
	private RequestDispatcher requestDispatcherMock;
	
	@Mock
	private DAOFactory daoFactoryMock;

	/**
	 * Récupère des instances de DAO.
	 * 
	 * @throws Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		DAOFactory daoFactory = DAOFactory.getInstance();
		sujetDAO = daoFactory.getSujetDao();
		optionESEODAO = daoFactory.getOptionESEODAO();
		professeurDAO = daoFactory.getProfesseurDAO();
		juryPosterDAO = daoFactory.getJuryPosterDAO();
		soutenanceDAO = daoFactory.getSoutenanceDAO();
		jurySoutenanceDAO = daoFactory.getJurySoutenanceDAO();
		equipeDAO = daoFactory.getEquipeDAO();
		posterDAO = daoFactory.getPosterDao();
		utilisateurDAO = daoFactory.getUtilisateurDao();
		professeurSujetDAO = daoFactory.getProfesseurSujetDAO();
	}

	/**
	 * Supprime l'utilisateur inséré dans la BDD.
	 * 
	 * @throws Exception
	 */
	@AfterClass
	public static void tearDownAfterClass()throws Exception {
		
	}

	/**
	 * Teste la méthode protected void doGet(HttpServletRequest request, HttpServletResponse response).
	 * 
	 * @throws ServletException
	 * @throws IOException
	 */
	@Test
	public void testDoGet() throws ServletException, IOException {
		/* Récupération d'instances de DAO */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
		expect(this.daoFactoryMock.getSujetDao()).andReturn(sujetDAO);
		expect(this.daoFactoryMock.getOptionESEODAO()).andReturn(optionESEODAO);
		expect(this.daoFactoryMock.getProfesseurDAO()).andReturn(professeurDAO);
		expect(this.daoFactoryMock.getJuryPosterDAO()).andReturn(juryPosterDAO);
		expect(this.daoFactoryMock.getSoutenanceDAO()).andReturn(soutenanceDAO);
		expect(this.daoFactoryMock.getJurySoutenanceDAO()).andReturn(jurySoutenanceDAO);
		expect(this.daoFactoryMock.getEquipeDAO()).andReturn(equipeDAO);
		expect(this.daoFactoryMock.getPosterDao()).andReturn(posterDAO);
		expect(this.daoFactoryMock.getUtilisateurDao()).andReturn(utilisateurDAO);
		expect(this.daoFactoryMock.getProfesseurSujetDAO()).andReturn(professeurSujetDAO);
		
		/* Récupération de la session depuis la requête */
		expect(this.httpServletRequestMock.getSession()).andReturn(this.httpSessionMock);
		
		/* Récupération d'attributs de l'Utilisateur en session */
		Role role = new Role();
		role.setIdRole(5L);
		role.setNomRole("profResponsable");
		List<Role> roles = new ArrayList<>();
		roles.add(role);
		expect(this.httpSessionMock.getAttribute(ATT_SESSION_ROLES)).andReturn(roles);
		OptionESEO option = new OptionESEO();
		option.setIdOption(1L);
		List<OptionESEO> options = new ArrayList<>();
		options.add(option);
		expect(this.httpSessionMock.getAttribute(ATT_SESSION_OPTIONS)).andReturn(options);
		
		/* Affichage du formulaire */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getRequestDispatcher(VUE_FORM)).andReturn(this.requestDispatcherMock);
		this.requestDispatcherMock.forward(this.httpServletRequestMock, this.httpservletResponseMock);
		expectLastCall();

		this.replayAll();

		/* Cycle de vie d'une servlet */
		this.gererSoutenance.init(this.servletConfigMock);
		this.gererSoutenance.doGet(this.httpServletRequestMock, this.httpservletResponseMock);
		this.gererSoutenance.destroy();

		this.verifyAll();
	}
	
//	/**
//	 * Teste la méthode protected void doPost(HttpServletRequest request, HttpServletResponse response).
//	 * 
//	 * @throws ServletException
//	 * @throws IOException
//	 */
//	@Test
//	public void testDoPostPremierIf() throws ServletException, IOException {
//		/* Récupération d'instances de DAO */
//		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
//		expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
//		expect(this.daoFactoryMock.getSujetDao()).andReturn(sujetDAO);
//		expect(this.daoFactoryMock.getOptionESEODAO()).andReturn(optionESEODAO);
//		expect(this.daoFactoryMock.getProfesseurDAO()).andReturn(professeurDAO);
//		expect(this.daoFactoryMock.getJuryPosterDAO()).andReturn(juryPosterDAO);
//		expect(this.daoFactoryMock.getSoutenanceDAO()).andReturn(soutenanceDAO);
//		expect(this.daoFactoryMock.getJurySoutenanceDAO()).andReturn(jurySoutenanceDAO);
//		expect(this.daoFactoryMock.getEquipeDAO()).andReturn(equipeDAO);
//		expect(this.daoFactoryMock.getPosterDao()).andReturn(posterDAO);
//		expect(this.daoFactoryMock.getUtilisateurDao()).andReturn(utilisateurDAO);
//		expect(this.daoFactoryMock.getProfesseurSujetDAO()).andReturn(professeurSujetDAO);
//		
//		/* Mapping de la soutenance si une seule soutenance est choisie */
//		expect(this.httpServletRequestMock.getParameter("numDivForm")).andReturn("1");
//		
//		/* Récupération des données saisies */
//		expect(this.httpServletRequestMock.getParameter("idSoutenance-" + 1)).andReturn("3");
//		expect(this.httpServletRequestMock.getParameter("idSujet-" + 1)).andReturn("3");
//		expect(this.httpServletRequestMock.getParameter("idEquipe-" + 1)).andReturn("2017_001");
//		expect(this.httpServletRequestMock.getParameter("dateSoutenance-" + 1)).andReturn("03-06-2018 14:14");
//		expect(this.httpServletRequestMock.getParameter("idJury-" + 1)).andReturn("2017_001");
//
//		Map<String, String[]> parametres = new LinkedHashMap<>();
//		parametres.put("idJury-1", PARAMETRES_STRING);
//		parametres.put("idSoutenance-1", PARAMETRES_LONG);
//		parametres.put("idEquipe-1", PARAMETRES_STRING);
//		parametres.put("idSujet-1", PARAMETRES_LONG);
//		parametres.put("dateSoutenance-1", PARAMETRES_DATE);
//		expect(this.httpServletRequestMock.getParameterMap()).andReturn(parametres);
//		
//		/* Redirection vers la page précédente */
//		expect(this.httpServletRequestMock.getHeader(HEADER)).andReturn("SoManager/GererSoutenance");
//		this.httpservletResponseMock.sendRedirect("SoManager/GererSoutenance");
//		expectLastCall();
//
//		this.replayAll();
//
//		/* Cycle de vie d'une servlet */
//		this.gererSoutenance.init(this.servletConfigMock);
//		this.gererSoutenance.doPost(this.httpServletRequestMock, this.httpservletResponseMock);
//		this.gererSoutenance.destroy();
//
//		this.verifyAll();
//	}
//	
//	
//	/**
//	 * Teste la méthode protected void doPost(HttpServletRequest request, HttpServletResponse response).
//	 * 
//	 * @throws ServletException
//	 * @throws IOException
//	 */
//	@Test
//	public void testDoPostApresIf() throws ServletException, IOException {
//		/* Récupération d'instances de DAO */
//		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
//		expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
//		expect(this.daoFactoryMock.getSujetDao()).andReturn(sujetDAO);
//		expect(this.daoFactoryMock.getOptionESEODAO()).andReturn(optionESEODAO);
//		expect(this.daoFactoryMock.getProfesseurDAO()).andReturn(professeurDAO);
//		expect(this.daoFactoryMock.getJuryPosterDAO()).andReturn(juryPosterDAO);
//		expect(this.daoFactoryMock.getSoutenanceDAO()).andReturn(soutenanceDAO);
//		expect(this.daoFactoryMock.getJurySoutenanceDAO()).andReturn(jurySoutenanceDAO);
//		expect(this.daoFactoryMock.getEquipeDAO()).andReturn(equipeDAO);
//		expect(this.daoFactoryMock.getPosterDao()).andReturn(posterDAO);
//		expect(this.daoFactoryMock.getUtilisateurDao()).andReturn(utilisateurDAO);
//		expect(this.daoFactoryMock.getProfesseurSujetDAO()).andReturn(professeurSujetDAO);
//		
//		/* Mapping de la soutenance si une seule soutenance est choisie */
//		expect(this.httpServletRequestMock.getParameter("numDivForm")).andReturn("");
//		expect(this.httpServletRequestMock.getParameter("idSoutenance-" + 1)).andReturn("3");
//		expect(this.httpServletRequestMock.getParameter("idSujet-" + 1)).andReturn("3");
//		expect(this.httpServletRequestMock.getParameter("idEquipe-" + 1)).andReturn("2016_001");
//		expect(this.httpServletRequestMock.getParameter("dateSoutenance-" + 1)).andReturn("03-06-2018 14:14");
//		expect(this.httpServletRequestMock.getParameter("idJury-" + 1)).andReturn("2016_001");
//		expect(this.httpServletRequestMock.getParameter("jury-" + 1)).andReturn("2");
//		Map<String, String[]> parametres = new LinkedHashMap<>();
//		parametres.put("idJury-1", PARAMETRES_STRING);
//		parametres.put("idSoutenance-1", PARAMETRES_LONG);
//		parametres.put("idEquipe-1", PARAMETRES_STRING);
//		parametres.put("idSujet-1", PARAMETRES_LONG);
//		parametres.put("dateSoutenance-1", PARAMETRES_DATE);
//		parametres.put("jury-1", PARAMETRES_DATE);
//		expect(this.httpServletRequestMock.getParameterMap()).andReturn(parametres);
//		
//		this.replayAll();
//
//		/* Cycle de vie d'une servlet */
//		this.gererSoutenance.init(this.servletConfigMock);
//		this.gererSoutenance.doPost(this.httpServletRequestMock, this.httpservletResponseMock);
//		this.gererSoutenance.destroy();
//
//		this.verifyAll();
//	}

}