package fr.eseo.ld.servlets;

import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.expectLastCall;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.easymock.EasyMockRunner;
import org.easymock.EasyMockSupport;
import org.easymock.Mock;
import org.easymock.MockType;
import org.easymock.TestSubject;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

import fr.eseo.ld.beans.Equipe;
import fr.eseo.ld.beans.EtudiantEquipe;
import fr.eseo.ld.beans.Utilisateur;
import fr.eseo.ld.dao.AnneeScolaireDAO;
import fr.eseo.ld.dao.DAOFactory;
import fr.eseo.ld.dao.EquipeDAO;
import fr.eseo.ld.dao.EtudiantDAO;
import fr.eseo.ld.dao.NotificationDAO;
import fr.eseo.ld.dao.OptionESEODAO;
import fr.eseo.ld.dao.RoleDAO;
import fr.eseo.ld.dao.SujetDAO;
import fr.eseo.ld.dao.UtilisateurDAO;

/**
 * Classe de tests unitaires JUnit 4 de la classe GererEquipes.
 *
 * <p>Utilisation de mocks (construits avec EasyMock).</p>
 *
 * @version 1.0
 * @author Maxime LENORMAND
 *
 * @see fr.eseo.ld.servlets.GererEquipes
 * @see org.junit
 * @see org.easymock
 */
@RunWith(EasyMockRunner.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class GererEquipesTest extends EasyMockSupport {

	private static final String CONF_DAO_FACTORY = "daoFactory";
	
	private static final String ATT_SESSION_USER = "utilisateur";
	private static final Long ID_PROF_RESPONSABLE = 16L;
	private static final Long ID_ETUDIANT_LD1 = 18L;
	private static final Long ID_ETUDIANT_LD2 = 1340L;
	private static final Long ID_ETUDIANT_OC1 = 1341L;
	private static final Long ID_ETUDIANT_IIT1 = 1342L;
	private static final Long ID_SUJET = 46L;
	
	private static final String CHAMP_FORMULAIRE = "formulaire";
	private static final String CHAMP_ID_EQUIPE = "idEquipe";
	private static final String CHAMP_TAILLE_EQUIPE = "tailleEquipe";
	private static final String CHAMP_NBR_MIN_ELEVES = "nbrMinEleves";
	private static final String CHAMP_ID_MEMBRE = "idMembre";
	
	private static final String VUE_FORM = "/WEB-INF/formulaires/gererEquipes.jsp";
	private static final String HEADER = "referer";

	private static final String ID_EQUIPE = "2017_002";
	
	private static Equipe equipe;
	private static UtilisateurDAO utilisateurDAO;
	private static OptionESEODAO optionESEODAO;
	private static SujetDAO sujetDAO;
	private static EquipeDAO equipeDAO;
	private static EtudiantDAO etudiantDAO;
	private static RoleDAO roleDAO;
	private static AnneeScolaireDAO anneeScolaireDAO;
	private static NotificationDAO notificationDAO;
	
	@TestSubject
	private GererEquipes gererEquipes = new GererEquipes();
	
	@Mock
	private ServletConfig servletConfigMock;
	
	@Mock
	private ServletContext servletContextMock;
	
	@Mock(type = MockType.NICE)
	private HttpServletRequest httpServletRequestMock;
	
	@Mock
	private HttpServletResponse httpservletResponseMock;
	
	@Mock
	private HttpSession httpSessionMock;
	
	@Mock
	private RequestDispatcher requestDispatcherMock;
	
	@Mock
	private DAOFactory daoFactoryMock;

	/**
	 * Récupère des instances de DAO.
	 * 
	 * @throws Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		/* Instanciations de DAO */
		DAOFactory daoFactory = DAOFactory.getInstance();
		utilisateurDAO = daoFactory.getUtilisateurDao();
		optionESEODAO = daoFactory.getOptionESEODAO();
		sujetDAO = daoFactory.getSujetDao();
		equipeDAO = daoFactory.getEquipeDAO();
		etudiantDAO = daoFactory.getEtudiantDao();
		roleDAO = daoFactory.getRoleDAO();
		anneeScolaireDAO = daoFactory.getAnneeScolaireDAO();
		notificationDAO = daoFactory.getNotificationDao();
	}

	/**
	 * Teste la méthode protected void doGet(HttpServletRequest request, HttpServletResponse response).
	 * 
	 * @throws ServletException
	 * @throws IOException
	 */
	@Test
	public void test1DoGet() throws ServletException, IOException {
		/* Récupération d'instances de DAO */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
		expect(this.daoFactoryMock.getUtilisateurDao()).andReturn(utilisateurDAO);
		expect(this.daoFactoryMock.getOptionESEODAO()).andReturn(optionESEODAO);
		expect(this.daoFactoryMock.getSujetDao()).andReturn(sujetDAO);
		expect(this.daoFactoryMock.getEquipeDAO()).andReturn(equipeDAO);
		expect(this.daoFactoryMock.getEtudiantDao()).andReturn(etudiantDAO);
		expect(this.daoFactoryMock.getRoleDAO()).andReturn(roleDAO);
		expect(this.daoFactoryMock.getAnneeScolaireDAO()).andReturn(anneeScolaireDAO);
		expect(this.daoFactoryMock.getNotificationDao()).andReturn(notificationDAO);
		
		/* Affichage du formulaire */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getRequestDispatcher(VUE_FORM)).andReturn(this.requestDispatcherMock);
		this.requestDispatcherMock.forward(this.httpServletRequestMock, this.httpservletResponseMock);
		expectLastCall();

		this.replayAll();

		/* Cycle de vie d'une servlet */
		this.gererEquipes.init(this.servletConfigMock); 
		this.gererEquipes.doGet(this.httpServletRequestMock, this.httpservletResponseMock);
		this.gererEquipes.destroy();

		this.verifyAll();
	}
	
	/**
	 * Teste la méthode protected void doPost(HttpServletRequest request, HttpServletResponse response).
	 * 
	 * <p>Cas de l'ajout d'un membre avec erreur mauvaises options.</p>
	 *  
	 * @throws ServletException
	 * @throws IOException
	 */
	@Test
	public void test2DoPostAjouterMembreErreurOptions() throws ServletException, IOException {
		/* Récupération d'instances de DAO */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
		expect(this.daoFactoryMock.getUtilisateurDao()).andReturn(utilisateurDAO);
		expect(this.daoFactoryMock.getOptionESEODAO()).andReturn(optionESEODAO);
		expect(this.daoFactoryMock.getSujetDao()).andReturn(sujetDAO);
		expect(this.daoFactoryMock.getEquipeDAO()).andReturn(equipeDAO);
		expect(this.daoFactoryMock.getEtudiantDao()).andReturn(etudiantDAO);
		expect(this.daoFactoryMock.getRoleDAO()).andReturn(roleDAO);
		expect(this.daoFactoryMock.getAnneeScolaireDAO()).andReturn(anneeScolaireDAO);
		expect(this.daoFactoryMock.getNotificationDao()).andReturn(notificationDAO);
		
		/* Récupération de l'Utilisateur en session */
		expect(this.httpServletRequestMock.getSession()).andReturn(this.httpSessionMock);
		final Utilisateur utilisateur = new Utilisateur();
		final Long idUtilisateur = ID_PROF_RESPONSABLE;
		utilisateur.setIdUtilisateur(idUtilisateur);
		expect(this.httpSessionMock.getAttribute(ATT_SESSION_USER)).andReturn(utilisateur);
		
		/* Récupération des données saisies */
		expect(this.httpServletRequestMock.getParameter(CHAMP_FORMULAIRE)).andReturn("ajouterMembre");
		expectLastCall().times(4);
		expect(this.httpServletRequestMock.getParameter(CHAMP_ID_EQUIPE)).andReturn(ID_EQUIPE);
		expect(this.httpServletRequestMock.getParameter(CHAMP_ID_MEMBRE)).andReturn(ID_ETUDIANT_OC1.toString());
		
		/* Redirection vers la page précédente */
		expect(this.httpServletRequestMock.getHeader(HEADER)).andReturn("SoManager/GererEquipes");
		this.httpservletResponseMock.sendRedirect("SoManager/GererEquipes");
		expectLastCall();

		this.replayAll();

		/* Cycle de vie d'une servlet */
		this.gererEquipes.init(this.servletConfigMock); 
		this.gererEquipes.doPost(this.httpServletRequestMock, this.httpservletResponseMock);
		this.gererEquipes.destroy();

		this.verifyAll();
	}
	
	/**
	 * Teste la méthode protected void doPost(HttpServletRequest request, HttpServletResponse response).
	 * 
	 * <p>Cas du succès de l'ajout d'un membre.</p>
	 *  
	 * @throws ServletException
	 * @throws IOException
	 */
	@Test
	public void test3DoPostAjouterMembreSucces() throws ServletException, IOException {
		/* Récupération d'instances de DAO */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
		expect(this.daoFactoryMock.getUtilisateurDao()).andReturn(utilisateurDAO);
		expect(this.daoFactoryMock.getOptionESEODAO()).andReturn(optionESEODAO);
		expect(this.daoFactoryMock.getSujetDao()).andReturn(sujetDAO);
		expect(this.daoFactoryMock.getEquipeDAO()).andReturn(equipeDAO);
		expect(this.daoFactoryMock.getEtudiantDao()).andReturn(etudiantDAO);
		expect(this.daoFactoryMock.getRoleDAO()).andReturn(roleDAO);
		expect(this.daoFactoryMock.getAnneeScolaireDAO()).andReturn(anneeScolaireDAO);
		expect(this.daoFactoryMock.getNotificationDao()).andReturn(notificationDAO);
		
		/* Récupération de l'Utilisateur en session */
		expect(this.httpServletRequestMock.getSession()).andReturn(this.httpSessionMock);
		final Utilisateur utilisateur = new Utilisateur();
		final Long idUtilisateur = ID_PROF_RESPONSABLE;
		utilisateur.setIdUtilisateur(idUtilisateur);
		expect(this.httpSessionMock.getAttribute(ATT_SESSION_USER)).andReturn(utilisateur);
		
		/* Récupération des données saisies */
		expect(this.httpServletRequestMock.getParameter(CHAMP_FORMULAIRE)).andReturn("ajouterMembre");
		expectLastCall().times(4);
		expect(this.httpServletRequestMock.getParameter(CHAMP_ID_EQUIPE)).andReturn(ID_EQUIPE);
		expect(this.httpServletRequestMock.getParameter(CHAMP_ID_MEMBRE)).andReturn(ID_ETUDIANT_LD2.toString());
		
		/* Redirection vers la page précédente */
		expect(this.httpServletRequestMock.getHeader(HEADER)).andReturn("SoManager/GererEquipes");
		this.httpservletResponseMock.sendRedirect("SoManager/GererEquipes");
		expectLastCall();

		this.replayAll();

		/* Cycle de vie d'une servlet */
		this.gererEquipes.init(this.servletConfigMock); 
		this.gererEquipes.doPost(this.httpServletRequestMock, this.httpservletResponseMock);
		this.gererEquipes.destroy();

		this.verifyAll();
	}
	
	/**
	 * Teste la méthode protected void doPost(HttpServletRequest request, HttpServletResponse response).
	 * 
	 * <p>Cas de l'ajout d'un membre avec erreur mauvaise taille.</p>
	 *  
	 * @throws ServletException
	 * @throws IOException
	 */
	@Test
	public void test4DoPostAjouterMembreErreurTaille() throws ServletException, IOException {
		/* Récupération d'instances de DAO */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
		expect(this.daoFactoryMock.getUtilisateurDao()).andReturn(utilisateurDAO);
		expect(this.daoFactoryMock.getOptionESEODAO()).andReturn(optionESEODAO);
		expect(this.daoFactoryMock.getSujetDao()).andReturn(sujetDAO);
		expect(this.daoFactoryMock.getEquipeDAO()).andReturn(equipeDAO);
		expect(this.daoFactoryMock.getEtudiantDao()).andReturn(etudiantDAO);
		expect(this.daoFactoryMock.getRoleDAO()).andReturn(roleDAO);
		expect(this.daoFactoryMock.getAnneeScolaireDAO()).andReturn(anneeScolaireDAO);
		expect(this.daoFactoryMock.getNotificationDao()).andReturn(notificationDAO);
		
		/* Récupération de l'Utilisateur en session */
		expect(this.httpServletRequestMock.getSession()).andReturn(this.httpSessionMock);
		final Utilisateur utilisateur = new Utilisateur();
		final Long idUtilisateur = ID_PROF_RESPONSABLE;
		utilisateur.setIdUtilisateur(idUtilisateur);
		expect(this.httpSessionMock.getAttribute(ATT_SESSION_USER)).andReturn(utilisateur);
		
		/* Récupération des données saisies */
		expect(this.httpServletRequestMock.getParameter(CHAMP_FORMULAIRE)).andReturn("ajouterMembre");
		expectLastCall().times(4);
		expect(this.httpServletRequestMock.getParameter(CHAMP_ID_EQUIPE)).andReturn(ID_EQUIPE);
		expect(this.httpServletRequestMock.getParameter(CHAMP_ID_MEMBRE)).andReturn(ID_ETUDIANT_IIT1.toString());
		
		/* Redirection vers la page précédente */
		expect(this.httpServletRequestMock.getHeader(HEADER)).andReturn("SoManager/GererEquipes");
		this.httpservletResponseMock.sendRedirect("SoManager/GererEquipes");
		expectLastCall();

		this.replayAll();

		/* Cycle de vie d'une servlet */
		this.gererEquipes.init(this.servletConfigMock); 
		this.gererEquipes.doPost(this.httpServletRequestMock, this.httpservletResponseMock);
		this.gererEquipes.destroy();

		this.verifyAll();
	}
	
	/**
	 * Teste la méthode protected void doPost(HttpServletRequest request, HttpServletResponse response).
	 * 
	 * <p>Cas du succès de la suppression d'un membre.</p>
	 *  
	 * @throws ServletException
	 * @throws IOException
	 */
	@Test
	public void test5DoPostSupprimerMembreSucces() throws ServletException, IOException {
		/* Récupération d'instances de DAO */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
		expect(this.daoFactoryMock.getUtilisateurDao()).andReturn(utilisateurDAO);
		expect(this.daoFactoryMock.getOptionESEODAO()).andReturn(optionESEODAO);
		expect(this.daoFactoryMock.getSujetDao()).andReturn(sujetDAO);
		expect(this.daoFactoryMock.getEquipeDAO()).andReturn(equipeDAO);
		expect(this.daoFactoryMock.getEtudiantDao()).andReturn(etudiantDAO);
		expect(this.daoFactoryMock.getRoleDAO()).andReturn(roleDAO);
		expect(this.daoFactoryMock.getAnneeScolaireDAO()).andReturn(anneeScolaireDAO);
		expect(this.daoFactoryMock.getNotificationDao()).andReturn(notificationDAO);
		
		/* Récupération de l'Utilisateur en session */
		expect(this.httpServletRequestMock.getSession()).andReturn(this.httpSessionMock);
		final Utilisateur utilisateur = new Utilisateur();
		final Long idUtilisateur = ID_PROF_RESPONSABLE;
		utilisateur.setIdUtilisateur(idUtilisateur);
		expect(this.httpSessionMock.getAttribute(ATT_SESSION_USER)).andReturn(utilisateur);
		
		/* Récupération des données saisies */
		expect(this.httpServletRequestMock.getParameter(CHAMP_FORMULAIRE)).andReturn("supprimerMembre");
		expectLastCall().times(3);
		expect(this.httpServletRequestMock.getParameter(CHAMP_TAILLE_EQUIPE)).andReturn("2");
		expect(this.httpServletRequestMock.getParameter(CHAMP_NBR_MIN_ELEVES)).andReturn("1");
		expect(this.httpServletRequestMock.getParameter(CHAMP_ID_MEMBRE)).andReturn(ID_ETUDIANT_LD2.toString());
		
		/* Redirection vers la page précédente */
		expect(this.httpServletRequestMock.getHeader(HEADER)).andReturn("SoManager/GererEquipes");
		this.httpservletResponseMock.sendRedirect("SoManager/GererEquipes");
		expectLastCall();

		this.replayAll();

		/* Cycle de vie d'une servlet */
		this.gererEquipes.init(this.servletConfigMock); 
		this.gererEquipes.doPost(this.httpServletRequestMock, this.httpservletResponseMock);
		this.gererEquipes.destroy();

		this.verifyAll();
	}
	
	/**
	 * Teste la méthode protected void doPost(HttpServletRequest request, HttpServletResponse response).
	 * 
	 * <p>Cas de la suppression d'un membre avec erreur mauvaise taille.</p>
	 *  
	 * @throws ServletException
	 * @throws IOException
	 */
	@Test
	public void test6DoPostSupprimerErreurTaille() throws ServletException, IOException {
		/* Récupération d'instances de DAO */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
		expect(this.daoFactoryMock.getUtilisateurDao()).andReturn(utilisateurDAO);
		expect(this.daoFactoryMock.getOptionESEODAO()).andReturn(optionESEODAO);
		expect(this.daoFactoryMock.getSujetDao()).andReturn(sujetDAO);
		expect(this.daoFactoryMock.getEquipeDAO()).andReturn(equipeDAO);
		expect(this.daoFactoryMock.getEtudiantDao()).andReturn(etudiantDAO);
		expect(this.daoFactoryMock.getRoleDAO()).andReturn(roleDAO);
		expect(this.daoFactoryMock.getAnneeScolaireDAO()).andReturn(anneeScolaireDAO);
		expect(this.daoFactoryMock.getNotificationDao()).andReturn(notificationDAO);
		
		/* Récupération de l'Utilisateur en session */
		expect(this.httpServletRequestMock.getSession()).andReturn(this.httpSessionMock);
		final Utilisateur utilisateur = new Utilisateur();
		final Long idUtilisateur = ID_PROF_RESPONSABLE;
		utilisateur.setIdUtilisateur(idUtilisateur);
		expect(this.httpSessionMock.getAttribute(ATT_SESSION_USER)).andReturn(utilisateur);
		
		/* Récupération des données saisies */
		expect(this.httpServletRequestMock.getParameter(CHAMP_FORMULAIRE)).andReturn("supprimerMembre");
		expectLastCall().times(3);
		expect(this.httpServletRequestMock.getParameter(CHAMP_TAILLE_EQUIPE)).andReturn("1");
		expect(this.httpServletRequestMock.getParameter(CHAMP_NBR_MIN_ELEVES)).andReturn("1");
		
		/* Redirection vers la page précédente */
		expect(this.httpServletRequestMock.getHeader(HEADER)).andReturn("SoManager/GererEquipes");
		this.httpservletResponseMock.sendRedirect("SoManager/GererEquipes");
		expectLastCall();

		this.replayAll();

		/* Cycle de vie d'une servlet */
		this.gererEquipes.init(this.servletConfigMock); 
		this.gererEquipes.doPost(this.httpServletRequestMock, this.httpservletResponseMock);
		this.gererEquipes.destroy();

		this.verifyAll();
	}
	
	/**
	 * Teste la méthode protected void doPost(HttpServletRequest request, HttpServletResponse response).
	 * 
	 * <p>Cas de la validation d'une équipe.</p>
	 *  
	 * @throws ServletException
	 * @throws IOException
	 */
	@Test
	public void test7DoPostValidationEquipe() throws ServletException, IOException {
		/* Récupération d'instances de DAO */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
		expect(this.daoFactoryMock.getUtilisateurDao()).andReturn(utilisateurDAO);
		expect(this.daoFactoryMock.getOptionESEODAO()).andReturn(optionESEODAO);
		expect(this.daoFactoryMock.getSujetDao()).andReturn(sujetDAO);
		expect(this.daoFactoryMock.getEquipeDAO()).andReturn(equipeDAO);
		expect(this.daoFactoryMock.getEtudiantDao()).andReturn(etudiantDAO);
		expect(this.daoFactoryMock.getRoleDAO()).andReturn(roleDAO);
		expect(this.daoFactoryMock.getAnneeScolaireDAO()).andReturn(anneeScolaireDAO);
		expect(this.daoFactoryMock.getNotificationDao()).andReturn(notificationDAO);
		
		/* Récupération de l'Utilisateur en session */
		expect(this.httpServletRequestMock.getSession()).andReturn(this.httpSessionMock);
		final Utilisateur utilisateur = new Utilisateur();
		final Long idUtilisateur = ID_PROF_RESPONSABLE;
		utilisateur.setIdUtilisateur(idUtilisateur);
		expect(this.httpSessionMock.getAttribute(ATT_SESSION_USER)).andReturn(utilisateur);
		
		/* Récupération des données saisies */
		expect(this.httpServletRequestMock.getParameter(CHAMP_FORMULAIRE)).andReturn("validerEquipe");
		expect(this.httpServletRequestMock.getParameter(CHAMP_ID_EQUIPE)).andReturn(ID_EQUIPE);
		
		/* Redirection vers la page précédente */
		expect(this.httpServletRequestMock.getHeader(HEADER)).andReturn("SoManager/GererEquipes");
		this.httpservletResponseMock.sendRedirect("SoManager/GererEquipes");
		expectLastCall();

		this.replayAll();

		/* Cycle de vie d'une servlet */
		this.gererEquipes.init(this.servletConfigMock); 
		this.gererEquipes.doPost(this.httpServletRequestMock, this.httpservletResponseMock);
		this.gererEquipes.destroy();

		this.verifyAll();
	}
	
	/**
	 * Teste la méthode protected void doPost(HttpServletRequest request, HttpServletResponse response).
	 * 
	 * <p>Cas du refus d'une équipe.</p>
	 *  
	 * @throws ServletException
	 * @throws IOException
	 */
	@Test
	public void test8DoPostRefuserEquipe() throws ServletException, IOException {
		/* Récupération d'instances de DAO */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
		expect(this.daoFactoryMock.getUtilisateurDao()).andReturn(utilisateurDAO);
		expect(this.daoFactoryMock.getOptionESEODAO()).andReturn(optionESEODAO);
		expect(this.daoFactoryMock.getSujetDao()).andReturn(sujetDAO);
		expect(this.daoFactoryMock.getEquipeDAO()).andReturn(equipeDAO);
		expect(this.daoFactoryMock.getEtudiantDao()).andReturn(etudiantDAO);
		expect(this.daoFactoryMock.getRoleDAO()).andReturn(roleDAO);
		expect(this.daoFactoryMock.getAnneeScolaireDAO()).andReturn(anneeScolaireDAO);
		expect(this.daoFactoryMock.getNotificationDao()).andReturn(notificationDAO);
		
		/* Récupération de l'Utilisateur en session */
		expect(this.httpServletRequestMock.getSession()).andReturn(this.httpSessionMock);
		final Utilisateur utilisateur = new Utilisateur();
		final Long idUtilisateur = ID_PROF_RESPONSABLE;
		utilisateur.setIdUtilisateur(idUtilisateur);
		expect(this.httpSessionMock.getAttribute(ATT_SESSION_USER)).andReturn(utilisateur);
		
		/* Récupération des données saisies */
		expect(this.httpServletRequestMock.getParameter(CHAMP_FORMULAIRE)).andReturn("refuserEquipe");
		expectLastCall().times(2);
		expect(this.httpServletRequestMock.getParameter(CHAMP_ID_EQUIPE)).andReturn(ID_EQUIPE);
		
		/* Redirection vers la page précédente */
		expect(this.httpServletRequestMock.getHeader(HEADER)).andReturn("SoManager/GererEquipes");
		this.httpservletResponseMock.sendRedirect("SoManager/GererEquipes");
		expectLastCall();

		this.replayAll();

		/* Cycle de vie d'une servlet */
		this.gererEquipes.init(this.servletConfigMock); 
		this.gererEquipes.doPost(this.httpServletRequestMock, this.httpservletResponseMock);
		this.gererEquipes.destroy();

		this.verifyAll();
	}

}