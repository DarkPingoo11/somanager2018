package fr.eseo.ld.servlets;

import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.expectLastCall;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.easymock.EasyMockRunner;
import org.easymock.EasyMockSupport;
import org.easymock.Mock;
import org.easymock.MockType;
import org.easymock.TestSubject;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

import fr.eseo.ld.beans.OptionESEO;
import fr.eseo.ld.beans.Role;
import fr.eseo.ld.beans.Utilisateur;
import fr.eseo.ld.dao.DAOFactory;
import fr.eseo.ld.dao.NotificationDAO;
import fr.eseo.ld.dao.OptionESEODAO;
import fr.eseo.ld.dao.PartageRoleDAO;
import fr.eseo.ld.dao.RoleDAO;
import fr.eseo.ld.dao.UtilisateurDAO;

/**
 * Classe de tests unitaires JUnit 4 de la classe PartagerRole.
 *
 * <p>Utilisation de mocks (construits avec EasyMock).</p>
 *
 * @version 1.0
 * @author Maxime LENORMAND
 *
 * @see fr.eseo.ld.servlets.PartagerRole
 * @see org.junit
 * @see org.easymock
 */
@RunWith(EasyMockRunner.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class PartagerRoleTest extends EasyMockSupport {

	private static final String CONF_DAO_FACTORY = "daoFactory";
	
	private static final String ATT_SESSION_USER = "utilisateur";
	private static final Long ID_ETUDIANT = 11L;
	private static final Long ID_PROF_RESPONSABLE = 71L;
	private static final String ATT_SESSION_ROLES = "roles";
	private static final String ATT_SESSION_OPTIONS = "options";
	
	private static final String VUE_FORM = "/WEB-INF/formulaires/partagerRole.jsp";
	
	private static UtilisateurDAO utilisateurDAO;
	private static NotificationDAO notificationDAO;
	private static PartageRoleDAO partagerRoleDAO;
	private static OptionESEODAO optionDAO;
	private static RoleDAO roleDAO;

	@TestSubject
	private PartagerRole partagerRole = new PartagerRole();
	
	@Mock
	private ServletConfig servletConfigMock;
	
	@Mock(type = MockType.NICE)
	private ServletContext servletContextMock;
	
	@Mock(type = MockType.NICE)
	private HttpServletRequest httpServletRequestMock;
	
	@Mock
	private HttpServletResponse httpservletResponseMock;
	
	@Mock
	private HttpSession httpSessionMock;
	
	@Mock(type = MockType.NICE)
	private RequestDispatcher requestDispatcherMock;
	
	@Mock
	private DAOFactory daoFactoryMock;

	/**
	 * Récupère des instances de DAO.
	 * 
	 * @throws Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		DAOFactory daoFactory = DAOFactory.getInstance();
		utilisateurDAO = daoFactory.getUtilisateurDao();
		notificationDAO = daoFactory.getNotificationDao();
		partagerRoleDAO = daoFactory.getPartageDAO();
		optionDAO = daoFactory.getOptionESEODAO();
		roleDAO = daoFactory.getRoleDAO();
	}

	/**
	 * Teste la méthode protected void doGet(HttpServletRequest request, HttpServletResponse response).
	 * 
	 * @throws ServletException
	 * @throws IOException
	 */
	@Test
	public void test1DoGet() throws ServletException, IOException {
		/* Récupération d'instances de DAO */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
		expect(this.daoFactoryMock.getUtilisateurDao()).andReturn(utilisateurDAO);
		expect(this.daoFactoryMock.getNotificationDao()).andReturn(notificationDAO);
		expect(this.daoFactoryMock.getPartageDAO()).andReturn(partagerRoleDAO);
		expect(this.daoFactoryMock.getOptionESEODAO()).andReturn(optionDAO);
		expect(this.daoFactoryMock.getRoleDAO()).andReturn(roleDAO);
		
		/* Récupération de l'Utilisateur en session */
		expect(this.httpServletRequestMock.getSession()).andReturn(this.httpSessionMock);
		expectLastCall().times(3);
		final Utilisateur utilisateur = new Utilisateur();
		final Long idUtilisateur = ID_ETUDIANT;
		utilisateur.setIdUtilisateur(idUtilisateur);
		expect(this.httpSessionMock.getAttribute(ATT_SESSION_USER)).andReturn(utilisateur);

		/* Récupération d'attributs de l'Utilisateur en session */
		Role role = new Role();
		role.setIdRole(5L);
		role.setNomRole("profResponsable");
		List<Role> roles = new ArrayList<>();
		roles.add(role);
		expect(this.httpSessionMock.getAttribute(ATT_SESSION_ROLES)).andReturn(roles);
		OptionESEO option = new OptionESEO();
		option.setIdOption(1L);
		List<OptionESEO> options = new ArrayList<>();
		options.add(option);
		expect(this.httpSessionMock.getAttribute(ATT_SESSION_OPTIONS)).andReturn(options);
		
		/* Affichage du formulaire */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getRequestDispatcher(VUE_FORM)).andReturn(this.requestDispatcherMock);
		this.requestDispatcherMock.forward(this.httpServletRequestMock, this.httpservletResponseMock);
		expectLastCall();

		this.replayAll();

		/* Cycle de vie d'une servlet */
		this.partagerRole.init(this.servletConfigMock);
		this.partagerRole.doGet(this.httpServletRequestMock, this.httpservletResponseMock);
		this.partagerRole.destroy();

		this.verifyAll();
	}
	
	/**
	 * Teste la méthode protected void doGet(HttpServletRequest request, HttpServletResponse response).
	 * cas du partage de son role avec erreur
	 * @throws ServletException
	 * @throws IOException
	 */
	@Test
	public void test2DoPostPartagerErreur() throws ServletException, IOException {
		/* Récupération d'instances de DAO */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
		expect(this.daoFactoryMock.getUtilisateurDao()).andReturn(utilisateurDAO);
		expect(this.daoFactoryMock.getNotificationDao()).andReturn(notificationDAO);
		expect(this.daoFactoryMock.getPartageDAO()).andReturn(partagerRoleDAO);
		expect(this.daoFactoryMock.getOptionESEODAO()).andReturn(optionDAO);
		expect(this.daoFactoryMock.getRoleDAO()).andReturn(roleDAO);
		
		/* Récupération des données saisies */
		expect(this.httpServletRequestMock.getParameter("formulaire")).andReturn("partagerRole");
		expect(this.httpServletRequestMock.getParameter("utilisateurRecherche")).andReturn("prof prof erreur");
		expect(this.httpServletRequestMock.getParameter("dateDebut")).andReturn("2018-10-10");
		expect(this.httpServletRequestMock.getParameter("heureDebut")).andReturn("00:00:00");
		expect(this.httpServletRequestMock.getParameter("dateFin")).andReturn("2018-12-12");
		expect(this.httpServletRequestMock.getParameter("heureFin")).andReturn("00:00:00");

		/* Récupération de l'Utilisateur en session */
		expect(this.httpServletRequestMock.getSession()).andReturn(this.httpSessionMock);
		expectLastCall().times(3);
		final Utilisateur utilisateur = new Utilisateur();
		final Long idUtilisateur = ID_PROF_RESPONSABLE;
		utilisateur.setIdUtilisateur(idUtilisateur);
		expect(this.httpSessionMock.getAttribute(ATT_SESSION_USER)).andReturn(utilisateur);

		/* Récupération d'attributs de l'Utilisateur en session */
		Role role = new Role();
		role.setIdRole(5L);
		role.setNomRole("profResponsable");
		List<Role> roles = new ArrayList<>();
		roles.add(role);
		expect(this.httpSessionMock.getAttribute(ATT_SESSION_ROLES)).andReturn(roles);
		OptionESEO option = new OptionESEO();
		option.setIdOption(1L);
		List<OptionESEO> options = new ArrayList<>();
		options.add(option);
		expect(this.httpSessionMock.getAttribute(ATT_SESSION_OPTIONS)).andReturn(options);
		
		/* Affichage du formulaire */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getRequestDispatcher(VUE_FORM)).andReturn(this.requestDispatcherMock);
		this.requestDispatcherMock.forward(this.httpServletRequestMock, this.httpservletResponseMock);
		expectLastCall();
		
		this.replayAll();

		/* Cycle de vie d'une servlet */
		this.partagerRole.init(this.servletConfigMock);
		this.partagerRole.doPost(this.httpServletRequestMock, this.httpservletResponseMock);
		this.partagerRole.destroy();

		this.verifyAll();
	}
	
	/**
	 * Teste la méthode protected void doGet(HttpServletRequest request, HttpServletResponse response).
	 * cas du partage de son role
	 * @throws ServletException
	 * @throws IOException
	 */
	@Test
	public void test3DoPostPartager() throws ServletException, IOException {
		/* Récupération d'instances de DAO */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expectLastCall().times(2);
		expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
		expect(this.daoFactoryMock.getUtilisateurDao()).andReturn(utilisateurDAO);
		expect(this.daoFactoryMock.getNotificationDao()).andReturn(notificationDAO);
		expect(this.daoFactoryMock.getPartageDAO()).andReturn(partagerRoleDAO);
		expect(this.daoFactoryMock.getOptionESEODAO()).andReturn(optionDAO);
		expect(this.daoFactoryMock.getRoleDAO()).andReturn(roleDAO);
		expect(this.servletContextMock.getRequestDispatcher("/WEB-INF/formulaires/partagerRole.jsp")).andReturn(this.requestDispatcherMock);
		/* Récupération des données saisies */
		expect(this.httpServletRequestMock.getParameter("formulaire")).andReturn("partagerRole");
		expect(this.httpServletRequestMock.getParameter("utilisateurRecherche")).andReturn("PROF PROF - prof");
		expect(this.httpServletRequestMock.getParameter("dateDebut")).andReturn("2018-10-10");
		expect(this.httpServletRequestMock.getParameter("heureDebut")).andReturn("11:10");
		expect(this.httpServletRequestMock.getParameter("dateFin")).andReturn("2018-10-10");
		expect(this.httpServletRequestMock.getParameter("heureFin")).andReturn("11:31");
		expect(this.httpServletRequestMock.getParameter("selectRole")).andReturn("profResponsable test LD");
		/* Récupération de l'Utilisateur en session */
		expect(this.httpServletRequestMock.getSession()).andReturn(this.httpSessionMock);
		expectLastCall().times(4);
		final Utilisateur utilisateur = new Utilisateur();
		final Long idUtilisateur = ID_PROF_RESPONSABLE;
		utilisateur.setIdUtilisateur(idUtilisateur);
		utilisateur.setNom("profResponsable");
		utilisateur.setPrenom("profResponsable");
		expect(this.httpSessionMock.getAttribute(ATT_SESSION_USER)).andReturn(utilisateur);
		expectLastCall().times(2);
		/* Récupération d'attributs de l'Utilisateur en session */
		Role role = new Role();
		role.setIdRole(5L);
		role.setNomRole("profResponsable");
		List<Role> roles = new ArrayList<>();
		roles.add(role);
		expect(this.httpSessionMock.getAttribute(ATT_SESSION_ROLES)).andReturn(roles);
		OptionESEO option = new OptionESEO();
		option.setIdOption(1L);
		List<OptionESEO> options = new ArrayList<>();
		options.add(option);
		expect(this.httpSessionMock.getAttribute(ATT_SESSION_OPTIONS)).andReturn(options);
		this.replayAll();

		/* Cycle de vie d'une servlet */
		this.partagerRole.init(this.servletConfigMock);
		this.partagerRole.doPost(this.httpServletRequestMock, this.httpservletResponseMock);
		this.partagerRole.destroy();

		this.verifyAll();
	}
	
	/**
	 * Teste la méthode protected void doGet(HttpServletRequest request, HttpServletResponse response).
	 * cas de la suppression du partage de son role
	 * @throws ServletException
	 * @throws IOException
	 */
	@Test
	public void test4DoPostSupprimer() throws ServletException, IOException {
		/* Récupération d'instances de DAO */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
		expect(this.daoFactoryMock.getUtilisateurDao()).andReturn(utilisateurDAO);
		expect(this.daoFactoryMock.getNotificationDao()).andReturn(notificationDAO);
		expect(this.daoFactoryMock.getPartageDAO()).andReturn(partagerRoleDAO);
		expect(this.daoFactoryMock.getOptionESEODAO()).andReturn(optionDAO);
		expect(this.daoFactoryMock.getRoleDAO()).andReturn(roleDAO);
		
		/* Récupération des données saisies */
		expect(this.httpServletRequestMock.getParameter("formulaire")).andReturn("supprimerPartage");

		this.replayAll();

		/* Cycle de vie d'une servlet */
		this.partagerRole.init(this.servletConfigMock);
		this.partagerRole.doPost(this.httpServletRequestMock, this.httpservletResponseMock);
		this.partagerRole.destroy();

		this.verifyAll();
	}

}