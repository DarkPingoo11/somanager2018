package fr.eseo.ld.servlets;

import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.expectLastCall;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.easymock.EasyMockRunner;
import org.easymock.EasyMockSupport;
import org.easymock.Mock;
import org.easymock.MockType;
import org.easymock.TestSubject;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import fr.eseo.ld.beans.Utilisateur;
import fr.eseo.ld.dao.DAOFactory;
import fr.eseo.ld.dao.NoteInteretSujetDAO;
import fr.eseo.ld.dao.NoteInteretTechnoDAO;
import fr.eseo.ld.dao.SujetDAO;

/**
 * Classe de tests unitaires JUnit 4 de la classe EvaluerInteret.
 *
 * <p>Utilisation de mocks (construits avec EasyMock).</p>
 *
 * @version 1.0
 * @author Maxime LENORMAND
 *
 * @see fr.eseo.ld.servlets.EvaluerInteret
 * @see org.junit
 * @see org.easymock
 */
@RunWith(EasyMockRunner.class)
public class EvaluerInteretTest extends EasyMockSupport {
	
	private static final String CONF_DAO_FACTORY = "daoFactory";
	
	private static final Long LONG_TEST = 10L;
	
	private static final String CHAMP_ID_SUJET = "idSujet";
	
	private static final String NOM_NOTE = "nameNote";
	private static final String NOM_NOTE_TECHNO = "techno";
	private static final String NOM_NOTE_SUJET = "sujet";
	
	private static SujetDAO sujetDAO;
	private static NoteInteretTechnoDAO noteInteretTechnoDAO;
	private static NoteInteretSujetDAO noteInteretSujetDAO;
	
	@TestSubject
	private EvaluerInteret evaluerInteret = new EvaluerInteret();
	
	@Mock
	private ServletConfig servletConfigMock;
	
	@Mock
	private ServletContext servletContextMock;
	
	@Mock
	private HttpServletRequest httpServletRequestMock;
	
	@Mock
	private HttpServletResponse httpservletResponseMock;
	
	@Mock
	private HttpSession httpSessionMock;
	
	@Mock
	private RequestDispatcher requestDispatcherMock;
	
	@Mock
	private DAOFactory daoFactoryMock;
	
	@Mock(type = MockType.NICE)
	private PrintWriter outMock;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		final DAOFactory daoFactory = DAOFactory.getInstance();
		sujetDAO = daoFactory.getSujetDao();
		noteInteretTechnoDAO = daoFactory.getNoteInteretTechnoDAO();
		noteInteretSujetDAO = daoFactory.getNoteInteretSujetDAO();
	}

	/**
	 * Teste la méthode protected void doGet(HttpServletRequest request, HttpServletResponse response).
	 * 
	 * <p>Cas de la recherche d'une noteInteretTechno.</p>
	 * 
	 * @throws ServletException
	 * @throws IOException
	 */
	@Test
	public void testDoGetTechno() throws ServletException, IOException {
		/* Récupération des paramètres de la requête */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
		expect(this.daoFactoryMock.getSujetDao()).andReturn(sujetDAO);
		expect(this.daoFactoryMock.getNoteInteretTechnoDAO()).andReturn(noteInteretTechnoDAO);
		expect(this.daoFactoryMock.getNoteInteretSujetDAO()).andReturn(noteInteretSujetDAO);

		/* Récupération des données saisies */
		expect(this.httpServletRequestMock.getParameter(CHAMP_ID_SUJET)).andReturn(LONG_TEST.toString());
		expect(this.httpServletRequestMock.getParameter(NOM_NOTE)).andReturn(NOM_NOTE_TECHNO);
		
		/* Ecriture de la réponse en JSON */
		this.httpservletResponseMock.setCharacterEncoding("UTF-8");
		expect(this.httpservletResponseMock.getWriter()).andReturn(this.outMock);
		expectLastCall();

		this.replayAll();

		/* Cycle de vie d'une servlet */
		this.evaluerInteret.init(this.servletConfigMock);
		this.evaluerInteret.doGet(this.httpServletRequestMock, this.httpservletResponseMock);
		this.evaluerInteret.destroy();

		this.verifyAll();
	}
	
	/**
	 * Teste la méthode protected void doGet(HttpServletRequest request, HttpServletResponse response).
	 * 
	 * <p>Cas de la recherche d'une noteInteretSujet.</p>
	 * 
	 * @throws ServletException
	 * @throws IOException
	 */
	@Test
	public void testDoGetSujet() throws ServletException, IOException {
		/* Récupération des paramètres de la requête */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
		expect(this.daoFactoryMock.getSujetDao()).andReturn(sujetDAO);
		expect(this.daoFactoryMock.getNoteInteretTechnoDAO()).andReturn(noteInteretTechnoDAO);
		expect(this.daoFactoryMock.getNoteInteretSujetDAO()).andReturn(noteInteretSujetDAO);

		/* Récupération des données saisies */
		expect(this.httpServletRequestMock.getParameter(CHAMP_ID_SUJET)).andReturn(LONG_TEST.toString());
		expect(this.httpServletRequestMock.getParameter(NOM_NOTE)).andReturn(NOM_NOTE_SUJET);
		
		/* Ecriture de la réponse en JSON */
		this.httpservletResponseMock.setCharacterEncoding("UTF-8");
		expect(this.httpservletResponseMock.getWriter()).andReturn(this.outMock);
		expectLastCall();

		this.replayAll();

		/* Cycle de vie d'une servlet */
		this.evaluerInteret.init(this.servletConfigMock);
		this.evaluerInteret.doGet(this.httpServletRequestMock, this.httpservletResponseMock);
		this.evaluerInteret.destroy();

		this.verifyAll();
	}
	
	/**
	 * Teste la méthode protected void doPost(HttpServletRequest request, HttpServletResponse response).
	 * cas note techno
	 * 
	 * @throws ServletException
	 * @throws IOException
	 */
	@Test
	public void testDoPostTechno() throws ServletException, IOException {
		/* Récupération des paramètres de la requête */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
		expect(this.daoFactoryMock.getSujetDao()).andReturn(sujetDAO);
		expect(this.daoFactoryMock.getNoteInteretTechnoDAO()).andReturn(noteInteretTechnoDAO);
		expect(this.daoFactoryMock.getNoteInteretSujetDAO()).andReturn(noteInteretSujetDAO);
		
		/* Récupération de l'Utilisateur en session */
		expect(this.httpServletRequestMock.getSession()).andReturn(this.httpSessionMock);
		final Utilisateur utilisateur = new Utilisateur();
		final Long idUtilisateur = 71L;
		utilisateur.setIdUtilisateur(idUtilisateur);
		expect(this.httpSessionMock.getAttribute("utilisateur")).andReturn(utilisateur);
		
		/* expect requete */
		expect(this.httpServletRequestMock.getParameter("idSujet")).andReturn("160");
		expect(this.httpServletRequestMock.getParameter("score")).andReturn("4");
		expect(this.httpServletRequestMock.getParameter("nameNote")).andReturn("techno");

		this.replayAll();

		/* Cycle de vie d'une servlet */
		this.evaluerInteret.init(this.servletConfigMock);
		this.evaluerInteret.doPost(this.httpServletRequestMock, this.httpservletResponseMock);
		this.evaluerInteret.destroy();

		this.verifyAll();
	}
	
	/**
	 * Teste la méthode protected void doPost(HttpServletRequest request, HttpServletResponse response).
	 * cas note sujet
	 * 
	 * @throws ServletException
	 * @throws IOException
	 */
	@Test
	public void testDoPostSujet() throws ServletException, IOException {
		/* Récupération des paramètres de la requête */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
		expect(this.daoFactoryMock.getSujetDao()).andReturn(sujetDAO);
		expect(this.daoFactoryMock.getNoteInteretTechnoDAO()).andReturn(noteInteretTechnoDAO);
		expect(this.daoFactoryMock.getNoteInteretSujetDAO()).andReturn(noteInteretSujetDAO);
		
		/* Récupération de l'Utilisateur en session */
		expect(this.httpServletRequestMock.getSession()).andReturn(this.httpSessionMock);
		final Utilisateur utilisateur = new Utilisateur();
		final Long idUtilisateur = 71L;
		utilisateur.setIdUtilisateur(idUtilisateur);
		expect(this.httpSessionMock.getAttribute("utilisateur")).andReturn(utilisateur);
		
		/* expect reuete */
		expect(this.httpServletRequestMock.getParameter("idSujet")).andReturn("160");
		expect(this.httpServletRequestMock.getParameter("score")).andReturn("4");
		expect(this.httpServletRequestMock.getParameter("nameNote")).andReturn("sujet");
		
		this.replayAll();

		/* Cycle de vie d'une servlet */
		this.evaluerInteret.init(this.servletConfigMock);
		this.evaluerInteret.doPost(this.httpServletRequestMock, this.httpservletResponseMock);
		this.evaluerInteret.destroy();

		this.verifyAll();
	}

}