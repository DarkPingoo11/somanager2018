package fr.eseo.ld.servlets;

import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.expectLastCall;
import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.easymock.EasyMockRunner;
import org.easymock.EasyMockSupport;
import org.easymock.Mock;
import org.easymock.TestSubject;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

import fr.eseo.ld.beans.Utilisateur;
import fr.eseo.ld.dao.DAOFactory;
import fr.eseo.ld.dao.UtilisateurDAO;

/**
 * Classe de tests unitaires JUnit 4 de la classe Inscription.
 *
 * <p>Utilisation de mocks (construits avec EasyMock).</p>
 *
 * @version 1.0
 * @author Maxime LENORMAND
 *
 * @see fr.eseo.ld.servlets.Inscription
 * @see org.junit
 * @see org.easymock
 */
@RunWith(EasyMockRunner.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class InscriptionTest extends EasyMockSupport {
	
	private static final String CONF_DAO_FACTORY = "daoFactory";
	
	private static final String TEST = "test";
	private static final String TEST_AUTRE = "test2";
	
	private static final String ATT_RESULTAT = "resultat";
	private static final String ATT_ERREURS = "erreurs";
	
	private static final String CHAMP_NOM = "nom";
	private static final String CHAMP_PRENOM = "prenom";
	private static final String CHAMP_IDENTIFIANT = "identifiant";
	private static final String CHAMP_MOT_DE_PASSE = "motDePasse";
	private static final String CHAMP_EMAIL = "email";
	
	private static final String VUE_FORM = "/WEB-INF/formulaires/inscription.jsp";
	
	private static UtilisateurDAO utilisateurDAO;
	
	@TestSubject
	private Inscription inscription = new Inscription();
	
	@Mock
	private ServletConfig servletConfigMock;
	
	@Mock
	private ServletContext servletContextMock;
	
	@Mock
	private HttpServletRequest httpServletRequestMock;
	
	@Mock
	private HttpServletResponse httpservletResponseMock;
	
	@Mock
	private HttpSession httpSessionMock;
	
	@Mock
	private RequestDispatcher requestDispatcherMock;
	
	@Mock
	private DAOFactory daoFactoryMock;
	
	/**
	 * Récupère une instance de DAO Utilisateur.
	 * 
	 * @throws Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		utilisateurDAO = DAOFactory.getInstance().getUtilisateurDao();
	}
	
	/**
	 * Supprime l'utilisateur inséré dans la BDD.
	 * 
	 * @throws Exception
	 */
	@AfterClass
	public static void tearDownAfterClass()throws Exception {
		final Utilisateur utilisateur = new Utilisateur();
		utilisateur.setIdentifiant(TEST);
		utilisateurDAO.supprimer(utilisateur);
	}

	/**
	 * Teste la méthode protected void doGet(HttpServletRequest request, HttpServletResponse response).
	 * 
	 * @throws ServletException
	 * @throws IOException
	 */
	@Test
	public void test1DoGet() throws ServletException, IOException {
		/* Mock de la méthode init() afin de simplifier le test */
		this.inscription = partialMockBuilder(Inscription.class).addMockedMethod("init").createMock();
		this.inscription.init();
		expectLastCall();

		/* A la réception d'une requête GET, simple affichage du formulaire */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getRequestDispatcher(VUE_FORM)).andReturn(this.requestDispatcherMock);
		this.requestDispatcherMock.forward(this.httpServletRequestMock, this.httpservletResponseMock);
		expectLastCall();

		this.replayAll();

		/* Cycle de vie d'une servlet */
		this.inscription.init(this.servletConfigMock);
		this.inscription.doGet(this.httpServletRequestMock, this.httpservletResponseMock);
		this.inscription.destroy();

		this.verifyAll();
	}
	
	/**
	 * Teste la méthode protected void doPost(HttpServletRequest request, HttpServletResponse response).
	 * 
	 * <p>Cas du succès de l'inscription : affichage du message de succès
	 * et insertion de l'utilisateur dans la BDD.</p>
	 * 
	 * @throws ServletException
	 * @throws IOException
	 */
	@Test
	public void test2DoPostSucces() throws ServletException, IOException {
		/* Récupération d'une instance de DAO Utilisateur */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
		expect(this.daoFactoryMock.getUtilisateurDao()).andReturn(utilisateurDAO);
		
		/* Récupération des données saisies */
		expect(this.httpServletRequestMock.getParameter(CHAMP_NOM)).andReturn(TEST);
		expect(this.httpServletRequestMock.getParameter(CHAMP_PRENOM)).andReturn(TEST);
		expect(this.httpServletRequestMock.getParameter(CHAMP_IDENTIFIANT)).andReturn(TEST);
		expect(this.httpServletRequestMock.getParameter(CHAMP_MOT_DE_PASSE)).andReturn(TEST);
		expect(this.httpServletRequestMock.getParameter(CHAMP_EMAIL)).andReturn(TEST);
		
		/* Mise en place du resultat dans la requete */
		final String resultat = "Succès de l'inscription. Veuillez attendre la validation de votre compte par l'administrateur.";
		this.httpServletRequestMock.setAttribute(ATT_RESULTAT, resultat);
		expectLastCall();

		/* Ré-affichage du formulaire avec message de succès */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getRequestDispatcher(VUE_FORM)).andReturn(this.requestDispatcherMock);
		this.requestDispatcherMock.forward(this.httpServletRequestMock, this.httpservletResponseMock);
		expectLastCall();
		
		this.replayAll();
		
		/* Cycle de vie d'une servlet */
		this.inscription.init(this.servletConfigMock);
		this.inscription.doPost(this.httpServletRequestMock, this.httpservletResponseMock);
		this.inscription.destroy();
		
		/* Vérification de l'insertion de l'utilisateur dans la BDD */
		final Utilisateur utilisateur = new Utilisateur();
		final String identifiantAttendu = TEST;
		utilisateur.setIdentifiant(identifiantAttendu);
		final List<Utilisateur> utilisateursTrouves = utilisateurDAO.trouver(utilisateur);
		String identifiantTrouve = "";
		if (!utilisateursTrouves.isEmpty()) {
			identifiantTrouve = utilisateursTrouves.get(0).getIdentifiant();
		}
		assertEquals("L'utilisateur n'a pas été inséré dans la BDD", identifiantAttendu, identifiantTrouve);
		
		this.verifyAll();
	}
	
	/**
	 * Teste la méthode protected void doPost(HttpServletRequest request, HttpServletResponse response).
	 * 
	 * <p>Cas de l'échec de l'inscription 
	 * (inscription d'un utilisateur possédant le même identifiant que celui d'un utilisateur
	 * déjà inséré dans la BDD) : affichage des erreurs.</p>
	 * 
	 * @throws ServletException
	 * @throws IOException
	 */
	@Test
	public void test3DoPostEchecIdentifiant() throws ServletException, IOException {
		/* Récupération d'une instance de DAO Utilisateur */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
		expect(this.daoFactoryMock.getUtilisateurDao()).andReturn(utilisateurDAO);
		
		/* Récupération des données saisies */
		expect(this.httpServletRequestMock.getParameter(CHAMP_IDENTIFIANT)).andReturn(TEST);
		expect(this.httpServletRequestMock.getParameter(CHAMP_EMAIL)).andReturn(TEST_AUTRE);
		
		/* Mise en place des erreurs dans la requete */
		final Map<String, String> erreurs = new HashMap<>();
		erreurs.put(CHAMP_IDENTIFIANT, "Identifiant déjà utilisé. ");
		this.httpServletRequestMock.setAttribute(ATT_ERREURS, erreurs);
		expectLastCall();
		
		/* Mise en place du resultat dans la requete */
		final String resultat = "Échec de l'inscription. ";
		this.httpServletRequestMock.setAttribute(ATT_RESULTAT, resultat);
		expectLastCall();

		/* Ré-affichage du formulaire avec les erreurs */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getRequestDispatcher(VUE_FORM)).andReturn(this.requestDispatcherMock);
		this.requestDispatcherMock.forward(this.httpServletRequestMock, this.httpservletResponseMock);
		expectLastCall();
		
		this.replayAll();
		
		/* Cycle de vie d'une servlet */
		this.inscription.init(this.servletConfigMock);
		this.inscription.doPost(this.httpServletRequestMock, this.httpservletResponseMock);
		this.inscription.destroy();
		
		/* Vérification que l'utilisateur n'a pas été inséré dans la BDD */
		final Utilisateur utilisateur = new Utilisateur();
		final String identifiant = TEST;
		utilisateur.setIdentifiant(identifiant);
		final int nbUtilisateursAttendus = 1; // un seul : l'utilisateur inséré lors du test2
		final int nbUtilisateursTrouves = utilisateurDAO.trouver(utilisateur).size();
		assertEquals("L'utilisateur a été inséré dans la BDD", nbUtilisateursAttendus, nbUtilisateursTrouves);
		
		this.verifyAll();
	}
	
	/**
	 * Teste la méthode protected void doPost(HttpServletRequest request, HttpServletResponse response).
	 * 
	 * <p>Cas de l'échec de l'inscription 
	 * (inscription d'un utilisateur possédant la même adresse email que celle d'un utilisateur
	 * déjà inséré dans la BDD) : affichage des erreurs.</p>
	 * 
	 * @throws ServletException
	 * @throws IOException
	 */
	@Test
	public void test4DoPostEchecEmail() throws ServletException, IOException {
		/* Récupération d'une instance de DAO Utilisateur */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
		expect(this.daoFactoryMock.getUtilisateurDao()).andReturn(utilisateurDAO);
		
		/* Récupération des données saisies */
		expect(this.httpServletRequestMock.getParameter(CHAMP_IDENTIFIANT)).andReturn(TEST_AUTRE);
		expect(this.httpServletRequestMock.getParameter(CHAMP_EMAIL)).andReturn(TEST);
		
		/* Mise en place des erreurs dans la requete */
		final Map<String, String> erreurs = new HashMap<>();
		erreurs.put(CHAMP_EMAIL, "Email déjà utilisé. ");
		this.httpServletRequestMock.setAttribute(ATT_ERREURS, erreurs);
		expectLastCall();
		
		/* Mise en place du resultat dans la requete */
		final String resultat = "Échec de l'inscription. ";
		this.httpServletRequestMock.setAttribute(ATT_RESULTAT, resultat);
		expectLastCall();

		/* Ré-affichage du formulaire avec les erreurs */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getRequestDispatcher(VUE_FORM)).andReturn(this.requestDispatcherMock);
		this.requestDispatcherMock.forward(this.httpServletRequestMock, this.httpservletResponseMock);
		expectLastCall();
		
		this.replayAll();
		
		/* Cycle de vie d'une servlet */
		this.inscription.init(this.servletConfigMock);
		this.inscription.doPost(this.httpServletRequestMock, this.httpservletResponseMock);
		this.inscription.destroy();
		
		/* Vérification que l'utilisateur n'a pas été inséré dans la BDD */
		final Utilisateur utilisateur = new Utilisateur();
		final String identifiant = TEST;
		utilisateur.setIdentifiant(identifiant);
		final int nbUtilisateursAttendus = 1; // un seul : l'utilisateur inséré lors du test2
		final int nbUtilisateursTrouves = utilisateurDAO.trouver(utilisateur).size();
		assertEquals("L'utilisateur a été inséré dans la BDD", nbUtilisateursAttendus, nbUtilisateursTrouves);
		
		this.verifyAll();
	}

}