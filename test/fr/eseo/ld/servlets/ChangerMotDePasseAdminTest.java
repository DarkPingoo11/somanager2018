package fr.eseo.ld.servlets;

import fr.eseo.ld.beans.Callback;
import fr.eseo.ld.beans.CallbackType;
import fr.eseo.ld.beans.Utilisateur;
import fr.eseo.ld.dao.DAOFactory;
import fr.eseo.ld.dao.NotificationDAO;
import fr.eseo.ld.dao.UtilisateurDAO;
import fr.eseo.ld.utils.CallbackUtilitaire;
import fr.eseo.testing.DAOTesting;
import org.easymock.*;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.mindrot.jbcrypt.BCrypt;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.expectLastCall;

@RunWith(EasyMockRunner.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ChangerMotDePasseAdminTest extends EasyMockSupport{
	private static final String CONF_DAO_FACTORY = "daoFactory";
	
	private static final String NOM_TEST = "test";
	private static final String PRENOM_TEST = "test";
	private static final String IDENTIFIANT_TEST = "test";
	private static final String MOT_DE_PASSE_TEST = "test";
	private static final String EMAIL_TEST = "test";
	private static final String NOUVEAU_MOT_DE_PASSE_TEST = "abcdefgh1";
	
	private static final String CHAMP_IDENTIFIANT = "identifiant";
	private static final String CHAMP_MOT_DE_PASSE = "motDePasse";
	private static final String CHAMP_NOUVEAU_MOT_DE_PASSE = "nouveauMDP2";
	private static final String CHAMP_NOM = "nom";
	private static final String CHAMP_PRENOM = "prenom";
	
	private static final String VUE_FORM = "/WEB-INF/formulaires/gererComptes.jsp";
	private static final String VUE_DASHBOARD = "/WEB-INF/formulaires/dashboard.jsp";
	
	private static Utilisateur utilisateur;
	private static UtilisateurDAO utilisateurDAO;
	private static NotificationDAO notificationDAO;

	@TestSubject
	private ChangerMotDePasseAdmin changerMotDePasse = new ChangerMotDePasseAdmin();
	
	@Mock
	private ServletConfig servletConfigMock;
	
	@Mock
	private ServletContext servletContextMock;
	
	@Mock(type = MockType.NICE)
	private HttpServletRequest httpServletRequestMock;
	
	@Mock
	private HttpServletResponse httpservletResponseMock;
	
	@Mock
	private HttpSession httpSessionMock;
	
	@Mock
	private RequestDispatcher requestDispatcherMock;
	
	@Mock
	private DAOFactory daoFactoryMock;
	
	@Mock
	private CallbackUtilitaire callbackUtilitaireMock;
	
	@Mock
	private Callback callbackMock;

	private static DAOTesting<Utilisateur> daoTesting;

	/**
	 * Récupère des instances de DAO.
	 * 
	 * @throws Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		daoTesting = new DAOTesting<>();
		daoTesting.initDao(daoTesting.getDaoFactory().getUtilisateurDao());
		DAOFactory daoFactory = daoTesting.getDaoFactory();
		notificationDAO = daoFactory.getNotificationDao();
		utilisateurDAO = daoFactory.getUtilisateurDao();
		
		utilisateur = new Utilisateur();
		utilisateur.setIdUtilisateur(1L);
		utilisateur.setNom(NOM_TEST);
		utilisateur.setPrenom(PRENOM_TEST);
		utilisateur.setIdentifiant(IDENTIFIANT_TEST);
		/* Hashage du mot de passe */
		String hash = BCrypt.hashpw(MOT_DE_PASSE_TEST, BCrypt.gensalt());
		utilisateur.setHash(hash);
		utilisateur.setEmail(EMAIL_TEST);
		utilisateur.setValide("oui");

		daoTesting.truncateTable(Utilisateur.class);
		daoTesting.createObjectInDatabase(utilisateur);
	}
	
	/**
	 * Supprime l'utilisateur inséré dans la BDD.
	 * 
	 * @throws Exception
	 */
	@AfterClass
	public static void tearDownAfterClass()throws Exception {
		daoTesting.truncateUsedTables();
	}
	
//	/**
//	 * Teste la méthode protected void doPost(HttpServletRequest request, HttpServletResponse response).
//	 * 
//	 * Test ignoré car il faut appeler une méthode statique qu'on ne peut mocker
//	 * 
//	 * <p>Cas de l'échec du changement de mot de passe : le nouveau mot de passe n'est pas valide.</p>
//	 * 
//	 * @throws ServletException
//	 * @throws IOException
//	 */
//	@Test
//	public void test2DoPostErreurNouveauMDPInvalide() throws ServletException, IOException {
//		/* Récupération d'instances de DAO */
//		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
//		expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
//		expect(this.daoFactoryMock.getNotificationDao()).andReturn(notificationDAO);
//		expect(this.daoFactoryMock.getUtilisateurDao()).andReturn(utilisateurDAO);
//		
//		/* Récupération des données saisies */
//		expect(this.httpServletRequestMock.getParameter(CHAMP_IDENTIFIANT)).andReturn(IDENTIFIANT_TEST);
//		expect(this.httpServletRequestMock.getParameter(CHAMP_MOT_DE_PASSE)).andReturn(MOT_DE_PASSE_TEST);
//		expect(this.httpServletRequestMock.getParameter(CHAMP_NOUVEAU_MOT_DE_PASSE)).andReturn(MOT_DE_PASSE_TEST);
//		
//		Callback callback = new Callback(CallbackType.SUCCESS, "Le mot de passe a bien été changé");
//		
//		this.httpservletResponseMock.setContentType("application/json");
//		expectLastCall();
//		
//		expect(this.httpservletResponseMock.getWriter()).andReturn(new PrintWriter("ff"));
//		
//		/* Cycle de vie d'une servlet */
//		this.changerMotDePasse.init(this.servletConfigMock);
//		this.changerMotDePasse.doPost(this.httpServletRequestMock, this.httpservletResponseMock);
//		this.changerMotDePasse.destroy();
//		
//		this.verifyAll();
//	}
	
	/**
	 * Teste la méthode protected void doPost(HttpServletRequest request, HttpServletResponse response).
	 * 
	 * <p>Cas de l'échec du changement de mot de passe : mauvaise confirmation de mot de passe.</p>
	 * 
	 * @throws ServletException
	 * @throws IOException
	 */
	@Test
	public void test3DoPostErreurConfirmationMDP() throws ServletException, IOException {
		/* Récupération d'instances de DAO */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
		expect(this.daoFactoryMock.getNotificationDao()).andReturn(notificationDAO);
		expect(this.daoFactoryMock.getUtilisateurDao()).andReturn(utilisateurDAO);
		
		/* Récupération des données saisies */
		expect(this.httpServletRequestMock.getParameter(CHAMP_IDENTIFIANT)).andReturn(IDENTIFIANT_TEST);
		expect(this.httpServletRequestMock.getParameter(CHAMP_MOT_DE_PASSE)).andReturn(NOUVEAU_MOT_DE_PASSE_TEST);
		expect(this.httpServletRequestMock.getParameter(CHAMP_NOUVEAU_MOT_DE_PASSE)).andReturn(MOT_DE_PASSE_TEST);
		
		Callback callback = new Callback(CallbackType.SUCCESS, "Erreur de changement de mot de passe : utilisateur");
		
		this.httpservletResponseMock.setContentType("application/json");
		expectLastCall();
		
		expect(this.httpservletResponseMock.getWriter()).andReturn(new PrintWriter("testf"));
		
		this.replayAll();
		
		/* Cycle de vie d'une servlet */
		this.changerMotDePasse.init(this.servletConfigMock);
		this.changerMotDePasse.doPost(this.httpServletRequestMock, this.httpservletResponseMock);
		this.changerMotDePasse.destroy();
		
		this.verifyAll();
	}
	
	/**
	 * Teste la méthode protected void doPost(HttpServletRequest request, HttpServletResponse response).
	 * 
	 * <p>Cas du succès de changement de mot de passe.</p>
	 * 
	 * @throws ServletException
	 * @throws IOException
	 */
	@Test
	public void test4DoPostSucces() throws ServletException, IOException {
		/* Récupération d'instances de DAO */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
		expect(this.daoFactoryMock.getNotificationDao()).andReturn(notificationDAO);
		expect(this.daoFactoryMock.getUtilisateurDao()).andReturn(utilisateurDAO);
		
		/* Récupération des données saisies */
		expect(this.httpServletRequestMock.getParameter(CHAMP_IDENTIFIANT)).andReturn(IDENTIFIANT_TEST);
		expect(this.httpServletRequestMock.getParameter(CHAMP_MOT_DE_PASSE)).andReturn(NOUVEAU_MOT_DE_PASSE_TEST);
		expect(this.httpServletRequestMock.getParameter(CHAMP_NOUVEAU_MOT_DE_PASSE)).andReturn(NOUVEAU_MOT_DE_PASSE_TEST);
		expect(this.httpServletRequestMock.getParameter(CHAMP_NOM)).andReturn(NOM_TEST);
		expect(this.httpServletRequestMock.getParameter(CHAMP_PRENOM)).andReturn(PRENOM_TEST);
		expect(this.httpServletRequestMock.getParameter(CHAMP_MOT_DE_PASSE)).andReturn(NOUVEAU_MOT_DE_PASSE_TEST);
		
		Callback callback = new Callback(CallbackType.SUCCESS, "Le mot de passe a bien été changé");
		
		this.httpservletResponseMock.setContentType("application/json");
		expectLastCall();
		
		expect(this.httpservletResponseMock.getWriter()).andReturn(new PrintWriter("testf"));
		
		this.replayAll();
		
		/* Cycle de vie d'une servlet */
		this.changerMotDePasse.init(this.servletConfigMock);
		this.changerMotDePasse.doPost(this.httpServletRequestMock, this.httpservletResponseMock);
		this.changerMotDePasse.destroy();
	}
	
}