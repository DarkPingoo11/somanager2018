package fr.eseo.ld.servlets;

import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.expectLastCall;
import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.easymock.EasyMockRunner;
import org.easymock.EasyMockSupport;
import org.easymock.Mock;
import org.easymock.TestSubject;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import fr.eseo.ld.beans.Commentaire;
import fr.eseo.ld.beans.EtatSujet;
import fr.eseo.ld.beans.Sujet;
import fr.eseo.ld.beans.Utilisateur;
import fr.eseo.ld.dao.CommentaireDAO;
import fr.eseo.ld.dao.DAOFactory;
import fr.eseo.ld.dao.NotificationDAO;
import fr.eseo.ld.dao.SujetDAO;

/**
 * Classe de tests unitaires JUnit 4 de la classe Commenter.
 *
 * <p>Utilisation de mocks (construits avec EasyMock).</p>
 *
 * @version 1.0
 * @author Maxime LENORMAND
 *
 * @see fr.eseo.ld.servlets.Commenter
 * @see org.junit
 * @see org.easymock
 */
@RunWith(EasyMockRunner.class)
public class CommenterTest extends EasyMockSupport {
	
	private static final String CONF_DAO_FACTORY = "daoFactory";
	
	private static final String TEST = "testCommenter";
	private static final Long ID_UTILISATEUR = 1L; // doit obligatoirement exister en base de donnée ne peut pas être créé car il est en autoIncrément donc non définissable et variable dans le temps
	private static final String ID_UTILISATEUR_NOTIF = "11";
	private static final String ID_SUJET = "1"; // doit obligatoirement exister en base de donnée ne peut pas être créé car il est en autoIncrément donc non définissable et variable dans le temps
	private static final String ID_OBSERVATION = "-1";
	
	private static final String ATT_SESSION_USER = "utilisateur";
	
	private static final String CHAMP_ID_UTILISATEUR = "idUtilisateur";
	private static final String CHAMP_ID_SUJET = "idSujet";
	private static final String CHAMP_ID_OBSERVATION = "idObservation";
	private static final String CHAMP_CONTENU = "contenu";
	private static final String CHAMP_TITRE_SUJET = "nomSujet";
	
	private static final String HEADER = "referer";

	private static final Integer NBR_MIN_ELEVES = 2;

	private static final Integer NBR_MAX_ELEVES = 99;
	
	private static CommentaireDAO commentaireDAO;
	private static NotificationDAO notificationDAO;
	private static SujetDAO sujetDAO;
	
	@TestSubject
	private Commenter commenter = new Commenter();
	
	@Mock
	private ServletConfig servletConfigMock;
	
	@Mock
	private ServletContext servletContextMock;
	
	@Mock
	private HttpServletRequest httpServletRequestMock;
	
	@Mock
	private HttpServletResponse httpservletResponseMock;
	
	@Mock
	private HttpSession httpSessionMock;
	
	@Mock
	private RequestDispatcher requestDispatcherMock;
	
	@Mock
	private DAOFactory daoFactoryMock;
	
	/**
	 * Récupère des instances de DAO.
	 * 
	 * @throws Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		final DAOFactory daoFactory = DAOFactory.getInstance();
		notificationDAO = daoFactory.getNotificationDao();
		commentaireDAO = daoFactory.getCommentaireDao();
		sujetDAO = daoFactory.getSujetDao();
				
//		final Sujet sujet = new Sujet();
//		sujet.setIdSujet(Long.parseLong(ID_SUJET));
//		sujet.setTitre(CHAMP_TITRE_SUJET);
//		sujet.setDescription(CHAMP_CONTENU);
//		sujet.setNbrMinEleves(NBR_MIN_ELEVES);
//		sujet.setNbrMaxEleves(NBR_MAX_ELEVES);
//		sujet.setEtat(EtatSujet.DEPOSE);
//		System.out.println(sujet);
//		sujetDAO.creer(sujet);
	}

	/**
	 * Supprime l'utilisateur inséré dans la BDD.
	 * 
	 * @throws Exception
	 */
	@AfterClass
	public static void tearDownAfterClass()throws Exception {
		final Commentaire commentaire = new Commentaire();
		commentaire.setContenu(TEST);
		commentaireDAO.supprimer(commentaire);
		
//		final Sujet sujet = new Sujet();
//		sujet.setIdSujet(Long.parseLong(ID_SUJET));
//		sujetDAO.supprimer(sujet);
	}

	/**
	 * Teste la méthode protected void doPost(HttpServletRequest request, HttpServletResponse response).
	 * 
	 * @throws ServletException
	 * @throws IOException
	 */
	@Test
	public void testDoPost() throws ServletException, IOException {
		/* Récupération d'instances de DAO */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
		expect(this.daoFactoryMock.getNotificationDao()).andReturn(notificationDAO);
		expect(this.daoFactoryMock.getCommentaireDao()).andReturn(commentaireDAO);
		
		/* Récupération de l'ID de l'Utilisateur en session */
		final Utilisateur utilisateur = new Utilisateur();
		final Long idUtilisateur = ID_UTILISATEUR;
		utilisateur.setIdUtilisateur(idUtilisateur);
		expect(this.httpServletRequestMock.getSession()).andReturn(this.httpSessionMock);
		expect(this.httpSessionMock.getAttribute(ATT_SESSION_USER)).andReturn(utilisateur);
		
		/* Récupération des données saisies */
		expect(this.httpServletRequestMock.getParameter(CHAMP_CONTENU)).andReturn(TEST);
		expect(this.httpServletRequestMock.getParameter(CHAMP_ID_SUJET)).andReturn(ID_SUJET);
		expect(this.httpServletRequestMock.getParameter(CHAMP_ID_OBSERVATION)).andReturn(ID_OBSERVATION);
		expect(this.httpServletRequestMock.getParameter(CHAMP_ID_UTILISATEUR)).andReturn(ID_UTILISATEUR_NOTIF);
		expect(this.httpServletRequestMock.getParameter(CHAMP_TITRE_SUJET)).andReturn(TEST);
		
		/* Redirection vers la page précédente */
		expect(this.httpServletRequestMock.getHeader(HEADER)).andReturn("SoManager/AjouterSujet");
		this.httpservletResponseMock.sendRedirect("SoManager/AjouterSujet");
		expectLastCall();
		
		this.replayAll();
		
		/* Cycle de vie d'une servlet */
		this.commenter.init(this.servletConfigMock);
		this.commenter.doPost(this.httpServletRequestMock, this.httpservletResponseMock);
		this.commenter.destroy();
		
		/* Vérification de l'insertion du commentaire dans la BDD */
		final Commentaire commentaire = new Commentaire();
		final String contenuAttendu = TEST;
		commentaire.setContenu(contenuAttendu);
		final List<Commentaire> commentairesTrouves = commentaireDAO.trouver(commentaire);
		String commentaireTrouve = "";
		if (!commentairesTrouves.isEmpty()) {
			commentaireTrouve = commentairesTrouves.get(0).getContenu();
		}
		assertEquals("Le commentaire n'a pas été inséré dans la BDD", contenuAttendu, commentaireTrouve);
		
		this.verifyAll();
	}

}