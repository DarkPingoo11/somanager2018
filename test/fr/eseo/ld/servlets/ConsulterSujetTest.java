package fr.eseo.ld.servlets;

import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.expectLastCall;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.easymock.EasyMockRunner;
import org.easymock.EasyMockSupport;
import org.easymock.Mock;
import org.easymock.MockType;
import org.easymock.TestSubject;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import fr.eseo.ld.beans.Utilisateur;
import fr.eseo.ld.dao.CommentaireDAO;
import fr.eseo.ld.dao.DAOFactory;
import fr.eseo.ld.dao.EtudiantDAO;
import fr.eseo.ld.dao.OptionESEODAO;
import fr.eseo.ld.dao.PosterDAO;
import fr.eseo.ld.dao.ProfesseurSujetDAO;
import fr.eseo.ld.dao.SujetDAO;
import fr.eseo.ld.dao.UtilisateurDAO;

/**
 * Classe de tests unitaires JUnit 4 de la classe ConsulterSujets.
 *
 * <p>Utilisation de mocks (construits avec EasyMock).</p>
 *
 * @version 1.0
 * @author Maxime LENORMAND
 *
 * @see fr.eseo.ld.servlets.ConsulterSujets
 * @see org.junit
 * @see org.easymock
 */
@RunWith(EasyMockRunner.class)
public class ConsulterSujetTest extends EasyMockSupport {
	
	private static final String CONF_DAO_FACTORY = "daoFactory";
	
	private static final String ATT_SESSION_USER = "utilisateur";
	private static final Long ID_ETUDIANT = 11L;
	
	private static final String VUE_FORM = "/WEB-INF/formulaires/consulterSujets.jsp";
	
	private static UtilisateurDAO utilisateurDAO;
	private static SujetDAO sujetDAO;
	private static CommentaireDAO commentaireDAO;
	private static OptionESEODAO optionESEODAO;
	private static PosterDAO posterDAO;
	private static EtudiantDAO etudiantDAO;
	private static ProfesseurSujetDAO professeurSujetDAO;

	@TestSubject
	private ConsulterSujets consulterSujets = new ConsulterSujets();
	
	@Mock
	private ServletConfig servletConfigMock;
	
	@Mock
	private ServletContext servletContextMock;
	
	@Mock(type = MockType.NICE)
	private HttpServletRequest httpServletRequestMock;
	
	@Mock
	private HttpServletResponse httpservletResponseMock;
	
	@Mock
	private HttpSession httpSessionMock;
	
	@Mock
	private RequestDispatcher requestDispatcherMock;
	
	@Mock
	private DAOFactory daoFactoryMock;

	/**
	 * Récupère des instances de DAO.
	 * 
	 * @throws Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		/* Instanciations de DAO */
		DAOFactory daoFactory = DAOFactory.getInstance();
		utilisateurDAO = daoFactory.getUtilisateurDao();
		sujetDAO = daoFactory.getSujetDao();
		commentaireDAO = daoFactory.getCommentaireDao();
		optionESEODAO = daoFactory.getOptionESEODAO();
		posterDAO = daoFactory.getPosterDao();
		etudiantDAO = daoFactory.getEtudiantDao();
		professeurSujetDAO = daoFactory.getProfesseurSujetDAO();
		
	}

	/**
	 * Teste la méthode protected void doGet(HttpServletRequest request, HttpServletResponse response).
	 * 
	 * @throws ServletException
	 * @throws IOException
	 */
	@Test
	public void testDoGet() throws ServletException, IOException {
		/* Récupération d'instances de DAO */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
		expect(this.daoFactoryMock.getUtilisateurDao()).andReturn(utilisateurDAO);
		expect(this.daoFactoryMock.getSujetDao()).andReturn(sujetDAO);
		expect(this.daoFactoryMock.getCommentaireDao()).andReturn(commentaireDAO);
		expect(this.daoFactoryMock.getOptionESEODAO()).andReturn(optionESEODAO);
		expect(this.daoFactoryMock.getPosterDao()).andReturn(posterDAO);
		expect(this.daoFactoryMock.getEtudiantDao()).andReturn(etudiantDAO);
		expect(this.daoFactoryMock.getProfesseurSujetDAO()).andReturn(professeurSujetDAO);
		
		/* Récupération de l'Utilisateur en session */
		expect(this.httpServletRequestMock.getSession()).andReturn(this.httpSessionMock);
		final Utilisateur utilisateur = new Utilisateur();
		final Long idUtilisateur = ID_ETUDIANT;
		utilisateur.setIdUtilisateur(idUtilisateur);
		expect(this.httpSessionMock.getAttribute(ATT_SESSION_USER)).andReturn(utilisateur);
		
		/* Affichage du formulaire */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getRequestDispatcher(VUE_FORM)).andReturn(this.requestDispatcherMock);
		this.requestDispatcherMock.forward(this.httpServletRequestMock, this.httpservletResponseMock);
		expectLastCall();

		this.replayAll();

		/* Cycle de vie d'une servlet */
		this.consulterSujets.init(this.servletConfigMock); 
		this.consulterSujets.doGet(this.httpServletRequestMock, this.httpservletResponseMock);
		this.consulterSujets.destroy();

		this.verifyAll();
	}

}