package fr.eseo.ld.servlets;

import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.expectLastCall;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.easymock.EasyMockRunner;
import org.easymock.EasyMockSupport;
import org.easymock.Mock;
import org.easymock.TestSubject;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 * Classe de tests unitaires JUnit 4 de la classe Deconnexion.
 *
 * <p>Utilisation de mocks (construits avec EasyMock).</p>
 *
 * @version 1.0
 * @author Maxime LENORMAND
 *
 * @see fr.eseo.ld.servlets.Deconnexion
 * @see org.junit
 * @see org.easymock
 */
@RunWith(EasyMockRunner.class)
public class DeconnexionTest extends EasyMockSupport {
	
	private static final String CONTEXT_PATH = "SoManager/CreerEquipe";
	
	private static final String VUE_INDEX = "/index.jsp";
	
	@TestSubject
	private Deconnexion deconnexion = new Deconnexion();
	
	@Mock
	private ServletConfig servletConfigMock;
	
	@Mock
	private HttpServletRequest httpServletRequestMock;
	
	@Mock
	private HttpServletResponse httpservletResponseMock;
	
	@Mock
	private FilterChain filterChainMock;
	
	@Mock
	private HttpSession httpSessionMock;
	
	@Mock
	private RequestDispatcher requestDispatcherMock;

	/**
	 * Teste la méthode protected void doGet(HttpServletRequest request, HttpServletResponse response).
	 * 
	 * @throws ServletException
	 * @throws IOException
	 */
	@Test
	public void testDoGet() throws ServletException, IOException {
		/* Récupération et destruction de la session en cours */
		expect(this.httpServletRequestMock.getSession()).andReturn(this.httpSessionMock);
		this.httpSessionMock.invalidate();
		expectLastCall();
		
		/* Redirection vers la page d'accueil */
		expect(this.httpServletRequestMock.getContextPath()).andReturn(CONTEXT_PATH);
		this.httpservletResponseMock.sendRedirect(CONTEXT_PATH + VUE_INDEX);
		expectLastCall();

		this.replayAll();

		/* Cycle de vie d'une servlet */
		this.deconnexion.init(this.servletConfigMock);
		this.deconnexion.doGet(this.httpServletRequestMock, this.httpservletResponseMock);
		this.deconnexion.destroy();

		this.verifyAll();
	}

}