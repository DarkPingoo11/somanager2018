package fr.eseo.ld.servlets;

import static org.junit.Assert.assertTrue;

import java.lang.reflect.Constructor;
import java.lang.reflect.Modifier;

import org.junit.Test;

/**
 * Classe de tests unitaires JUnit 4 de la classe ServletUtilitaire.
 * 
 * <p>Utilisation de la réflexivité.</p>
 *
 * @version 1.0
 * @author Maxime LENORMAND
 *
 * @see fr.eseo.ld.servlets.ServletUtilitaire
 * @see org.junit
 */
public class ServletUtilitaireTest {

	/**
	 * Teste le constructeur privé de la classe utilitaire grâce à la réflexion.
	 * 
	 * @throws Exception
	 */
	@Test
	public void testConstructeurPrive() throws Exception {
		final Constructor<ServletUtilitaire> constructeur = ServletUtilitaire.class.getDeclaredConstructor();
		assertTrue("Le constructeur n'est pas privé", Modifier.isPrivate(constructeur.getModifiers()));
		constructeur.setAccessible(true);
		constructeur.newInstance();
	}

}