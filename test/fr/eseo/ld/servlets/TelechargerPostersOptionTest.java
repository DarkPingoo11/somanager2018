package fr.eseo.ld.servlets;

import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.expectLastCall;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.easymock.EasyMockRunner;
import org.easymock.EasyMockSupport;
import org.easymock.Mock;
import org.easymock.MockType;
import org.easymock.TestSubject;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;

import fr.eseo.ld.beans.AnneeScolaire;
import fr.eseo.ld.beans.Equipe;
import fr.eseo.ld.beans.EtatSujet;
import fr.eseo.ld.beans.Etudiant;
import fr.eseo.ld.beans.EtudiantEquipe;
import fr.eseo.ld.beans.OptionESEO;
import fr.eseo.ld.beans.Poster;
import fr.eseo.ld.beans.Professeur;
import fr.eseo.ld.beans.Role;
import fr.eseo.ld.beans.RoleUtilisateur;
import fr.eseo.ld.beans.Sujet;
import fr.eseo.ld.beans.Utilisateur;
import fr.eseo.ld.dao.AnneeScolaireDAO;
import fr.eseo.ld.dao.DAOFactory;
import fr.eseo.ld.dao.EquipeDAO;
import fr.eseo.ld.dao.EtudiantDAO;
import fr.eseo.ld.dao.NotificationDAO;
import fr.eseo.ld.dao.OptionESEODAO;
import fr.eseo.ld.dao.PosterDAO;
import fr.eseo.ld.dao.ProfesseurDAO;
import fr.eseo.ld.dao.RoleDAO;
import fr.eseo.ld.dao.SujetDAO;
import fr.eseo.ld.dao.UtilisateurDAO;

/**
 * Classe de tests unitaires JUnit 4 de la classe TelechargerPostersOption.
 *
 * <p>Utilisation de mocks (construits avec EasyMock).</p>
 *
 * @version 1.0
 * @author Maxime LENORMAND
 *
 * @see fr.eseo.ld.servlets.TelechargerPostersOption
 * @see org.junit
 * @see org.easymock
 */
@RunWith(EasyMockRunner.class)
public class TelechargerPostersOptionTest extends EasyMockSupport {

	private static final String CONF_DAO_FACTORY = "daoFactory";
	
	private static final Long ID_SUJET_TEST = 161L;
	private static final Long ID_UTILISATEUR_TEST_SUCCES = 16L;
	private static final Long ID_UTILISATEUR_TEST_ECHEC = 54L;
	
	private static final String ATT_SESSION_USER = "utilisateur";
	
	private static final String CHEMIN_POSTER = "/home/etudiant/Posters/2018/";
	private static final String VUE_FORM = "/WEB-INF/formulaires/gererSujets.jsp";
	
	public static final int TAILLE_TAMPON = 10240;
	
	private static PosterDAO posterDAO;
	private static NotificationDAO notificationDAO;
	private static EquipeDAO equipeDAO;
	private static Utilisateur utilisateurTest;
	private static Etudiant etudiantTest;
	private static Sujet sujetTest;
	private static Equipe equipeTest;
	private static UtilisateurDAO utilisateurDAO;
	private static EtudiantDAO etudiantDAO;
	private static SujetDAO sujetDAO;
	private static EtudiantEquipe etudiantEquipeTest ;
	private static OptionESEODAO optionESEODAO;
	private static OptionESEO optionESEOTest;
	private static AnneeScolaireDAO anneeScolaireDAO;
	private static AnneeScolaire anneeTest;
	private static Poster posterTest;
	private static RoleDAO roleDAO;
	private static Role roleTest;
	private static RoleUtilisateur roleUtilisateurTest;
	private static ProfesseurDAO professeurDAO;
	private static Professeur professeurTest;

	private static Utilisateur utilisateurTest2;
	
	
	@TestSubject
	private TelechargerPostersOption telechargerPostersOption = new TelechargerPostersOption();
	
	@Mock
	private ServletConfig servletConfigMock;
	
	@Mock
	private ServletContext servletContextMock;
	
	@Mock
	private HttpServletRequest httpServletRequestMock;
	
	@Mock
	private HttpServletResponse httpservletResponseMock;
	
	@Mock
	private HttpSession httpSessionMock;
	
	@Mock
	private RequestDispatcher requestDispatcherMock;
	
	@Mock(type = MockType.NICE)
	private ServletOutputStream sosMock;
	
	@Mock
	private DAOFactory daoFactoryMock;
	
	@Mock
	private Role roleMock;
	
	/**
	 * Récupère des instances de DAO et Insère un poster test.
	 * 
	 * @throws Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		/* Instanciation de DAO */
		final DAOFactory daoFactory = DAOFactory.getInstance();
		notificationDAO = daoFactory.getNotificationDao();
		posterDAO = daoFactory.getPosterDao();
		equipeDAO = daoFactory.getEquipeDAO();
		utilisateurDAO = daoFactory.getUtilisateurDao();
		etudiantDAO = daoFactory.getEtudiantDao();
		sujetDAO = daoFactory.getSujetDao();
		optionESEODAO = daoFactory.getOptionESEODAO();
		anneeScolaireDAO = daoFactory.getAnneeScolaireDAO();
		roleDAO = daoFactory.getRoleDAO();
		professeurDAO = daoFactory.getProfesseurDAO();
		
		/*Création des données de test*/
		
		/* Insertion d'un utilisateur test dans la BDD */
        Utilisateur utilisateur = new Utilisateur();
        utilisateur.setIdentifiant("test");
        utilisateur.setNom("test");
        utilisateur.setPrenom("test");
        utilisateur.setValide("oui");
        utilisateurDAO.creer(utilisateur);
        utilisateurTest = utilisateurDAO.trouver(utilisateur).get(0);
        
        Utilisateur utilisateur2 = new Utilisateur();
        utilisateur2.setIdentifiant("test2");
        utilisateur2.setNom("test2");
        utilisateur2.setPrenom("test2");
        utilisateur2.setValide("oui");
        utilisateurDAO.creer(utilisateur2);
        utilisateurTest2 = utilisateurDAO.trouver(utilisateur2).get(0);
        
        Etudiant etudiant = new Etudiant();
        etudiant.setIdEtudiant(utilisateurTest2.getIdUtilisateur());
        etudiant.setAnnee(2018);
        etudiant.setContratPro("non");
        etudiantDAO.creer(etudiant);
        etudiantTest = etudiantDAO.trouver(etudiant).get(0);
        
		Sujet sujet = new Sujet();
		sujet.setTitre("sujetTest");
		sujet.setDescription("description");
		sujet.setNbrMinEleves(1);
		sujet.setNbrMaxEleves(4);
		sujet.setEtat(EtatSujet.ATTRIBUE);
		sujetDAO.creer(sujet);
		sujetTest = sujetDAO.trouver(sujet).get(0);
		
		OptionESEO option = new OptionESEO();
		option.setIdOption(1L);
		option.setNomOption("LD");
		optionESEODAO.creer(option);
		optionESEOTest = optionESEODAO.trouver(option).get(0);
		
		AnneeScolaire annee = new AnneeScolaire();
		annee.setAnneeDebut(2018);
		annee.setAnneeFin(2019);
		annee.setIdOption(1L);
		anneeScolaireDAO.creer(annee);
		anneeTest = anneeScolaireDAO.trouver(annee).get(0);
		
		Professeur professeur = new Professeur();
        professeur.setIdProfesseur(utilisateurTest.getIdUtilisateur());;
        professeurDAO.creer(professeur);
        professeurTest = professeurDAO.trouver(professeur).get(0);
        
        Role role = new Role();
        role.setIdRole(5L);
        role.setNomRole("profResponsable");
        role.setType("applicatif");
        roleDAO.creerRole(role);
        roleTest = roleDAO.trouver(role).get(0);
        
        utilisateurDAO.associerRoleOption(utilisateurTest, roleTest, optionESEOTest);
		
		Equipe equipe = new Equipe();
		equipe.setAnnee(2018);
		equipe.setTaille(1);
		equipe.setIdSujet(sujetTest.getIdSujet());
		equipe.setValide("oui");
		equipeDAO.creer(equipe);
		equipeTest = equipeDAO.trouver(equipe).get(0);
		
		EtudiantEquipe etudiantEquipe = new EtudiantEquipe();
		etudiantEquipe.setIdEquipe(equipeTest.getIdEquipe());
		etudiantEquipe.setIdEtudiant(etudiantTest.getIdEtudiant());
		equipeDAO.attribuerEtudiantEquipe(etudiantEquipe);
		etudiantEquipeTest = equipeDAO.trouverEtudiantEquipe(etudiantEquipe).get(0);
		
		String cheminPoster = CHEMIN_POSTER + sujetTest.getIdSujet() + "_test.pdf";
		System.out.println(cheminPoster);
		Poster poster = new Poster();
		poster.setChemin(cheminPoster);
		poster.setIdEquipe(equipeTest.getIdEquipe());
		poster.setIdSujet(sujetTest.getIdSujet());
		poster.setValide("oui");
		posterDAO.creer(poster);
		posterTest = posterDAO.trouver(poster).get(0);
	}
	
	/**
     * Supprime les données de test dans la BDD.
     *
     * @throws Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
    	posterDAO.supprimer(posterTest);
    	equipeDAO.supprimerEtudiantEquipe(etudiantEquipeTest);
    	utilisateurDAO.supprimerRoleOption(utilisateurTest, roleTest, optionESEOTest);
    	roleDAO.supprimer(roleTest);
    	anneeScolaireDAO.supprimer(anneeTest);
    	optionESEODAO.supprimer(optionESEOTest);
    	sujetDAO.supprimer(sujetTest);
    	professeurDAO.supprimer(professeurTest);
        utilisateurDAO.supprimer(utilisateurTest);
        etudiantDAO.supprimer(etudiantTest);
        utilisateurDAO.supprimer(utilisateurTest2);
    }
	
	/**
	 * Teste la méthode protected void doPost(HttpServletRequest request, HttpServletResponse response).
	 * 
	 * <p>Cas du succès du téléchargement de tous les posters validés des options concernées.</p>
	 * 
	 * @throws ServletException
	 * @throws IOException
	 */
	@Test
	public void testDoPostSucces() throws ServletException, IOException {
		/* Récupération d'instances de DAO */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
		expect(this.daoFactoryMock.getPosterDao()).andReturn(posterDAO);
		expect(this.daoFactoryMock.getSujetDao()).andReturn(sujetDAO);
		expect(this.daoFactoryMock.getOptionESEODAO()).andReturn(optionESEODAO);
		expect(this.daoFactoryMock.getUtilisateurDao()).andReturn(utilisateurDAO);
		expect(this.daoFactoryMock.getNotificationDao()).andReturn(notificationDAO);
		
		/* Récupération de l'ID de l'Utilisateur en session */
		expect(this.httpServletRequestMock.getSession()).andReturn(this.httpSessionMock);
		expect(this.httpSessionMock.getAttribute(ATT_SESSION_USER)).andReturn(utilisateurTest);
		
		/* Formatage de la response */
		expect(this.httpservletResponseMock.getOutputStream()).andReturn(this.sosMock);
		this.httpservletResponseMock.setContentType("application/zip");
		expectLastCall();
		this.httpservletResponseMock.setHeader("Content-Disposition", "attachment; filename=\"posters.zip\"");
		expectLastCall();
		
		this.replayAll();
		
		/* Cycle de vie d'une servlet */
		this.telechargerPostersOption.init(this.servletConfigMock);
		this.telechargerPostersOption.doPost(this.httpServletRequestMock, this.httpservletResponseMock);
		this.telechargerPostersOption.destroy();
		
		this.verifyAll();
	}
	
	/**
	 * Teste la méthode protected void doPost(HttpServletRequest request, HttpServletResponse response).
	 * 
	 * <p>Cas de l'échec du téléchargement de tous les posters validés des options concernées.</p>
	 * 
	 * @throws ServletException
	 * @throws IOException
	 */
	@Test
	@Ignore
	public void testDoPostEchec() throws ServletException, IOException {
		/* Récupération d'instances de DAO */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
		expect(this.daoFactoryMock.getPosterDao()).andReturn(posterDAO);
		expect(this.daoFactoryMock.getSujetDao()).andReturn(sujetDAO);
		expect(this.daoFactoryMock.getOptionESEODAO()).andReturn(optionESEODAO);
		expect(this.daoFactoryMock.getUtilisateurDao()).andReturn(utilisateurDAO);
		expect(this.daoFactoryMock.getNotificationDao()).andReturn(notificationDAO);
		
		/* Récupération de l'ID de l'Utilisateur en session */
		Utilisateur utilisateur = new Utilisateur();
		utilisateur.setIdUtilisateur(ID_UTILISATEUR_TEST_ECHEC);
		expect(this.httpServletRequestMock.getSession()).andReturn(this.httpSessionMock);
		expect(this.httpSessionMock.getAttribute(ATT_SESSION_USER)).andReturn(utilisateur);
		
		/* Ré-affichage du formulaire avec notification d'erreur */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getRequestDispatcher(VUE_FORM)).andReturn(this.requestDispatcherMock);
		this.requestDispatcherMock.forward(this.httpServletRequestMock, this.httpservletResponseMock);
		expectLastCall();
		
		this.replayAll();
		
		/* Cycle de vie d'une servlet */
		this.telechargerPostersOption.init(this.servletConfigMock);
		this.telechargerPostersOption.doPost(this.httpServletRequestMock, this.httpservletResponseMock);
		this.telechargerPostersOption.destroy();
		
		this.verifyAll();
	}

}