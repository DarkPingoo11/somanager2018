package fr.eseo.ld.servlets;

import fr.eseo.ld.beans.OptionESEO;
import fr.eseo.ld.beans.Role;
import fr.eseo.ld.beans.Utilisateur;
import fr.eseo.ld.dao.*;
import org.easymock.*;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.expectLastCall;

/**
 * Classe de tests unitaires JUnit 4 de la classe Dashboard.
 *
 * <p>Utilisation de mocks (construits avec EasyMock).</p>
 *
 * @version 1.0
 * @author Maxime LENORMAND
 *
 * @see fr.eseo.ld.servlets.Dashboard
 * @see org.junit
 * @see org.easymock
 */
@RunWith(EasyMockRunner.class)
public class DashboardTest extends EasyMockSupport {

	private static final String CONF_DAO_FACTORY = "daoFactory";
	
	private static final String ATT_SESSION_ROLES = "roles";
	private static final String ATT_SESSION_OPTIONS = "options";
	
	private static final String ATT_SESSION_USER = "utilisateur";
	private static final Long ID_ADMIN = 250L;
	private static final Long ID_ETUDIANT_SEUL = 11L;
	private static final Long ID_ETUDIANT_EQUIPE = 5L;
	private static final Long ID_PROF = 69L;
	private static final Long ID_PROF_OPTION = 70L;
	private static final Long ID_PROF_RESPONSABLE = 71L;  
	
	private static final String VUE_INDEX = "/index.jsp";
	private static final String VUE_FORM = "/WEB-INF/formulaires/dashboard.jsp";
	
	private static SujetDAO sujetDAO;
	private static UtilisateurDAO utilisateurDAO;
	private static EquipeDAO equipeDAO;
	private static OptionESEODAO optionDAO;
	private static AnneeScolaireDAO anneeScolaireDAO;
	private static EtudiantDAO etudiantDAO;
	private static JurySoutenanceDAO jurySoutenanceDAO;
	private static JuryPosterDAO juryPosterDAO;
	private static SoutenanceDAO soutenanceDAO;
	private static NotePosterDAO notePosterDAO;
	private static NoteSoutenanceDAO noteSoutenanceDAO;

	private static PosterDAO posterDAO;

	private static ProfesseurSujetDAO professeurSujetDAO;

	private static PglEtudiantEquipeDAO etudiantEquipeDAO;

	private static BonusMalusDAO bonusMalusDAO;

	private static DemandeJuryDAO demandeJuryDAO;

	private static ProfesseurDAO professeurDAO;
	
	@TestSubject
	private Dashboard dashboard = new Dashboard();
	
	@Mock
	private ServletConfig servletConfigMock;
	
	@Mock
	private ServletContext servletContextMock;
	
	@Mock(type = MockType.NICE)
	private HttpServletRequest httpServletRequestMock;
	
	@Mock
	private HttpServletResponse httpservletResponseMock;
	
	@Mock
	private HttpSession httpSessionMock;
	
	@Mock
	private RequestDispatcher requestDispatcherMock;
	
	@Mock
	private DAOFactory daoFactoryMock;

	/**
	 * Récupère des instances de DAO.
	 * 
	 * @throws Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		DAOFactory daoFactory = DAOFactory.getInstance();
		sujetDAO = daoFactory.getSujetDao();
		utilisateurDAO = daoFactory.getUtilisateurDao();
		equipeDAO = daoFactory.getEquipeDAO();
		optionDAO = daoFactory.getOptionESEODAO();
		professeurDAO = daoFactory.getProfesseurDAO();
		anneeScolaireDAO = daoFactory.getAnneeScolaireDAO();
		etudiantDAO = daoFactory.getEtudiantDao();
		juryPosterDAO = daoFactory.getJuryPosterDAO();
		jurySoutenanceDAO = daoFactory.getJurySoutenanceDAO();
		soutenanceDAO = daoFactory.getSoutenanceDAO();
		notePosterDAO = daoFactory.getNotePosterDao();
		noteSoutenanceDAO = daoFactory.getNoteSoutenanceDAO();
		posterDAO = daoFactory.getPosterDao();
		professeurSujetDAO = daoFactory.getProfesseurSujetDAO();
		etudiantEquipeDAO = daoFactory.getPglEtudiantEquipeDAO();
		bonusMalusDAO = daoFactory.getBonusMalusDAO();
		demandeJuryDAO = daoFactory.getDemandeJuryDAO();
	}

	/**
	 * Teste la méthode protected void doGet(HttpServletRequest request, HttpServletResponse response).
	 * 
	 * <p>Cas d'un utilisateur non connecté.</p>
	 * 
	 * @throws ServletException
	 * @throws IOException
	 */
	@Test
	public void testDoGetNonConnecte() throws ServletException, IOException {
		/* Récupération d'instances de DAO */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
		expect(this.daoFactoryMock.getSujetDao()).andReturn(sujetDAO);
		expect(this.daoFactoryMock.getUtilisateurDao()).andReturn(utilisateurDAO);
		expect(this.daoFactoryMock.getEquipeDAO()).andReturn(equipeDAO);
		expect(this.daoFactoryMock.getOptionESEODAO()).andReturn(optionDAO);
		expect(this.daoFactoryMock.getProfesseurDAO()).andReturn(professeurDAO);
		expect(this.daoFactoryMock.getAnneeScolaireDAO()).andReturn(anneeScolaireDAO);
		expect(this.daoFactoryMock.getEtudiantDao()).andReturn(etudiantDAO);
		expect(this.daoFactoryMock.getJuryPosterDAO()).andReturn(juryPosterDAO);
		expect(this.daoFactoryMock.getJurySoutenanceDAO()).andReturn(jurySoutenanceDAO);
		expect(this.daoFactoryMock.getSoutenanceDAO()).andReturn(soutenanceDAO);
		expect(this.daoFactoryMock.getNotePosterDao()).andReturn(notePosterDAO);
		expect(this.daoFactoryMock.getNoteSoutenanceDAO()).andReturn(noteSoutenanceDAO);
		expect(this.daoFactoryMock.getPosterDao()).andReturn(posterDAO);
		expect(this.daoFactoryMock.getProfesseurSujetDAO()).andReturn(professeurSujetDAO);
		expect(this.daoFactoryMock.getPglEtudiantEquipeDAO()).andReturn(etudiantEquipeDAO);
		expect(this.daoFactoryMock.getBonusMalusDAO()).andReturn(bonusMalusDAO);
		expect(this.daoFactoryMock.getDemandeJuryDAO()).andReturn(demandeJuryDAO);
		
		/* Récupération de la session depuis la requête */
		expect(this.httpServletRequestMock.getSession()).andReturn(this.httpSessionMock);
		
		/* Récupération d'attributs de l'Utilisateur en session */
		List<Role> roles = null;
		expect(this.httpSessionMock.getAttribute(ATT_SESSION_ROLES)).andReturn(roles);
		List<OptionESEO> options = null;
		expect(this.httpSessionMock.getAttribute(ATT_SESSION_OPTIONS)).andReturn(options);

		/* A la réception d'une requête GET, simple affichage du formulaire */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getRequestDispatcher(VUE_INDEX)).andReturn(this.requestDispatcherMock);
		this.requestDispatcherMock.forward(this.httpServletRequestMock, this.httpservletResponseMock);
		expectLastCall();

		this.replayAll();

		/* Cycle de vie d'une servlet */
		this.dashboard.init(this.servletConfigMock);
		this.dashboard.doGet(this.httpServletRequestMock, this.httpservletResponseMock);
		this.dashboard.destroy();

		this.verifyAll();
	}

	/**
	 * Teste la méthode protected void doGet(HttpServletRequest request, HttpServletResponse response).
	 *
	 * <p>Cas d'un utilisateur possédant le rôle "admin".</p>
	 *
	 * @throws ServletException
	 * @throws IOException
	 */
	@Test
	public void testDoGetAdmin() throws ServletException, IOException {
		/* Récupération d'instances de DAO */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
		expect(this.daoFactoryMock.getSujetDao()).andReturn(sujetDAO);
		expect(this.daoFactoryMock.getUtilisateurDao()).andReturn(utilisateurDAO);
		expect(this.daoFactoryMock.getEquipeDAO()).andReturn(equipeDAO);
		expect(this.daoFactoryMock.getOptionESEODAO()).andReturn(optionDAO);
		expect(this.daoFactoryMock.getProfesseurDAO()).andReturn(professeurDAO);
		expect(this.daoFactoryMock.getAnneeScolaireDAO()).andReturn(anneeScolaireDAO);
		expect(this.daoFactoryMock.getEtudiantDao()).andReturn(etudiantDAO);
		expect(this.daoFactoryMock.getJuryPosterDAO()).andReturn(juryPosterDAO);
		expect(this.daoFactoryMock.getJurySoutenanceDAO()).andReturn(jurySoutenanceDAO);
		expect(this.daoFactoryMock.getSoutenanceDAO()).andReturn(soutenanceDAO);
		expect(this.daoFactoryMock.getNotePosterDao()).andReturn(notePosterDAO);
		expect(this.daoFactoryMock.getNoteSoutenanceDAO()).andReturn(noteSoutenanceDAO);
		expect(this.daoFactoryMock.getPosterDao()).andReturn(posterDAO);
		expect(this.daoFactoryMock.getProfesseurSujetDAO()).andReturn(professeurSujetDAO);
		expect(this.daoFactoryMock.getPglEtudiantEquipeDAO()).andReturn(etudiantEquipeDAO);
		expect(this.daoFactoryMock.getBonusMalusDAO()).andReturn(bonusMalusDAO);
		expect(this.daoFactoryMock.getDemandeJuryDAO()).andReturn(demandeJuryDAO);
		
		/* Récupération de la session depuis la requête */
		expect(this.httpServletRequestMock.getSession()).andReturn(this.httpSessionMock);

		/* Récupération d'attributs de l'Utilisateur en session */
		Role role = new Role();
		role.setIdRole(2L);
		role.setNomRole("admin");
		List<Role> roles = new ArrayList<>();
		roles.add(role);
		expect(this.httpSessionMock.getAttribute(ATT_SESSION_ROLES)).andReturn(roles);
		List<OptionESEO> options = null;
		expect(this.httpSessionMock.getAttribute(ATT_SESSION_OPTIONS)).andReturn(options);

		/* Récupération de l'ID de l'Utilisateur en session */
		final Utilisateur utilisateur = new Utilisateur();
		final Long idUtilisateur = ID_ADMIN;
		utilisateur.setIdUtilisateur(idUtilisateur);
		expect(this.httpSessionMock.getAttribute(ATT_SESSION_USER)).andReturn(utilisateur);

		/* A la réception d'une requête GET, simple affichage du formulaire */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getRequestDispatcher(VUE_FORM)).andReturn(this.requestDispatcherMock);
		this.requestDispatcherMock.forward(this.httpServletRequestMock, this.httpservletResponseMock);
		expectLastCall();

		this.replayAll();

		/* Cycle de vie d'une servlet */
		this.dashboard.init(this.servletConfigMock);
		this.dashboard.doGet(this.httpServletRequestMock, this.httpservletResponseMock);
		this.dashboard.destroy();

		this.verifyAll();
	}

	/**
	 * Teste la méthode protected void doGet(HttpServletRequest request, HttpServletResponse response).
	 *
	 * <p>Cas d'un utilisateur possédant le rôle "etudiant" et possédant une équipe.</p>
	 *
	 * @throws ServletException
	 * @throws IOException
	 */
	@Test
	public void testDoGetEtudiantEquipe() throws ServletException, IOException {
		/* Récupération d'instances de DAO */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
		expect(this.daoFactoryMock.getSujetDao()).andReturn(sujetDAO);
		expect(this.daoFactoryMock.getUtilisateurDao()).andReturn(utilisateurDAO);
		expect(this.daoFactoryMock.getEquipeDAO()).andReturn(equipeDAO);
		expect(this.daoFactoryMock.getOptionESEODAO()).andReturn(optionDAO);
		expect(this.daoFactoryMock.getProfesseurDAO()).andReturn(professeurDAO);
		expect(this.daoFactoryMock.getAnneeScolaireDAO()).andReturn(anneeScolaireDAO);
		expect(this.daoFactoryMock.getEtudiantDao()).andReturn(etudiantDAO);
		expect(this.daoFactoryMock.getJuryPosterDAO()).andReturn(juryPosterDAO);
		expect(this.daoFactoryMock.getJurySoutenanceDAO()).andReturn(jurySoutenanceDAO);
		expect(this.daoFactoryMock.getSoutenanceDAO()).andReturn(soutenanceDAO);
		expect(this.daoFactoryMock.getNotePosterDao()).andReturn(notePosterDAO);
		expect(this.daoFactoryMock.getNoteSoutenanceDAO()).andReturn(noteSoutenanceDAO);
		expect(this.daoFactoryMock.getPosterDao()).andReturn(posterDAO);
		expect(this.daoFactoryMock.getProfesseurSujetDAO()).andReturn(professeurSujetDAO);
		expect(this.daoFactoryMock.getPglEtudiantEquipeDAO()).andReturn(etudiantEquipeDAO);
		expect(this.daoFactoryMock.getBonusMalusDAO()).andReturn(bonusMalusDAO);
		expect(this.daoFactoryMock.getDemandeJuryDAO()).andReturn(demandeJuryDAO);
		
		/* Récupération de la session depuis la requête */
		expect(this.httpServletRequestMock.getSession()).andReturn(this.httpSessionMock);

		/* Récupération d'attributs de l'Utilisateur en session */
		Role role = new Role();
		role.setIdRole(1L);
		role.setNomRole("etudiant");
		List<Role> roles = new ArrayList<>();
		roles.add(role);
		expect(this.httpSessionMock.getAttribute(ATT_SESSION_ROLES)).andReturn(roles);
		OptionESEO option = new OptionESEO();
		option.setIdOption(1L);
		option.setNomOption("LD");
		List<OptionESEO> options = new ArrayList<>();
		options.add(option);
		expect(this.httpSessionMock.getAttribute(ATT_SESSION_OPTIONS)).andReturn(options);

		/* Récupération de l'ID de l'Utilisateur en session */
		final Utilisateur utilisateur = new Utilisateur();
		final Long idUtilisateur = ID_ETUDIANT_EQUIPE;
		utilisateur.setIdUtilisateur(idUtilisateur);
		expect(this.httpSessionMock.getAttribute(ATT_SESSION_USER)).andReturn(utilisateur);

		/* A la réception d'une requête GET, simple affichage du formulaire */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getRequestDispatcher(VUE_FORM)).andReturn(this.requestDispatcherMock);
		this.requestDispatcherMock.forward(this.httpServletRequestMock, this.httpservletResponseMock);
		expectLastCall();

		this.replayAll();

		/* Cycle de vie d'une servlet */
		this.dashboard.init(this.servletConfigMock);
		this.dashboard.doGet(this.httpServletRequestMock, this.httpservletResponseMock);
		this.dashboard.destroy();

		this.verifyAll();
	}

	/**
	 * Teste la méthode protected void doGet(HttpServletRequest request, HttpServletResponse response).
	 *
	 * <p>Cas d'un utilisateur possédant le rôle "etudiant" et ne possédant pas d'équipe.</p>
	 *
	 * @throws ServletException
	 * @throws IOException
	 */
	@Test
	public void testDoGetEtudiantNonEquipe() throws ServletException, IOException {
		/* Récupération d'instances de DAO */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
		expect(this.daoFactoryMock.getSujetDao()).andReturn(sujetDAO);
		expect(this.daoFactoryMock.getUtilisateurDao()).andReturn(utilisateurDAO);
		expect(this.daoFactoryMock.getEquipeDAO()).andReturn(equipeDAO);
		expect(this.daoFactoryMock.getOptionESEODAO()).andReturn(optionDAO);
		expect(this.daoFactoryMock.getProfesseurDAO()).andReturn(professeurDAO);
		expect(this.daoFactoryMock.getAnneeScolaireDAO()).andReturn(anneeScolaireDAO);
		expect(this.daoFactoryMock.getEtudiantDao()).andReturn(etudiantDAO);
		expect(this.daoFactoryMock.getJuryPosterDAO()).andReturn(juryPosterDAO);
		expect(this.daoFactoryMock.getJurySoutenanceDAO()).andReturn(jurySoutenanceDAO);
		expect(this.daoFactoryMock.getSoutenanceDAO()).andReturn(soutenanceDAO);
		expect(this.daoFactoryMock.getNotePosterDao()).andReturn(notePosterDAO);
		expect(this.daoFactoryMock.getNoteSoutenanceDAO()).andReturn(noteSoutenanceDAO);
		expect(this.daoFactoryMock.getPosterDao()).andReturn(posterDAO);
		expect(this.daoFactoryMock.getProfesseurSujetDAO()).andReturn(professeurSujetDAO);
		expect(this.daoFactoryMock.getPglEtudiantEquipeDAO()).andReturn(etudiantEquipeDAO);
		expect(this.daoFactoryMock.getBonusMalusDAO()).andReturn(bonusMalusDAO);
		expect(this.daoFactoryMock.getDemandeJuryDAO()).andReturn(demandeJuryDAO);
		
		/* Récupération de la session depuis la requête */
		expect(this.httpServletRequestMock.getSession()).andReturn(this.httpSessionMock);

		/* Récupération d'attributs de l'Utilisateur en session */
		Role role = new Role();
		role.setIdRole(1L);
		role.setNomRole("etudiant");
		List<Role> roles = new ArrayList<>();
		roles.add(role);
		expect(this.httpSessionMock.getAttribute(ATT_SESSION_ROLES)).andReturn(roles);
		OptionESEO option = new OptionESEO();
		option.setIdOption(1L);
		option.setNomOption("LD");
		List<OptionESEO> options = new ArrayList<>();
		options.add(option);
		expect(this.httpSessionMock.getAttribute(ATT_SESSION_OPTIONS)).andReturn(options);

		/* Récupération de l'ID de l'Utilisateur en session */
		final Utilisateur utilisateur = new Utilisateur();
		final Long idUtilisateur = ID_ETUDIANT_SEUL;
		utilisateur.setIdUtilisateur(idUtilisateur);
		expect(this.httpSessionMock.getAttribute(ATT_SESSION_USER)).andReturn(utilisateur);

		/* A la réception d'une requête GET, simple affichage du formulaire */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getRequestDispatcher(VUE_FORM)).andReturn(this.requestDispatcherMock);
		this.requestDispatcherMock.forward(this.httpServletRequestMock, this.httpservletResponseMock);
		expectLastCall();

		this.replayAll();

		/* Cycle de vie d'une servlet */
		this.dashboard.init(this.servletConfigMock);
		this.dashboard.doGet(this.httpServletRequestMock, this.httpservletResponseMock);
		this.dashboard.destroy();

		this.verifyAll();
	}

	/**
	 * Teste la méthode protected void doGet(HttpServletRequest request, HttpServletResponse response).
	 *
	 * <p>Cas d'un utilisateur possédant le rôle "prof".</p>
	 *
	 * @throws ServletException
	 * @throws IOException
	 */
	@Test
	public void testDoGetProf() throws ServletException, IOException {
		/* Récupération d'instances de DAO */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
		expect(this.daoFactoryMock.getSujetDao()).andReturn(sujetDAO);
		expect(this.daoFactoryMock.getUtilisateurDao()).andReturn(utilisateurDAO);
		expect(this.daoFactoryMock.getEquipeDAO()).andReturn(equipeDAO);
		expect(this.daoFactoryMock.getOptionESEODAO()).andReturn(optionDAO);
		expect(this.daoFactoryMock.getProfesseurDAO()).andReturn(professeurDAO);
		expect(this.daoFactoryMock.getAnneeScolaireDAO()).andReturn(anneeScolaireDAO);
		expect(this.daoFactoryMock.getEtudiantDao()).andReturn(etudiantDAO);
		expect(this.daoFactoryMock.getJuryPosterDAO()).andReturn(juryPosterDAO);
		expect(this.daoFactoryMock.getJurySoutenanceDAO()).andReturn(jurySoutenanceDAO);
		expect(this.daoFactoryMock.getSoutenanceDAO()).andReturn(soutenanceDAO);
		expect(this.daoFactoryMock.getNotePosterDao()).andReturn(notePosterDAO);
		expect(this.daoFactoryMock.getNoteSoutenanceDAO()).andReturn(noteSoutenanceDAO);
		expect(this.daoFactoryMock.getPosterDao()).andReturn(posterDAO);
		expect(this.daoFactoryMock.getProfesseurSujetDAO()).andReturn(professeurSujetDAO);
		expect(this.daoFactoryMock.getPglEtudiantEquipeDAO()).andReturn(etudiantEquipeDAO);
		expect(this.daoFactoryMock.getBonusMalusDAO()).andReturn(bonusMalusDAO);
		expect(this.daoFactoryMock.getDemandeJuryDAO()).andReturn(demandeJuryDAO);
		
		/* Récupération de la session depuis la requête */
		expect(this.httpServletRequestMock.getSession()).andReturn(this.httpSessionMock);

		/* Récupération d'attributs de l'Utilisateur en session */
		Role role = new Role();
		role.setIdRole(3L);
		role.setNomRole("prof");
		List<Role> roles = new ArrayList<>();
		roles.add(role);
		expect(this.httpSessionMock.getAttribute(ATT_SESSION_ROLES)).andReturn(roles);
		OptionESEO option = new OptionESEO();
		option.setIdOption(1L);
		option.setNomOption("LD");
		List<OptionESEO> options = new ArrayList<>();
		options.add(option);
		expect(this.httpSessionMock.getAttribute(ATT_SESSION_OPTIONS)).andReturn(options);

		/* Récupération de l'ID de l'Utilisateur en session */
		final Utilisateur utilisateur = new Utilisateur();
		final Long idUtilisateur = ID_PROF;
		utilisateur.setIdUtilisateur(idUtilisateur);
		expect(this.httpSessionMock.getAttribute(ATT_SESSION_USER)).andReturn(utilisateur);

		/* A la réception d'une requête GET, simple affichage du formulaire */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getRequestDispatcher(VUE_FORM)).andReturn(this.requestDispatcherMock);
		this.requestDispatcherMock.forward(this.httpServletRequestMock, this.httpservletResponseMock);
		expectLastCall();

		this.replayAll();

		/* Cycle de vie d'une servlet */
		this.dashboard.init(this.servletConfigMock);
		this.dashboard.doGet(this.httpServletRequestMock, this.httpservletResponseMock);
		this.dashboard.destroy();

		this.verifyAll();
	}

	/**
	 * Teste la méthode protected void doGet(HttpServletRequest request, HttpServletResponse response).
	 *
	 * <p>Cas d'un utilisateur possédant le rôle "profOption".</p>
	 *
	 * @throws ServletException
	 * @throws IOException
	 */
	@Test
	public void testDoGetProfOption() throws ServletException, IOException {
		/* Récupération d'instances de DAO */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
		expect(this.daoFactoryMock.getSujetDao()).andReturn(sujetDAO);
		expect(this.daoFactoryMock.getUtilisateurDao()).andReturn(utilisateurDAO);
		expect(this.daoFactoryMock.getEquipeDAO()).andReturn(equipeDAO);
		expect(this.daoFactoryMock.getOptionESEODAO()).andReturn(optionDAO);
		expect(this.daoFactoryMock.getProfesseurDAO()).andReturn(professeurDAO);
		expect(this.daoFactoryMock.getAnneeScolaireDAO()).andReturn(anneeScolaireDAO);
		expect(this.daoFactoryMock.getEtudiantDao()).andReturn(etudiantDAO);
		expect(this.daoFactoryMock.getJuryPosterDAO()).andReturn(juryPosterDAO);
		expect(this.daoFactoryMock.getJurySoutenanceDAO()).andReturn(jurySoutenanceDAO);
		expect(this.daoFactoryMock.getSoutenanceDAO()).andReturn(soutenanceDAO);
		expect(this.daoFactoryMock.getNotePosterDao()).andReturn(notePosterDAO);
		expect(this.daoFactoryMock.getNoteSoutenanceDAO()).andReturn(noteSoutenanceDAO);
		expect(this.daoFactoryMock.getPosterDao()).andReturn(posterDAO);
		expect(this.daoFactoryMock.getProfesseurSujetDAO()).andReturn(professeurSujetDAO);
		expect(this.daoFactoryMock.getPglEtudiantEquipeDAO()).andReturn(etudiantEquipeDAO);
		expect(this.daoFactoryMock.getBonusMalusDAO()).andReturn(bonusMalusDAO);
		expect(this.daoFactoryMock.getDemandeJuryDAO()).andReturn(demandeJuryDAO);
		
		/* Récupération de la session depuis la requête */
		expect(this.httpServletRequestMock.getSession()).andReturn(this.httpSessionMock);

		/* Récupération d'attributs de l'Utilisateur en session */
		Role role = new Role();
		role.setIdRole(4L);
		role.setNomRole("profOption");
		List<Role> roles = new ArrayList<>();
		roles.add(role);
		expect(this.httpSessionMock.getAttribute(ATT_SESSION_ROLES)).andReturn(roles);
		OptionESEO option = new OptionESEO();
		option.setIdOption(1L);
		option.setNomOption("LD");
		List<OptionESEO> options = new ArrayList<>();
		options.add(option);
		expect(this.httpSessionMock.getAttribute(ATT_SESSION_OPTIONS)).andReturn(options);

		/* Récupération de l'ID de l'Utilisateur en session */
		final Utilisateur utilisateur = new Utilisateur();
		final Long idUtilisateur = ID_PROF_OPTION;
		utilisateur.setIdUtilisateur(idUtilisateur);
		expect(this.httpSessionMock.getAttribute(ATT_SESSION_USER)).andReturn(utilisateur);

		/* A la réception d'une requête GET, simple affichage du formulaire */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getRequestDispatcher(VUE_FORM)).andReturn(this.requestDispatcherMock);
		this.requestDispatcherMock.forward(this.httpServletRequestMock, this.httpservletResponseMock);
		expectLastCall();

		this.replayAll();

		/* Cycle de vie d'une servlet */
		this.dashboard.init(this.servletConfigMock);
		this.dashboard.doGet(this.httpServletRequestMock, this.httpservletResponseMock);
		this.dashboard.destroy();

		this.verifyAll();
	}

	/**
	 * Teste la méthode protected void doGet(HttpServletRequest request, HttpServletResponse response).
	 *
	 * <p>Cas d'un utilisateur possédant le rôle "profResponsable".</p>
	 *
	 * @throws ServletException
	 * @throws IOException
	 */
	@Test
	public void testDoGetProfResp() throws ServletException, IOException {
		/* Récupération d'instances de DAO */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
		expect(this.daoFactoryMock.getSujetDao()).andReturn(sujetDAO);
		expect(this.daoFactoryMock.getUtilisateurDao()).andReturn(utilisateurDAO);
		expect(this.daoFactoryMock.getEquipeDAO()).andReturn(equipeDAO);
		expect(this.daoFactoryMock.getOptionESEODAO()).andReturn(optionDAO);
		expect(this.daoFactoryMock.getProfesseurDAO()).andReturn(professeurDAO);
		expect(this.daoFactoryMock.getAnneeScolaireDAO()).andReturn(anneeScolaireDAO);
		expect(this.daoFactoryMock.getEtudiantDao()).andReturn(etudiantDAO);
		expect(this.daoFactoryMock.getJuryPosterDAO()).andReturn(juryPosterDAO);
		expect(this.daoFactoryMock.getJurySoutenanceDAO()).andReturn(jurySoutenanceDAO);
		expect(this.daoFactoryMock.getSoutenanceDAO()).andReturn(soutenanceDAO);
		expect(this.daoFactoryMock.getNotePosterDao()).andReturn(notePosterDAO);
		expect(this.daoFactoryMock.getNoteSoutenanceDAO()).andReturn(noteSoutenanceDAO);
		expect(this.daoFactoryMock.getPosterDao()).andReturn(posterDAO);
		expect(this.daoFactoryMock.getProfesseurSujetDAO()).andReturn(professeurSujetDAO);
		expect(this.daoFactoryMock.getPglEtudiantEquipeDAO()).andReturn(etudiantEquipeDAO);
		expect(this.daoFactoryMock.getBonusMalusDAO()).andReturn(bonusMalusDAO);
		expect(this.daoFactoryMock.getDemandeJuryDAO()).andReturn(demandeJuryDAO);
		
		/* Récupération de la session depuis la requête */
		expect(this.httpServletRequestMock.getSession()).andReturn(this.httpSessionMock);

		/* Récupération d'attributs de l'Utilisateur en session */
		Role role = new Role();
		role.setIdRole(5L);
		role.setNomRole("profResponsable");
		List<Role> roles = new ArrayList<>();
		roles.add(role);
		expect(this.httpSessionMock.getAttribute(ATT_SESSION_ROLES)).andReturn(roles);
		OptionESEO option = new OptionESEO();
		option.setIdOption(1L);
		option.setNomOption("LD");
		List<OptionESEO> options = new ArrayList<>();
		options.add(option);
		expect(this.httpSessionMock.getAttribute(ATT_SESSION_OPTIONS)).andReturn(options);

		/* Récupération de l'ID de l'Utilisateur en session */
		final Utilisateur utilisateur = new Utilisateur();
		final Long idUtilisateur = ID_PROF_RESPONSABLE;
		utilisateur.setIdUtilisateur(idUtilisateur);
		expect(this.httpSessionMock.getAttribute(ATT_SESSION_USER)).andReturn(utilisateur);

		/* A la réception d'une requête GET, simple affichage du formulaire */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getRequestDispatcher(VUE_FORM)).andReturn(this.requestDispatcherMock);
		this.requestDispatcherMock.forward(this.httpServletRequestMock, this.httpservletResponseMock);
		expectLastCall();

		this.replayAll();

		/* Cycle de vie d'une servlet */
		this.dashboard.init(this.servletConfigMock);
		this.dashboard.doGet(this.httpServletRequestMock, this.httpservletResponseMock);
		this.dashboard.destroy();

		this.verifyAll();
	}

}