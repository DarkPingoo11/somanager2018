package fr.eseo.ld.servlets;

import fr.eseo.ld.servlets.ajax.InfosUtilisateurTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

/**
 * Suite de tests unitaires JUnit 4 pour le package fr.eseo.ld.servlets.
 *
 * @author Maxime LENORMAND
 *
 * @see org.junit
 */
@RunWith(Suite.class)
@SuiteClasses({
		AjouterSujetTest.class,
		ChangerMotDePasseTest.class,
		ChangerMotDePasseAdminTest.class,
		CommenterTest.class,
		//ConnexionTest.class,
		ConsulterSujetTest.class,
		CreerEquipesI2Test.class,
		CreerEquipeTest.class,
		CreerJuryTest.class,

		DashboardTest.class,
		DeconnexionTest.class,
		//DeposerPosterTest.class,
		DepotGoogleTest.class,

		EvaluerInteretTest.class,
		ExporterNotesTest.class,

		GererApplicationTest.class,
		GererComptesTest.class,
		GererEquipesTest.class,
		GererMeetingsTest.class,
		GererNotesTest.class,
		//GererProfesseursTest.class,
		GererRolesTest.class,
		GererSujetsTest.class,
		GestionProjetsEtPFETest.class,

        InscriptionProfSujetTest.class,
		InscriptionTest.class,
		InfosUtilisateurTest.class,

		//ModifierPosterTest.class,

		NoterTest.class,

		PartagerRoleTest.class,

		ServletUtilitaireTest.class,

		//TelechargerPostersOptionTest.class,
		//TelechargerPosterTest.class,
		TrombinoscopeTest.class,
		VoirMesSujetsTest.class,
})
public class ServletsTestSuite {
	/* Classe vide */
}