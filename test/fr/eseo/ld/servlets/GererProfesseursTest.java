package fr.eseo.ld.servlets;

import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.expectLastCall;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.easymock.EasyMockRunner;
import org.easymock.EasyMockSupport;
import org.easymock.Mock;
import org.easymock.MockType;
import org.easymock.TestSubject;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

import fr.eseo.ld.beans.AnneeScolaire;
import fr.eseo.ld.beans.Equipe;
import fr.eseo.ld.beans.EtatSujet;
import fr.eseo.ld.beans.Etudiant;
import fr.eseo.ld.beans.EtudiantEquipe;
import fr.eseo.ld.beans.FonctionProfesseurSujet;
import fr.eseo.ld.beans.OptionESEO;
import fr.eseo.ld.beans.Poster;
import fr.eseo.ld.beans.Professeur;
import fr.eseo.ld.beans.ProfesseurSujet;
import fr.eseo.ld.beans.Role;
import fr.eseo.ld.beans.RoleUtilisateur;
import fr.eseo.ld.beans.Sujet;
import fr.eseo.ld.beans.Utilisateur;
import fr.eseo.ld.dao.AnneeScolaireDAO;
import fr.eseo.ld.dao.DAOFactory;
import fr.eseo.ld.dao.EquipeDAO;
import fr.eseo.ld.dao.EtudiantDAO;
import fr.eseo.ld.dao.NotificationDAO;
import fr.eseo.ld.dao.OptionESEODAO;
import fr.eseo.ld.dao.PosterDAO;
import fr.eseo.ld.dao.ProfesseurDAO;
import fr.eseo.ld.dao.ProfesseurSujetDAO;
import fr.eseo.ld.dao.RoleDAO;
import fr.eseo.ld.dao.SujetDAO;
import fr.eseo.ld.dao.UtilisateurDAO;

/**
 * Classe de tests unitaires JUnit 4 de la classe GererProfesseurs.
 *
 * <p>Utilisation de mocks (construits avec EasyMock).</p>
 * 
 *
 * @version 1.0
 * @author Maxime LENORMAND
 *
 * @see fr.eseo.ld.servlets.GererProfesseurs
 * @see org.junit
 * @see org.easymock
 */
@RunWith(EasyMockRunner.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class GererProfesseursTest extends EasyMockSupport {
	
	private static final String CONF_DAO_FACTORY = "daoFactory";
	
	private static final String VUE_FORM = "/WEB-INF/formulaires/gererProfesseurs.jsp";
	

	private static ProfesseurSujetDAO professeurSujetDAO;
	private static PosterDAO posterDAO;
	private static NotificationDAO notificationDAO;
	private static EquipeDAO equipeDAO;
	private static Utilisateur utilisateurTest;
	private static Etudiant etudiantTest;
	private static Sujet sujetTest;
	private static Equipe equipeTest;
	private static UtilisateurDAO utilisateurDAO;
	private static EtudiantDAO etudiantDAO;
	private static SujetDAO sujetDAO;
	private static EtudiantEquipe etudiantEquipeTest ;
	private static OptionESEODAO optionESEODAO;
	private static OptionESEO optionESEOTest;
	private static AnneeScolaireDAO anneeScolaireDAO;
	private static AnneeScolaire anneeTest;
	private static Poster posterTest;
	private static RoleDAO roleDAO;
	private static Role roleTest;
	private static RoleUtilisateur roleUtilisateurTest;
	private static ProfesseurDAO professeurDAO;
	private static Professeur professeurTest;

	private static Utilisateur utilisateurTest2;

	private static Professeur professeurTest2;

	private static Sujet sujetTest2;

	private static ProfesseurSujet profSujet;

	private static ProfesseurSujet profSujet2;

	@TestSubject
	private GererProfesseurs gererProfesseurs = new GererProfesseurs();
	
	@Mock
	private ServletConfig servletConfigMock;
	
	@Mock
	private ServletContext servletContextMock;
	
	@Mock(type = MockType.NICE)
	private HttpServletRequest httpServletRequestMock;
	
	@Mock
	private HttpServletResponse httpservletResponseMock;
	
	@Mock
	private HttpSession httpSessionMock;
	
	@Mock
	private RequestDispatcher requestDispatcherMock;
	
	@Mock
	private DAOFactory daoFactoryMock;

	/**
	 * Récupère des instances de DAO.
	 * 
	 * @throws Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		DAOFactory daoFactory = DAOFactory.getInstance();
		utilisateurDAO = daoFactory.getUtilisateurDao();
		sujetDAO = daoFactory.getSujetDao();
		notificationDAO = daoFactory.getNotificationDao();
		professeurSujetDAO = daoFactory.getProfesseurSujetDAO();
		posterDAO = daoFactory.getPosterDao();
		equipeDAO = daoFactory.getEquipeDAO();
		etudiantDAO = daoFactory.getEtudiantDao();
		optionESEODAO = daoFactory.getOptionESEODAO();
		anneeScolaireDAO = daoFactory.getAnneeScolaireDAO();
		roleDAO = daoFactory.getRoleDAO();
		professeurDAO = daoFactory.getProfesseurDAO();
		
		/*Création des données de test*/
		
		/* Insertion d'un utilisateur test dans la BDD */
        Utilisateur utilisateur = new Utilisateur();
        utilisateur.setIdentifiant("test");
        utilisateur.setNom("test");
        utilisateur.setPrenom("test");
        utilisateur.setValide("oui");
        utilisateurDAO.creer(utilisateur);
        utilisateurTest = utilisateurDAO.trouver(utilisateur).get(0);
        
        Professeur professeur = new Professeur();
        professeur.setIdProfesseur(utilisateurTest.getIdUtilisateur());;
        professeurDAO.creer(professeur);
        professeurTest = professeurDAO.trouver(professeur).get(0);
        
        Utilisateur utilisateur2 = new Utilisateur();
        utilisateur2.setIdentifiant("test2");
        utilisateur2.setNom("test2");
        utilisateur2.setPrenom("test2");
        utilisateur2.setValide("oui");
        utilisateurDAO.creer(utilisateur2);
        utilisateurTest2 = utilisateurDAO.trouver(utilisateur2).get(0);
        
        Professeur professeur2 = new Professeur();
        professeur2.setIdProfesseur(utilisateurTest2.getIdUtilisateur());;
        professeurDAO.creer(professeur2);
        professeurTest2 = professeurDAO.trouver(professeur2).get(0);
        
		Sujet sujet = new Sujet();
		sujet.setTitre("sujetTest");
		sujet.setDescription("description");
		sujet.setNbrMinEleves(1);
		sujet.setNbrMaxEleves(4);
		sujet.setEtat(EtatSujet.ATTRIBUE);
		sujetDAO.creer(sujet);
		sujetTest = sujetDAO.trouver(sujet).get(0);
		
		Sujet sujet2 = new Sujet();
		sujet2.setTitre("sujetTest2");
		sujet2.setDescription("description2");
		sujet2.setNbrMinEleves(1);
		sujet2.setNbrMaxEleves(4);
		sujet2.setEtat(EtatSujet.ATTRIBUE);
		sujetDAO.creer(sujet2);
		sujetTest2 = sujetDAO.trouver(sujet2).get(0);
		
		OptionESEO option = new OptionESEO();
		option.setIdOption(1L);
		option.setNomOption("LD");
		optionESEODAO.creer(option);
		optionESEOTest = optionESEODAO.trouver(option).get(0);
		
		AnneeScolaire annee = new AnneeScolaire();
		annee.setAnneeDebut(2018);
		annee.setAnneeFin(2019);
		annee.setIdOption(1L);
		anneeScolaireDAO.creer(annee);
		anneeTest = anneeScolaireDAO.trouver(annee).get(0);
        
        Role role = new Role();
        role.setIdRole(3L);
        role.setNomRole("prof");
        role.setType("applicatif");
        roleDAO.creerRole(role);
        roleTest = roleDAO.trouver(role).get(0);
        
        utilisateurDAO.associerRoleOption(utilisateurTest, roleTest, optionESEOTest);
        utilisateurDAO.associerRoleOption(utilisateurTest2, roleTest, optionESEOTest);
        
        profSujet = new ProfesseurSujet();
        profSujet.setIdProfesseur(professeurTest.getIdProfesseur());
        profSujet.setIdSujet(sujetTest.getIdSujet());
        profSujet.setValide("non");
        profSujet.setFonction(FonctionProfesseurSujet.REFERENT);
        professeurSujetDAO.creer(profSujet);
        
        profSujet2 = new ProfesseurSujet();
        profSujet2.setIdProfesseur(professeurTest2.getIdProfesseur());
        profSujet2.setIdSujet(sujetTest2.getIdSujet());
        profSujet2.setValide("non");
        profSujet2.setFonction(FonctionProfesseurSujet.REFERENT);
        professeurSujetDAO.creer(profSujet2);
	}
	
	/**
     * Supprime les données de test dans la BDD.
     *
     * @throws Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
    	professeurSujetDAO.supprimer(profSujet);
    	professeurSujetDAO.supprimer(profSujet2);
    	utilisateurDAO.supprimerRoleOption(utilisateurTest, roleTest, optionESEOTest);
    	utilisateurDAO.supprimerRoleOption(utilisateurTest2, roleTest, optionESEOTest);
    	roleDAO.supprimer(roleTest);
    	anneeScolaireDAO.supprimer(anneeTest);
    	optionESEODAO.supprimer(optionESEOTest);
    	sujetDAO.supprimer(sujetTest);
    	sujetDAO.supprimer(sujetTest2);
    	professeurDAO.supprimer(professeurTest);
        utilisateurDAO.supprimer(utilisateurTest);
        professeurDAO.supprimer(professeurTest2);
        utilisateurDAO.supprimer(utilisateurTest2);
    }

	/**
	 * Teste la méthode protected void doGet(HttpServletRequest request, HttpServletResponse response).
	 * 
	 * @throws ServletException
	 * @throws IOException
	 */
	@Test
	public void test1DoGet() throws ServletException, IOException {
		/* Récupération d'instances de DAO */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
		expect(this.daoFactoryMock.getSujetDao()).andReturn(sujetDAO);
		expect(this.daoFactoryMock.getNotificationDao()).andReturn(notificationDAO);
		expect(this.daoFactoryMock.getUtilisateurDao()).andReturn(utilisateurDAO);

		/* Affichage du formulaire */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getRequestDispatcher(VUE_FORM)).andReturn(this.requestDispatcherMock);
		this.requestDispatcherMock.forward(this.httpServletRequestMock, this.httpservletResponseMock);
		expectLastCall();

		this.replayAll();

		/* Cycle de vie d'une servlet */
		this.gererProfesseurs.init(this.servletConfigMock);
		this.gererProfesseurs.doGet(this.httpServletRequestMock, this.httpservletResponseMock);
		this.gererProfesseurs.destroy();

		this.verifyAll();
	}
	
	/**
	 * Teste la méthode protected void doGet(HttpServletRequest request, HttpServletResponse response).
	 * Test du cas de refus d'un référent
	 * 
	 * @throws ServletException
	 * @throws IOException
	 */
	@Test
	public void test2DoPostRefuserReferent() throws ServletException, IOException {
		/* Récupération d'instances de DAO */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
		expect(this.daoFactoryMock.getSujetDao()).andReturn(sujetDAO);
		expect(this.daoFactoryMock.getNotificationDao()).andReturn(notificationDAO);
		expect(this.daoFactoryMock.getUtilisateurDao()).andReturn(utilisateurDAO);

		/* Affichage du formulaire */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getRequestDispatcher(VUE_FORM)).andReturn(this.requestDispatcherMock);
		this.requestDispatcherMock.forward(this.httpServletRequestMock, this.httpservletResponseMock);
		expectLastCall();

		/* création des expect requete */
		Map<String, String[]> parameters = new HashMap<>();
		parameters.put("optradio-0", new String[]{"coucou","coucou"});
		expect(this.httpServletRequestMock.getParameterMap()).andReturn(parameters);
		expect(this.httpServletRequestMock.getParameter("formulaire")).andReturn("validerReferent");
		expectLastCall().times(2);
		expect(this.httpServletRequestMock.getParameter("optradio-0")).andReturn("false");
		expectLastCall();
		
		this.replayAll();

		/* Cycle de vie d'une servlet */
		this.gererProfesseurs.init(this.servletConfigMock);
		this.gererProfesseurs.doPost(this.httpServletRequestMock, this.httpservletResponseMock);
		this.gererProfesseurs.destroy();

		this.verifyAll();
	}
	
	/**
	 * Teste la méthode protected void doGet(HttpServletRequest request, HttpServletResponse response).
	 * Test du cas de validation d'un référent
	 * 
	 * @throws ServletException
	 * @throws IOException
	 */
	@Test
	public void test3DoPostValiderReferent() throws ServletException, IOException {
		/* Récupération d'instances de DAO */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
		expect(this.daoFactoryMock.getSujetDao()).andReturn(sujetDAO);
		expect(this.daoFactoryMock.getNotificationDao()).andReturn(notificationDAO);
		expect(this.daoFactoryMock.getUtilisateurDao()).andReturn(utilisateurDAO);

		/* Affichage du formulaire */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getRequestDispatcher(VUE_FORM)).andReturn(this.requestDispatcherMock);
		this.requestDispatcherMock.forward(this.httpServletRequestMock, this.httpservletResponseMock);
		expectLastCall();

		/* création des expect requete */
		Map<String, String[]> parameters = new HashMap<>();
		parameters.put("optradio-0", new String[]{"coucou","coucou"});
		expect(this.httpServletRequestMock.getParameterMap()).andReturn(parameters);
		expect(this.httpServletRequestMock.getParameter("formulaire")).andReturn("validerReferent");
		expectLastCall().times(2);
		expect(this.httpServletRequestMock.getParameter("optradio-0")).andReturn("true");
		
		this.replayAll();

		/* Cycle de vie d'une servlet */
		this.gererProfesseurs.init(this.servletConfigMock);
		this.gererProfesseurs.doPost(this.httpServletRequestMock, this.httpservletResponseMock);
		this.gererProfesseurs.destroy();

		this.verifyAll();
	}
	
	/**
	 * Teste la méthode protected void doGet(HttpServletRequest request, HttpServletResponse response).
	 * Test du cas de suppression d'une fonction
	 * 
	 * @throws ServletException
	 * @throws IOException
	 */
	@Test
	public void test4DoPostSupprimerFonction() throws ServletException, IOException {
		/* Récupération d'instances de DAO */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
		expect(this.daoFactoryMock.getSujetDao()).andReturn(sujetDAO);
		expect(this.daoFactoryMock.getNotificationDao()).andReturn(notificationDAO);
		expect(this.daoFactoryMock.getUtilisateurDao()).andReturn(utilisateurDAO);

		/* Affichage du formulaire */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getRequestDispatcher(VUE_FORM)).andReturn(this.requestDispatcherMock);
		this.requestDispatcherMock.forward(this.httpServletRequestMock, this.httpservletResponseMock);
		expectLastCall();

		/* création des expect requete */
		Map<String, String[]> parameters = new HashMap<>();
		parameters.put("optradio-2", new String[]{"coucou","coucou"});
		expect(this.httpServletRequestMock.getParameterMap()).andReturn(parameters);
		expect(this.httpServletRequestMock.getParameter("formulaire")).andReturn("supprimerFonction");
		expectLastCall().times(3);
		expectLastCall();
		
		this.replayAll();

		/* Cycle de vie d'une servlet */
		this.gererProfesseurs.init(this.servletConfigMock);
		this.gererProfesseurs.doPost(this.httpServletRequestMock, this.httpservletResponseMock);
		this.gererProfesseurs.destroy();

		this.verifyAll();
	}
	
	/**
	 * Teste la méthode protected void doGet(HttpServletRequest request, HttpServletResponse response).
	 * Test du cas d'attribution d'un référent
	 * 
	 * @throws ServletException
	 * @throws IOException
	 */
	@Test
	public void test5DoPostAttribuerReferent() throws ServletException, IOException {
		/* Récupération d'instances de DAO */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
		expect(this.daoFactoryMock.getSujetDao()).andReturn(sujetDAO);
		expect(this.daoFactoryMock.getNotificationDao()).andReturn(notificationDAO);
		expect(this.daoFactoryMock.getUtilisateurDao()).andReturn(utilisateurDAO);

		/* Affichage du formulaire */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getRequestDispatcher(VUE_FORM)).andReturn(this.requestDispatcherMock);
		this.requestDispatcherMock.forward(this.httpServletRequestMock, this.httpservletResponseMock);
		expectLastCall();

		/* création des expect requete */
		Map<String, String[]> parameters = new HashMap<>();
		parameters.put("idReferent-" + sujetTest.getIdSujet(), new String[]{"coucou","coucou"});
		expect(this.httpServletRequestMock.getParameterMap()).andReturn(parameters);
		expect(this.httpServletRequestMock.getParameter("formulaire")).andReturn("attribuerReferent");
		expect(this.httpServletRequestMock.getParameter("idReferent-"  + sujetTest.getIdSujet())).andReturn(professeurTest.getIdProfesseur().toString());
		
		this.replayAll();

		/* Cycle de vie d'une servlet */
		this.gererProfesseurs.init(this.servletConfigMock);
		this.gererProfesseurs.doPost(this.httpServletRequestMock, this.httpservletResponseMock);
		this.gererProfesseurs.destroy();

		this.verifyAll();
	}
	
	

}