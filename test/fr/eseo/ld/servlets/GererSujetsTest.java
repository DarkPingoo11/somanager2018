package fr.eseo.ld.servlets;

import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.expectLastCall;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.easymock.EasyMockRunner;
import org.easymock.EasyMockSupport;
import org.easymock.Mock;
import org.easymock.MockType;
import org.easymock.TestSubject;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

import fr.eseo.ld.beans.EtatSujet;
import fr.eseo.ld.beans.OptionESEO;
import fr.eseo.ld.beans.Sujet;
import fr.eseo.ld.beans.Utilisateur;
import fr.eseo.ld.dao.CommentaireDAO;
import fr.eseo.ld.dao.DAOFactory;
import fr.eseo.ld.dao.NotificationDAO;
import fr.eseo.ld.dao.OptionESEODAO;
import fr.eseo.ld.dao.SujetDAO;
import fr.eseo.ld.dao.UtilisateurDAO;

/**
 * Classe de tests unitaires JUnit 4 de la classe GererSujets.
 *
 * <p>Utilisation de mocks (construits avec EasyMock).</p>
 *
 * @version 1.0
 * @author Maxime LENORMAND
 *
 * @see fr.eseo.ld.servlets.GererSujets
 * @see org.junit
 * @see org.easymock
 */
@RunWith(EasyMockRunner.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class GererSujetsTest extends EasyMockSupport {
	
	private static final String CONF_DAO_FACTORY = "daoFactory";
	
	private static final String ATT_SESSION_USER = "utilisateur";
	private static final Long ID_UTILISATEUR = 16L;
	
	private static final String VUE_FORM = "/WEB-INF/formulaires/gererSujets.jsp";

	private static final Long ID_SUJET_1 = 4L;

	private static final Long ID_SUJET_2 = 5L;

	private static final Long ID_SUJET_3 = 3L;
	
	private static UtilisateurDAO utilisateurDAO;
	private static SujetDAO sujetDAO;
	private static OptionESEODAO optionESEODAO;
	private static NotificationDAO notificationDAO;
	private static CommentaireDAO commentaireDAO;
	
	@TestSubject
	private GererSujets gererSujets = new GererSujets();
	
	@Mock
	private ServletConfig servletConfigMock;
	
	@Mock
	private ServletContext servletContextMock;
	
	@Mock(type = MockType.NICE)
	private HttpServletRequest httpServletRequestMock;
	
	@Mock(type = MockType.NICE)
	private HttpServletResponse httpservletResponseMock;
	
	@Mock
	private HttpSession httpSessionMock;
	
	@Mock
	private RequestDispatcher requestDispatcherMock;
	
	@Mock
	private DAOFactory daoFactoryMock;

	/**
	 * Récupère des instances de DAO.
	 * 
	 * @throws Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		DAOFactory daoFactory = DAOFactory.getInstance();
		utilisateurDAO = daoFactory.getUtilisateurDao();
		sujetDAO = daoFactory.getSujetDao();
		optionESEODAO = daoFactory.getOptionESEODAO();
		notificationDAO = daoFactory.getNotificationDao();
		commentaireDAO = daoFactory.getCommentaireDao();
	}
	
	/**
	 * Supprime les sujets inséré dans la BDD.
	 * 
	 * @throws Exception
	 */
	@AfterClass
	public static void tearDownAfterClass()throws Exception {
		Sujet sujet1 = sujetDAO.trouver(ID_SUJET_1);
		Sujet sujet2 = sujetDAO.trouver(ID_SUJET_2);
		Sujet sujet3 = sujetDAO.trouver(ID_SUJET_3);
		sujet1.setEtat(EtatSujet.DEPOSE);
		sujet2.setEtat(EtatSujet.DEPOSE);
		sujet3.setEtat(EtatSujet.VALIDE);
		sujetDAO.modifier(sujet1);;
		sujetDAO.modifier(sujet2);
		sujetDAO.modifier(sujet3);
	}

	/**
	 * Teste la méthode protected void doGet(HttpServletRequest request, HttpServletResponse response).
	 * 
	 * @throws ServletException
	 * @throws IOException
	 */
	@Test
	public void testDoGet() throws ServletException, IOException {
		/* Récupération d'instances de DAO */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
		expect(this.daoFactoryMock.getUtilisateurDao()).andReturn(utilisateurDAO);
		expect(this.daoFactoryMock.getSujetDao()).andReturn(sujetDAO);
		expect(this.daoFactoryMock.getOptionESEODAO()).andReturn(optionESEODAO);
		expect(this.daoFactoryMock.getNotificationDao()).andReturn(notificationDAO);
		expect(this.daoFactoryMock.getCommentaireDao()).andReturn(commentaireDAO);
		
		/* Récupération de l'Utilisateur en session */
		expect(this.httpServletRequestMock.getSession()).andReturn(this.httpSessionMock);
		final Utilisateur utilisateur = new Utilisateur();
		final Long idUtilisateur = ID_UTILISATEUR;
		utilisateur.setIdUtilisateur(idUtilisateur);
		expect(this.httpSessionMock.getAttribute(ATT_SESSION_USER)).andReturn(utilisateur);
		
		/* Affichage du formulaire */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getRequestDispatcher(VUE_FORM)).andReturn(this.requestDispatcherMock);
		this.requestDispatcherMock.forward(this.httpServletRequestMock, this.httpservletResponseMock);
		expectLastCall();

		this.replayAll();

		/* Cycle de vie d'une servlet */
		this.gererSujets.init(this.servletConfigMock);
		this.gererSujets.doGet(this.httpServletRequestMock, this.httpservletResponseMock);
		this.gererSujets.destroy();

		this.verifyAll();
	}
	
	/**
	 * Teste la méthode protected void doPost(HttpServletRequest request, HttpServletResponse response).
	 * cas validation sujet
	 * 
	 * @throws ServletException
	 * @throws IOException
	 */
	@Test
	public void testDoPostValider() throws ServletException, IOException {
		/* Récupération d'instances de DAO */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
		expect(this.daoFactoryMock.getUtilisateurDao()).andReturn(utilisateurDAO);
		expect(this.daoFactoryMock.getSujetDao()).andReturn(sujetDAO);
		expect(this.daoFactoryMock.getOptionESEODAO()).andReturn(optionESEODAO);
		expect(this.daoFactoryMock.getNotificationDao()).andReturn(notificationDAO);
		expect(this.daoFactoryMock.getCommentaireDao()).andReturn(commentaireDAO);
		
		/* Récupération de l'Utilisateur en session */
		expect(this.httpServletRequestMock.getSession()).andReturn(this.httpSessionMock);
		final Utilisateur utilisateur = new Utilisateur();
		final Long idUtilisateur = ID_UTILISATEUR;
		utilisateur.setIdUtilisateur(idUtilisateur);
		expect(this.httpSessionMock.getAttribute(ATT_SESSION_USER)).andReturn(utilisateur);
		
		/* scpect reuete */
		expect(this.httpServletRequestMock.getParameter("idSujet")).andReturn(String.valueOf(ID_SUJET_1));
		expect(this.httpServletRequestMock.getParameter("actionValiderRefuser")).andReturn("valider");

		this.replayAll();

		/* Cycle de vie d'une servlet */
		this.gererSujets.init(this.servletConfigMock);
		this.gererSujets.doPost(this.httpServletRequestMock, this.httpservletResponseMock);
		this.gererSujets.destroy();

		this.verifyAll();
	}
	
	/**
	 * Teste la méthode protected void doPost(HttpServletRequest request, HttpServletResponse response).
	 * cas refus sujet
	 * 
	 * @throws ServletException
	 * @throws IOException
	 */
	@Test
	public void testDoPostRefuser() throws ServletException, IOException {
		/* Récupération d'instances de DAO */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
		expect(this.daoFactoryMock.getUtilisateurDao()).andReturn(utilisateurDAO);
		expect(this.daoFactoryMock.getSujetDao()).andReturn(sujetDAO);
		expect(this.daoFactoryMock.getOptionESEODAO()).andReturn(optionESEODAO);
		expect(this.daoFactoryMock.getNotificationDao()).andReturn(notificationDAO);
		expect(this.daoFactoryMock.getCommentaireDao()).andReturn(commentaireDAO);
		
		/* Récupération de l'Utilisateur en session */
		expect(this.httpServletRequestMock.getSession()).andReturn(this.httpSessionMock);
		final Utilisateur utilisateur = new Utilisateur();
		final Long idUtilisateur = ID_UTILISATEUR;
		utilisateur.setIdUtilisateur(idUtilisateur);
		expect(this.httpSessionMock.getAttribute(ATT_SESSION_USER)).andReturn(utilisateur);
		
		/* scpect reuete */
		expect(this.httpServletRequestMock.getParameter("idSujet")).andReturn(String.valueOf(ID_SUJET_2));
		expect(this.httpServletRequestMock.getParameter("actionValiderRefuser")).andReturn("refuser");

		this.replayAll();

		/* Cycle de vie d'une servlet */
		this.gererSujets.init(this.servletConfigMock);
		this.gererSujets.doPost(this.httpServletRequestMock, this.httpservletResponseMock);
		this.gererSujets.destroy();

		this.verifyAll();
	}
	
	/**
	 * Teste la méthode protected void doPost(HttpServletRequest request, HttpServletResponse response).
	 * cas publication sujet
	 * 
	 * @throws ServletException
	 * @throws IOException
	 */
	@Test
	public void testDoPostPublier() throws ServletException, IOException {
		/* Récupération d'instances de DAO */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
		expect(this.daoFactoryMock.getUtilisateurDao()).andReturn(utilisateurDAO);
		expect(this.daoFactoryMock.getSujetDao()).andReturn(sujetDAO);
		expect(this.daoFactoryMock.getOptionESEODAO()).andReturn(optionESEODAO);
		expect(this.daoFactoryMock.getNotificationDao()).andReturn(notificationDAO);
		expect(this.daoFactoryMock.getCommentaireDao()).andReturn(commentaireDAO);
		
		/* Récupération de l'Utilisateur en session */
		expect(this.httpServletRequestMock.getSession()).andReturn(this.httpSessionMock);
		final Utilisateur utilisateur = new Utilisateur();
		final Long idUtilisateur = ID_UTILISATEUR;
		utilisateur.setIdUtilisateur(idUtilisateur);
		expect(this.httpSessionMock.getAttribute(ATT_SESSION_USER)).andReturn(utilisateur);
		
		/* scpect reuete */
		expect(this.httpServletRequestMock.getParameter("idSujet")).andReturn(String.valueOf(ID_SUJET_3));
		

		this.replayAll();

		/* Cycle de vie d'une servlet */
		this.gererSujets.init(this.servletConfigMock);
		this.gererSujets.doPost(this.httpServletRequestMock, this.httpservletResponseMock);
		this.gererSujets.destroy();

		this.verifyAll();
	}
	
}