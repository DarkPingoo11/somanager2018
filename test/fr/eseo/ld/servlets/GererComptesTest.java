package fr.eseo.ld.servlets;

import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.expectLastCall;
import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.eseo.ld.dao.*;
import org.easymock.EasyMockRunner;
import org.easymock.EasyMockSupport;
import org.easymock.Mock;
import org.easymock.MockType;
import org.easymock.TestSubject;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

import fr.eseo.ld.beans.Utilisateur;

/**
 * Classe de tests unitaires JUnit 4 de la classe GererComptes.
 *
 * <p>Utilisation de mocks (construits avec EasyMock).</p>
 *
 * @version 1.0
 * @author Maxime LENORMAND
 *
 * @see fr.eseo.ld.servlets.GererComptes
 * @see org.junit
 * @see org.easymock
 */
@RunWith(EasyMockRunner.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class GererComptesTest extends EasyMockSupport {
	
	private static final String CONF_DAO_FACTORY = "daoFactory";
	
	private static final String TEST = "testutilisateur";
	private static final String TEST_AUTRE = "test2";
	private static final Long ID_UTILISATEUR_TEST = 11L;
	
	private static final String ATT_SESSION_USER = "utilisateur";
	private static final String ATT_UTILISATEURS = "utilisateurs";
	private static final String ATT_RESULTAT = "resultat";
	private static final String ATT_ERREURS = "erreurs";
	private static final String ATT_FORMULAIRE = "formulaire";
	private static final String ATT_VALIDERCOMPTE = "valider";
	
	private static final String CHAMP_ID_UTILISATEUR = "idUtilisateur";
	private static final String CHAMP_NOM = "nom";
	private static final String CHAMP_PRENOM = "prenom";
	private static final String CHAMP_IDENTIFIANT = "identifiant";
	private static final String CHAMP_MOT_DE_PASSE = "motDePasse";
	private static final String CHAMP_EMAIL = "email";

	private static final String MSG_SUCCESS_COMPTE = "{\"type\":\"success\",\"message\":\"Le compte de " +
			TEST.toUpperCase() + " " + TEST + " a bien été validé\"}";
	
	private static final String VUE_FORM = "/WEB-INF/formulaires/gererComptes.jsp";
	
	private static Utilisateur utilisateur;
	private static UtilisateurDAO utilisateurDAO;
	private static NotificationDAO notificationDAO;
	private static AnneeScolaireDAO anneeScolaireDAO;
	private static RoleDAO roleDAO;

	@TestSubject
	private GererComptes gererComptes = new GererComptes();
	
	@Mock
	private ServletConfig servletConfigMock;
	
	@Mock
	private ServletContext servletContextMock;
	
	@Mock(type = MockType.NICE)
	private HttpServletRequest httpServletRequestMock;
	
	@Mock
	private HttpServletResponse httpservletResponseMock;
	
	@Mock
	private HttpSession httpSessionMock;
	
	@Mock
	private RequestDispatcher requestDispatcherMock;
	
	@Mock
	private DAOFactory daoFactoryMock;

	@Mock
	private PrintWriter printWriter;

	/**
	 * Instancie des DAO et le bean utilisateur.
	 * 
	 * @throws Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		/* Instanciations de DAO */
		final DAOFactory daoFactoy = DAOFactory.getInstance();
		utilisateurDAO = daoFactoy.getUtilisateurDao();
		notificationDAO = daoFactoy.getNotificationDao();
		anneeScolaireDAO = daoFactoy.getAnneeScolaireDAO();
		roleDAO = daoFactoy.getRoleDAO();
		
		/* Instanciation du bean utilisateur */
		utilisateur = new Utilisateur();
	}
	
	/**
	 * Supprime l'utilisateur inséré dans la BDD.
	 * 
	 * @throws Exception
	 */
	@AfterClass
	public static void tearDownAfterClass()throws Exception {
		utilisateurDAO.supprimer(utilisateur);
	}

	/**
	 * Teste la méthode protected void doGet(HttpServletRequest request, HttpServletResponse response).
	 * 
	 * @throws ServletException
	 * @throws IOException
	 */
	@Test
	public void test1DoGet() throws ServletException, IOException {
		/* Récupération d'instances de DAO */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
		expect(this.daoFactoryMock.getUtilisateurDao()).andReturn(utilisateurDAO);
		expect(this.daoFactoryMock.getNotificationDao()).andReturn(notificationDAO);
		expect(this.daoFactoryMock.getAnneeScolaireDAO()).andReturn(anneeScolaireDAO);
		expect(this.daoFactoryMock.getRoleDAO()).andReturn(roleDAO);

		/* Chargement de la liste des utilisateurs non validés */
		expectLastCall();
		
		/* A la réception d'une requête GET, simple affichage du formulaire */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getRequestDispatcher(VUE_FORM)).andReturn(this.requestDispatcherMock);
		this.requestDispatcherMock.forward(this.httpServletRequestMock, this.httpservletResponseMock);
		expectLastCall();

		this.replayAll();

		/* Cycle de vie d'une servlet */
		this.gererComptes.init(this.servletConfigMock);
		this.gererComptes.doGet(this.httpServletRequestMock, this.httpservletResponseMock);
		this.gererComptes.destroy();

		this.verifyAll();
	}
	
	/**
	 * Teste la méthode protected void doPost(HttpServletRequest request, HttpServletResponse response).
	 * 
	 * <p>Cas du succès de l'inscription.</p>
	 * 
	 * @throws ServletException
	 * @throws IOException
	 */
	@Test
	public void test2_1DoPostAjouterUtilisateurSucces() throws ServletException, IOException {
		/* Récupération d'une instance de DAO Utilisateur */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
		expect(this.daoFactoryMock.getUtilisateurDao()).andReturn(utilisateurDAO);
		expect(this.daoFactoryMock.getNotificationDao()).andReturn(notificationDAO);
		expect(this.daoFactoryMock.getAnneeScolaireDAO()).andReturn(anneeScolaireDAO);
		expect(this.daoFactoryMock.getRoleDAO()).andReturn(roleDAO);
		
		/* Récupération de l'utilisateur en session */
		final Utilisateur utilisateurSession = new Utilisateur();
		final Long idUtilisateurSession = ID_UTILISATEUR_TEST;
		utilisateurSession.setIdUtilisateur(idUtilisateurSession);
		expect(this.httpServletRequestMock.getSession()).andReturn(this.httpSessionMock);
		expect(this.httpSessionMock.getAttribute(ATT_SESSION_USER)).andReturn(utilisateurSession);
		
		/* Récupération des données saisies */
		expect(this.httpServletRequestMock.getParameter(ATT_FORMULAIRE)).andReturn("ajouterUtilisateur");
		expect(this.httpServletRequestMock.getParameter(CHAMP_NOM)).andReturn(TEST);
		expect(this.httpServletRequestMock.getParameter(CHAMP_PRENOM)).andReturn(TEST);
		expect(this.httpServletRequestMock.getParameter(CHAMP_IDENTIFIANT)).andReturn(TEST);
		expect(this.httpServletRequestMock.getParameter(CHAMP_MOT_DE_PASSE)).andReturn(TEST);
		expect(this.httpServletRequestMock.getParameter(CHAMP_EMAIL)).andReturn(TEST);
		
		/* Mise en place du resultat dans la requete */
		final String resultat = "Succès de l'inscription.";
		this.httpServletRequestMock.setAttribute(ATT_RESULTAT, resultat);
		expectLastCall();

		/* Ré-affichage du formulaire avec message de succès */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getRequestDispatcher(VUE_FORM)).andReturn(this.requestDispatcherMock);
		this.requestDispatcherMock.forward(this.httpServletRequestMock, this.httpservletResponseMock);
		expectLastCall();
		
		this.replayAll();
		
		/* Cycle de vie d'une servlet */
		this.gererComptes.init(this.servletConfigMock);
		this.gererComptes.doPost(this.httpServletRequestMock, this.httpservletResponseMock);
		this.gererComptes.destroy();
		
		/* Vérification de l'insertion de l'utilisateur dans la BDD */
		final String identifiantAttendu = TEST;
		utilisateur.setIdentifiant(identifiantAttendu);
		final List<Utilisateur> utilisateursTrouves = utilisateurDAO.trouver(utilisateur);
		
		final Long idUtilisateur = utilisateursTrouves.get(0).getIdUtilisateur();
		utilisateur.setIdUtilisateur(idUtilisateur); // ajout de la clé primaire générée au bean
		
		String identifiantTrouve = "";
		if (!utilisateursTrouves.isEmpty()) {
			identifiantTrouve = utilisateursTrouves.get(0).getIdentifiant();
		}
		assertEquals("L'utilisateur n'a pas été inséré dans la BDD", identifiantAttendu, identifiantTrouve);
		
		this.verifyAll();
	}
	
	/**
	 * Teste la méthode protected void doPost(HttpServletRequest request, HttpServletResponse response).
	 * 
	 * <p>Cas de l'échec de l'inscription : identifiant déjà utilisé.</p>
	 * 
	 * @throws ServletException
	 * @throws IOException
	 */
	@Test
	public void test2_2DoPostAjouterUtilisateurEchecIdentifiant() throws ServletException, IOException {
		/* Récupération d'une instance de DAO Utilisateur */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
		expect(this.daoFactoryMock.getUtilisateurDao()).andReturn(utilisateurDAO);
		expect(this.daoFactoryMock.getNotificationDao()).andReturn(notificationDAO);
		expect(this.daoFactoryMock.getAnneeScolaireDAO()).andReturn(anneeScolaireDAO);
		expect(this.daoFactoryMock.getRoleDAO()).andReturn(roleDAO);
		
		/* Récupération de l'utilisateur en session */
		final Utilisateur utilisateurSession = new Utilisateur();
		final Long idUtilisateurSession = ID_UTILISATEUR_TEST;
		utilisateurSession.setIdUtilisateur(idUtilisateurSession);
		expect(this.httpServletRequestMock.getSession()).andReturn(this.httpSessionMock);
		expect(this.httpSessionMock.getAttribute(ATT_SESSION_USER)).andReturn(utilisateurSession);
		
		/* Récupération des données saisies */
		expect(this.httpServletRequestMock.getParameter(ATT_FORMULAIRE)).andReturn("ajouterUtilisateur");
		expect(this.httpServletRequestMock.getParameter(CHAMP_IDENTIFIANT)).andReturn(TEST);
		expect(this.httpServletRequestMock.getParameter(CHAMP_EMAIL)).andReturn(TEST_AUTRE);
		
		/* Mise en place du resultat dans la requete */
		final Map<String, String> erreurs = new HashMap<>();
		erreurs.put(CHAMP_IDENTIFIANT, "Identifiant déjà utilisé. ");
		final String resultat = "Échec de l'inscription.";
		this.httpServletRequestMock.setAttribute(ATT_RESULTAT, resultat);
		expectLastCall();
		this.httpServletRequestMock.setAttribute(ATT_ERREURS, erreurs);
		expectLastCall();

		/* Ré-affichage du formulaire avec message de succès */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getRequestDispatcher(VUE_FORM)).andReturn(this.requestDispatcherMock);
		this.requestDispatcherMock.forward(this.httpServletRequestMock, this.httpservletResponseMock);
		expectLastCall();
		
		this.replayAll();
		
		/* Cycle de vie d'une servlet */
		this.gererComptes.init(this.servletConfigMock);
		this.gererComptes.doPost(this.httpServletRequestMock, this.httpservletResponseMock);
		this.gererComptes.destroy();
		
		/* Vérification que l'utilisateur n'a pas été inséré dans la BDD */
		final int nbUtilisateursAttendus = 1; // un seul : l'utilisateur inséré lors du test2_1
		final int nbUtilisateursTrouves = utilisateurDAO.trouver(utilisateur).size();
		assertEquals("L'utilisateur a été inséré dans la BDD", nbUtilisateursAttendus, nbUtilisateursTrouves);
		
		this.verifyAll();
	}
	
	/**
	 * Teste la méthode protected void doPost(HttpServletRequest request, HttpServletResponse response).
	 * 
	 * <p>Cas de l'échec de l'inscription : email déjà utilisé.</p>
	 * 
	 * @throws ServletException
	 * @throws IOException
	 */
	@Test
	public void test2_3DoPostAjouterUtilisateurEchecEmail() throws ServletException, IOException {
		/* Récupération d'une instance de DAO Utilisateur */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
		expect(this.daoFactoryMock.getUtilisateurDao()).andReturn(utilisateurDAO);
		expect(this.daoFactoryMock.getNotificationDao()).andReturn(notificationDAO);
		expect(this.daoFactoryMock.getAnneeScolaireDAO()).andReturn(anneeScolaireDAO);
		expect(this.daoFactoryMock.getRoleDAO()).andReturn(roleDAO);
		
		/* Récupération de l'utilisateur en session */
		final Utilisateur utilisateurSession = new Utilisateur();
		final Long idUtilisateurSession = ID_UTILISATEUR_TEST;
		utilisateurSession.setIdUtilisateur(idUtilisateurSession);
		expect(this.httpServletRequestMock.getSession()).andReturn(this.httpSessionMock);
		expect(this.httpSessionMock.getAttribute(ATT_SESSION_USER)).andReturn(utilisateurSession);
		
		/* Récupération des données saisies */
		expect(this.httpServletRequestMock.getParameter(ATT_FORMULAIRE)).andReturn("ajouterUtilisateur");
		expect(this.httpServletRequestMock.getParameter(CHAMP_IDENTIFIANT)).andReturn(TEST_AUTRE);
		expect(this.httpServletRequestMock.getParameter(CHAMP_EMAIL)).andReturn(TEST);
		
		/* Mise en place du resultat dans la requete */
		final Map<String, String> erreurs = new HashMap<>();
		erreurs.put(CHAMP_EMAIL, "Email déjà utilisé. ");
		final String resultat = "Échec de l'inscription.";
		this.httpServletRequestMock.setAttribute(ATT_RESULTAT, resultat);
		expectLastCall();
		this.httpServletRequestMock.setAttribute(ATT_ERREURS, erreurs);
		expectLastCall();

		/* Ré-affichage du formulaire avec message de succès */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getRequestDispatcher(VUE_FORM)).andReturn(this.requestDispatcherMock);
		this.requestDispatcherMock.forward(this.httpServletRequestMock, this.httpservletResponseMock);
		expectLastCall();
		
		this.replayAll();
		
		/* Cycle de vie d'une servlet */
		this.gererComptes.init(this.servletConfigMock);
		this.gererComptes.doPost(this.httpServletRequestMock, this.httpservletResponseMock);
		this.gererComptes.destroy();
		
		/* Vérification que l'utilisateur n'a pas été inséré dans la BDD */
		final int nbUtilisateursAttendus = 1; // un seul : l'utilisateur inséré lors du test2_1
		final int nbUtilisateursTrouves = utilisateurDAO.trouver(utilisateur).size();
		assertEquals("L'utilisateur a été inséré dans la BDD", nbUtilisateursAttendus, nbUtilisateursTrouves);
		
		this.verifyAll();
	}
	
	/**
	 * Teste la méthode protected void doPost(HttpServletRequest request, HttpServletResponse response).
	 * 
	 * <p>Cas de la validation d'un compte.</p>
	 * 
	 * @throws ServletException
	 * @throws IOException
	 */
	@Test
	public void test3DoPostValiderCompte() throws ServletException, IOException {
		/* Récupération d'une instance de DAO Utilisateur */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
		expect(this.daoFactoryMock.getUtilisateurDao()).andReturn(utilisateurDAO);
		expect(this.daoFactoryMock.getNotificationDao()).andReturn(notificationDAO);
		expect(this.daoFactoryMock.getAnneeScolaireDAO()).andReturn(anneeScolaireDAO);
		expect(this.daoFactoryMock.getRoleDAO()).andReturn(roleDAO);
		
		/* Récupération de l'utilisateur en session */
		final Utilisateur utilisateurSession = new Utilisateur();
		final Long idUtilisateurSession = ID_UTILISATEUR_TEST;
		utilisateurSession.setIdUtilisateur(idUtilisateurSession);
		expect(this.httpServletRequestMock.getSession()).andReturn(this.httpSessionMock);
		expect(this.httpSessionMock.getAttribute(ATT_SESSION_USER)).andReturn(utilisateurSession);
		
		/* Récupération des données saisies */
		expect(this.httpServletRequestMock.getParameter(ATT_VALIDERCOMPTE)).andReturn("oui");
		expectLastCall();
		expect(this.httpServletRequestMock.getParameter(ATT_FORMULAIRE)).andReturn("validerCompte");
		expectLastCall();
		expect(this.httpServletRequestMock.getParameter(CHAMP_ID_UTILISATEUR))
				.andReturn(utilisateur.getIdUtilisateur().toString());
		this.printWriter.flush();
		expectLastCall();

		/* Définition de la réponse */
		this.printWriter.print(MSG_SUCCESS_COMPTE);
		expectLastCall();

		/* Définition du format de la réponse */
		this.httpservletResponseMock.setContentType("application/json");
		expectLastCall();
		expect(this.httpservletResponseMock.getWriter()).andReturn(printWriter);


		/* Ré-affichage du formulaire avec message de succès */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getRequestDispatcher(VUE_FORM)).andReturn(this.requestDispatcherMock);
		this.requestDispatcherMock.forward(this.httpServletRequestMock, this.httpservletResponseMock);
		expectLastCall();
		
		this.replayAll();
		
		/* Modification de la validité de l'utilisateur dans la BDD */
		utilisateur.setValide("non");
		utilisateurDAO.modifier(utilisateur);

		/* Cycle de vie d'une servlet */
		this.gererComptes.init(this.servletConfigMock);
		this.gererComptes.doPost(this.httpServletRequestMock, this.httpservletResponseMock);
		this.gererComptes.destroy();
		
		/* Vérification de la modification de l'utilisateur dans la BDD */
		Long idUtilisateur = utilisateur.getIdUtilisateur();
		utilisateur = new Utilisateur();
		utilisateur.setIdUtilisateur(idUtilisateur);
		
		final String valideAttendu = "oui";
		System.out.println(utilisateurDAO.trouver(utilisateur).get(0));

		final String valideTrouve = utilisateurDAO.trouver(utilisateur).get(0).getValide();
		assertEquals("L'utilisateur a été modifié dans la BDD", valideAttendu, valideTrouve);
		
		//this.verifyAll();
	}
	
	/**
	 * Teste la méthode protected void doPost(HttpServletRequest request, HttpServletResponse response).
	 * 
	 * <p>Cas de la synchronisation avec la BDD.</p>
	 * 
	 * @throws ServletException
	 * @throws IOException
	 */
	@Test
	public void test4DoPostSynchronisation() throws ServletException, IOException {
		/* Récupération d'une instance de DAO Utilisateur */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
		expect(this.daoFactoryMock.getUtilisateurDao()).andReturn(utilisateurDAO);
		expect(this.daoFactoryMock.getNotificationDao()).andReturn(notificationDAO);
		expect(this.daoFactoryMock.getAnneeScolaireDAO()).andReturn(anneeScolaireDAO);
		expect(this.daoFactoryMock.getRoleDAO()).andReturn(roleDAO);
		
		/* Récupération de l'utilisateur en session */
		final Utilisateur utilisateurSession = new Utilisateur();
		final Long idUtilisateurSession = ID_UTILISATEUR_TEST;
		utilisateurSession.setIdUtilisateur(idUtilisateurSession);
		expect(this.httpServletRequestMock.getSession()).andReturn(this.httpSessionMock);
		expect(this.httpSessionMock.getAttribute(ATT_SESSION_USER)).andReturn(utilisateurSession);
		
		/* Récupération des données saisies */
		expect(this.httpServletRequestMock.getParameter(ATT_FORMULAIRE)).andReturn("synchroniserBDD");

		/* Ré-affichage du formulaire avec notification */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getRequestDispatcher(VUE_FORM)).andReturn(this.requestDispatcherMock);
		this.requestDispatcherMock.forward(this.httpServletRequestMock, this.httpservletResponseMock);
		expectLastCall();
		
		this.replayAll();
		
		/* Cycle de vie d'une servlet */
		this.gererComptes.init(this.servletConfigMock);
		this.gererComptes.doPost(this.httpServletRequestMock, this.httpservletResponseMock);
		this.gererComptes.destroy();
	
		this.verifyAll();
	}

}