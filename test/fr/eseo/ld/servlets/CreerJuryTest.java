package fr.eseo.ld.servlets;

import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.expectLastCall;

import java.io.IOException;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.easymock.EasyMockRunner;
import org.easymock.EasyMockSupport;
import org.easymock.Mock;
import org.easymock.MockType;
import org.easymock.TestSubject;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;

import fr.eseo.ld.beans.OptionESEO;
import fr.eseo.ld.beans.Role;
import fr.eseo.ld.dao.DAOFactory;
import fr.eseo.ld.dao.EquipeDAO;
import fr.eseo.ld.dao.JuryPosterDAO;
import fr.eseo.ld.dao.JurySoutenanceDAO;
import fr.eseo.ld.dao.OptionESEODAO;
import fr.eseo.ld.dao.PosterDAO;
import fr.eseo.ld.dao.ProfesseurDAO;
import fr.eseo.ld.dao.ProfesseurSujetDAO;
import fr.eseo.ld.dao.SoutenanceDAO;
import fr.eseo.ld.dao.SujetDAO;
import fr.eseo.ld.dao.UtilisateurDAO;

/**
 * Classe de tests unitaires JUnit 4 de la classe CreerJury.
 *
 * <p>Utilisation de mocks (construits avec EasyMock).</p>
 *
 * @version 1.0
 * @author Maxime LENORMAND
 *
 * @see fr.eseo.ld.servlets.CreerJury
 * @see org.junit
 * @see org.easymock
 */
@RunWith(EasyMockRunner.class)
public class CreerJuryTest extends EasyMockSupport {

	private static final String CONF_DAO_FACTORY = "daoFactory";
	
	private static final String[] PARAMETRES_LONG = { "3" };
	private static final String[] PARAMETRES_STRING = { "2017_001" };
	private static final String[] PARAMETRES_DATE = { "03-06-2018 14:14" };
	
	private static final String DATE_TEST = "2018-06-30 00:00:00";
	
	private static final String ATT_SESSION_ROLES = "roles";
	private static final String ATT_SESSION_OPTIONS = "options";
	
	private static final String HEADER = "referer";
	private static final String VUE_FORM = "/WEB-INF/formulaires/gererJurysPFE.jsp";
	
	private static SujetDAO sujetDAO;
	private static OptionESEODAO optionESEODAO;
	private static ProfesseurDAO professeurDAO;
	private static JuryPosterDAO juryPosterDAO;
	private static EquipeDAO equipeDAO;

	private static SoutenanceDAO soutenanceDAO;

	private static JurySoutenanceDAO jurySoutenanceDAO;

	private static PosterDAO posterDAO;

	private static UtilisateurDAO utilisateurDAO;

	private static ProfesseurSujetDAO professeurSujetDAO;

	@TestSubject
	private GererJuryPFE creerJury = new GererJuryPFE();
	
	@Mock
	private ServletConfig servletConfigMock;
	
	@Mock
	List<Long> idParticipantsMock;
	
	@Mock
	private ServletContext servletContextMock;
	
	@Mock(type = MockType.NICE)
	private HttpServletRequest httpServletRequestMock;
	
	@Mock
	private HttpServletResponse httpservletResponseMock;
	
	@Mock(type = MockType.NICE)
	private HttpSession httpSessionMock;
	
	@Mock
	private RequestDispatcher requestDispatcherMock;
	
	@Mock
	private DAOFactory daoFactoryMock;

	/**
	 * Récupère des instances de DAO.
	 * 
	 * @throws Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		DAOFactory daoFactory = DAOFactory.getInstance();
		sujetDAO = daoFactory.getSujetDao();
		optionESEODAO = daoFactory.getOptionESEODAO();
		professeurDAO = daoFactory.getProfesseurDAO();
		juryPosterDAO = daoFactory.getJuryPosterDAO();
		soutenanceDAO = daoFactory.getSoutenanceDAO();
		jurySoutenanceDAO = daoFactory.getJurySoutenanceDAO();
		equipeDAO = daoFactory.getEquipeDAO();
		posterDAO = daoFactory.getPosterDao();
		utilisateurDAO = daoFactory.getUtilisateurDao();
		professeurSujetDAO = daoFactory.getProfesseurSujetDAO();
	}

	/**
	 * Supprime l'utilisateur inséré dans la BDD.
	 * 
	 * @throws Exception
	 */
	@AfterClass
	public static void tearDownAfterClass()throws Exception {
		
	}

	/**
	 * Teste la méthode protected void doGet(HttpServletRequest request, HttpServletResponse response).
	 * 
	 * @throws ServletException
	 * @throws IOException
	 */
	@Test
	public void testDoGet() throws ServletException, IOException {
		/* Récupération d'instances de DAO */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
		expect(this.daoFactoryMock.getSujetDao()).andReturn(sujetDAO);
		expect(this.daoFactoryMock.getOptionESEODAO()).andReturn(optionESEODAO);
		expect(this.daoFactoryMock.getProfesseurDAO()).andReturn(professeurDAO);
		expect(this.daoFactoryMock.getJuryPosterDAO()).andReturn(juryPosterDAO);
		expect(this.daoFactoryMock.getSoutenanceDAO()).andReturn(soutenanceDAO);
		expect(this.daoFactoryMock.getJurySoutenanceDAO()).andReturn(jurySoutenanceDAO);
		expect(this.daoFactoryMock.getEquipeDAO()).andReturn(equipeDAO);
		expect(this.daoFactoryMock.getPosterDao()).andReturn(posterDAO);
		expect(this.daoFactoryMock.getUtilisateurDao()).andReturn(utilisateurDAO);
		expect(this.daoFactoryMock.getProfesseurSujetDAO()).andReturn(professeurSujetDAO);
		/* Récupération de la session depuis la requête */
		expect(this.httpServletRequestMock.getSession()).andReturn(this.httpSessionMock);
		
		/* Récupération d'attributs de l'Utilisateur en session */
		Role role = new Role();
		role.setIdRole(5L);
		role.setNomRole("profResponsable");
		List<Role> roles = new ArrayList<>();
		roles.add(role);
		expect(this.httpSessionMock.getAttribute(ATT_SESSION_ROLES)).andReturn(roles);
		OptionESEO option = new OptionESEO();
		option.setIdOption(1L);
		List<OptionESEO> options = new ArrayList<>();
		options.add(option);
		expect(this.httpSessionMock.getAttribute(ATT_SESSION_OPTIONS)).andReturn(options);
		
		/* Affichage du formulaire */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getRequestDispatcher(VUE_FORM)).andReturn(this.requestDispatcherMock);
		this.requestDispatcherMock.forward(this.httpServletRequestMock, this.httpservletResponseMock);
		expectLastCall();

		this.replayAll();

		/* Cycle de vie d'une servlet */
		this.creerJury.init(this.servletConfigMock);
		this.creerJury.doGet(this.httpServletRequestMock, this.httpservletResponseMock);
		this.creerJury.destroy();
	}

//	/**
//	 * Teste la méthode protected void doPost(HttpServletRequest request, HttpServletResponse response).
//	 * 
//	 * @throws ServletException
//	 * @throws IOException
//	 */
//	@Ignore
//	@Test
//	public void testDoPostFin() throws ServletException, IOException {
//		/* Récupération d'instances de DAO */
//		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
//		expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
//		expect(this.daoFactoryMock.getSujetDao()).andReturn(sujetDAO);
//		expect(this.daoFactoryMock.getOptionESEODAO()).andReturn(optionESEODAO);
//		expect(this.daoFactoryMock.getProfesseurDAO()).andReturn(professeurDAO);
//		expect(this.daoFactoryMock.getJuryPosterDAO()).andReturn(juryPosterDAO);
//		expect(this.daoFactoryMock.getSoutenanceDAO()).andReturn(soutenanceDAO);
//		expect(this.daoFactoryMock.getJurySoutenanceDAO()).andReturn(jurySoutenanceDAO);
//		expect(this.daoFactoryMock.getEquipeDAO()).andReturn(equipeDAO);
//		expect(this.daoFactoryMock.getPosterDao()).andReturn(posterDAO);
//		expect(this.daoFactoryMock.getUtilisateurDao()).andReturn(utilisateurDAO);
//		expect(this.daoFactoryMock.getProfesseurSujetDAO()).andReturn(professeurSujetDAO);
//		
//		expect(this.httpServletRequestMock.getParameter("formulaire")).andReturn("");
//		
//		this.httpServletRequestMock.setAttribute("onglet", "juryPoster");
//		expectLastCall();
//		
//		/* Récupération de la session depuis la requête */
//		expect(this.httpServletRequestMock.getSession()).andReturn(this.httpSessionMock);
//		
//		/* Récupération d'attributs de l'Utilisateur en session */
//		Role role = new Role();
//		role.setIdRole(5L);
//		role.setNomRole("profResponsable");
//		List<Role> roles = new ArrayList<>();
//		roles.add(role);
//		expect(this.httpSessionMock.getAttribute(ATT_SESSION_ROLES)).andReturn(roles);
//		OptionESEO option = new OptionESEO();
//		option.setIdOption(1L);
//		List<OptionESEO> options = new ArrayList<>();
//		options.add(option);
//		expect(this.httpSessionMock.getAttribute(ATT_SESSION_OPTIONS)).andReturn(options);
//		
//		/* Affichage du formulaire */
//		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
//		expect(this.servletContextMock.getRequestDispatcher(VUE_FORM)).andReturn(this.requestDispatcherMock);
//		this.requestDispatcherMock.forward(this.httpServletRequestMock, this.httpservletResponseMock);
//		expectLastCall();
//
//		this.replayAll();
//
//		/* Cycle de vie d'une servlet */
//		this.creerJury.init(this.servletConfigMock);
//		this.creerJury.doPost(this.httpServletRequestMock, this.httpservletResponseMock);
//		this.creerJury.destroy();
//
//		this.verifyAll();
//	}
//	
//	/**
//	 * Teste la méthode protected void doPost(HttpServletRequest request, HttpServletResponse response).
//	 * 
//	 * @throws ServletException
//	 * @throws IOException
//	 */
//	@Ignore
//	@Test
//	public void testDoPostPremierIf() throws ServletException, IOException {
//		List<Long> idParticipants = new ArrayList<>();
//		idParticipants.add(0L);
//		idParticipants.add(1L);
//		idParticipants.add(2L);
//		
//		/* Récupération d'instances de DAO */
//		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
//		expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
//		expect(this.daoFactoryMock.getSujetDao()).andReturn(sujetDAO);
//		expect(this.daoFactoryMock.getOptionESEODAO()).andReturn(optionESEODAO);
//		expect(this.daoFactoryMock.getProfesseurDAO()).andReturn(professeurDAO);
//		expect(this.daoFactoryMock.getJuryPosterDAO()).andReturn(juryPosterDAO);
//		expect(this.daoFactoryMock.getSoutenanceDAO()).andReturn(soutenanceDAO);
//		expect(this.daoFactoryMock.getJurySoutenanceDAO()).andReturn(jurySoutenanceDAO);
//		expect(this.daoFactoryMock.getEquipeDAO()).andReturn(equipeDAO);
//		expect(this.daoFactoryMock.getPosterDao()).andReturn(posterDAO);
//		expect(this.daoFactoryMock.getUtilisateurDao()).andReturn(utilisateurDAO);
//		expect(this.daoFactoryMock.getProfesseurSujetDAO()).andReturn(professeurSujetDAO);
//		
//		//this.creerJury = partialMockBuilder(GererJuryPFE.class).addMockedMethods("idParticipants","modifierJuryPoster").createMock();
//		
//		expect(this.httpServletRequestMock.getParameter("modifJuryPoster")).andReturn("modifierJuryP");
//		
//		expect(this.creerJury.idParticipants(httpServletRequestMock)).andReturn(idParticipants);
//		
//		expect(this.httpServletRequestMock.getParameter("numJuryPoster")).andReturn("1");
//		
//		expect(this.httpServletRequestMock.getParameter("datePopup")).andReturn(DATE_TEST);
//		
//		
//		//java.util.Date date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm").parse(DATE_TEST);
//		
////		this.creerJury.modifierJuryPoster(this.httpServletRequestMock, "1", idParticipants, date);
////		expectLastCall();
//		
//		this.httpServletRequestMock.setAttribute("onglet", "juryPoster");
//		expectLastCall();
//		
//		/* Récupération de la session depuis la requête */
//		expect(this.httpServletRequestMock.getSession()).andReturn(this.httpSessionMock);
//		
//		/* Récupération d'attributs de l'Utilisateur en session */
//		Role role = new Role();
//		role.setIdRole(5L);
//		role.setNomRole("profResponsable");
//		List<Role> roles = new ArrayList<>();
//		roles.add(role);
//		expect(this.httpSessionMock.getAttribute(ATT_SESSION_ROLES)).andReturn(roles);
//		OptionESEO option = new OptionESEO();
//		option.setIdOption(1L);
//		List<OptionESEO> options = new ArrayList<>();
//		options.add(option);
//		expect(this.httpSessionMock.getAttribute(ATT_SESSION_OPTIONS)).andReturn(options);
//		
//		/* Affichage du formulaire */
//		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
//		expect(this.servletContextMock.getRequestDispatcher(VUE_FORM)).andReturn(this.requestDispatcherMock);
//		this.requestDispatcherMock.forward(this.httpServletRequestMock, this.httpservletResponseMock);
//		expectLastCall();
//
//		this.replayAll();
//
//		/* Cycle de vie d'une servlet */
//		this.creerJury.init(this.servletConfigMock);
//		this.creerJury.doPost(this.httpServletRequestMock, this.httpservletResponseMock);
//		this.creerJury.destroy();
//
//		this.verifyAll();
//	}
	
}