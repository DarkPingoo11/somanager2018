package fr.eseo.ld.servlets;

import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.expectLastCall;
import static org.junit.Assert.assertEquals;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.eseo.ld.dao.*;
import org.easymock.EasyMockRunner;
import org.easymock.EasyMockSupport;
import org.easymock.Mock;
import org.easymock.MockType;
import org.easymock.TestSubject;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

import fr.eseo.ld.beans.Utilisateur;

/**
 * Classe de tests unitaires JUnit 4 de la classe GererRoles.
 *
 * <p>Utilisation de mocks (construits avec EasyMock).</p>
 *
 * @version 1.0
 * @author Maxime LENORMAND
 *
 * @see fr.eseo.ld.servlets.GererRoles
 * @see org.junit
 * @see org.easymock
 */
@RunWith(EasyMockRunner.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class GererRolesTest extends EasyMockSupport {
	
	private static final String CONF_DAO_FACTORY = "daoFactory";
	
	private static final String TEST = "test";
	
	private static final String ATT_SESSION_USER = "utilisateur";
	private static final String ATT_UTILISATEURS = "listeNomPrenomID";
	private static final String ATT_ROLES = "listeRoles";
	private static final String ATT_ROLES_UTILISATEUR = "listeRolesUtilisateur";
	private static final String ATT_OPTIONS = "listeOptions";
	private static final String ATT_OPTIONS_UTILISATEUR = "listeOptionsUtilisateur";
	private static final String ATT_FORMULAIRE = "formulaire";
	private static final String NOM_ROLE_ETUDIANT = "etudiant";
	private static final String ATT_UTILISATEUR_CHOISI = "utilisateurChoisi";
	private static final String ATT_UTILISATEUR_RECHERCHE = "utilisateurRecherche";
	private static final String ATT_CHOIX_VALIDATION = "choixValidation";
	private static final String ATT_RADIO_OPTION = "radioOption";
	private static final String ATT_RADIO_ROLE = "radioRole";
	private static final String ATT_OPTION_UTILISATEUR = "optionUtilisateur";
	private static final String ATT_ROLE_UTILISATEUR = "roleUtilisateur";

	private static final String[] ROLE_NECESSITANT_OPTION = { "etudiant", "prof", "profOption", "profResponsable",
			"entrepriseExt" };

	private static final String CHAMP_ERREUR_RECHERCHE = "recherche";
	private static final String CHAMP_ERREUR_ROLE_OPTION = "erreurRole";

	private static final String VUE_FORM = "/WEB-INF/formulaires/gererRoles.jsp";
	
	private static Utilisateur utilisateur;
	private static Utilisateur etudiant;
	private static Utilisateur etudiantPgl;
	private static UtilisateurDAO utilisateurDAO;
	private static EtudiantDAO etudiantDAO;
	private static RoleDAO roleDAO;
	private static OptionESEODAO optionESEODAO;
	private static AnneeScolaireDAO anneeScolaireDAO;
	private static NotificationDAO notificationDAO;
	private static PglEtudiantDAO pglEtudiantDAO;


	@TestSubject
	private GererRoles gererRoles = new GererRoles();
	
	@Mock
	private ServletConfig servletConfigMock;
	
	@Mock
	private ServletContext servletContextMock;
	
	@Mock(type = MockType.NICE)
	private HttpServletRequest httpServletRequestMock;
	
	@Mock
	private HttpServletResponse httpservletResponseMock;
	
	@Mock(type = MockType.NICE)
	private HttpSession httpSessionMock;
	
	@Mock
	private RequestDispatcher requestDispatcherMock;
	
	@Mock
	private DAOFactory daoFactoryMock;

	/**
	 * Instancie des DAO et Insère un utilisateur test.
	 * 
	 * @throws Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		/* Instanciations de DAO */
		final DAOFactory daoFactory = DAOFactory.getInstance();
		utilisateurDAO = daoFactory.getUtilisateurDao();
		etudiantDAO = daoFactory.getEtudiantDao();
		roleDAO = daoFactory.getRoleDAO();
		optionESEODAO = daoFactory.getOptionESEODAO();
		anneeScolaireDAO = daoFactory.getAnneeScolaireDAO();
		notificationDAO = daoFactory.getNotificationDao();
		pglEtudiantDAO = daoFactory.getPglEtudiantDAO();
		
		/* Insertion d'un utilisateur test dans la BDD */
		utilisateur = new Utilisateur();
		utilisateur.setIdentifiant(TEST);
		utilisateur.setNom(TEST);
		utilisateur.setPrenom(TEST);
		utilisateur.setValide("oui");
		utilisateurDAO.creer(utilisateur);
		
		/* Insertion d'un utilisateur test dans la BDD */
		etudiant = new Utilisateur();
		etudiant.setIdentifiant("coucou");
		etudiant.setNom("coucou");
		etudiant.setPrenom("coucou");
		etudiant.setValide("oui");
		utilisateurDAO.creer(etudiant);

		etudiantPgl = new Utilisateur();
		etudiantPgl.setIdentifiant("pgl");
		etudiantPgl.setNom("pgl");
		etudiantPgl.setPrenom("pgl");
		etudiantPgl.setValide("oui");
		utilisateurDAO.creer(etudiantPgl);


		/* Ajout de la clé primaire générée au bean */
		final Long idUtilisateur = utilisateurDAO.trouver(utilisateur).get(0).getIdUtilisateur();
		utilisateur.setIdUtilisateur(idUtilisateur);
		final Long idEtudiant = utilisateurDAO.trouver(etudiant).get(0).getIdUtilisateur();
		final Long idEtudiantPGl = utilisateurDAO.trouver(etudiantPgl).get(0).getIdUtilisateur();
		etudiant.setIdUtilisateur(idEtudiant);
		etudiantPgl.setIdUtilisateur(idEtudiantPGl);
	}
	
	
	/**
	 * Supprime l'utilisateur inséré dans la BDD.
	 * 
	 * @throws Exception
	 */
	@AfterClass
	public static void tearDownAfterClass()throws Exception {
		utilisateurDAO.supprimer(utilisateur);
		utilisateurDAO.supprimer(etudiant);
		utilisateurDAO.supprimer(etudiantPgl);
	}

	/**
	 * Teste la méthode protected void doGet(HttpServletRequest request, HttpServletResponse response).
	 * 
	 * @throws ServletException
	 * @throws IOException
	 */
	@Test
	public void test1DoGet() throws ServletException, IOException {
		/* Récupération d'instances de DAO */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
		expect(this.daoFactoryMock.getUtilisateurDao()).andReturn(utilisateurDAO);
		expect(this.daoFactoryMock.getEtudiantDao()).andReturn(etudiantDAO);
		expect(this.daoFactoryMock.getRoleDAO()).andReturn(roleDAO);
		expect(this.daoFactoryMock.getOptionESEODAO()).andReturn(optionESEODAO);
		expect(this.daoFactoryMock.getAnneeScolaireDAO()).andReturn(anneeScolaireDAO);
		expect(this.daoFactoryMock.getNotificationDao()).andReturn(notificationDAO);
		expect(this.daoFactoryMock.getPglEtudiantDAO()).andReturn(pglEtudiantDAO);

		/* Mise en place de la liste en session */
		expect(this.httpServletRequestMock.getSession()).andReturn(this.httpSessionMock);
		final StringBuilder liste = new StringBuilder();
		this.httpSessionMock.setAttribute(ATT_UTILISATEURS, liste);
		expectLastCall().asStub();
		
		/* Affichage du formulaire */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getRequestDispatcher(VUE_FORM)).andReturn(this.requestDispatcherMock);
		this.requestDispatcherMock.forward(this.httpServletRequestMock, this.httpservletResponseMock);
		expectLastCall();

		this.replayAll();

		/* Cycle de vie d'une servlet */
		this.gererRoles.init(this.servletConfigMock);
		this.gererRoles.doGet(this.httpServletRequestMock, this.httpservletResponseMock);
		this.gererRoles.destroy();

		this.verifyAll();
	}
	
	/**
	 * Teste la méthode protected void doPost(HttpServletRequest request, HttpServletResponse response).
	 * 
	 * <p>Cas du choix "chercherUtilisateur".</p>
	 * 
	 * @throws ServletException
	 * @throws IOException
	 */
	@Test
	public void test2_1DoPostChercherUtilisateur() throws ServletException, IOException {
		/* Récupération d'instances de DAO */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
		expect(this.daoFactoryMock.getUtilisateurDao()).andReturn(utilisateurDAO);
		expect(this.daoFactoryMock.getEtudiantDao()).andReturn(etudiantDAO);
		expect(this.daoFactoryMock.getRoleDAO()).andReturn(roleDAO);
		expect(this.daoFactoryMock.getOptionESEODAO()).andReturn(optionESEODAO);
		expect(this.daoFactoryMock.getAnneeScolaireDAO()).andReturn(anneeScolaireDAO);
		expect(this.daoFactoryMock.getNotificationDao()).andReturn(notificationDAO);
		expect(this.daoFactoryMock.getPglEtudiantDAO()).andReturn(pglEtudiantDAO);


		/* Récupération des données saisies */
		expect(this.httpServletRequestMock.getParameter(ATT_UTILISATEUR_CHOISI)).andReturn(utilisateur.getIdUtilisateur().toString());
		expect(this.httpServletRequestMock.getParameter(ATT_FORMULAIRE)).andReturn("chercherUtilisateur");
		expect(this.httpServletRequestMock.getParameter(ATT_UTILISATEUR_RECHERCHE)).andReturn("TEST TEST - test");
		
		/* Mise en place de l'utilisateur trouvé dans la requête */
		Utilisateur utilisateurChoisi = new Utilisateur();
		this.httpServletRequestMock.setAttribute(ATT_UTILISATEUR_CHOISI, utilisateurChoisi);
		expectLastCall().asStub();
		
		/* Affichage du formulaire */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getRequestDispatcher(VUE_FORM)).andReturn(this.requestDispatcherMock);
		this.requestDispatcherMock.forward(this.httpServletRequestMock, this.httpservletResponseMock);
		expectLastCall();

		this.replayAll();

		/* Cycle de vie d'une servlet */
		this.gererRoles.init(this.servletConfigMock);
		this.gererRoles.doPost(this.httpServletRequestMock, this.httpservletResponseMock);
		this.gererRoles.destroy();

		this.verifyAll();
	}
	
	/**
	 * Teste la méthode protected void doPost(HttpServletRequest request, HttpServletResponse response).
	 * 
	 * <p>Cas du choix "chercherUtilisateur" avec un utilisateur choisi nul.</p>
	 * 
	 * @throws ServletException
	 * @throws IOException
	 */
	@Test
	public void test2_2DoPostChercherUtilisateurUtilisateurChoisiNul() throws ServletException, IOException {
		/* Récupération d'instances de DAO */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
		expect(this.daoFactoryMock.getUtilisateurDao()).andReturn(utilisateurDAO);
		expect(this.daoFactoryMock.getEtudiantDao()).andReturn(etudiantDAO);
		expect(this.daoFactoryMock.getRoleDAO()).andReturn(roleDAO);
		expect(this.daoFactoryMock.getOptionESEODAO()).andReturn(optionESEODAO);
		expect(this.daoFactoryMock.getAnneeScolaireDAO()).andReturn(anneeScolaireDAO);
		expect(this.daoFactoryMock.getNotificationDao()).andReturn(notificationDAO);
		expect(this.daoFactoryMock.getPglEtudiantDAO()).andReturn(pglEtudiantDAO);


		/* Récupération des données saisies */
		expect(this.httpServletRequestMock.getParameter(ATT_UTILISATEUR_CHOISI)).andReturn(utilisateur.getIdUtilisateur().toString());
		expect(this.httpServletRequestMock.getParameter(ATT_FORMULAIRE)).andReturn("chercherUtilisateur");
		expect(this.httpServletRequestMock.getParameter(ATT_UTILISATEUR_RECHERCHE)).andReturn("");
		
		/* Mise en place de la liste en session */
		expect(this.httpServletRequestMock.getSession()).andReturn(this.httpSessionMock);
		StringBuilder liste = new StringBuilder();
		this.httpSessionMock.setAttribute(ATT_UTILISATEURS, liste);
		expectLastCall().asStub();
		
		/* Affichage du formulaire */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getRequestDispatcher(VUE_FORM)).andReturn(this.requestDispatcherMock);
		this.requestDispatcherMock.forward(this.httpServletRequestMock, this.httpservletResponseMock);
		expectLastCall();

		this.replayAll();

		/* Cycle de vie d'une servlet */
		this.gererRoles.init(this.servletConfigMock);
		this.gererRoles.doPost(this.httpServletRequestMock, this.httpservletResponseMock);
		this.gererRoles.destroy();

		this.verifyAll();
	}
	
	/**
	 * Teste la méthode protected void doPost(HttpServletRequest request, HttpServletResponse response).
	 * 
	 * <p>Cas du choix "ajouterAutorisation" pour un prof.</p>
	 * 
	 * @throws ServletException
	 * @throws IOException
	 */
	@Test
	public void test3_1DoPostAjouterAutorisationProf() throws ServletException, IOException {
		/* Récupération d'instances de DAO */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
		expect(this.daoFactoryMock.getUtilisateurDao()).andReturn(utilisateurDAO);
		expect(this.daoFactoryMock.getEtudiantDao()).andReturn(etudiantDAO);
		expect(this.daoFactoryMock.getRoleDAO()).andReturn(roleDAO);
		expect(this.daoFactoryMock.getOptionESEODAO()).andReturn(optionESEODAO);
		expect(this.daoFactoryMock.getAnneeScolaireDAO()).andReturn(anneeScolaireDAO);
		expect(this.daoFactoryMock.getNotificationDao()).andReturn(notificationDAO);
		expect(this.daoFactoryMock.getPglEtudiantDAO()).andReturn(pglEtudiantDAO);


		/* Récupération des données saisies */
		expect(this.httpServletRequestMock.getParameter(ATT_UTILISATEUR_CHOISI)).andReturn(utilisateur.getIdUtilisateur().toString());
		expect(this.httpServletRequestMock.getParameter(ATT_FORMULAIRE)).andReturn("ajouterAutorisation");
		expectLastCall().times(4);
		expect(this.httpServletRequestMock.getParameter(ATT_RADIO_OPTION)).andReturn("LD");
		expect(this.httpServletRequestMock.getParameter(ATT_RADIO_ROLE)).andReturn("prof");
		
		/* Récupération de l'utilisateur en session */
		expect(this.httpServletRequestMock.getSession()).andReturn(this.httpSessionMock);
		expect(this.httpSessionMock.getAttribute(ATT_SESSION_USER)).andReturn(utilisateur);
		
		/* Affichage du formulaire */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getRequestDispatcher(VUE_FORM)).andReturn(this.requestDispatcherMock);
		this.requestDispatcherMock.forward(this.httpServletRequestMock, this.httpservletResponseMock);
		expectLastCall();

		this.replayAll();

		/* Cycle de vie d'une servlet */
		this.gererRoles.init(this.servletConfigMock);
		this.gererRoles.doPost(this.httpServletRequestMock, this.httpservletResponseMock);
		this.gererRoles.destroy();
		
		/* Vérification de l'autorisation pour une option */
		final String optionAttendue = "LD";
		final String optionTrouvee = utilisateurDAO.trouverOption(utilisateur).get(0).getNomOption();
		assertEquals("Mauvaise option autorisée", optionAttendue, optionTrouvee);
		
		/* Vérification de l'autorisation pour un role */
		final String roleAttendu = "prof";
		final String roleTrouve = utilisateurDAO.trouverRole(utilisateur).get(0).getNomRole();
		assertEquals("Mauvais role autorisé", roleAttendu, roleTrouve);

		this.verifyAll();
	}
	
	/**
	 * Teste la méthode protected void doPost(HttpServletRequest request, HttpServletResponse response).
	 * 
	 * <p>Cas du choix "ajouterAutorisation" pour un etudiant.</p>
	 * 
	 * @throws ServletException
	 * @throws IOException
	 */
	@Test
	public void test3_2DoPostAjouterAutorisationEtudiant() throws ServletException, IOException {
		/* Récupération d'instances de DAO */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
		expect(this.daoFactoryMock.getUtilisateurDao()).andReturn(utilisateurDAO);
		expect(this.daoFactoryMock.getEtudiantDao()).andReturn(etudiantDAO);
		expect(this.daoFactoryMock.getRoleDAO()).andReturn(roleDAO);
		expect(this.daoFactoryMock.getOptionESEODAO()).andReturn(optionESEODAO);
		expect(this.daoFactoryMock.getAnneeScolaireDAO()).andReturn(anneeScolaireDAO);
		expect(this.daoFactoryMock.getNotificationDao()).andReturn(notificationDAO);
		expect(this.daoFactoryMock.getPglEtudiantDAO()).andReturn(pglEtudiantDAO);


		/* Récupération des données saisies */
		expect(this.httpServletRequestMock.getParameter(ATT_UTILISATEUR_CHOISI)).andReturn(etudiant.getIdUtilisateur().toString());
		expect(this.httpServletRequestMock.getParameter(ATT_FORMULAIRE)).andReturn("ajouterAutorisation");
		expectLastCall().times(4);
		expect(this.httpServletRequestMock.getParameter("radioContratPro")).andReturn("oui");
		expect(this.httpServletRequestMock.getParameter(ATT_RADIO_OPTION)).andReturn("LD");
		expect(this.httpServletRequestMock.getParameter(ATT_RADIO_ROLE)).andReturn("etudiant");
		
		/* Récupération de l'utilisateur en session */
		expect(this.httpServletRequestMock.getSession()).andReturn(this.httpSessionMock);
		expect(this.httpSessionMock.getAttribute(ATT_SESSION_USER)).andReturn(utilisateur);
		
		/* Affichage du formulaire */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getRequestDispatcher(VUE_FORM)).andReturn(this.requestDispatcherMock);
		this.requestDispatcherMock.forward(this.httpServletRequestMock, this.httpservletResponseMock);
		expectLastCall();

		this.replayAll();

		/* Cycle de vie d'une servlet */
		this.gererRoles.init(this.servletConfigMock);
		this.gererRoles.doPost(this.httpServletRequestMock, this.httpservletResponseMock);
		this.gererRoles.destroy();

//		this.verifyAll();
	}
	
	/**
	 * Teste la méthode protected void doPost(HttpServletRequest request, HttpServletResponse response).
	 * 
	 * <p>Cas du choix "ajouterAutorisation" avec un role déjà existant.</p>
	 * 
	 * @throws ServletException
	 * @throws IOException
	 */
	@Test
	public void test3_3DoPostAjouterAutorisationRoleDejaExistant() throws ServletException, IOException {
		/* Récupération d'instances de DAO */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
		expect(this.daoFactoryMock.getUtilisateurDao()).andReturn(utilisateurDAO);
		expect(this.daoFactoryMock.getEtudiantDao()).andReturn(etudiantDAO);
		expect(this.daoFactoryMock.getRoleDAO()).andReturn(roleDAO);
		expect(this.daoFactoryMock.getOptionESEODAO()).andReturn(optionESEODAO);
		expect(this.daoFactoryMock.getAnneeScolaireDAO()).andReturn(anneeScolaireDAO);
		expect(this.daoFactoryMock.getNotificationDao()).andReturn(notificationDAO);
		expect(this.daoFactoryMock.getPglEtudiantDAO()).andReturn(pglEtudiantDAO);


		/* Récupération des données saisies */
		expect(this.httpServletRequestMock.getParameter(ATT_UTILISATEUR_CHOISI)).andReturn(utilisateur.getIdUtilisateur().toString());
		expect(this.httpServletRequestMock.getParameter(ATT_FORMULAIRE)).andReturn("ajouterAutorisation");
		expectLastCall().times(4);
		expect(this.httpServletRequestMock.getParameter(ATT_RADIO_OPTION)).andReturn("");
		expect(this.httpServletRequestMock.getParameter(ATT_RADIO_ROLE)).andReturn("prof");
		
		/* Récupération de l'utilisateur en session */
		expect(this.httpServletRequestMock.getSession()).andReturn(this.httpSessionMock);
		expect(this.httpSessionMock.getAttribute(ATT_SESSION_USER)).andReturn(utilisateur);
		
		/* Affichage du formulaire */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getRequestDispatcher(VUE_FORM)).andReturn(this.requestDispatcherMock);
		this.requestDispatcherMock.forward(this.httpServletRequestMock, this.httpservletResponseMock);
		expectLastCall();

		this.replayAll();

		/* Cycle de vie d'une servlet */
		this.gererRoles.init(this.servletConfigMock);
		this.gererRoles.doPost(this.httpServletRequestMock, this.httpservletResponseMock);
		this.gererRoles.destroy();
		
		/* Vérification de l'autorisation pour une option */
		final String optionAttendue = "LD";
		final String optionTrouvee = utilisateurDAO.trouverOption(utilisateur).get(0).getNomOption();
		assertEquals("Mauvaise option autorisée", optionAttendue, optionTrouvee);
		
		/* Vérification de l'autorisation pour un role */
		final String roleAttendu = "prof";
		final String roleTrouve = utilisateurDAO.trouverRole(utilisateur).get(0).getNomRole();
		assertEquals("Mauvais role autorisé", roleAttendu, roleTrouve);

		this.verifyAll();
	}
	
	/**
	 * Teste la méthode protected void doPost(HttpServletRequest request, HttpServletResponse response).
	 * 
	 * <p>Cas du choix "ajouterAutorisation" sans option pour le role.</p>
	 * 
	 * @throws ServletException
	 * @throws IOException
	 */
	@Test
	public void test3_4DoPostAjouterAutorisationSansOption() throws ServletException, IOException {
		/* Récupération d'instances de DAO */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
		expect(this.daoFactoryMock.getUtilisateurDao()).andReturn(utilisateurDAO);
		expect(this.daoFactoryMock.getEtudiantDao()).andReturn(etudiantDAO);
		expect(this.daoFactoryMock.getRoleDAO()).andReturn(roleDAO);
		expect(this.daoFactoryMock.getOptionESEODAO()).andReturn(optionESEODAO);
		expect(this.daoFactoryMock.getAnneeScolaireDAO()).andReturn(anneeScolaireDAO);
		expect(this.daoFactoryMock.getNotificationDao()).andReturn(notificationDAO);
		expect(this.daoFactoryMock.getPglEtudiantDAO()).andReturn(pglEtudiantDAO);


		/* Récupération des données saisies */
		expect(this.httpServletRequestMock.getParameter(ATT_UTILISATEUR_CHOISI)).andReturn(utilisateur.getIdUtilisateur().toString());
		expect(this.httpServletRequestMock.getParameter(ATT_FORMULAIRE)).andReturn("ajouterAutorisation");
		expectLastCall().times(4);
		expect(this.httpServletRequestMock.getParameter(ATT_RADIO_OPTION)).andReturn(null);
		expect(this.httpServletRequestMock.getParameter(ATT_RADIO_ROLE)).andReturn("prof");
		
		/* Affichage du formulaire */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getRequestDispatcher(VUE_FORM)).andReturn(this.requestDispatcherMock);
		this.requestDispatcherMock.forward(this.httpServletRequestMock, this.httpservletResponseMock);
		expectLastCall();

		this.replayAll();

		/* Cycle de vie d'une servlet */
		this.gererRoles.init(this.servletConfigMock);
		this.gererRoles.doPost(this.httpServletRequestMock, this.httpservletResponseMock);
		this.gererRoles.destroy();

		this.verifyAll();
	}
	
	/**
	 * Teste la méthode protected void doPost(HttpServletRequest request, HttpServletResponse response).
	 * 
	 * <p>Cas du choix "ajouterAutorisation" avec option pour un role qui ne doit pas en avoir.</p>
	 * 
	 * @throws ServletException
	 * @throws IOException
	 */
	@Test
	public void test3_5DoPostAjouterAutorisationAvecOptionErronee() throws ServletException, IOException {
		/* Récupération d'instances de DAO */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
		expect(this.daoFactoryMock.getUtilisateurDao()).andReturn(utilisateurDAO);
		expect(this.daoFactoryMock.getEtudiantDao()).andReturn(etudiantDAO);
		expect(this.daoFactoryMock.getRoleDAO()).andReturn(roleDAO);
		expect(this.daoFactoryMock.getOptionESEODAO()).andReturn(optionESEODAO);
		expect(this.daoFactoryMock.getAnneeScolaireDAO()).andReturn(anneeScolaireDAO);
		expect(this.daoFactoryMock.getNotificationDao()).andReturn(notificationDAO);
		expect(this.daoFactoryMock.getPglEtudiantDAO()).andReturn(pglEtudiantDAO);


		/* Récupération des données saisies */
		expect(this.httpServletRequestMock.getParameter(ATT_UTILISATEUR_CHOISI)).andReturn(utilisateur.getIdUtilisateur().toString());
		expect(this.httpServletRequestMock.getParameter(ATT_FORMULAIRE)).andReturn("ajouterAutorisation");
		expectLastCall().times(4);
		expect(this.httpServletRequestMock.getParameter(ATT_RADIO_OPTION)).andReturn("LD");
		expect(this.httpServletRequestMock.getParameter(ATT_RADIO_ROLE)).andReturn("admin");
		
		/* Affichage du formulaire */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getRequestDispatcher(VUE_FORM)).andReturn(this.requestDispatcherMock);
		this.requestDispatcherMock.forward(this.httpServletRequestMock, this.httpservletResponseMock);
		expectLastCall();

		this.replayAll();

		/* Cycle de vie d'une servlet */
		this.gererRoles.init(this.servletConfigMock);
		this.gererRoles.doPost(this.httpServletRequestMock, this.httpservletResponseMock);
		this.gererRoles.destroy();

//		this.verifyAll();
	}
	
	/**
	 * Teste la méthode protected void doPost(HttpServletRequest request, HttpServletResponse response).
	 * 
	 * <p>Cas du choix "supprimerAutorisation".</p>
	 * 
	 * @throws ServletException
	 * @throws IOException
	 */
	@Test
	public void test4_1DoPostSupprimerAutorisation() throws ServletException, IOException {
		/* Récupération d'instances de DAO */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
		expect(this.daoFactoryMock.getUtilisateurDao()).andReturn(utilisateurDAO);
		expect(this.daoFactoryMock.getEtudiantDao()).andReturn(etudiantDAO);
		expect(this.daoFactoryMock.getRoleDAO()).andReturn(roleDAO);
		expect(this.daoFactoryMock.getOptionESEODAO()).andReturn(optionESEODAO);
		expect(this.daoFactoryMock.getAnneeScolaireDAO()).andReturn(anneeScolaireDAO);
		expect(this.daoFactoryMock.getNotificationDao()).andReturn(notificationDAO);
		expect(this.daoFactoryMock.getPglEtudiantDAO()).andReturn(pglEtudiantDAO);


		/* Récupération des données saisies */
		expect(this.httpServletRequestMock.getParameter(ATT_UTILISATEUR_CHOISI)).andReturn(utilisateur.getIdUtilisateur().toString());
		expect(this.httpServletRequestMock.getParameter(ATT_FORMULAIRE)).andReturn("supprimerAutorisation");
		expectLastCall().times(3);
		expect(this.httpServletRequestMock.getParameter(ATT_OPTION_UTILISATEUR)).andReturn("");
		expect(this.httpServletRequestMock.getParameter(ATT_ROLE_UTILISATEUR)).andReturn("prof");
		
		/* Affichage du formulaire */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getRequestDispatcher(VUE_FORM)).andReturn(this.requestDispatcherMock);
		this.requestDispatcherMock.forward(this.httpServletRequestMock, this.httpservletResponseMock);
		expectLastCall();

		this.replayAll();

		/* Cycle de vie d'une servlet */
		this.gererRoles.init(this.servletConfigMock);
		this.gererRoles.doPost(this.httpServletRequestMock, this.httpservletResponseMock);
		this.gererRoles.destroy();
		
		/* Vérification de la suppression d'autorisation pour une option */
		final String optionAttendue = "LD";
		final String optionTrouvee = utilisateurDAO.trouverOption(utilisateur).get(0).getNomOption();
		assertEquals("Mauvaise suppression d'autorisation d'option", optionAttendue, optionTrouvee);
		
		/* Vérification de la suppression d'autorisation pour un role */
		final String roleAttendu = "prof";
		final String roleTrouve = utilisateurDAO.trouverRole(utilisateur).get(0).getNomRole();
		assertEquals("Mauvaise suppression d'autorisation de role", roleAttendu, roleTrouve);

		//this.verifyAll();
	}
	
	/**
	 * Teste la méthode protected void doPost(HttpServletRequest request, HttpServletResponse response).
	 * 
	 * <p>Cas du choix "suspendreCompte".</p>
	 * 
	 * @throws ServletException
	 * @throws IOException
	 */
	@Test
	public void testDo5_1PostSuspendreCompte() throws ServletException, IOException {
		/* Récupération d'instances de DAO */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
		expect(this.daoFactoryMock.getUtilisateurDao()).andReturn(utilisateurDAO);
		expect(this.daoFactoryMock.getEtudiantDao()).andReturn(etudiantDAO);
		expect(this.daoFactoryMock.getRoleDAO()).andReturn(roleDAO);
		expect(this.daoFactoryMock.getOptionESEODAO()).andReturn(optionESEODAO);
		expect(this.daoFactoryMock.getAnneeScolaireDAO()).andReturn(anneeScolaireDAO);
		expect(this.daoFactoryMock.getNotificationDao()).andReturn(notificationDAO);
		expect(this.daoFactoryMock.getPglEtudiantDAO()).andReturn(pglEtudiantDAO);


		/* Récupération des données saisies */
		expect(this.httpServletRequestMock.getParameter(ATT_UTILISATEUR_CHOISI)).andReturn(utilisateur.getIdUtilisateur().toString());
		expect(this.httpServletRequestMock.getParameter(ATT_FORMULAIRE)).andReturn("suspendreCompte");
		expectLastCall().times(2);
		expect(this.httpServletRequestMock.getParameter(ATT_CHOIX_VALIDATION)).andReturn("non");
		
		/* Affichage du formulaire */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getRequestDispatcher(VUE_FORM)).andReturn(this.requestDispatcherMock);
		this.requestDispatcherMock.forward(this.httpServletRequestMock, this.httpservletResponseMock);
		expectLastCall();

		this.replayAll();

		/* Cycle de vie d'une servlet */
		this.gererRoles.init(this.servletConfigMock);
		this.gererRoles.doPost(this.httpServletRequestMock, this.httpservletResponseMock);
		this.gererRoles.destroy();
		
		/* Vérification que le compte de l'utilisateur a bien été suspendu */
		utilisateur.setValide("non");
		final String resultatAttendu = "non"; // le compte n'est plus valide
		final String resultatTrouve = utilisateurDAO.trouver(utilisateur).get(0).getValide();
		assertEquals("L'utilisateur a été inséré dans la BDD", resultatAttendu, resultatTrouve);

		this.verifyAll();
	}

}