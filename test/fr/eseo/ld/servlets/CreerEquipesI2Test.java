package fr.eseo.ld.servlets;

import fr.eseo.ld.beans.AnneeScolaire;
import fr.eseo.ld.beans.OptionESEO;
import fr.eseo.ld.beans.Role;
import fr.eseo.ld.beans.Utilisateur;
import fr.eseo.ld.dao.*;
import fr.eseo.ld.pgl.beans.Equipe;
import fr.eseo.ld.pgl.beans.Projet;
import fr.eseo.ld.pgl.beans.Sprint;
import org.easymock.*;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.expectLastCall;

/**
 * Classe de tests unitaires JUnit 4 de la classe CreerEquipeI2.
 *
 * <p>Utilisation de mocks (construits avec EasyMock).</p>
 *
 * @version 1.0
 * @see fr.eseo.ld.servlets.CreerEquipesI2
 * @see org.junit
 * @see org.easymock
 */
@RunWith(EasyMockRunner.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class CreerEquipesI2Test extends EasyMockSupport {

    private static final String CONF_DAO_FACTORY = "daoFactory";

    private static final String ATT_SESSION_USER = "utilisateur";
    private static final Long ID_ETUDIANT_LD1 = 1339L;


    private static final String STRING_TEST = "test";

    private static final String TEST = "testutilisateur";
    private static final String TEST_DATE = "2018/01/01";
    private static final Long ID_UTILISATEUR_TEST = 11L;
    private static final String ID_MEETING = "1";

    private static final Integer ANNEE_COURANTE = 2032;
    private static final String EQUIPEID = ANNEE_COURANTE + "_001";
    private static final Long ID_DEFAULT = 1L;
    private static final String ID_DEFAULT_STR = "1";
    private static final Long ID_PROF_RESPONSABLE = 16L;

    private static final String NOM_EQUIPE = "Alpha";
    private static final String NOM_EQUIPE_2 = "Beta";
    public static final String CHAMP_NOM_EQUIPE = "nomEquipe";
    public static final String CHAMP_PROJET_EQUIPE = "projetEquipe";
    public static final String CHAMP_LISTE_EQUIPE = "equipeEquipe";
    public static final String CHAMP_ANNEE_SCOLAIRE_EQUIPE = "anneeScolaireEquipe";
    public static final String CHAMP_ID_EQUIPE_PGL_DELETE = "idEquipeDelete";
    public static final String CHAMP_ID_EQUIPE = "idEquipe";
    public static final String SPRINT_SELECT = "sprintSelect";

    private static UtilisateurDAO utilisateurDAO;
    private static AnneeScolaireDAO anneeScolaireDAO;
    private static RoleDAO roleDAO;
    private static OptionESEODAO optionESEODAO;
    /* Récupération DAO PGL */
    private static PglEquipeDAO pglEquipeDAO;
    private static PglEtudiantEquipeDAO pglEtudiantEquipeDAO;
    private static PglProjetDAO pglProjetDAO;
    private static SprintDAO sprintDAO;
    private static PglRoleEquipeDAO pglRoleEquipeDAO;
    private static BonusMalusDAO bonusMalusDAO;


    private static final String CONTEXT_PATH = "SoManager/CreerEquipesI2";
    private static final String VUE_DASHBOARD = "/Dashboard";
    private static final String VUE_FORM = "/WEB-INF/formulaires/creerEquipesI2.jsp";

    private static AnneeScolaire anneeScolaire;
    private static Sprint sprint;
    private static Projet projet;
    private static Equipe equipe;
    private static Role role;
    private static Utilisateur utilisateur;
    private static OptionESEO optionESEO;

    @TestSubject
    private CreerEquipesI2 creerEquipesI2 = new CreerEquipesI2();

    @Mock
    private ServletConfig servletConfigMock;

    @Mock
    private ServletContext servletContextMock;

    @Mock(type = MockType.NICE)
    private HttpServletRequest httpServletRequestMock;

    @Mock
    private HttpServletResponse httpservletResponseMock;

    @Mock(type = MockType.NICE)
    private HttpSession httpSessionMock;

    @Mock
    private RequestDispatcher requestDispatcherMock;

    @Mock
    private DAOFactory daoFactoryMock;


    /**
     * Récupère des instances de DAO.
     *
     * @throws Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        /* Instanciations de DAO */
        DAOFactory daoFactory = DAOFactory.getInstance();
        utilisateurDAO = daoFactory.getUtilisateurDao();
        anneeScolaireDAO = daoFactory.getAnneeScolaireDAO();
        pglEquipeDAO = daoFactory.getPglEquipeDAO();
        pglEtudiantEquipeDAO = daoFactory.getPglEtudiantEquipeDAO();
        pglProjetDAO = daoFactory.getPglProjetDAO();
        sprintDAO = daoFactory.getSprintDAO();
        pglRoleEquipeDAO = daoFactory.getPglRoleEquipeDAO();
        roleDAO = daoFactory.getRoleDAO();
        optionESEODAO = daoFactory.getOptionESEODAO();
        bonusMalusDAO = daoFactory.getBonusMalusDAO();

        /* Instanciation du bean anneeScolaire */
        anneeScolaire = new AnneeScolaire();
        anneeScolaire.setIdAnneeScolaire(ID_DEFAULT);
        anneeScolaire.setAnneeDebut(ANNEE_COURANTE);
        anneeScolaire.setAnneeFin(ANNEE_COURANTE + 1);
        anneeScolaire.setIdOption(ID_DEFAULT);

        sprint = new Sprint();
        sprint.setIdSprint(ID_DEFAULT);
        sprint.setDateDebut("2018-01-01");
        sprint.setDateFin("2018-05-01");
        sprint.setNbrHeures(10);
        sprint.setNumero(1);
        sprint.setRefAnnee(ID_DEFAULT);
        sprint.setCoefficient(1.0f);
        sprint.setNotesPubliees(false);
        sprintDAO.creer(sprint);

        projet = new Projet();
        projet.setIdProfReferent(ID_DEFAULT);
        projet.setDescription("DESC");
        projet.setTitre("Titre");
        projet.setIdProjet(ID_DEFAULT);
        pglProjetDAO.creer(projet);

        /* Insertion d'un utilisateur test dans la BDD */
        utilisateur = new Utilisateur();
        utilisateur.setIdentifiant(STRING_TEST);
        utilisateur.setNom(STRING_TEST);
        utilisateur.setPrenom(STRING_TEST);
        utilisateur.setValide("oui");
        utilisateurDAO.creer(utilisateur);
        final Long idUtilisateur = utilisateurDAO.trouver(utilisateur).get(0).getIdUtilisateur();
        utilisateur.setIdUtilisateur(idUtilisateur);

        role = new Role();
        role.setNomRole("productOwner");
        role.setType("applicatif");
        role.setIdRole(ID_DEFAULT);
        roleDAO.creer(role);

        optionESEO = new OptionESEO();
        optionESEO.setIdOption(ID_DEFAULT);
        optionESEO.setNomOption("LD");
        optionESEODAO.creer(optionESEO);

        utilisateurDAO.associerRoleOption(utilisateur, role, optionESEO);

        equipe = new Equipe();
        equipe.setRefAnnee(anneeScolaire.getIdAnneeScolaire());
        equipe.setRefProjet(projet.getIdProjet());
        equipe.setNom(NOM_EQUIPE);
        equipe.setIdEquipe(ID_DEFAULT);
        pglEquipeDAO.creer(equipe);

    }


    /**
     * Supprime le meeting inséré dans la BDD.
     *
     * @throws Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        //anneeScolaireDAO.supprimer(anneeScolaire);
        pglProjetDAO.supprimer(projet);
        pglEquipeDAO.supprimer(equipe);
        sprintDAO.supprimer(sprint);
        utilisateurDAO.supprimer(utilisateur);
        roleDAO.supprimer(role);
    }


    /**
     * Teste la méthode protected void doGet(HttpServletRequest request, HttpServletResponse response).
     *
     * @throws ServletException
     * @throws IOException
     */
    @Test
    public void test1DoGet() throws ServletException, IOException {
        /* Récupération d'instances de DAO */
        expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
        expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
        expect(this.daoFactoryMock.getUtilisateurDao()).andReturn(utilisateurDAO);
        expect(this.daoFactoryMock.getAnneeScolaireDAO()).andReturn(anneeScolaireDAO);
        expect(this.daoFactoryMock.getPglEquipeDAO()).andReturn(pglEquipeDAO);
        expect(this.daoFactoryMock.getPglEtudiantEquipeDAO()).andReturn(pglEtudiantEquipeDAO);
        expect(this.daoFactoryMock.getPglProjetDAO()).andReturn(pglProjetDAO);
        expect(this.daoFactoryMock.getSprintDAO()).andReturn(sprintDAO);
        expect(this.daoFactoryMock.getPglRoleEquipeDAO()).andReturn(pglRoleEquipeDAO);
        expect(this.daoFactoryMock.getRoleDAO()).andReturn(roleDAO);
        expect(this.daoFactoryMock.getOptionESEODAO()).andReturn(optionESEODAO);
        expect(this.daoFactoryMock.getBonusMalusDAO()).andReturn(bonusMalusDAO);

        /* Récupération de l'Utilisateur en session */
        final Utilisateur utilisateur = new Utilisateur();
        final Long idUtilisateur = ID_ETUDIANT_LD1;
        utilisateur.setIdUtilisateur(idUtilisateur);
        expect(this.httpServletRequestMock.getSession()).andReturn(this.httpSessionMock);
        expect(this.httpSessionMock.getAttribute(ATT_SESSION_USER)).andReturn(utilisateur);

        /* Affichage du formulaire */
        expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
        expect(this.servletContextMock.getRequestDispatcher(VUE_FORM)).andReturn(this.requestDispatcherMock);
        this.requestDispatcherMock.forward(this.httpServletRequestMock, this.httpservletResponseMock);
        expectLastCall();

        this.replayAll();

        /* Cycle de vie d'une servlet */
        this.creerEquipesI2.init(this.servletConfigMock);
        this.creerEquipesI2.doGet(this.httpServletRequestMock, this.httpservletResponseMock);
        this.creerEquipesI2.destroy();

        //this.verifyAll();
    }


    /**
     * Teste la méthode protected void doPost(HttpServletRequest request, HttpServletResponse response).
     *
     * <p>Cas du succès de la création d'une équipe.</p>
     *
     * @throws ServletException
     * @throws IOException
     */
    @Test
    public void testDoPostErreurAjouterEquipeI2() throws ServletException, IOException {
        /* Récupération d'instances de DAO */
        expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
        expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
        expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
        expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
        expect(this.daoFactoryMock.getUtilisateurDao()).andReturn(utilisateurDAO);
        expect(this.daoFactoryMock.getAnneeScolaireDAO()).andReturn(anneeScolaireDAO);
        expect(this.daoFactoryMock.getPglEquipeDAO()).andReturn(pglEquipeDAO);
        expect(this.daoFactoryMock.getPglEtudiantEquipeDAO()).andReturn(pglEtudiantEquipeDAO);
        expect(this.daoFactoryMock.getPglProjetDAO()).andReturn(pglProjetDAO);
        expect(this.daoFactoryMock.getSprintDAO()).andReturn(sprintDAO);
        expect(this.daoFactoryMock.getPglRoleEquipeDAO()).andReturn(pglRoleEquipeDAO);
        expect(this.daoFactoryMock.getRoleDAO()).andReturn(roleDAO);
        expect(this.daoFactoryMock.getOptionESEODAO()).andReturn(optionESEODAO);
        expect(this.daoFactoryMock.getBonusMalusDAO()).andReturn(bonusMalusDAO);


        /* Récupération de l'Utilisateur en session */
        expect(this.httpServletRequestMock.getSession()).andReturn(this.httpSessionMock);
        final Utilisateur utilisateur = new Utilisateur();
        final Long idUtilisateur = ID_PROF_RESPONSABLE;
        utilisateur.setIdUtilisateur(idUtilisateur);
        expect(this.httpSessionMock.getAttribute(ATT_SESSION_USER)).andReturn(utilisateur);


        /* Récupération des données saisies */
        expect(this.httpServletRequestMock.getParameter("formulaire")).andReturn("ajouterEquipeI2");
        expectLastCall().times(4);


        /* Récupération des données saisies */
        expect(this.httpServletRequestMock.getParameter(CHAMP_NOM_EQUIPE)).andReturn(NOM_EQUIPE_2);
        expectLastCall().times(2);
        expect(this.httpServletRequestMock.getParameter(CHAMP_PROJET_EQUIPE)).andReturn(ID_DEFAULT_STR);
        expectLastCall().times(2);
        expect(this.httpServletRequestMock.getParameter(CHAMP_ANNEE_SCOLAIRE_EQUIPE)).andReturn(anneeScolaire.getIdAnneeScolaire().toString());
        expectLastCall().times(2);

        expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
        expect(this.servletContextMock.getRequestDispatcher(VUE_FORM)).andReturn(this.requestDispatcherMock);
        this.requestDispatcherMock.forward(this.httpServletRequestMock, this.httpservletResponseMock);
        expectLastCall();

        this.replayAll();

        /* Cycle de vie d'une servlet */
        this.creerEquipesI2.init(this.servletConfigMock);
        this.creerEquipesI2.doPost(this.httpServletRequestMock, this.httpservletResponseMock);
        this.creerEquipesI2.destroy();

        //this.verifyAll();
    }


    /**
     * Teste la méthode protected void doPost(HttpServletRequest request, HttpServletResponse response).
     *
     * <p>Cas du succès de la suppression d'une équipe.</p>
     *
     * @throws ServletException
     * @throws IOException
     */
    @Test
    public void testDoPostErreurSuppressionEquipeI2() throws ServletException, IOException {
        /* Récupération d'instances de DAO */
        expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
        expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
        expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
        expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
        expect(this.daoFactoryMock.getUtilisateurDao()).andReturn(utilisateurDAO);
        expect(this.daoFactoryMock.getAnneeScolaireDAO()).andReturn(anneeScolaireDAO);
        expect(this.daoFactoryMock.getPglEquipeDAO()).andReturn(pglEquipeDAO);
        expect(this.daoFactoryMock.getPglEtudiantEquipeDAO()).andReturn(pglEtudiantEquipeDAO);
        expect(this.daoFactoryMock.getPglProjetDAO()).andReturn(pglProjetDAO);
        expect(this.daoFactoryMock.getSprintDAO()).andReturn(sprintDAO);
        expect(this.daoFactoryMock.getPglRoleEquipeDAO()).andReturn(pglRoleEquipeDAO);
        expect(this.daoFactoryMock.getRoleDAO()).andReturn(roleDAO);
        expect(this.daoFactoryMock.getOptionESEODAO()).andReturn(optionESEODAO);
        expect(this.daoFactoryMock.getBonusMalusDAO()).andReturn(bonusMalusDAO);

        /* Récupération de l'Utilisateur en session */
        expect(this.httpServletRequestMock.getSession()).andReturn(this.httpSessionMock);
        final Utilisateur utilisateur = new Utilisateur();
        final Long idUtilisateur = ID_PROF_RESPONSABLE;
        utilisateur.setIdUtilisateur(idUtilisateur);
        expect(this.httpSessionMock.getAttribute(ATT_SESSION_USER)).andReturn(utilisateur);

        /* Récupération des données saisies */
        expect(this.httpServletRequestMock.getParameter("formulaire")).andReturn("supprimerEquipeI2");
        expectLastCall().times(4);


        /* Récupération des données saisies */
        expect(this.httpServletRequestMock.getParameter(CHAMP_ID_EQUIPE_PGL_DELETE)).andReturn(ID_DEFAULT_STR);
        expectLastCall().times(2);

        expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
        expect(this.servletContextMock.getRequestDispatcher(VUE_FORM)).andReturn(this.requestDispatcherMock);
        this.requestDispatcherMock.forward(this.httpServletRequestMock, this.httpservletResponseMock);
        expectLastCall();

        this.replayAll();

        /* Cycle de vie d'une servlet */
        this.creerEquipesI2.init(this.servletConfigMock);
        this.creerEquipesI2.doPost(this.httpServletRequestMock, this.httpservletResponseMock);
        this.creerEquipesI2.destroy();

        //this.verifyAll();
    }


    /**
     * Teste la méthode protected void doPost(HttpServletRequest request, HttpServletResponse response).
     *
     * <p>Cas du succès de l'ajout de membre d'une équipe.</p>
     *
     * @throws ServletException
     * @throws IOException
     */
    @Test
    public void testDoPostSuccesErreurMembreEquipeI2() throws ServletException, IOException {
        /* Récupération d'instances de DAO */
        expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
        expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
        expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
        expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
        expect(this.daoFactoryMock.getUtilisateurDao()).andReturn(utilisateurDAO);
        expect(this.daoFactoryMock.getAnneeScolaireDAO()).andReturn(anneeScolaireDAO);
        expect(this.daoFactoryMock.getPglEquipeDAO()).andReturn(pglEquipeDAO);
        expect(this.daoFactoryMock.getPglEtudiantEquipeDAO()).andReturn(pglEtudiantEquipeDAO);
        expect(this.daoFactoryMock.getPglProjetDAO()).andReturn(pglProjetDAO);
        expect(this.daoFactoryMock.getSprintDAO()).andReturn(sprintDAO);
        expect(this.daoFactoryMock.getPglRoleEquipeDAO()).andReturn(pglRoleEquipeDAO);
        expect(this.daoFactoryMock.getRoleDAO()).andReturn(roleDAO);
        expect(this.daoFactoryMock.getOptionESEODAO()).andReturn(optionESEODAO);
        expect(this.daoFactoryMock.getBonusMalusDAO()).andReturn(bonusMalusDAO);

        /* Récupération de l'Utilisateur en session */
        expect(this.httpServletRequestMock.getSession()).andReturn(this.httpSessionMock);
        final Utilisateur utilisateur = new Utilisateur();
        final Long idUtilisateur = ID_PROF_RESPONSABLE;
        utilisateur.setIdUtilisateur(idUtilisateur);
        expect(this.httpSessionMock.getAttribute(ATT_SESSION_USER)).andReturn(utilisateur);

        /* Récupération des données saisies */
        expect(this.httpServletRequestMock.getParameter("formulaire")).andReturn("ajouterMembreEquipeI2");
        expectLastCall().times(4);


        /* Récupération des données saisies */
        expect(this.httpServletRequestMock.getParameter(CHAMP_ID_EQUIPE)).andReturn(equipe.getIdEquipe().toString());
        expectLastCall().times(2);
        expect(this.httpServletRequestMock.getParameter(SPRINT_SELECT)).andReturn(sprint.getIdSprint().toString());
        expectLastCall().times(2);


        expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
        expect(this.servletContextMock.getRequestDispatcher(VUE_FORM)).andReturn(this.requestDispatcherMock);
        this.requestDispatcherMock.forward(this.httpServletRequestMock, this.httpservletResponseMock);
        expectLastCall();

        this.replayAll();

        /* Cycle de vie d'une servlet */
        this.creerEquipesI2.init(this.servletConfigMock);
        this.creerEquipesI2.doPost(this.httpServletRequestMock, this.httpservletResponseMock);
        this.creerEquipesI2.destroy();

        //this.verifyAll();
    }


    /**
     * Teste la méthode protected void doPost(HttpServletRequest request, HttpServletResponse response).
     *
     * <p>Cas du succès de la suppression de membre d'une équipe.</p>
     *
     * @throws ServletException
     * @throws IOException
     */
    @Test
    public void testDoPostErreurSupprimerMembreEquipeI2() throws ServletException, IOException {
        /* Récupération d'instances de DAO */
        expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
        expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
        expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
        expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
        expect(this.daoFactoryMock.getUtilisateurDao()).andReturn(utilisateurDAO);
        expect(this.daoFactoryMock.getAnneeScolaireDAO()).andReturn(anneeScolaireDAO);
        expect(this.daoFactoryMock.getPglEquipeDAO()).andReturn(pglEquipeDAO);
        expect(this.daoFactoryMock.getPglEtudiantEquipeDAO()).andReturn(pglEtudiantEquipeDAO);
        expect(this.daoFactoryMock.getPglProjetDAO()).andReturn(pglProjetDAO);
        expect(this.daoFactoryMock.getSprintDAO()).andReturn(sprintDAO);
        expect(this.daoFactoryMock.getPglRoleEquipeDAO()).andReturn(pglRoleEquipeDAO);
        expect(this.daoFactoryMock.getRoleDAO()).andReturn(roleDAO);
        expect(this.daoFactoryMock.getOptionESEODAO()).andReturn(optionESEODAO);
        expect(this.daoFactoryMock.getBonusMalusDAO()).andReturn(bonusMalusDAO);

        /* Récupération de l'Utilisateur en session */
        expect(this.httpServletRequestMock.getSession()).andReturn(this.httpSessionMock);
        final Utilisateur utilisateur = new Utilisateur();
        final Long idUtilisateur = ID_PROF_RESPONSABLE;
        utilisateur.setIdUtilisateur(idUtilisateur);
        expect(this.httpSessionMock.getAttribute(ATT_SESSION_USER)).andReturn(utilisateur);

        /* Récupération des données saisies */
        expect(this.httpServletRequestMock.getParameter("formulaire")).andReturn("supprimerMembreEquipeI2");
        expectLastCall().times(4);


        /* Récupération des données saisies */
        expect(this.httpServletRequestMock.getParameter(CHAMP_ID_EQUIPE)).andReturn(equipe.getIdEquipe().toString());
        expectLastCall().times(2);
        expect(this.httpServletRequestMock.getParameter(SPRINT_SELECT)).andReturn(sprint.getIdSprint().toString());
        expectLastCall().times(2);


        expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
        expect(this.servletContextMock.getRequestDispatcher(VUE_FORM)).andReturn(this.requestDispatcherMock);
        this.requestDispatcherMock.forward(this.httpServletRequestMock, this.httpservletResponseMock);
        expectLastCall();

        this.replayAll();

        /* Cycle de vie d'une servlet */
        this.creerEquipesI2.init(this.servletConfigMock);
        this.creerEquipesI2.doPost(this.httpServletRequestMock, this.httpservletResponseMock);
        this.creerEquipesI2.destroy();

        //this.verifyAll();
    }

    /**
     * Teste la méthode protected void doPost(HttpServletRequest request, HttpServletResponse response).
     *
     * <p>Cas du succès de transfert de membre d'une équipe.</p>
     *
     * @throws ServletException
     * @throws IOException
     */
    @Test
    public void testDoPostErreursTransfererMembreEquipeI2() throws ServletException, IOException {
        /* Récupération d'instances de DAO */
        expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
        expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
        expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
        expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
        expect(this.daoFactoryMock.getUtilisateurDao()).andReturn(utilisateurDAO);
        expect(this.daoFactoryMock.getAnneeScolaireDAO()).andReturn(anneeScolaireDAO);
        expect(this.daoFactoryMock.getPglEquipeDAO()).andReturn(pglEquipeDAO);
        expect(this.daoFactoryMock.getPglEtudiantEquipeDAO()).andReturn(pglEtudiantEquipeDAO);
        expect(this.daoFactoryMock.getPglProjetDAO()).andReturn(pglProjetDAO);
        expect(this.daoFactoryMock.getSprintDAO()).andReturn(sprintDAO);
        expect(this.daoFactoryMock.getPglRoleEquipeDAO()).andReturn(pglRoleEquipeDAO);
        expect(this.daoFactoryMock.getRoleDAO()).andReturn(roleDAO);
        expect(this.daoFactoryMock.getOptionESEODAO()).andReturn(optionESEODAO);
        expect(this.daoFactoryMock.getBonusMalusDAO()).andReturn(bonusMalusDAO);

        /* Récupération de l'Utilisateur en session */
        expect(this.httpServletRequestMock.getSession()).andReturn(this.httpSessionMock);
        final Utilisateur utilisateur = new Utilisateur();
        final Long idUtilisateur = ID_PROF_RESPONSABLE;
        utilisateur.setIdUtilisateur(idUtilisateur);
        expect(this.httpSessionMock.getAttribute(ATT_SESSION_USER)).andReturn(utilisateur);

        /* Récupération des données saisies */
        expect(this.httpServletRequestMock.getParameter("formulaire")).andReturn("modifierMembreEquipeI2");
        expectLastCall().times(4);


        /* Récupération des données saisies */
        expect(this.httpServletRequestMock.getParameter(CHAMP_LISTE_EQUIPE)).andReturn(equipe.getIdEquipe().toString());
        expectLastCall().times(2);
        expect(this.httpServletRequestMock.getParameter(SPRINT_SELECT)).andReturn(sprint.getIdSprint().toString());
        expectLastCall().times(2);


        expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
        expect(this.servletContextMock.getRequestDispatcher(VUE_FORM)).andReturn(this.requestDispatcherMock);
        this.requestDispatcherMock.forward(this.httpServletRequestMock, this.httpservletResponseMock);
        expectLastCall();

        this.replayAll();

        /* Cycle de vie d'une servlet */
        this.creerEquipesI2.init(this.servletConfigMock);
        this.creerEquipesI2.doPost(this.httpServletRequestMock, this.httpservletResponseMock);
        this.creerEquipesI2.destroy();

        //this.verifyAll();
    }

    /**
     * Teste la méthode protected void doPost(HttpServletRequest request, HttpServletResponse response).
     *
     * <p>Cas d'erreur d'affectaction de Product Owner à une équipe.</p>
     *
     * @throws ServletException
     * @throws IOException
     */
    @Test
    public void testDoPostErreurAffecterPOMembreEquipeI2() throws ServletException, IOException {
        /* Récupération d'instances de DAO */
        expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
        expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
        expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
        expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
        expect(this.daoFactoryMock.getUtilisateurDao()).andReturn(utilisateurDAO);
        expect(this.daoFactoryMock.getAnneeScolaireDAO()).andReturn(anneeScolaireDAO);
        expect(this.daoFactoryMock.getPglEquipeDAO()).andReturn(pglEquipeDAO);
        expect(this.daoFactoryMock.getPglEtudiantEquipeDAO()).andReturn(pglEtudiantEquipeDAO);
        expect(this.daoFactoryMock.getPglProjetDAO()).andReturn(pglProjetDAO);
        expect(this.daoFactoryMock.getSprintDAO()).andReturn(sprintDAO);
        expect(this.daoFactoryMock.getPglRoleEquipeDAO()).andReturn(pglRoleEquipeDAO);
        expect(this.daoFactoryMock.getRoleDAO()).andReturn(roleDAO);
        expect(this.daoFactoryMock.getOptionESEODAO()).andReturn(optionESEODAO);
        expect(this.daoFactoryMock.getBonusMalusDAO()).andReturn(bonusMalusDAO);

        /* Récupération de l'Utilisateur en session */
        expect(this.httpServletRequestMock.getSession()).andReturn(this.httpSessionMock);
        final Utilisateur utilisateur = new Utilisateur();
        final Long idUtilisateur = ID_PROF_RESPONSABLE;
        utilisateur.setIdUtilisateur(idUtilisateur);
        expect(this.httpSessionMock.getAttribute(ATT_SESSION_USER)).andReturn(utilisateur);

        /* Récupération des données saisies */
        expect(this.httpServletRequestMock.getParameter("formulaire")).andReturn("affecterProductOwnerEquipeI2");
        expectLastCall().times(4);

        /* Récupération des données saisies */
        expect(this.httpServletRequestMock.getParameter(CHAMP_LISTE_EQUIPE)).andReturn(ID_DEFAULT_STR);
        expectLastCall().times(2);

        expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
        expect(this.servletContextMock.getRequestDispatcher(VUE_FORM)).andReturn(this.requestDispatcherMock);
        this.requestDispatcherMock.forward(this.httpServletRequestMock, this.httpservletResponseMock);
        expectLastCall();

        this.replayAll();

        /* Cycle de vie d'une servlet */
        this.creerEquipesI2.init(this.servletConfigMock);
        this.creerEquipesI2.doPost(this.httpServletRequestMock, this.httpservletResponseMock);
        this.creerEquipesI2.destroy();

        //this.verifyAll();
    }

    /**
     * Teste la méthode protected void doPost(HttpServletRequest request, HttpServletResponse response).
     *
     * <p>Cas du succès d'affectaction de Product Owner à une équipe.</p>
     *
     * @throws ServletException
     * @throws IOException
     */
    @Test
    public void testDoPostSuccesAffecterPOMembreEquipeI2() throws ServletException, IOException {
        /* Récupération d'instances de DAO */
        expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
        expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
        expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
        expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
        expect(this.daoFactoryMock.getUtilisateurDao()).andReturn(utilisateurDAO);
        expect(this.daoFactoryMock.getAnneeScolaireDAO()).andReturn(anneeScolaireDAO);
        expect(this.daoFactoryMock.getPglEquipeDAO()).andReturn(pglEquipeDAO);
        expect(this.daoFactoryMock.getPglEtudiantEquipeDAO()).andReturn(pglEtudiantEquipeDAO);
        expect(this.daoFactoryMock.getPglProjetDAO()).andReturn(pglProjetDAO);
        expect(this.daoFactoryMock.getSprintDAO()).andReturn(sprintDAO);
        expect(this.daoFactoryMock.getPglRoleEquipeDAO()).andReturn(pglRoleEquipeDAO);
        expect(this.daoFactoryMock.getRoleDAO()).andReturn(roleDAO);
        expect(this.daoFactoryMock.getOptionESEODAO()).andReturn(optionESEODAO);
        expect(this.daoFactoryMock.getBonusMalusDAO()).andReturn(bonusMalusDAO);

        /* Récupération de l'Utilisateur en session */
        expect(this.httpServletRequestMock.getSession()).andReturn(this.httpSessionMock);
        final Utilisateur utilisateur = new Utilisateur();
        final Long idUtilisateur = ID_PROF_RESPONSABLE;
        utilisateur.setIdUtilisateur(idUtilisateur);
        expect(this.httpSessionMock.getAttribute(ATT_SESSION_USER)).andReturn(utilisateur);

        /* Récupération des données saisies */
        expect(this.httpServletRequestMock.getParameter("formulaire")).andReturn("affecterProductOwnerEquipeI2");
        expectLastCall().times(4);

        /* Récupération des données saisies */
        expect(this.httpServletRequestMock.getParameter(CHAMP_LISTE_EQUIPE)).andReturn(String.valueOf(ID_DEFAULT));
        expectLastCall().times(2);

        expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
        expect(this.servletContextMock.getRequestDispatcher(VUE_FORM)).andReturn(this.requestDispatcherMock);
        this.requestDispatcherMock.forward(this.httpServletRequestMock, this.httpservletResponseMock);
        expectLastCall();

        this.replayAll();

        /* Cycle de vie d'une servlet */
        this.creerEquipesI2.init(this.servletConfigMock);
        this.creerEquipesI2.doPost(this.httpServletRequestMock, this.httpservletResponseMock);
        this.creerEquipesI2.destroy();

        //this.verifyAll();
    }


}
