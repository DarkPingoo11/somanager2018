package fr.eseo.ld.servlets;

import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.expectLastCall;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.easymock.EasyMockRunner;
import org.easymock.EasyMockSupport;
import org.easymock.Mock;
import org.easymock.MockType;
import org.easymock.TestSubject;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.mindrot.jbcrypt.BCrypt;

import fr.eseo.ld.beans.Utilisateur;
import fr.eseo.ld.dao.DAOFactory;
import fr.eseo.ld.dao.NotificationDAO;
import fr.eseo.ld.dao.UtilisateurDAO;

/**
 * Classe de tests unitaires JUnit 4 de la classe ChangerMotDePasse.
 *
 * <p>Utilisation de mocks (construits avec EasyMock).</p>
 *
 * @version 1.0
 * @author Maxime LENORMAND
 *
 * @see fr.eseo.ld.servlets.ChangerMotDePasse
 * @see org.junit
 * @see org.easymock
 */
@RunWith(EasyMockRunner.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ChangerMotDePasseTest extends EasyMockSupport {
	
	private static final String CONF_DAO_FACTORY = "daoFactory";
	
	private static final String NOM_TEST = "tetsnom";
	private static final String PRENOM_TEST = "testprenom";
	private static final String IDENTIFIANT_TEST = "testidentifiant";
	private static final String MOT_DE_PASSE_TEST = "testmotdepasse";
	private static final String EMAIL_TEST = "test@email.testmail";
	private static final String NOUVEAU_MOT_DE_PASSE_TEST = "abcdefgh1";
	
	private static final String CHAMP_IDENTIFIANT = "identifiant";
	private static final String CHAMP_MOT_DE_PASSE = "motDePasse";
	private static final String CHAMP_EMAIL = "email";
	private static final String CHAMP_ANCIEN_MOT_DE_PASSE = "ancienMotDePasse";
	private static final String CHAMP_NOUVEAU_MOT_DE_PASSE = "nouveauMDP2";
	private static final String CHAMP_NOM = "nom";
	private static final String CHAMP_PRENOM = "prenom";
	
	private static final String VUE_FORM = "/WEB-INF/formulaires/changerMotDePasse.jsp";
	private static final String VUE_DASHBOARD = "/WEB-INF/formulaires/dashboard.jsp";
	
	private static Utilisateur utilisateur;
	private static UtilisateurDAO utilisateurDAO;
	private static NotificationDAO notificationDAO;

	@TestSubject
	private ChangerMotDePasse changerMotDePasse = new ChangerMotDePasse();
	
	@Mock
	private ServletConfig servletConfigMock;
	
	@Mock
	private ServletContext servletContextMock;
	
	@Mock(type = MockType.NICE)
	private HttpServletRequest httpServletRequestMock;
	
	@Mock
	private HttpServletResponse httpservletResponseMock;
	
	@Mock
	private HttpSession httpSessionMock;
	
	@Mock
	private RequestDispatcher requestDispatcherMock;
	
	@Mock
	private DAOFactory daoFactoryMock;

	/**
	 * Récupère des instances de DAO.
	 * 
	 * @throws Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		DAOFactory daoFactory = DAOFactory.getInstance();
		notificationDAO = daoFactory.getNotificationDao();
		utilisateurDAO = daoFactory.getUtilisateurDao();
		
		utilisateur = new Utilisateur();
		utilisateur.setNom(NOM_TEST);
		utilisateur.setPrenom(PRENOM_TEST);
		utilisateur.setIdentifiant(IDENTIFIANT_TEST);
		/* Hashage du mot de passe */
		String hash = BCrypt.hashpw(MOT_DE_PASSE_TEST, BCrypt.gensalt());
		utilisateur.setHash(hash);
		utilisateur.setEmail(EMAIL_TEST);
		utilisateur.setValide("oui");
		utilisateurDAO.creer(utilisateur);
		
		Long idUtilisateur = utilisateurDAO.trouver(utilisateur).get(0).getIdUtilisateur();
		utilisateur = new Utilisateur();
		utilisateur.setIdUtilisateur(idUtilisateur);
	}
	
	/**
	 * Supprime l'utilisateur inséré dans la BDD.
	 * 
	 * @throws Exception
	 */
	@AfterClass
	public static void tearDownAfterClass()throws Exception {
		utilisateurDAO.supprimer(utilisateur);
	}

	/**
	 * Teste la méthode protected void doGet(HttpServletRequest request, HttpServletResponse response).
	 * 
	 * @throws ServletException
	 * @throws IOException
	 */
	@Test
	public void test1DoGet() throws ServletException, IOException {
		/* Mock de la méthode init() afin de simplifier le test */
		this.changerMotDePasse = partialMockBuilder(ChangerMotDePasse.class).addMockedMethod("init").createMock();
		this.changerMotDePasse.init();
		expectLastCall();

		/* A la réception d'une requête GET, simple affichage du formulaire */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getRequestDispatcher(VUE_FORM)).andReturn(this.requestDispatcherMock);
		this.requestDispatcherMock.forward(this.httpServletRequestMock, this.httpservletResponseMock);
		expectLastCall();

		this.replayAll();

		/* Cycle de vie d'une servlet */
		this.changerMotDePasse.init(this.servletConfigMock);
		this.changerMotDePasse.doGet(this.httpServletRequestMock, this.httpservletResponseMock);
		this.changerMotDePasse.destroy();

		this.verifyAll();
	}
	
	/**
	 * Teste la méthode protected void doPost(HttpServletRequest request, HttpServletResponse response).
	 * 
	 * <p>Cas de l'échec du changement de mot de passe : le nouveau mot de passe n'est pas valide.</p>
	 * 
	 * @throws ServletException
	 * @throws IOException
	 */
	@Test
	public void test2DoPostErreurNouveauMDPInvalide() throws ServletException, IOException {
		/* Récupération d'instances de DAO */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
		expect(this.daoFactoryMock.getNotificationDao()).andReturn(notificationDAO);
		expect(this.daoFactoryMock.getUtilisateurDao()).andReturn(utilisateurDAO);
		
		/* Récupération des données saisies */
		expect(this.httpServletRequestMock.getParameter(CHAMP_IDENTIFIANT)).andReturn(IDENTIFIANT_TEST);
		expect(this.httpServletRequestMock.getParameter(CHAMP_EMAIL)).andReturn(EMAIL_TEST);
		expect(this.httpServletRequestMock.getParameter(CHAMP_ANCIEN_MOT_DE_PASSE)).andReturn(MOT_DE_PASSE_TEST);
		expect(this.httpServletRequestMock.getParameter(CHAMP_MOT_DE_PASSE)).andReturn(MOT_DE_PASSE_TEST);
		expect(this.httpServletRequestMock.getParameter(CHAMP_NOUVEAU_MOT_DE_PASSE)).andReturn(MOT_DE_PASSE_TEST);
		
		/* Ré-affichage du formulaire */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getRequestDispatcher(VUE_FORM)).andReturn(this.requestDispatcherMock);
		this.requestDispatcherMock.forward(this.httpServletRequestMock, this.httpservletResponseMock);
		expectLastCall();
		
		this.replayAll();
		
		/* Cycle de vie d'une servlet */
		this.changerMotDePasse.init(this.servletConfigMock);
		this.changerMotDePasse.doPost(this.httpServletRequestMock, this.httpservletResponseMock);
		this.changerMotDePasse.destroy();
		
		this.verifyAll();
	}
	
	/**
	 * Teste la méthode protected void doPost(HttpServletRequest request, HttpServletResponse response).
	 * 
	 * <p>Cas de l'échec du changement de mot de passe : mauvaise confirmation de mot de passe.</p>
	 * 
	 * @throws ServletException
	 * @throws IOException
	 */
	@Test
	public void test3DoPostErreurConfirmationMDP() throws ServletException, IOException {
		/* Récupération d'instances de DAO */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
		expect(this.daoFactoryMock.getNotificationDao()).andReturn(notificationDAO);
		expect(this.daoFactoryMock.getUtilisateurDao()).andReturn(utilisateurDAO);
		
		/* Récupération des données saisies */
		expect(this.httpServletRequestMock.getParameter(CHAMP_IDENTIFIANT)).andReturn(IDENTIFIANT_TEST);
		expect(this.httpServletRequestMock.getParameter(CHAMP_EMAIL)).andReturn(EMAIL_TEST);
		expect(this.httpServletRequestMock.getParameter(CHAMP_ANCIEN_MOT_DE_PASSE)).andReturn(MOT_DE_PASSE_TEST);
		expect(this.httpServletRequestMock.getParameter(CHAMP_MOT_DE_PASSE)).andReturn(NOUVEAU_MOT_DE_PASSE_TEST);
		expect(this.httpServletRequestMock.getParameter(CHAMP_NOUVEAU_MOT_DE_PASSE)).andReturn(MOT_DE_PASSE_TEST);
		
		/* Ré-affichage du formulaire */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getRequestDispatcher(VUE_FORM)).andReturn(this.requestDispatcherMock);
		this.requestDispatcherMock.forward(this.httpServletRequestMock, this.httpservletResponseMock);
		expectLastCall();
		
		this.replayAll();
		
		/* Cycle de vie d'une servlet */
		this.changerMotDePasse.init(this.servletConfigMock);
		this.changerMotDePasse.doPost(this.httpServletRequestMock, this.httpservletResponseMock);
		this.changerMotDePasse.destroy();
		
		this.verifyAll();
	}
	
	/**
	 * Teste la méthode protected void doPost(HttpServletRequest request, HttpServletResponse response).
	 * 
	 * <p>Cas du succès de changement de mot de passe.</p>
	 * 
	 * @throws ServletException
	 * @throws IOException
	 */
	@Test
	public void test4DoPostSucces() throws ServletException, IOException {
		/* Récupération d'instances de DAO */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
		expect(this.daoFactoryMock.getNotificationDao()).andReturn(notificationDAO);
		expect(this.daoFactoryMock.getUtilisateurDao()).andReturn(utilisateurDAO);
		
		/* Récupération des données saisies */
		expect(this.httpServletRequestMock.getParameter(CHAMP_IDENTIFIANT)).andReturn(IDENTIFIANT_TEST);
		expect(this.httpServletRequestMock.getParameter(CHAMP_EMAIL)).andReturn(EMAIL_TEST);
		expect(this.httpServletRequestMock.getParameter(CHAMP_ANCIEN_MOT_DE_PASSE)).andReturn(MOT_DE_PASSE_TEST);
		expect(this.httpServletRequestMock.getParameter(CHAMP_MOT_DE_PASSE)).andReturn(NOUVEAU_MOT_DE_PASSE_TEST);
		expect(this.httpServletRequestMock.getParameter(CHAMP_NOUVEAU_MOT_DE_PASSE)).andReturn(NOUVEAU_MOT_DE_PASSE_TEST);
		expect(this.httpServletRequestMock.getParameter(CHAMP_NOM)).andReturn(NOM_TEST);
		expect(this.httpServletRequestMock.getParameter(CHAMP_PRENOM)).andReturn(PRENOM_TEST);
		expect(this.httpServletRequestMock.getParameter(CHAMP_MOT_DE_PASSE)).andReturn(NOUVEAU_MOT_DE_PASSE_TEST);

		/* Ré-affichage du formulaire avec message de succès */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getRequestDispatcher(VUE_DASHBOARD)).andReturn(this.requestDispatcherMock);
		this.requestDispatcherMock.forward(this.httpServletRequestMock, this.httpservletResponseMock);
		expectLastCall();
		
		this.replayAll();
		
		/* Cycle de vie d'une servlet */
		this.changerMotDePasse.init(this.servletConfigMock);
		this.changerMotDePasse.doPost(this.httpServletRequestMock, this.httpservletResponseMock);
		this.changerMotDePasse.destroy();
		
		this.verifyAll();
	}
	
}