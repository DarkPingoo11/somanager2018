package fr.eseo.ld.servlets;

import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.expectLastCall;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.easymock.EasyMockRunner;
import org.easymock.EasyMockSupport;
import org.easymock.Mock;
import org.easymock.MockType;
import org.easymock.TestSubject;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import fr.eseo.ld.beans.Etudiant;
import fr.eseo.ld.beans.Utilisateur;
import fr.eseo.ld.dao.AnneeScolaireDAO;
import fr.eseo.ld.dao.DAOFactory;
import fr.eseo.ld.dao.EquipeDAO;
import fr.eseo.ld.dao.EtudiantDAO;
import fr.eseo.ld.dao.MatiereDAO;
import fr.eseo.ld.dao.NotePosterDAO;
import fr.eseo.ld.dao.NoteSoutenanceDAO;
import fr.eseo.ld.dao.PglEquipeDAO;
import fr.eseo.ld.dao.PglEtudiantDAO;
import fr.eseo.ld.dao.PglEtudiantEquipeDAO;
import fr.eseo.ld.dao.PglNoteDAO;
import fr.eseo.ld.dao.PosterDAO;
import fr.eseo.ld.dao.SoutenanceDAO;
import fr.eseo.ld.dao.SprintDAO;
import fr.eseo.ld.dao.SujetDAO;
import fr.eseo.ld.dao.UtilisateurDAO;

/**
 * Classe de tests unitaires JUnit 4 de la classe Noter.
 *
 * <p>
 * Utilisation de mocks (construits avec EasyMock).
 * </p>
 *
 * @version 1.0
 * @author Maxime LENORMAND
 *
 * @see fr.eseo.ld.servlets.Noter
 * @see org.junit
 * @see org.easymock
 */
@RunWith(EasyMockRunner.class)
public class NoterTest extends EasyMockSupport {

	private static final String CONF_DAO_FACTORY = "daoFactory";

	private static final String ATT_SESSION_USER = "utilisateur";
	private static final String TEST = "test";
	private static final Long ID_PROF = 16L;

	private static final String TYPE_SUIVI = "suivi";
	private static final String TYPE_POSTER = "poster";
	private static final String TYPE_SOUTENANCE = "soutenance";

	private static final String CHAMP_TYPE = "type";
	private static final String CHAMP_NOTE_INTERMEDIAIRE = "noteIntermediaire";
	private static final String CHAMP_NOTE_PROJET = "noteProjet";
	private static final String CHAMP_NOTE_POSTER = "notePoster";
	private static final String CHAMP_NOTE_SOUTENANCE = "noteSoutenance";

	private static final String VUE_FORM = "/WEB-INF/formulaires/gererNotation.jsp";

	private static Utilisateur utilisateur;
	private static UtilisateurDAO utilisateurDAO;
	private static SujetDAO sujetDAO;
	private static EtudiantDAO etudiantDAO;
	private static EquipeDAO equipeDAO;
	private static PosterDAO posterDAO;
	private static NotePosterDAO notePosterDAO;
	private static SoutenanceDAO soutenanceDAO;
	private static NoteSoutenanceDAO noteSoutenanceDAO;
	private static List<Etudiant> etudiants;

	private static MatiereDAO matiereDAO;

	private static PglEtudiantDAO pglEtudiantDAO;

	private static PglEquipeDAO pglEquipeDAO;

	private static SprintDAO sprintDAO;

	private static AnneeScolaireDAO anneeScolaireDAO;

	private static PglNoteDAO noteDAO;

	private static PglEtudiantEquipeDAO etudiantEquipeDAO;

	@TestSubject
	private Noter noter = new Noter();

	@Mock
	private ServletConfig servletConfigMock;

	@Mock
	private ServletContext servletContextMock;

	@Mock(type = MockType.NICE)
	private HttpServletRequest httpServletRequestMock;

	@Mock
	private HttpServletResponse httpservletResponseMock;

	@Mock
	private HttpSession httpSessionMock;

	@Mock
	private RequestDispatcher requestDispatcherMock;

	@Mock
	private DAOFactory daoFactoryMock;

	/**
	 * Récupère des instances de DAO.
	 * 
	 * @throws Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		/* Instanciations de DAO */
		DAOFactory daoFactory = DAOFactory.getInstance();
		utilisateurDAO = daoFactory.getUtilisateurDao();
		sujetDAO = daoFactory.getSujetDao();
		etudiantDAO = daoFactory.getEtudiantDao();
		equipeDAO = daoFactory.getEquipeDAO();
		posterDAO = daoFactory.getPosterDao();
		notePosterDAO = daoFactory.getNotePosterDao();
		soutenanceDAO = daoFactory.getSoutenanceDAO();
		noteSoutenanceDAO = daoFactory.getNoteSoutenanceDAO();
		matiereDAO = daoFactory.getMatiereDAO();
		pglEtudiantDAO = daoFactory.getPglEtudiantDAO();
		pglEquipeDAO = daoFactory.getPglEquipeDAO();
		sprintDAO = daoFactory.getSprintDAO();
		anneeScolaireDAO = daoFactory.getAnneeScolaireDAO();
		noteDAO = daoFactory.getPglNoteDAO();
		etudiantEquipeDAO = daoFactory.getPglEtudiantEquipeDAO();

		/* Creation de la liste d'étudiants */
		etudiants = etudiantDAO.lister();
	}

	/**
	 * Supprime l'étudiant test inséré dans la BDD.
	 * 
	 * @throws Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * Teste la méthode protected void doGet(HttpServletRequest request,
	 * HttpServletResponse response).
	 * 
	 * @throws ServletException
	 * @throws IOException
	 */
	@Test
	public void testDoGet() throws ServletException, IOException {
		/* Récupération d'instances de DAO */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
		expect(this.daoFactoryMock.getUtilisateurDao()).andReturn(utilisateurDAO);
		expect(this.daoFactoryMock.getSujetDao()).andReturn(sujetDAO);
		expect(this.daoFactoryMock.getEtudiantDao()).andReturn(etudiantDAO);
		expect(this.daoFactoryMock.getEquipeDAO()).andReturn(equipeDAO);
		expect(this.daoFactoryMock.getPosterDao()).andReturn(posterDAO);
		expect(this.daoFactoryMock.getNotePosterDao()).andReturn(notePosterDAO);
		expect(this.daoFactoryMock.getSoutenanceDAO()).andReturn(soutenanceDAO);
		expect(this.daoFactoryMock.getNoteSoutenanceDAO()).andReturn(noteSoutenanceDAO);
		expect(this.daoFactoryMock.getMatiereDAO()).andReturn(matiereDAO);
		expect(this.daoFactoryMock.getPglEtudiantDAO()).andReturn(pglEtudiantDAO);
		expect(this.daoFactoryMock.getPglEquipeDAO()).andReturn(pglEquipeDAO);
		expect(this.daoFactoryMock.getSprintDAO()).andReturn(sprintDAO);
		expect(this.daoFactoryMock.getAnneeScolaireDAO()).andReturn(anneeScolaireDAO);
		expect(this.daoFactoryMock.getPglNoteDAO()).andReturn(noteDAO);
		expect(this.daoFactoryMock.getPglEtudiantEquipeDAO()).andReturn(etudiantEquipeDAO);

		/* Récupération de l'Utilisateur en session */
		expect(this.httpServletRequestMock.getSession()).andReturn(this.httpSessionMock);
		final Utilisateur utilisateur = new Utilisateur();
		final Long idUtilisateur = ID_PROF;
		utilisateur.setIdUtilisateur(idUtilisateur);
		expect(this.httpSessionMock.getAttribute(ATT_SESSION_USER)).andReturn(utilisateur);

		/* Affichage du formulaire */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getRequestDispatcher(VUE_FORM)).andReturn(this.requestDispatcherMock);
		this.requestDispatcherMock.forward(this.httpServletRequestMock, this.httpservletResponseMock);
		expectLastCall();

		this.replayAll();

		/* Cycle de vie d'une servlet */
		this.noter.init(this.servletConfigMock);
		this.noter.doGet(this.httpServletRequestMock, this.httpservletResponseMock);
		this.noter.destroy();

		this.verifyAll();
	}

	/**
	 * Teste la méthode protected void doPost(HttpServletRequest request,
	 * HttpServletResponse response).
	 * 
	 * <p>
	 * Cas de la modification des notes de suivi.
	 * </p>
	 * 
	 * @throws ServletException
	 * @throws IOException
	 */
	@Test
	public void testDoPostNotesSuivi() throws ServletException, IOException {
		/* Récupération d'instances de DAO */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
		expect(this.daoFactoryMock.getUtilisateurDao()).andReturn(utilisateurDAO);
		expect(this.daoFactoryMock.getSujetDao()).andReturn(sujetDAO);
		expect(this.daoFactoryMock.getEtudiantDao()).andReturn(etudiantDAO);
		expect(this.daoFactoryMock.getEquipeDAO()).andReturn(equipeDAO);
		expect(this.daoFactoryMock.getPosterDao()).andReturn(posterDAO);
		expect(this.daoFactoryMock.getNotePosterDao()).andReturn(notePosterDAO);
		expect(this.daoFactoryMock.getSoutenanceDAO()).andReturn(soutenanceDAO);
		expect(this.daoFactoryMock.getNoteSoutenanceDAO()).andReturn(noteSoutenanceDAO);
		expect(this.daoFactoryMock.getMatiereDAO()).andReturn(matiereDAO);
		expect(this.daoFactoryMock.getPglEtudiantDAO()).andReturn(pglEtudiantDAO);
		expect(this.daoFactoryMock.getPglEquipeDAO()).andReturn(pglEquipeDAO);
		expect(this.daoFactoryMock.getSprintDAO()).andReturn(sprintDAO);
		expect(this.daoFactoryMock.getAnneeScolaireDAO()).andReturn(anneeScolaireDAO);
		expect(this.daoFactoryMock.getPglNoteDAO()).andReturn(noteDAO);
		expect(this.daoFactoryMock.getPglEtudiantEquipeDAO()).andReturn(etudiantEquipeDAO);

		/* Récupération de l'Utilisateur en session */
		expect(this.httpServletRequestMock.getSession()).andReturn(this.httpSessionMock);
		final Utilisateur utilisateur = new Utilisateur();
		final Long idUtilisateur = ID_PROF;
		utilisateur.setIdUtilisateur(idUtilisateur);
		expect(this.httpSessionMock.getAttribute(ATT_SESSION_USER)).andReturn(utilisateur);

		/* Récupération des données saisies */
		for (Etudiant etudiant : etudiants) {
			expect(this.httpServletRequestMock.getParameter(CHAMP_NOTE_INTERMEDIAIRE + etudiant.getIdEtudiant()))
					.andReturn("0");
			expect(this.httpServletRequestMock.getParameter(CHAMP_NOTE_PROJET + etudiant.getIdEtudiant()))
					.andReturn("0");
		}
		expect(this.httpServletRequestMock.getParameter(CHAMP_TYPE)).andReturn(TYPE_SUIVI);

		/* Récupération de l'Utilisateur en session */
		expect(this.httpServletRequestMock.getSession()).andReturn(this.httpSessionMock);
		expect(this.httpSessionMock.getAttribute(ATT_SESSION_USER)).andReturn(utilisateur);

		/* Affichage du formulaire */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getRequestDispatcher(VUE_FORM)).andReturn(this.requestDispatcherMock);
		this.requestDispatcherMock.forward(this.httpServletRequestMock, this.httpservletResponseMock);
		expectLastCall();

		this.replayAll();

		/* Cycle de vie d'une servlet */
		this.noter.init(this.servletConfigMock);
		this.noter.doPost(this.httpServletRequestMock, this.httpservletResponseMock);
		this.noter.destroy();

		this.verifyAll();
	}

	/**
	 * Teste la méthode protected void doPost(HttpServletRequest request,
	 * HttpServletResponse response).
	 * 
	 * <p>
	 * Cas de la modification des notes de poster.
	 * </p>
	 * 
	 * @throws ServletException
	 * @throws IOException
	 */
	@Test
	public void testDoPostNotesPoster() throws ServletException, IOException {
		/* Récupération d'instances de DAO */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
		expect(this.daoFactoryMock.getUtilisateurDao()).andReturn(utilisateurDAO);
		expect(this.daoFactoryMock.getSujetDao()).andReturn(sujetDAO);
		expect(this.daoFactoryMock.getEtudiantDao()).andReturn(etudiantDAO);
		expect(this.daoFactoryMock.getEquipeDAO()).andReturn(equipeDAO);
		expect(this.daoFactoryMock.getPosterDao()).andReturn(posterDAO);
		expect(this.daoFactoryMock.getNotePosterDao()).andReturn(notePosterDAO);
		expect(this.daoFactoryMock.getSoutenanceDAO()).andReturn(soutenanceDAO);
		expect(this.daoFactoryMock.getNoteSoutenanceDAO()).andReturn(noteSoutenanceDAO);
		expect(this.daoFactoryMock.getMatiereDAO()).andReturn(matiereDAO);
		expect(this.daoFactoryMock.getPglEtudiantDAO()).andReturn(pglEtudiantDAO);
		expect(this.daoFactoryMock.getPglEquipeDAO()).andReturn(pglEquipeDAO);
		expect(this.daoFactoryMock.getSprintDAO()).andReturn(sprintDAO);
		expect(this.daoFactoryMock.getAnneeScolaireDAO()).andReturn(anneeScolaireDAO);
		expect(this.daoFactoryMock.getPglNoteDAO()).andReturn(noteDAO);
		expect(this.daoFactoryMock.getPglEtudiantEquipeDAO()).andReturn(etudiantEquipeDAO);

		/* Récupération de l'Utilisateur en session */
		expect(this.httpServletRequestMock.getSession()).andReturn(this.httpSessionMock);
		final Utilisateur utilisateur = new Utilisateur();
		final Long idUtilisateur = ID_PROF;
		utilisateur.setIdUtilisateur(idUtilisateur);
		expect(this.httpSessionMock.getAttribute(ATT_SESSION_USER)).andReturn(utilisateur);

		/* Récupération des données saisies */
		expect(this.httpServletRequestMock.getParameter(CHAMP_TYPE)).andReturn(TYPE_POSTER);
		for (Etudiant etudiant : etudiants) {
			expect(this.httpServletRequestMock.getParameter(CHAMP_NOTE_POSTER + etudiant.getIdEtudiant()))
					.andReturn("0");
		}

		/* Récupération de l'Utilisateur en session */
		expect(this.httpServletRequestMock.getSession()).andReturn(this.httpSessionMock);
		expect(this.httpSessionMock.getAttribute(ATT_SESSION_USER)).andReturn(utilisateur);

		/* Affichage du formulaire */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getRequestDispatcher(VUE_FORM)).andReturn(this.requestDispatcherMock);
		this.requestDispatcherMock.forward(this.httpServletRequestMock, this.httpservletResponseMock);
		expectLastCall();

		this.replayAll();

		/* Cycle de vie d'une servlet */
		this.noter.init(this.servletConfigMock);
		this.noter.doPost(this.httpServletRequestMock, this.httpservletResponseMock);
		this.noter.destroy();

		this.verifyAll();
	}

	/**
	 * Teste la méthode protected void doPost(HttpServletRequest request,
	 * HttpServletResponse response).
	 * 
	 * <p>
	 * Cas de la modification des notes de soutenance.
	 * </p>
	 * 
	 * @throws ServletException
	 * @throws IOException
	 */
	@Test
	public void testDoPostNotesSoutenance() throws ServletException, IOException {
		/* Récupération d'instances de DAO */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
		expect(this.daoFactoryMock.getUtilisateurDao()).andReturn(utilisateurDAO);
		expect(this.daoFactoryMock.getSujetDao()).andReturn(sujetDAO);
		expect(this.daoFactoryMock.getEtudiantDao()).andReturn(etudiantDAO);
		expect(this.daoFactoryMock.getEquipeDAO()).andReturn(equipeDAO);
		expect(this.daoFactoryMock.getPosterDao()).andReturn(posterDAO);
		expect(this.daoFactoryMock.getNotePosterDao()).andReturn(notePosterDAO);
		expect(this.daoFactoryMock.getSoutenanceDAO()).andReturn(soutenanceDAO);
		expect(this.daoFactoryMock.getNoteSoutenanceDAO()).andReturn(noteSoutenanceDAO);
		expect(this.daoFactoryMock.getMatiereDAO()).andReturn(matiereDAO);
		expect(this.daoFactoryMock.getPglEtudiantDAO()).andReturn(pglEtudiantDAO);
		expect(this.daoFactoryMock.getPglEquipeDAO()).andReturn(pglEquipeDAO);
		expect(this.daoFactoryMock.getSprintDAO()).andReturn(sprintDAO);
		expect(this.daoFactoryMock.getAnneeScolaireDAO()).andReturn(anneeScolaireDAO);
		expect(this.daoFactoryMock.getPglNoteDAO()).andReturn(noteDAO);
		expect(this.daoFactoryMock.getPglEtudiantEquipeDAO()).andReturn(etudiantEquipeDAO);

		/* Récupération de l'Utilisateur en session */
		expect(this.httpServletRequestMock.getSession()).andReturn(this.httpSessionMock);
		final Utilisateur utilisateur = new Utilisateur();
		final Long idUtilisateur = ID_PROF;
		utilisateur.setIdUtilisateur(idUtilisateur);
		expect(this.httpSessionMock.getAttribute(ATT_SESSION_USER)).andReturn(utilisateur);

		/* Récupération des données saisies */
		expect(this.httpServletRequestMock.getParameter(CHAMP_TYPE)).andReturn(TYPE_SOUTENANCE);
		for (Etudiant etudiant : etudiants) {
			expect(this.httpServletRequestMock.getParameter(CHAMP_NOTE_SOUTENANCE + etudiant.getIdEtudiant()))
					.andReturn("0");
		}

		/* Récupération de l'Utilisateur en session */
		expect(this.httpServletRequestMock.getSession()).andReturn(this.httpSessionMock);
		expect(this.httpSessionMock.getAttribute(ATT_SESSION_USER)).andReturn(utilisateur);

		/* Affichage du formulaire */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getRequestDispatcher(VUE_FORM)).andReturn(this.requestDispatcherMock);
		this.requestDispatcherMock.forward(this.httpServletRequestMock, this.httpservletResponseMock);
		expectLastCall();

		this.replayAll();

		/* Cycle de vie d'une servlet */
		this.noter.init(this.servletConfigMock);
		this.noter.doPost(this.httpServletRequestMock, this.httpservletResponseMock);
		this.noter.destroy();

		this.verifyAll();
	}

}