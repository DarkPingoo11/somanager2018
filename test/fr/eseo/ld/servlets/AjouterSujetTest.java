package fr.eseo.ld.servlets;

import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.expectLastCall;
import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.easymock.EasyMockRunner;
import org.easymock.EasyMockSupport;
import org.easymock.Mock;
import org.easymock.TestSubject;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

import fr.eseo.ld.beans.Sujet;
import fr.eseo.ld.beans.Utilisateur;
import fr.eseo.ld.dao.DAOFactory;
import fr.eseo.ld.dao.NotificationDAO;
import fr.eseo.ld.dao.OptionESEODAO;
import fr.eseo.ld.dao.ProfesseurDAO;
import fr.eseo.ld.dao.SujetDAO;
import fr.eseo.ld.dao.UtilisateurDAO;

/**
 * Classe de tests unitaires JUnit 4 de la classe AjouterSujet.
 *
 * <p>Utilisation de mocks (construits avec EasyMock).</p>
 *
 * @version 1.0
 * @author Maxime LENORMAND
 *
 * @see fr.eseo.ld.servlets.AjouterSujet
 * @see org.junit
 * @see org.easymock
 */
@RunWith(EasyMockRunner.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class AjouterSujetTest extends EasyMockSupport {
	
	private static final String CONF_DAO_FACTORY = "daoFactory";
	
	private static final String TEST = "testSujet";
	
	private static final String ATT_RESULTAT = "resultat";
	private static final String ATT_ERREURS = "erreurs";
	
	private static final String ATT_SESSION_USER = "utilisateur";
	private static final Long ID_UTILISATEUR = 11L;
	
	private static final String CHAMP_TITRE = "titre";
	private static final String CHAMP_DESCRIPTION = "description";
	private static final String CHAMP_NBR_MIN_ELEVES = "nbrMinEleves";
	private static final String CHAMP_NBR_MAX_ELEVES = "nbrMaxEleves";
	private static final String CHAMP_CONTRAT_PRO = "contratPro";
	private static final String CHAMP_CONFIDENTIALITE = "confidentialite";
	private static final String CHAMP_OPTIONS = "options";
	private static final String CHAMP_OPTION_CC = "CC";
	private static final String CHAMP_OPTION_IIT = "IIT";
	private static final String CHAMP_OPTION_LD = "LD";
	private static final String CHAMP_OPTION_SE = "SE";
	private static final String CHAMP_OPTION_BIO = "BIO";
	private static final String CHAMP_OPTION_OC = "OC";
	private static final String CHAMP_OPTION_NRJ = "NRJ";
	private static final String CHAMP_OPTION_DSMT = "DSMT";
	private static final String CHAMP_OPTION_BD = "BD";
	private static final String CHAMP_INTERET_JPO = "JPO";
	private static final String CHAMP_INTERET_GP = "GP";
	private static final String CHAMP_AUTRES = "autres";
	private static final String CHAMP_LIENS = "liens";
	private static final String ATT_LIEN = "lienForm";
	
	private static final String CONTEXT_PATH = "SoManager/AjouterSujet";
	
	private static final String VUE_INDEX = "/Dashboard";
	private static final String VUE_FORM = "/WEB-INF/formulaires/ajouterSujet.jsp";
	
	private static SujetDAO sujetDAO;
	private static OptionESEODAO optionESEODAO;
	private static NotificationDAO notificationDAO;
	private static ProfesseurDAO professeurDAO;
	private static UtilisateurDAO utilisateurDAO;
	
	@TestSubject
	private AjouterSujet ajouterSujet = new AjouterSujet();
	
	@Mock
	private ServletConfig servletConfigMock;
	
	@Mock
	private ServletContext servletContextMock;
	
	@Mock
	private HttpServletRequest httpServletRequestMock;
	
	@Mock
	private HttpServletResponse httpservletResponseMock;
	
	@Mock
	private HttpSession httpSessionMock;
	
	@Mock
	private RequestDispatcher requestDispatcherMock;
	
	@Mock
	private DAOFactory daoFactoryMock;

	/**
	 * Récupère des instances de DAO.
	 * 
	 * @throws Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		DAOFactory daoFactory = DAOFactory.getInstance();
		sujetDAO = daoFactory.getSujetDao();
		optionESEODAO = daoFactory.getOptionESEODAO();
		notificationDAO = daoFactory.getNotificationDao();
		professeurDAO = daoFactory.getProfesseurDAO();
		utilisateurDAO = daoFactory.getUtilisateurDao();
	}

	/**
	 * Supprime le sujet inséré dans la BDD.
	 * 
	 * @throws Exception
	 */
	@AfterClass
	public static void tearDownAfterClass()throws Exception {
		final Sujet sujet = new Sujet();
		sujet.setTitre(TEST);
		sujetDAO.supprimer(sujet);
	}

	/**
	 * Teste la méthode protected void doGet(HttpServletRequest request, HttpServletResponse response).
	 * 
	 * @throws ServletException
	 * @throws IOException
	 */
	@Test
	public void test1DoGet() throws ServletException, IOException {
		/* Mock de la méthode init() afin de simplifier le test */
		this.ajouterSujet = partialMockBuilder(AjouterSujet.class).addMockedMethod("init").createMock();
		this.ajouterSujet.init();
		expectLastCall();
		
		//Affichage de lien google nul
		this.httpServletRequestMock.setAttribute(ATT_LIEN, null);

		/* A la réception d'une requête GET, simple affichage du formulaire */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getRequestDispatcher(VUE_FORM)).andReturn(this.requestDispatcherMock);
		this.requestDispatcherMock.forward(this.httpServletRequestMock, this.httpservletResponseMock);
		expectLastCall();

		this.replayAll();

		/* Cycle de vie d'une servlet */
		this.ajouterSujet.init(this.servletConfigMock);
		this.ajouterSujet.doGet(this.httpServletRequestMock, this.httpservletResponseMock);
		this.ajouterSujet.destroy();

		this.verifyAll();
	}
	
	/**
	 * Teste la méthode protected void doPost(HttpServletRequest request, HttpServletResponse response).
	 * 
	 * <p>Cas du succès de l'ajout du sujet : affichage du message de succès
	 * et insertion du sujet dans la BDD.</p>
	 * 
	 * @throws ServletException
	 * @throws IOException
	 */
	@Test
	public void test2DoPostSucces() throws ServletException, IOException {
		/* Récupération d'instances de DAO */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
		expect(this.daoFactoryMock.getSujetDao()).andReturn(sujetDAO);
		expect(this.daoFactoryMock.getNotificationDao()).andReturn(notificationDAO);
		expect(this.daoFactoryMock.getOptionESEODAO()).andReturn(optionESEODAO);
		expect(this.daoFactoryMock.getProfesseurDAO()).andReturn(professeurDAO);
		expect(this.daoFactoryMock.getUtilisateurDao()).andReturn(utilisateurDAO);
		
		/* Récupération de l'ID de l'Utilisateur en session */
		final Utilisateur utilisateur = new Utilisateur();
		final Long idUtilisateur = ID_UTILISATEUR;
		utilisateur.setIdUtilisateur(idUtilisateur);
		expect(this.httpServletRequestMock.getSession()).andReturn(this.httpSessionMock);
		expect(this.httpSessionMock.getAttribute(ATT_SESSION_USER)).andReturn(utilisateur);
		
		/* Récupération des données saisies */
		expect(this.httpServletRequestMock.getParameter(CHAMP_TITRE)).andReturn(TEST);
		expect(this.httpServletRequestMock.getParameter(CHAMP_DESCRIPTION)).andReturn(TEST);
		expect(this.httpServletRequestMock.getParameter(CHAMP_NBR_MIN_ELEVES)).andReturn("1");
		expect(this.httpServletRequestMock.getParameter(CHAMP_NBR_MAX_ELEVES)).andReturn("2");
		expect(this.httpServletRequestMock.getParameter(CHAMP_CONTRAT_PRO)).andReturn("true");
		expect(this.httpServletRequestMock.getParameter(CHAMP_CONFIDENTIALITE)).andReturn("false");
		expect(this.httpServletRequestMock.getParameter(CHAMP_OPTION_CC)).andReturn(null);
		expect(this.httpServletRequestMock.getParameter(CHAMP_OPTION_IIT)).andReturn(null);
		expect(this.httpServletRequestMock.getParameter(CHAMP_OPTION_LD)).andReturn("LD");
		expect(this.httpServletRequestMock.getParameter(CHAMP_OPTION_SE)).andReturn(null);
		expect(this.httpServletRequestMock.getParameter(CHAMP_OPTION_BIO)).andReturn("BIO");
		expect(this.httpServletRequestMock.getParameter(CHAMP_OPTION_OC)).andReturn(null);
		expect(this.httpServletRequestMock.getParameter(CHAMP_OPTION_NRJ)).andReturn("NRJ");
		expect(this.httpServletRequestMock.getParameter(CHAMP_OPTION_DSMT)).andReturn("DMST");
		expect(this.httpServletRequestMock.getParameter(CHAMP_OPTION_BD)).andReturn("BD");
		expect(this.httpServletRequestMock.getParameter("")).andReturn(null);
		expect(this.httpServletRequestMock.getParameter(CHAMP_INTERET_JPO)).andReturn("JPO");
		expect(this.httpServletRequestMock.getParameter(CHAMP_INTERET_GP)).andReturn(null);
		expect(this.httpServletRequestMock.getParameter(CHAMP_AUTRES)).andReturn(null);
		expect(this.httpServletRequestMock.getParameter(CHAMP_LIENS)).andReturn(TEST);

		/* Redirection vers la page d'accueil */
		expect(this.httpServletRequestMock.getContextPath()).andReturn(CONTEXT_PATH);
		this.httpservletResponseMock.sendRedirect(CONTEXT_PATH + VUE_INDEX);
		expectLastCall();
		
		this.replayAll();
		
		/* Cycle de vie d'une servlet */
		this.ajouterSujet.init(this.servletConfigMock);
		this.ajouterSujet.doPost(this.httpServletRequestMock, this.httpservletResponseMock);
		this.ajouterSujet.destroy();
		
		/* Vérification de l'insertion du sujet dans la BDD */
		final Sujet sujet = new Sujet();
		final String titreAttendu = TEST;
		sujet.setTitre(titreAttendu);
		final List<Sujet> sujetsTrouves = sujetDAO.trouver(sujet);
		String titreTrouve = "";
		if (!sujetsTrouves.isEmpty()) {
			titreTrouve = sujetsTrouves.get(0).getTitre();
		}
		assertEquals("Le sujet n'a pas été inséré dans la BDD", titreAttendu, titreTrouve);
		
		this.verifyAll();
	}
	
	/**
	 * Teste la méthode protected void doPost(HttpServletRequest request, HttpServletResponse response).
	 * 
	 * <p>Cas de l'échec de l'ajout du sujet 
	 * (ajout d'un sujet possédant un nombre minimum d'élèves 
	 * supérieur au nombre maximum d'élèves) : affichage des erreurs.</p>
	 * 
	 * @throws ServletException
	 * @throws IOException
	 */
	@Test
	public void test3DoPostEchecNbrEleves() throws ServletException, IOException {
		/* Récupération d'instances de DAO */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
		expect(this.daoFactoryMock.getSujetDao()).andReturn(sujetDAO);
		expect(this.daoFactoryMock.getNotificationDao()).andReturn(notificationDAO);
		expect(this.daoFactoryMock.getOptionESEODAO()).andReturn(optionESEODAO);
		expect(this.daoFactoryMock.getProfesseurDAO()).andReturn(professeurDAO);
		expect(this.daoFactoryMock.getUtilisateurDao()).andReturn(utilisateurDAO);
		
		/* Récupération de la session */
		expect(this.httpServletRequestMock.getSession()).andReturn(this.httpSessionMock);
		
		/* Récupération des données saisies */
		expect(this.httpServletRequestMock.getParameter(CHAMP_TITRE)).andReturn(TEST);
		expect(this.httpServletRequestMock.getParameter(CHAMP_DESCRIPTION)).andReturn(TEST);
		expect(this.httpServletRequestMock.getParameter(CHAMP_LIENS)).andReturn(TEST);
		expect(this.httpServletRequestMock.getParameter(CHAMP_NBR_MIN_ELEVES)).andReturn("2");
		expect(this.httpServletRequestMock.getParameter(CHAMP_NBR_MAX_ELEVES)).andReturn("1");
		expect(this.httpServletRequestMock.getParameter(CHAMP_CONTRAT_PRO)).andReturn("true");
		expect(this.httpServletRequestMock.getParameter(CHAMP_CONFIDENTIALITE)).andReturn("false");
		expect(this.httpServletRequestMock.getParameter(CHAMP_OPTION_CC)).andReturn(null);
		expect(this.httpServletRequestMock.getParameter(CHAMP_OPTION_IIT)).andReturn(null);
		expect(this.httpServletRequestMock.getParameter(CHAMP_OPTION_LD)).andReturn("LD");
		expect(this.httpServletRequestMock.getParameter(CHAMP_OPTION_SE)).andReturn(null);
		expect(this.httpServletRequestMock.getParameter(CHAMP_OPTION_BIO)).andReturn("BIO");
		expect(this.httpServletRequestMock.getParameter(CHAMP_OPTION_OC)).andReturn(null);
		expect(this.httpServletRequestMock.getParameter(CHAMP_OPTION_NRJ)).andReturn("NRJ");
		expect(this.httpServletRequestMock.getParameter(CHAMP_OPTION_DSMT)).andReturn("DMST");
		expect(this.httpServletRequestMock.getParameter(CHAMP_OPTION_BD)).andReturn("BD");
		expect(this.httpServletRequestMock.getParameter("")).andReturn(null);

		/* Mise en place des erreurs dans la requete */
		final Map<String, String> erreurs = new HashMap<>();
		erreurs.put(CHAMP_NBR_MIN_ELEVES, "Nombre minimum d'élèves supérieur au Nombre maximum d'élèves. ");
		this.httpServletRequestMock.setAttribute(ATT_ERREURS, erreurs);
		expectLastCall();
		
		/* Mise en place du resultat dans la requete */
		final String resultat = "Échec de l'ajout du sujet. ";
		this.httpServletRequestMock.setAttribute(ATT_RESULTAT, resultat);
		expectLastCall();

		/* Ré-affichage du formulaire avec les erreurs */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getRequestDispatcher(VUE_FORM)).andReturn(this.requestDispatcherMock);
		this.requestDispatcherMock.forward(this.httpServletRequestMock, this.httpservletResponseMock);
		expectLastCall();
		
		this.replayAll();
		
		/* Cycle de vie d'une servlet */
		this.ajouterSujet.init(this.servletConfigMock);
		this.ajouterSujet.doPost(this.httpServletRequestMock, this.httpservletResponseMock);
		this.ajouterSujet.destroy();
		
		/* Vérification que le sujet n'a pas été inséré dans la BDD */
		final Sujet sujet = new Sujet();
		final String titre = TEST;
		sujet.setTitre(titre);
		final int nbSujetsAttendus = 1; // un seul : le sujet inséré lors du test2
		final int nbSujetsTrouves = sujetDAO.trouver(sujet).size();
		assertEquals("Le sujet a été inséré dans la BDD", nbSujetsAttendus, nbSujetsTrouves);
		
		this.verifyAll();
	}
	
	/**
	 * Teste la méthode protected void doPost(HttpServletRequest request, HttpServletResponse response).
	 * 
	 * <p>Cas de l'échec de l'ajout du sujet 
	 * (ajout d'un sujet ne possédant aucune option) : affichage des erreurs.</p>
	 * 
	 * @throws ServletException
	 * @throws IOException
	 */
	@Test
	public void test4DoPostEchecOptions() throws ServletException, IOException {
		/* Récupération d'instances de DAO */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
		expect(this.daoFactoryMock.getSujetDao()).andReturn(sujetDAO);
		expect(this.daoFactoryMock.getNotificationDao()).andReturn(notificationDAO);
		expect(this.daoFactoryMock.getOptionESEODAO()).andReturn(optionESEODAO);
		expect(this.daoFactoryMock.getProfesseurDAO()).andReturn(professeurDAO);
		expect(this.daoFactoryMock.getUtilisateurDao()).andReturn(utilisateurDAO);
		
		/* Récupération de la session */
		expect(this.httpServletRequestMock.getSession()).andReturn(this.httpSessionMock);
		
		/* Récupération des données saisies */
		expect(this.httpServletRequestMock.getParameter(CHAMP_TITRE)).andReturn(TEST);
		expect(this.httpServletRequestMock.getParameter(CHAMP_DESCRIPTION)).andReturn(TEST);
		expect(this.httpServletRequestMock.getParameter(CHAMP_LIENS)).andReturn(TEST);
		expect(this.httpServletRequestMock.getParameter(CHAMP_NBR_MIN_ELEVES)).andReturn("1");
		expect(this.httpServletRequestMock.getParameter(CHAMP_NBR_MAX_ELEVES)).andReturn("2");
		expect(this.httpServletRequestMock.getParameter(CHAMP_CONTRAT_PRO)).andReturn("true");
		expect(this.httpServletRequestMock.getParameter(CHAMP_CONFIDENTIALITE)).andReturn("false");
		expect(this.httpServletRequestMock.getParameter(CHAMP_OPTION_CC)).andReturn(null);
		expect(this.httpServletRequestMock.getParameter(CHAMP_OPTION_IIT)).andReturn(null);
		expect(this.httpServletRequestMock.getParameter(CHAMP_OPTION_LD)).andReturn(null);
		expect(this.httpServletRequestMock.getParameter(CHAMP_OPTION_SE)).andReturn(null);
		expect(this.httpServletRequestMock.getParameter(CHAMP_OPTION_BIO)).andReturn(null);
		expect(this.httpServletRequestMock.getParameter(CHAMP_OPTION_OC)).andReturn(null);
		expect(this.httpServletRequestMock.getParameter(CHAMP_OPTION_NRJ)).andReturn(null);
		expect(this.httpServletRequestMock.getParameter(CHAMP_OPTION_DSMT)).andReturn(null);
		expect(this.httpServletRequestMock.getParameter(CHAMP_OPTION_BD)).andReturn(null);
		expect(this.httpServletRequestMock.getParameter("")).andReturn(null);

		/* Mise en place des erreurs dans la requete */
		final Map<String, String> erreurs = new HashMap<>();
		erreurs.put(CHAMP_OPTIONS, "Aucune option sélectionnée. ");
		this.httpServletRequestMock.setAttribute(ATT_ERREURS, erreurs);
		expectLastCall();
		
		/* Mise en place du resultat dans la requete */
		final String resultat = "Échec de l'ajout du sujet. ";
		this.httpServletRequestMock.setAttribute(ATT_RESULTAT, resultat);
		expectLastCall();

		/* Ré-affichage du formulaire avec les erreurs */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getRequestDispatcher(VUE_FORM)).andReturn(this.requestDispatcherMock);
		this.requestDispatcherMock.forward(this.httpServletRequestMock, this.httpservletResponseMock);
		expectLastCall();
		
		this.replayAll();
		
		/* Cycle de vie d'une servlet */
		this.ajouterSujet.init(this.servletConfigMock);
		this.ajouterSujet.doPost(this.httpServletRequestMock, this.httpservletResponseMock);
		this.ajouterSujet.destroy();
		
		/* Vérification que le sujet n'a pas été inséré dans la BDD */
		final Sujet sujet = new Sujet();
		final String titre = TEST;
		sujet.setTitre(titre);
		final int nbSujetsAttendus = 1; // un seul : le sujet inséré lors du test2
		final int nbSujetsTrouves = sujetDAO.trouver(sujet).size();
		assertEquals("Le sujet a été inséré dans la BDD", nbSujetsAttendus, nbSujetsTrouves);
		
		this.verifyAll();
	}

}