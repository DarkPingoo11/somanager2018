package fr.eseo.ld.servlets.ajax;

import fr.eseo.ld.beans.Utilisateur;
import fr.eseo.ld.dao.DAOFactory;
import fr.eseo.ld.dao.UtilisateurDAO;
import fr.eseo.ld.servlets.ServletUtilitaire;
import org.easymock.*;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import static junit.framework.TestCase.assertEquals;
import static org.easymock.EasyMock.*;

/**
 * Classe de test de InfosUtilisateurDAO
 *
 * @author Tristan LE GACQUE
 * @see InfosUtilisateur
 **/
@RunWith(EasyMockRunner.class)
public class InfosUtilisateurTest extends EasyMockSupport {

    @Mock
    HttpServletResponse response;

    @Mock
    HttpServletRequest request;

    @Mock
    HttpSession session;

    @Mock
    DAOFactory daoFactory;

    @Mock
    UtilisateurDAO utilisateurDAO;

    @Mock
    ServletContext servletContext;

    @Mock
    private ServletConfig servletConfigMock;

    @Test
    public void testInfosUtilisateur() throws ServletException, IOException {
        //Définition du mock
        Utilisateur utilisateur = new Utilisateur();
        utilisateur.setIdUtilisateur(12L);
        Utilisateur utilisateurT = new Utilisateur();
        utilisateurT.setIdUtilisateur(12L);
        utilisateurT.setPrenom("Prenom");
        utilisateurT.setNom("nom");
        utilisateurT.setIdentifiant("identifiant");

        List<Utilisateur> utilisateursT = new ArrayList<>();
        utilisateursT.add(utilisateurT);

        InfosUtilisateur infosUtilisateur = new InfosUtilisateur();

        Capture<String> stringCapture = Capture.newInstance();


        //init
        expect(servletConfigMock.getServletContext()).andReturn(servletContext);
        expect(servletContext.getAttribute(ServletUtilitaire.CONF_DAO_FACTORY)).andReturn(daoFactory);
        expect(daoFactory.getUtilisateurDao()).andReturn(utilisateurDAO);

        //doPost
        expect(request.getSession()).andReturn(session);
        expect(session.getAttribute(ServletUtilitaire.ATT_SESSION_USER)).andReturn(utilisateur);

        //Request
        expect(request.getParameter(anyString())).andReturn(utilisateur.getIdUtilisateur()+"");
        expect(utilisateurDAO.trouver(anyObject())).andReturn(utilisateursT);

        //Callback
        response.setContentType(anyString());

        PrintWriter writer = mock(PrintWriter.class);
        expect(response.getWriter()).andReturn(writer);
        writer.flush();

        //Capture
        writer.print(EasyMock.capture(stringCapture));

        this.replayAll();

        //Lancement du test
        infosUtilisateur.init(servletConfigMock);
        infosUtilisateur.doPost(request, response);

        //Verification des valeurs
        assertEquals("Mauvais résultat", "{\"type\":\"success\",\"utilisateur\":{\"idUtilisateur\":12,\"nom\":\"nom\",\"prenom\":\"Prenom\",\"identifiant\":\"identifiant\",\"hash\":\"confidentiel\"}}", stringCapture.getValue());

    }
}