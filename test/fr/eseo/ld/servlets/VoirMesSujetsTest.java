package fr.eseo.ld.servlets;

import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.expectLastCall;
import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.easymock.EasyMockRunner;
import org.easymock.EasyMockSupport;
import org.easymock.Mock;
import org.easymock.MockType;
import org.easymock.TestSubject;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import fr.eseo.ld.beans.EtatSujet;
import fr.eseo.ld.beans.OptionESEO;
import fr.eseo.ld.beans.Sujet;
import fr.eseo.ld.beans.Utilisateur;
import fr.eseo.ld.dao.CommentaireDAO;
import fr.eseo.ld.dao.DAOFactory;
import fr.eseo.ld.dao.NotificationDAO;
import fr.eseo.ld.dao.OptionESEODAO;
import fr.eseo.ld.dao.SujetDAO;
import fr.eseo.ld.dao.UtilisateurDAO;

/**
 * Classe de tests unitaires JUnit 4 de la classe VoirMesSujets.
 *
 * <p>Utilisation de mocks (construits avec EasyMock).</p>
 *
 * @version 1.0
 * @author Maxime LENORMAND
 *
 * @see fr.eseo.ld.servlets.VoirMesSujets
 * @see org.junit
 * @see org.easymock
 */
@RunWith(EasyMockRunner.class)
public class VoirMesSujetsTest extends EasyMockSupport {
	
	private static final String CONF_DAO_FACTORY = "daoFactory";
	
	private static final String STRING_TEST = "test";
	private static final String STRING_TEST_MODIFIE = "test2";
	private static final Integer INTEGER_TEST = 1;
	private static final Integer INTEGER_TEST_MODIFIE = 2;
	
	private static final String ATT_SESSION_USER = "utilisateur";
	
	private static final String CHAMP_ID_SUJET = "idSujet";
	private static final String CHAMP_TITRE = "titre";
	private static final String CHAMP_DESCRIPTION = "description";
	private static final String CHAMP_NBR_MIN_ELEVES = "nbrMinEleves";
	private static final String CHAMP_NBR_MAX_ELEVES = "nbrMaxEleves";
	private static final String CHAMP_CONTRAT_PRO = "contratPro";
	private static final String CHAMP_CONFIDENTIALITE = "confidentialite";
	private static final String CHAMP_OPTION_CC = "CC";
	private static final String CHAMP_OPTION_INFRA = "IIT";
	private static final String CHAMP_OPTION_LD = "LD";
	private static final String CHAMP_OPTION_SE = "SE";
	private static final String CHAMP_OPTION_BIO = "BIO";
	private static final String CHAMP_OPTION_OC = "OC";
	private static final String CHAMP_OPTION_NRJ = "NRJ";
	private static final String CHAMP_OPTION_DSMT = "DSMT";
	private static final String CHAMP_INTERET_JPO = "JPO";
	private static final String CHAMP_INTERET_GP = "GP";
	private static final String CHAMP_AUTRES = "autres";
	private static final String CHAMP_LIENS = "liens";
	
	private static final String CONTEXT_PATH = "SoManager/VoirMesSujets";
	
	private static final String VUE_INDEX = "/Dashboard";
	private static final String VUE_FORM = "/WEB-INF/formulaires/voirMesSujets.jsp";
	
	private static Utilisateur utilisateur;
	private static UtilisateurDAO utilisateurDAO;
	private static Sujet sujet;
	private static SujetDAO sujetDAO;
	private static OptionESEODAO optionESEODAO;
	private static CommentaireDAO commentaireDAO;
	private static NotificationDAO notificationDAO;
	
	@TestSubject
	private VoirMesSujets voirMesSujets = new VoirMesSujets();
	
	@Mock
	private ServletConfig servletConfigMock;
	
	@Mock
	private ServletContext servletContextMock;
	
	@Mock(type = MockType.NICE)
	private HttpServletRequest httpServletRequestMock;
	
	@Mock
	private HttpServletResponse httpservletResponseMock;
	
	@Mock
	private HttpSession httpSessionMock;
	
	@Mock
	private RequestDispatcher requestDispatcherMock;
	
	@Mock
	private DAOFactory daoFactoryMock;

	/**
	 * Instancie des DAO et Insère un utilisateur et un sujet test.
	 * 
	 * @throws Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		/* Instanciations de DAO */
		final DAOFactory daoFactoy = DAOFactory.getInstance();
		utilisateurDAO = daoFactoy.getUtilisateurDao();
		sujetDAO = daoFactoy.getSujetDao();
		optionESEODAO = daoFactoy.getOptionESEODAO();
		commentaireDAO = daoFactoy.getCommentaireDao();
		notificationDAO = daoFactoy.getNotificationDao();
		
		/* Insertion d'un utilisateur test dans la BDD */
		utilisateur = new Utilisateur();
		utilisateur.setIdentifiant(STRING_TEST);
		utilisateur.setNom(STRING_TEST);
		utilisateur.setPrenom(STRING_TEST);
		utilisateur.setValide("oui");
		utilisateurDAO.creer(utilisateur);
		final Long idUtilisateur = utilisateurDAO.trouver(utilisateur).get(0).getIdUtilisateur();
		utilisateur.setIdUtilisateur(idUtilisateur);
		
		/* Insertion d'un sujet test dans la BDD */
		sujet = new Sujet();
		sujet.setTitre(STRING_TEST);
		sujet.setDescription(STRING_TEST);
		sujet.setNbrMinEleves(INTEGER_TEST);
		sujet.setNbrMaxEleves(INTEGER_TEST);
		sujet.setEtat(EtatSujet.DEPOSE);
		sujetDAO.creer(sujet);
		final Long idSujet = sujetDAO.trouver(sujet).get(0).getIdSujet();
		sujet.setIdSujet(idSujet);
		
		/* Attribution du sujet à un utilisateur et une option */
		sujetDAO.creerPorteurSujet(utilisateur, sujet);
		List<OptionESEO> options = new ArrayList<>();
		OptionESEO option = new OptionESEO();
		option.setIdOption(1l);
		options.add(option);
		sujetDAO.attribuerOptionsSujet(sujet, options);
	}
	
	/**
	 * Supprime les entrées insérées dans la BDD.
	 * 
	 * @throws Exception
	 */
	@AfterClass
	public static void tearDownAfterClass()throws Exception {
		utilisateurDAO.supprimer(utilisateur);
		sujetDAO.supprimer(sujet);
	}

	/**
	 * Teste la méthode protected void doGet(HttpServletRequest request, HttpServletResponse response).
	 * 
	 * @throws ServletException
	 * @throws IOException
	 */
	@Test
	public void testDoGet() throws ServletException, IOException {
		/* Récupération d'instances de DAO */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
		expect(this.daoFactoryMock.getUtilisateurDao()).andReturn(utilisateurDAO);
		expect(this.daoFactoryMock.getSujetDao()).andReturn(sujetDAO);
		expect(this.daoFactoryMock.getOptionESEODAO()).andReturn(optionESEODAO);
		expect(this.daoFactoryMock.getCommentaireDao()).andReturn(commentaireDAO);
		expect(this.daoFactoryMock.getNotificationDao()).andReturn(notificationDAO);
		
		/* Récupération de l'ID de l'utilisateur en session */
		expect(this.httpServletRequestMock.getSession()).andReturn(this.httpSessionMock);
		expect(this.httpSessionMock.getAttribute(ATT_SESSION_USER)).andReturn(utilisateur);
		
		/* Affichage du formulaire */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getRequestDispatcher(VUE_FORM)).andReturn(this.requestDispatcherMock);
		this.requestDispatcherMock.forward(this.httpServletRequestMock, this.httpservletResponseMock);
		expectLastCall();

		this.replayAll();

		/* Cycle de vie d'une servlet */
		this.voirMesSujets.init(this.servletConfigMock);
		this.voirMesSujets.doGet(this.httpServletRequestMock, this.httpservletResponseMock);
		this.voirMesSujets.destroy();

		this.verifyAll();
	}
	
	/**
	 * Teste la méthode protected void doPost(HttpServletRequest request, HttpServletResponse response).
	 * 
	 * @throws ServletException
	 * @throws IOException
	 */
	@Test
	public void testDoPost() throws ServletException, IOException {
		/* Récupération d'instances de DAO */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
		expect(this.daoFactoryMock.getUtilisateurDao()).andReturn(utilisateurDAO);
		expect(this.daoFactoryMock.getSujetDao()).andReturn(sujetDAO);
		expect(this.daoFactoryMock.getOptionESEODAO()).andReturn(optionESEODAO);
		expect(this.daoFactoryMock.getCommentaireDao()).andReturn(commentaireDAO);
		expect(this.daoFactoryMock.getNotificationDao()).andReturn(notificationDAO);
		
		/* Récupération de l'ID de l'utilisateur en session */
		expect(this.httpServletRequestMock.getSession()).andReturn(this.httpSessionMock);
		expect(this.httpSessionMock.getAttribute(ATT_SESSION_USER)).andReturn(utilisateur);
		
		/* Récupération des données saisies */
		expect(this.httpServletRequestMock.getParameter(CHAMP_ID_SUJET)).andReturn(sujet.getIdSujet().toString());
		expect(this.httpServletRequestMock.getParameter(CHAMP_TITRE)).andReturn(STRING_TEST_MODIFIE);
		expect(this.httpServletRequestMock.getParameter(CHAMP_DESCRIPTION)).andReturn(STRING_TEST_MODIFIE);
		expect(this.httpServletRequestMock.getParameter(CHAMP_NBR_MIN_ELEVES)).andReturn(INTEGER_TEST_MODIFIE.toString());
		expect(this.httpServletRequestMock.getParameter(CHAMP_NBR_MAX_ELEVES)).andReturn(INTEGER_TEST_MODIFIE.toString());
		expect(this.httpServletRequestMock.getParameter(CHAMP_CONTRAT_PRO)).andReturn(null);
		expect(this.httpServletRequestMock.getParameter(CHAMP_CONFIDENTIALITE)).andReturn(null);
		expect(this.httpServletRequestMock.getParameter(CHAMP_OPTION_CC)).andReturn(null);
		expect(this.httpServletRequestMock.getParameter(CHAMP_OPTION_INFRA)).andReturn(null);
		expect(this.httpServletRequestMock.getParameter(CHAMP_OPTION_LD)).andReturn(null);
		expect(this.httpServletRequestMock.getParameter(CHAMP_OPTION_SE)).andReturn(null);
		expect(this.httpServletRequestMock.getParameter(CHAMP_OPTION_BIO)).andReturn(null);
		expect(this.httpServletRequestMock.getParameter(CHAMP_OPTION_OC)).andReturn(null);
		expect(this.httpServletRequestMock.getParameter(CHAMP_OPTION_NRJ)).andReturn(null);
		expect(this.httpServletRequestMock.getParameter(CHAMP_OPTION_DSMT)).andReturn("DMST");
		expect(this.httpServletRequestMock.getParameter("")).andReturn(null);
		expect(this.httpServletRequestMock.getParameter(CHAMP_INTERET_JPO)).andReturn("JPO");
		expect(this.httpServletRequestMock.getParameter(CHAMP_INTERET_GP)).andReturn(null);
		expect(this.httpServletRequestMock.getParameter(CHAMP_AUTRES)).andReturn(null);
		expect(this.httpServletRequestMock.getParameter(CHAMP_LIENS)).andReturn(STRING_TEST_MODIFIE);
		
		/* Redirection vers la page d'accueil */
		expect(this.httpServletRequestMock.getContextPath()).andReturn(CONTEXT_PATH);
		this.httpservletResponseMock.sendRedirect(CONTEXT_PATH + VUE_INDEX);
		expectLastCall();

		this.replayAll();

		/* Cycle de vie d'une servlet */
		this.voirMesSujets.init(this.servletConfigMock);
		this.voirMesSujets.doPost(this.httpServletRequestMock, this.httpservletResponseMock);
		this.voirMesSujets.destroy();
		
		/* Mise à jour du bean suite à la modification */
		Long idSujet = sujet.getIdSujet();
		sujet = new Sujet();
		sujet.setIdSujet(idSujet);
		
		/* Vérification de la modification du sujet dans la BDD */
		List<Sujet> sujets = sujetDAO.trouver(sujet);
		assertEquals("Titre non modifié", STRING_TEST_MODIFIE, sujets.get(0).getTitre());
		assertEquals("Description non modifiée", STRING_TEST_MODIFIE, sujets.get(0).getDescription());
		assertEquals("NrbMinEleves non modifié", INTEGER_TEST_MODIFIE, sujets.get(0).getNbrMinEleves());
		assertEquals("NrbMaxEleves non modifié", INTEGER_TEST_MODIFIE, sujets.get(0).getNbrMaxEleves());
		assertEquals("Interets non modifiés", "JPO;Autres;null", sujets.get(0).getInterets());
		assertEquals("Liens non modifiés", STRING_TEST_MODIFIE, sujets.get(0).getLiens());
		List<OptionESEO> options = optionESEODAO.trouverOptionSujet(sujet);
		assertEquals("Options non modifiées", "DSMT", options.get(0).getNomOption());
		
		this.verifyAll();
	}

}