package fr.eseo.ld.servlets;

import fr.eseo.ld.beans.AnneeScolaire;
import fr.eseo.ld.beans.Utilisateur;
import fr.eseo.ld.dao.*;
import fr.eseo.ld.pgl.beans.Sprint;
import org.easymock.*;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;

import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.expectLastCall;

/**
 * Classe de tests unitaires JUnit 4 de la classe ExporterNotesTest.
 *
 * <p>Utilisation de mocks (construits avec EasyMock).</p>
 *
 * @author Dimitri Jarneau
 * @version 1.0
 * @see ExporterNotesTest
 * @see org.junit
 * @see org.easymock
 */
@RunWith(EasyMockRunner.class)
public class ExporterNotesTest extends EasyMockSupport {

    private static final String CONF_DAO_FACTORY = "daoFactory";
    private static final String ATT_SESSION_USER = "utilisateur";
    private static final Long ID_ETUDIANT_LD1 = 1339L;

    private static final String CONTEXT_PATH = "SoManager/ExporterNotes";
    private static final String VUE_DASHBOARD = "/Dashboard";
    private static final String VUE_FORM = "/WEB-INF/formulaires/exporterNotes.jsp";

    private static final String PARAM_ANNEESCOLAIRE = "anneeChoisi";
    private static final String PARAM_OPTION = "optionChoisi";
    private static final String PARAM_EXPORTTYPE = "exportChoisi";
    private static final String PARAM_NOMFICHIER = "nomFichier";
    private static final String PARAM_ANNEEEXPORTTYPE = "exportAnneeChoisi";
    private static final String ATT_CHOIXMODELE = "choixModele";

    private static final String PARAM_MODELESEL = "I3_ModeleNoteJury";
    private static final String PARAM_LIGNEHEADER = "ligneHeader";
    private static final String PARAM_COLID = "colId";
    private static final String PARAM_COLNOM = "colNom";
    private static final String PARAM_COLNOTE = "colNote";
    private static final String PARAM_COLCOM = "colCom";
    private static final String PARAM_COLMOYENNE = "colMoyenne";
    private static final String PARAM_COLGRADE = "colGrade";

    private static final String ONGLET_CONFIG = "configuration";

    private static final Integer ANNEE_COURANTE = 2017;
    private static final Long ID_DEFAULT = 1L;
    private static final String ID_DEFAULT_STR = "1";
    private static final String TEST = "test";
    private static final Integer TEST_2 = 1;
    private static final Long ID_PROF_RESPONSABLE = 16L;

    private static AnneeScolaire anneeScolaire;
    private static Sprint sprint;
    private static AnneeScolaireDAO anneeScolaireDAO;
    private static OptionESEODAO optionESEODAO;
    private static SprintDAO sprintDAO;
    private static MatiereDAO matiereDAO;
    private static UtilisateurDAO utilisateurDAO;
    private static ModeleExcelDAO modeleDAO;
    private static ParametreDAO parametreDAO;

    @TestSubject
    private ExporterNotes exporterNotes = new ExporterNotes();

    @Mock
    private ServletConfig servletConfigMock;

    @Mock
    private ServletContext servletContextMock;

    @Mock(type = MockType.NICE)
    private HttpServletRequest httpServletRequestMock;

    @Mock
    private HttpServletResponse httpservletResponseMock;

    @Mock
    private HttpSession httpSessionMock;

    @Mock
    private RequestDispatcher requestDispatcherMock;

    @Mock
    private DAOFactory daoFactoryMock;

    /**
     * Récupère des instances de DAO.
     *
     * @throws Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        /* Instanciations de DAO */
        DAOFactory daoFactory = DAOFactory.getInstance();
        anneeScolaireDAO = daoFactory.getAnneeScolaireDAO();
        optionESEODAO = daoFactory.getOptionESEODAO();
        sprintDAO = daoFactory.getSprintDAO();
        matiereDAO = daoFactory.getMatiereDAO();
        modeleDAO = daoFactory.getModeleExcelDAO();
        parametreDAO = daoFactory.getParametreDAO();
        utilisateurDAO = daoFactory.getUtilisateurDao();
    }

    /**
     * Supprime les insertions dans la BDD.
     *
     * @throws Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {

    }

    /**
     * Teste la méthode protected void doGet(HttpServletRequest request, HttpServletResponse response).
     *
     * @throws ServletException
     * @throws IOException
     */
    @Test
    public void test1DoGet() throws ServletException, IOException {
        /* Récupération d'instances de DAO */
        expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
        expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
        expect(this.daoFactoryMock.getUtilisateurDao()).andReturn(utilisateurDAO);
        expect(this.daoFactoryMock.getAnneeScolaireDAO()).andReturn(anneeScolaireDAO);
        expect(this.daoFactoryMock.getSprintDAO()).andReturn(sprintDAO);
        expect(this.daoFactoryMock.getOptionESEODAO()).andReturn(optionESEODAO);
        expect(this.daoFactoryMock.getParametreDAO()).andReturn(parametreDAO);
        expect(this.daoFactoryMock.getModeleExcelDAO()).andReturn(modeleDAO);
        expect(this.daoFactoryMock.getMatiereDAO()).andReturn(matiereDAO);


        /* Récupération de l'Utilisateur en session */
        final Utilisateur utilisateur = new Utilisateur();
        final Long idUtilisateur = ID_ETUDIANT_LD1;
        utilisateur.setIdUtilisateur(idUtilisateur);
        expect(this.httpServletRequestMock.getSession()).andReturn(this.httpSessionMock);
        expect(this.httpSessionMock.getAttribute(ATT_SESSION_USER)).andReturn(utilisateur);

        /* Affichage du formulaire */
        expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
        expect(this.servletContextMock.getRequestDispatcher(VUE_FORM)).andReturn(this.requestDispatcherMock);
        this.requestDispatcherMock.forward(this.httpServletRequestMock, this.httpservletResponseMock);
        expectLastCall();

        this.replayAll();

        /* Cycle de vie d'une servlet */
        this.exporterNotes.init(this.servletConfigMock);
        this.exporterNotes.doGet(this.httpServletRequestMock, this.httpservletResponseMock);
        this.exporterNotes.destroy();

        //this.verifyAll();
    }

    /**
     * Teste la méthode protected void doGet(HttpServletRequest request, HttpServletResponse response).
     *
     * @throws ServletException
     * @throws IOException
     */
    @Test
    public void testdoEnvoyerModele_attributs_vides() throws ServletException, IOException {
        /* Récupération d'instances de DAO */
        expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
        expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
        expect(this.daoFactoryMock.getUtilisateurDao()).andReturn(utilisateurDAO);
        expect(this.daoFactoryMock.getAnneeScolaireDAO()).andReturn(anneeScolaireDAO);
        expect(this.daoFactoryMock.getSprintDAO()).andReturn(sprintDAO);
        expect(this.daoFactoryMock.getOptionESEODAO()).andReturn(optionESEODAO);
        expect(this.daoFactoryMock.getParametreDAO()).andReturn(parametreDAO);
        expect(this.daoFactoryMock.getModeleExcelDAO()).andReturn(modeleDAO);
        expect(this.daoFactoryMock.getMatiereDAO()).andReturn(matiereDAO);

        /* Récupération des données saisies */
        expect(this.httpServletRequestMock.getParameter("formulaire")).andReturn("envoyerModele");

        /* Récupération des données saisies */
        IOException e = new IOException();
        expect(this.httpServletRequestMock.getPart("file_selected")).andThrow(e);
        expectLastCall().times(4);

        expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
        expect(this.servletContextMock.getRequestDispatcher(VUE_FORM)).andReturn(this.requestDispatcherMock);
        this.requestDispatcherMock.forward(this.httpServletRequestMock, this.httpservletResponseMock);
        expectLastCall();

        this.replayAll();

        /* Cycle de vie d'une servlet */
        this.exporterNotes.init(this.servletConfigMock);
        this.exporterNotes.doPost(this.httpServletRequestMock, this.httpservletResponseMock);
        this.exporterNotes.destroy();

        //this.verifyAll();
    }


    /**
     * Teste la méthode protected void doGet(HttpServletRequest request, HttpServletResponse response).
     *
     * @throws ServletException
     * @throws IOException
     */
    @Test
    public void testdoExporterNotesErreur() throws ServletException, IOException {
        /* Récupération d'instances de DAO */
        expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
        expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
        expect(this.daoFactoryMock.getUtilisateurDao()).andReturn(utilisateurDAO);
        expect(this.daoFactoryMock.getAnneeScolaireDAO()).andReturn(anneeScolaireDAO);
        expect(this.daoFactoryMock.getSprintDAO()).andReturn(sprintDAO);
        expect(this.daoFactoryMock.getOptionESEODAO()).andReturn(optionESEODAO);
        expect(this.daoFactoryMock.getParametreDAO()).andReturn(parametreDAO);
        expect(this.daoFactoryMock.getModeleExcelDAO()).andReturn(modeleDAO);
        expect(this.daoFactoryMock.getMatiereDAO()).andReturn(matiereDAO);

        /* Récupération des données saisies */
        expect(this.httpServletRequestMock.getParameter("formulaire")).andReturn("exporterNotes");


        /* Récupération des données saisies */
        expect(this.httpServletRequestMock.getParameter(PARAM_ANNEESCOLAIRE)).andReturn(String.valueOf(ANNEE_COURANTE));
        expectLastCall().times(2);
        expect(this.httpServletRequestMock.getParameter(PARAM_EXPORTTYPE)).andReturn(ID_DEFAULT_STR);
        expectLastCall().times(2);
        expect(this.httpServletRequestMock.getParameter(PARAM_OPTION)).andReturn(ID_DEFAULT_STR);
        expectLastCall().times(2);
        expect(this.httpServletRequestMock.getParameter(PARAM_NOMFICHIER)).andReturn(TEST);
        expectLastCall().times(2);
        expect(this.httpServletRequestMock.getParameter(PARAM_ANNEEEXPORTTYPE)).andReturn(TEST);
        expectLastCall().times(2);

        expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
        expect(this.servletContextMock.getRequestDispatcher(VUE_FORM)).andReturn(this.requestDispatcherMock);
        this.requestDispatcherMock.forward(this.httpServletRequestMock, this.httpservletResponseMock);
        expectLastCall();

        this.replayAll();

        /* Cycle de vie d'une servlet */
        this.exporterNotes.init(this.servletConfigMock);
        this.exporterNotes.doPost(this.httpServletRequestMock, this.httpservletResponseMock);
        this.exporterNotes.destroy();

        //this.verifyAll();
    }

    /**
     * Teste la méthode protected void doGet(HttpServletRequest request, HttpServletResponse response).
     *
     * @throws ServletException
     * @throws IOException
     */
    @Test
    public void testdoChangerModeleErreur() throws ServletException, IOException {
        /* Récupération d'instances de DAO */
        expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
        expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
        expect(this.daoFactoryMock.getUtilisateurDao()).andReturn(utilisateurDAO);
        expect(this.daoFactoryMock.getAnneeScolaireDAO()).andReturn(anneeScolaireDAO);
        expect(this.daoFactoryMock.getSprintDAO()).andReturn(sprintDAO);
        expect(this.daoFactoryMock.getOptionESEODAO()).andReturn(optionESEODAO);
        expect(this.daoFactoryMock.getParametreDAO()).andReturn(parametreDAO);
        expect(this.daoFactoryMock.getModeleExcelDAO()).andReturn(modeleDAO);
        expect(this.daoFactoryMock.getMatiereDAO()).andReturn(matiereDAO);

        /* Récupération des données saisies */
        expect(this.httpServletRequestMock.getParameter("formulaire")).andReturn("choisirModele");


        /* Récupération des données saisies */
        expect(this.httpServletRequestMock.getParameter(ATT_CHOIXMODELE)).andReturn(TEST);
        expectLastCall().times(2);

        expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
        expect(this.servletContextMock.getRequestDispatcher(VUE_FORM)).andReturn(this.requestDispatcherMock);
        this.requestDispatcherMock.forward(this.httpServletRequestMock, this.httpservletResponseMock);
        expectLastCall();

        this.replayAll();

        /* Cycle de vie d'une servlet */
        this.exporterNotes.init(this.servletConfigMock);
        this.exporterNotes.doPost(this.httpServletRequestMock, this.httpservletResponseMock);
        this.exporterNotes.destroy();

        //this.verifyAll();
    }

    /**
     * Teste la méthode protected void doGet(HttpServletRequest request, HttpServletResponse response).
     *
     * @throws ServletException
     * @throws IOException
     */
    @Test
    public void testdoParametrerModeleErreur() throws ServletException, IOException {
        /* Récupération d'instances de DAO */
        expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
        expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
        expect(this.daoFactoryMock.getUtilisateurDao()).andReturn(utilisateurDAO);
        expect(this.daoFactoryMock.getAnneeScolaireDAO()).andReturn(anneeScolaireDAO);
        expect(this.daoFactoryMock.getSprintDAO()).andReturn(sprintDAO);
        expect(this.daoFactoryMock.getOptionESEODAO()).andReturn(optionESEODAO);
        expect(this.daoFactoryMock.getParametreDAO()).andReturn(parametreDAO);
        expect(this.daoFactoryMock.getModeleExcelDAO()).andReturn(modeleDAO);
        expect(this.daoFactoryMock.getMatiereDAO()).andReturn(matiereDAO);

        /* Récupération des données saisies */
        expect(this.httpServletRequestMock.getParameter("formulaire")).andReturn("parametrerModele");


        /* Récupération des données saisies */
        expect(this.httpServletRequestMock.getParameter(PARAM_LIGNEHEADER)).andReturn(String.valueOf(TEST_2));
        expect(this.httpServletRequestMock.getParameter(PARAM_COLID)).andReturn(String.valueOf(TEST_2));
        expect(this.httpServletRequestMock.getParameter(PARAM_COLNOM)).andReturn(String.valueOf(TEST_2));
        expect(this.httpServletRequestMock.getParameter(PARAM_COLNOTE)).andReturn(String.valueOf(TEST_2));
        expect(this.httpServletRequestMock.getParameter(PARAM_COLCOM)).andReturn(String.valueOf(TEST_2));
        expect(this.httpServletRequestMock.getParameter(PARAM_COLMOYENNE)).andReturn(String.valueOf(TEST_2));
        expect(this.httpServletRequestMock.getParameter(PARAM_COLGRADE)).andReturn(String.valueOf(TEST_2));
        expect(this.httpServletRequestMock.getParameter(ATT_CHOIXMODELE)).andReturn(String.valueOf(TEST_2));
        expectLastCall().times(2);


        expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
        expect(this.servletContextMock.getRequestDispatcher(VUE_FORM)).andReturn(this.requestDispatcherMock);
        this.requestDispatcherMock.forward(this.httpServletRequestMock, this.httpservletResponseMock);
        expectLastCall();

        this.replayAll();

        /* Cycle de vie d'une servlet */
        this.exporterNotes.init(this.servletConfigMock);
        this.exporterNotes.doPost(this.httpServletRequestMock, this.httpservletResponseMock);
        this.exporterNotes.destroy();

        //this.verifyAll();
    }

    /**
     * Teste la méthode protected void doGet(HttpServletRequest request, HttpServletResponse response).
     *
     * @throws ServletException
     * @throws IOException
     */
    @Test
    public void testdoSupprimerModeleErreur() throws ServletException, IOException {
        /* Récupération d'instances de DAO */
        expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
        expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
        expect(this.daoFactoryMock.getUtilisateurDao()).andReturn(utilisateurDAO);
        expect(this.daoFactoryMock.getAnneeScolaireDAO()).andReturn(anneeScolaireDAO);
        expect(this.daoFactoryMock.getSprintDAO()).andReturn(sprintDAO);
        expect(this.daoFactoryMock.getOptionESEODAO()).andReturn(optionESEODAO);
        expect(this.daoFactoryMock.getParametreDAO()).andReturn(parametreDAO);
        expect(this.daoFactoryMock.getModeleExcelDAO()).andReturn(modeleDAO);
        expect(this.daoFactoryMock.getMatiereDAO()).andReturn(matiereDAO);

        /* Récupération des données saisies */
        expect(this.httpServletRequestMock.getParameter("formulaire")).andReturn("supprimerModele");


        /* Récupération des données saisies */
        expect(this.httpServletRequestMock.getParameter(ATT_CHOIXMODELE)).andReturn(TEST);
        expectLastCall().times(2);


        expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
        expect(this.servletContextMock.getRequestDispatcher(VUE_FORM)).andReturn(this.requestDispatcherMock);
        this.requestDispatcherMock.forward(this.httpServletRequestMock, this.httpservletResponseMock);
        expectLastCall();

        this.replayAll();

        /* Cycle de vie d'une servlet */
        this.exporterNotes.init(this.servletConfigMock);
        this.exporterNotes.doPost(this.httpServletRequestMock, this.httpservletResponseMock);
        this.exporterNotes.destroy();

        //this.verifyAll();
    }

}
