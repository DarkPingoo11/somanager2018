package fr.eseo.ld.servlets;

import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.expectLastCall;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.easymock.EasyMockRunner;
import org.easymock.EasyMockSupport;
import org.easymock.Mock;
import org.easymock.MockType;
import org.easymock.TestSubject;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import fr.eseo.ld.beans.Utilisateur;
import fr.eseo.ld.dao.DAOFactory;
import fr.eseo.ld.dao.NotificationDAO;
import fr.eseo.ld.dao.OptionESEODAO;
import fr.eseo.ld.dao.SujetDAO;
import fr.eseo.ld.dao.UtilisateurDAO;

/**
 * Classe de tests unitaires JUnit 4 de la classe InscriptionProfSujetTest.
 *
 * <p>Utilisation de mocks (construits avec EasyMock).</p>
 *
 * @author Dimitri Jarneau
 * @version 1.0
 * @see InscriptionProfSujetTest
 * @see org.junit
 * @see org.easymock
 */
@RunWith(EasyMockRunner.class)
public class InscriptionProfSujetTest extends EasyMockSupport {
    private static final String CONF_DAO_FACTORY = "daoFactory";

    private static final String CONTEXT_PATH = "SoManager/InscriptionProfSujet";
    private static final String VUE_DASHBOARD = "/Dashboard";
    private static final String VUE_FORM = "/WEB-INF/formulaires/inscriptionProfSujet.jsp";

    public static final String CHAMP_FONCTION_PROF = "fonction";
    public static final String CHAMP_ID_SUJET = "idSujet";
    public static final String ATT_SESSION_USER = "utilisateur";


    private static final Long ID_ETUDIANT_LD1 = 1339L;


    private static final String STRING_TEST = "test";

    private static final String TEST = "testutilisateur";
    private static final String TEST_DATE = "2018/01/01";
    private static final Long ID_UTILISATEUR_TEST = 11L;
    private static final String ID_MEETING = "1";

    private static final Integer ANNEE_COURANTE = 2017;
    private static final String EQUIPEID = ANNEE_COURANTE + "_001";
    private static final Long ID_DEFAULT = 1L;
    private static final String ID_DEFAULT_STR = "1";
    private static final Long ID_PROF_RESPONSABLE = 16L;

    private static UtilisateurDAO utilisateurDAO;
    private static SujetDAO sujetDAO;
    private static OptionESEODAO optionESEODAO;
    private static NotificationDAO notificationDAO;

    private static Utilisateur utilisateur;

    @TestSubject
    private InscriptionProfSujet inscriptionProfSujet = new InscriptionProfSujet();

    @Mock
    private ServletConfig servletConfigMock;

    @Mock
    private ServletContext servletContextMock;

    @Mock(type = MockType.NICE)
    private HttpServletRequest httpServletRequestMock;

    @Mock
    private HttpServletResponse httpservletResponseMock;

    @Mock(type = MockType.NICE)
    private HttpSession httpSessionMock;

    @Mock
    private RequestDispatcher requestDispatcherMock;

    @Mock
    private DAOFactory daoFactoryMock;

    /**
     * Récupère des instances de DAO.
     *
     * @throws Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        /* Instanciations de DAO */
        DAOFactory daoFactory = DAOFactory.getInstance();
        utilisateurDAO = daoFactory.getUtilisateurDao();
        optionESEODAO = daoFactory.getOptionESEODAO();
        sujetDAO = daoFactory.getSujetDao();
        notificationDAO = daoFactory.getNotificationDao();

//        /* Insertion d'un utilisateur test dans la BDD */
//        utilisateur = new Utilisateur();
//        utilisateur.setIdentifiant(STRING_TEST);
//        utilisateur.setNom(STRING_TEST);
//        utilisateur.setPrenom(STRING_TEST);
//        utilisateur.setValide("oui");
//        utilisateurDAO.creer(utilisateur);
//        final Long idUtilisateur = utilisateurDAO.trouver(utilisateur).get(0).getIdUtilisateur();
//        utilisateur.setIdUtilisateur(idUtilisateur);

    }
    /**
     * Supprime le meeting inséré dans la BDD.
     *
     * @throws Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        //utilisateurDAO.supprimer(utilisateur);
    }


    /**
     * Teste la méthode protected void doGet(HttpServletRequest request, HttpServletResponse response).
     *
     * @throws ServletException
     * @throws IOException
     */
    @Test
    public void test1DoGet() throws ServletException, IOException {
        /* Récupération d'instances de DAO */
    	System.out.println(this.daoFactoryMock);
        expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
        expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
        expect(this.daoFactoryMock.getUtilisateurDao()).andReturn(utilisateurDAO);
        expect(this.daoFactoryMock.getSujetDao()).andReturn(sujetDAO);
        expect(this.daoFactoryMock.getOptionESEODAO()).andReturn(optionESEODAO);
        expect(this.daoFactoryMock.getNotificationDao()).andReturn(notificationDAO);

        /* Récupération de l'Utilisateur en session */
        final Utilisateur utilisateur = new Utilisateur();
        final Long idUtilisateur = ID_ETUDIANT_LD1;
        utilisateur.setIdUtilisateur(idUtilisateur);
        expect(this.httpServletRequestMock.getSession()).andReturn(this.httpSessionMock);
        expect(this.httpSessionMock.getAttribute(ATT_SESSION_USER)).andReturn(utilisateur);

        /* Affichage du formulaire */
        expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
        expect(this.servletContextMock.getRequestDispatcher(VUE_FORM)).andReturn(this.requestDispatcherMock);
        this.requestDispatcherMock.forward(this.httpServletRequestMock, this.httpservletResponseMock);
        expectLastCall();

        this.replayAll();

        /* Cycle de vie d'une servlet */
        this.inscriptionProfSujet.init(this.servletConfigMock);
        this.inscriptionProfSujet.doGet(this.httpServletRequestMock, this.httpservletResponseMock);
        this.inscriptionProfSujet.destroy();

        //this.verifyAll();
    }

    /**
     * Teste la méthode protected void doPost(HttpServletRequest request, HttpServletResponse response).
     *
     * <p>Cas du succès de la création d'une équipe.</p>
     *
     * @throws ServletException
     * @throws IOException
     */
    @Test
    public void testDoPostErreurAjouterEquipeI2() throws ServletException, IOException {
        /* Récupération d'instances de DAO */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
        expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
        expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
        expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
        expect(this.daoFactoryMock.getUtilisateurDao()).andReturn(utilisateurDAO);
        expect(this.daoFactoryMock.getSujetDao()).andReturn(sujetDAO);
        expect(this.daoFactoryMock.getOptionESEODAO()).andReturn(optionESEODAO);
        expect(this.daoFactoryMock.getNotificationDao()).andReturn(notificationDAO);

        /* Récupération de l'Utilisateur en session */
        expect(this.httpServletRequestMock.getSession()).andReturn(this.httpSessionMock);
        final Utilisateur utilisateur = new Utilisateur();
        final Long idUtilisateur = ID_PROF_RESPONSABLE;
        utilisateur.setIdUtilisateur(idUtilisateur);

        expectLastCall().times(4);


        /* Récupération des données saisies */
        expect(this.httpServletRequestMock.getParameter(CHAMP_FONCTION_PROF)).andReturn("prof");
        expectLastCall().times(2);
        expect(this.httpServletRequestMock.getParameter(CHAMP_ID_SUJET)).andReturn(ID_DEFAULT_STR);
        expectLastCall().times(2);
        expect(this.httpSessionMock.getAttribute(ATT_SESSION_USER)).andReturn(utilisateur);
        expectLastCall().times(2);

        /* Redirection vers la page d'accueil */
		expect(this.httpServletRequestMock.getContextPath()).andReturn(CONTEXT_PATH);
		this.httpservletResponseMock.sendRedirect(null);
		expectLastCall();

        this.replayAll();

        /* Cycle de vie d'une servlet */
        this.inscriptionProfSujet.init(this.servletConfigMock);
        this.inscriptionProfSujet.doPost(this.httpServletRequestMock, this.httpservletResponseMock);
        this.inscriptionProfSujet.destroy();

        //this.verifyAll();
    }

}
