package fr.eseo.ld.servlets;

import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.expectLastCall;

import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.easymock.EasyMockRunner;
import org.easymock.EasyMockSupport;
import org.easymock.Mock;
import org.easymock.MockType;
import org.easymock.TestSubject;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

import fr.eseo.ld.beans.Utilisateur;
import fr.eseo.ld.dao.DAOFactory;
import fr.eseo.ld.dao.NotificationDAO;
import fr.eseo.ld.utils.IOUtilitaire;

/**
 * Classe de tests unitaires JUnit 4 de la classe GererApplication.
 *
 * <p>
 * Utilisation de mocks (construits avec EasyMock).
 * </p>
 *
 * @version 1.0
 * @author Maxime LENORMAND
 *
 * @see fr.eseo.ld.servlets.GererApplication
 * @see org.junit
 * @see org.easymock
 */
@RunWith(EasyMockRunner.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class GererApplicationTest extends EasyMockSupport {

	private static final String CONF_DAO_FACTORY = "daoFactory";

	private static final String ATT_FICHIER_CSS = "/css/style.css";
	private static final String ATT_CHEMIN_DOSSIER_IMG = "/images";
	private static final String ATT_SESSION_USER = "utilisateur";

	private static final String VUE_FORM = "/WEB-INF/formulaires/gererApplication.jsp";

	private static final String ATT_TEXTE_CCS = "/* BALISE PRINCIAPLES */¿¿html {¿  height: 100%;¿}¿¿body {¿  padding-top: 10px;¿  min-height: 100%;¿  margin: 0;¿  padding: 0;¿  position:relative;¿  padding-bottom: 60px;¿  font-size: 14px;¿}¿¿header {¿	text-align: center;¿	font-size: 40px;¿	font-family: Verdana, Arial, Helvetica, sans-serif; ¿}¿¿.navbar {¿	text-align: center;¿	border-color: black;¿	border-radius: 6px;¿	border-width:thin;¿  	line-height: 15px;¿}¿¿footer {¿	text-align: center;¿	background-color: lightgray;¿	border-radius: 6px;¿  	line-height: 25px;¿	position: absolute;¿	bottom: 0;¿}¿¿.container {¿  padding-right: 15px;¿  padding-left: 15px;¿  margin-right: auto;¿  margin-left: auto;¿}¿¿/* ATTRIBUTS DES PANELS PRIMARY */¿¿.panel-primary {¿	color:#333;¿    background-color:#fff ;¿    border-color:#337ab7 ;¿    font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif;¿}¿¿.panel-primary>.panel-heading{¿	color: #fff;¿    background-color: #337ab7;¿    border-color: #337ab7;¿    font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif;¿}¿¿.panel-default>.panel-body{¿    color: #333;¿    background-color: #fff;¿    border-color: #337ab7;¿    font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif;¿}¿¿/* ATTRIBUTS DES PANELS DEFAUT */¿¿.panel-default {¿	color: #333;¿    background-color: #fff;¿    border-color: #ddd;¿    font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif;¿}¿¿.panel-default>.panel-heading{¿	color: #333;¿    background-color: #f5f5f5;¿    border-color: #ddd;¿    font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif;¿}¿¿.panel-default>.panel-body{¿    color: #333;¿    background-color: #fff;¿    border-color: #337ab7;¿    font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif;¿}¿¿/* ATTRIBUTS DES BOUTONS */¿¿.btn {¿    font-size: 14px;¿    text-align: center;¿    vertical-align: middle;¿}¿.btn-primary {¿    color: #fff;¿    background-color: #337ab7;¿    border-color: #2e6da4;¿}¿¿.btn-danger {¿    color: #fff;¿    background-color: #d9534f;¿    border-color: #d43f3a;¿}¿section {¿	text-align: justify;¿}¿¿.requis {¿    color: #c00;¿}¿¿.erreur {¿    color: #900;¿}¿¿.succes {¿    color: #090;¿}¿¿.info {¿    font-style: italic;¿    color: #E8A22B;¿}¿";

	private static NotificationDAO notificationDAO;

	@TestSubject
	private GererApplication gererApplication = new GererApplication();

	@Mock
	private ServletConfig servletConfigMock;

	@Mock(type = MockType.NICE)
	private ServletContext servletContextMock;

	@Mock(type = MockType.NICE)
	private HttpServletRequest httpServletRequestMock;

	@Mock(type = MockType.NICE)
	private HttpServletResponse httpservletResponseMock;

	@Mock
	private HttpSession httpSessionMock;

	@Mock(type = MockType.NICE)
	private RequestDispatcher requestDispatcherMock;

	@Mock
	private DAOFactory daoFactoryMock;

	@Mock
	FileWriter writerMock;

	@Mock
	IOUtilitaire ioUtilMock;

	/**
	 * Récupère des instances de DAO.
	 * 
	 * @throws Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		DAOFactory daoFactory = DAOFactory.getInstance();
		notificationDAO = daoFactory.getNotificationDao();
	}

	/**
	 * Teste la méthode protected void doGet(HttpServletRequest request,
	 * HttpServletResponse response).
	 * 
	 * @throws ServletException
	 * @throws IOException
	 */
	@Test
	public void test1DoGet() throws ServletException, IOException {
		/* Récupération d'instances de DAO */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
		expect(this.daoFactoryMock.getNotificationDao()).andReturn(notificationDAO);

		/* Chargement des éléments de la page */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expectLastCall().times(3);
		expect(this.servletContextMock.getRealPath(ATT_FICHIER_CSS)).andReturn("");
		expect(this.servletContextMock.getRealPath(ATT_CHEMIN_DOSSIER_IMG)).andReturn("");
		expect(this.servletContextMock.getRequestDispatcher(VUE_FORM)).andReturn(this.requestDispatcherMock);
		this.requestDispatcherMock.forward(this.httpServletRequestMock, this.httpservletResponseMock);
		expectLastCall();
		
		String[] proprietes = new String[] { "", "", "" };
		expect(this.daoFactoryMock.getProprietes()).andReturn(proprietes);
		expect(this.daoFactoryMock.getUrlBdd()).andReturn("");
		expect(this.daoFactoryMock.getDriver()).andReturn("");
		expect(this.daoFactoryMock.getUserBdd()).andReturn("");
		expect(this.daoFactoryMock.getMdpBdd()).andReturn("");

		this.replayAll();

		/* Cycle de vie d'une servlet */
		this.gererApplication.init(this.servletConfigMock);
		this.gererApplication.doGet(this.httpServletRequestMock, this.httpservletResponseMock);
		this.gererApplication.destroy();

		this.verifyAll();
	}

	/**
	 * Teste la méthode protected void doGet(HttpServletRequest request,
	 * HttpServletResponse response). Test du cas de modif LDAP
	 * 
	 * @throws ServletException
	 * @throws IOException
	 */
	@Test
	public void test2DoPostModifLDAP() throws ServletException, IOException {
		/* Récupération d'instances de DAO */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
		expect(this.daoFactoryMock.getNotificationDao()).andReturn(notificationDAO);

		/* création des expect session */
		expect(this.httpServletRequestMock.getSession()).andReturn(this.httpSessionMock);
		final Utilisateur utilisateur = new Utilisateur();
		final Long idUtilisateur = 250L;
		utilisateur.setIdUtilisateur(idUtilisateur);
		expect(this.httpSessionMock.getAttribute(ATT_SESSION_USER)).andReturn(utilisateur);

		/* creation expect requete */
		expect(this.httpServletRequestMock.getParameter("formulaire")).andReturn("modifierConfLDAP");
		expectLastCall().times(4);
		expect(this.httpServletRequestMock.getContentType()).andReturn("");
		expect(this.httpServletRequestMock.getParameter("contexteDN")).andReturn("dc=ldcr,dc=tp");
		expect(this.httpServletRequestMock.getParameter("serveur")).andReturn("192.168.4.11");
		expect(this.httpServletRequestMock.getParameter("port")).andReturn("389");
		expect(this.httpServletRequestMock.getParameter("nom")).andReturn("sn");
		expect(this.httpServletRequestMock.getParameter("prenom")).andReturn("givenName");
		expect(this.httpServletRequestMock.getParameter("mail")).andReturn("mail");
		expect(this.httpServletRequestMock.getParameter("identifiant")).andReturn("uid");
		expect(this.httpServletRequestMock.getParameter("contexteDNUser")).andReturn("ou=people,dc=ldcr,dc=tp");
		expect(this.httpServletRequestMock.getParameter("compteUtilisateur")).andReturn("thomas menard");
		expect(this.httpServletRequestMock.getParameter("mdpUtilisateur")).andReturn("network");
		expect(this.httpServletRequestMock.getParameter("compteAdministrateur")).andReturn("cn=admin,dc=ldcr,dc=tp");
		expect(this.httpServletRequestMock.getParameter("mdpAdministrateur")).andReturn("network");

		this.replayAll();

		/* Cycle de vie d'une servlet */
		this.gererApplication.init(this.servletConfigMock);
		this.gererApplication.doPost(this.httpServletRequestMock, this.httpservletResponseMock);
		this.gererApplication.destroy();

		this.verifyAll();
	}

	/**
	 * Teste la méthode protected void doGet(HttpServletRequest request,
	 * HttpServletResponse response). Test du cas de defaut LDAP
	 * 
	 * @throws ServletException
	 * @throws IOException
	 */
	@Test
	public void test3DoPostDefautLDAP() throws ServletException, IOException {
		/* Récupération d'instances de DAO */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
		expect(this.daoFactoryMock.getNotificationDao()).andReturn(notificationDAO);

		/* création des expect session */
		expect(this.httpServletRequestMock.getSession()).andReturn(this.httpSessionMock);
		final Utilisateur utilisateur = new Utilisateur();
		final Long idUtilisateur = 250L;
		utilisateur.setIdUtilisateur(idUtilisateur);
		expect(this.httpSessionMock.getAttribute(ATT_SESSION_USER)).andReturn(utilisateur);

		/* creation expect requete */
		expect(this.httpServletRequestMock.getParameter("formulaire")).andReturn("defautConfLDAP");
		expectLastCall().times(4);
		expect(this.httpServletRequestMock.getContentType()).andReturn("");

		this.replayAll();

		/* Cycle de vie d'une servlet */
		this.gererApplication.init(this.servletConfigMock);
		this.gererApplication.doPost(this.httpServletRequestMock, this.httpservletResponseMock);
		this.gererApplication.destroy();

		this.verifyAll();
	}

	/**
	 * Teste la méthode protected void doGet(HttpServletRequest request,
	 * HttpServletResponse response). Test du cas de modif css
	 * 
	 * @throws ServletException
	 * @throws IOException
	 */
	@Test
	public void test4DoPostModifCss() throws ServletException, IOException {
		/* Récupération d'instances de DAO */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expectLastCall().times(5);
		expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
		expect(this.daoFactoryMock.getNotificationDao()).andReturn(notificationDAO);

		/* création des expect session */
		expect(this.httpServletRequestMock.getSession()).andReturn(this.httpSessionMock);
		final Utilisateur utilisateur = new Utilisateur();
		final Long idUtilisateur = 250L;
		utilisateur.setIdUtilisateur(idUtilisateur);
		expect(this.httpSessionMock.getAttribute(ATT_SESSION_USER)).andReturn(utilisateur);

		/* creation expect requete */
		expect(this.httpServletRequestMock.getParameter("formulaire")).andReturn("modifierCss");
		expectLastCall().times(4);
		expect(this.httpServletRequestMock.getContentType()).andReturn("");
		expect(this.httpServletRequestMock.getParameter("texteCss")).andReturn(ATT_TEXTE_CCS);

		expect(this.servletContextMock.getRealPath("/css/style.css")).andReturn("");

		String[] proprietes = new String[] { "", "", "" };
		expect(this.daoFactoryMock.getProprietes()).andReturn(proprietes);
		expect(this.daoFactoryMock.getUrlBdd()).andReturn("");
		expect(this.daoFactoryMock.getDriver()).andReturn("");
		expect(this.daoFactoryMock.getUserBdd()).andReturn("");
		expect(this.daoFactoryMock.getMdpBdd()).andReturn("");

		expect(this.servletContextMock.getRealPath("/css/style.css")).andReturn("");
		expect(this.servletContextMock.getRealPath("/images")).andReturn("");
		expect(this.servletContextMock.getRequestDispatcher(VUE_FORM)).andReturn(this.requestDispatcherMock);
		this.requestDispatcherMock.forward(this.httpServletRequestMock, this.httpservletResponseMock);

		this.replayAll();

		/* Cycle de vie d'une servlet */
		this.gererApplication.init(this.servletConfigMock);
		this.gererApplication.doPost(this.httpServletRequestMock, this.httpservletResponseMock);
		this.gererApplication.destroy();

		this.verifyAll();
	}

	/**
	 * Teste la méthode protected void doGet(HttpServletRequest request,
	 * HttpServletResponse response). Test du cas de defaut css
	 * 
	 * @throws ServletException
	 * @throws IOException
	 */
	@Test
	public void test5DoPostDefautCss() throws ServletException, IOException {
		/* Récupération d'instances de DAO */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
		expect(this.daoFactoryMock.getNotificationDao()).andReturn(notificationDAO);

		/* Affichage du formulaire */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expectLastCall().times(5);
		expect(this.servletContextMock.getRequestDispatcher(VUE_FORM)).andReturn(this.requestDispatcherMock);
		this.requestDispatcherMock.forward(this.httpServletRequestMock, this.httpservletResponseMock);
		expectLastCall();

		/* création des expect session */
		expect(this.httpServletRequestMock.getSession()).andReturn(this.httpSessionMock);
		final Utilisateur utilisateur = new Utilisateur();
		final Long idUtilisateur = 250L;
		utilisateur.setIdUtilisateur(idUtilisateur);
		expect(this.httpSessionMock.getAttribute(ATT_SESSION_USER)).andReturn(utilisateur);

		/* creation expect requete */
		expect(this.httpServletRequestMock.getParameter("formulaire")).andReturn("defautStyleCss");
		expectLastCall().times(4);
		expect(this.httpServletRequestMock.getContentType()).andReturn("");

		expect(this.servletContextMock.getRealPath("/css/styleDefaut.css")).andReturn("");

		String[] proprietes = new String[] { "", "", "" };
		expect(this.daoFactoryMock.getProprietes()).andReturn(proprietes);
		expect(this.daoFactoryMock.getUrlBdd()).andReturn("");
		expect(this.daoFactoryMock.getDriver()).andReturn("");
		expect(this.daoFactoryMock.getUserBdd()).andReturn("");
		expect(this.daoFactoryMock.getMdpBdd()).andReturn("");
		
		expect(this.servletContextMock.getRealPath("/css/style.css")).andReturn("");
		expect(this.servletContextMock.getRealPath("/css/style.css")).andReturn("");
		expect(this.servletContextMock.getRealPath("/images")).andReturn("");
		

		this.replayAll();

		/* Cycle de vie d'une servlet */
		this.gererApplication.init(this.servletConfigMock);
		this.gererApplication.doPost(this.httpServletRequestMock, this.httpservletResponseMock);
		this.gererApplication.destroy();

		this.verifyAll();
	}

}