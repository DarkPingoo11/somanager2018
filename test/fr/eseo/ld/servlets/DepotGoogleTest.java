package fr.eseo.ld.servlets;

import fr.eseo.ld.beans.OptionESEO;
import fr.eseo.ld.beans.Parametre;
import fr.eseo.ld.beans.Role;
import fr.eseo.ld.beans.Utilisateur;
import fr.eseo.ld.dao.*;
import fr.eseo.ld.utils.CallbackUtilitaire;
import org.easymock.*;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.easymock.EasyMock.*;

/**
 * Classe de tests unitaires JUnit 4 de la classe Dashboard.
 *
 * <p>Utilisation de mocks (construits avec EasyMock).</p>
 *
 * @version 1.0
 * @author Maxime LENORMAND
 *
 * @see Dashboard
 * @see org.junit
 * @see org.easymock
 */
@RunWith(EasyMockRunner.class)
public class DepotGoogleTest extends ServletTest {

	@Mock
	private ParametreDAO parametreDAO;

	@TestSubject
	private DepotGoogle depotgoogle = new DepotGoogle();


	public void servletLife() throws ServletException, IOException {
		expect(this.getServletConfigMock().getServletContext()).andReturn(getServletContextMock()).times(2);
		expect(getServletContextMock().getAttribute(anyObject())).andReturn(getDaoFactoryMock());
		expect(this.getServletContextMock().getRequestDispatcher(anyObject())).andReturn(getRequestDispatcher());

		getRequestDispatcher().forward(anyObject(), anyObject());
		expectLastCall();

		this.replayAll();

		this.depotgoogle.init(getServletConfigMock());
		this.depotgoogle.doPost(getRequest(), getResponse());
		this.depotgoogle.destroy();

		this.verifyAll();
	}

	/**
	 * Teste la méthode protected void doGet(HttpServletRequest request, HttpServletResponse response).
	 * 
	 * <p>Cas d'un utilisateur non connecté.</p>
	 * 
	 * @throws ServletException
	 * @throws IOException
	 */
	@Test
	public void testDoChangerLien() throws ServletException, IOException {
		//Mock
		expect(this.getDaoFactoryMock().getParametreDAO()).andReturn(parametreDAO);

		//Params
		Parametre parametreLien = new Parametre();
		parametreLien.setNomParametre(ParametreDAO.IDENTIFIANT_LIEN_FORM_SUJET);
		List<Parametre> params = new ArrayList<>();
		params.add(parametreLien);

		//DO
		String lien = "https://docs.google.com/forms/u/0/d/1QSwZ0oX3awmSGj-NHWn7dzkiQ1QBonAGox7psC-qoOM/edit?usp=forms_home&ths=true";
		expect(this.getRequest().getParameter("nouveauLien")).andReturn(lien);

		expect(parametreDAO.trouver(anyObject())).andReturn(params);

		parametreDAO.modifier(anyObject());
		expectLastCall();


		/* Cycle de vie d'une servlet */
		expect(getRequest().getParameter(anyString())).andReturn("changerLien").times(2);
		this.servletLife();
	}

}