package fr.eseo.ld.servlets;

import static org.easymock.EasyMock.expect;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import org.easymock.EasyMockRunner;
import org.easymock.EasyMockSupport;
import org.easymock.Mock;
import org.easymock.MockType;
import org.easymock.TestSubject;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import fr.eseo.ld.beans.AnneeScolaire;
import fr.eseo.ld.beans.Equipe;
import fr.eseo.ld.beans.EtatSujet;
import fr.eseo.ld.beans.Etudiant;
import fr.eseo.ld.beans.EtudiantEquipe;
import fr.eseo.ld.beans.OptionESEO;
import fr.eseo.ld.beans.Sujet;
import fr.eseo.ld.beans.Utilisateur;
import fr.eseo.ld.dao.AnneeScolaireDAO;
import fr.eseo.ld.dao.DAOFactory;
import fr.eseo.ld.dao.EquipeDAO;
import fr.eseo.ld.dao.EtudiantDAO;
import fr.eseo.ld.dao.NotificationDAO;
import fr.eseo.ld.dao.OptionESEODAO;
import fr.eseo.ld.dao.PosterDAO;
import fr.eseo.ld.dao.SujetDAO;
import fr.eseo.ld.dao.UtilisateurDAO;

/**
 * Classe de tests unitaires JUnit 4 de la classe DeposerPoster.
 *
 * <p>Utilisation de mocks (construits avec EasyMock).</p>
 *
 * @version 1.0
 * @author Pierre CESMAT
 *
 * @see fr.eseo.ld.servlets.DeposerPoster
 * @see org.junit
 * @see org.easymock
 */
@RunWith(EasyMockRunner.class)
public class DeposerPosterTest extends EasyMockSupport {
	
	private static final String CONF_DAO_FACTORY = "daoFactory";
	private static final String ATT_SESSION_USER = "utilisateur";
	
	private static final String CHEMIN_POSTER = "/home/etudiant/Posters/2018/test.pdf";
	private static final Long ID_UTILISATEUR = 19L;
	
	private static PosterDAO posterDAO;
	private static NotificationDAO notificationDAO;
	private static EquipeDAO equipeDAO;
	private static Utilisateur utilisateurTest;
	private static Etudiant etudiantTest;
	private static Sujet sujetTest;
	private static Equipe equipeTest;
	private static UtilisateurDAO utilisateurDAO;
	private static EtudiantDAO etudiantDAO;
	private static SujetDAO sujetDAO;
	private static EtudiantEquipe etudiantEquipeTest ;
	private static OptionESEODAO optionESEODAO;
	private static OptionESEO optionESEOTest;
	private static AnneeScolaireDAO anneeScolaireDAO;
	private static AnneeScolaire anneeTest;
	
	
	@TestSubject
	private DeposerPoster deposerPoster = new DeposerPoster();
	
	@Mock
	private ServletConfig servletConfigMock;
	
	@Mock
	private ServletContext servletContextMock;
	
	@Mock
	private HttpServletRequest httpServletRequestMock;
	
	@Mock(type = MockType.NICE)
	private HttpServletResponse httpservletResponseMock;
	
	@Mock
	private HttpSession httpSessionMock;
	
	@Mock
	private RequestDispatcher requestDispatcherMock;
	
	@Mock
	private DAOFactory daoFactoryMock;
	
	@Mock
	private Part part;
	
	/**
	 * Récupère des instances de DAO.
	 * 
	 * @throws Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		final DAOFactory daoFactory = DAOFactory.getInstance();
		notificationDAO = daoFactory.getNotificationDao();
		posterDAO = daoFactory.getPosterDao();
		equipeDAO = daoFactory.getEquipeDAO();
		utilisateurDAO = daoFactory.getUtilisateurDao();
		etudiantDAO = daoFactory.getEtudiantDao();
		sujetDAO = daoFactory.getSujetDao();
		optionESEODAO = daoFactory.getOptionESEODAO();
		anneeScolaireDAO = daoFactory.getAnneeScolaireDAO();
		
/*Création des données de test*/
		
		/* Insertion d'un utilisateur test dans la BDD */
        Utilisateur utilisateur = new Utilisateur();
        utilisateur.setIdentifiant("test");
        utilisateur.setNom("test");
        utilisateur.setPrenom("test");
        utilisateur.setValide("oui");
        utilisateurDAO.creer(utilisateur);
        utilisateurTest = utilisateurDAO.trouver(utilisateur).get(0);
        
        Etudiant etudiant = new Etudiant();
        etudiant.setIdEtudiant(utilisateurTest.getIdUtilisateur());
        etudiant.setAnnee(2018);
        etudiant.setContratPro("non");
        etudiantDAO.creer(etudiant);
        etudiantTest = etudiantDAO.trouver(etudiant).get(0);
        
		Sujet sujet = new Sujet();
		sujet.setTitre("sujetTest");
		sujet.setDescription("description");
		sujet.setNbrMinEleves(1);
		sujet.setNbrMaxEleves(4);
		sujet.setEtat(EtatSujet.ATTRIBUE);
		sujetDAO.creer(sujet);
		sujetTest = sujetDAO.trouver(sujet).get(0);
		
		OptionESEO option = new OptionESEO();
		option.setIdOption(1L);
		option.setNomOption("LD");
		optionESEODAO.creer(option);
		optionESEOTest = optionESEODAO.trouver(option).get(0);
		
		AnneeScolaire annee = new AnneeScolaire();
		annee.setAnneeDebut(2018);
		annee.setAnneeFin(2019);
		annee.setIdOption(1L);
		anneeScolaireDAO.creer(annee);
		anneeTest = anneeScolaireDAO.trouver(annee).get(0);
		
		Equipe equipe = new Equipe();
		equipe.setAnnee(2018);
		equipe.setTaille(1);
		equipe.setIdSujet(sujetTest.getIdSujet());
		equipe.setValide("oui");
		equipeDAO.creer(equipe);
		equipeTest = equipeDAO.trouver(equipe).get(0);
		
		EtudiantEquipe etudiantEquipe = new EtudiantEquipe();
		etudiantEquipe.setIdEquipe(equipeTest.getIdEquipe());
		etudiantEquipe.setIdEtudiant(etudiantTest.getIdEtudiant());
		equipeDAO.attribuerEtudiantEquipe(etudiantEquipe);
		etudiantEquipeTest = equipeDAO.trouverEtudiantEquipe(etudiantEquipe).get(0);
	}
	
	/**
     * Supprime les données de test dans la BDD.
     *
     * @throws Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
    	equipeDAO.supprimerEtudiantEquipe(etudiantEquipeTest);
    	anneeScolaireDAO.supprimer(anneeTest);
    	optionESEODAO.supprimer(optionESEOTest);
    	sujetDAO.supprimer(sujetTest);
    	etudiantDAO.supprimer(etudiantTest);
        utilisateurDAO.supprimer(utilisateurTest);
    }
    
	/**
	 * Teste la méthode protected void doPost(HttpServletRequest request, HttpServletResponse response).
	 * 
	 * @throws ServletException
	 * @throws IOException
	 */
	@Test
	public void testDoPost() throws ServletException, IOException {
		/* Récupération d'instances de DAO */
		expect(this.servletConfigMock.getServletContext()).andReturn(this.servletContextMock);
		expect(this.servletContextMock.getAttribute(CONF_DAO_FACTORY)).andReturn(this.daoFactoryMock);
		expect(this.daoFactoryMock.getNotificationDao()).andReturn(notificationDAO);
		expect(this.daoFactoryMock.getPosterDao()).andReturn(posterDAO);
		expect(this.daoFactoryMock.getEquipeDAO()).andReturn(equipeDAO);
		
		/* Récupération de l'ID de l'Utilisateur en session */
		expect(this.httpServletRequestMock.getSession()).andReturn(this.httpSessionMock);
		expect(this.httpSessionMock.getAttribute(ATT_SESSION_USER)).andReturn(utilisateurTest);
		
		/* Récupération des données saisies */
		expect(this.httpServletRequestMock.getPart( "poster" )).andReturn(part);
		expect(this.part.getHeader( "content-disposition" )).andReturn("filename=\"test.pdf\";");
		expect(this.httpServletRequestMock.getHeader( "referer" )).andReturn("test");
		InputStream stream = new FileInputStream(CHEMIN_POSTER);//ne fonctionne pas en local
		expect(this.part.getInputStream()).andReturn(stream);
		
		this.replayAll();
		
		/* Cycle de vie d'une servlet */
		this.deposerPoster.init(this.servletConfigMock);
		this.deposerPoster.doPost(this.httpServletRequestMock, this.httpservletResponseMock);
		this.deposerPoster.destroy();
	}
}
