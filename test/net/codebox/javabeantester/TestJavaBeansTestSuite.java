package net.codebox.javabeantester;

import fr.eseo.ld.servlets.*;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

/**
 * Suite de tests unitaires JUnit 4 pour le package net.codebox.javabeantester
 *
 * @author Tristan LE GACQUE
 *
 * @see org.junit
 */
@RunWith(Suite.class)
@SuiteClasses({
		TestJavaBeanTester.class
})
public class TestJavaBeansTestSuite {
	/* Classe vide */
}